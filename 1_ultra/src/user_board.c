/*
 *  HYDRA Board.c
 */
 
#include <stdlib.h>
#include "prime_framework.h"
#include "user_board.h"
#include "prime_CO_hal.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_uart0.h"
#include "prime_rit.h"
#include "prime_can1.h"
#include "prime_delay.h"
#include "prime_timer0.h"
#include "prime_ultrasonic.h"

#define 	BV(_n_) 		(1 << (_n_))

#define 	BOARD_LED0_PORT	 	4
#define 	BOARD_LED0_PIN  	29
#define 	BOARD_LED1_PORT 	0
#define 	BOARD_LED1_PIN  	26
#define 	BOARD_LED2_PORT 	0
#define 	BOARD_LED2_PIN  	25
#define 	BOARD_BTN0_PORT 	0
#define 	BOARD_BTN0_PIN  	24
#define 	BOARD_BTN1_PORT 	0
#define 	BOARD_BTN1_PIN  	23
#define 	BOARD_BTN2_PORT 	1
#define 	BOARD_BTN2_PIN  	31

extern void app_timerIrqHandler(void);
extern void app_canIrqHandler(void);

#if BOARD_USE_SYSTICK == 1
	void SysTick_Handler(void);
#else
	void RIT_IRQHandler(void);
#endif
void CAN_IRQHandler(void);

#define GPIO_PIN_COUNT		8

PFCfgGpio gpioPins[] = 
{
	// LED 2, LED 3
	{GPIO_PORT_0, GPIO_PIN_25 | GPIO_PIN_26, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED 1
	{GPIO_PORT_4, GPIO_PIN_29, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// UART 0 Tx
	{GPIO_PORT_0, GPIO_PIN_2, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	// UART 0 Rx
	{GPIO_PORT_0, GPIO_PIN_3, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0},
	// CAN 1 RD
	{GPIO_PORT_0, GPIO_PIN_21, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_21_RD1},
	// CAN 2 RD
	{GPIO_PORT_0, GPIO_PIN_4, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_4_RD2},
	// CAN 1 TD
	{GPIO_PORT_0, GPIO_PIN_22, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_22_TD1},
	// CAN 2 TD
	{GPIO_PORT_0, GPIO_PIN_5, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_5_TD2}
};

PFCfgClk clockcfg = 
{	
	100000000,			//cpuFreqHz
	12000000,			//oscFreq
	enPllClkSrcMainOSC	//pllClkSrc
};


PFCfgGpio pfUART0Cfg[2] = 
{
	//TX0
	{UART_0_TX_PORT,UART_0_TX_PIN, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	//RX0
	{UART_0_RX_PORT,UART_0_RX_PIN, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0},
};

PFCfgUart0 uartConfig = 
{
	enPclkDiv_4, 			
	enUart0Baudrate_9600, 	
	enUart0Databits_8, 		
	enUart0ParityNone, 		
	enUart0StopBits_1, 
	enUart0IntNone
};

PFCfgRit ritConfig = 
{
	25000,			// configure RIT for 10us
	0x00,			// compare mask
	app_timerIrqHandler,	// callback
	enPclkDiv_4,	// pclk divider
	enBooleanTrue,	// halt on break
	enBooleanTrue,	// reset on match
};

void canHandler()
{
	pfUart0WriteString("CAN Error\r");
}

PFCfgCan1 can1config= 
{	
	app_canIrqHandler,
	app_canIrqHandler,
	100000,				// baudrate
	canHandler,			// CAN Error callback
	enPclkDiv_4,		// ClkDiv
	enCan1IntTxRx		// interrupts
};



PFCfgGpio pfUltrasonicGpio[10] =
{
			//ultrasonic trigger_0
			{J6_3_TRIGGER0_PORT, J6_3_TRIGGER0_PIN, enGpioDirOutput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio}, 
			//ultrasonic echo_0
			{J6_4_ECHO0_PORT, J6_4_ECHO0_PIN, enGpioDirInput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio}, 

			//ultrasonic trigger_1
			{J6_5_TRIGGER1_PORT, J6_5_TRIGGER1_PIN, enGpioDirOutput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio}, 
			//ultrasonic echo_1
			{J6_6_ECHO1_PORT, J6_6_ECHO1_PIN, enGpioDirInput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio},  
			
			//ultrasonic trigger_2
			{J6_9_TRIGGER2_PORT, J6_9_TRIGGER2_PIN, enGpioDirOutput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio}, 
			//ultrasonic echo_2		
			{J6_10_ECHO2_PORT, J6_10_ECHO2_PIN, enGpioDirInput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio} , 
		
			//ultrasonic trigger_3
			{J6_11_TRIGGER3_PORT, J6_11_TRIGGER3_PIN, enGpioDirOutput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio}, 
			//ultrasonic echo_3
			{J6_12_ECHO3_PORT, J6_12_ECHO3_PIN, enGpioDirInput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio},  
			
			//ultrasonic trigger_4
			{J6_13_TRIGGER4_PORT, J6_13_TRIGGER4_PIN, enGpioDirOutput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio},
			//ultrasonic echo_4
			{J6_14_ECHO4_PORT, J6_14_ECHO4_PIN, enGpioDirInput, enGpioPinModePullDown, enGpioOpenDrainDisable, enGpioFunctionGpio} 

};


PFCfgUltrasonic ultrasonicConfig[5] = {
											{
												{ J6_3_TRIGGER0_PORT, J6_3_TRIGGER0_PIN},
												{ J6_4_ECHO0_PORT, J6_4_ECHO0_PIN}
											},																		
											{
												{ J6_5_TRIGGER1_PORT, J6_5_TRIGGER1_PIN},
												{ J6_6_ECHO1_PORT, J6_6_ECHO1_PIN}
											},				

											{
												{ J6_9_TRIGGER2_PORT, J6_9_TRIGGER2_PIN},
												{ J6_10_ECHO2_PORT, J6_10_ECHO2_PIN}
											},
											{
												{ J6_11_TRIGGER3_PORT, J6_11_TRIGGER3_PIN},
												{ J6_12_ECHO3_PORT, J6_12_ECHO3_PIN}
											},
											{
												{ J6_13_TRIGGER4_PORT, J6_13_TRIGGER4_PIN},
												{ J6_14_ECHO4_PORT, J6_14_ECHO4_PIN}
											}
									};
PFCfgTimer0 timer0Config =
{
		2,
		{250,0,0,0},
		{enTimer0MatchActResetInt,enTimer0MatchActNone,enTimer0MatchActNone,enTimer0MatchActNone},
		{enTimer0ExtMatchCtrlNone,enTimer0ExtMatchCtrlNone,enTimer0ExtMatchCtrlNone,enTimer0ExtMatchCtrlNone},
		pfDelayTickUpdate,
		enPclkDiv_4,
		enTimer0ModeTimer,
		enBooleanTrue
};

extern PFbyte id[2];
int pfBoardinit(PFCoCanModuleHwConfig *canHwCfg)
{
	PFEnStatus status;
	PFbyte buf[128];

	// system clock init
	pfSysSetCpuClock(&clockcfg);
	
	status = pfGpioInit(gpioPins, GPIO_PIN_COUNT);
	if(status != enStatusSuccess)
	{
		while(1);
	}

	status = pfUart0Open(&uartConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	pfUart0WriteString("Uart initialized\r");
	
	status = pfCan1Open(&can1config);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	pfUart0WriteString("Can initialized\r");
	
    canHwCfg->uBase = PERIPH_CAN1;

#if BOARD_USE_SYSTICK == 1
    SYSTICK_InternalInit(1);
    SYSTICK_IntCmd(DISABLE);
    SYSTICK_Cmd(ENABLE);
#else
    status = pfRitOpen(&ritConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	pfDelaySetTimerPeriod(10);
#endif
	
	pfGpioPinsClear(BOARD_LED0_PORT, BOARD_LED0_PIN);
	pfGpioPinsClear(BOARD_LED1_PORT, BOARD_LED1_PIN);
	pfGpioPinsClear(BOARD_LED2_PORT, BOARD_LED2_PIN);
    //Initialize buttons for TPDO
    pfGpioPinsSet(BOARD_BTN0_PORT, BOARD_BTN0_PIN);
    pfGpioPinsSet(BOARD_BTN1_PORT, BOARD_BTN1_PIN);
    pfGpioPinsSet(BOARD_BTN2_PORT, BOARD_BTN2_PIN);

#if BOARD_USE_SYSTICK == 0
    /* CAN should have higher prio then 1ms timer */
    NVIC_SetPriority(RIT_IRQn, 10);
    NVIC_SetPriority(CAN_IRQn, 5);
	NVIC_SetPriority(UART0_IRQn,2);
#endif


	
	// GPIO initialization - Ultrasonic Modules
	status = pfGpioInit(pfUltrasonicGpio,2);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	// TIMER0 initialization
	status = pfTimer0Open(&timer0Config);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	else
		pfUart0WriteString("\rTimer 0 initialized successfully.");
	
	pfUart0WriteString("\rultra GPIO initializing");
	// GPIO initialization - Ultrasonic Modules
	status = pfGpioInit(pfUltrasonicGpio,10);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	pfUart0WriteString("\rultra GPIO initialized successfully.");
	// Ultrasonic initialization
		
	status = pfUltrasonicOpen(id,ultrasonicConfig,1);
	if(status != enStatusSuccess)
	{
		sprintf(buf,"\rstatus = %d",status);
		pfUart0WriteString(buf);
		pfUart0WriteString("\rUltrasonic initialization failed.");
		while(1);
	}
	else
		pfUart0WriteString("\rUltrasonic initialized successfully.");

	pfUart0Write("\rPhi Robotics", 13);
	pfDelaySetTimerPeriod(30);
	pfTimer0Start();

    return 0;
}

void leds_write(PFbyte ledsBits)
{
    if(ledsBits & 0x1)
        pfGpioPinsSet(BOARD_LED0_PORT, BV(BOARD_LED0_PIN));
    else
        pfGpioPinsClear(BOARD_LED0_PORT, BV(BOARD_LED0_PIN));
    if(ledsBits & 0x2)
        pfGpioPinsSet(BOARD_LED1_PORT, BV(BOARD_LED1_PIN));
    else
        pfGpioPinsClear(BOARD_LED1_PORT, BV(BOARD_LED1_PIN));
    if(ledsBits & 0x4)
        pfGpioPinsSet(BOARD_LED2_PORT, BV(BOARD_LED2_PIN));
    else
        pfGpioPinsClear(BOARD_LED2_PORT, BV(BOARD_LED2_PIN));
}

PFbyte leds_read(void)
{
    PFbyte bits = 0;

    PFdword val = pfGpioPortRead(BOARD_LED0_PORT);
    if(val & BV(BOARD_LED0_PIN))
        bits |= 0x1;
    val = pfGpioPortRead(BOARD_LED1_PORT);
    if(val & BV(BOARD_LED1_PIN))
        bits |= 0x2;
    val = pfGpioPortRead(BOARD_LED2_PORT);
    if(val & BV(BOARD_LED2_PIN))
        bits |= 0x4;

		
	
    return bits;
}

PFbyte buttons_read(void)
{
    PFbyte bits = 0;

    PFdword val = pfGpioPortRead(BOARD_BTN0_PORT);
    if(val & BV(BOARD_BTN0_PIN))
        bits |= 0x1;
    val = pfGpioPortRead(BOARD_BTN1_PORT);
    if(val & BV(BOARD_BTN1_PIN))
        bits |= 0x2;
    val = pfGpioPortRead(BOARD_BTN2_PORT);
    if(val & BV(BOARD_BTN2_PIN))
        bits |= 0x4;

    return bits;
}

PFbyte board_get_char(void)
{
    PFbyte tmp = 0;
    if(pfUart0ReadByte(&tmp) == 0)
		return tmp;
	else
		return 0xFF;
	
}

void board_put_char(PFbyte c)
{
    pfUart0WriteByte(c); 
}

#if BOARD_USE_SYSTICK == 1
void SysTick_Handler(void)
{
    //clear interrupt flag bit
    SYSTICK_ClearCounterFlag();
    app_timerIrqHandler();
}
#else
/*
void TIMER0_IRQHandler(void)
{
    //clear interrupt flag bit
    TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);
    app_timerIrqHandler();
}*/
#endif

/*
void CAN_IRQHandler(void)
{
    app_canIrqHandler();
} */

