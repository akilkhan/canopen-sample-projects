/*
 * HYDRA board.h
 *
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "prime_framework.h"
#include "prime_CO_driver.h"
#include "prime_can2.h"

#define BOARD_USE_SYSTICK       0

#if BOARD_USE_SYSTICK == 1
	#include "prime_systick.h"
#else
	#include "prime_rit.h"
#endif

#define USED_BOARD_DEBUG_UART   0

#if (USED_BOARD_DEBUG_UART==0)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART0
#elif (USED_BOARD_DEBUG_UART==1)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART1
#elif (USED_BOARD_DEBUG_UART==2)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART2
#elif (USED_BOARD_DEBUG_UART==3)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART3
#endif

#define 	DEBUGS(x)   	pfUart0WriteString(x)
#define 	GETCHAR()   	board_get_char()

int pfBoardinit(PFCoCanModuleHwConfig *canHwCfg);

#define board_reset()       do{/*WDT_Start(10000);*/while(1);/*NVIC_SystemReset();*/}while(0)

#if BOARD_USE_SYSTICK == 1
	#define timer_enableIrq()   SYSTICK_IntCmd(ENABLE)
	#define timer_disableIrq()  SYSTICK_IntCmd(DISABLE)
#else
	#define timer_enableIrq()   NVIC_EnableIRQ(RIT_IRQn)
	#define timer_disableIrq()  NVIC_DisableIRQ(RIT_IRQn)
#endif

#define 	can_enableIrq()     NVIC_EnableIRQ(CAN_IRQn)
#define 	can_disableIrq()    NVIC_DisableIRQ(CAN_IRQn)

void leds_write(PFbyte ledsBits);
PFbyte leds_read(void);
PFbyte buttons_read(void);
PFbyte board_get_char(void);
void board_put_char(PFbyte c);


/** 						UART configuration macros 						*/

/** UART 0 */
#define 	UART_0_TX_PORT		GPIO_PORT_0
#define 	UART_0_TX_PIN			GPIO_PIN_2

#define 	UART_0_RX_PORT		GPIO_PORT_0
#define		UART_0_RX_PIN			GPIO_PIN_3


/** 								Ultrasonic Configuration macros 
*/
#define		J6_3_TRIGGER0_PORT				GPIO_PORT_3
#define		J6_3_TRIGGER0_PIN				GPIO_PIN_26 

#define		J6_4_ECHO0_PORT					GPIO_PORT_3
#define		J6_4_ECHO0_PIN					GPIO_PIN_25 

#define		J6_5_TRIGGER1_PORT				GPIO_PORT_1
#define		J6_5_TRIGGER1_PIN				GPIO_PIN_29 

#define		J6_6_ECHO1_PORT					GPIO_PORT_2
#define		J6_6_ECHO1_PIN					GPIO_PIN_13 

#define		J6_9_TRIGGER2_PORT				GPIO_PORT_0
#define		J6_9_TRIGGER2_PIN					GPIO_PIN_20 

#define		J6_10_ECHO2_PORT					GPIO_PORT_0
#define		J6_10_ECHO2_PIN						GPIO_PIN_19 

#define		J6_11_TRIGGER3_PORT				GPIO_PORT_2
#define		J6_11_TRIGGER3_PIN				GPIO_PIN_7 

#define		J6_12_ECHO3_PORT					GPIO_PORT_2
#define		J6_12_ECHO3_PIN						GPIO_PIN_6 

#define		J6_13_TRIGGER4_PORT				GPIO_PORT_2
#define		J6_13_TRIGGER4_PIN				GPIO_PIN_5

#define		J6_14_ECHO4_PORT					GPIO_PORT_2
#define		J6_14_ECHO4_PIN						GPIO_PIN_4



#endif /* BOARD_H_ */
