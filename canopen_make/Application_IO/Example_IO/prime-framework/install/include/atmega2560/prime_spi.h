/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework SPI driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */

#pragma once


#define SPI_CH				SPI
#define SPI_CHANNEL			PERIPH(SPI_CH)

/** 
 * Mark PF_SPI_USE_FIFO as 1 if internal software buffer is to be used in interrupt based communication.
 * If it is marked as 0, user should provide callbacks to handle transmit and receive interrupts.
 */
#define PF_SPI_USE_FIFO				1
	
#if(PF_SPI_USE_FIFO != 0)
/** 
 * Define size in bytes for internal software buffer.
 * The buffer size should be a non-zero and power of 2 number.
 */
	#define SPI_BUFFER_SIZE		256
#endif	// #if(PF_SPI_USE_FIFO != 0)



#define SPI_MAX_DEVICE_SUPPORTED	8

/** Enumeration for SPI modes		*/
typedef enum
{	
	enFreqDivideBy4,				/**< SPI Clock Rate = (Oscillator Frequency /4) */
	enFreqDivideBy16,				/**< SPI Clock Rate = (Oscillator Frequency /16) */
	enFreqDivideBy64,				/**< SPI Clock Rate = (Oscillator Frequency /64) */
	enFreqDivideBy128,				/**< SPI Clock Rate = (Oscillator Frequency /128) */
	enFreqDivideBy2,				/**< SPI Clock Rate = (Oscillator Frequency /2) */
	enFreqDivideBy8,				/**< SPI Clock Rate = (Oscillator Frequency /8) */
	enFreqDivideBy32				/**< SPI Clock Rate = (Oscillator Frequency /32) */
}PFEnSpiBuad;

typedef enum
{
	enSpiMode_0 = 0,				/**< SPI mode 0: Phase 0, Polarity 0	*/
	enSpiMode_1,					/**< SPI mode 1: Phase 0, Polarity 1	*/
	enSpiMode_2,					/**< SPI mode 2: Phase 1, Polarity 0	*/
	enSpiMode_3,					/**< SPI mode 3: Phase 1, Polarity 1	*/
}PFEnSpiDataMode;


typedef struct
{
	PFEnSpiDataMode		spiMode;		/**< SPI communication mode				*/
	PFEnBoolean			interrupt;		/**< Interrupt Enable/Disable			*/
	PFEnBoolean			masterMode;		/**< Master/Slave						*/
	PFEnSpiBuad			baudRate;		/**< Set baud rate for SPI Communication*/
#if(SPI_USE_FIFO ==0)
	PFcallback			spiCallback;	
#endif
}PFCfgSpi;

typedef PFCfgSpi* PFpCfgSpi;

/**
 * Initializes the SPI channel with provided settings
 *
 * \param config configuration structure which contains the settings for the communication channel to be used.
 * \return SPI initialization status.
 */
PFEnStatus pfSpiOpen(PFpCfgSpi config);

/**
 * Turn offs the SPI channel
 * 
 * \return Status.
 */
PFEnStatus pfSpiClose(void);

/**
 * Registers SPI device to operate. 
 * The SPI device should be registered before any read write operation take place for that device.
 *
 * \param id Pointer to load the id generated for the device being registered. This id is required to carry out any operation on that device.
 * \param chipSelect Pointer to PFGpioPortPin structure which describes the chip select pin used for the device.
 *
 * \return Device registration status.
 */
PFEnStatus pfSpiRegisterDevice(PFbyte* id, PFpGpioPortPin chipSelect);

/**
 * Unregisters the SPI0 device from the device list.
 * No operation on the device is possible once it is unregistered.
 * In order to operate on that device, it needs to be registered again.
 *
 * \param id Pointer to id of the device to be unregistered.
 *
 * \return Device unregistration status.
 */
PFEnStatus pfSpiUnregisterDevice(PFbyte* id);

/**
 * Operate chip select pin of the device to start or stop the communication.
 *
 * \param id Pointer to id of the device to operate the chip select pin
 * \param pinStatus Status of the chip select pin to set. 0 for making chip select low, 1 for making it high
 *
 * \return Chip select operation status
 *
 * \note Before calling any SPI read/write function for a device, it necessary to assert the chip select first.
 * Once chip select of any device is asserted, chip select for any other device can not be asserted before de-asserting the previous device chip select.
 */
PFEnStatus pfSpiChipSelect(PFbyte* id, PFbyte pinStatus);

/**
 * This function writes 16 bit data to SPI data register and 
 * returns 16 bit received from SPI bus.
 *
 * \param id ID for subscribed device.
 * \param data 16 bit data to write to the UART channel. 
 *
 * \return 16 bit data received from SPI bus. 
 *
 * \note Even if the datasize for parameter and return data is 16 bit,
 * valid data size will depend upon the databits setting for the channel. 
 */
PFEnStatus pfSpiExchangeByte( PFbyte* id ,PFbyte data, PFbyte* exchangevalue);

/**
 * The function sends multiple bytes on SPI channel.  
 * If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
 * Otherwise it will wait in the function and send each byte by polling the line status.
 *
 * \param id ID for subscribed device.
 * \param data Pointer to the data to be sent.
 * \param size Total number of bytes to send.
 *
 * \return SPI write status.
 */
PFEnStatus pfSpiWrite(PFbyte* id, PFbyte* data, PFdword size);

/**
 * The function reads one byte from SPI channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param id ID for subscribed device.
 * \param data pointer to the buffer where the read data should be loaded.
 * \param size total number of bytes to read.
 * \param readBytes pointer to double word, in which function will fill number bytes actually read.
 *
 * \return SPI read status.
 */
PFEnStatus pfSpiRead(PFbyte* id, PFbyte* data, PFdword size, PFdword* readBytes);

/**
 * Returns the SPI channel interrupt status
 *
 * \return interrupt status for SPI channel
 */
PFdword pfSpiGetIntStatus(void);

/**
 * Clears pending flags for given interrupts
 *
 * \param intStatus interrupts for which flags are to be cleared
 *
 * \return interrupt flag clear status
 */
PFEnStatus pfSpiClearIntStatus(PFdword intStatus);

#if(PF_SPI_USE_FIFO != 0)
/**
 * Returns the number of bytes received in SPI buffer.
 *
 * \param count Pointer to variable where count will be loaded.
 *
 * \return Status.
 */
PFEnStatus pfSpiGetRxBufferCount(PFdword* count);

/**
 * This function empties the transmit buffer.
 *
 * \return Status.
 */
PFEnStatus pfSpiTxBufferFlush(void);

/**
 * This function empties the receive buffer.
 *
 * \return Status.
 */
PFEnStatus pfSpiRxBufferFlush(void);
#endif	// #if(PF_SPI_USE_FIFO != 0)

/** @} */



