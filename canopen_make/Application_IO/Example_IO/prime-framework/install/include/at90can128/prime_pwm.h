#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 

#define PWM_CH				TIMER0
#define PWM_CHANNEL			PERIPH(PWM_CH)
//  #if(( PWM_TIMER !=TIMER0) ||(PWM_TIMER !=TIMER2))
//  #error "PWM is available only on TIMER0 and TIMER2 for this controller "
//  #endif
//  
 #define PWM_OVF_VECT 		__PWM_OVF_VECT(PWM_CHANNEL)
  #define __PWM_OVF_VECT(x)		_PWM_OVF_VECT(x)
  #define _PWM_OVF_VECT(x)		x##_OVF_vect
  
  #define PWM_COMPA_VECT 		__PWM_COMPA_VECT(PWM_CHANNEL)
  #define __PWM_COMPA_VECT(x)		_PWM_COMPA_VECT(x)
  #define _PWM_COMPA_VECT(x)		x##_COMPA_vect
  
  #define PWM_COMPB_VECT 		__PWM_COMPB_VECT(PWM_CHANNEL)
  #define __PWM_COMPB_VECT(x)		_PWM_COMPB_VECT(x)
  #define _PWM_COMPB_VECT(x)		x##_COMPB_vect
  
	
/** Enumeration for timer modes			*/
typedef enum
{
	enPwmNoclock = 0,			/**No clock source is selected and the timer is disabled	*/
	enPwmNoprescaledClk,		/**< clock source is selected and the timer is not pre-scaled	*/
	enPwmClkDivideby8,			/**< Clock source freqency is divided by 8	*/
	enPwmClkDivideby64,		/**< Clock source freqency is divided by 8	*/
	enPwmClkDivideby256,		/**< Clock source freqency is divided by 8	*/
	enPwmClkDivideby1024,		/**< Clock source freqency is divided by 8	*/
	enPwmExtClkFallingEdge,	/**<  timer with external source on falling edge 	*/
	enPwmExtClkRisingEdge		/**<  timer with external source on falling edge 	*/
}PFEnPwmClocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enPwmExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enPwmExtMatchCtrlTogglePin,			/**< Toggle match pin								*/
	enPwmExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enPwmExtMatchCtrlSetPin				/**< Set match pin									*/
}PFEnPwmExtMatchCtrl;

typedef enum
{
	enPwmPhaseCrctMode=0x01,	/**<	Phase correct Mode 	*/
	enPwmFastMode=0x03,			/**<	Fast Mode	*/  
}PFEnPwmMode;

typedef enum
{
	enPwmIntNone,			/**	No interrupt	*/
	enPwmOverflowIntr=0x01,	/**	interrupt on overflow	*/
	enPwmMatchRegAIntr=0x02,	/**	interrupt on matching with the value in OCR0A 	*/
	enPwmMatchRegBIntr=0x04,	/**	interrupt on matching with the value in OCR0B 	*/
	enPwmAllBIntr=0x07,	/**	interrupt on matching with the value in OCR0A ,OCR0B and overflow 	*/
}PFEnPwmIntrrupt;

/**		Timer configure structure		*/
typedef struct
{
	PFEnPwmClocksource	    clockSource;		/**< Select clock source				*/
	PFEnPwmMode			    pwmMode;			/** PWM mode*/
	PFbyte				    matchValueA;		/**< Match register A compare value							*/
	PFEnPwmExtMatchCtrl 	exMatchActionA;	/**< match pin control on count match 				*/ 
	PFEnPwmIntrrupt     	interrupt;			/**< To enable or disable timer interrupt			*/
	PFcallback			    cmpMatchACallback;	/**< Callback function for timer ISR				*/
	PFcallback			    overFlowCallback;
}PFCfgPwm;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgPwm* PPFCfgPwm;

/**
 * Intialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer intialization status
 */
PFEnStatus pfPwmOpen(PPFCfgPwm config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfPwmClose(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfPwmStart(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfPwmStop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfPwmReset(void);

/**
 * Returns the timer count
 * 
 * \return timer count
 */
PFEnStatus pfTimerReadCount(void);



/** @} */

