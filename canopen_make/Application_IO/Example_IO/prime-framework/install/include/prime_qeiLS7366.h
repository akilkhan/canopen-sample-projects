/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework LS7366Left Quadrature Encoder Driver
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_LS7366Left_API LS7366Left QEI API
 * @{
 */


#define  PF_MAX_QEI_SUPPORTED				4

/** Enumeration for quadrature count mode		*/
typedef enum
{
	enQeiLS7366QuadratureModeNonQuadrature = 0,	/**< Non quadrature mode. A = clock, B = direction.				*/
	enQeiLS7366QuadratureMode1x,				/**< x1 quadrature count mode. 1 count per quadrature cycle		*/
	enQeiLS7366QuadratureMode2x,				/**< x2 quadrature count mode. 2 counts per quadrature cycle	*/
	enQeiLS7366QuadratureMode4x,				/**< x4 quadrature count mode. 4 counts per quadrature cycle	*/
}PFEnQeiLS7366QuadratureMode;

/** Enumeration for count mode which defines range limit for count 			*/
typedef enum
{
	enQeiLS7366CountLimitFreeRunning = 0x00,	/**< Free running count mode			*/
	enQeiLS7366CountLimitSingleCycle = 0x04,	/**< Single cycle count mode. Counter disabled with carry or borrow	*/
	enQeiLS7366CountLimitRangeLimit = 0x08,		/**< Range count mode. Count range is limited between 0 and DTR	*/
	enQeiLS7366CountLimitModuloN = 0x0C,		/**< Module-n count mode. Input count clock freq is divided by (n+1) where n = DTR */
}PFEnQeiLS7366CountLimit;

/** Enumeration for index pin mode which defines behaviour for index pin	 */
typedef enum
{
	enQeiLS7366IndexModeDisable = 0x00,
	enQeiLS7366IndexModeLoadCNTR = 0x10,
	enQeiLS7366IndexModeRstCNTR = 0x20,
	enQeiLS7366IndexModeLoadOTR = 0x30,
}PFEnQeiLS7366IndexMode;

/** Enumeration for fileter clock division factor		*/
typedef enum
{
	enQeiLS7366FilterClockDiv_1 = 0x00,
	enQeiLS7366FilterClockDiv_2 = 0x80,
}PFEnQeiLS7366FilterClockDiv;

/** Enumeration for counter register byte size		*/
typedef enum
{
	enQeiLS7366CounterByteSize_1 = 0x01,
	enQeiLS7366CounterByteSize_2 = 0x02,
	enQeiLS7366CounterByteSize_3 = 0x03,
	enQeiLS7366CounterByteSize_4 = 0x04
}PFEnQeiLS7366CounterByteSize;


/** Configuration structure for LS7366Left QEI chip		*/
typedef struct
{
	PFGpioPortPin lFlagGpio;						/**< Assign gpio pin for LFLAG input					*/
	PFGpioPortPin dFlagGpio;						/**< Assign gpio pin for DFLAG input					*/
	PFGpioPortPin countEnGpio;						/**< Assign gpio pin for count enable output			*/
	PFGpioPortPin spiCsGpio;						/**< Assign gpio pin for spi chip select				*/
	PFbyte spiCh;									/**< Assign spi channel for communication				*/
	PFEnQeiLS7366QuadratureMode quadratureMode;		/**< Quadrature mode for LS7366Left							*/
	PFEnQeiLS7366CountLimit countLimit;				/**< Set counting limit mode for LS7366Left					*/
	PFEnQeiLS7366IndexMode indexMode;				/**< Set behaviour for index pin						*/
	PFEnBoolean syncIndex;							/**< Set mode for index pin synchronous or asynchronous	*/
	PFEnQeiLS7366FilterClockDiv filterClockDiv;		/**< Filter clock division factor						*/
	PFEnQeiLS7366CounterByteSize counterSize;		/**< COunter register size in bytes						*/
	PFEnBoolean FlagOnIndex;						/**< Enable flag on index latch							*/
	PFEnBoolean FlagOnCompare;						/**< Enable flag on DTR and CNTR comparison match		*/
	PFEnBoolean FlagOnBorrow;						/**< Enable flag on counter underflow					*/
	PFEnBoolean FlagOnCarry;						/**< Enable flag on counter overflow					*/
	void (*pfSpiClose)(void);
	PFEnStatus (*spiRegisterDevice)(PFbyte* id, PFGpioPortPin chipSelect);
	PFEnStatus (*spiUnregisterDevice)(PFbyte* id);
	PFEnStatus (*spiChipSelect)(PFbyte* id, PFbyte pinStatus);
	PFword (*spiExchangeByte)(PFbyte *id, PFword data);
	PFEnStatus (*spiWrite)(PFbyte* id, PFbyte* data, PFdword size);
	PFEnStatus (*spiRead)(PFbyte* id, PFbyte* data, PFdword size, PFdword* readBytes);
}PFCfgQeiLS7366;

/** Pointer to PFCfgQeiLS7366 structure	*/
typedef PFCfgQeiLS7366* PPFCfgQeiLS7366;

/**
 * Initializes the LS7366Left qei chip with given settings
 *
 * \param config Configuration structure to initialize L7266 qei chip.
 * 
 * \return Status for initialization.
 */
PFEnStatus pfQeiLS7366Open(PFbyte *id, PPFCfgQeiLS7366 config,PFbyte count);

/**
 * Deinitializes LS7366Left chip and releases the resources subsribed.
 */
PFEnStatus pfQeiLS7366Close(PFbyte id);

/**
 * Enables or disables LS7366Left counting.
 *
 * \param status Set status \a enBooleanTrue to enable the counting and \a enBooleanFlase to disable the counting.
 *
 * \return Status for setting the count state.
 */
PFEnStatus pfQeiLS7366CountEnable(PFbyte id,PFEnBoolean status);

/**
 * Reads the qei counter value from LS7366Left chip.
 *
 * \param count Pointer to load the counter value.
 *
 * \return Counter reading status.
 */
PFEnStatus pfQeiLS7366ReadCount(PFbyte id,PFdword* count);

/**
 * Resets the qei counter value to 0.
 *
 * \return Counter reset status.
 */
PFEnStatus pfQeiLS7366ResetCount(PFbyte id);

/**
 * Reads the data register value from LS7366Left chip.
 *
 * \param data Pointer to load the data register value.
 *
 * \return Data register reading status.
 */
PFEnStatus pfQeiLS7366ReadDataReg(PFbyte id,PFdword* data);

/**
 * Writes the data register with given \a value.
 *
 * \param data Value to write to data register.
 *
 * \return Data register writing status.
 */
PFEnStatus pfQeiLS7366WriteDataReg(PFbyte id,PFdword data);

/**
 * Reads the status register value from LS7366Left chip.
 *
 * \param status Pointer to load the status value.
 *
 * \return Status reading status.
 */
PFEnStatus pfQeiLS7366ReadDataReg(PFbyte id,PFdword* status);

 
/** @} */ 

