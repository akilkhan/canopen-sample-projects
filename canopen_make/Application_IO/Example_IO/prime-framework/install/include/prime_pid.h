#pragma once

/** Maximum number of PID to be used*/
#define MAX_PID_NO 			2

/** Defines the data-type of  PID */
#define PF_PID_PARAM_IS_DWORD	1

/**  Defines the type of calculation of PID to be integer or fixed math */

#define PF_USE_FIXEDMATH_CAL		0

#if(PF_USE_FIXEDMATH_CAL == 0)

#if(PF_PID_PARAM_IS_FLOAT == 1)
typedef PFfloat_t pfPidData_t;
typedef PFfloat_t pfPidConstant_t;

#elif(PF_PID_PARAM_IS_BYTE == 1)
typedef PFbyte pfPidData_t;
typedef PFbyte pfPidConstant_t;

#elif(PF_PID_PARAM_IS_WORD == 1)
typedef PFword pfPidData_t;
typedef PFword pfPidConstant_t;

#elif (PF_PID_PARAM_IS_DWORD == 1)
typedef PFsdword pfPidData_t;
typedef PFsdword pfPidConstant_t;
#endif


#else

typedef pfFixedDataType pfPidData_t;
typedef pfFixedDataType pfPidConstant_t;

#endif




/** Defines whether to use PID control or PI control */
#define PF_USE_PID						1

/** Weight constant of filter to be given in 
 * numerator and denominator form for fraction to gain accuracy
 * in case of integer calculation */
#if(PF_USE_FIXEDMATH_CAL == 0)

/** Weight constant of filter for process variable  */
#define K_PV_NUM					2
#define K_PV_DENOM					10

/** Weight constant of filter for control output */
#define K_CO_NUM					1
#define K_CO_DENOM					1



/** Weight constant of filter to be given in 
 * form ((intpart + (fractional val / divpart( for fractional precision))
 * e.g. 10.5 = 10 + 5/10 intpart  = 10 ,fractPart = 5 ,divpart = 10
 * 10.05 = 10 + 5/100 intpart  = 10 ,fractPart = 5 ,divpart = 100
 * in case of fixed math calculation */
 
#elif(PF_USE_FIXEDMATH_CAL == 1)

#undef K_PV_NUM					
#undef K_PV_DENOM					
#undef K_CO_NUM					
#undef K_CO_DENOM					
#undef __PF_PID_PARAM_IS_DWORD__ 

#define K_CO_INTPART_VAL					1
#define K_CO_FRACTPART_VAL					0
#define K_CO_FRACTION_PRECISION				10

#define K_PV_INTPART_VAL					0
#define K_PV_FRACTPART_VAL					2
#define K_PV_FRACTION_PRECISION				10


#endif


/** Structure for storing the errors of PID */
typedef struct 
{
		int64_t errEachLoop; 	/**< Error value in each PID loop */
		int64_t errAccumalated; /**< Accumulated Error value in PID loop over the time */
}pfPidErrors;

typedef int pidLoopTime_t;

/** Configuration structure of only PID parameters */

#if(PF_USE_FIXEDMATH_CAL == 0)

typedef struct
{
	pfPidConstant_t kpNum; 				/**< defines the proportional constant Numerator */
	pfPidConstant_t kpDenom; 			/**< defines the proportional constant Denominator */
	pfPidConstant_t tiNum; 				/**< defines the integral Time constant Numerator */
	pfPidConstant_t tiDenom; 			/**< defines the integral Time constant Denominator */
	pfPidConstant_t tdNum; 				/**< defines the differential constant Numerator */
	pfPidConstant_t tdDenom; 			/**< defines the differential constant Denominator */
	pfPidData_t maxPidOutputNum; 		/**<  maximum limit for the PID output Numerator */
	pfPidData_t maxPidOutputDenom; 		/**<  maximum limit for the PID output	Denominator */
	pfPidData_t minPidOutputNum; 		/**<  minimum limit for the PID output(threshold) */
	pfPidData_t minPidOutputDenom; 		/**<  minimum limit for the PID output(threshold) */
	pfPidData_t outputSpan;				/**<  Span of effective output */
	pfPidData_t maxPidInputNum; 		/**<  maximum limit for the PID control input per pid loop */
	pfPidData_t maxPidInputDenom; 		/**<  maximum limit for the PID control input per pid loop */    
	pfPidData_t minPidInputNum; 		/**<  minimum limit for the PID control input per pid loop */	
	pfPidData_t minPidInputDenom; 		/**<  minimum limit for the PID control input per pid loop */      
	pfPidData_t inputSpan;				/**<  Span of effective input */
		
} PFCfgPid; /** Pid control configuration */

typedef PFCfgPid* PFpCfgPid;

/** Structure to save the  context of PID  */
typedef struct
{
	PFpCfgPid  configuration; 	/**< pointer to static structure of PID */
	pfPidData_t setPoint;			 /**< set point of PID */
	pfPidData_t accumulatedErrorNum; /**< Accumulated error numerator*/ 
	pfPidData_t accumulatedErrorDenom; /**< Accumulated error denominator*/ 
	pfPidData_t lastErrorInputNum; /**< Error numerator value in the previous PID loop  */
	pfPidData_t lastErrorInputDenom; /**< Error denominator value in the previous PID loop  */
	pfPidData_t lastFilteredCO; /** Control Output value of last PID loop */	
} PFPidContext;	

/** 
 * * Update the control output according to current value of PID input
 * \param id Id of the PID whose output is required
 * \param controlInput control Input value of PID 
 * \param errPrint Pointer to PID error structure
 * \return return PID control output 
 *
 */
 
pfPidData_t pfPidUpdate(PFbyte id, pfPidData_t controlInput,pfPidErrors *errPrint);

	
#elif(PF_USE_FIXEDMATH_CAL == 1)
	
typedef struct
{
	pfPidConstant_t kp; 			/**< defines the proportional constant */
	pfPidConstant_t ti; 			/**< defines the integral Time constant  */
	pfPidConstant_t td; 			/**< defines the differential constant  */
	pfPidData_t maxPidOutput; 		/**<  maximum limit for the PID output */
	pfPidData_t minPidOutput; 		/**<  minimum limit for the PID output(threshold) */
	pfPidData_t outputSpan;			/**<  Span of effective output */
	pfPidData_t maxPidInput; 		/**<  maximum limit for the PID error input per pid loop  */     
	pfPidData_t minPidInput; 		/**<  minimum limit for the PID error input per pid loop */      
	pfPidData_t inputSpan;			/**<  Span of effective input */
		
} PFCfgPid; /** < Pid control configuration */

typedef struct
{
	PFpCfgPid  configuration; /**< pointer to static structure of PID */
	pfPidData_t setPoint;	/**< set point of PID */
	pfPidData_t lastErrorInput; /**< Error value in the previous PID loop  */
	pfPidData_t accumulatedError; /**< Accumulated error */ 
	pfPidData_t lastControlOut;	/** Control Output value of last PID loop */	
} PFPidContext; /** < Pid context ...*/


/** 
 * * Update the control output according to current value of PID input
 * \param id Id of the PID whose output is required
 * \param currentPidInput PID input value
 * \param errPrint Pointer to PID error structure
 * \return return PID control output 
 *
 */
int64_t pfPidUpdate(PFbyte id, int64_t controlInput,pfPidErrors *errPrint);
	
#endif

/** 
 * * Initialize the PID with given PID parameters
 * 
 * \param idArray pointer to array of id of PID created
 * \param config PID configuration structure
 * \param count Number of PID to be created
 * 
 * \return PID initialization status

*/
PFEnStatus pfPidOpen(PFbyte* idArray,const PFpCfgPid  config, PFbyte count);

/** 
 * * Reset the PID with given PID parameters
 * 
 * \param id id of PID that to be reset;
 * 
 * \return PID initialization status

*/

PFEnStatus pfPidControlReset(PFbyte id);

/** 
 * * Reset the PID with given PID parameters
 * 
 * \param id id of PID that to be reset;
 * \param setPoint Pointer to the value to be set as setpoint.
 * 
 * \return return Setpoint set status

*/
PFEnStatus pfPidChangeSetPoint(PFbyte id, pfPidData_t* setPoint);


