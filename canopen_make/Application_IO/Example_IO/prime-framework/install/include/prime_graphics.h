/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Graphics Display Driver
 *
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_GRAPHICS_DRIVER_API for graphic driver
 * @{
 */
/** Enumeration for the font type		*/
typedef enum{
	enFont_8X8=0,	/**< FONT SIZE 8x8			*/
	enFont_8X16,	/**< FONT SIZE 8x16			*/
	enFont_24X16,   /**< FONT SIZE 24x16		*/
	enFont_16X16,   /**< FONT SIZE 16x16		*/
	enFont_32X32	/**< FONT SIZE 32x32		*/ 
}PFEnGraphicFonts;

/**	Enumeration for the Quadrant of the graphic Driver 		*/
typedef enum 
{
	enQuadrant_I=0,        	/**< Quadrant I			*/
	enQuadrant_II,    	 	/**< Quadrant II		*/
	enQuadrant_III, 		/**< Quadrant III		*/
	enQuadrant_IV			/**< Quadrant IV		*/
}PFEnQuadrant;

/** Enumeration for the drawing style of graphic driver		*/
typedef enum 
{
	enLineStyleSolid=0,    /**<   Solid drawing style  	*/
	enLineStyleDotted,	  /**<	 Dotted drawing style	*/
	enLineStyleDashDot	  /**<  Dash drawing style    	*/
}PFEnLineStyle;

/**
 * To set the Pen Size of the graphic display driver	
 *
 * \param Size of the pen for graphic display driver
 *
 * \return status of whether pen size is set or not
 * 
 */

PFEnStatus pfGfxSetPenSizes(const PFdword size);
/**

 * To get the pen size from the graphic display driver
 *
 * \param Variable address to store the value at that address
 *
 * \return status of whether user got the pen
 * 
 */
PFEnStatus pfGfxGetPenSize(PFword *data);
/**

 * To set the color in graphic display driver
 *
 * \param Color to be set in the display driver
 *
 * \return status of whether is color is set or not
 * 
 */
PFEnStatus pfGfxSetColor(const PFdword color);
/**
 * To get the color from graphic display driver
 *
 * \param Variable to store the width of the LCD Display
 *
 * \return status of whether user got the color from graphic display driver
 * 
 */
PFEnStatus pfGfxGetColor( PFword *data);
/**

 * To set the pixel on the LCD Display
 *
 * \param X-Cordinate of the pixel to be set
 *
 * \param Y-Cordinate of the pixel to be set
 *
 * \param Size of the pixel 
 *
 * \param Color of the pixel 
 *
 * \return status of whether pixel is set or not
 * 
 */

PFEnStatus pfGfxPixels(const PFdword xPos, const PFdword yPos, const PFdword size, PFword color);
/**

 * To Draw the pixel on the LCD Display
 *
 * \param X-Cordinate of the pixel to be set
 *
 * \param Y-Cordinate of the pixel to be set
 *
 * \return status of whether is pixel is drawn or not
 * 
 */
PFEnStatus pfGfxDrawPixels(const PFdword xPos, const PFdword yPos);
/**

 * To Draw rectangle on the LCD Display
  *
 * \param X-Cordinate of the first vertex 
 *
 * \param Y-Cordinate of the first vertex
 *
 * \param X-Cordinate of the second vertex 
 *
 * \param Y-Cordinate of the second vertex
 *
 * \return status of whether rectangle is drawn or not
 * 
 */
PFEnStatus pfGfxDrawRectangle(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2);
/**

 * To Draw solid circle on the LCD Display
 *
 * \param X and Y Coordinate of the center of the circle
 *
 * \param Radius of the circle
 *
 * \param  Color of the solid circle
 *
 * \return status of whether solid circle is drawn or not
 * 
 */
PFEnStatus pfGfxDrawSolidCircle(const PFsdword xc, const PFsdword yc, PFsdword radius, const PFsdword color);
/**

 * To Draw solid rectangle  on the LCD Display
 *
 * \param X and Y Coordinate of the first vertex 
 *
 * \param X and Y Coordinate of the second vertex
 *
 * \param Color of the solid circle
 *
 * \return status of whether solid rectangle is drawn or not
 * 
 */
PFEnStatus pfGfxDrawSolidRectangle(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2, const PFdword color);
/**

 * To Draw solid round rectangle on the LCD Display
 *
 * \param X and Y Coordinate of the first vertex 
 *
 * \param X and Y Coordinate of the second vertex
 *
 * \param Radius of the edge of the round rectangle 
 *
 * \param Color of the solid round rectangle 
 *
 * \return status of whether solid round rectangle is drawn or not
 * 
 */
PFEnStatus pfGfxDrawSolidRoundRectangle(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2, PFdword radius, const PFdword color);
/**

 * To Draw Bresenham line on the LCD Display
 *
 * \param X and Y Coordinate of the one end of the line
 *  
 * \param X and Y Coordinate of the other end of the line
 *
 * \return status of whether bresenham line is drawn or not
 * 
 */
PFEnStatus pfGfxDrawLine( PFsdword x1, PFsdword y1, const PFsdword x2, const PFsdword y2);
/**

 * To Draw Bresenham circle on the LCD Display
 *
 * \param X and Y Coordinate of the center of the circle
 *
 * \param Radius of the quarter circle
 *
 * \return status of whether quarter circle is drawn or not
 * 
 */
PFEnStatus pfGfxDrawCircle(const PFsdword xc, const PFsdword yc, PFsdword radius);
/**

 * To Draw quarter circle on the LCD Display
 *
 * \param X and Y Coordinate of the center of the circle
 *
 * \param Radius of the quarter circle
 *
 * \param Quadrant of the quarter circle
 *
 * \return status of whether quarter circle is drawn or not
 * 
 */
 PFEnStatus pfGfxDrawQCircle(const PFsdword xc, const PFsdword yc, PFsdword radius, PFEnQuadrant quad);
/**

 * To Draw solid quarter circle on the LCD Display
 *
 * \param X and Y Coordinate of the center of the circle
 *
 * \param Radius of the quarter circle
 *
 * \param Quadrant of the quarter circle
 *
 * \param Color of the solid quarter circle
 *
 * \return status of whether solid quarter circle is drawn or not
 * 
 */
 PFEnStatus pfGfxDrawSolidQuarterCircle(const PFsdword xc, const PFsdword yc, PFsdword radius, const PFEnQuadrant quadrant, const PFsdword color);
/**

 * To Draw polygon on the LCD Display
 *
 * \param Array of the X Coordinate of the polygon
 *
 * \param Array of the Y Coordinate of the polygon	
 *
 * \param Number of the sides in the polygon 
 *
 * \return status of whether polygon is drawn or not
 * 
 */
PFEnStatus pfGfxDrawPolygon(const PFdword *xArray, const PFdword *yArray, const PFdword num);
/**

 * To Draw solid ellipse on the LCD Display
 *
 * \param X and Y Coordinate of the ellipse
 *
 * \param  Constants of the solid ellipse (a and b)
 *
 * \param color of the solid ellipse
 *
 * \return status of whether solid ellipse is drawn or not
 * 
 */
PFEnStatus pfGfxDrawSolidEllipse(const PFsdword xc, const PFsdword yc, const PFsdword a, const PFsdword b, const PFsdword color);
/**

 * To Draw circular bresenham lines on the LCD Display
 *
 * \param X and Y Coordinate of the bresenham circular lines
 *
 * \param Radius of the bresenham circular lines
 *
 * \param Numbers of the lines to be drawn
 *
 * \return status of whether circular bresenham lines are drawn or not
 * 
 */
PFEnStatus pfGfxDrawCircularLines(const PFdword xc, const PFdword yc,const PFdword radius, const PFdword num);
/**

 * To Draw character on the LCD display
 *
 * \param X and Y Coordinate of the character 
 * 
 * \param Font Type of the character
 *
 * \param Font Color of the character
 *
 * \param Background color of the LCD display 
 *
 * \return status of whether char is written or not
 * 
 */
PFEnStatus pfGfxDrawChar(PFdword x, PFdword y, PFchar character, PFbyte fontType, PFdword fontColor,PFdword backColor);
/**

 * To write string on the LCD display
 *
 * \param X and Y Coordinate of the string to be written to the LCD Display
 *
 * \param String to be written on the LCD display
 *
 * \param Font Type of the string to be written
 *
 * \param Font Color of the string to be written
 *
 * \param Background color of the LCD display 
 *
 * \return status of whether string is written or not
 * 
 */
PFEnStatus pfGfxDrawString(PFdword x, PFdword y, PFchar *string,PFbyte fontType, PFdword fontColor, PFdword backColor);
/**

 * To Draw the arc on the LCD Display
 *
 * \param X and Y Coordinate of the center of the arc to be drawn on LCD display
 *
 * \param Radius of the arc to be drawn on the LCD display
 *
 * \param Start angle of the arc
 *
 * \param Extent of the arc
 *
 * \return status of whether arc is drawn or not
 * 
 */
PFEnStatus pfGfxDrawArc(const PFsdword xc, const PFsdword yc, const PFsdword radius, PFsdword startAngle, PFsdword extent);

/**

 * To Draw styled bresenham line on the LCD Display
 *
 * \param  X and Y Coordinate of the one end of the bresenham line
 *
 * \param X and Y Coordinate of the other end of the bresenham line
 *
 * \param Drawing style of the line
 *
 * \param Pen size of the line	
 *
 * \param Color of the line to be drawn 
 *
 * \return status of whether bresenham styled line is drawn or not
 * 
 */
PFEnStatus pfGfxDrawStyledLine(PFsdword x1, PFsdword y1, const PFsdword x2, const PFsdword y2, const PFEnLineStyle style, PFchar penSize, const PFsdword color);
/**

 * To draw fine quadratic bezier bresenham line 
 *
 * \param X and Y Coordinates of the line
 *
 * \return status of whether fine quadratic bezier bresenham line is drawn or not
 * 
 */
PFEnStatus pfGfxDrawFineQuadBezier(PFsdword x1, PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword x3, const PFsdword y3);
/**

 * To Draw Cuboid on the LCD Display
 *
 * \param X and Y Coordinate of the one vertex of the first rectangle
 *
 * \param X and Y Coordinate of the other vertex of the first rectangle
 *
 * \param X and Y Coordinate of the one vertex of the second rectangle
 *
 * \param X and Y Coordinate of the other vertex of the second rectangle
 *
 * \return status of whether cuboid is drawn or not
 * 
 */
PFEnStatus pfGfxDrawCuboid(PFdword x11, PFdword y11, PFdword x12, PFdword y12, PFdword x21, PFdword y21, PFdword x22, PFdword y22);
/**

 * To Draw parallelogram of the LCD Display
 *
 * \param X and Y Coordinate of the one of the parallel line
 *
 * \param X and Y Coordinate of the one of the other parallel line 
 *
 * \param Length of the parallel line
 *
 * \return status of whether parallelogram is drawn or not
 * 
 */
PFEnStatus pfGfxDrawParallelogram(PFdword x1, PFdword y1, PFdword x2, PFdword y2, PFdword len);
/**

 * To Draw parallelepiped on the LCD Display
 *
 * \param X and Y Coordinate of the one of the parallel line of first parallelogram
 *
 * \param X and Y Coordinate of the one of the other parallel line of first parallelogram
 *
 * \param Length of the parallel lines of first parallelogram
 *
 * \param X and Y Coordinate of the one of the parallel line of second parallelogram
 *
 * \param X and Y Coordinate of the one of the other parallel line of second parallelogram
 *
 * \param Length of the parallel lines of second parallelogram
 *
 * \return status of whether  parallellepiped  is drawns or not
 * 
 */
PFEnStatus pfGfxDrawParallelepiped(PFdword x11,PFdword  y11,PFdword  x12,PFdword  y12,PFbyte l1,PFdword  x21,PFdword  y21,PFdword  x22,PFdword y22,PFbyte l2);
/**

 * To Draw filled Lines on the LCD Display
 *
 * \param X and Y Coordinate of Center
 *
 * \param X and Y Coordinate of One end of the line	
 *
 * \param X and Y Coordinate of other end of the line	
 *
 * \param Y Reference point of the line 
 *
 * \return status of whether triangle is  drawn or not
 * 
 */
PFEnStatus pfGfxDrawFilledLines(const PFsdword xc, const PFsdword yc, const PFsdword x1, const PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword yRef);
/**

 * To Draw filled triangle on the LCD Display
 *
 * \param X and Y Coordinate of each vertex
 *
 * \return status of whether triangle is  drawn or not
 * 
 */
PFEnStatus pfGfxDrawFilledTriangle(const PFsdword x1, const PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword x3, const PFsdword y3);
/**

 * To Draw filled Polygon on the LCD Display
 *
 * \param X and Y Coordinate array each vertex of polygon
 *
 * \param Number of sides in the polygon
 *
 * \return status of whether Polygon is  drawn or not
 * 
 */
PFEnStatus pfGfxDrawFilledPolygon(const PFsdword *xArray, const PFsdword *yArray, const PFsdword num);
/**

 * To Draw Plot circle on the LCD Display
 *
 * \param X and Y Coordinate of center
 *
 * \param radius of the center
 *
 * \param background color of the circle
 *
 * \param front coloe of the circle
 *
 * \return status of whether Circle is  drawn or not
 * 
 */
PFEnStatus pfGfxPlotCircle(PFdword xm, PFdword ym, PFdword r,PFdword bColor, PFdword fColor);
/**

 * To set LCD Debug Print Configuration  on the LCD Display
 *
 * \param Font of the character
 *
 * \param Font Color
 *
 * \param Font Background color
 *
 * \return status of whether Configuration is set or not
 * 
 */
PFEnStatus pfSetLcdDebugPrintConfig(PFEnGraphicFonts dFont, PFdword dFontClr, PFdword dFontBackClr);
/**

 * To Debug a character on the LCD Display
 *
 * \param character to be debugged
 *
 * \return status of whether character is debugged or not
 * 
 */
PFEnStatus pfGfxDrawDebugChar(PFbyte data);
/**

 * To Debug a string on the LCD Display
 *
 * \param string to be debugged
 *
 * \return status of whether string is debugged or not
 * 
 */
PFEnStatus pfLcdDebugString(PFbyte* str);
/**

 * To scroll up the debug line on the LCD Display
 *
 * \param Number of lines
 *
 * \return status of whether  scrolling is done or not
 * 
 */
PFEnStatus pfLcdDebugScrollUp(PFword lines);
/**

 * To scroll down the debug line on the LCD Display
 *
 * \param Number of lines
 *
 * \return status of whether  scrolling is done or not
 * 
 */
PFEnStatus pfLcdDebugScrollDown(PFword lines);
