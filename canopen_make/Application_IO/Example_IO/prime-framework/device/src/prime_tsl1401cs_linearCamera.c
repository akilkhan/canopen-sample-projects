#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_gpio.h"
#include "prime_adc.h"
#include "prime_tsl1401cs_linearCamera.h"

static PFCfgTsl1401LinearCamera tsl1401ConfigStruct[PF_MAX_TSL1401CS_SUPPORTED];    /* structure to copy configuration settings */
PFbyte tsl1401csInitFlag[PF_MAX_TSL1401CS_SUPPORTED]={enBooleanFalse};              /* flag to test for sensor initialization */
PFbyte linearCameraCh;

void pfLinCamDelay_us(PFdword val)
{
	PFdword loop1,loop2;
	
	for(loop1=0;loop1 < val;loop1++ )
	for(loop2 =0;loop2< 23;loop2++ );
}

PFEnStatus pfTsl1401LinearCameraOpen(PFbyte *id , PFpCfgTsl1401LinearCamera sensorConfig , PFbyte cnt)
{
  PFbyte loop,i;
	  for(i=0; i < cnt; i++)
	{
		for(loop=0; loop < PF_MAX_TSL1401CS_SUPPORTED; loop++)
		{
			if(tsl1401csInitFlag[loop] == enBooleanFalse)
			{
				tsl1401csInitFlag[loop] = enBooleanTrue;
				pfMemCopy(&tsl1401ConfigStruct[loop], &sensorConfig[i], sizeof(PFCfgTsl1401LinearCamera));
				id[i]= loop;
				if(i== PF_MAX_TSL1401CS_SUPPORTED)
				{
					return enStatusError;
				}
				else
				{
					break;
				}
			}
		}
	}
  return enStatusSuccess;
}

PFEnStatus pfTsl1401LinearCameraClose(PFbyte id)
{
	 // tsl1401csInitFlag=0;
	if(tsl1401csInitFlag[id] == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
	else
	{	
		return enStatusSuccess;
	}
}

PFEnStatus pfTsl1401LinearCameraGetFrame(PFbyte id,PFdword frame[], PFword frameIntTime)
{
    PFbyte j=0;
    PFword delay=0;
    PFEnStatus status;

    if(tsl1401csInitFlag[id] == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
/* flush previous frame data to initiate fresh data integration */
    status = pfTsl1401LinearCameraFlush(id);
    if(status!=enStatusSuccess)
	{
		return status;
	}


/* allow delay to complete data integration */
    if(frameIntTime>10)
    {
        delay = frameIntTime -10;

        while(delay>25)
        {
            pfLinCamDelay_us(25000);
            delay = delay -25;
        }
		delay = delay * 1000;
        pfLinCamDelay_us(delay);
    }

/* issue Serial Input pulse signal */
    pfGpioPinsSet(tsl1401ConfigStruct[id].siPort.port, tsl1401ConfigStruct[id].siPort.pin);
    pfLinCamDelay_us(5);

    pfGpioPinsSet(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
    pfLinCamDelay_us(5);

    pfGpioPinsClear(tsl1401ConfigStruct[id].siPort.port, tsl1401ConfigStruct[id].siPort.pin);

    pfLinCamDelay_us(5);
    pfGpioPinsClear(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);

    
	status = pfAdcGetVoltageSingleConversion(tsl1401ConfigStruct[id].adcChannel, &frame[0]);
    if(status!=enStatusSuccess)
    return status;

    for(j=1;j<128;++j)
    {
        pfGpioPinsSet(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
        pfLinCamDelay_us(5);
        pfLinCamDelay_us(5);
        pfGpioPinsClear(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
		status = pfAdcGetVoltageSingleConversion(tsl1401ConfigStruct[id].adcChannel, &frame[j]);
		if(status!=enStatusSuccess)
		{
            return status;
		}
    }

    pfLinCamDelay_us(5);
    pfLinCamDelay_us(5);

    pfGpioPinsSet(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
    pfLinCamDelay_us(5);
    pfLinCamDelay_us(5);
    pfGpioPinsClear(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);

    pfLinCamDelay_us(5);
    pfLinCamDelay_us(5);

    pfGpioPinsSet(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
    pfLinCamDelay_us(5);
    pfLinCamDelay_us(5);
    pfGpioPinsClear(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);

    return enStatusSuccess;
}

PFEnStatus pfTsl1401LinearCameraFlush(PFbyte id)
{
    PFbyte j=0;

    if(tsl1401csInitFlag[id] == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}

    pfGpioPinsSet(tsl1401ConfigStruct[id].siPort.port, tsl1401ConfigStruct[id].siPort.pin);
    pfLinCamDelay_us(5);

    pfGpioPinsSet(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
    pfLinCamDelay_us(5);

    pfGpioPinsClear(tsl1401ConfigStruct[id].siPort.port, tsl1401ConfigStruct[id].siPort.pin);

    pfLinCamDelay_us(5);
    pfGpioPinsClear(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
    pfLinCamDelay_us(5);
    pfLinCamDelay_us(5);

    for(j=1;j<130;++j)
    {
        pfGpioPinsSet(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
        pfLinCamDelay_us(5);
        pfLinCamDelay_us(5);
        pfGpioPinsClear(tsl1401ConfigStruct[id].clkPort.port, tsl1401ConfigStruct[id].clkPort.pin);
        pfLinCamDelay_us(5);
        pfLinCamDelay_us(5);
    }

    return enStatusSuccess;
}

