#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_gpio.h"
#include "prime_tcs230coloursensor.h"

/* distance of colour sensor from colour sheet is kept constant 5.5cm **/
static PFCfgTCS230colourSensor tcs230colourSensorConfigStruct[PF_MAX_TCS230_SUPPORTED];/* structure to copy configuration settings */
static PFEnBoolean tcs230colourSensorInitFlag[PF_MAX_TCS230_SUPPORTED]={enBooleanFalse};          	/* flag to check initialization */
static PFword  tcs230colourSensorBlackBalance[PF_MAX_TCS230_SUPPORTED][4]={{310,310,330,870}};    	/* array holds value for black colour */
static PFword  tcs230colourSensorWhiteBalance[PF_MAX_TCS230_SUPPORTED][4]={{950, 910, 1070, 2710}}; /* array holds value for white colour */

/*
 * to set colour(red, green, blue, no-colour) filter for frequency measurement
 *
 * \param filter colour filter to be set
 */
static PFEnStatus pfTCS230colourSensorSetFilter(PFbyte id, PFEnTCS230filterSelect filter);

/*
 * to get colour(red, green, blue, no-colour) filter for frequency measurement
 *
 * \param filter colour filter to be set
 */
static PFEnStatus pfTCS230colourSensorGetFrequency(PFbyte id ,PFEnTCS230filterSelect filter, PFword* frequency);
PFEnStatus pfTCS230colourSensorOpen(PFbyte *id,PFpCfgTCS230colourSensor sensorConfig,PFbyte cnt)
{
	PFbyte loop,i;
	for(i=0 ; i < cnt ; i++ )
	{
		for(loop=0; loop < PF_MAX_TCS230_SUPPORTED; loop++ )
		{
			 if(tcs230colourSensorInitFlag[loop]== enBooleanFalse)
			 {
				  tcs230colourSensorInitFlag[loop]= enBooleanTrue;
						 switch(sensorConfig[i].scalingFreq)
						 {
								 case enTCS230scalingFrequency_2:   pfGpioPinsClear(sensorConfig[i].s0_port.port, sensorConfig[i].s0_port.pin);
																										pfGpioPinsSet(sensorConfig[i].s1_port.port, sensorConfig[i].s1_port.pin);
																										break;

								case enTCS230scalingFrequency_20:   pfGpioPinsSet(sensorConfig[i].s0_port.port, sensorConfig[i].s0_port.pin);
																										pfGpioPinsClear(sensorConfig[i].s1_port.port, sensorConfig[i].s1_port.pin);
																										break;

								case enTCS230scalingFrequency_100:  pfGpioPinsSet(sensorConfig[i].s0_port.port, sensorConfig[i].s0_port.pin);
																										pfGpioPinsSet(sensorConfig[i].s1_port.port, sensorConfig[i].s1_port.pin);
																										break;

								default:                            return enStatusInvArgs;
							 
						 }
						pfMemCopy(&tcs230colourSensorConfigStruct[loop], &sensorConfig[i], sizeof(PFCfgTCS230colourSensor));
				     id[i]= loop;
						 if(i== PF_MAX_TCS230_SUPPORTED)
			        return enStatusError;
						 else
							 break;
			 }
			
		}
		
		 
	}
 return enStatusSuccess;
}

PFEnStatus pfTCS230colourSensorClose(PFbyte id)
{
	#if (PF_TCS230COLOURSENSOR_DEBUG == 1)
	if(id>=PF_MAX_TCS230_SUPPORTED)
		return enStatusInvArgs;
	if(tcs230colourSensorInitFlag[i]==enBooleanFalse)
		return enStatusNotConfigured;
	#endif
    if( tcs230colourSensorInitFlag[id]==enBooleanFalse)
  	{
			return enStatusNotConfigured;
		}
    /** power down mode */
    pfGpioPinsClear(tcs230colourSensorConfigStruct[id].s0_port.port, tcs230colourSensorConfigStruct[id].s0_port.pin);
    pfGpioPinsClear(tcs230colourSensorConfigStruct[id].s1_port.port, tcs230colourSensorConfigStruct[id].s1_port.pin);

    return enStatusSuccess;
}

PFEnStatus pfTCS230colourSensorBlackBalance(PFbyte id)
{
    volatile PFEnStatus status;
	 #if (PF_TCS230COLOURSENSOR_DEBUG == 1)
	 if(id>=TCS230_MAX_DEVICE)
		return enStatusInvArgs;
	 if(tcs230colourSensorInitFlag[i]==enBooleanFalse)
		return enStatusNotConfigured;
	 #endif
    if( tcs230colourSensorInitFlag[id] == enBooleanFalse)
    return enStatusNotConfigured;
    
    status=pfTCS230colourSensorGetFrequency(id, enTCS230filter_Red, &tcs230colourSensorBlackBalance[id][0]);
    if(status != enStatusSuccess)
    return status;

    status=pfTCS230colourSensorGetFrequency(id,enTCS230filter_Green, &tcs230colourSensorBlackBalance[id][1]);
    if(status != enStatusSuccess)
    return status;

    status=pfTCS230colourSensorGetFrequency(id,enTCS230filter_Blue, &tcs230colourSensorBlackBalance[id][2]);
    if(status != enStatusSuccess)
    return status;

    status=pfTCS230colourSensorGetFrequency(id ,enTCS230filter_None, &tcs230colourSensorBlackBalance[id][3]);
    if(status != enStatusSuccess)
    return status;

    return enStatusSuccess;
}

PFEnStatus pfTCS230colourSensorWhiteBalance(PFbyte id)
{
    volatile PFEnStatus status;
	  #if (PF_TCS230COLOURSENSOR_DEBUG == 1)
	  if(id>=TCS230_MAX_DEVICE)
		return enStatusInvArgs;
		if(tcs230colourSensorInitFlag[i]==enBooleanFalse)
		return enStatusNotConfigured;
		#endif
    if( tcs230colourSensorInitFlag[id] == enBooleanFalse )
    return enStatusNotConfigured;

    status=pfTCS230colourSensorGetFrequency(id, enTCS230filter_Red, &tcs230colourSensorWhiteBalance[id][0]);
    if(status != enStatusSuccess)
    return status;

    status=pfTCS230colourSensorGetFrequency(id, enTCS230filter_Green, &tcs230colourSensorWhiteBalance[id][1]);
    if(status != enStatusSuccess)
    return status;

    status=pfTCS230colourSensorGetFrequency(id, enTCS230filter_Blue, &tcs230colourSensorWhiteBalance[id][2]);
    if(status != enStatusSuccess)
    return status;

    status=pfTCS230colourSensorGetFrequency(id, enTCS230filter_None, &tcs230colourSensorWhiteBalance[id][3]);
    if(status != enStatusSuccess)
    return status;

    return enStatusSuccess;
}

static PFEnStatus pfTCS230colourSensorGetFrequency(PFbyte id ,PFEnTCS230filterSelect filter, PFword* frequency)
{
    volatile PFEnStatus status;
    volatile PFdword timeout=0;
    volatile PFdword currentCount=0;
#if (PF_TCS230COLOURSENSOR_DEBUG == 1)
	  if(id>=TCS230_MAX_DEVICE)
		return enStatusInvArgs;
	  if(tcs230colourSensorInitFlag[i]==enBooleanFalse)
		return enStatusNotConfigured;
#endif
    pfTCS230colourSensorSetFilter(id,filter);

    /** wait to adjust Timer tick for proper delay measurement */
    pfTickDelayMs(1);
    //timerTicks=(pfTickGetTimerTick()) + 1;
    //while(timerTicks>(pfTickGetTimerTick()));

    timeout=pfTickSetTimeoutMs(TCS230ColourSensorMeasurePeriod);

    status=tcs230colourSensorConfigStruct[id].timerReset();
    if(status!=enStatusSuccess)
    return status;

    while( !pfTickCheckTimeout(timeout) );

    tcs230colourSensorConfigStruct[id].timerReadCount((PFdword*)&currentCount);

    switch(tcs230colourSensorConfigStruct[id].scalingFreq)
    {
        case enTCS230scalingFrequency_2:     *frequency = 50*(currentCount * (1000/TCS230ColourSensorMeasurePeriod));
                                             break;

        case enTCS230scalingFrequency_20:    *frequency = 5*(currentCount * (1000/TCS230ColourSensorMeasurePeriod));
                                             break;

        case enTCS230scalingFrequency_100:   *frequency = currentCount*(1000/TCS230ColourSensorMeasurePeriod);
                                             break;
    }

    return enStatusSuccess;
}

static PFEnStatus pfTCS230colourSensorSetFilter(PFbyte id, PFEnTCS230filterSelect filter)
{	
		#if (PF_TCS230COLOURSENSOR_DEBUG == 1)
		if(id>=TCS230_MAX_DEVICE)
		return enStatusInvArgs;
		if(tcs230colourSensorInitFlag[i]==enBooleanFalse)
		return enStatusNotConfigured;
		#endif
    switch(filter)
    {
        case enTCS230filter_Red:    pfGpioPinsClear(tcs230colourSensorConfigStruct[id].s2_port.port, tcs230colourSensorConfigStruct[id].s2_port.pin);
                                    pfGpioPinsClear(tcs230colourSensorConfigStruct[id].s3_port.port, tcs230colourSensorConfigStruct[id].s3_port.pin);
                                    break;

        case enTCS230filter_Green:  pfGpioPinsSet(tcs230colourSensorConfigStruct[id].s2_port.port, tcs230colourSensorConfigStruct[id].s2_port.pin);
                                    pfGpioPinsSet(tcs230colourSensorConfigStruct[id].s3_port.port, tcs230colourSensorConfigStruct[id].s3_port.pin);
                                    break;

        case enTCS230filter_Blue:   pfGpioPinsClear(tcs230colourSensorConfigStruct[id].s2_port.port, tcs230colourSensorConfigStruct[id].s2_port.pin);
                                    pfGpioPinsSet(tcs230colourSensorConfigStruct[id].s3_port.port, tcs230colourSensorConfigStruct[id].s3_port.pin);
                                    break;

        default:                    pfGpioPinsSet(tcs230colourSensorConfigStruct[id].s2_port.port, tcs230colourSensorConfigStruct[id].s2_port.pin);
                                    pfGpioPinsClear(tcs230colourSensorConfigStruct[id].s3_port.port, tcs230colourSensorConfigStruct[id].s3_port.pin);
                                    break;
    }
	return enStatusSuccess;
}

PFEnStatus pfTCS230colourSensorGetColor(PFbyte id, PFdword* colour)
{
    PFEnStatus status;
    PFbyte red = 0, green = 0, blue = 0;//,noFilter=0;
    PFword temp = 0;
    PFdword cal = 0;
    //PFdword i, j = 0;
#if (PF_TCS230COLOURSENSOR_DEBUG == 1)
	if(id>=TCS230_MAX_DEVICE)
	{	
		return enStatusInvArgs;
	}
	if(tcs230colourSensorInitFlag[id]==enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif

	// Get Red channel frequency
	status = pfTCS230colourSensorGetFrequency(id,enTCS230filter_Red, &temp);
	if(status != enStatusSuccess)
    {
		return status;
	}
    if(temp<=tcs230colourSensorBlackBalance[id][0])
    {
		red = 0;
	}	
    else
    {
		cal = ( 255*(PFdword)(temp - tcs230colourSensorBlackBalance[id][0]) )/(tcs230colourSensorWhiteBalance[id][0] - tcs230colourSensorBlackBalance[id][0]);
		red = (cal<255) ? (PFbyte)cal : 255;
    }

	// Get Green channel frequency
	status=pfTCS230colourSensorGetFrequency(id,enTCS230filter_Green, &temp);
    if(status != enStatusSuccess)
    {
		return status;
	}
    if(temp<=tcs230colourSensorBlackBalance[id][1])
    {
		green = 0;
    }
		else
    {
		cal = ( 255*(PFdword)(temp - tcs230colourSensorBlackBalance[id][1]) )/(tcs230colourSensorWhiteBalance[id][1] - tcs230colourSensorBlackBalance[id][1]);
		green = (cal<255) ? (PFbyte)cal : 255;
	}

	// Get Blue channel frequency
	status=pfTCS230colourSensorGetFrequency(id,enTCS230filter_Blue, &temp);
    if(status != enStatusSuccess)
    {
		return status;
	}
    if(temp<=tcs230colourSensorBlackBalance[id][2])
    {
		blue = 0;
    }
	else
	{
		cal = ( 255*(PFdword)(temp - tcs230colourSensorBlackBalance[id][2]) )/(tcs230colourSensorWhiteBalance[id][2] - tcs230colourSensorBlackBalance[id][2]);
        blue = (cal<255) ? (PFbyte)cal : 255;
	}

	// Get NoFilter frequency
/*
	status=pfTCS230colourSensorGetFrequency(id,enTCS230filter_None, &temp);
    if(status != enStatusSuccess)
    {
		return status;
	}
    if(temp<=tcs230colourSensorBlackBalance[id][3])
    {
		noFilter = 0;
    }
	else
	{
		cal = ( 255*(PFdword)(temp - tcs230colourSensorBlackBalance[id][3]) )/(tcs230colourSensorWhiteBalance[id][3] - tcs230colourSensorBlackBalance[id][3]);
        noFilter = (cal<255) ? (PFbyte)cal : 255;
	}
*/
    *colour = ((PFdword)red << 16) | ((PFdword)green << 8) | blue;

    return enStatusSuccess;
}
