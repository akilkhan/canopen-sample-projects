#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_gpio.h"
#include "prime_qeiLS7366.h"
#include "prime_cheetah.h"


static PFCfgCheetah CheetahCfg[PF_MAX_CHEETAH_SUPPORTED];
static PFbyte cheetahId[PF_MAX_CHEETAH_SUPPORTED];
static PFbyte qeiId[PF_MAX_CHEETAH_SUPPORTED];
static PFbyte cheetahDevCount;
static PFEnBoolean CheetahInit = enBooleanFalse;

PFEnStatus pfCheetahOpen(PFbyte* id, PPFCfgCheetah config,PFbyte count)
{
	PFbyte loop,i;
#if (PF_CHEETAH_DEBUG == 1)
	if(id==0)
		return enStatusInvArgs;
	if(config == 0)
		return enStatusInvArgs;
	if(count > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif	// #if (PF_CHEETAH_DEBUG == 1)
	
	
	
	for(i =0; i < count;i++ )
	{
		for(loop=0 ; loop<PF_MAX_CHEETAH_SUPPORTED ; loop++)
		{
			if(cheetahId[loop] == enBooleanFalse)
			{
				cheetahId[loop] = enBooleanTrue;
				cheetahDevCount++;
				if(pfQeiLS7366Open(&qeiId[loop],&config[i].qeiConfig,1) != enStatusSuccess)
				return enStatusError;
				pfQeiLS7366CountEnable(qeiId[loop],enBooleanTrue);
			
				config[i].ppfPwmUpdateMatchRegister(config[i].motorPwmCh, 0);
				pfGpioPinsSet(config[i].motorCsGpio.port, config[i].motorCsGpio.pin);
				pfGpioPinsClear(config[i].motorEnableGpio.port, config[i].motorEnableGpio.pin);
				pfGpioPinsSet(config[i].motorDisableGpio.port, config[i].motorDisableGpio.pin);
			
				pfMemCopy(&CheetahCfg[loop], &config[i], sizeof(PFCfgCheetah));
				id[i] = loop;
				break;
			}
			if(loop== PF_MAX_CHEETAH_SUPPORTED)
			return enStatusError;
		}
	}
	CheetahInit = enBooleanTrue;
	return enStatusSuccess;
}

PFEnStatus pfCheetahClose(PFbyte id)
{
#if (PF_CHEETAH_DEBUG == 1)
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
	
#endif	// #if (PF_CHEETAH_DEBUG == 1)
	
	pfGpioPinsClear(CheetahCfg[id].motorEnableGpio.port, CheetahCfg[id].motorEnableGpio.pin);
	pfGpioPinsSet(CheetahCfg[id].motorDisableGpio.port, CheetahCfg[id].motorDisableGpio.pin);
	//pfSpiUnregisterDevice(&id);
	pfQeiLS7366Close(id);
	CheetahInit = enBooleanFalse;
	cheetahDevCount--;
	return enStatusSuccess;
}

PFEnStatus pfCheetahEnable(PFbyte id,PFEnBoolean status)
{
#if (PF_CHEETAH_DEBUG == 1)
	if(CheetahInit != enBooleanTrue)
		return enStatusNotConfigured;
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif	
	if(status == enBooleanTrue)
	{
		pfGpioPinsSet(CheetahCfg[id].motorEnableGpio.port, CheetahCfg[id].motorEnableGpio.pin);
		pfGpioPinsClear(CheetahCfg[id].motorDisableGpio.port, CheetahCfg[id].motorDisableGpio.pin);
		pfQeiLS7366CountEnable(qeiId[id],enBooleanTrue);		
	}
	else
	{
		pfGpioPinsClear(CheetahCfg[id].motorEnableGpio.port, CheetahCfg[id].motorEnableGpio.pin);
		pfGpioPinsSet(CheetahCfg[id].motorDisableGpio.port, CheetahCfg[id].motorDisableGpio.pin);
		pfQeiLS7366CountEnable(qeiId[id],enBooleanFalse);
	}
	
	return enStatusSuccess;
}

PFEnStatus pfCheetahSetDirection(PFbyte id,PFbyte dir)
{
#if (PF_CHEETAH_DEBUG == 1)
	if(CheetahInit != enBooleanTrue)
		return enStatusNotConfigured;
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif
	
	if(dir == 0)
	{
		pfGpioPinsClear(CheetahCfg[id].motorDirGpio.port, CheetahCfg[id].motorDirGpio.pin);
	}
	else
	{
		pfGpioPinsSet(CheetahCfg[id].motorDirGpio.port, CheetahCfg[id].motorDirGpio.pin);
	}
	
	return enStatusSuccess;
}

PFEnStatus pfCheetahSetSpeed(PFbyte id,PFbyte speed)
{
	PFdword pwmMaxCount, pwmVal;
#if (PF_CHEETAH_DEBUG == 1)
	if(CheetahInit != enBooleanTrue)
		return enStatusNotConfigured;
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif
	
	if(CheetahCfg[id].ppfPwmGetRepititionRate(&pwmMaxCount) != enStatusSuccess)
	{
		return enStatusError;
	}
	
	pwmVal = (pwmMaxCount * speed) / 100;
	
	if(CheetahCfg[id].ppfPwmUpdateMatchRegister(CheetahCfg[id].motorPwmCh, pwmVal) != enStatusSuccess)
	{
		return enStatusError;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfCheetahMove(PFbyte id,PFbyte dir, PFbyte speed)
{
	PFdword pwmMaxCount, pwmVal;
#if (PF_CHEETAH_DEBUG == 1)
	if(CheetahInit != enBooleanTrue)
		return enStatusNotConfigured;
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif
	// Set direction
	if(dir == 0)
	{
		pfGpioPinsClear(CheetahCfg[id].motorDirGpio.port, CheetahCfg[id].motorDirGpio.pin);
	}
	else
	{
		pfGpioPinsSet(CheetahCfg[id].motorDirGpio.port, CheetahCfg[id].motorDirGpio.pin);
	}
	
	// Get PWM repition rate
	if(CheetahCfg[id].ppfPwmGetRepititionRate(&pwmMaxCount) != enStatusSuccess)
	{
		return enStatusError;
	}
	// Calculate PWM value
	pwmVal = (pwmMaxCount * speed) / 100;
	// Update PWM match register
	if(CheetahCfg[id].ppfPwmUpdateMatchRegister(CheetahCfg[id].motorPwmCh, pwmVal) != enStatusSuccess)
	{
		return enStatusError;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfCheetahGetEncoderCount(PFbyte id,PFdword* count)
{
#if (PF_CHEETAH_DEBUG == 1)
	if(CheetahInit != enBooleanTrue)
		return enStatusNotConfigured;
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif
	
	if(pfQeiLS7366ReadCount(id,count) != enStatusSuccess)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfCheetahClearEncoderCount(PFbyte id)
{
#if (PF_CHEETAH_DEBUG == 1)
	if(CheetahInit != enBooleanTrue)
		return enStatusNotConfigured;
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif
	
	if(pfQeiLS7366ResetCount(id) != enStatusSuccess)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfCheetahGetBatteryVoltage(PFbyte id,PFdword* battVtg)
{
	PFdword adcVal;
#if (PF_CHEETAH_DEBUG == 1)
	if(CheetahInit != enBooleanTrue)
		return enStatusNotConfigured;
	if(id > PF_MAX_CHEETAH_SUPPORTED)
		return enStatusInvArgs;
#endif
	// Get ADC value
	if(CheetahCfg[id].ppfAdcGetVoltageSingleConversion(CheetahCfg[id].battMonAdcCh, &adcVal) != enStatusSuccess)
	{
		return enStatusError;
	}
	
	// Convert ADC value into battery voltage
	// Since there is a resistor divider of 10K-1K resistor and we are measuring voltage across 1K resistor,
	// we need to multiply adc value by 11.
	*battVtg = adcVal * 11;
		
	return enStatusSuccess;
}
