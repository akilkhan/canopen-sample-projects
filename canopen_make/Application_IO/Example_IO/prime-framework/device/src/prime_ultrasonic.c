#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_delay.h"
#include "prime_gpio.h"
#include "prime_ultrasonic.h"

static PFCfgUltrasonic ultrasonicConfigStruct[MAX_NO_OF_ULTRASONIC_SUPPORTED];  /* to store sensor configuration settings */
static volatile PFbyte ultrasonicInitFlag[MAX_NO_OF_ULTRASONIC_SUPPORTED]={0}; 	/* to check for initialization of sensor */
static volatile PFbyte ultrasonicDeviceCount=0;

PFEnStatus pfUltrasonicOpen(PFbyte* devId,PFpCfgUltrasonic configs, PFbyte deviceCount)
{
	PFbyte i,j;
	
#if (PF_ULTRASONIC_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(configs == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_ULTRASONIC_DEBUG == 1)
   
		for(i=0;i<deviceCount;i++)
		{
			for(j=0;j<MAX_NO_OF_ULTRASONIC_SUPPORTED;j++)
			{
				if(ultrasonicInitFlag[j]==enBooleanFalse)
				{
					pfMemCopy(&ultrasonicConfigStruct[j], &configs[i], sizeof(PFCfgUltrasonic));
					ultrasonicDeviceCount++;
					devId[i]=j;
					ultrasonicInitFlag[j] = enBooleanTrue;
					break;
				}
			}
			if(j == MAX_NO_OF_ULTRASONIC_SUPPORTED)
					return enStatusError;
		}	
   return enStatusSuccess;
}
PFEnStatus pfUltrasonicClose(PFbyte* devId)
{

#if (PF_ULTRASONIC_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_ULTRASONIC_DEBUG == 1)

		/* check for initialization */
			if(ultrasonicInitFlag[*devId]==enBooleanFalse)
				return enStatusNotConfigured;

			if( *devId >= MAX_NO_OF_ULTRASONIC_SUPPORTED )
				return enStatusInvArgs;

			ultrasonicDeviceCount--;
			ultrasonicInitFlag[*devId] = enBooleanFalse;	
			return enStatusSuccess;
}

PFEnStatus pfUltrasonicGetDistance(PFbyte* devId, PFdword* distance)
{
	PFdword time,temp;
	volatile PFdword count=0;
						
#if (PF_ULTRASONIC_DEBUG == 1)
	if(devId == NULL)
		return enStatusInvArgs;
	if(distance == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_ULTRASONIC_DEBUG == 1)
		

	/* check for initialization */
	if(ultrasonicInitFlag[*devId]==enBooleanFalse)
	{
		return enStatusNotConfigured;
	}			
	if( *devId >= MAX_NO_OF_ULTRASONIC_SUPPORTED )
	{
		return enStatusInvArgs;
	}
			
	count = pfDelayGetTick();
	while( count >= (pfDelayGetTick()) );
				
	/* Issue Trigger pulse of 30us (should be greater than 10us) to Trigger pin of Ultrasonic sensor */
	pfGpioPinsSet(ultrasonicConfigStruct[*devId].trigger.port, ultrasonicConfigStruct[*devId].trigger.pin);

	pfDelayMicroSec(10);
	pfGpioPinsClear(ultrasonicConfigStruct[*devId].trigger.port, ultrasonicConfigStruct[*devId].trigger.pin);

	temp = pfDelayGetTimerPeriod();
	count = (pfDelayGetTick())+(1000/temp);

	do
	{
		if(count < (pfDelayGetTick()) )
		{
			return enStatusTimeout;
		}
	}while( !(pfGpioPortRead(ultrasonicConfigStruct[*devId].echo.port) & ultrasonicConfigStruct[*devId].echo.pin) );
	temp = pfDelayGetTick();

	while( pfGpioPortRead(ultrasonicConfigStruct[*devId].echo.port) & ultrasonicConfigStruct[*devId].echo.pin );

	count = (pfDelayGetTick()) - temp;
	time = count * (pfDelayGetTimerPeriod());
	*distance = (time*10)/58;

	return enStatusSuccess;
}


