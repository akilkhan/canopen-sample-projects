/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework MMC Card Driver
 *
 *
 * Review status: NO
 *
 */
#pragma once
/** Configure maximum MMC cards to allow	*/
#define PF_MMC_MAX_DEVICE_SUPPORTED		2

/** Enable/disable write operations to the MMC card				*/
#define PF_MMC_READONLY					0
/** Set to enable I/O control functions support for MMC card	*/
#define PF_MMC_USE_IOCTL				1


/** MMC card configuration structure		*/
typedef struct
{
	PFGpioPortPin gpioChipSelect;														/**< GPIO pin used for SPI chip select			*/
	PFGpioPortPin gpioPower;															/**< GPIO pin used for power control			*/
	PFGpioPortPin gpioCardDetect;														/**< GPIO pin used for device detection			*/
	PFEnBoolean usePowerControl;														/**< Use power control option for MMC card		*/
	PFEnBoolean useCardDetect;															/**< Use card detect option for MMC card		*/
	PFEnStatus (*spiRegister)(PFbyte* id, PFpGpioPortPin chipSelect);					/**< Pointer to SPI device register function	*/
	PFEnStatus (*spiUnregister)(PFbyte* id);											/**< Pointer to SPI device unregister function	*/
	PFEnStatus (*spiChipSelect)(PFbyte* id, PFbyte pinStatus);							/**< Pointer to SPI chip select function		*/
	PFEnStatus (*spiExchangeByte)(PFbyte *id, PFword data, PFword* rxData);				/**< Pointer to SPI exchange byte function		*/
	PFEnStatus (*spiWrite)(PFbyte* id, PFbyte* data, PFdword size);						/**< Pointer to SPI write function				*/
	PFEnStatus (*spiRead)(PFbyte* id, PFbyte* data, PFdword size, PFdword* readBytes);	/**< Pointer to SPI read function				*/
}PFCfgMmc;

/** Pointer to PFCfgMmc structure		*/
typedef PFCfgMmc* PFpCfgMmc;

/**
 * Initialize mmc card with the given configuration.
 *
 * \param id Pointer to PFbyte variable to store the device id for newly initialized device.
 * \param config Pointer to PFCfgMmc structure initialized with MMC card configuration.
 *
 * \return Status
 */
PFEnStatus pfMmcOpen(PFbyte* id, PFpCfgMmc config);

/**
 * The function returns current status of the mmc card.
 *
 * \param id Pointer to id of the mmc card to get status.
 *
 * \return Mmc card status
 * enStatusSuccess: card physically present, initialized.
 * enStatusNotConfigured: mc card is physically present but not initialized
 * enStatusNotExist: mmc card is physically absent.
 */
PFEnStatus pfMmcGetStatus(PFbyte* id);


/**
 * The function reads \a uCount sectors starting from sector number specified by \a sector from mmc card.
 *
 * \param id Pointer to id of the mmc card to read
 * \param pBuf Pointer to buffer to store read data
 * \param sector Sector number to start reading
 * \param uCount Number of sectors to read
 *
 * return Status
 */
PFEnStatus pfMmcRead(PFbyte* id, PFbyte *pBuf, PFdword sector, PFbyte uCount);

/**
 * The function writes data to mmc card. Data from \a pBuf buffer is written to \a uCount sectors
 * starting from sector number specified by \a sector.
 *
 * \param id Pointer to id of the mmc card to write
 * \param pBuf Pointer to buffer to be written on the device
 * \param sector Sector number to start writing
 * \param uCount Number of sectors to write
 *
 * return Status
 */
PFEnStatus pfMmcWrite (PFbyte* id, const PFbyte *pBuf, PFdword sector, PFbyte uCount);

/**
 * Control function for device specific features.
 *
 * \param id Pointer to id of the mmc card to access
 * \param ctrl Command code
 * \param pBuf Pointer to buffer for command dependent parameters. When not in use, null pointer should be used.
 *
 * return Status
 */
PFEnStatus pfMmcIoCtrl(PFbyte* id, PFbyte ctrl,	void *pBuf);



