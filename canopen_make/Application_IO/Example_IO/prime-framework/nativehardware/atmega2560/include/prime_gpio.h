/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework GPIO driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */


#pragma once

typedef struct
{
	GPIO_typedef *port;			// Port number for GPIO. Predefine d port numbers should be used.
	PFbyte pin;			// Pins to initialize. Pins should be mentioned in bit-shifted format.
}PFGpioPortPin;

/** Pointer to PFGpioPortPin structure	 */
typedef PFGpioPortPin* PFpGpioPortPin;

/**
 * Set the given no of pins
 * \param port port that is to be used
 * \param bit pin/pins that is to be used  
 */
#define pfGpioPinsSet(port,bit)	port->PORT |= bit

/**
 * Clear the given no of pins
 * \param port port that is to be used
 * \param bit pin/pins that is to be used
 */
#define pfGpioPinsClear(port,bit)		port->PORT &=~(bit)

/**
 * \param Set the complete pins on given port
 * \param port port that is to be used
 */
#define pfGpioPortWrite(port,val)		port->PORT = val

/**
 * Read the complete pins on given port
 * \param port port that is to be used
 */
#define pfGpioPortRead(port)			port->PIN



typedef enum
{
	enGpioDirInput,
	enGpioDirOutput
}PFEnDirection;

// Enumeration for pin mode
typedef enum
{
	enGpioPinModePullUp,
	enGpioPinModePullDown
}PFEnGpioPinMode;

// GPIO configuration structure.
typedef struct
{
	GPIO_typedef* port;			/**< Port number for GPIO. Predefine d port numbers should be used.			*/
	PFbyte pins;			 	/**< Pins to initialize. Pins should be mentioned in bit-shifted format.	*/
	PFEnDirection direction;	/**< Pin I/O direction														*/
	PFEnGpioPinMode mode;		/**< Pin operating mode.													*/
}PFCfgGpio, *PFpCfgGpio;

/**
 * The function configures one or more gpio pins on one or more ports for different modes
 *
 * \param config gpioConfig structure array, which contains array port numbers, pin numbers and port settings to configure for these pins.
 * \param count number of elements in the config array.
 *
 * \return 	gpio initialization status. 
 */
PFEnStatus pfGpioInit(PFpCfgGpio config, PFbyte count);


/**
 * The function configures one or more gpio pins on a port for different modes
 *
 * \param config gpioConfig structure, which contains port number, pin numbers and port settings to configure for these pins.
 *
 * \return 	enStatus. gpio initialization status. 
 *			enOK for no error, 
 *			enInvalid for any invalid parameters passed, 
 *			enUnknown for any other error
 */
PFEnStatus pfGpioPinConfig(PFpCfgGpio config);

