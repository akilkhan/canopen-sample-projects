#include "prime_framework.h"
#if (PF_USE_UART3 == 1)
#include "prime_fifo.h"
#include "prime_uart3.h"

#if(UART3_USE_FIFO != 0)
	#warning UART3 FIFO is enabled for interrupt based communication
	#if( (UART3_BUFFER_SIZE == 0) || ((UART3_BUFFER_SIZE & (UART3_BUFFER_SIZE - 1)) != 0) )
		#error UART3_BUFFER_SIZE cannot be zero. UART3_BUFFER_SIZE should be power of 2
	#endif
#endif

static PFword uart3BaudArray[]={383,191,95,63,47,31,23,15,11,7,3};

static PFEnBoolean uart3ChInit;						// initialize flag for the channel
static PFEnBoolean uart3ChBusy;						// busy flag for the channel
static PFbyte uart3ChInt;
	
#if(UART3_USE_FIFO != 0)
static PFbyte uart3RxBuffer[UART3_BUFFER_SIZE];	// UART3 transmit buffer
static PFbyte uart3TxBuffer[UART3_BUFFER_SIZE];	// UART3 receive buffer
static PFFifo uart3TxFifo;							// UART3 transmit fifo structure
static PFFifo uart3RxFifo;							// UART3 receive fifo structure
#else	
static PFcallback uart3TxCallback;					// transmit callback for the channel
static PFcallback uart3RxCallback;					// receive callback for the channel
#endif	// #if(UART3_USE_FIFO != 0)


PFEnStatus pfUart3Open(PFpCfgUart3 config)
{	
#ifdef PFUART3_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if ((config->mode) >0x03)
	{
		return enStatusNotSupported;
	}
	if ((config->parity) > 0x02)
	{
		return enStatusNotSupported;
	}
	if ((config->stopBits) !=enUart3StopBits_1)
	{
		return enStatusNotSupported;
	}	
#if(UART3_USE_FIFO != 0)
	if((config->transmitCallback) ==0)
	{
		return enStatusInvArgs;
	}
	if((config->receiveCallback) ==0)
	{
		return enStatusInvArgs;
	}	
#endif
#endif

	
	UART3_CHANNEL->UBRRH = (uart3BaudArray[(config->baudrate)] >> 8);
	UART3_CHANNEL->UBRRL = uart3BaudArray[(config->baudrate)];
	if ((config->mode) <=0x03)
	{
		UART3_CHANNEL->UCSRB |= ((config->mode)<<3);
	} 
				
	if ((config->parity) !=enUart3ParityNone)
	{
		if ((config->parity) !=enUart3ParityEven)
		{
			UART3_CHANNEL->UCSRC = ((1<<5)|(1<<4));
		} 
		else
		{
			UART3_CHANNEL->UCSRC = ((1<<5)|(0<<4));
		}
	} 
	
				
	if ((config->stopBits) !=enUart3StopBits_1)
	{
		UART3_CHANNEL->UCSRC |=(1<<3);
	}
 
	if ((config->interrupts) != enUart3IntNone)
	{
		UART3_CHANNEL->UCSRB |=(config->interrupts) <<4;
		uart3ChInt =(config->interrupts);
	} 
				
	if(((config->dataBits) & 0x07) !=0x07)
	{	
		UART3_CHANNEL->UCSRC |= config->dataBits <<1;
	}
	else
	{	
		UART3_CHANNEL->UCSRC |= (config->dataBits)<<1;
		UART3_CHANNEL->UCSRB |= (1<<2);
	}
		
#if(UART3_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uart3TxFifo, uart3TxBuffer, UART3_BUFFER_SIZE);
	pfFifoInit(&uart3RxFifo, uart3RxBuffer, UART3_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	if((config->transmitCallback) !=0)
	{
		uart3TxCallback = (config->transmitCallback);
	}
	if((config->receiveCallback) !=0)
	{
		uart3RxCallback = (config->receiveCallback);
	}	
#endif	// #if(UART3_USE_FIFO != 0)
	uart3ChInit =  enBooleanTrue;
	return enStatusSuccess;

}
			
/** ---------------------------------------------------*/
PFEnStatus pfUart3WriteByte(PFbyte data)
{

#ifdef PF_UART3_DEBUG
	// check if the channel is initialized
	if(uart3ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// _DEBUG_

	UART3_CHANNEL->UDR =data;
	while(!(UART3_CHANNEL->UCSRA &(1<<5)));
	return enStatusSuccess;
}

PFEnStatus pfUart3ReadByte(PFbyte* data)
{
#ifdef PF_UART3_DEBUG
	// check if the channel is initialized
	if(uart3ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
	if(data == 0)
	{
		return enStatusInvArgs;
	}
#endif	// PFUART3_DEBUG

	while(!(UART3_CHANNEL->UCSRA &(1<<7)));
	*data = UART3_CHANNEL->UDR;
	return enStatusSuccess;
}

PFEnStatus pfUart3Write(PFbyte* data, PFdword size)
{
	PFdword index;
#ifdef PF_UART3_DEBUG
	// check if the channel is initialized
	if(uart3ChInit !=  enBooleanTrue)
	{
		return enStatusNotConfigured;
	}		
#endif	// PFUART3_DEBUG
	
	// check if the channel is busy
	if(uart3ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}		
	
	// set channel busy	
	uart3ChBusy = enBooleanTrue;
	
	// if fifo is to be used push data into the fifo
	if((uart3ChInt & enUart3IntTx) != 0)
	{
	#if(UART3_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uart3TxFifo, *(data + index));
		}
		UART3_CHANNEL->UDR = pfFifoPop(&uart3TxFifo);
	#else
		UART3_CHANNEL->UDR = *data;
	#endif	// #if(UART3_USE_FIFO != 0)	
		//LPC_UART30->IER = uart3ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART3_CHANNEL->UDR = *(data + index);
			while(!(UART3_CHANNEL->UCSRA &(1<<5)));

		}
	}
	
	uart3ChBusy = enBooleanFalse;
	return enStatusSuccess;
}




PFEnStatus pfUart3Read(PFbyte* data, PFdword size,PFdword* readBytes)
{
	PFdword index;
#ifdef PFUART3_DEBUG
	if(uart3ChInit !=  enBooleanTrue)
	{	
		return enStatusNotConfigured;
	}	
#endif	// PFUART3_DEBUG
	
	if(uart3ChBusy != enBooleanFalse)
	{	
		return enStatusBusy;
	}		
	uart3ChBusy = enBooleanTrue;
	
	if((uart3ChInt & enUart3IntRx) != 0)
	{
	#if(UART3_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uart3RxFifo) == enBooleanTrue)
				break;
			
			*(data + index) = pfFifoPop(&uart3RxFifo);
				*readBytes += 1;
		}
	#endif	// #if(UART3_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while(!(UART3_CHANNEL->UCSRA &(1<<7)));
			*(data + index) = UART3_CHANNEL->UDR;
				*readBytes++;
		}
	}
	uart3ChBusy = enBooleanFalse;
	return enStatusSuccess;
}


PFEnStatus pfUart3Close(void)
{
#ifdef PF_UART3_DEBUG
	// check if the channel is initialized
	if(uart3ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART3_DEBUG
	UART3_CHANNEL->UCSRB |=(enUart3ModeTxRx)<<3;
	uart3ChInit = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART3_USE_FIFO != 0)
PFEnStatus pfUart3GetRxBufferCount(PFdword* count)
{
#ifdef PF_UART3_DEBUG
	// check if the channel is initialized
	if(uart3ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
	if(count == 0)
		return enStatusInvArgs;
#endif	// PF_UART3_DEBUG

	*count = uart3RxFifo.count;
	return enStatusSuccess;
}

PFEnStatus pfUart3TxBufferFlush(void)
{
#ifdef PF_UART3_DEBUG
	// check if the channel is initialized
	if(uart3ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART3_DEBUG

	pfFifoFlush(&uart3TxFifo);
	return enStatusSuccess;
}

PFEnStatus pfUart3RxBufferFlush(void)
{
#ifdef PF_UART3_DEBUG
	// check if the channel is initialized
	if(uart3ChInit !=  enBooleanTrue)
		return enStatusNotConfigured;
#endif	// PF_UART3_DEBUG

	pfFifoFlush(&uart3RxFifo);
	return enStatusSuccess;
}
#endif	// #if(UART3_USE_FIFO != 0)

void INT_HANDLER(USART3_RX)(void)
{
	PFbyte rxByte;
	#if(UART3_USE_FIFO != 0)
		rxByte = UART3_CHANNEL->UDR;
		pfFifoPush(&uart3RxFifo, rxByte);
	#else
		if(uart3RxCallback != 0)
		{
			uart3RxCallback();
		}
		return;
	#endif	
}

void INT_HANDLER(USART3_TX)(void)
{
	#if(UART3_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uart3TxFifo) != enBooleanTrue)
		{
			UART3_CHANNEL->UDR = pfFifoPop(&uart3TxFifo);
		}
		
	#else	
		if(uart3TxCallback != 0)
		{
			uart3TxCallback();
		}
		return;
	#endif		//
}

#endif 		//#if (PF_USE_UART3 == 1)