#include "prime_framework.h"
#if (PF_USE_TIMER5 == 1)
#include "prime_timer5.h"

static PFEnStatus timer5InitFlag=0;
static PFcallback timer5cmpMatchACallback;
static PFcallback timer5cmpMatchBCallback;
static PFcallback timer5cmpMatchCCallback;
static PFcallback timer5inputCaptureCallback;
static PFcallback timer5overFlowCallback;

PFEnStatus pfTimer5Open(PFpCfgTimer5 config)
{
#ifdef PF_TIMER5_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}		
    if( (config->timer5Mode != 0x08) && (config->timer5Mode != 0x18) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionC < 0) || (config->exMatchActionC> 3) )
	{
		return enStatusInvArgs;
	}		

		if( ((config->interrupt & TIMER5_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER5_INT_MATCHREG_B) != 0) && (config->cmpMatchBCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER5_INT_MATCHREG_C) != 0) && (config->cmpMatchCCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER5_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER5_INT_INPUTCAPTURE) != 0) && (config->inputCaptureCallback == 0) )
	{
		return enStatusInvArgs;
	}
	
#endif
	if(config->timer5Mode)
	{
		TIMER5_CHANNEL->TCCRB = config->timer5Mode;
	}	
	TIMER5_CHANNEL->OCRA = config->matchValueA;
	TIMER5_CHANNEL->OCRB = config->matchValueB;
	TIMER5_CHANNEL->OCRC = config->matchValueC;
	
	if(config->inputCaptureRisingEdge)
	{
		TIMER5_CHANNEL->TCCRB |= 1<<6;
	}	
	if(Timer5InputCaptureNoiseCanceler__)
	{
		TIMER5_CHANNEL->TCCRB |= 1<<7;
	}	
	if(config->exMatchActionA != enTimer5ExtMatchCtrlNone)
	{
		TIMER5_CHANNEL->TCCRA = config->exMatchActionA << 6;
	}	
	if(config->exMatchActionB!=enTimer5ExtMatchCtrlNone)
	{
		TIMER5_CHANNEL->TCCRA |=config->exMatchActionB << 4;
	}	
	if(config->exMatchActionC!=enTimer5ExtMatchCtrlNone)
	{
		TIMER5_CHANNEL->TCCRA |=config->exMatchActionC << 2;
	}	
	if(config->cmpMatchACallback !=0)
	{
		timer5cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		timer5cmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->cmpMatchCCallback !=0)
	{
		timer5cmpMatchCCallback =config->cmpMatchCCallback;
	}	
	if(config->inputCaptureCallback !=0)
	{
		timer5inputCaptureCallback =config->inputCaptureCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer5overFlowCallback =config->overFlowCallback;
	}
	if( config->interrupt != TIMER5_INT_NONE )
	{
		//TIMER5_CHANNEL->TIMSK = config.interrupt;
		TIMSK_INT->TIMER5 = config->interrupt;
	}		
	if(config->clockSource <= 7)
	{
		TIMER5_CHANNEL->TCCRB |= config->clockSource;
	}		

    timer5InitFlag=1;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer5Close(void)
{
#ifdef PF_TIMER5_DEBUG
    if(timer5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER5_CHANNEL->TCCRB = 0x00;
	TIMER5_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}

PFEnStatus pfTimer5Start(void)
{
#ifdef PF_TIMER5_DEBUG
	if(timer5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	PFbyte val;
	val=TIMER5_CHANNEL->TCCRB;
	TIMER5_CHANNEL->TCCRB = 0x00;
	TIMER5_CHANNEL->TCCRB |= val;
	TIMER5_CHANNEL->TCNT = 0;
	
	return enStatusSuccess;
}
PFEnStatus pfTimer5Reset(void)
{
#ifdef PF_TIMER5_DEBUG
	if(timer5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	TIMER5_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}

PFEnStatus pfTimer5ReadCount(PFdword* count)
{
#ifdef PF_TIMER5_DEBUG
	if(timer5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER5_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer5ReadCaptureCount(PFdword* count)
{
#ifdef PF_TIMER5_DEBUG
	if(timer5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
	if(count == 0)
	{
		return enStatusInvArgs;
	}
#endif
	*count = TIMER5_CHANNEL->ICR;
	return enStatusSuccess;
}

PFEnStatus pfTimer5UpdateMatchRegister(PFbyte regNum, PFword regVal)
{
#ifdef PF_TIMER5_DEBUG
	if(timer5InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if( (regNum < 'A') || (regNum > 'C') )
	{
		return enStatusNotConfigured;
	}	
#endif
	switch(regNum)
	{
		case 'A':	
				  TIMER5_CHANNEL->OCRA =regVal;
				  break;
		
		case 'B':
				  TIMER5_CHANNEL->OCRB =regVal;
				  break;
				
		case 'C':
				  TIMER5_CHANNEL->OCRC =regVal;
				  break;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer5IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER5_DEBUG
    if(interrupt && (~TIMER5_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER5 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer5IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER5_DEBUG
    if(interrupt && (~TIMER5_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER5 &= (~interrupt);
	return enStatusSuccess;	
}

void INT_HANDLER(TIMER5_COMPA)(void)
{
	if(timer5cmpMatchACallback !=0)
	timer5cmpMatchACallback();
}

void INT_HANDLER(TIMER5_COMPB)(void)
{
	if(timer5cmpMatchBCallback !=0)
	timer5cmpMatchBCallback();
}

void INT_HANDLER(TIMER5_COMPC)(void)
{
	if(timer5cmpMatchCCallback !=0)
	timer5cmpMatchCCallback();
}

void INT_HANDLER(TIMER5_CAPT)(void)
{
	if(timer5inputCaptureCallback !=0)
	timer5inputCaptureCallback();
}

void INT_HANDLER(TIMER5_OVF)(void)
{
	if(timer5overFlowCallback !=0)
	timer5overFlowCallback();
}
#endif		// #if (PF_USE_TIMER5 == 1)