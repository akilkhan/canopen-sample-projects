#include "prime_framework.h"
#if (PF_USE_I2C == 1)
#include "prime_fifo.h"
#include "prime_i2c.h"


#if(PF_I2C_USE_FIFO != 0)
#warning I2C FIFO is enabled for interrupt based communication
#if( (I2C_BUFFER_SIZE == 0) || ((I2C_BUFFER_SIZE & (I2C_BUFFER_SIZE - 1)) != 0) )
#error I2C_BUFFER_SIZE cannot be zero. I2C_BUFFER_SIZE should be power of 2
#endif
#endif	// #if(PF_I2C_USE_FIFO != 0)

// I2C control bits
#define I2C_ACK_FLAG				0x40
#define I2C_INT_FLAG				0x80
#define I2C_STOP_FLAG				0x10
#define I2C_START_FLAG				0x20
#define I2C_EN_FLAG					0x04


#define I2C_SET_CTRL_BIT(BIT)		I2C_CHANNEL->TWCR |= (BIT)
#define I2C_CLR_CTRL_BIT(BIT)		I2C_CHANNEL->TWCR &= ~(BIT)
#define I2C_GET_STATE()				I2C_CHANNEL->TWSR

#define I2C_SET_AA()				I2C_SET_CTRL_BIT(I2C_ACK_FLAG)
#define I2C_CLR_AA()				I2C_CLR_CTRL_BIT(I2C_ACK_FLAG)

#define I2C_SET_START()				I2C_SET_CTRL_BIT(I2C_START_FLAG)
#define I2C_CLR_START()				I2C_CLR_CTRL_BIT(I2C_START_FLAG)

#define I2C_SET_STOP()				I2C_SET_CTRL_BIT(I2C_STOP_FLAG)
#define I2C_CLR_STOP()				I2C_CLR_CTRL_BIT(I2C_STOP_FLAG)

#define I2C_CLR_INT()				I2C_SET_CTRL_BIT(I2C_INT_FLAG)

#define I2C_CALL_SLAVE(ADDR, RW)	I2C_CHANNEL->TWDR = ADDR | RW


// State codes for I2C communication
// Master mode
#define I2C_M_START					0x08
#define I2C_M_REP_START				0x10
#define I2C_M_ARB_LOST				0x38
// Master transmitter
#define I2C_M_SLA_W_TX_ACK			0x18
#define I2C_M_SLA_W_TX_NACK			0x20
#define I2C_M_DATA_TX_ACK			0x28
#define I2C_M_DATA_TX_NACK			0x30
// Master reciever
#define I2C_M_SLA_R_ACK				0x40
#define I2C_M_SLA_R_NACK			0x48
#define I2C_M_DATA_RX_ACK			0x50
#define I2C_M_DATA_RX_NACK			0x58

// Slave receiver
#define I2C_S_SLA_W_RX_ACK			0x60
#define I2C_S_ARB_LOST_SLA_W_RX		0x68
#define I2C_S_GEN_CALL_RX_ACK		0x70
#define	I2C_S_ARB_LOST_GC_RX		0x78
#define I2C_S_SLA_W_DATA_RX_ACK		0x80
#define I2C_S_SLA_W_DATA_RX_NACK	0x88
#define I2C_S_GC_DATA_RX_ACK		0x90
#define I2C_S_GC_DATA_RX_NACK		0x98
#define I2C_S_STOP_REP_START		0xA0
// Slave trasmitter
#define I2C_S_SLA_R_RX_ACK			0xA8
#define I2C_S_ARB_LOST_SLA_R_RX		0xB0
#define I2C_S_DATA_TX_ACK			0xB8
#define I2C_S_DATA_TX_NACK			0xC0
#define I2C_S_LAST_DATA_TX_NACK		0xC8

static volatile PFEnBoolean i2cChError = enBooleanFalse;		// transmission error flag
static volatile PFbyte i2cChRWFlag = 0;						// read/write flag
static volatile PFbyte i2cSlaveAddr = 0;						// slave addr to call
static volatile PFword i2cBytesToRead = 0;					// bytes to read from slave
static volatile PFword i2cBytesRead = 0;					// bytes actually read from I2C bus
static volatile PFEnBoolean i2cInitFlag = enBooleanFalse;		// initalize flag for the channel
static volatile PFEnBoolean i2cChBusy = enBooleanFalse;		// busy flag for the channel
static volatile PFbyte i2cChInt;								// interrupt flag for the channel
#if(PF_I2C_USE_FIFO != 0)
static volatile PFbyte i2cRxBuffer[I2C_BUFFER_SIZE];			// I2C transmit buffer
static volatile PFbyte i2cTxBuffer[I2C_BUFFER_SIZE];			// I2C receive buffer
static volatile PFFifo i2cTxFifo;							// I2C transmit fifo structure
static volatile PFFifo i2cRxFifo;							// I2C receive fifo structure
#else
static volatile PFcallback i2cCallback = 0;					// transmit callback for the channel
#endif	// #if(PF_I2C_USE_FIFO != 0)


PFEnStatus pfI2cOpen(PFpCfgI2c config)
{

#ifdef PF_I2C_DEBUG	
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if(config->preScale>3)
	{
		return enStatusInvArgs;
	}
	if((config->ownAddress & 0x10))	//addr is 7bit only last bit should be chked for 0 while passing 1 byte addr)
	{
		return enStatusInvArgs;
	}	
#if(PF_I2C_USE_FIFO == 0)	
	if( (config->i2cIntEnable != enBooleanFalse) && (config->callback == 0) )
	{
		return enStatusInvArgs;	
	}		
#endif	// #if(PF_I2C_USE_FIFO == 0)	
#endif	// #ifdef PF_I2C_DEBUG

	I2C_CHANNEL->TWBR |= config->baudrate;
	if(config->preScale <= 3)
	{
		I2C_CHANNEL->TWSR |=config->preScale;
	}	
	
#if(PF_I2C_USE_FIFO == 0)	
	if(config->callback!=0)
	{
		i2cCallback =config->callback;
	}
#endif
	
	if(!(config->ownAddress & 0x10))	//addr is 7bit only last bit should be chked for 0 while passing 1 byte addr)
	{
		I2C_CHANNEL->TWAR =config->ownAddress<<1 ;
	}
	
	if(config->enableGenCall != enBooleanFalse)
	{	
		I2C_CHANNEL->TWAR |=(BIT_0); // General call enable bit
	}	
	if(config->enableAck !=enBooleanFalse)
	{
		I2C_CHANNEL->TWCR |= (BIT_6);
	}
	
	// initialize FIFO
#if(PF_I2C_USE_FIFO != 0)
	pfFifoInit(&i2cTxFifo, i2cTxBuffer, I2C_BUFFER_SIZE);
	pfFifoInit(&i2cRxFifo, i2cRxBuffer, I2C_BUFFER_SIZE);
#endif	// #if(PF_I2C_USE_FIFO != 0)

	if(config->i2cIntEnable!=enBooleanFalse)
	{
		I2C_CHANNEL->TWCR |=(BIT_0);
		i2cChInt = config->i2cIntEnable;
	}

	//Enable I2C
	I2C_CHANNEL->TWCR |=(BIT_2);
	i2cInitFlag =1;
	return enStatusSuccess;
}


PFEnStatus pfI2cClose(void)
{
#ifdef PF_I2C_DEBUG
	if(i2cInitFlag==0)
	{
		return enStatusNotConfigured;
	}		
#endif
	I2C_CHANNEL->TWCR &=~(BIT_2);
	return enStatusSuccess;
	
}


PFEnStatus pfI2cIntEnable(void)
{
#ifdef PF_I2C_DEBUG
	if(i2cInitFlag==0)
	{
		return enStatusNotConfigured;
	}	
#endif
	I2C_CHANNEL->TWCR &=~(BIT_2);
	I2C_CHANNEL->TWCR |=(BIT_0);
	I2C_CHANNEL->TWCR |=(BIT_2);
		
	return enStatusSuccess;
	
}


PFEnStatus pfI2cIntDisable(void)
{
#ifdef PF_I2C_DEBUG
	if(i2cInitFlag==0)
	{
		return enStatusNotConfigured;
	}		
#endif
	I2C_CHANNEL->TWCR &=~(BIT_2);
	I2C_CHANNEL->TWCR &=~(BIT_0);
	I2C_CHANNEL->TWCR |=(BIT_2);
	return enStatusSuccess;
	
}

PFEnStatus pfI2cStart( void )
{

#ifdef PF_I2C_DEBUG
	if(i2cInitFlag == enBooleanFalse)
		return enStatusNotConfigured;
		
	if(i2cChBusy == enBooleanTrue)
		return enStatusBusy;
#endif	// _DEBUG_

	I2C_SET_START();
	return enStatusSuccess ;
}
PFEnStatus pfI2cStop( void )
{
#ifdef PF_I2C_DEBUG
	if(i2cInitFlag == enBooleanFalse)
		return enStatusNotConfigured;
#endif	// _DEBUG_

	I2C_SET_STOP();
	I2C_CLR_INT();
	return enStatusSuccess ;
}


static PFEnStatus pfDataProcessing(PFbyte* data, PFword size)
{
	PFbyte state;
	PFword index=0;
	 
	while(1)
	{
		while(!( I2C_CHANNEL->TWCR & I2C_INT_FLAG ));
		state = I2C_GET_STATE();
		switch(state)
		{
//
// Master mode
//
			case I2C_M_START:
			case I2C_M_REP_START:
				I2C_CALL_SLAVE(i2cSlaveAddr, i2cChRWFlag);
				I2C_CLR_START();
				break;
//
// Master transmitter
//
			case I2C_M_SLA_W_TX_ACK:
			case I2C_M_DATA_TX_ACK:
				if( index < size )
				{
						I2C_CHANNEL->TWDR = *(data + index);
						index++;
				}
				else
				{
					i2cChBusy = enBooleanFalse;
					return enStatusSuccess ;
				}
				break;
		
//
// Master transmitter error conditions
//
			case I2C_M_ARB_LOST:
			case I2C_M_SLA_W_TX_NACK:
			case I2C_M_DATA_TX_NACK:
				i2cChBusy = enBooleanFalse;
				return enStatusError ;
				//break;
					
//
// Master reciever
//
			case I2C_M_SLA_R_NACK:
				i2cChBusy = enBooleanFalse;
				return enStatusError ;
				//break;
		
			case I2C_M_SLA_R_ACK:
				// do nothing
				break;

			case I2C_M_DATA_RX_ACK:
				*(data + index)=I2C_CHANNEL->TWDR;
				index++;
				i2cBytesRead++;
			
				if(i2cBytesRead == i2cBytesToRead -1)
				{
					I2C_CLR_AA();
					return enStatusSuccess ;
				}
				break;

			case I2C_M_DATA_RX_NACK:
				*(data + index)=I2C_CHANNEL->TWDR;
				i2cBytesRead++;
				index++;
				i2cChBusy = enBooleanFalse;
				return enStatusSuccess ;
				//break;	
					
//
// Slave receiver
//
			case I2C_S_SLA_W_RX_ACK:
			case I2C_S_ARB_LOST_SLA_W_RX:
			case I2C_S_GEN_CALL_RX_ACK:
			case I2C_S_ARB_LOST_GC_RX:
			case I2C_S_STOP_REP_START:
				//dataIndex[ch] = 0;
				break;

			case I2C_S_SLA_W_DATA_RX_ACK:
			case I2C_S_GC_DATA_RX_ACK:
			case I2C_S_SLA_W_DATA_RX_NACK:
			case I2C_S_GC_DATA_RX_NACK:
				*(data + index)=I2C_CHANNEL->TWDR;
				index++;
				break;
		
//
// Slave trasmitter
//
			case I2C_S_SLA_R_RX_ACK:
			case I2C_S_ARB_LOST_SLA_R_RX:
			case I2C_S_DATA_TX_ACK:
				if( index < size )
				{
						I2C_CHANNEL->TWDR = *(data + index);
						index++;
				}
				else
				{
					I2C_CHANNEL->TWDR = 0;
					return enBooleanTrue;
				}
							
				break;

			case I2C_S_DATA_TX_NACK:
			case I2C_S_LAST_DATA_TX_NACK:
				i2cChBusy = enBooleanFalse;
				return enStatusSuccess ;
				//break;

			default:
				i2cChBusy = enBooleanFalse;
				return enStatusError;
				//break;
		
					
		}
		I2C_SET_CTRL_BIT(I2C_INT_FLAG);
	}				
}


PFEnStatus pfI2cWrite(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size)
{
	PFword index;
	PFEnStatus status;
	
#ifdef PF_I2C_DEBUG
	if(i2cInitFlag == enBooleanFalse)
		return enStatusNotConfigured;
		
	if(i2cChBusy == enBooleanTrue)
		return enStatusBusy;
#endif	// _DEBUG_

	i2cChBusy = enBooleanTrue;
	i2cSlaveAddr = slaveAddr;
	i2cChRWFlag = 0;
		
// Interrupt Based==========================================================================
	if( i2cChInt != enBooleanFalse)           
	{	
#if(PF_I2C_USE_FIFO != 0)
		
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&i2cTxFifo, *(data + index));
		}
		if(master == enBooleanTrue)
		{
			I2C_SET_START();
				
		}
		else
		{
			I2C_SET_AA();
		}
		while(i2cChBusy == enBooleanTrue);
		if(i2cChError == enBooleanTrue)
		{
			i2cChError = enBooleanFalse;
			return enStatusError;
		}
#else
	return enStatusNotSupported;
	
#endif	// #if(PF_I2C_USE_FIFO != 0)
	}
//Polling===================================================================================
	else
	{
		
		status = pfDataProcessing(data,size);								
		if(status != enStatusSuccess)
					return status;	
	}	
		
	return enStatusSuccess;
}

PFEnStatus pfI2cRead(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size, PFword* readBytes)
{
	PFword index = 0;
	PFEnStatus status;
	volatile PFEnBoolean loop = enBooleanTrue; 
	
#ifdef PF_I2C_DEBUG
	if(i2cInitFlag == enBooleanFalse)
		return enStatusNotConfigured;
	if(i2cChBusy == enBooleanTrue)
		return enStatusBusy;
		
#endif	// _DEBUG_

	i2cChBusy = enBooleanTrue;
	i2cSlaveAddr = slaveAddr;
	i2cChRWFlag = 1;
	i2cBytesToRead = size;
	
	if( i2cChInt != enBooleanFalse)           
	{	
#if(PF_I2C_USE_FIFO != 0)
			if(master != enBooleanTrue)
			{
				for(index = 0; index < size; index++)
				{
					if(pfFifoIsEmpty(&i2cRxFifo) == enBooleanTrue)
						break;
					*(data + index) = pfFifoPop(&i2cRxFifo);
				}
				*readBytes = index;
				return enStatusSuccess;
			}
		
			if(size > 1)
			{
				I2C_SET_AA();
			}
			else
			{
				I2C_CLR_AA();
			}
			I2C_SET_START();
			
			while(i2cChBusy == enBooleanTrue);
			
			I2C_CLR_AA();
	
			for(index = 0; index < i2cBytesRead; index++)
			{
				*(data + index) = pfFifoPop(&i2cRxFifo);
			}
			*readBytes = i2cBytesRead;
			i2cBytesRead = 0;
	
			if(i2cChError == enBooleanTrue)
			{
				i2cChError = enBooleanFalse;
				return enStatusError;
			}
#endif	// #if(PF_I2C_USE_FIFO != 0)
	}	
		
// Polling==========================================================================
	else
	{
 			status = pfDataProcessing(data,size);
			if(status != enStatusSuccess)
					return status;
			
			I2C_CLR_AA();		
			*readBytes = i2cBytesRead;
			i2cBytesRead = 0;
	}	
	return enStatusSuccess;
}


PFEnStatus pfI2cGetState(PFdword* state)
{
#ifdef PF_I2C_DEBUG
	if(i2cInitFlag == enBooleanFalse)
	{
		return enStatusNotConfigured;
	}
#endif 

	*state = I2C_CHANNEL->TWSR;
	return enStatusSuccess;
}


void INT_HANDLER(I2C_VECT)(void)
{
	PFbyte status = I2C_GET_STATE();
	switch(status)
	{
		//
		// Master mode
		//
		case I2C_M_START:
		case I2C_M_REP_START:
		I2C_CALL_SLAVE(i2cSlaveAddr, i2cChRWFlag);
		I2C_CLR_START();
		break;
		//
		// Master transmitter
		//
		case I2C_M_SLA_W_TX_ACK:
		case I2C_M_DATA_TX_ACK:
		if(pfFifoIsEmpty(&i2cTxFifo) != enBooleanTrue)
		{
			I2C_CHANNEL->TWDR = pfFifoPop(&i2cTxFifo);
		}
		else
		{
			i2cChError= enBooleanFalse;
			i2cChBusy = enBooleanFalse;
			I2C_SET_STOP();
		}
		break;
		
		//
		// Master transmitter error conditions
		//
		case I2C_M_ARB_LOST:
		case I2C_M_SLA_W_TX_NACK:
		case I2C_M_DATA_TX_NACK:
		i2cChError= enBooleanTrue;
		i2cChBusy = enBooleanFalse;
		I2C_SET_STOP();
		break;
		
		//
		// Master reciever
		//
		case I2C_M_SLA_R_NACK:
		i2cChError= enBooleanTrue;
		i2cChBusy = enBooleanFalse;
		I2C_SET_STOP();
		break;
		
		case I2C_M_SLA_R_ACK:
		// do nothing
		break;

		case I2C_M_DATA_RX_ACK:
		pfFifoPush(&i2cRxFifo, I2C_CHANNEL->TWDR);
		i2cBytesRead++;
		if(i2cBytesRead == i2cBytesToRead -1)
		{
			I2C_CLR_AA();
		}
		break;

		case I2C_M_DATA_RX_NACK:
		pfFifoPush(&i2cRxFifo,I2C_CHANNEL->TWDR);
		i2cBytesRead++;
		i2cChError= enBooleanFalse;
		i2cChBusy = enBooleanFalse;
		I2C_SET_STOP();
		break;
		
		//
		// Slave receiver
		//
		case I2C_S_SLA_W_RX_ACK:
		case I2C_S_ARB_LOST_SLA_W_RX:
		case I2C_S_GEN_CALL_RX_ACK:
		case I2C_S_ARB_LOST_GC_RX:
		case I2C_S_STOP_REP_START:
		//dataIndex[ch] = 0;
		break;

		case I2C_S_SLA_W_DATA_RX_ACK:
		case I2C_S_GC_DATA_RX_ACK:
		case I2C_S_SLA_W_DATA_RX_NACK:
		case I2C_S_GC_DATA_RX_NACK:
		pfFifoPush(&i2cRxFifo, I2C_CHANNEL->TWDR);
		break;
		
		//
		// Slave trasmitter
		//
		case I2C_S_SLA_R_RX_ACK:
		case I2C_S_ARB_LOST_SLA_R_RX:
		case I2C_S_DATA_TX_ACK:
		if(pfFifoIsEmpty(&i2cTxFifo) != enBooleanTrue)
		{
			I2C_CHANNEL->TWDR = pfFifoPop(&i2cTxFifo);
		}
		else
		{
			I2C_CHANNEL->TWDR = 0;
		}
		break;

		case I2C_S_DATA_TX_NACK:
		case I2C_S_LAST_DATA_TX_NACK:
		i2cChError= enBooleanFalse;
		i2cChBusy = enBooleanFalse;
		break;

		default:
		i2cChError= enBooleanTrue;
		i2cChBusy = enBooleanFalse;
		break;
	}

	I2C_CLR_INT();
}

#endif		//#if (PF_USE_I2C == 1)
