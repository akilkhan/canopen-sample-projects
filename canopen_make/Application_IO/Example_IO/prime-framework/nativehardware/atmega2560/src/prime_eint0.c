#include "prime_framework.h"
#if (PF_USE_EINT0 == 1)
#include "prime_eint0.h"


static PFcallback eint0CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint0MaxIntCount=0;
static PFbyte eint0InitFlag = 0;

PFEnStatus pfEint0Open(PFpCfgEint0 config)
{
	PFbyte index;
#ifdef PF_EINT0_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <= 3)
	{
		EXTINT->EICRA |=config->mode << (EINT0_CH * 2) ;
	}	
	
	eint0MaxIntCount = config->maxCallbacks ;
// 	if(eint2MaxIntCount >0)
// 	eint2CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint2MaxIntCount);
// 	else
// 	return enStatusNotSupported;
			
	for(index = 0; index < eint0MaxIntCount; index++)
	{
		if(config->callbackList != 0 )
		eint0CallBackList[index] = config->callbackList[index];
	}
	 eint0InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint0Enable(void)
{
#ifdef PF_EINT0_DEBUG
	if(eint0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT0_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint0Disable(void)
{
#ifdef PF_EINT0_DEBUG
	if(eint0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &= ~(1<<EINT0_CH);
	return enStatusSuccess;
}

PFEnStatus pfEint0AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT0_DEBUG
	if(eint0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint0MaxIntCount;index++)
	{
		if (eint0CallBackList[index]==0)
		{
			eint0CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint0RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT0_DEBUG
	if(eint0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint0MaxIntCount;index++)
	{
		if (eint0CallBackList[index]==callback)
		{
			eint0CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint0Close(void)
{
#ifdef PF_EINT0_DEBUG
	if(eint0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT0_CH);
	//free(eint0CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT0_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint0MaxIntCount;index++)
	{
		if(eint0CallBackList[index] !=0)
		eint0CallBackList[index]();
		
	}
}

#endif		//#if (PF_USE_EINT0 == 1)


