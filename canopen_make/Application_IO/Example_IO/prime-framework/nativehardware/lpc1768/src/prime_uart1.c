#include "prime_framework.h"

#if (PF_USE_UART1 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_uart1.h"


#if(UART1_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning UART1 FIFO is enabled for interrupt based communication
	#if( (UART1_BUFFER_SIZE == 0) || ((UART1_BUFFER_SIZE & (UART1_BUFFER_SIZE - 1)) != 0) )
		#error UART1_BUFFER_SIZE cannot be zero. UART1_BUFFER_SIZE should be power of 2
	#endif
#endif	// #if(UART1_USE_FIFO != 0)


/*
// Lookup table for clock dividers according baudrates.
static const PFUart1BaudrateDiv baudrateDividerLookup[] = 
{
	{514, 4, 15}, 		// 2400
	{257, 4, 15},		// 4800
	{92, 10, 13},		// 9600
	{46, 10, 13},		// 19200
	{37, 1, 10},		// 38400
	{19, 3, 7},		// 57600
	{10, 5, 14},		// 115200
};
 */ 
// Lookup tables for clock dividers according baudrates.
static const PFdword uart1BaudValue[] = {2400, 4800, 9600, 19200, 38400, 57600, 115200};
//static const PFdword uart1BaudPrescale[] = {514, 257, 92, 46, 37, 19, 10};
//static const PFbyte uart1BaudFractDiv[] = {4, 4, 10, 10, 1, 3, 5};
//static const PFbyte uart1BaudFractMul[] = {15, 15, 13, 13, 10, 7, 14};

static PFEnBoolean uart1ChInit = enBooleanFalse;		// initialize flag for the channel
static PFEnBoolean uart1ChBusy = enBooleanFalse;		// busy flag for the channel
static PFbyte uart1ChInt;							// interrupt flag for the channel
#if(UART1_USE_FIFO != 0)
static PFbyte uart1RxBuffer[UART1_BUFFER_SIZE];	// UART1 transmit buffer
static PFbyte uart1TxBuffer[UART1_BUFFER_SIZE];	// UART1 receive buffer
static PFFifo uart1TxFifo;							// UART1 transmit fifo structure
static PFFifo uart1RxFifo;							// UART1 receive fifo structure
#else	
static PFcallback uart1TxCallback = 0;					// transmit callback for the channel
static PFcallback uart1RxCallback = 0;					// receive callback for the channel
#endif	// #if(UART1_USE_FIFO != 0)
static PFCfgUart1 uart1Cfg;

static struct fract_div_entry
{
	PFfloat fr;
	PFbyte	div;
	PFbyte	mul;

} fract_divs[] =
{
	{ 1.000, 0, 1 }, 	{ 1.067, 1, 15 }, 	{ 1.071, 1, 14 }, 	{ 1.077, 1, 13 },
	{ 1.083, 1, 12 }, 	{ 1.091, 1, 11 }, 	{ 1.100, 1, 10 }, 	{ 1.111, 1, 9 },
	{ 1.125, 1, 8 }, 	{ 1.133, 2, 15 }, 	{ 1.143, 1, 7 }, 	{ 1.154, 2, 13 },
	{ 1.167, 1, 6 }, 	{ 1.182, 2, 11 }, 	{ 1.200, 1, 5 }, 	{ 1.214, 3, 14 },
	{ 1.222, 2, 9 }, 	{ 1.231, 3, 13 }, 	{ 1.250, 1, 4 }, 	{ 1.267, 4, 15 },
	{ 1.273, 3, 11 }, 	{ 1.286, 2, 7 }, 	{ 1.300, 3, 10 }, 	{ 1.308, 4, 13 },
	{ 1.333, 1, 3 }, 	{ 1.357, 5, 14 }, 	{ 1.364, 4, 11 }, 	{ 1.375, 3, 8 },
	{ 1.385, 5, 13 }, 	{ 1.400, 2, 5 }, 	{ 1.417, 5, 12 }, 	{ 1.429, 3, 7 },
	{ 1.444, 4, 9 }, 	{ 1.455, 5, 11 }, 	{ 1.462, 6, 13 }, 	{ 1.467, 7, 15 },
	{ 1.500, 1, 2 }, 	{ 1.533, 8, 15 }, 	{ 1.538, 7, 13 }, 	{ 1.545, 6, 11 },
	{ 1.556, 5, 9 }, 	{ 1.571, 4, 7 }, 	{ 1.583, 7, 12 }, 	{ 1.600, 3, 5 },
	{ 1.615, 8, 13 }, 	{ 1.625, 5, 8 }, 	{ 1.636, 7, 11 }, 	{ 1.643, 9, 14 },
	{ 1.667, 2, 3 }, 	{ 1.692, 9, 13 }, 	{ 1.700, 7, 10 }, 	{ 1.714, 5, 7 },
	{ 1.727, 8, 11 }, 	{ 1.733, 11, 15 }, 	{ 1.750, 3, 4 }, 	{ 1.769, 10, 13 },
	{ 1.778, 7, 9 }, 	{ 1.786, 11, 14 }, 	{ 1.800, 4, 5 }, 	{ 1.818, 9, 11 },
	{ 1.833, 5, 6 }, 	{ 1.846, 11, 13 }, 	{ 1.857, 6, 7 }, 	{ 1.867, 13, 15 },
	{ 1.875, 7, 8 }, 	{ 1.889, 8, 9 }, 	{ 1.900, 9, 10 }, 	{ 1.909, 10, 11 },
	{ 1.917, 11, 12 }, 	{ 1.923, 12, 13 },	{ 1.929, 13, 14 },	{ 1.933, 14, 15 },
};

PFdword pfUart1BaudrateCalc(PFdword pclk, PFdword baud, PFbyte *mul, PFbyte *div)
{
    PFfloat fr_est, curr_fr;
    uint16_t dl;
    uint32_t i;
	if( pclk % (16 * baud) == 0 )
	{
		*mul = 1;
		*div = 0;
		return (pclk / (16 * baud));
	}

	fr_est = 1.5;
	while(1)
	{
		dl = pclk / (16 * baud * fr_est);
		curr_fr = (PFfloat)pclk / (PFfloat)(16 * baud * dl);
		if(curr_fr <= 1.1)
                {
			fr_est += 0.1;
                }
		else if(curr_fr >= 1.9)
                {
			fr_est -= 0.1;
		}
                else
		{
			fr_est = curr_fr;
			break;
		}
	}

	for(i = 0; i < (sizeof(fract_divs)/sizeof(struct fract_div_entry)); i++)
	{
		if( fr_est <= fract_divs[i].fr )
		{
			if( i == 0 )
			{
				*mul = fract_divs[i].mul;
				*div = fract_divs[i].div;
			}
			else if( (fract_divs[i].fr - fr_est) <= (fr_est - fract_divs[i-1].fr) )
			{
				*mul = fract_divs[i].mul;
				*div = fract_divs[i].div;
			}
			else
			{
				*mul = fract_divs[i-1].mul;
				*div = fract_divs[i-1].div;
			}
			return dl;
		}
	}

	*mul = 0;
	*div = 0;

	return 0;
}

PFEnStatus pfUart1Open(PFpCfgUart1 config)
{
	PFdword temp;
	PFdword uart1Pclk, baudDl;
	PFbyte baudFractMul, baudFractDiv;
#if (PF_UART1_DEBUG == 1)
	// Validate config pointer
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if(config->databits > 3)
	{
		return enStatusInvArgs;
	}
#if(UART1_USE_FIFO == 0)
	if( ((config->interrupts & enUart1IntRx) != 0) && (config->receiveCallback == PF_NULL) )
	{
		return enStatusInvArgs;
	}
	if( ((config->interrupts & enUart1IntTx) != 0) && (config->transmitCallback == PF_NULL) )
	{
		return enStatusInvArgs;
	}
#endif	// #if(UART1_USE_FIFO == 0)
#endif	// #if (PF_UART1_DEBUG == 1)	

	// power on UART1 peripheral
	POWER_ON(UART1_CH);
	// set peripheral clock divider
	pfSysSetPclkDiv(PCLK_DIV(UART1_CH), config->clkDiv);
	
	// configure UART1 channel
	temp = (config->databits & 0x03) | ((config->stopBits & 0x01) << 2) | ((config->parity & 0x07) << 3) | (BIT_MASK_7);
	UART1_CHANNEL->LCR = temp;					// set databits, stop bits and parity and enable divisor latch
	
	// set baudrate
        uart1Pclk = pfSysGetPclk(PCLK_DIV(UART1_CH));
        baudDl = pfUart1BaudrateCalc(uart1Pclk, uart1BaudValue[config->baudrate], &baudFractMul, &baudFractDiv);
        if(baudDl == 0)
        {
            return enStatusNotSupported;
        }
        UART1_CHANNEL->DLL = baudDl & 0xFF;
	UART1_CHANNEL->DLM = (baudDl >> 8) & 0xFF;
	UART1_CHANNEL->FDR = ((baudFractDiv & 0x0F) | ((baudFractMul << 4) & 0xF0));
        
	//UART1_CHANNEL->DLL = uart1BaudPrescale[config->baudrate] & 0xFF;
	//UART1_CHANNEL->DLM = (uart1BaudPrescale[config->baudrate] >> 8) & 0xFF;
	//UART1_CHANNEL->FDR = ((uart1BaudFractDiv[config->baudrate] & 0x0F) | ((uart1BaudFractMul[config->baudrate] << 4) & 0xF0));
	
	// disable divisor latch
	UART1_CHANNEL->LCR &= INV_BIT_MASK_7;

#if(UART1_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&uart1TxFifo, uart1TxBuffer, UART1_BUFFER_SIZE);
	pfFifoInit(&uart1RxFifo, uart1RxBuffer, UART1_BUFFER_SIZE);
#else	
	// set transmit and receive user callbacks
	uart1TxCallback = config->transmitCallback;
	uart1RxCallback = config->receiveCallback;
#endif	// #if(UART1_USE_FIFO != 0)
	
	// set UART1 interrupts
	if(config->interrupts != enUart1IntNone)
	{
		UART1_CHANNEL->IER = config->interrupts;
		uart1ChInt = config->interrupts;
		
		NVIC_EnableIRQ(IRQ_NUM(UART1_CH));
		uart1ChInt |= 0x80;
	}
	
	pfMemCopy(&uart1Cfg, config, sizeof(PFCfgUart1));
	// set initialize flag and clear busy flag
	uart1ChInit = enBooleanTrue;
	uart1ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

void pfUart1Close(void)
{
	uart1ChInit = enBooleanFalse;
	uart1ChBusy = enBooleanFalse;
	// power off UART1 peripheral
	POWER_OFF(UART1_CH);
}

PFEnStatus pfUart1WriteByte(PFbyte data)
{
#if (PF_UART1_DEBUG == 1) 	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart1ChInit);
#endif	// #if (PF_UART1_DEBUG == 1)	
	// check if the channel is busy
	if(uart1ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	UART1_CHANNEL->THR = data;
	while((UART1_CHANNEL->LSR & (BIT_MASK_5)) == 0);
	return enStatusSuccess;
}

PFEnStatus pfUart1Write(PFbyte* data, PFdword size)
{
	PFdword index;
#if (PF_UART1_DEBUG == 1)	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart1ChInit);
        CHECK_NULL_PTR(data);
#endif	// #if (PF_UART1_DEBUG == 1)
	// check if the channel is busy
	if(uart1ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart1ChBusy = enBooleanTrue;
	// if fifo is to be used push data into the fifo
	if((uart1ChInt & enUart1IntTx) != 0)
	{
	#if(UART1_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&uart1TxFifo, *(data + index));
		}
		UART1_CHANNEL->THR = pfFifoPop(&uart1TxFifo);
	#else
		UART1_CHANNEL->THR = *data;
	#endif	// #if(UART1_USE_FIFO != 0)	
		//UART1_CHANNEL->IER = uart1ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			UART1_CHANNEL->THR = *(data + index);
			while((UART1_CHANNEL->LSR & (BIT_MASK_5)) == 0);
		}
	}
	
	uart1ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfUart1WriteString(PFbyte* data)
{
	PFdword index;
#if (PF_UART1_DEBUG == 1)	
	// check if the channel is initialized
	CHECK_DEV_INIT(uart1ChInit);
        CHECK_NULL_PTR(data);
#endif	// #if (PF_UART1_DEBUG == 1)
	// check if the channel is busy
	if(uart1ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart1ChBusy = enBooleanTrue;
	// if fifo is to be used push data into the fifo
	if((uart1ChInt & enUart1IntTx) != 0)
	{
	#if(UART1_USE_FIFO != 0)
		for(index = 0; *(data + index) != 0; index++)
		{
			pfFifoPush(&uart1TxFifo, *(data + index));
		}
		UART1_CHANNEL->THR = pfFifoPop(&uart1TxFifo);
	#else
		UART1_CHANNEL->THR = *data;
	#endif	// #if(UART1_USE_FIFO != 0)	
		//UART1_CHANNEL->IER = uart1ChInt & (0x7F);
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; *(data + index) != 0; index++)
		{
			UART1_CHANNEL->THR = *(data + index);
			while((UART1_CHANNEL->LSR & (BIT_MASK_5)) == 0);
		}
	}
	
	uart1ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

PFEnStatus pfUart1ReadByte(PFbyte* rxByte)
{
	PFbyte rdByte;
#if (PF_UART1_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
    CHECK_NULL_PTR(rxByte);
#endif	// #if (PF_UART1_DEBUG == 1)	
	if(uart1ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}	
	while((UART1_CHANNEL->LSR & 0x01) == 0);
	rdByte = UART1_CHANNEL->RBR;
	
	*rxByte = rdByte;
	return enStatusSuccess;
}

PFEnStatus pfUart1Read(PFbyte* data, PFdword size, PFdword* readBytes)
{
	PFdword index;
#if (PF_UART1_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
        CHECK_NULL_PTR(data);
        CHECK_NULL_PTR(readBytes);
#endif	// #if (PF_UART1_DEBUG == 1)	
	// check if channel is busy
	if(uart1ChBusy != enBooleanFalse)
	{
		return enStatusBusy;
	}
	// set channel busy	
	uart1ChBusy = enBooleanTrue;
	// reset read byte count
	*readBytes = 0;
	
	if((uart1ChInt & enUart1IntRx) != 0)
	{
	#if(UART1_USE_FIFO != 0)
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&uart1RxFifo) == enBooleanTrue)
			{
				break;
			}
			*(data + index) = pfFifoPop(&uart1RxFifo);
			*readBytes += 1;
		}
	#else
		uart1ChBusy = enBooleanFalse;
		{
			return enStatusUnknown;
		}
	#endif	// #if(UART1_USE_FIFO != 0)	
	}
	else
	{
		for(index = 0; index < size; index++)
		{
			while((UART1_CHANNEL->LSR & 0x01) == 0);
			*(data + index) = UART1_CHANNEL->RBR;
			*readBytes += 1;
		}
	}
	uart1ChBusy = enBooleanFalse;
	return enStatusSuccess;
}

#if(UART1_USE_FIFO != 0)
PFEnStatus pfUart1GetRxBufferSize(PFdword* size)
{
#if (PF_UART3_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_UART1_DEBUG == 1)    
    *size = pfFifoLength(&uart1RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart1GetRxBufferCount(PFdword* count)
{
#if (PF_UART1_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
        CHECK_NULL_PTR(count);
#endif	// #if (PF_UART3_DEBUG == 1)    
    *count = uart1RxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfUart1RxBufferFlush(void)
{
#if (PF_UART1_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
#endif	// #if (PF_UART1_DEBUG == 1)    
    pfFifoFlush(&uart1RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart1GetTxBufferSize(PFdword* size)
{
#if (PF_UART1_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_UART1_DEBUG == 1)    
    *size = pfFifoLength(&uart1TxFifo);
    return enStatusSuccess;
}

PFEnStatus pfUart1GetTxBufferCount(PFdword* count)
{
#if (PF_UART1_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
    CHECK_NULL_PTR(count);
#endif	// #if (PF_UART1_DEBUG == 1)    
    *count = uart1TxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfUart1TxBufferFlush(void)
{
#if (PF_UART1_DEBUG == 1)
	CHECK_DEV_INIT(uart1ChInit);
#endif	// #if (PF_UART1_DEBUG == 1)    
    pfFifoFlush(&uart1TxFifo);
    return enStatusSuccess;
}
#endif	// #if(UART1_USE_FIFO != 0)

void UART1_INT_HANDLER(void)
{
	PFbyte rxByte;
	PFdword intStat = UART1_CHANNEL->IIR;	//pfUart1GetIntStatus();

	if((intStat & 0x04) != 0)		// Data received interrupt
	{
	#if(UART1_USE_FIFO != 0)
		rxByte = UART1_CHANNEL->RBR;
		pfFifoPush(&uart1RxFifo, rxByte);
	#else
		if(uart1RxCallback != 0)
		{
			uart1RxCallback();
		}
		return;
	#endif		// #if(UART1_USE_FIFO != 0)
	}

	if((intStat & 0x02) != 0)		// THR Reg empty interrypt
	{
	#if(UART1_USE_FIFO != 0)
		if(pfFifoIsEmpty(&uart1TxFifo) != enBooleanTrue)
		{
			UART1_CHANNEL->THR = pfFifoPop(&uart1TxFifo);
		}
	#else	
		if(uart1TxCallback != 0)
		{
			uart1TxCallback();
		}
		return;
	#endif		// #if(UART1_USE_FIFO != 0)	
	}
}

#endif	// #if (PF_USE_UART1 == 1)
