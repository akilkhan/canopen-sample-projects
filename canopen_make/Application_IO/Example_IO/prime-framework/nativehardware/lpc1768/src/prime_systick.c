#include "prime_framework.h"

#if (PF_USE_SYSTICK == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_systick.h"

static PFcallback systickCallback = 0;
static PFdword systick = 0;
static PFEnBoolean systickInit = enBooleanFalse;
static PFCfgSystick systickCfg;

PFEnStatus pfSystickOpen(PFpCfgSystick config)
{
#if (PF_SYSTICK_DEBUG == 1)
    CHECK_NULL_PTR(config);
#endif	// #if (PF_SYSTICK_DEBUG == 1)
	// select clock source
	if(config->clkSource == enSystickClkCpu)
	{
		SysTick->CTRL |= BIT_MASK_2;
	}
	else
	{
		SysTick->CTRL &= INV_BIT_MASK_2;
	}
	// load count
	SysTick->LOAD = config->compareValue;
	// set systick ISR callback
	systickCallback = config->callback;
	// interrupt enable
	SysTick->CTRL |= BIT_MASK_1;
	// enable
	SysTick->CTRL |= BIT_MASK_0;
	
        pfMemCopy(&systickCfg, config, sizeof(PFCfgSystick));
        systickInit = enBooleanTrue;
	return enStatusSuccess;
}

void pfSystickClose(void)
{
	SysTick->CTRL &= INV_BIT_MASK_1;
}

PFEnStatus pfSystickGetCount(PFdword* count)
{
#if (PF_SYSTICK_DEBUG == 1)
    CHECK_DEV_INIT(systickInit);
#endif	// #if (PF_SYSTICK_DEBUG == 1)    
	*count = systick;
	return enStatusSuccess;
}

void SysTick_Handler(void)
{
	systick++;
	
	if(systickCallback != 0)
        {
		systickCallback();
        }
}

#endif	// #if (PF_USE_SYSTICK == 1)
