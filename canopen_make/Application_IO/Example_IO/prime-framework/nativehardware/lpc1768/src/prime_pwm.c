#include "prime_framework.h"

#if (PF_USE_PWM == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_pwm.h"


static PFEnBoolean pwmInit = enBooleanFalse;
static PFcallback pwmCallback = 0;
static PFCfgPwm pwmCfg;

PFEnStatus pfPwmOpen(PFpCfgPwm config)
{
	PFbyte loop;
#if (PF_PWM_DEBUG == 1)
	PFbyte dgbLoop;
	// Validate config pointer
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if( (config->mode == 5) || (config->mode > 8) )
	{
		return enStatusInvArgs;
	}
	for(dgbLoop = 0; dgbLoop < 6; dgbLoop++)
	{	
		if( (config->matchAction[dgbLoop] > 7) || (config->chMode[dgbLoop] > 3) )
		{
			return enStatusInvArgs;
		}
	}
	if( (config->interrupt == enBooleanTrue) && (config->callback == 0) )
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_PWM_DEBUG == 1)	
	// power on
	POWER_ON(PWM_CH);
	// set PCLK clock divider
	pfSysSetPclkDiv(PCLK_DIV(PWM_CH), config->clkDiv);

	// set prescaler
	PWM_CHANNEL->PR = config->prescaler-1;
	// set mode for PWM module
	if(config->mode == enPwmModePwm)
	{
		PWM_CHANNEL->TCR |= BIT_MASK_3;
		PWM_CHANNEL->CTCR = 0;
	}
	else
	{
		PWM_CHANNEL->CTCR = config->mode - 1;
	}
	//set match register values
	PWM_CHANNEL->MR0 = config->matchReg[0];
	PWM_CHANNEL->MR1 = config->matchReg[1];
	PWM_CHANNEL->MR2 = config->matchReg[2];
	PWM_CHANNEL->MR3 = config->matchReg[3];
	PWM_CHANNEL->MR4 = config->matchReg[4];
	PWM_CHANNEL->MR5 = config->matchReg[5];
	PWM_CHANNEL->MR6 = config->matchReg[6];
	// update latch
	PWM_CHANNEL->LER = 0x7F;
	
	for(loop = 0; loop < 7; loop++)
	{
		// set match action
		PWM_CHANNEL->MCR &= ~(0x07 << (loop * 3));
		PWM_CHANNEL->MCR |= (config->matchAction[loop] << (loop * 3));
		
		// set channel mode
		if(loop > 0)
		{
			if(config->chMode[loop-1] != enPwmChannelModeDisable)
			{
				if(config->chMode[loop-1] == enPwmChannelModeDoubleEdge)		// double edge
				{
					PWM_CHANNEL->PCR |= (1 << loop);
				}
				else												// single edge
				{
					PWM_CHANNEL->PCR &= ~(1 << loop);
				}
				PWM_CHANNEL->PCR |= (1 << (8 + loop));
			}
		}
	}
	
	// enable interrupt
	if(config->interrupt == enBooleanTrue)
	{
		// set callback
		pwmCallback = config->callback;
		
		NVIC_EnableIRQ(PWM1_IRQn);
	}
	
	pfMemCopy(&pwmCfg, config, sizeof(PFCfgPwm));
	pwmInit = enBooleanTrue;
	return enStatusSuccess;
}

void pfPwmClose(void)
{
	POWER_OFF(PWM_CH);
	pwmInit = enBooleanFalse;
}

PFEnStatus pfPwmStart(void)
{
#if (PF_PWM_DEBUG == 1)	
    CHECK_DEV_INIT(pwmInit);
#endif	// #if (PF_PWM_DEBUG == 1)	
	PWM_CHANNEL->TCR &= ~(3 << 0);
	PWM_CHANNEL->TCR |= BIT_MASK_0;
	return enStatusSuccess;
}

PFEnStatus pfPwmStop(void)
{
#if (PF_PWM_DEBUG == 1)	
	CHECK_DEV_INIT(pwmInit);
#endif	// #if (PF_PWM_DEBUG == 1)	
	PWM_CHANNEL->TCR &= ~(BIT_MASK_0);
	return enStatusSuccess;
}

PFEnStatus pfPwmReset(void)
{
#if (PF_PWM_DEBUG == 1)	
	CHECK_DEV_INIT(pwmInit);
#endif	// #if (PF_PWM_DEBUG == 1)
	PWM_CHANNEL->TCR |= (BIT_MASK_1);
	PWM_CHANNEL->TCR &= ~(BIT_MASK_1);
	return enStatusSuccess;
}

PFEnStatus pfPwmGetRepititionCount(PFdword* repRateVal)
{
#if (PF_PWM_DEBUG == 1)	
	CHECK_DEV_INIT(pwmInit);
	CHECK_NULL_PTR(repRateVal);
#endif	// #if (PF_PWM_DEBUG == 1)
	*repRateVal = PWM_CHANNEL->MR0;
	return enStatusSuccess;
}

PFEnStatus pfPwmGetRepititionFreq(PFdword* repFreq)
{
	PFdword pwmPclk;
#if (PF_PWM_DEBUG == 1)	
	CHECK_DEV_INIT(pwmInit);
	CHECK_NULL_PTR(repFreq);
#endif	// #if (PF_PWM_DEBUG == 1)
	pwmPclk = pfSysGetPclk(PCLK_DIV(PWM_CH));
	*repFreq = pwmPclk / ( (PWM_CHANNEL->PR+1) * PWM_CHANNEL->MR0);
	return enStatusSuccess;
}

PFEnStatus pfPwmGetTickFreq(PFdword* tickClk)
{
	PFdword pwmPclk;
#if (PF_PWM_DEBUG == 1)	
	CHECK_DEV_INIT(pwmInit);
	CHECK_NULL_PTR(tickClk);
#endif	// #if (PF_PWM_DEBUG == 1)
	pwmPclk = pfSysGetPclk(PCLK_DIV(PWM_CH));
	*tickClk = pwmPclk / (PWM_CHANNEL->PR+1);
	return enStatusSuccess;
}

PFEnStatus pfPwmUpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#if (PF_PWM_DEBUG == 1)	
	CHECK_DEV_INIT(pwmInit);
	if(regNum > 6)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_PWM_DEBUG == 1)
	switch(regNum)
	{
		case 0:
			PWM_CHANNEL->MR0 = regVal;
			break;

		case 1:
			PWM_CHANNEL->MR1 = regVal;
			break;

		case 2:
			PWM_CHANNEL->MR2 = regVal;
			break;

		case 3:
			PWM_CHANNEL->MR3 = regVal;
			break;

		case 4:
			PWM_CHANNEL->MR4 = regVal;
			break;

		 case 5:
			PWM_CHANNEL->MR5 = regVal;
			break;

		 case 6:
			PWM_CHANNEL->MR6 = regVal;
			break;
	}
	// update latch
	PWM_CHANNEL->LER |= (1 << regNum);
	return enStatusSuccess;
}

PFdword pfPwmGetIntStatus(void)
{
#if (PF_PWM_DEBUG == 1)	
	CHECK_DEV_INIT(pwmInit);
#endif	// #if (PF_PWM_DEBUG == 1)
	return PWM_CHANNEL->IR;
}

void PWM_INT_HANDLER(void)
{
	if(pwmCallback != 0)
	{
		pwmCallback();
	}
}

#endif	// #if (PF_USE_PWM == 1)
