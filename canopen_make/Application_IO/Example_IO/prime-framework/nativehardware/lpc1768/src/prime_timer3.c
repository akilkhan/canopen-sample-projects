#include "prime_framework.h"

#if (PF_USE_TIMER3 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_timer3.h"


static PFcallback timer3Callback = 0;
static PFEnBoolean timer3Init = enBooleanFalse;
static PFCfgTimer3 timer3Cfg;

PFEnStatus pfTimer3Open(PFpCfgTimer3 config)
{
	PFbyte loop;
#if (PF_TIMER3_DEBUG ==1)
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if( (config->timer3Mode == 3) || (config->timer3Mode > 5) )
	{
		return enStatusInvArgs;
	}
	if( (config->interrupt == enBooleanTrue) && (config->callback == 0) )
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_TIMER3_DEBUG ==1)	
	// power on timer3 module
	POWER_ON(TIMER3_CH);
	
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(TIMER3_CH), config->clkDiv);
	
	// set prescaler
	TIMER3_CHANNEL->PR = config->prescaler-1;
	
	// set timer3 counter mode, with counter pin selection
	TIMER3_CHANNEL->CTCR = config->timer3Mode;
	
	// set match register count
	TIMER3_CHANNEL->MR0 = config->matchValue[0];
	TIMER3_CHANNEL->MR1 = config->matchValue[1];
	TIMER3_CHANNEL->MR2 = config->matchValue[2];
	TIMER3_CHANNEL->MR3 = config->matchValue[3];
	
	for(loop = 0; loop < 4; loop++)
	{
		// set match action	
		TIMER3_CHANNEL->MCR |= ( (config->matchAction[loop] & 0x07) << (loop * 3));
		
		// set pin action for count match
		TIMER3_CHANNEL->EMR |= ( (config->exMatchAction[loop] & 0x03) << ((loop * 2) + 4));
	}
	
	timer3Callback = config->callback;
	
	if(config->interrupt != enBooleanFalse)
	{
		NVIC_EnableIRQ(IRQ_NUM(TIMER3_CH));
	}
	
	pfMemCopy(&timer3Cfg, config, sizeof(PFCfgTimer3));
	timer3Init = enBooleanTrue;
	return enStatusSuccess;
}

void pfTimer3Close(void)
{
	timer3Init = enBooleanFalse;
	// power off timer3 module
	POWER_OFF(TIMER3_CH);
}

PFEnStatus pfTimer3Start(void)
{
#if (PF_TIMER3_DEBUG ==1)
        CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG ==1)	
	TIMER3_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer3Stop(void)
{
#if (PF_TIMER3_DEBUG ==1)
	CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG ==1)	
	TIMER3_CHANNEL->TCR = 2;
	return enStatusSuccess;
}

PFEnStatus pfTimer3Reset(void)
{
#if (PF_TIMER3_DEBUG == 1)
	CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG ==1)	
	TIMER3_CHANNEL->TCR = 2;
	TIMER3_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer3UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#if (PF_TIMER3_DEBUG == 1)
	CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG == 1)	
	switch(regNum)
	{
		case 0:
			TIMER3_CHANNEL->MR0 = regVal;
			break;

		case 1:
			TIMER3_CHANNEL->MR1 = regVal;
			break;

		case 2:
			TIMER3_CHANNEL->MR2 = regVal;
			break;

		case 3:
			TIMER3_CHANNEL->MR3 = regVal;
			break;
		
		default:
			return enStatusInvArgs;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer3ReadCount(PFdword* count)
{
#if (PF_TIMER3_DEBUG == 1)
	CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG == 1)	
        *count = TIMER3_CHANNEL->TC;
	return enStatusSuccess;
}

PFEnStatus pfTimer3GetIntStatus(PFdword* status)
{
#if (PF_TIMER3_DEBUG == 1)
        CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG == 1)	
	*status = TIMER3_CHANNEL->IR;
	return enStatusSuccess;
}

PFEnStatus pfTimer3ClearIntStatus(PFdword intStatus)
{
#if (PF_TIMER3_DEBUG == 1)
	CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG == 1)	
	TIMER3_CHANNEL->IR = intStatus;
	return enStatusSuccess;
}

void TIMER3_INT_HANDLER(void)
{
	if(timer3Callback != 0)
	{
		timer3Callback();
	}
	TIMER3_CHANNEL->IR = TIMER3_CHANNEL->IR;
}

PFEnStatus pfTimer3IntDisable(void)
{
#if (PF_TIMER3_DEBUG ==1)
        CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG ==1)
	NVIC_EnableIRQ(IRQ_NUM(TIMER3_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer3IntEnable(void)
{
#if (PF_TIMER3_DEBUG ==1)
        CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG ==1)
	NVIC_DisableIRQ(IRQ_NUM(TIMER3_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer3GetTickFreq(PFdword* tickFreq)
{
	PFdword timerPclk;
#if (PF_TIMER3_DEBUG == 1)	
	CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG == 1)
	timerPclk = pfSysGetPclk(PCLK_DIV(TIMER3_CH));
	*tickFreq = timerPclk / (TIMER3_CHANNEL->PR + 1);
	return enStatusSuccess;
}

PFEnStatus pfTimer3SetTickFreq(PFdword tickFreq)
{
	PFdword f_pclk, prescale;
	PFdword pclk[4] = {25000000, 100000000, 50000000, 12500000}	;
	PFbyte pclkInd, done = 0;
#if (PF_TIMER3_DEBUG == 1)	
	CHECK_DEV_INIT(timer3Init);	
#endif	// #if (PF_TIMER3_DEBUG == 1)	
	for(pclkInd = 0; pclkInd < 4; pclkInd++)
	{
		prescale = pclk[pclkInd] / tickFreq;
		if ((tickFreq * prescale) == pclk[pclkInd])
		{
			done = 1;
			pfSysSetPclkDiv(PCLK_DIV(TIMER3_CH), (PFEnPclkDivider)pclkInd);
			TIMER3_CHANNEL->PR = prescale-1;
			break;
		}
	}
	
	if(done == 1)
	{
		return enStatusSuccess;
	}
	else
	{
		return enStatusNotSupported;
	}
}

PFEnStatus pfTimer3UpdateMatchControlRegister(PFbyte regNum,PFEnTimer3MatchAction matchAction)
{
#if (PF_TIMER3_DEBUG == 1)	
	if((regNum >3) || (matchAction > 7)) 
        return enStatusInvArgs;
#endif	// #if (PF_TIMER3_DEBUG == 1)
    
    TIMER3_CHANNEL->MCR |= ( (matchAction & 0x07) << (regNum * 3));
	return enStatusSuccess;
}

#endif	// #if (PF_USE_TIMER3 == 1)
