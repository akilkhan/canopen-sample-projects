#include "prime_framework.h"

#if (PF_USE_SPI1 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_spi1.h"


#if(SPI1_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning SPI1 FIFO is enabled for interrupt based communication
	#if( (SPI1_BUFFER_SIZE == 0) || ((SPI1_BUFFER_SIZE & (SPI1_BUFFER_SIZE - 1)) != 0) )
		#error SPI1_BUFFER_SIZE cannot be zero. SPI1_BUFFER_SIZE should be power of 2
	#endif
#endif

static PFEnBoolean spi1ChInit = enBooleanFalse;		// initialize flag for the channel
static PFEnBoolean spi1ChBusy = enBooleanFalse;		// busy flag for the channel
static PFEnBoolean spi1ChMaster;						// master/slave mode
static PFbyte spi1ChInt = 0;						// interrupt flag for the channel
#if(SPI1_USE_FIFO != 0)
#define SPI1_STATE_IDLE		0
#define SPI1_STATE_READ		1
#define SPI1_STATE_WRITE		2
static PFbyte spi1ChState = SPI1_STATE_IDLE;					// read write flag	
static PFbyte spi1RxBuffer[SPI1_BUFFER_SIZE];		// SPI1 transmit buffer
static PFbyte spi1TxBuffer[SPI1_BUFFER_SIZE];		// SPI1 receive buffer
static PFFifo spi1TxFifo;							// SPI1 transmit fifo structure
static PFFifo spi1RxFifo;							// SPI1 receive fifo structure
#else	
static PFcallback spi1Callback = 0;					// callback for the channel interrupt
#endif	// #if(SPI1_USE_FIFO != 0)
static PFEnBoolean spi1RegDevice[SPI1_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};	
static PFGpioPortPin spi1ChipSelect[SPI1_MAX_DEVICE_SUPPORTED] = {{0, 0}};
static PFEnBoolean spi1ChipSelectAssert[SPI1_MAX_DEVICE_SUPPORTED] = {enBooleanFalse};	
static PFbyte spi1RegDeviceCount = 0;
static PFCfgSpi1 spi1Cfg;
	
PFEnStatus pfSpi1Open(PFpCfgSpi1 config)
{
	PFdword baudrateVal = 0;
#if (PF_SPI1_DEBUG == 1)
    CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
        {
		return enStatusInvArgs;
        }
	if(config->mode > 3)
        {
		return enStatusInvArgs;
        }
#if(SPI1_USE_FIFO == 0)
	if( (config->interrupt != enSpi1IntNone) && (config->callback == PF_NULL_PTR) )
        {
		return enStatusInvArgs;
        }
#endif	// #if(SPI1_USE_FIFO == 0)
#endif	// #if (PF_SPI1_DEBUG == 1)
	// power on 
	POWER_ON(SPI1_CH);
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(SPI1_CH), config->clkDiv);
		
	// set datasize
	SPI1_CHANNEL->CR0 &= 0xFFFFFFF0;
	SPI1_CHANNEL->CR0 |= config->datasize;
	
	// set mode
	SPI1_CHANNEL->CR0 &= ~(0x03 << 6);
	SPI1_CHANNEL->CR0 |= config->mode << 6;
	// set master/slave
	if(config->masterMode == enBooleanTrue)
	{
		SPI1_CHANNEL->CR1 &= INV_BIT_MASK_2;
	}
	else
	{
		SPI1_CHANNEL->CR1 |= BIT_MASK_2;
	}
	// set baudrate
	baudrateVal = pfSysGetPclk(PCLK_DIV(SPI1_CH)) / config->baudrate;
	SPI1_CHANNEL->CPSR = baudrateVal;
	
#if(SPI1_USE_FIFO != 0)
	// initialize fifo 
	pfFifoInit(&spi1TxFifo, spi1TxBuffer, SPI1_BUFFER_SIZE);
	pfFifoInit(&spi1RxFifo, spi1RxBuffer, SPI1_BUFFER_SIZE);
#else	
	// set callback
	spi1Callback = config->callback;
#endif	// #if(SPI1_USE_FIFO != 0)
	// set interrupts
	if(config->interrupt != enSpi1IntNone)
	{
		spi1ChInt = config->interrupt;
		SPI1_CHANNEL->IMSC = config->interrupt;
		NVIC_EnableIRQ((IRQn_Type)IRQ_NUM(SPI1_CH));
	}	
	
	// enable SPI1 channel
	SPI1_CHANNEL->CR1 |= BIT_MASK_1;
	
	spi1ChMaster = config->masterMode;
        pfMemCopy(&spi1Cfg, config, sizeof(PFCfgSpi1));
	spi1ChInit = enBooleanTrue;
	return enStatusSuccess;
}

void pfSpi1Close(void)
{
	POWER_OFF(SPI1_CH);
	spi1ChInit = enBooleanFalse;
}

PFEnStatus pfSpi1RegisterDevice(PFbyte* id, PFpGpioPortPin chipSelect)
{
	PFbyte index;
#if (PF_SPI1_DEBUG == 1)
        CHECK_DEV_INIT(spi1ChInit);
        CHECK_NULL_PTR(id);
#endif	// #if (PF_SPI1_DEBUG == 1)
	if(spi1RegDeviceCount >= SPI1_MAX_DEVICE_SUPPORTED)
	{
		return enStatusNoMem;
	}
	for(index = 0; index < SPI1_MAX_DEVICE_SUPPORTED; index++)
	{
		if(spi1RegDevice[index] == enBooleanFalse)
		{
			spi1RegDevice[index] = enBooleanTrue;
			spi1RegDeviceCount++;
			spi1ChipSelect[index].port = chipSelect->port;
			spi1ChipSelect[index].pin = chipSelect->pin;
			*id = index;
			pfGpioPinsSet(chipSelect->port, chipSelect->pin);
			return enStatusSuccess;
		}
	}
	return enStatusError;
}

PFEnStatus pfSpi1UnregisterDevice(PFbyte* id)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
	CHECK_NULL_PTR(id);
	if(*id >= SPI1_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_SPI1_DEBUG == 1)
	
	spi1RegDevice[*id] = enBooleanFalse;
	spi1RegDeviceCount--;
	return enStatusSuccess;
}

PFEnStatus pfSpi1ChipSelect(PFbyte* id, PFbyte pinStatus)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
	CHECK_NULL_PTR(id);
	if(*id >= SPI1_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
	if(spi1RegDevice[*id] != enBooleanTrue)
	{
		return enStatusError;
	}
#endif	// #if (PF_SPI1_DEBUG == 1)	
	
	if(pinStatus == 0)
	{
		if(spi1ChBusy == enBooleanTrue)
		{
			return enStatusBusy;
		}
		pfGpioPinsClear(spi1ChipSelect[*id].port, spi1ChipSelect[*id].pin);
		spi1ChipSelectAssert[*id] = enBooleanTrue;	
		spi1ChBusy = enBooleanTrue;
	}
	else
	{
		pfGpioPinsSet(spi1ChipSelect[*id].port, spi1ChipSelect[*id].pin);
		spi1ChipSelectAssert[*id] = enBooleanFalse;	
		spi1ChBusy = enBooleanFalse;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfSpi1ExchangeByte(PFbyte *id, PFword data, PFword* rxData)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
	CHECK_NULL_PTR(id);
	if(*id >= SPI1_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_SPI1_DEBUG == 1)
// 	if(spi1ChipSelectAssert[*id] != enBooleanTrue)
// 	{
// 		return enStatusError;
// 	}
	
	// wait if transmit FIFO is full
	while( (SPI1_CHANNEL->SR & (1 << 1)) != (1 << 1) );
	SPI1_CHANNEL->DR = data;
	// wait till data is transmitted
	while( (SPI1_CHANNEL->SR & (1 << 4)) == (1 << 4) );
	*rxData = SPI1_CHANNEL->DR;
	return enStatusSuccess;
}

PFEnStatus pfSpi1Write(PFbyte* id, PFbyte* data, PFdword size)
{
	PFdword index;
	PFword dummyRead;
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
	CHECK_NULL_PTR(id);
	CHECK_NULL_PTR(data);
	if(*id >= SPI1_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_SPI1_DEBUG == 1)
	if(spi1ChipSelectAssert[*id] != enBooleanTrue)
	{
		return enStatusError;
	}
	
	// if fifo is enabled, push data into the fifo
	if(spi1ChInt != enSpi1IntNone)
	{
	#if(SPI1_USE_FIFO != 0)
		// set state
		spi1ChState = SPI1_STATE_WRITE;
		
		for(index = 0; index < size; index++)
		{
			pfFifoPush(&spi1TxFifo, *(data + index));
		}
		while( (SPI1_CHANNEL->SR & (1 << 2)) != 0 )
		{
			dummyRead = SPI1_CHANNEL->DR;
		}
		SPI1_CHANNEL->DR = pfFifoPop(&spi1TxFifo);
	#else
		SPI1_CHANNEL->DR = *data;
	#endif	// #if(SPI1_USE_FIFO != 0)	
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			SPI1_CHANNEL->DR = *(data + index);
			while( (SPI1_CHANNEL->SR & (1 << 4)) == (1 << 4) );
			dummyRead = SPI1_CHANNEL->DR;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfSpi1Read(PFbyte* id, PFbyte* data, PFdword size, PFdword* readBytes)
{
	PFdword index;
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
	CHECK_NULL_PTR(id);
	CHECK_NULL_PTR(data);
	CHECK_NULL_PTR(readBytes);
	if(*id >= SPI1_MAX_DEVICE_SUPPORTED)
	{
		return enStatusInvArgs;
	}
	if(spi1RegDevice[*id] != enBooleanTrue)
	{
		return enStatusError;
	}
#endif	// #if (PF_SPI1_DEBUG == 1)
	// reset read byte count
	*readBytes = 0;
	
	if(spi1ChInt != enSpi1IntNone)
	{
	// if fifo is to be used, push data into the fifo	
	#if(SPI1_USE_FIFO != 0)
		// set state
		spi1ChState = SPI1_STATE_WRITE;
		
		if(spi1ChMaster == enBooleanTrue)
		{
			// for master mode, push dummy data in buffer
			for(index = 0; index < size; index++)
			{
				pfFifoPush(&spi1TxFifo, 0);
			}
			SPI1_CHANNEL->DR = pfFifoPop(&spi1TxFifo);
			// wait for all bytes to transmit
			while(pfFifoIsEmpty(&spi1TxFifo) != enBooleanTrue);
		}
		// read receive fifo
		for(index = 0; index < size; index++)
		{
			if(pfFifoIsEmpty(&spi1RxFifo) == enBooleanTrue)
				break;

			*(data + index) = pfFifoPop(&spi1RxFifo);
			*readBytes += 1;
		}
		spi1ChState = SPI1_STATE_IDLE;
	#else
		if(spi1ChMaster == enBooleanTrue)
		{
			SPI1_CHANNEL->DR = *data;
		}
		else
		{
			return enStatusUnknown;
		}
	#endif	// #if(SPI1_USE_FIFO != 0)	
	}
	else
	{
		// for polling based transmission, write each byte one by one to data register
		for(index = 0; index < size; index++)
		{
			SPI1_CHANNEL->DR = 0;
			while( (SPI1_CHANNEL->SR & (1 << 4)) == (1 << 4) );
			*(data + index) = SPI1_CHANNEL->DR;
			*readBytes += 1;
		}
	}
	
	return enStatusSuccess;
}

PFdword pfSpi1GetIntStatus(void)
{
	return SPI1_CHANNEL->MIS;
}

PFEnStatus pfSpi1ClearIntStatus(PFdword intStatus)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
#endif	// #if (PF_SPI1_DEBUG == 1)
	SPI1_CHANNEL->MIS = intStatus;
	return enStatusSuccess;
}

#if (SPI1_USE_FIFO != 0)

PFEnStatus pfSpi1GetRxBufferSize(PFdword* size)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_SPI1_DEBUG == 1)    
    *size = pfFifoLength(&spi1RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfSpi1GetRxBufferCount(PFdword* count)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
        CHECK_NULL_PTR(count);
#endif	// #if (PF_SPI3_DEBUG == 1)    
    *count = spi1RxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfSpi1RxBufferFlush(void)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
#endif	// #if (PF_SPI1_DEBUG == 1)    
    pfFifoFlush(&spi1RxFifo);
    return enStatusSuccess;
}

PFEnStatus pfSpi1GetTxBufferSize(PFdword* size)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_SPI1_DEBUG == 1)    
    *size = pfFifoLength(&spi1TxFifo);
    return enStatusSuccess;
}

PFEnStatus pfSpi1GetTxBufferCount(PFdword* count)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
    CHECK_NULL_PTR(count);
#endif	// #if (PF_SPI1_DEBUG == 1)    
    *count = spi1TxFifo.count;
    return enStatusSuccess;
}

PFEnStatus pfSpi1TxBufferFlush(void)
{
#if (PF_SPI1_DEBUG == 1)
	CHECK_DEV_INIT(spi1ChInit);
#endif	// #if (PF_SPI1_DEBUG == 1)    
    pfFifoFlush(&spi1TxFifo);
    return enStatusSuccess;
}

#endif	// #if (SPI1_USE_FIFO != 0)

void SPI1_INT_HANDLER(void)
{
	PFword dummyRead;
#if(SPI1_USE_FIFO != 0)
// 	if(spi1ChState == SPI1_STATE_IDLE)
// 		return;
	// for write operation
	if(spi1ChState == SPI1_STATE_WRITE)
	{
		dummyRead = SPI1_CHANNEL->DR;
		if(pfFifoIsEmpty(&spi1TxFifo) == enBooleanTrue)
		{
			spi1ChState = SPI1_STATE_IDLE;
			return;
		}
		else
			SPI1_CHANNEL->DR = pfFifoPop(&spi1TxFifo);
	}
	else		// for read operation
	{
		if(spi1ChMaster == enBooleanTrue)
		{
			pfFifoPush(&spi1RxFifo, SPI1_CHANNEL->DR);
			if( pfFifoIsEmpty(&spi1TxFifo) != enBooleanTrue )
			{
				SPI1_CHANNEL->DR = pfFifoPop(&spi1TxFifo);
			}
		}
		else		// slave mode
		{
			while( (SPI1_CHANNEL->SR & (1 << 2)) != 0 )
			{
				pfFifoPush(&spi1RxFifo, SPI1_CHANNEL->DR);
				//SPI1_CHANNEL->DR = 0;
			}
		}
	}
#else	
	if(spi1Callback != PF_NULL_PTR)
	{
		spi1Callback();
		return;
	}
#endif	// #if(SPI1_USE_FIFO != 0)	
}

#endif	// #if (PF_USE_SPI1 == 1)
