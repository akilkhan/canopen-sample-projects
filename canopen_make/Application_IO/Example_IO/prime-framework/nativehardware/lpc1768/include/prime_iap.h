/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Flash IAP driver for LPC17xx.
 * 
 *
 * Review status: NO
 *
 */
#pragma once

/**
 * \defgroup PF_IAP_API IAP API
 * @{
 */ 
 

/** IAP entry location */
#define PF_IAP_ADDR              (0x1FFF1FF1UL)


/**
 * \brief IAP command code definitions
 */
typedef enum
{
    enIapCmdPrepare = 50,    		/**< Prepare sector(s) for write operation		*/
    enIapCmdCopyRamToFlash = 51,    /**< Copy RAM to Flash							*/
    enIapCmdErase = 52,             /**< Erase sector(s)							*/
    enIapCmdBlankCheck = 53,        /**< Blank check sector(s)						*/
    enIapCmdReadPartId = 54,       	/**< Read chip part ID							*/
    enIapCmdReadBootVersion = 55,   /**< Read chip boot code version				*/
    enIapCmdCompare = 56,           /**< pfIapCompare memory areas						*/
    enIapCmdReinvokeIsp = 57,       /**< Reinvoke ISP								*/
    enIapCmdReadSerialNum = 58, 	/**< Read serial number							*/
}PFEnIapCmd;//  IAP_COMMAND_CODE;

/**
 * \brief IAP status code definitions
 */
typedef enum
{
    enIapStatusCmdSuccess = 0,    				/**< Command is executed successfully.		*/
    enIapStatusInvCmd,             				/**< Invalid command.		*/
    enIapStatusSrcAddrErr,        				/**< Source address is not on a word boundary.		*/
    enIapStatusDstAddrErr,        				/**< Destination address is not on a correct boundary.		*/
    enIapStatusSrcAddrNotMapped, 				/**< Source address is not mapped in the memory map.		*/
    enIapStatusDstAddrNotMapped,   				/**< Destination address is not mapped in the memory map.		*/
    enIapStatusCountErr,          				/**< Byte count is not multiple of 4 or is not a permitted value.		*/
    enIapStatusInvSector,     					/**< Sector number is invalid.		*/
    enIapStatusSectorNotBlank,    				/**< Sector is not blank.		*/
    enIapStatusSectorNotReady,					/**< Command to prepare sector for write operation was not executed.		*/
    enIapStatusCompareErr,         				/**< Source and destination data is not same.		*/
	enIapStatusBusy,              				/**< Flash programming hardware interface is busy.		*/
}PFEnIapStatus;// PFEnIapStatus;

/**
 * \brief IAP write length definitions
 */
typedef enum {
  enIapWriteSize256  = 256,
  enIapWriteSize512  = 512,
  enIapWriteSize1024 = 1024,
  enIapWriteSize4096 = 4096,
} PFEnIapWriteSize;//PFEnIapWriteSize;

/**
 * \brief IAP command structure
 */
typedef struct {
    PFdword cmd;   			// Command
    PFdword param[4];      	// Parameters
    PFdword status;        	// status code
    PFdword result[4];     	// Result
}PFIapCommand;



/**  Get sector number of an address */
PFdword pfIapGetSecNum (PFdword adr);
/**  Prepare sector(s) for write operation */
PFEnIapStatus pfIapPrepareSector(PFdword start_sec, PFdword end_sec);
/**  Copy RAM to Flash */
PFEnIapStatus pfIapCopyRamToFlash(PFbyte * dest, PFbyte* source, PFEnIapWriteSize size);
/**  Prepare sector(s) for write operation */
PFEnIapStatus pfIapEraseSector(PFdword start_sec, PFdword end_sec);
/**  Blank check sectors */
PFEnIapStatus pfIapBlankCheckSector(PFdword start_sec, PFdword end_sec, PFdword *first_nblank_loc, PFdword *first_nblank_val);
/**  Read part identification number */
PFEnIapStatus pfIapReadPartId(PFdword *partID);
/**  Read boot code version */
PFEnIapStatus pfIapReadBootCodeVer(PFbyte *major, PFbyte* minor);
/**  Read Device serial number */
PFEnIapStatus pfIapReadDeviceSerialNum(PFdword *uid);
/**  pfIapCompare memory */
PFEnIapStatus pfIapCompare(PFbyte *addr1, PFbyte *addr2, PFdword size);
/**  Invoke ISP */
void pfIapInvokeIsp(void);


/**
 * @}
 */
 

