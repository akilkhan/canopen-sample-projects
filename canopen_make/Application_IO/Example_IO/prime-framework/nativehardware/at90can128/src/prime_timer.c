/*
 * pfTimer.c
 *
 * Created: 26-12-2013 13:31:40
 *  Author: Admin
 */ 
#include "prime_framework.h"
#include "prime_timer.h"

static PFEnStatus timer8InitFlag=0;
static PFcallback timer8cmpMatchACallback;
static PFcallback timer8cmpMatchBCallback;
static PFcallback timer8overFlowCallback;

PFEnStatus pfTimerOpen(PPFCfgTimer config)
{
#ifdef PFTIMER8_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->timerMode<0) || ((config->timerMode > enTimerCtcMode)))
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	
	if(config->cmpMatchACallback ==0)
	{
		return enStatusInvArgs;
	}
	
	if(config->overFlowCallback ==0)
	{
		return enStatusInvArgs;
	}		
#endif
	
	if(config->timerMode !=0)
	{
		TIMER8_CHANNEL->TCCRA |=config->timerMode;
	}	
	TIMER8_CHANNEL->OCRA =config->matchValueA;
	
	//TIMSK(TIMER8_CHANNEL) = config->interrupt;
	if(config->exMatchActionA !=enExtMatchCtrlNone)
	{
		TIMER8_CHANNEL->TCCRA |=config->exMatchActionA << 4;
	}
		
			
	if(config->cmpMatchACallback !=0)
	{
		timer8cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer8overFlowCallback =config->overFlowCallback;
	}
	if(config->interrupt !=enTimerIntNone)
	{
		//TIMER8->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER2 = config->interrupt;
	}
	if(config->clockSource <=7)
	{
		TIMER8_CHANNEL->TCCRA |= config->clockSource;
	}
	
	
	timer8InitFlag = enBooleanTrue;

	return enStatusSuccess;
	
}

PFEnStatus pfTimerClose(void)
{
#ifdef PFTIMER8_DEBUG
    if(timer8InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	TIMER8_CHANNEL->TCCRA = 0x00;
	TIMER8_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}

PFEnStatus pfTimerStart(void)
{
	PFbyte val;
#ifdef PFTIMER8_DEBUG
    if(timer8InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	val=TIMER8_CHANNEL->TCCRA;
	TIMER8_CHANNEL->TCCRA = 0x00;
	TIMER8_CHANNEL->TCCRA |= val;
	TIMER8_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimerReset(void)
{
#ifdef PFTIMER8_DEBUG
    if(timer8InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif

	TIMER8_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimerReadCount(PFbyte* data)
{
#ifdef PFTIMER8_DEBUG
    if(timer8InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	*data =TIMER8_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimerUpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#ifdef PFTIMER8_DEBUG
    if(timer8InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif
	switch(regNum)
	{
		case 0:	
			TIMER8_CHANNEL->OCRA =regVal;
			break;
		
	}
}

//ISR(TIMER2_COMPA_vect)
void INT_HANDLER(TIMER2_COMPA)(void)
//INT_HANDLER(TIMER2_COMPA)
{
	if(timer8cmpMatchACallback !=0)
	timer8cmpMatchACallback();
}



//ISR(TIMER2_OVF_vect)
void INT_HANDLER(TIMER2_OVF)(void)
//INT_HANDLER(TIMER2_OVF)
{
	if(timer8overFlowCallback !=0)
	timer8overFlowCallback();
}