#include "prime_framework.h"
#if(PF_USE_PWM2 == 1)	
	#include "prime_pwm2.h"

static PFcallback pwm2cmpMatchACallback;
static PFcallback pwm2overFlowCallback;
static PFbyte pwm2initFlag=0;
static PFbyte pwm2startFlag=0;
static PFbyte pwm2Value=0;


PFEnStatus pfPwm2Open(PPpCfgPwm2 config)
{
#ifdef PF_PWM2_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->pwm2Mode<0) || ((config->pwm2Mode > enPwmFastMode)))
	{
		return enStatusInvArgs;
	}
	
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	
	if(config->cmpMatchACallback ==0)
	{
		return enStatusInvArgs;
	}
	
	if(config->overFlowCallback ==0)
	{
		return enStatusInvArgs;
	}	
#endif	//PF_PWM2_DEBUG

	if((config->pwm2Modeode& 0x01)==0x01)
	{
		PWM2_CHANNEL->TCCRA |=(1<<3);
	}
	else if((config->pwm2Mode& 0x03)==0x03)
	{
		PWM2_CHANNEL->TCCRA |=((1<<3) |(1<<6));
	}
	
	PWM2_CHANNEL->OCRA =config->matchValueA;
	
	
	if(config->exMatchActionA !=enPwm2ExtMatchCtrlNone)
	{
		PWM2_CHANNEL->TCCRA |=config->exMatchActionA <<4;
	//	GPIO_PORTB->DDR |=(1<<7);
	}		
	if(config->cmpMatchACallback !=0)
	{
		pwm2cmpMatchACallback =config->cmpMatchACallback;
	}	
		
	if(config->overFlowCallback !=0)
	{
		pwm2overFlowCallback =config->overFlowCallback;
	}
	if(config->clockSource >=0 && config->clockSource <=7)
	{
		PWM2_CHANNEL->TCCRA |= config->clockSource;
	}
	if(config->interrupt !=enPwm2IntNone)
	{
		//	PWM2_CHANNEL->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER0 |= config->interrupt;	
	}	
	PWM2_CHANNEL->TCNT = 0;
	pwm2initFlag=1;
	return enStatusSuccess;
	
}

PFEnStatus pfPwm2Close(void)
{
#ifdef PF_PWM2_DEBUG
	if(pwm2initFlag ==0)
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM2_DEBUG
	PWM2_CHANNEL->TCCRA = 0x00;
	PWM2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
		
}

PFEnStatus pfPwm2Stop(void)
{
#ifdef PF_PWM2_DEBUG
	if((pwm2initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM2_DEBUG
	pwm2startFlag=PWM2_CHANNEL->TCCRA;
	pwm2Value=PWM2_CHANNEL->TCNT;
	PWM2_CHANNEL->TCCRA = 0x00;
	return enStatusSuccess;

}

PFEnStatus pfPwm2Start(void)
{
	PFbyte val;
	
#ifdef PF_PWM2_DEBUG
	if((pwm2initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM2_DEBUG	
	if(pwm2startFlag!=0)
	{
		PWM2_CHANNEL->TCCRA = pwm2startFlag;
		PWM2_CHANNEL->TCNT = pwm2Value;
	}
	else
	{
		val =PWM2_CHANNEL->TCCRA;
		PWM2_CHANNEL->TCCRA = 0x00;
		PWM2_CHANNEL->TCCRA =val;
		PWM2_CHANNEL->TCNT = 0;
	}
	return	enStatusSuccess;	
	
}
PFEnStatus pfPwm2Reset(void)
{
#ifdef PF_PWM2_DEBUG
	if((pwm2initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM2_DEBUG
	PWM2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}
PFEnStatus pfPwm2ReadCount(PFbyte *data)
{
#ifdef PF_PWM2_DEBUG
	if((pwm2initFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	//PF_PWM2_DEBUG
	*data =PWM2_CHANNEL->TCNT;
	return enStatusSuccess;
	
}

//ISR(TIMER0_COMPA_vect)
void INT_HANDLER(TIMER2_COMPA)(void)
//INT_HANDLER(TIMER0_COMPA)
{
	if(pwm2cmpMatchACallback !=0)
	pwm2cmpMatchACallback();
}


////ISR(TIMER0_OVF_vect)
void INT_HANDLER(TIMER2_OVF)(void)
//INT_HANDLER(TIMER0_OVF)
{
	if(pwm2overFlowCallback !=0)
	pwm2overFlowCallback();
}


PFEnStatus pfPwm2UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
	switch(regNum)
	{
		case 0:
		PWM2_CHANNEL->OCRA =regVal;
		break;
		
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}
#endif		// #if (PF_USE_PWM2 == 1)