#include "prime_framework.h"
#if (PF_USE_TIMER2 == 1)
#include "prime_timer2.h"

static PFEnStatus timer2InitFlag=0;
static PFcallback timer2cmpMatchACallback;
static PFcallback timer2cmpMatchBCallback;
static PFcallback timer2overFlowCallback;

PFEnStatus pfTimer2Open(PFpCfgTimer2 config)
{
#ifdef PFTIMER2_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->timer2Mode<0) || ((config->timer2Mode > enTimer2CtcMode)))
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER2_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}

	if( ((config->interrupt & TIMER2_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}
#endif
	
	if(config->timer2Mode !=0)
	{
		TIMER2_CHANNEL->TCCRA |=config->timer2Mode;
	}	
	TIMER2_CHANNEL->OCRA =config->matchValueA;
	
	//TIMSK(TIMER2_CHANNEL) = config->interrupt;
	if(config->exMatchActionA !=enTimer2ExtMatchCtrlNone)
	{
		TIMER2_CHANNEL->TCCRA |=config->exMatchActionA << 4;
	}
		
			
	if(config->cmpMatchACallback !=0)
	{
		timer2cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer2overFlowCallback =config->overFlowCallback;
	}
	if(config->interrupt !=TIMER2_INT_NONE)
	{
		//TIMER2->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER2 = config->interrupt;
	}
	if(config->clockSource <=7)
	{
		TIMER2_CHANNEL->TCCRA |= config->clockSource;
	}
	
	timer2InitFlag = enBooleanTrue;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer2Close(void)
{
#ifdef PFTIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	TIMER2_CHANNEL->TCCRA = 0x00;
	TIMER2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}

PFEnStatus pfTimer2Start(void)
{
	PFbyte val;
#ifdef PFTIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	val=TIMER2_CHANNEL->TCCRA;
	TIMER2_CHANNEL->TCCRA = 0x00;
	TIMER2_CHANNEL->TCCRA |= val;
	TIMER2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer2Reset(void)
{
#ifdef PFTIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif

	TIMER2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer2ReadCount(PFbyte* data)
{
#ifdef PFTIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	*data =TIMER2_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer2UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#ifdef PFTIMER2_DEBUG
    if(timer2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif
	switch(regNum)
	{
		case 0:	
			TIMER2_CHANNEL->OCRA =regVal;
			break;
		
	}
}

PFEnStatus pfTimer2IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER2_DEBUG
    if(interrupt && (~TIMER2_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER2 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer2IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER2_DEBUG
    if(interrupt && (~TIMER2_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER2 &= (~interrupt);
	return enStatusSuccess;	
}


void INT_HANDLER(TIMER2_COMPA)(void)
{
	if(timer2cmpMatchACallback !=0)
	timer2cmpMatchACallback();
}

void INT_HANDLER(TIMER2_COMPB)(void)
{
	if(timer2cmpMatchBCallback !=0)
	timer2cmpMatchBCallback();
}

void INT_HANDLER(TIMER2_OVF)(void)
{
	if(timer2overFlowCallback !=0)
	timer2overFlowCallback();
}
#endif		//#if (PF_USE_TIMER2 == 1)