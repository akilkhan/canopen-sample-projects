#include "prime_framework.h"
#if (PF_USE_TIMER0 == 1)
#include "prime_timer0.h"

static PFEnStatus timer0InitFlag=0;
static PFcallback timer0cmpMatchACallback;
static PFcallback timer0overFlowCallback;

PFEnStatus pfTimer0Open(PFpCfgTimer0 config)
{
#ifdef PF_TIMER0_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->timer0Mode< enTimer0NormalMode ) || ((config->timer0Mode > enTimer0CtcMode)))
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER0_INT_MATCHREG_A) != 0) && (config->cmpMatchACallback == 0) )
	{
		return enStatusInvArgs;
	}
	
	if( ((config->interrupt & TIMER0_INT_OVERFLOW) != 0) && (config->overFlowCallback == 0) )
	{
		return enStatusInvArgs;
	}		
#endif		
	
	if(config->timer0Mode !=0)
	{
		TIMER0_CHANNEL->TCCRA |=config->timer0Mode;
	}	
	TIMER0_CHANNEL->OCRA =config->matchValueA;
	
	//TIMSK(TIMER0_CHANNEL) = config->interrupt;
	if(config->exMatchActionA !=enTimer0ExtMatchCtrlNone)
	{
		TIMER0_CHANNEL->TCCRA |=config->exMatchActionA << 4;
	}
		
			
	if(config->cmpMatchACallback !=0)
	{
		timer0cmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->overFlowCallback !=0)
	{
		timer0overFlowCallback =config->overFlowCallback;
	}
	if(config->interrupt != TIMER0_INT_NONE)
	{
		//TIMER0->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER0 = config->interrupt;
	}
	if(config->clockSource <=7)
	{
		TIMER0_CHANNEL->TCCRA |= config->clockSource;
	}
	
	
	timer0InitFlag = enBooleanTrue;

	return enStatusSuccess;
	
}

PFEnStatus pfTimer0Close(void)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	TIMER0_CHANNEL->TCCRA = 0x00;
	TIMER0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}

PFEnStatus pfTimer0Start(void)
{
	PFbyte val;
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif

	val=TIMER0_CHANNEL->TCCRA;
	TIMER0_CHANNEL->TCCRA = 0x00;
	TIMER0_CHANNEL->TCCRA |= val;
	TIMER0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer0Reset(void)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif

	TIMER0_CHANNEL->TCNT = 0;
	return enStatusSuccess;
}
PFEnStatus pfTimer0ReadCount(PFbyte* data)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}	
#endif
	*data =TIMER0_CHANNEL->TCNT;
	return enStatusSuccess;
}

PFEnStatus pfTimer0UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#ifdef PF_TIMER0_DEBUG
    if(timer0InitFlag == 0)
	{
		return enStatusNotConfigured;
	}		
#endif
	switch(regNum)
	{
		case 0:	
			TIMER0_CHANNEL->OCRA =regVal;
			break;
		
	}
}

PFEnStatus pfTimer0IntEnable(PFbyte interrupt)
{
#ifdef PF_TIMER0_DEBUG
    if(interrupt && (~TIMER0_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER0 |= interrupt;
	return enStatusSuccess;	
}

PFEnStatus pfTimer0IntDisable(PFbyte interrupt)
{
#ifdef PF_TIMER0_DEBUG
    if(interrupt && (~TIMER0_INT_ALL))
	{
		return enStatusInvArgs;
	}
#endif
	TIMSK_INT->TIMER0 &= (~interrupt);
	return enStatusSuccess;	
}


void INT_HANDLER(TIMER0_COMPA)(void)

{
	if(timer0cmpMatchACallback !=0)
	timer0cmpMatchACallback();
}



//ISR(TIMER02_OVF_vect)
void INT_HANDLER(TIMER0_OVF)(void)
//INT_HANDLER(TIMER02_OVF)
{
	if(timer0overFlowCallback !=0)
	timer0overFlowCallback();
}

#endif		// #if (PF_USE_TIMER0 == 1)