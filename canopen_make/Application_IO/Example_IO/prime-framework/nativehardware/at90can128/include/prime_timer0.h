/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework 8-bit Timer0 driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
#pragma once
/**
 * \defgroup PF_TIMER0_API Timer0 API
 * @{
 */ 
 
 #define TIMER0_CH				TIMER0
 #define TIMER0_CHANNEL			PERIPH(TIMER0_CH)
 
 
/** Enumeration for timer0 modes			*/
typedef enum
{
	enTimer0Noclock = 0,				/**No clock source is selected and the timer0 is disabled	*/
	enTimer0NoprescaledClk,				/**< clock source is selected and the timer0 is not pre-scaled	*/
	enTimer0ClkDivideby8,				/**< Clock source frequency is divided by 8	*/
	enTimer0ClkDivideby64,				/**< Clock source frequency is divided by 8	*/
	enTimer0ClkDivideby256,				/**< Clock source frequency is divided by 8	*/
	enTimer0ClkDivideby1024,			/**< Clock source frequency is divided by 8	*/
	enTimer0ExtClkFallingEdge,			/**<  timer0 with external source on falling edge 	*/
	enTimer0ExtClkRisingEdge			/**<  timer0 with external source on falling edge 	*/
}PFEnTimer0Clocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enTimer0ExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enTimer0ExtMatchCtrlTogglePin,				/**< Toggle match pin								*/
	enTimer0ExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enTimer0ExtMatchCtrlSetPin					/**< Set match pin									*/
}PFEnTimer0ExtMatchCtrl;

typedef enum
{
	enTimer0NormalMode,		/**<  timer0 in Normal Mode 	*/
	enTimer0CtcMode=0x02	/**<  timer0 in clear timer0 on compare  Mode 	*/
	
}PFEnTimer0Mode;

/**	No interrupt	*/
#define TIMER0_INT_NONE			0x00
/**	interrupt on overflow	*/
#define TIMER0_INT_OVERFLOW		0x01
/**	interrupt on matching with the value in OCR0A 	*/
#define TIMER0_INT_MATCHREG_A	0x02
/**	interrupt on matching with the value in OCR0A ,OCR0B and overflow 	*/
#define TIMER0_INT_ALL			0x07

/**		Timer0 configure structure		*/
typedef struct
{
	PFEnTimer0Clocksource	clockSource;		/**< Select clock source					*/
	PFEnTimer0Mode			timer0Mode;			/**< timer0 mode								*/
	PFbyte					matchValueA;		/**< Match register A compare value			*/
	PFEnTimer0ExtMatchCtrl 	exMatchActionA;		/**< match pin control on count match 		*/ 
	PFbyte					interrupt;			/**< To enable or disable timer0 interrupt	*/
	PFcallback				cmpMatchACallback;	/**< Callback function for timer0 ISR		*/
	PFcallback				overFlowCallback;
}PFCfgTimer0;

/** Pointer to PFCfgTiemr structure		*/
typedef PFCfgTimer0* PFpCfgTimer0;

/**
 * Initialized timer0 with given parameters
 * 
 * \param config timer0 configuration structure
 * 
 * \return timer0 initialization status
 */
PFEnStatus pfTimer0Open(PFpCfgTimer0 config);

/**
 * Stops timer0 operation and turn offs the timer0 module
 * 
 * \return timer0 turn off operation status
 */
PFEnStatus pfTimer0Close(void);

/**
 * Starts timer0 operation
 * 
 * \return timer0 start status
 */
PFEnStatus pfTimer0Start(void);

/**
 * Stops timer0 operation
 * 
 * \return timer0 stop status
 */
PFEnStatus pfTimer0Stop(void);

/**
 * Resets the timer0 operation. Timer0 will start counting from zero again.
 * 
 * \return timer0 reset status
 */
PFEnStatus pfTimer0Reset(void);

/**
 * Enables timer interrupt.
 * 
 * \param interrupt value to enable specific timer interrupt.
 * 
 * \return timer interrupt enable status.
 */
PFEnStatus pfTimer0IntEnable(PFbyte interrupt);

/**
 * Disables timer interrupt.
 * 
 * \param interrupt value to disable specific timer interrupt.
 * 
 * \return timer interrupt disable status.
 */
PFEnStatus pfTimer0IntDisable(PFbyte interrupt);


/**
 * Returns the timer0 count
 * 
 * \return timer0 count
 */
PFEnStatus pfTimer0ReadCount(PFbyte* data);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimer0UpdateMatchRegister(PFbyte regNum, PFdword regVal); 



/** @} */


