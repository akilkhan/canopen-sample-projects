#ifndef PRIMEDCP_INT_H_
#define PRIMEDCP_INT_H_

#include "prime_dcppacket.h"

PF_EXTERN_C_BEGIN



PFpDcpPacketBuf     pfDcpPacketAlloc(PFpDcpContext pCtx, PFdcpfxmemsize uPayloadSz);
void                pfDcpPacketFree(PFpDcpContext pCtx, PFpDcpPacketBuf pPacketBuf);
void                pfDcpIncomPacketFree(PFpDcpContext pCtx, PFpDcpIncomPacketBuf pIncPacketBuf);
PFpDcpOutEndpoint   PFDcpOutEpAlloc(PFpDcpContext pCtx);
void                pfDcpOutEpFree(PFpDcpContext pCtx, PFpDcpOutEndpoint pEp);
PFpDcpInEndpoint    pfDcpInEpAlloc(PFpDcpServiceContext pServ, PFdcpSeqNum uStartSeqNum);
void                pfDcpInEpFree(PFpDcpServiceContext pServ, PFpDcpInEndpoint pEp);
PFEnStatus          pfDcpWriteCompleted(PFpgeneric pPriv, PFpgeneric pPackDesc);
PFEnStatus          pfDcpRxHandler(PFpgeneric pArg, PFpDcpL2DevDesc pL2Dev, PFpdcpHwAddr pHwAddr, 
                                        PFpdcpfxmemptr pBuf, PFdcpfxmemsize uBufSz);
PFEnStatus          pfDcpSpecPacketSend(PFpDcpServiceContext pServ, PFpDcpL2DevDesc pL2Dev,
                                                PFpDcpIncomPacketBuf pIncPackBuf, PFbyte uId);
PFEnStatus          pfDcpRejectSend(PFpDcpContext pCtx, PFpDcpL2DevDesc pL2Dev, PFpDcpIncomPacketBuf pIncPackBuf, 
                                            PFdcpServiceId uServId);
PFuint16            pfDcpCrc16(PFpuint8 pHdr, PFEnBoolean bHdrNetOrder, PFpuint8 pBuf, PFdcpfxmemsize uLen);
PF_EXTERN_C_END

#endif /*PRIMEDCP_INT_H_*/
