//done
/**
 * Microcontroller specific code for CANopenNode nonvolatile variables.
 */

#include "prime_CO_driver.h"
#include "prime_CO_SDO.h"
#include "prime_CO_Emergency.h"
#include "prime_eeprom.h"
#include "prime_crc16-ccitt.h"

/**
 * OD function for accessing _Store parameters_ (index 0x1010) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
 
static PFEnCoSdoAbortCode pfCoODF_1010(PFCoODFarg *ODF_arg);
static PFEnCoSdoAbortCode pfCoODF_1010(PFCoODFarg *ODF_arg)
{
    PFCoEe *ee;
    PFdword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    ee = (PFCoEe*) ODF_arg->object;
    value = pfCoGetUint32(ODF_arg->data);

    if(!ODF_arg->reading)
	{
        /* don't change the old value */
        pfCoMemcpy(ODF_arg->data, (const PFbyte*)ODF_arg->ODdataStorage, 4U);

        if(ODF_arg->subIndex == 1U)
		{
            if(value == 0x65766173UL)
			{
                /* write ee->OD_ROMAddress, ee->OD_ROMSize to eeprom (blocking function) */

                /* verify data */
                if(0/*error*/)
				{
                    ret = enCO_SDO_AB_HW;
                }
            }
            else
			{
                ret = enCO_SDO_AB_DATA_TRANSF;
            }
        }
    }

    return ret;
}


/**
 * OD function for accessing _Restore default parameters_ (index 0x1011) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODF_1011(PFCoODFarg *ODF_arg);
static PFEnCoSdoAbortCode pfCoODF_1011(PFCoODFarg *ODF_arg)
{
    PFCoEe *ee;
    PFdword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    ee = (PFCoEe*) ODF_arg->object;
    value = pfCoGetUint32(ODF_arg->data);

    if(!ODF_arg->reading)
	{
        /* don't change the old value */
        pfCoMemcpy(ODF_arg->data, (const PFbyte*)ODF_arg->ODdataStorage, 4U);

        if(ODF_arg->subIndex >= 1U)
		{
            if(value == 0x64616F6CUL)
			{
                /* Clear the eeprom */

            }
            else
			{
                ret = enCO_SDO_AB_DATA_TRANSF;
            }
        }
    }

    return ret;
}


/******************************************************************************/
PFEnCoReturnError pfCoEeInit_1(
        PFCoEe                *ee,
        PFbyte                *OD_EEPROMAddress,
        PFdword                OD_EEPROMSize,
        PFbyte                *OD_ROMAddress,
        PFdword                OD_ROMSize)
{

    /* Configure eeprom */


    /* configure object variables */
    ee->OD_EEPROMAddress = OD_EEPROMAddress;
    ee->OD_EEPROMSize = OD_EEPROMSize;
    ee->OD_ROMAddress = OD_ROMAddress;
    ee->OD_ROMSize = OD_ROMSize;
    ee->OD_EEPROMCurrentIndex = 0U;
    ee->OD_EEPROMWriteEnable = enBooleanFalse;

    /* read the CO_OD_EEPROM from EEPROM, first verify, if data are OK */

    /* read the CO_OD_ROM from EEPROM and verify CRC */

    return enCO_ERROR_NO;
}


/******************************************************************************/
void pfCoEeInit_2(
        PFCoEe                *ee, 
        PFEnCoReturnError eeStatus,
        PFCoSdo               *SDO,
        PFCoEm                *em)
{
    pfCoODConfigure(SDO, OD_H1010_STORE_PARAM_FUNC,pfCoODF_1010, (void*)ee, 0, 0U);
    pfCoODConfigure(SDO, OD_H1011_REST_PARAM_FUNC, pfCoODF_1011, (void*)ee, 0, 0U);
    if(eeStatus != enCO_ERROR_NO)
	{
        pfCoErrorReport(em, CO_EM_NON_VOLATILE_MEMORY, CO_EMC_HARDWARE, (PFdword)eeStatus);
    }
}

/******************************************************************************/
void pfCoEeProcess(PFCoEe *ee)
{
    if((ee != 0) && (ee->OD_EEPROMWriteEnable)/* && !isWriteInProcess()*/)
	{
        PFdword i;
        PFbyte RAMdata, eeData;

        /* verify next word */
        if(++ee->OD_EEPROMCurrentIndex == ee->OD_EEPROMSize)
		{
            ee->OD_EEPROMCurrentIndex = 0U;
        }
        i = ee->OD_EEPROMCurrentIndex;

        /* read eeprom */
        RAMdata = ee->OD_EEPROMAddress[i];
        eeData = 0/*EE_readByte(i)*/;

        /* if bytes in EEPROM and in RAM are different, then write to EEPROM */
        if(eeData != RAMdata)
		{
            /* EE_writeByteNoWait(RAMdata, i);*/
        }
    }
}
