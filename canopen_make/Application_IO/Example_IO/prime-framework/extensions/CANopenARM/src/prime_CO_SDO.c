/*
 * CANopen Service Data Object - server.
 */
#include "prime_framework.h"
#include "prime_CO_driver.h"
#include "prime_CO_SDO.h"
#include "prime_crc16-ccitt.h"
#include "prime_CO_OD.h"

/* Client command specifier, see DS301 */
#define CCS_DOWNLOAD_INITIATE          1U
#define CCS_DOWNLOAD_SEGMENT           0U
#define CCS_UPLOAD_INITIATE            2U
#define CCS_UPLOAD_SEGMENT             3U
#define CCS_DOWNLOAD_BLOCK             6U
#define CCS_UPLOAD_BLOCK               5U
#define CCS_ABORT                      0x80U


#if CO_SDO_BUFFER_SIZE < 7
    #error CO_SDO_BUFFER_SIZE must be greater than 7
#endif


/* Helper functions. **********************************************************/
void pfCoMemcpy(PFbyte dest[], const PFbyte src[], const PFword size)
{
    PFword i;
    for(i = 0; i < size; i++)
	{
        dest[i] = src[i];
    }
}

PFword pfCoGetUint16(const PFbyte data[])
{
    PFCoBytes b;
    b.u8[0] = data[0];
    b.u8[1] = data[1];
    return b.u16[0];
}

PFdword pfCoGetUint32(const PFbyte data[])
{
    PFCoBytes b;
    b.u8[0] = data[0];
    b.u8[1] = data[1];
    b.u8[2] = data[2];
    b.u8[3] = data[3];
    return b.u32[0];
}

void pfCoSetUint16(PFbyte data[], const PFword value)
{
    PFCoBytes b;
    b.u16[0] = value;
    data[0] = b.u8[0];
    data[1] = b.u8[1];
}

void pfCoSetUint32(PFbyte data[], const PFdword value)
{
    PFCoBytes b;
    b.u32[0] = value;
    data[0] = b.u8[0];
    data[1] = b.u8[1];
    data[2] = b.u8[2];
    data[3] = b.u8[3];
}

#ifdef CO_LITTLE_ENDIAN
void pfCoMemcpySwap2(PFbyte dest[], const PFbyte src[])
{
    dest[0] = src[0];
    dest[1] = src[1];
}

void pfCoMemcpySwap4(PFbyte dest[], const PFbyte src[])
{
    dest[0] = src[0];
    dest[1] = src[1];
    dest[2] = src[2];
    dest[3] = src[3];
}
#endif
#ifdef CO_BIG_ENDIAN
void pfCoMemcpySwap2(PFbyte dest[], const PFbyte src[])
{
    dest[0] = src[1];
    dest[1] = src[0];
}
void pfCoMemcpySwap4(PFbyte dest[], const PFbyte src[])
{
    dest[0] = src[3];
    dest[1] = src[2];
    dest[2] = src[1];
    dest[3] = src[0];
}
#endif

/*
 * Read received message from CAN module.
 *
 * Function will be called (by CAN receive interrupt) every time, when CAN
 * message with correct identifier will be received. For more information and
 * description of parameters see file CO_driver.h.
 */
static void pfCoSdoReceive(void *object, const PFCoCanRxMsg *msg);
static void pfCoSdoReceive(void *object, const PFCoCanRxMsg *msg)
{
    PFCoSdo *SDO;
	SDO = (PFCoSdo*)object;   /* this is the correct pointer type of the first argument */

    /* verify message length and message overflow (previous message was not processed yet) */
    if((msg->DLC == 8U) && (!SDO->CANrxNew))
	{
        if(SDO->state != enCO_SDO_ST_DOWNLOAD_BL_SUBBLOCK) 
		{
            /* copy data and set 'new message' flag */
            SDO->CANrxData[0] = msg->data[0];
            SDO->CANrxData[1] = msg->data[1];
            SDO->CANrxData[2] = msg->data[2];
            SDO->CANrxData[3] = msg->data[3];
            SDO->CANrxData[4] = msg->data[4];
            SDO->CANrxData[5] = msg->data[5];
            SDO->CANrxData[6] = msg->data[6];
            SDO->CANrxData[7] = msg->data[7];

            SDO->CANrxNew = enBooleanTrue;
        }
        else 
		{
            /* block download, copy data directly */
            PFbyte seqno;

            SDO->CANrxData[0] = msg->data[0];
            seqno = SDO->CANrxData[0] & 0x7fU;
            SDO->timeoutTimer = 0;

            /* check correct sequence number. */
            if(seqno == (SDO->sequence + 1U)) 
			{
                /* sequence is correct */
                PFbyte i;

                SDO->sequence++;

                /* copy data */
                for(i=1; i<8; i++) 
				{
                    SDO->ODF_arg.data[SDO->bufferOffset++] = msg->data[i]; //SDO->ODF_arg.data is equal as SDO->databuffer
                    if(SDO->bufferOffset >= CO_SDO_BUFFER_SIZE) 
					{
                        /* buffer full, break reception */
                        SDO->state = enCO_SDO_ST_DOWNLOAD_BL_SUB_RESP;
                        SDO->CANrxNew = enBooleanTrue;
                        break;
                    }
                }

                /* break reception if last segment or block sequence is too large */
                if(((SDO->CANrxData[0] & 0x80U) == 0x80U) || (SDO->sequence >= SDO->blksize)) 
				{
                    SDO->state = enCO_SDO_ST_DOWNLOAD_BL_SUB_RESP;
                    SDO->CANrxNew = enBooleanTrue;
                }
            }
            else if((seqno == SDO->sequence) || (SDO->sequence == 0U))
			{
                /* Ignore message, if it is duplicate or if sequence didn't started yet. */
            }
            else 
			{
                /* seqno is totally wrong, break reception. */
                SDO->state = enCO_SDO_ST_DOWNLOAD_BL_SUB_RESP;
                SDO->CANrxNew = enBooleanTrue;
            }
        }

        /* Optional signal to RTOS, which can resume task, which handles SDO server. */
        if(SDO->CANrxNew && SDO->pFunctSignal) 
		{
            SDO->pFunctSignal(SDO->functArg);
        }
    }
}


/*
 * Function for accessing _SDO server parameter_ (index 0x1200+) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODF_1200(PFCoODFarg *ODF_arg);
static PFEnCoSdoAbortCode pfCoODF_1200(PFCoODFarg *ODF_arg)
{
    PFbyte *nodeId;
    PFdword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    nodeId = (PFbyte*) ODF_arg->object;
    value = pfCoGetUint32(ODF_arg->data);

    /* if SDO reading Object dictionary 0x1200, add nodeId to the value */
    if((ODF_arg->reading) && (ODF_arg->subIndex > 0U))
	{
        pfCoSetUint32(ODF_arg->data, value + *nodeId);
    }

    return ret;
}


/******************************************************************************/
PFint16 pfCoSdoInit(
        PFCoSdo               *SDO,
        PFword                COB_IDClientToServer,
        PFword                COB_IDServerToClient,
        PFword                ObjDictIndex_SDOServerParameter,
        PFCoSdo               *parentSDO,
        const PFCoODentry     OD[],
        PFword                ODSize,
        PFCoODExtension		  *ODExtensions,
        PFbyte                 nodeId,
        PFCoCanModule         *CANdevRx,
        PFword                CANdevRxIdx,
        PFCoCanModule         *CANdevTx,
        PFword                CANdevTxIdx)
{

    /* configure own object dictionary */
    if(parentSDO == NULL)
	{
        PFword i;

        SDO->ownOD = enBooleanTrue;
        SDO->OD = OD;
        SDO->ODSize = ODSize;
        SDO->ODExtensions = ODExtensions;

        /* clear pointers in ODExtensions */
        for(i=0U; i<ODSize; i++)
		{
            SDO->ODExtensions[i].pODFunc = NULL;
            SDO->ODExtensions[i].object = NULL;
            SDO->ODExtensions[i].flags = NULL;
        }
    }
    /* copy object dictionary from parent */
    else
	{
        SDO->ownOD = enBooleanFalse;
        SDO->OD = parentSDO->OD;
        SDO->ODSize = parentSDO->ODSize;
        SDO->ODExtensions = parentSDO->ODExtensions;
    }

    /* Configure object variables */
    SDO->nodeId = nodeId;
    SDO->state = enCO_SDO_ST_IDLE;
    SDO->CANrxNew = enBooleanFalse;
    SDO->pFunctSignal = 0;
    SDO->functArg = 0;


    /* Configure Object dictionary entry at index 0x1200 */
    if(ObjDictIndex_SDOServerParameter == OD_H1200_SDO_SERVER_PARAM)
	{
        pfCoODConfigure(SDO, ObjDictIndex_SDOServerParameter, pfCoODF_1200, (void*)&SDO->nodeId, 0U, 0U);
    }

    /* configure SDO server CAN reception */
    pfCoCanRxBufferInit(
            CANdevRx,               /* CAN device */
            CANdevRxIdx,            /* rx buffer index */
            COB_IDClientToServer,   /* CAN identifier */
            0x7FF,                  /* mask */
            0,                      /* rtr */
            (void*)SDO,             /* object passed to receive function */
            pfCoSdoReceive);        /* this function will process received message */

    /* configure SDO server CAN transmission */
    SDO->CANdevTx = CANdevTx;
    SDO->CANtxBuff = pfCoCanTxBufferInit(
            CANdevTx,               /* CAN device */
            CANdevTxIdx,            /* index of specific buffer inside CAN module */
            COB_IDServerToClient,   /* CAN identifier */
            0,                      /* rtr */
            8,                      /* number of data bytes */
            0);                     /* synchronous message flag bit */

    return enCO_ERROR_NO;
}


/******************************************************************************/
void pfCoODConfigure(
        PFCoSdo               *SDO,
        PFword                index,
        PFEnCoSdoAbortCode    (*pODFunc)(PFCoODFarg *ODF_arg),
        void                   *object,
        PFbyte                *flags,
        PFbyte                 flagsSize)
{
    PFword entryNo;

    entryNo = pfCoODfind(SDO, index);
    if(entryNo < 0xFFFFU)
	{
        PFCoODExtension *ext = &SDO->ODExtensions[entryNo];
        PFbyte maxSubIndex = CO_OD_getEntryMaxSubIndex(&(SDO->OD[entryNo]));

        ext->pODFunc = pODFunc;
        ext->object = object;
        if((flags != NULL) && (flagsSize != 0U) && (flagsSize == maxSubIndex))
		{
            PFword i;
            ext->flags = flags;
            for(i=0U; i<=maxSubIndex; i++)
			{
                ext->flags[i] = 0U;
            }
        }
        else
		{
            ext->flags = NULL;
        }
    }
}


/******************************************************************************/
PFword pfCoODfind(PFCoSdo *SDO, PFword index)
{
    /* Fast search in ordered Object Dictionary. If indexes are mixed, this won't work. */
    /* If Object Dictionary has up to 2^N entries, then N is max number of loop passes. */
    PFword cur, min, max;
    const PFCoODentry* object;

    min = 0U;
    max = SDO->ODSize - 1U;
    while(min < max)
	{
        cur = (min + max) / 2;
        object = &SDO->OD[cur];
        /* Is object matched */
        if(index == CO_OD_getEntryIndex(object))
		{
            return cur;
        }
        if(index < CO_OD_getEntryIndex(object))
		{
            max = cur;
            if(max) max--;
        }
        else
            min = cur + 1U;
    }

    if(min == max)
	{
        object = &SDO->OD[min];
        /* Is object matched */
        if(index == CO_OD_getEntryIndex(object))
		{
            return min;
        }
    }

    return 0xFFFFU;  /* object does not exist in OD */
}


/******************************************************************************/
PFword pfCoODGetLength(PFCoSdo *SDO, PFword entryNo, PFbyte subIndex)
{
    const PFCoODentry* object = &SDO->OD[entryNo];

    if(entryNo == 0xFFFFU)
	{
        return 0U;
    }

    if(CO_OD_getEntryMaxSubIndex(object) == 0U)
	{    /* Object type is Var */
        if(CO_OD_getEntryDataPtr(object) == 0)
		{ /* data type is domain */
            return CO_SDO_BUFFER_SIZE;
        }
        else
		{
            return CO_OD_getEntryLength(object);
        }
    }
    else if(CO_OD_getEntryAttr(object) != 0U)
	{ /* Object type is Array */
        if(subIndex == 0U)
		{
            return 1U;
        }
        else if(CO_OD_getEntryDataPtr(object) == 0)
		{
            /* data type is domain */
            return CO_SDO_BUFFER_SIZE;
        }
        else
		{
            return CO_OD_getEntryLength(object);
        }
    }
    else
	{                            /* Object type is Record */
        const PFCoODentryRecord *pRecEntry = (const PFCoODentryRecord *)CO_OD_getEntryDataPtr(object);
        if(CO_OD_getEntryDataPtr(&pRecEntry[subIndex]) == 0)
		{
            /* data type is domain */
            return CO_SDO_BUFFER_SIZE;
        }
        else
		{
            return CO_OD_getEntryLength(&pRecEntry[subIndex]);
        }
    }
}

/******************************************************************************/
PFword pfCoODGetAttribute(PFCoSdo *SDO, PFword entryNo, PFbyte subIndex)
{
    const PFCoODentry* object = &SDO->OD[entryNo];

    if(entryNo == 0xFFFFU)
	{
        return 0U;
    }

    if(CO_OD_getEntryMaxSubIndex(object) == 0U)
	{    /* Object type is Var */
        return CO_OD_getEntryAttr(object);
    }
    else if(CO_OD_getEntryAttr(object) != 0U)
	{ /* Object type is Array */
        PFword attr = CO_OD_getEntryAttr(object);
        if(subIndex == 0U)
		{
            /* First subIndex is readonly */
            attr &= ~(CO_ODA_WRITEABLE | CO_ODA_RPDO_MAPABLE);
            attr |= CO_ODA_READABLE;
        }
        return attr;
    }
    else
	{                            /* Object type is Record */
        const PFCoODentryRecord *pRecEntry = (const PFCoODentryRecord *)CO_OD_getEntryDataPtr(object);
        return CO_OD_getEntryAttr(&pRecEntry[subIndex]);
    }
}


/******************************************************************************/
void* pfCoODGetDataPointer(PFCoSdo *SDO, PFword entryNo, PFbyte subIndex)
{
    const PFCoODentry* object = &SDO->OD[entryNo];

    if(entryNo == 0xFFFFU)
	{
        return 0;
    }

    if(CO_OD_getEntryMaxSubIndex(object) == 0U)
	{    /* Object type is Var */
        return CO_OD_getEntryDataPtr(object);
    }
    else if(CO_OD_getEntryAttr(object) != 0U)
	{ /* Object type is Array */
        if(subIndex==0)
		{
            /* this is the data, for the subIndex 0 in the array */
            return (void*) &object->maxSubIndex;
        }
        else if(CO_OD_getEntryDataPtr(object) == 0)
		{
            /* data type is domain */
            return 0;
        }
        else
		{
            return (void*)(((PFint8*)CO_OD_getEntryDataPtr(object)) + ((subIndex-1) * CO_OD_getEntryLength(object)));
        }
    }
    else
	{                            /* Object Type is Record */
        const PFCoODentryRecord *pRecEntry = (const PFCoODentryRecord *)CO_OD_getEntryDataPtr(object);
        return CO_OD_getEntryDataPtr(&pRecEntry[subIndex]);
    }
}


/******************************************************************************/
PFbyte* pfCoODGetFlagsPointer(PFCoSdo *SDO, PFword entryNo, PFbyte subIndex)
{
    PFCoODExtension* ext;

    if((entryNo == 0xFFFFU) || (SDO->ODExtensions == 0)){
        return 0;
    }

    ext = &SDO->ODExtensions[entryNo];

    return &ext->flags[subIndex];
}

/******************************************************************************/
PFdword pfCoSdoInitTransfer(PFCoSdo *SDO, PFword index, PFbyte subIndex)
{
    SDO->ODF_arg.index = index;
    SDO->ODF_arg.subIndex = subIndex;

    /* find object in Object Dictionary */
    SDO->entryNo = pfCoODfind(SDO, index);
    if(SDO->entryNo == 0xFFFFU)
	{
        return enCO_SDO_AB_NOT_EXIST ;     /* object does not exist in OD */
    }

    /* verify existance of subIndex */
    if(subIndex > CO_OD_getEntryMaxSubIndex(&(SDO->OD[SDO->entryNo])))
	{
        return enCO_SDO_AB_SUB_UNKNOWN;     /* Sub-index does not exist. */
    }

    /* pointer to data in Object dictionary */
    SDO->ODF_arg.ODdataStorage = pfCoODGetDataPointer(SDO, SDO->entryNo, subIndex);

    /* fill ODF_arg */
    SDO->ODF_arg.object = NULL;
    if(SDO->ODExtensions)
	{
        PFCoODExtension *ext = &SDO->ODExtensions[SDO->entryNo];
        SDO->ODF_arg.object = ext->object;
    }
    SDO->ODF_arg.data = SDO->databuffer;
    SDO->ODF_arg.dataLength = pfCoODGetLength(SDO, SDO->entryNo, subIndex);
    SDO->ODF_arg.attribute = pfCoODGetAttribute(SDO, SDO->entryNo, subIndex);
    SDO->ODF_arg.pFlags = pfCoODGetFlagsPointer(SDO, SDO->entryNo, subIndex);

    SDO->ODF_arg.firstSegment = enBooleanTrue;
    SDO->ODF_arg.lastSegment = enBooleanTrue;

    /* indicate total data length, if not domain */
    SDO->ODF_arg.dataLengthTotal = (SDO->ODF_arg.ODdataStorage) ? SDO->ODF_arg.dataLength : 0U;

    /* verify length */
    if(SDO->ODF_arg.dataLength > CO_SDO_BUFFER_SIZE){
        return enCO_SDO_AB_DEVICE_INCOMPAT;     /* general internal incompatibility in the device */
    }

    return 0U;
}


/******************************************************************************/
PFdword pfCoSdoReadOD(PFCoSdo *SDO, PFword SDOBufferSize)
{
    PFbyte *SDObuffer = SDO->ODF_arg.data;
    const PFbyte *ODdata = SDO->ODF_arg.ODdataStorage;
    PFword length = SDO->ODF_arg.dataLength;
    PFCoODExtension *ext = 0;

    /* is object readable? */
    if((SDO->ODF_arg.attribute & CO_ODA_READABLE) == 0)
        return enCO_SDO_AB_WRITEONLY;     /* attempt to read a write-only object */

    /* find  */
    if(SDO->ODExtensions != NULL)
	{
        ext = &SDO->ODExtensions[SDO->entryNo];
    }

    /* copy data from OD to SDO buffer if not domain */
    if(ODdata != NULL)
	{
        PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
        while(length--)
		{
            if((SDO->ODF_arg.attribute & CO_ODA_MEM_ROM) && (SDO->ODF_arg.attribute & CO_ODA_MEM_RAM) == 0)
            {
                /* use special functions to read from ROM */
                *(SDObuffer++) = CO_ROM_READ8_FROMADDR(ODdata);
            }
            else
                *(SDObuffer++) = *ODdata;
            ODdata++;
        }
        pfCO_hal_InterruptsRestore(flags);
    }
    /* if domain, Object dictionary function MUST exist */
    else
	{
        if(ext->pODFunc == NULL)
		{
            return enCO_SDO_AB_DEVICE_INCOMPAT;     /* general internal incompatibility in the device */
        }
    }

    /* call Object dictionary function if registered */
    SDO->ODF_arg.reading = enBooleanTrue;
    if(ext->pODFunc != NULL)
	{
        PFdword abortCode = ext->pODFunc(&SDO->ODF_arg);
        if(abortCode != 0U)
		{
            return abortCode;
        }

        /* dataLength (upadted by pODFunc) must be inside limits */
        if((SDO->ODF_arg.dataLength == 0U) || (SDO->ODF_arg.dataLength > SDOBufferSize))
		{
            return enCO_SDO_AB_DEVICE_INCOMPAT;     /* general internal incompatibility in the device */
        }
    }
    SDO->ODF_arg.firstSegment = enBooleanFalse;

    /* swap data if processor is not little endian (CANopen is) */
#ifdef CO_BIG_ENDIAN
    if((SDO->ODF_arg.attribute & CO_ODA_MB_VALUE) != 0)
	{
        PFword len = SDO->ODF_arg.dataLength;
        PFbyte *buf1 = SDO->ODF_arg.data;
        PFbyte *buf2 = buf1 + len - 1;

        len /= 2;
        while(len--)
		{
            PFbyte b = *buf1;
            *(buf1++) = *buf2;
            *(buf2--) = b;
        }
    }
#endif

    return 0U;
}

/******************************************************************************/
PFdword pfCoSdoWriteOD(PFCoSdo *SDO, PFword length)
{
    PFbyte *SDObuffer = SDO->ODF_arg.data;
    PFbyte *ODdata = (PFbyte*)SDO->ODF_arg.ODdataStorage;

    /* is object writeable? */
    if((SDO->ODF_arg.attribute & CO_ODA_WRITEABLE) == 0)
	{
        return enCO_SDO_AB_READONLY;     /* attempt to write a read-only object */
    }
    /* @AGv: currently write to ROM is not supported */
    if((SDO->ODF_arg.attribute & CO_ODA_MEM_ROM) && (SDO->ODF_arg.attribute & CO_ODA_MEM_RAM) == 0)
	{
        return enCO_SDO_AB_READONLY;     /* attempt to write a read-only object */
    }

    /* length of domain data is application specific and not verified */
    if(ODdata == 0)
	{
        SDO->ODF_arg.dataLength = length;
    }

    /* verify length except for domain data type */
    else if(SDO->ODF_arg.dataLength != length)
	{
        return enCO_SDO_AB_TYPE_MISMATCH;     /* Length of service parameter does not match */
    }

    /* swap data if processor is not little endian (CANopen is) */
#ifdef CO_BIG_ENDIAN
    if((SDO->ODF_arg.attribute & CO_ODA_MB_VALUE) != 0)
	{
        PFword len = SDO->ODF_arg.dataLength;
        PFbyte *buf1 = SDO->ODF_arg.data;
        PFbyte *buf2 = buf1 + len - 1;

        len /= 2;
        while(len--){
            PFbyte b = *buf1;
            *(buf1++) = *buf2;
            *(buf2--) = b;
        }
    }
#endif

    /* call Object dictionary function if registered */
    SDO->ODF_arg.reading = enBooleanFalse;
    if(SDO->ODExtensions != NULL)
	{
        PFCoODExtension *ext = &SDO->ODExtensions[SDO->entryNo];

        if(ext->pODFunc != NULL)
		{
            PFdword abortCode = ext->pODFunc(&SDO->ODF_arg);
            if(abortCode != 0U)
			{
                return abortCode;
            }
        }
    }
    SDO->ODF_arg.firstSegment = enBooleanFalse;

    /* copy data from SDO buffer to OD if not domain */
    if(ODdata != NULL)
	{
        PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
        while(length--)
		{
            *(ODdata++) = *(SDObuffer++);
        }
        pfCO_hal_InterruptsRestore(flags);
    }

    return 0;
}

/******************************************************************************/
static void pfCoSdoAbort(PFCoSdo *SDO, PFdword code)
{
    SDO->CANtxBuff->data[0] = 0x80;
    SDO->CANtxBuff->data[1] = SDO->ODF_arg.index & 0xFF;
    SDO->CANtxBuff->data[2] = (SDO->ODF_arg.index>>8) & 0xFF;
    SDO->CANtxBuff->data[3] = SDO->ODF_arg.subIndex;
    pfCoMemcpySwap4(&SDO->CANtxBuff->data[4], (PFbyte*)&code);
    SDO->state = enCO_SDO_ST_IDLE;
    SDO->CANrxNew = enBooleanFalse;
    pfCoCanSend(SDO->CANdevTx, SDO->CANtxBuff);
}


/******************************************************************************/
PFint8 pfCoSdoProcess(
        PFCoSdo               *SDO,
        PFEnBoolean           NMTisPreOrOperational,
        PFword                timeDifference_ms,
        PFword                SDOtimeoutTime)
{
    PFEnCoSdoState state = enCO_SDO_ST_IDLE;
    PFEnBoolean timeoutSubblockDownolad = enBooleanFalse;
    PFEnBoolean sendResponse = enBooleanFalse;

    /* return if idle */
    if((SDO->state == enCO_SDO_ST_IDLE) && (!SDO->CANrxNew))
	{
        return 0;
    }

    /* SDO is allowed to work only in operational or pre-operational NMT state */
    if(!NMTisPreOrOperational)
	{
        SDO->state = enCO_SDO_ST_IDLE;
        SDO->CANrxNew = enBooleanFalse;
        return 0;
    }

    /* Is something new to process? */
    if((!SDO->CANtxBuff->bufferFull) && ((SDO->CANrxNew) || (SDO->state == enCO_SDO_ST_UPLOAD_BL_SUBBLOCK)))
	{
        PFbyte CCS = SDO->CANrxData[0] >> 5;   /* Client command specifier */

        /* reset timeout */
        if(SDO->state != enCO_SDO_ST_UPLOAD_BL_SUBBLOCK)
            SDO->timeoutTimer = 0;

        /* clear response buffer */
        SDO->CANtxBuff->data[0] = SDO->CANtxBuff->data[1] = SDO->CANtxBuff->data[2] = SDO->CANtxBuff->data[3] = 0;
        SDO->CANtxBuff->data[4] = SDO->CANtxBuff->data[5] = SDO->CANtxBuff->data[6] = SDO->CANtxBuff->data[7] = 0;

        /* Is abort from client? */
        if((SDO->CANrxNew) && (SDO->CANrxData[0] == CCS_ABORT))
		{
            SDO->state = enCO_SDO_ST_IDLE;
            SDO->CANrxNew = enBooleanFalse;
            return -1;
        }

        /* continue with previous SDO communication or start new */
        if(SDO->state != enCO_SDO_ST_IDLE)
		{
            state = SDO->state;
        }
        else
		{
            PFdword abortCode;

            /* Is client command specifier valid */
            if((CCS != CCS_DOWNLOAD_INITIATE) && (CCS != CCS_UPLOAD_INITIATE) &&
                (CCS != CCS_DOWNLOAD_BLOCK) && (CCS != CCS_UPLOAD_BLOCK))
				{
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
				}

            /* init ODF_arg */
            abortCode = pfCoSdoInitTransfer(SDO, (PFword)SDO->CANrxData[2]<<8 | SDO->CANrxData[1], SDO->CANrxData[3]);
            if(abortCode != 0U)
			{
                pfCoSdoAbort(SDO, abortCode);
                return -1;
            }

            /* download */
            if((CCS == CCS_DOWNLOAD_INITIATE) || (CCS == CCS_DOWNLOAD_BLOCK))
			{
                if((SDO->ODF_arg.attribute & CO_ODA_WRITEABLE) == 0U)
				{
                    pfCoSdoAbort(SDO, enCO_SDO_AB_READONLY); /* attempt to write a read-only object */
                    return -1;
                }

                /* set state machine to normal or block download */
                if(CCS == CCS_DOWNLOAD_INITIATE)
				{
                    state = enCO_SDO_ST_DOWNLOAD_INITIATE;
                }
                else
				{
                    state = enCO_SDO_ST_DOWNLOAD_BL_INITIATE;
                }
            }

            /* upload */
            else
			{
                abortCode = pfCoSdoReadOD(SDO, CO_SDO_BUFFER_SIZE);
                if(abortCode != 0U)
				{
                    pfCoSdoAbort(SDO, abortCode);
                    return -1;
                }

                /* if data size is large enough set state machine to block upload, otherwise set to normal transfer */
                if((CCS == CCS_UPLOAD_BLOCK) && (SDO->ODF_arg.dataLength > SDO->CANrxData[5]))
				{
                    state = enCO_SDO_ST_UPLOAD_BL_INITIATE;
                }
                else
				{
                    state = enCO_SDO_ST_UPLOAD_INITIATE;
                }
            }
        }
    }

    /* verify SDO timeout */
    if(SDO->timeoutTimer < SDOtimeoutTime)
	{
        SDO->timeoutTimer += timeDifference_ms;
    }
    if(SDO->timeoutTimer >= SDOtimeoutTime)
	{
        if((SDO->state == enCO_SDO_ST_DOWNLOAD_BL_SUBBLOCK) && (SDO->sequence != 0) && (!SDO->CANtxBuff->bufferFull))
		{
            timeoutSubblockDownolad = enBooleanTrue;
            state = enCO_SDO_ST_DOWNLOAD_BL_SUB_RESP;
        }
        else
		{
            pfCoSdoAbort(SDO, enCO_SDO_AB_TIMEOUT); /* SDO protocol timed out */
            return -1;
        }
    }

    /* return immediately if still idle */
    if(state == enCO_SDO_ST_IDLE){
        return 0;
    }

    /* state machine (buffer is freed (SDO->CANrxNew = 0;) at the end) */
    switch(state){
        PFdword abortCode;
        PFword len, i;
        PFEnBoolean lastSegmentInSubblock;

        case enCO_SDO_ST_DOWNLOAD_INITIATE:
		{
            /* default response */
            SDO->CANtxBuff->data[0] = 0x60;
            SDO->CANtxBuff->data[1] = SDO->CANrxData[1];
            SDO->CANtxBuff->data[2] = SDO->CANrxData[2];
            SDO->CANtxBuff->data[3] = SDO->CANrxData[3];

            /* Expedited transfer */
            if((SDO->CANrxData[0] & 0x02U) != 0U)
			{
                /* is size indicated? Get message length */
                if((SDO->CANrxData[0] & 0x01U) != 0U)
				{
                    len = 4U - ((SDO->CANrxData[0] >> 2U) & 0x03U);
                }
                else
				{
                    len = 4U;
                }

                /* copy data to SDO buffer */
                SDO->ODF_arg.data[0] = SDO->CANrxData[4];
                SDO->ODF_arg.data[1] = SDO->CANrxData[5];
                SDO->ODF_arg.data[2] = SDO->CANrxData[6];
                SDO->ODF_arg.data[3] = SDO->CANrxData[7];

                /* write data to the Object dictionary */
                abortCode = pfCoSdoWriteOD(SDO, len);
                if(abortCode != 0U)
				{
                    pfCoSdoAbort(SDO, abortCode);
                    return -1;
                }

                /* finish the communication */
                SDO->state = enCO_SDO_ST_IDLE;
                sendResponse = enBooleanTrue;
            }

            /* Segmented transfer */
            else
			{
                /* verify length if size is indicated */
                if((SDO->CANrxData[0]&0x01) != 0)
				{
                    PFdword lenRx;
                    pfCoMemcpySwap4((PFbyte*)&lenRx, &SDO->CANrxData[4]);
                    SDO->ODF_arg.dataLengthTotal = lenRx;

                    /* verify length except for domain data type */
                    if((lenRx != SDO->ODF_arg.dataLength) && (SDO->ODF_arg.ODdataStorage != 0))
					{
                        pfCoSdoAbort(SDO, enCO_SDO_AB_TYPE_MISMATCH);  /* Length of service parameter does not match */
                        return -1;
                    }
                }
                SDO->bufferOffset = 0;
                SDO->sequence = 0;
                SDO->state = enCO_SDO_ST_DOWNLOAD_SEGMENTED;
                sendResponse = enBooleanTrue;
            }
            break;
        }

        case enCO_SDO_ST_DOWNLOAD_SEGMENTED:{
            /* verify client command specifier */
            if((SDO->CANrxData[0]&0xE0) != 0x00U)
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
            }

            /* verify toggle bit */
            i = (SDO->CANrxData[0]&0x10U) ? 1U : 0U;
            if(i != SDO->sequence)
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_TOGGLE_BIT);/* toggle bit not alternated */
                return -1;
            }

            /* get size of data in message */
            len = 7U - ((SDO->CANrxData[0] >> 1U) & 0x07U);

            /* verify length. Domain data type enables length larger than SDO buffer size */
            if((SDO->bufferOffset + len) > SDO->ODF_arg.dataLength)
			{
                if(SDO->ODF_arg.ODdataStorage != 0)
				{
                    pfCoSdoAbort(SDO, enCO_SDO_AB_DATA_LONG);  /* Length of service parameter too high */
                    return -1;
                }
                else
				{
                    /* empty buffer in domain data type */
                    SDO->ODF_arg.lastSegment = enBooleanFalse;
                    abortCode = pfCoSdoWriteOD(SDO, SDO->bufferOffset);
                    if(abortCode != 0U)
					{
                        pfCoSdoAbort(SDO, abortCode);
                        return -1;
                    }

                    SDO->ODF_arg.dataLength = CO_SDO_BUFFER_SIZE;
                    SDO->bufferOffset = 0;
                }
            }

            /* copy data to buffer */
            for(i=0U; i<len; i++)
                SDO->ODF_arg.data[SDO->bufferOffset++] = SDO->CANrxData[i+1];

            /* If no more segments to be downloaded, write data to the Object dictionary */
            if((SDO->CANrxData[0] & 0x01U) != 0U)
			{
                SDO->ODF_arg.lastSegment = enBooleanTrue;
                abortCode = pfCoSdoWriteOD(SDO, SDO->bufferOffset);
                if(abortCode != 0U)
				{
                    pfCoSdoAbort(SDO, abortCode);
                    return -1;
                }

                /* finish */
                SDO->state = enCO_SDO_ST_IDLE;
            }

            /* download segment response and alternate toggle bit */
            SDO->CANtxBuff->data[0] = 0x20 | (SDO->sequence ? 0x10 : 0x00);
            SDO->sequence = (SDO->sequence) ? 0 : 1;
            sendResponse = enBooleanTrue;
            break;
        }

        case enCO_SDO_ST_DOWNLOAD_BL_INITIATE:{
            /* verify client command specifier and subcommand */
            if((SDO->CANrxData[0]&0xE1U) != 0xC0U)
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
            }

            /* prepare response */
            SDO->CANtxBuff->data[0] = 0xA4;
            SDO->CANtxBuff->data[1] = SDO->CANrxData[1];
            SDO->CANtxBuff->data[2] = SDO->CANrxData[2];
            SDO->CANtxBuff->data[3] = SDO->CANrxData[3];

            /* blksize */
            SDO->blksize = (CO_SDO_BUFFER_SIZE > (7*127)) ? 127 : (CO_SDO_BUFFER_SIZE / 7);
            SDO->CANtxBuff->data[4] = SDO->blksize;

            /* is CRC enabled */
            SDO->crcEnabled = (SDO->CANrxData[0] & 0x04) ? enBooleanTrue : enBooleanFalse;
            SDO->crc = 0;

            /* verify length if size is indicated */
            if((SDO->CANrxData[0]&0x02) != 0U)
			{
                PFdword lenRx;
                pfCoMemcpySwap4((PFbyte*)&lenRx, &SDO->CANrxData[4]);
                SDO->ODF_arg.dataLengthTotal = lenRx;

                /* verify length except for domain data type */
                if((lenRx != SDO->ODF_arg.dataLength) && (SDO->ODF_arg.ODdataStorage != 0))
				{
                    pfCoSdoAbort(SDO, enCO_SDO_AB_TYPE_MISMATCH);  /* Length of service parameter does not match */
                    return -1;
                }
            }

            SDO->bufferOffset = 0;
            SDO->sequence = 0;
            SDO->state = enCO_SDO_ST_DOWNLOAD_BL_SUBBLOCK;

            /* send response */
            sendResponse = enBooleanTrue;
            break;
        }

        case enCO_SDO_ST_DOWNLOAD_BL_SUBBLOCK:{
            /* data are copied directly in receive function */
            break;
        }

        case enCO_SDO_ST_DOWNLOAD_BL_SUB_RESP:
		{
            /* no new message received, SDO timeout occured, try to response */
            lastSegmentInSubblock = (!timeoutSubblockDownolad &&
                        ((SDO->CANrxData[0] & 0x80U) == 0x80U)) ? enBooleanTrue : enBooleanFalse;

            /* prepare response */
            SDO->CANtxBuff->data[0] = 0xA2;
            SDO->CANtxBuff->data[1] = SDO->sequence;
            SDO->sequence = 0;

            /* empty buffer in domain data type if not last segment */
            if((SDO->ODF_arg.ODdataStorage == 0) && (SDO->bufferOffset != 0) && !lastSegmentInSubblock)
			{
                /* calculate CRC on next bytes, if enabled */
                if(SDO->crcEnabled)
				{
                    SDO->crc = crc16_ccitt(SDO->ODF_arg.data, SDO->bufferOffset, SDO->crc);
                }

                /* write data to the Object dictionary */
                SDO->ODF_arg.lastSegment = enBooleanFalse;
                abortCode = pfCoSdoWriteOD(SDO, SDO->bufferOffset);
                if(abortCode != 0U)
				{
                    pfCoSdoAbort(SDO, abortCode);
                    return -1;
                }

                SDO->ODF_arg.dataLength = CO_SDO_BUFFER_SIZE;
                SDO->bufferOffset = 0;
            }

            /* blksize */
            len = CO_SDO_BUFFER_SIZE - SDO->bufferOffset;
            SDO->blksize = (len > (7*127)) ? 127 : (len / 7);
            SDO->CANtxBuff->data[2] = SDO->blksize;

            /* set next state */
            if(lastSegmentInSubblock) 
			{
                SDO->state = enCO_SDO_ST_DOWNLOAD_BL_END;
            }
            else if(SDO->bufferOffset >= CO_SDO_BUFFER_SIZE) 
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_DEVICE_INCOMPAT);
                return -1;
            }
            else 
			{
                SDO->state = enCO_SDO_ST_DOWNLOAD_BL_SUBBLOCK;
            }

            /* send response */
            sendResponse = enBooleanTrue;

            break;
        }

        case enCO_SDO_ST_DOWNLOAD_BL_END:{
            /* verify client command specifier and subcommand */
            if((SDO->CANrxData[0]&0xE1U) != 0xC1U)
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
            }

            /* number of bytes in the last segment of the last block that do not contain data. */
            len = (SDO->CANrxData[0]>>2U) & 0x07U;
            SDO->bufferOffset -= len;

            /* calculate and verify CRC, if enabled */
            if(SDO->crcEnabled)
			{
                PFword crc;
                SDO->crc = crc16_ccitt(SDO->ODF_arg.data, SDO->bufferOffset, SDO->crc);

                pfCoMemcpySwap2((PFbyte*)&crc, &SDO->CANrxData[1]);

                if(SDO->crc != crc)
				{
                    pfCoSdoAbort(SDO, enCO_SDO_AB_CRC);   /* CRC error (block mode only). */
                    return -1;
                }
            }

            /* write data to the Object dictionary */
            SDO->ODF_arg.lastSegment = enBooleanTrue;
            abortCode = pfCoSdoWriteOD(SDO, SDO->bufferOffset);
            if(abortCode != 0U)
			{
                pfCoSdoAbort(SDO, abortCode);
                return -1;
            }

            /* send response */
            SDO->CANtxBuff->data[0] = 0xA1;
            SDO->state = enCO_SDO_ST_IDLE;
            sendResponse = enBooleanTrue;
            break;
        }

        case enCO_SDO_ST_UPLOAD_INITIATE:{
            /* default response */
            SDO->CANtxBuff->data[1] = SDO->CANrxData[1];
            SDO->CANtxBuff->data[2] = SDO->CANrxData[2];
            SDO->CANtxBuff->data[3] = SDO->CANrxData[3];

            /* Expedited transfer */
            if(SDO->ODF_arg.dataLength <= 4U)
			{
                for(i=0U; i<SDO->ODF_arg.dataLength; i++)
                    SDO->CANtxBuff->data[4U+i] = SDO->ODF_arg.data[i];

                SDO->CANtxBuff->data[0] = 0x43U | ((4U-SDO->ODF_arg.dataLength) << 2U);
                SDO->state = enCO_SDO_ST_IDLE;

                sendResponse = enBooleanTrue;
            }

            /* Segmented transfer */
            else
			{
                SDO->bufferOffset = 0U;
                SDO->sequence = 0U;
                SDO->state = enCO_SDO_ST_UPLOAD_SEGMENTED;

                /* indicate data size, if known */
                if(SDO->ODF_arg.dataLengthTotal != 0U)
				{
                    PFdword len = SDO->ODF_arg.dataLengthTotal;
                    pfCoMemcpySwap4(&SDO->CANtxBuff->data[4], (PFbyte*)&len);
                    SDO->CANtxBuff->data[0] = 0x41U;
                }
                else
				{
                    SDO->CANtxBuff->data[0] = 0x40U;
                }

                /* send response */
                sendResponse = enBooleanTrue;
            }
            break;
        }

        case enCO_SDO_ST_UPLOAD_SEGMENTED:{
            /* verify client command specifier */
            if((SDO->CANrxData[0]&0xE0U) != 0x60U){
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
            }

            /* verify toggle bit */
            i = ((SDO->CANrxData[0]&0x10U) != 0) ? 1U : 0U;
            if(i != SDO->sequence)
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_TOGGLE_BIT);/* toggle bit not alternated */
                return -1;
            }

            /* calculate length to be sent */
            len = SDO->ODF_arg.dataLength - SDO->bufferOffset;
            if(len > 7U) len = 7U;

            /* If data type is domain, re-fill the data buffer if neccessary and indicated so. */
            if((SDO->ODF_arg.ODdataStorage == 0) && (len < 7U) && (!SDO->ODF_arg.lastSegment))
			{
                /* copy previous data to the beginning */
                for(i=0U; i<len; i++)
				{
                    SDO->ODF_arg.data[i] = SDO->ODF_arg.data[SDO->bufferOffset+i];
                }

                /* move the beginning of the data buffer */
                SDO->ODF_arg.data += len;
                SDO->ODF_arg.dataLength = pfCoODGetLength(SDO, SDO->entryNo, SDO->ODF_arg.subIndex) - len;

                /* read next data from Object dictionary function */
                abortCode = pfCoSdoReadOD(SDO, CO_SDO_BUFFER_SIZE);
                if(abortCode != 0U)
				{
                    pfCoSdoAbort(SDO, abortCode);
                    return -1;
                }

                /* return to the original data buffer */
                SDO->ODF_arg.data -= len;
                SDO->ODF_arg.dataLength +=  len;
                SDO->bufferOffset = 0;

                /* re-calculate the length */
                len = SDO->ODF_arg.dataLength;
                if(len > 7U) len = 7U;
            }

            /* fill response data bytes */
            for(i=0U; i<len; i++)
                SDO->CANtxBuff->data[i+1] = SDO->ODF_arg.data[SDO->bufferOffset++];

            /* first response byte */
            SDO->CANtxBuff->data[0] = 0x00 | (SDO->sequence ? 0x10 : 0x00) | ((7-len)<<1);
            SDO->sequence = (SDO->sequence) ? 0 : 1;

            /* verify end of transfer */
            if((SDO->bufferOffset == SDO->ODF_arg.dataLength) && (SDO->ODF_arg.lastSegment))
			{
                SDO->CANtxBuff->data[0] |= 0x01;
                SDO->state = enCO_SDO_ST_IDLE;
            }

            /* send response */
            sendResponse = enBooleanTrue;
            break;
        }

        case enCO_SDO_ST_UPLOAD_BL_INITIATE:
		{
            /* default response */
            SDO->CANtxBuff->data[1] = SDO->CANrxData[1];
            SDO->CANtxBuff->data[2] = SDO->CANrxData[2];
            SDO->CANtxBuff->data[3] = SDO->CANrxData[3];

            /* calculate CRC, if enabled */
            if((SDO->CANrxData[0] & 0x04U) != 0U)
			{
                SDO->crcEnabled = enBooleanTrue;
                SDO->crc = crc16_ccitt(SDO->ODF_arg.data, SDO->ODF_arg.dataLength, 0);
            }
            else
			{
                SDO->crcEnabled = enBooleanFalse;
                SDO->crc = 0;
            }

            /* Number of segments per block */
            SDO->blksize = SDO->CANrxData[4];

            /* verify client subcommand and blksize */
            if(((SDO->CANrxData[0]&0x03U) != 0x00U) || (SDO->blksize < 1U) || (SDO->blksize > 127U))
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
            }

            /* verify if SDO data buffer is large enough */
            if(((SDO->blksize*7U) > SDO->ODF_arg.dataLength) && (!SDO->ODF_arg.lastSegment))
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_BLOCK_SIZE); /* Invalid block size (block mode only). */
                return -1;
            }

            /* indicate data size, if known */
            if(SDO->ODF_arg.dataLengthTotal != 0U)
			{
                PFdword len = SDO->ODF_arg.dataLengthTotal;
                pfCoMemcpySwap4(&SDO->CANtxBuff->data[4], (PFbyte*)&len);
                SDO->CANtxBuff->data[0] = 0xC6U;
            }
            else
			{
                SDO->CANtxBuff->data[0] = 0xC4U;
            }

            /* send response */
            SDO->state = enCO_SDO_ST_UPLOAD_BL_INITIATE_2;
            sendResponse = enBooleanTrue;
            break;
        }

        case enCO_SDO_ST_UPLOAD_BL_INITIATE_2:{
            /* verify client command specifier and subcommand */
            if((SDO->CANrxData[0]&0xE3U) != 0xA3U)
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
            }

            SDO->bufferOffset = 0;
            SDO->sequence = 0;
            SDO->endOfTransfer = enBooleanFalse;
            SDO->CANrxNew = enBooleanFalse;
            SDO->state = enCO_SDO_ST_UPLOAD_BL_SUBBLOCK;
            /* continue in next case */
        }

        case enCO_SDO_ST_UPLOAD_BL_SUBBLOCK:{
            /* is block confirmation received */
            if(SDO->CANrxNew)
			{
                PFbyte ackseq;
                PFword j;

                /* verify client command specifier and subcommand */
                if((SDO->CANrxData[0]&0xE3U) != 0xA2U)
				{
                    pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                    return -1;
                }

                ackseq = SDO->CANrxData[1];   /* sequence number of the last segment, that was received correctly. */

                /* verify if response is too early */
                if(ackseq > SDO->sequence)
				{
                    pfCoSdoAbort(SDO, enCO_SDO_AB_BLOCK_SIZE); /* Invalid block size (block mode only). */
                    return -1;
                }

                /* end of transfer */
                if((SDO->endOfTransfer) && (ackseq == SDO->blksize))
				{
                    /* first response byte */
                    SDO->CANtxBuff->data[0] = 0xC1 | ((7 - SDO->lastLen) << 2);

                    /* CRC */
                    if(SDO->crcEnabled)
                        pfCoMemcpySwap2(&SDO->CANtxBuff->data[1], (PFbyte*)&SDO->crc);

                    SDO->state = enCO_SDO_ST_UPLOAD_BL_END;

                    /* send response */
                    sendResponse = enBooleanTrue;
                    break;
                }

                /* move remaining data to the beginning */
                for(i=ackseq*7, j=0; i<SDO->ODF_arg.dataLength; i++, j++)
                    SDO->ODF_arg.data[j] = SDO->ODF_arg.data[i];

                /* set remaining data length in buffer */
                SDO->ODF_arg.dataLength -= ackseq * 7U;

                /* new block size */
                SDO->blksize = SDO->CANrxData[2];

                /* If data type is domain, re-fill the data buffer if neccessary and indicated so. */
                if((SDO->ODF_arg.ODdataStorage == 0) && (SDO->ODF_arg.dataLength < (SDO->blksize*7U)) && (!SDO->ODF_arg.lastSegment))
				{
                    /* move the beginning of the data buffer */
                    len = SDO->ODF_arg.dataLength; /* length of valid data in buffer */
                    SDO->ODF_arg.data += len;
                    SDO->ODF_arg.dataLength = pfCoODGetLength(SDO, SDO->entryNo, SDO->ODF_arg.subIndex) - len;

                    /* read next data from Object dictionary function */
                    abortCode = pfCoSdoReadOD(SDO, CO_SDO_BUFFER_SIZE);
                    if(abortCode != 0U)
					{
                        pfCoSdoAbort(SDO, abortCode);
                        return -1;
                    }

                    /* calculate CRC on next bytes, if enabled */
                    if(SDO->crcEnabled)
					{
                        SDO->crc = crc16_ccitt(SDO->ODF_arg.data, SDO->ODF_arg.dataLength, SDO->crc);
                    }

                  /* return to the original data buffer */
                    SDO->ODF_arg.data -= len;
                    SDO->ODF_arg.dataLength +=  len;
                }

                /* verify if SDO data buffer is large enough */
                if(((SDO->blksize*7U) > SDO->ODF_arg.dataLength) && (!SDO->ODF_arg.lastSegment))
				{
                    pfCoSdoAbort(SDO, enCO_SDO_AB_BLOCK_SIZE); /* Invalid block size (block mode only). */
                    return -1;
                }

                SDO->bufferOffset = 0U;
                SDO->sequence = 0U;
                SDO->endOfTransfer = enBooleanFalse;

                /* clear flag here */
                SDO->CANrxNew = enBooleanFalse;
            }

            /* return, if all segments was allready transfered or on end of transfer */
            if((SDO->sequence == SDO->blksize) || (SDO->endOfTransfer))
			{
                return 1;/* don't clear the SDO->CANrxNew flag, so return directly */
            }

            /* reset timeout */
            SDO->timeoutTimer = 0;

            /* calculate length to be sent */
            len = SDO->ODF_arg.dataLength - SDO->bufferOffset;
            if(len > 7U)
			{
                len = 7U;
            }

            /* fill response data bytes */
            for(i=0U; i<len; i++)
			{
                SDO->CANtxBuff->data[i+1] = SDO->ODF_arg.data[SDO->bufferOffset++];
            }

            /* first response byte */
            SDO->CANtxBuff->data[0] = ++SDO->sequence;

            /* verify end of transfer */
            if((SDO->bufferOffset == SDO->ODF_arg.dataLength) && (SDO->ODF_arg.lastSegment))
			{
                SDO->CANtxBuff->data[0] |= 0x80;
                SDO->lastLen = len;
                SDO->blksize = SDO->sequence;
                SDO->endOfTransfer = enBooleanTrue;
            }

            /* send response */
            pfCoCanSend(SDO->CANdevTx, SDO->CANtxBuff);

            /* don't clear the SDO->CANrxNew flag, so return directly */
            return 1;
        }

        case enCO_SDO_ST_UPLOAD_BL_END:
		{
            /* verify client command specifier */
            if((SDO->CANrxData[0]&0xE1U) != 0xA1U)
			{
                pfCoSdoAbort(SDO, enCO_SDO_AB_CMD);/* Client command specifier not valid or unknown. */
                return -1;
            }

            SDO->state = enCO_SDO_ST_IDLE;
            break;
        }

        default:
		{
            pfCoSdoAbort(SDO, enCO_SDO_AB_DEVICE_INCOMPAT);/* general internal incompatibility in the device */
            return -1;
        }
    }

    /* free buffer and send message */
    SDO->CANrxNew = enBooleanFalse;
    if(sendResponse) 
	{
        pfCoCanSend(SDO->CANdevTx, SDO->CANtxBuff);
    }

    if(SDO->state != enCO_SDO_ST_IDLE)
	{
        return 1;
    }

    return 0;
}
