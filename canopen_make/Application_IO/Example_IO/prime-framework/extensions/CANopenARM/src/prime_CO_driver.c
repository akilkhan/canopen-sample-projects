 /*
 * CAN module object for generic microcontroller.
 */
#include "prime_framework.h"
#include "prime_CO_driver.h"
#include "prime_CO_Emergency.h"
#include "prime_canCommon.h"
#include "prime_can1.h"

#define 	LPC17XX_SYNC_TXB1    0x1 /* sync messsage in tx buf 1 */
#define 	LPC17XX_SYNC_TXB2    0x2 /* sync messsage in tx buf 2 */
#define 	LPC17XX_SYNC_TXB3    0x4 /* sync messsage in tx buf 3 */

/******************************************************************************/
PFEnCoReturnError pfCoCanModuleInit(
        PFCoCanModule          *CANmodule,
        PFCoCanModuleHwConfig  *CANhwCfg,
        PFCoCanRx              rxArray[],
        PFword                	rxSize,
        PFCoCanTx              txArray[],
        PFword                	txSize,
        PFword                	CANbitRate)
{
    PFword i;

    /* Configure object variables */
    CANmodule->hw.CANbaseAddress = CANhwCfg->uBase;
#if CO_NO_SYNC > 0
    CANmodule->hw.flags = 0;
#endif
    CANmodule->rxArray = rxArray;
    CANmodule->rxSize = rxSize;
    CANmodule->txArray = txArray;
    CANmodule->txSize = txSize;
    CANmodule->useCANrxFilters = enBooleanFalse;		/* microcontroller dependent */
    CANmodule->bufferInhibitFlag = enBooleanFalse;
    CANmodule->firstCANtxMessage = enBooleanTrue;
    CANmodule->CANtxCount = 0U;
    CANmodule->errOld = 0U;
    CANmodule->em = NULL;

    for(i=0U; i<rxSize; i++)
	{
        rxArray[i].ident = 0U;
        rxArray[i].pFunct = NULL;
    }
    for(i=0U; i<txSize; i++)
	{
        txArray[i].bufferFull = enBooleanFalse;
    }


    /* Configure CAN module registers */
    /* Configure CAN timing */
	// CAN_Init(CANmodule->hw.CANbaseAddress, (PFdword)CANbitRate*1000);
	
    /* Configure CAN module hardware filters */
    if(CANmodule->useCANrxFilters)
	{
        /* CAN module filters are used, they will be configured with */
        /* pfCoCanRxBufferInit() functions, called by separate CANopen */
        /* init functions. */
        /* Configure all masks so, that received message must match filter */
#ifdef __LPC17XX__
    //    CAN_SetAFMode(LPC_CANAF, CAN_Normal);
#elif defined(__LPC177X_8X__)
    //    CAN_SetAFMode(CAN_NORMAL);
#endif
    }
    else
	{
	    /* CAN module filters are not used, all messages with standard 11-bit */
        /* identifier will be received */
        /* Configure mask 0 so, that all messages with standard identifier are accepted */
#ifdef __LPC17XX__
//        CAN_SetAFMode(LPC_CANAF, CAN_AccBP);
#elif defined(__LPC177X_8X__)
//        CAN_SetAFMode(CAN_ACC_BP);
#endif
	}

    /* configure CAN interrupt registers */
//  CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_RIE, ENABLE);
//   CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_TIE1, ENABLE);
//  CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_TIE2, ENABLE);
//	CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_TIE3, ENABLE);
    /*CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_EIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_DOIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_EPIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_ALIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_BEIE, ENABLE);*/

    return enCO_ERROR_NO;
}


/******************************************************************************/
void pfCoCanModuleDisable(PFCoCanModule *CANmodule)
{
    /* turn off the module */
  //  CAN_ModeConfig(CANmodule->hw.CANbaseAddress, CAN_RESET_MODE, ENABLE);
  //  CAN_DeInit(CANmodule->hw.CANbaseAddress);
}


/******************************************************************************/
/*PFword CO_CANrxMsg_readIdent(const PFCoCanRxMsg *rxMsg){
    return (PFword) rxMsg->ident;
}*/


/******************************************************************************/
PFEnCoReturnError pfCoCanRxBufferInit(
        PFCoCanModule         *CANmodule,
        PFword                index,
        PFword                ident,
        PFword                mask,
        PFEnBoolean           rtr,
        void                  *object,
        void                  (*pFunct)(void *object, const PFCoCanRxMsg *message))
{
    PFEnCoReturnError ret = enCO_ERROR_NO;

    if((CANmodule!=NULL) && (object!=NULL) && (pFunct!=NULL) && (index < CANmodule->rxSize))
	{
        /* buffer, which will be configured */
        PFCoCanRx *buffer = &CANmodule->rxArray[index];

        /* Configure object variables */
        buffer->object = object;
        buffer->pFunct = pFunct;

        /* CAN identifier and CAN mask */
        buffer->ident = ident & 0x07FFU;
        if(rtr)
		{
            buffer->ident |= 0x0800U;
        }
        buffer->mask = (mask & 0x07FFU) | 0x0800U;

/*#define LPC_CANAF_SFF_CONTR(v)  ((v) & 0x7000 >> 13)
#define LPC_CANAF_SFF_NU        (0x800)
#define LPC_CANAF_SFF_ID(v)     ((v) & 0x7FF)

        PFdword i;
        PFEnBoolean bFiltFound = enBooleanFalse;
        for(i = 0; i < LPC_CANAF->SFF_GRP_sa / sizeof(PFdword); i++)
        {
            PFword id = LPC_CANAF_RAM->mask[i] & 0xFFFF;
            if(LPC_CANAF_SFF_CONTR(id) == 1 && (id & LPC_CANAF_SFF_NU) == 0 && LPC_CANAF_SFF_ID(id) == (ident & 0x07FFU))
            {
                bFiltFound = enBooleanTrue;
                break;
            }
        }

        if(!bFiltFound)*/{
            /* Set CAN hardware module filter and mask. */
            PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
            if(CANmodule->useCANrxFilters)
			{
            //    if(CAN_LoadExplicitEntry(CANmodule->hw.CANbaseAddress, ident & 0x07FFU, STD_ID_FORMAT) != CAN_OK)
			//	{
                    CANmodule->useCANrxFilters = enBooleanFalse;
            #ifdef __LPC17XX__
            //        CAN_SetAFMode(LPC_CANAF, CAN_AccBP);
            #elif defined(__LPC177X_8X__)
            //        CAN_SetAFMode(CAN_ACC_BP);
            #endif
            //    }
            }
            pfCO_hal_InterruptsRestore(flags);
        }
    }
    else
	{
        ret = enCO_ERROR_ILLEGAL_ARGUMENT;
    }
    return ret;
}

/******************************************************************************/
PFCoCanTx *pfCoCanTxBufferInit(
        PFCoCanModule         *CANmodule,
        PFword                index,
        PFword                ident,
        PFEnBoolean           rtr,
        PFbyte                noOfBytes,
        PFEnBoolean           syncFlag)
{
    PFCoCanTx *buffer = NULL;
	
    if((CANmodule != NULL) && (index < CANmodule->txSize))
	{
        /* get specific buffer */
        buffer = &CANmodule->txArray[index];

        /* CAN identifier, DLC and rtr, bit aligned with CAN module transmit buffer.
         * Microcontroller specific. */
        buffer->ident = ident & 0x07FFU;					/* 11-bit identifier */
        buffer->len = noOfBytes > 8 ? 8 : noOfBytes;		/* number of data bytes */
        buffer->type = rtr ? 1 : 0;		/* Frame Format */
        buffer->bufferFull = enBooleanFalse;						
        buffer->syncFlag = syncFlag;
    }

    return buffer;				/* returns an Initialized Tx Buffer */
}

//#include "user_board.h"

/******************************************************************************/
PFEnCoReturnError pfCoCanSend(PFCoCanModule *CANmodule, PFCoCanTx *buffer)
{
    PFEnCoReturnError err = enCO_ERROR_NO;

    PFCanMsgHeader Head;
    PFdword stat;
    PFbyte i;
    PFEnStatus res = enStatusError;

    /* Verify overflow */
    if(buffer->bufferFull)
	{
        if(!CANmodule->firstCANtxMessage)
		{
            /* don't set error, if bootup message is still on buffers */
            pfCoErrorReport((PFCoEm*)CANmodule->em, CO_EM_CAN_TX_OVERFLOW, CO_EMC_CAN_OVERRUN, buffer->ident);
        }
        err = enCO_ERROR_TX_OVERFLOW;
    }

    PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
    /* if CAN TX buffer is free, copy message to it */
    pfCan1GetCtrlStatus(&stat);
	
    if(stat & (CAN_SR_TBS1 | CAN_SR_TBS2 | CAN_SR_TBS3))
	{
        CANmodule->bufferInhibitFlag = buffer->syncFlag;
        /* copy message and txRequest */
        Head.id = buffer->ident;
        Head.remoteFrame = buffer->type;
        Head.frameFormat = enCanFrameStandard;
		
        res = pfCan1Write(&Head, buffer->data, buffer->len);
		
#if CO_NO_SYNC > 0
        if(res == enStatusSuccess && buffer->syncFlag)
		{
            /* @AGV: some kind of hack: CAN_SendMsg will use the first free TX buffer,
             * we need to know it to set appropriate sync flag */
            if(stat & CAN_SR_TBS1)
                CANmodule->hw.flags |= LPC17XX_SYNC_TXB1;
            else if(stat & CAN_SR_TBS2)
                CANmodule->hw.flags |= LPC17XX_SYNC_TXB2;
            else if(stat & CAN_SR_TBS3)
                CANmodule->hw.flags |= LPC17XX_SYNC_TXB3;
        }
#endif
    }
    /* if no buffer is free or error, message will be sent by interrupt */
    if(res != enStatusSuccess)
	{
        buffer->bufferFull = enBooleanTrue;	
        CANmodule->CANtxCount++;
    }
    pfCO_hal_InterruptsRestore(flags);

    return err;
}


#if CO_NO_SYNC > 0
/******************************************************************************/
void pfCoCanClearPendingSyncPDOs(PFCoCanModule *CANmodule)
{
    PFdword tpdoDeleted = 0U;

    PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
	
    /* Abort message from CAN module, if there is synchronous TPDO. */
    if(CANmodule->hw.flags & (LPC17XX_SYNC_TXB1 | LPC17XX_SYNC_TXB2 | LPC17XX_SYNC_TXB3) &&
        CANmodule->bufferInhibitFlag)
	{
        /* clear TXREQ */
        if(CANmodule->hw.flags & LPC17XX_SYNC_TXB1)
		{
            pfCan1SetCommand(CAN_CMR_AT | CAN_CMR_STB1,enBooleanTrue);
        }
        if(CANmodule->hw.flags & LPC17XX_SYNC_TXB2)
		{
            pfCan1SetCommand(CAN_CMR_AT | CAN_CMR_STB2,enBooleanTrue);
        }
        
		if(CANmodule->hw.flags & LPC17XX_SYNC_TXB3)
		{
            pfCan1SetCommand(CAN_CMR_AT | CAN_CMR_STB3,enBooleanTrue);
        }
        CANmodule->hw.flags = 0;
        CANmodule->bufferInhibitFlag = enBooleanFalse;
        tpdoDeleted = 1U;
    }
    /* delete also pending synchronous TPDOs in TX buffers */
    if(CANmodule->CANtxCount != 0U)
	{
        PFword i;
        PFCoCanTx *buffer = &CANmodule->txArray[0];
        for(i = CANmodule->txSize; i > 0U; i--){
            if(buffer->bufferFull)
			{
                if(buffer->syncFlag)
				{
                    buffer->bufferFull = enBooleanFalse;
                    CANmodule->CANtxCount--;
                    tpdoDeleted = 2U;
                }
            }
            buffer++;
        }
    }
    pfCO_hal_InterruptsRestore(flags);

    if(tpdoDeleted != 0U)
	{
        pfCoErrorReport((PFCoEm*)CANmodule->em, PFCoEmPDO_OUTSIDE_WINDOW, CO_EMC_COMMUNICATION, tpdoDeleted);
    }
}
#endif

/******************************************************************************/
void pfCoCanVerifyErrors(PFCoCanModule *CANmodule)
{
    PFword rxErrors, txErrors, overflow;
    PFCoEm* em = (PFCoEm*)CANmodule->em;
    PFdword err;
    PFdword stat;

    pfCan1GetCtrlStatus(&stat);
    rxErrors = (PFbyte)((stat&0xFF) >> 16);
    txErrors = (PFbyte)((stat&0xFF) >> 24);
	
    if(stat & CAN_GSR_BS) 
		txErrors = 256; /*bus off*/
    overflow = stat & CAN_GSR_DOS ? 1 : 0;

    err = (((PFdword)txErrors << 16) | ((PFdword)rxErrors << 8) | ((PFdword)overflow));

    if(CANmodule->errOld != err)
	{
        CANmodule->errOld = err;

        if(txErrors >= 256U)
		{                               /* bus off */
            pfCoErrorReport(em, CO_EM_CAN_TX_BUS_OFF, CO_EMC_BUS_OFF_RECOVERED, err);
        }
        else
		{                                               /* not bus off */
            pfCoErrorReset(em, CO_EM_CAN_TX_BUS_OFF, err);

            if((rxErrors >= 96U) || (txErrors >= 96U))
			{     /* bus warning */
                pfCoErrorReport(em, CO_EM_CAN_BUS_WARNING, CO_EMC_NO_ERROR, err);
            }

            if(rxErrors >= 128U)
			{                           /* RX bus passive */
                pfCoErrorReport(em, CO_EM_CAN_RX_BUS_PASSIVE, CO_EMC_CAN_PASSIVE, err);
            }
            else
			{
                pfCoErrorReset(em, CO_EM_CAN_RX_BUS_PASSIVE, err);
            }

            if(txErrors >= 128U)
			{                           /* TX bus passive */
                if(!CANmodule->firstCANtxMessage)
				{
                    pfCoErrorReport(em, CO_EM_CAN_TX_BUS_PASSIVE, CO_EMC_CAN_PASSIVE, err);
                }
            }
            else
			{
                PFEnBoolean isError = pfCoIsError(em, CO_EM_CAN_TX_BUS_PASSIVE);
                if(isError)
				{
                    pfCoErrorReset(em, CO_EM_CAN_TX_BUS_PASSIVE, err);
                    pfCoErrorReset(em, CO_EM_CAN_TX_OVERFLOW, err);
                }
            }

            if((rxErrors < 96U) && (txErrors < 96U)){       /* no error */
                pfCoErrorReset(em, CO_EM_CAN_BUS_WARNING, err);
            }
        }

        if(overflow != 0U)
		{                                 /* CAN RX bus overflow */
            pfCoErrorReport(em, CO_EM_CAN_RXB_OVERFLOW, CO_EMC_CAN_OVERRUN, err);
        }
    }
}

/******************************************************************************/
void pfCoCanInterrupt(PFCoCanModule *CANmodule)
{
    PFdword stat;
	pfCan1GetIntStatus(&stat);

    /* receive interrupt and processing */
    if(stat & CAN_ICR_RI)
	{
        PFCanMessage msg;
		PFCoCanRxMsg rcvMsg;      	/* pointer to received message in CAN module */
        PFword index;             /* index of received message */
        PFdword rcvMsgIdent;       /* identifier of the received message */
        PFCoCanRx *buffer = NULL;  /* receive message buffer from PFCoCanModule object. */
        PFEnBoolean msgMatched = enBooleanFalse;
        PFbyte i;

        if(CANmodule->useCANrxFilters)
		{
            /* read filter index before recv buffer will be released */
/*#ifdef __LPC17XX__
            index = CAN_RFS_ID_INDEX(CANmodule->hw.CANbaseAddress->RFS);
#elif defined(__LPC177X_8X__)
            LPC_CAN_TypeDef* pCan = CANmodule->hw.CANbaseAddress == CAN_ID_1 ? LPC_CAN1 : LPC_CAN2;
            index = CAN_RFS_ID_INDEX(pCan->RFS);
#endif*/
        }

        if(pfCan1Read(&msg) == enStatusSuccess)
        {
            rcvMsgIdent = msg.id;
            if(msg.remoteFrame == 1)
                rcvMsgIdent |= 0x0800;
#if 0
            if(CANmodule->useCANrxFilters)
			{
                /* CAN module filters are used. Message with known 11-bit identifier has */
                /* been received */
                //index = 0;  /* get index of the received message here. Or something similar */
                if(index < CANmodule->rxSize)
				{
                    buffer = &CANmodule->rxArray[index];
                    /* verify also RTR */
                    if(((rcvMsgIdent ^ buffer->ident) & buffer->mask) == 0U)
					{
                        msgMatched = enBooleanTrue;
                    }
                }
            }
            else
#endif
            {
                /* CAN module filters are not used, message with any standard 11-bit identifier */
                /* has been received. Search rxArray form CANmodule for the same CAN-ID. */
                buffer = &CANmodule->rxArray[0];
                for(index = CANmodule->rxSize; index > 0U; index--)
				{
                    if(((rcvMsgIdent ^ buffer->ident) & buffer->mask) == 0U)
					{
                        msgMatched = enBooleanTrue;
					    break;
                    }
                    buffer++;
                }
            }
			/* Search the rxArray of PFCoCanRx for the same CAN-ID node for communication with */
            /* Call specific function, which will process the message */
            if(msgMatched && (buffer != NULL) && (buffer->pFunct != NULL))
			{
                rcvMsg.DLC = msg.length;
                for(i = 0; i < msg.length && i < 8; i++)
                    rcvMsg.data[i] = msg.data[i];

				buffer->pFunct(buffer->object, &rcvMsg);
            }
            /* Clear interrupt flag (no need) */
			/* Can be done using functions in hal.h */
        }
    }
/* transmit interrupt and processing*/
    
	if(stat & (CAN_ICR_TI1 | CAN_ICR_TI2 | CAN_ICR_TI3))
	{
		PFCanMsgHeader head;
        /* Clear interrupt flag (no need) */
	#if CO_NO_SYNC > 0
        if(stat & CAN_ICR_TI1)
            CANmodule->hw.flags &= ~LPC17XX_SYNC_TXB1;
        else if(stat & CAN_ICR_TI2)
            CANmodule->hw.flags &= ~LPC17XX_SYNC_TXB2;
        else if(stat & CAN_ICR_TI3)
            CANmodule->hw.flags &= ~LPC17XX_SYNC_TXB3;
	#endif
        /* First CAN message (bootup) was sent successfully */
        CANmodule->firstCANtxMessage = enBooleanFalse;
        /* clear flag from previous message */
        CANmodule->bufferInhibitFlag = enBooleanFalse;
        /* Are there any new messages waiting to be send */
        if(CANmodule->CANtxCount > 0U)
		{
            PFword i, k;             /* index of transmitting message */
            PFCanMessage msg;		   /* CAN Message format */

            /* first buffer */
            PFCoCanTx *buffer = &CANmodule->txArray[0];
            /* search through whole array of pointers to transmit message buffers. */
            for(i = CANmodule->txSize; i > 0U; i--)
			{
                /* if message buffer is full, send it. */
                if(buffer->bufferFull)
				{
                    buffer->bufferFull = enBooleanFalse;
                    CANmodule->CANtxCount--;

                    /* Copy message to CAN buffer */
                    CANmodule->bufferInhibitFlag = buffer->syncFlag;

                    head.id = buffer->ident;
                    head.remoteFrame = buffer->type;
                    head.frameFormat = enCanFrameStandard;

				#if CO_NO_SYNC > 0
                    PFdword txStat = 0;
                    if(buffer->syncFlag)
                        pfCan1GetCtrlStatus(&txStat);
				#endif
                    if(pfCan1Write(&head,buffer->data,buffer->len) != enStatusSuccess)
                    {
                        pfCoErrorReport((PFCoEm*)CANmodule->em, CO_EM_CAN_TX_OVERFLOW, CO_EMC_CAN_OVERRUN, buffer->ident);
                    }
				#if CO_NO_SYNC > 0
                    else if(buffer->syncFlag)
					{
                        /* @AGV: some kind of hack: CAN_SendMsg will use the first free TX buffer,
                         * we need to know it to set appropriate sync flag */
                        if(txStat & CAN_SR_TBS1)
                            CANmodule->hw.flags |= LPC17XX_SYNC_TXB1;
                        else if(txStat & CAN_SR_TBS2)
                            CANmodule->hw.flags |= LPC17XX_SYNC_TXB2;
                        else if(txStat & CAN_SR_TBS3)
                            CANmodule->hw.flags |= LPC17XX_SYNC_TXB3;
                    }
				#endif
                    break;                      /* exit for loop */
                }
                buffer++;
            }/* end of for loop */

            /* Clear counter if no more messages */
            if(i == 0U)
			{
                CANmodule->CANtxCount = 0U;
            }
        }
    }
    else if(stat & CAN_ICR_EPI)
	{

    }
    else if(stat != 0)
	{
        /* some other interrupt reason */
    }

}


    