/*
 * CANopen Heartbeat consumer object.
 */

#include "prime_framework.h"
#include "prime_CO_driver.h"
#include "prime_CO_SDO.h"
#include "prime_CO_Emergency.h"
#include "prime_CO_NMT_Heartbeat.h"
#include "prime_CO_HBconsumer.h"
#include "prime_CO_OD_cfg.h"
#include "prime_CO_OD.h"

#if CO_NO_HB_CONS > 0

/*
 * Read received message from CAN module.
 *
 * Function will be called (by CAN receive interrupt) every time, when CAN
 * message with correct identifier will be received. For more information and
 * description of parameters see file CO_driver.h.
 */
static void pfCoHbConsReceive(void *object, const PFCoCanRxMsg *msg);
static void pfCoHbConsReceive(void *object, const PFCoCanRxMsg *msg)
{
    PFCoHbConsNode *HBconsNode;

    HBconsNode = (PFCoHbConsNode*) object; /* this is the correct pointer type of the first argument */

    /* verify message length and message overflow (previous message was not processed yet) */
    if((msg->DLC == 1) && !HBconsNode->CANrxNew)
	{
        /* copy data and set 'new message' flag */
        HBconsNode->NMTstate = msg->data[0];
        HBconsNode->CANrxNew = CO_true;
    }
}


/*
 * Configure one monitored node.
 */
static void pfCoHbConsMonitoredNodeConfig(
        PFCoHbConsumer        *HBcons,
        PFbyte                 idx,
        PFdword                HBconsTime)
{
    PFword COB_ID;
    PFword NodeID;
    PFCoHbConsNode *monitoredNode;

    if(idx >= HBcons->numberOfMonitoredNodes) return;

    NodeID = (PFword)((HBconsTime>>16)&0xFF);
    monitoredNode = &HBcons->monitoredNodes[idx];
    monitoredNode->time = (PFword)HBconsTime;
    monitoredNode->NMTstate = 0;
    monitoredNode->monStarted = 0;

    /* is channel used */
    if(NodeID && monitoredNode->time)
	{
        COB_ID = NodeID + 0x700;
    }
    else
	{
        COB_ID = 0;
        monitoredNode->time = 0;
    }

    /* configure Heartbeat consumer CAN reception */
    pfCoCanRxBufferInit(
            HBcons->CANdevRx,
            HBcons->CANdevRxIdxStart + idx,
            COB_ID,
            0x7FF,
            0,
            (void*)&HBcons->monitoredNodes[idx],
            pfCoHbConsReceive);
}


/*
 * OD function for accessing _Consumer Heartbeat Time_ (index 0x1016) from SDO server.
 *
 * For more information see file CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODF_1016(PFCoODFarg *ODF_arg);
static PFEnCoSdoAbortCode pfCoODF_1016(PFCoODFarg *ODF_arg)
{
    PFCoHbConsumer *HBcons;
    PFdword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    HBcons = (PFCoHbConsumer*) ODF_arg->object;
    value = pfCoGetUint32(ODF_arg->data);

    if(!ODF_arg->reading)
	{
        PFbyte NodeID;
        PFword HBconsTime;

        NodeID = (value >> 16U) & 0xFFU;
        HBconsTime = value & 0xFFFFU;

        if((value & 0xFF800000U) != 0)
		{
            ret = enCO_SDO_AB_PRAM_INCOMPAT;
        }
        else if((HBconsTime != 0) && (NodeID != 0))
		{
            PFbyte i;
            /* there must not be more entries with same index and time different than zero */
            for(i = 0U; i<HBcons->numberOfMonitoredNodes; i++)
			{
                PFdword objectCopy = CO_OD_getConsumerHeartbeatTime(HBcons->HBconsTime, i);
                PFbyte NodeIDObj = (objectCopy >> 16U) & 0xFFU;
                PFword HBconsTimeObj = objectCopy & 0xFFFFU;
                if(((ODF_arg->subIndex-1U) != i) && (HBconsTimeObj != 0) && (NodeID == NodeIDObj))
				{
                    ret = enCO_SDO_AB_PRAM_INCOMPAT;
                }
            }
        }
        else
		{
            ret = enCO_SDO_AB_NONE;
        }

        /* Configure */
        if(ret == enCO_SDO_AB_NONE)
		{
            pfCoHbConsMonitoredNodeConfig(HBcons, ODF_arg->subIndex-1U, value);
        }
    }

    return ret;
}


/******************************************************************************/
PFint16 pfCoHbConsumerInit(
        PFCoHbConsumer        *HBcons,
        PFCoEm                *em,
        PFCoSdo               *SDO,
        const PFdword          HBconsTime[],
        PFCoHbConsNode         monitoredNodes[],
        PFbyte                 numberOfMonitoredNodes,
        PFCoCanModule         *CANdevRx,
        PFword                CANdevRxIdxStart)
{
    PFbyte i;

    /* Configure object variables */
    HBcons->em = em;
    HBcons->HBconsTime = HBconsTime;
    HBcons->monitoredNodes = monitoredNodes;
    HBcons->numberOfMonitoredNodes = numberOfMonitoredNodes;
    HBcons->allMonitoredOperational = 0;
    HBcons->CANdevRx = CANdevRx;
    HBcons->CANdevRxIdxStart = CANdevRxIdxStart;

    for(i=0; i<HBcons->numberOfMonitoredNodes; i++)
        pfCoHbConsMonitoredNodeConfig(HBcons, i, CO_OD_getConsumerHeartbeatTime(HBcons->HBconsTime, i));

    /* Configure Object dictionary entry at index 0x1016 */
    pfCoODConfigure(SDO, OD_H1016_CONSUMER_HB_TIME, pfCoODF_1016, (void*)HBcons, 0, 0);

    return enCO_ERROR_NO;
}


/******************************************************************************/
void pfCoHbConsumerProcess(
        PFCoHbConsumer        *HBcons,
        PFEnBoolean               NMTisPreOrOperational,
        PFword                timeDifference_ms)
{
    PFbyte i;
    PFbyte AllMonitoredOperationalCopy;
    PFCoHbConsNode *monitoredNode;

    AllMonitoredOperationalCopy = 5;
    monitoredNode = &HBcons->monitoredNodes[0];

    if(NMTisPreOrOperational)
	{
        for(i=0; i<HBcons->numberOfMonitoredNodes; i++)
		{
            if(monitoredNode->time)
			{/* is node monitored */
                /* Verify if new Consumer Heartbeat message received */
                if(monitoredNode->CANrxNew)
				{
                    if(monitoredNode->NMTstate)
					{
                        /* not a bootup message */
                        monitoredNode->monStarted = 1;
                        monitoredNode->timeoutTimer = 0;  /* reset timer */
                        timeDifference_ms = 0;
                    }
                    monitoredNode->CANrxNew = CO_false;
                }
                /* Verify timeout */
                if(monitoredNode->timeoutTimer < monitoredNode->time) monitoredNode->timeoutTimer += timeDifference_ms;

                if(monitoredNode->monStarted)
				{
                    if(monitoredNode->timeoutTimer >= monitoredNode->time)
					{
                        pfCoErrorReport(HBcons->em, CO_EM_HEARTBEAT_CONSUMER, CO_EMC_HEARTBEAT, i);
                        monitoredNode->NMTstate = 0;
                    }
                    else if(monitoredNode->NMTstate == 0)
					{
                        /* there was a bootup message */
                        pfCoErrorReport(HBcons->em, CO_EM_HB_CONSUMER_REMOTE_RESET, CO_EMC_HEARTBEAT, i);
                    }
                }
                if(monitoredNode->NMTstate != enCO_NMT_OPERATIONAL)
                    AllMonitoredOperationalCopy = 0;
            }
            monitoredNode++;
        }
    }
    else{ /* not in (pre)operational state */
        for(i=0; i<HBcons->numberOfMonitoredNodes; i++){
            monitoredNode->NMTstate = 0;
            monitoredNode->CANrxNew = CO_false;
            monitoredNode->monStarted = 0;
            monitoredNode++;
        }
        AllMonitoredOperationalCopy = 0;
    }
    HBcons->allMonitoredOperational = AllMonitoredOperationalCopy;
}

#endif
