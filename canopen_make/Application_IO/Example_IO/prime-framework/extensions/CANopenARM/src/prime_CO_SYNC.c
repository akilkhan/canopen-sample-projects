//done
/*
 * CANopen SYNC object.
 */	

#include "prime_CO_driver.h"
#include "prime_CO_SDO.h"
#include "prime_CO_Emergency.h"
#include "prime_CO_NMT_Heartbeat.h"
#include "prime_CO_SYNC.h"
#include "prime_CO_OD_cfg.h"

#if CO_NO_SYNC > 0
/*
 * Read received message from CAN module.
 *
 * Function will be called (by CAN receive interrupt) every time, when CAN
 * message with correct identifier will be received. For more information and
 * description of parameters see file prime_CO_driver.h.
 */
static void pfCoSyncReceive(void *object, const PFCoCanRxMsg *msg)
{
    PFCoSync *SYNC;
    PFbyte operState;
    PFEnBoolean err = enBooleanFalse;

    SYNC = (PFCoSync*)object;   /* this is the correct pointer type of the first argument */
    operState = *SYNC->operatingState;

    if((operState == enCO_NMT_OPERATIONAL) || (operState == enCO_NMT_PRE_OPERATIONAL))
	{
        if(SYNC->counterOverflowValue != 0)
		{
            if(msg->DLC != 1U)
			{
                SYNC->receiveError = (PFword)msg->DLC | 0x0100U;
                err = enBooleanTrue;
            }
            else
			{
                SYNC->counter = msg->data[0];
            }
        }
        else
		{
            if(msg->DLC != 0U)
			{
                SYNC->receiveError = (PFword)msg->DLC | 0x0200U;
                err = enBooleanTrue;
            }
        }

        if(!err)
		{
            if(operState == enCO_NMT_OPERATIONAL)
			{
                SYNC->running = enBooleanTrue;
            }
            SYNC->timer = 0;
        }
    }
}

/*
 * Function for accessing _COB ID SYNC Message_ (index 0x1005) from SDO server.
 *
 * For more information see file Prime_CO_SDO.h.
 */
 
static PFEnCoSdoAbortCode pfCoODF_1005(PFCoODFarg *ODF_arg)
{
    PFCoSync *SYNC;
    PFdword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    SYNC = (PFCoSync*) ODF_arg->object;
    value = pfCoGetUint32(ODF_arg->data);

    if(!ODF_arg->reading)
	{
        PFbyte configureSyncProducer = 0;

        /* only 11-bit CAN identifier is supported */
        if(value & 0x20000000UL)
		{
            ret = enCO_SDO_AB_INVALID_VALUE;
        }
        else
		{
            /* is 'generate Sync messge' bit set? */
            if(value & 0x40000000UL)
			{
                /* if bit was set before, value can not be changed */
                if(SYNC->isProducer)
				{
                    ret = enCO_SDO_AB_DATA_DEV_STATE;
                }
                else
				{
                    configureSyncProducer = 1;
                }
            }
        }

        /* configure sync producer and consumer */
        if(ret == enCO_SDO_AB_NONE)
		{
            SYNC->COB_ID = (PFword)(value & 0x7FFU);

            if(configureSyncProducer)
			{
                PFbyte len = 0U;
                if(SYNC->counterOverflowValue != 0U)
				{
                    len = 1U;
                    SYNC->counter = 0U;
                    SYNC->running = enBooleanFalse;
                    SYNC->timer = 0U;
                }
                SYNC->CANtxBuff = pfCoCanTxBufferInit
				(
                        SYNC->CANdevTx,         /* CAN device */
                        SYNC->CANdevTxIdx,      /* index of specific buffer inside CAN module */
                        SYNC->COB_ID,           /* CAN identifier */
                        0,                      /* rtr */
                        len,                    /* number of data bytes */
                        0);                     /* synchronous message flag bit */
                SYNC->isProducer = enBooleanTrue;
            }
            else
			{
                SYNC->isProducer = enBooleanFalse;
            }

            pfCoCanRxBufferInit(
                    SYNC->CANdevRx,         /* CAN device */
                    SYNC->CANdevRxIdx,      /* rx buffer index */
                    SYNC->COB_ID,           /* CAN identifier */
                    0x7FF,                  /* mask */
                    0,                      /* rtr */
                    (void*)SYNC,            /* object passed to receive function */
                    pfCoSyncReceive);       /* this function will process received message */
        }
    }

    return ret;
}


/*
 * Function for accessing _Communication cycle period_ (index 0x1006) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODF_1006(PFCoODFarg *ODF_arg)
{
    PFCoSync *SYNC;
    PFdword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    SYNC = (PFCoSync*) ODF_arg->object;
    value = pfCoGetUint32(ODF_arg->data);

    if(!ODF_arg->reading)
	{
        /* period transition from 0 to something */
        if((SYNC->periodTime == 0) && (value != 0))
		{
            SYNC->counter = 0;
        }

        SYNC->periodTime = value;
        SYNC->periodTimeoutTime = (value / 2U) * 3U;
        /* overflow? */
        if(SYNC->periodTimeoutTime < value)
		{
            SYNC->periodTimeoutTime = 0xFFFFFFFFUL;
        }

        SYNC->running = enBooleanFalse;
        SYNC->timer = 0;
    }

    return ret;
}


/**
 * Function for accessing _Synchronous counter overflow value_ (index 0x1019) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODF_1019(PFCoODFarg *ODF_arg)
{
    PFCoSync *SYNC;
    PFbyte value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    SYNC = (PFCoSync*) ODF_arg->object;
    value = ODF_arg->data[0];

    if(!ODF_arg->reading)
	{
        PFbyte len = 0U;

        if(SYNC->periodTime)
		{
            ret = enCO_SDO_AB_DATA_DEV_STATE;
        }
        else
		{
            SYNC->counterOverflowValue = value;
            if(value != 0)
			{
                len = 1U;
            }

            SYNC->CANtxBuff = pfCoCanTxBufferInit(
                    SYNC->CANdevTx,         /* CAN device */
                    SYNC->CANdevTxIdx,      /* index of specific buffer inside CAN module */
                    SYNC->COB_ID,           /* CAN identifier */
                    0,                      /* rtr */
                    len,                    /* number of data bytes */
                    0);                     /* synchronous message flag bit */
        }
    }

    return ret;
}


/******************************************************************************/
PFint16 pfCoSyncInit(
        PFCoSync             *SYNC,
        PFCoEm               *em,
        PFCoSdo              *SDO,
        PFbyte                *operatingState,
        PFdword               COB_ID_SYNCMessage,
        PFdword               communicationCyclePeriod,
        PFbyte                synchronousCounterOverflowValue,
        PFCoCanModule        *CANdevRx,
        PFword                CANdevRxIdx,
        PFCoCanModule        *CANdevTx,
        PFword                CANdevTxIdx)
{
    PFbyte len = 0;

    /* Configure object variables */
    SYNC->isProducer = (COB_ID_SYNCMessage&0x40000000L) ? enBooleanTrue : enBooleanFalse;
    SYNC->COB_ID = COB_ID_SYNCMessage&0x7FF;

    SYNC->periodTime = communicationCyclePeriod;
    SYNC->periodTimeoutTime = communicationCyclePeriod / 2 * 3;
    /* overflow? */
    if(SYNC->periodTimeoutTime < communicationCyclePeriod) SYNC->periodTimeoutTime = 0xFFFFFFFFL;

    SYNC->counterOverflowValue = synchronousCounterOverflowValue;
    if(synchronousCounterOverflowValue) len = 1;

    SYNC->curentSyncTimeIsInsideWindow = enBooleanTrue;

    SYNC->running = enBooleanFalse;
    SYNC->timer = 0;
    SYNC->counter = 0;
    SYNC->receiveError = 0U;

    SYNC->em = em;
    SYNC->operatingState = operatingState;
    SYNC->CANdevRx = CANdevRx;
    SYNC->CANdevRxIdx = CANdevRxIdx;

    /* Configure Object dictionary entry at index 0x1005, 0x1006 and 0x1019 */
    pfCoODConfigure(SDO, OD_H1005_COBID_SYNC,        pfCoODF_1005, (void*)SYNC, 0, 0);
    pfCoODConfigure(SDO, OD_H1006_COMM_CYCL_PERIOD,  pfCoODF_1006, (void*)SYNC, 0, 0);
    pfCoODConfigure(SDO, OD_H1019_SYNC_CNT_OVERFLOW, pfCoODF_1019, (void*)SYNC, 0, 0);

    /* configure SYNC CAN reception */
    pfCoCanRxBufferInit(
            CANdevRx,               /* CAN device */
            CANdevRxIdx,            /* rx buffer index */
            SYNC->COB_ID,           /* CAN identifier */
            0x7FF,                  /* mask */
            0,                      /* rtr */
            (void*)SYNC,            /* object passed to receive function */
            pfCoSyncReceive);       /* this function will process received message */

    /* configure SYNC CAN transmission */
    SYNC->CANdevTx = CANdevTx;
    SYNC->CANdevTxIdx = CANdevTxIdx;
    SYNC->CANtxBuff = pfCoCanTxBufferInit(
            CANdevTx,               /* CAN device */
            CANdevTxIdx,            /* index of specific buffer inside CAN module */
            SYNC->COB_ID,           /* CAN identifier */
            0,                      /* rtr */
            len,                    /* number of data bytes */
            0);                     /* synchronous message flag bit */

    return enCO_ERROR_NO;
}


/******************************************************************************/
PFbyte pfCoSyncProcess(
        PFCoSync              *SYNC,
        PFdword                timeDifference_us,
        PFdword                ObjDict_synchronousWindowLength)
{
    PFbyte ret = 0;
    PFdword timerNew;

    if(*SYNC->operatingState == enCO_NMT_OPERATIONAL || *SYNC->operatingState == enCO_NMT_PRE_OPERATIONAL)
	{
        /* was SYNC just received */
        if(SYNC->running && SYNC->timer == 0)
            ret = 1;

        /* update sync timer, no overflow */
        PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
        timerNew = SYNC->timer + timeDifference_us;
        if(timerNew > SYNC->timer) SYNC->timer = timerNew;
        pfCO_hal_InterruptsRestore(flags);

        /* SYNC producer */
        if(SYNC->isProducer && SYNC->periodTime)
		{
            if(SYNC->timer >= SYNC->periodTime)
			{
                if(++SYNC->counter > SYNC->counterOverflowValue) SYNC->counter = 1;
                SYNC->running = enBooleanTrue;
                SYNC->timer = 0;
                SYNC->CANtxBuff->data[0] = SYNC->counter;
                pfCoCanSend(SYNC->CANdevTx, SYNC->CANtxBuff);
                ret = 1;
            }
        }

        /* Synchronous PDOs are allowed only inside time window */
        if(ObjDict_synchronousWindowLength)
		{
            if(SYNC->timer > ObjDict_synchronousWindowLength)
			{
                if(SYNC->curentSyncTimeIsInsideWindow)
				{
                    ret = 2;
                }
                SYNC->curentSyncTimeIsInsideWindow = enBooleanFalse;
            }
            else
			{
                SYNC->curentSyncTimeIsInsideWindow = enBooleanTrue;
            }
        }
        else
		{
            SYNC->curentSyncTimeIsInsideWindow = enBooleanTrue;
        }

        /* Verify timeout of SYNC */
        if(SYNC->periodTime && SYNC->timer > SYNC->periodTimeoutTime && *SYNC->operatingState == enCO_NMT_OPERATIONAL)
            pfCoErrorReport(SYNC->em, CO_EM_SYNC_TIME_OUT, CO_EMC_COMMUNICATION, SYNC->timer);
    }

    if(*SYNC->operatingState != enCO_NMT_OPERATIONAL)
	{
        SYNC->running = enBooleanFalse;
    }

    /* verify error from receive function */
    if(SYNC->receiveError != 0U)
	{
        pfCoErrorReport(SYNC->em, CO_EM_SYNC_LENGTH, CO_EMC_SYNC_DATA_LENGTH, (PFdword)SYNC->receiveError);
        SYNC->receiveError = 0U;
    }

    return ret;
}
#endif
