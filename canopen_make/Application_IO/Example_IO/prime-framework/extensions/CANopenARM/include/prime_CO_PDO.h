/**
 * CANopen Process Data Object protocol.
 */


#ifndef PF_CO_PDO
#define PF_CO_PDO

#include "prime_CO_OD_cfg.h"

/**
 * @defgroup PF_CO_PDO 
 * @ingroup PF_CO_CANopen
 * @{
 *
 * CANopen Process Data Object protocol.
 *
 * Process data objects are used for real-time data transfer with no protocol
 * overhead.
 *
 * TPDO with specific identifier is transmitted by one device and recieved by
 * zero or more devices as RPDO. PDO communication parameters(COB-ID,
 * transmission type, etc.) are in Object Dictionary at index 0x1400+ and
 * 0x1800+. PDO mapping parameters (size and contents of the PDO) are in Object
 * Dictionary at index 0x1600+ and 0x1A00+.
 *
 * Features of the PDO as implemented here, in CANopenNode:
 *  - Dynamic PDO mapping.
 *  - Map granularity of one byte.
 *  - After RPDO is received from CAN bus, its data are copied to buffer.
 *    Function pfCoRpdoProcess() (called by application) copies data to
 *    mapped objects in Object Dictionary.
 *  - Function pfCoTpdoProcess() (called by application) sends TPDO if
 *    necessary. There are possible different transmission types, including
 *    automatic detection of Change of State of specific variable.
 */

/**
 * RPDO communication parameter. The same as record from Object dictionary (index 0x1400+).
 */
typedef struct{
    PFbyte             maxSubIndex;    /**< Equal to 2 */
    /** Communication object identifier for message received. Meaning of the specific bits:
        - Bit  0-10: COB-ID for PDO, to change it bit 31 must be set.
        - Bit 11-29: set to 0 for 11 bit COB-ID.
        - Bit 30:    If true, rtr are NOT allowed for PDO.
        - Bit 31:    If true, node does NOT use the PDO. */ 
    PFdword            COB_IDUsedByRPDO;
    /** Transmission type. Values:
        - 0-240:   Reciving is synchronous, process after next reception of the SYNC object.
        - 241-253: Not used.
        - 254:     Manufacturer specific.
        - 255:     Asynchronous. */
    PFbyte             transmissionType;
}PFCoRpdoCommPar;


/**
 * RPDO mapping parameter. The same as record from Object dictionary (index 0x1600+).
 */
typedef struct{
    /** Actual number of mapped objects from 0 to 8. To change mapped object,
    this value must be 0. */
    PFbyte             numberOfMappedObjects;
    /** Location and size of the mapped object. Bit meanings `0xIIIISSLL`:
        - Bit  0-7:  Data Length in bits.
        - Bit 8-15:  Subindex from object distionary.
        - Bit 16-31: Index from object distionary. */
    PFdword            mappedObject1;
    PFdword            mappedObject2;  /**< Same */
    PFdword            mappedObject3;  /**< Same */
    PFdword            mappedObject4;  /**< Same */
    PFdword            mappedObject5;  /**< Same */
    PFdword            mappedObject6;  /**< Same */
    PFdword            mappedObject7;  /**< Same */
    PFdword            mappedObject8;  /**< Same */
}PFCoRpdoMapPar;


/**
 * TPDO communication parameter. The same as record from Object dictionary (index 0x1800+).
 */
typedef struct{
    PFbyte             maxSubIndex;    /**< Equal to 6 */
    /** Communication object identifier for transmitting message. Meaning of the specific bits:
        - Bit  0-10: COB-ID for PDO, to change it bit 31 must be set.
        - Bit 11-29: set to 0 for 11 bit COB-ID.
        - Bit 30:    If true, rtr are NOT allowed for PDO.
        - Bit 31:    If true, node does NOT use the PDO. */
    PFdword            COB_IDUsedByTPDO;
    /** Transmission type. Values:
        - 0:       Transmiting is synchronous, specification in device profile.
        - 1-240:   Transmiting is synchronous after every N-th SYNC object.
        - 241-251: Not used.
        - 252-253: Transmited only on reception of Remote Transmission Request.
        - 254:     Manufacturer specific.
        - 255:     Asynchronous, specification in device profile. */
    PFbyte             transmissionType;
    /** Minimum time between transmissions of the PDO in 100micro seconds.
    Zero disables functionality. */
    PFword            inhibitTime;
    /** Not used */
    PFbyte             compatibilityEntry;
    /** Time between periodic transmissions of the PDO in milliseconds.
    Zero disables functionality. */
    PFword            eventTimer;
    /** Used with numbered SYNC messages. Values:
        - 0:       Counter of the SYNC message shall not be processed.
        - 1-240:   The SYNC message with the counter value equal to this value
                   shall be regarded as the first received SYNC message. */
    PFbyte             SYNCStartValue;
}PFCoTpdoCommPar;


/**
 * TPDO mapping parameter. The same as record from Object dictionary (index 0x1A00+).
 */
typedef struct{
    /** Actual number of mapped objects from 0 to 8. To change mapped object,
    this value must be 0. */
    PFbyte             numberOfMappedObjects;
    /** Location and size of the mapped object. Bit meanings `0xIIIISSLL`:
        - Bit  0-7:  Data Length in bits.
        - Bit 8-15:  Subindex from object distionary.
        - Bit 16-31: Index from object distionary. */
    PFdword            mappedObject1;
    PFdword            mappedObject2;  /**< Same */
    PFdword            mappedObject3;  /**< Same */
    PFdword            mappedObject4;  /**< Same */
    PFdword            mappedObject5;  /**< Same */
    PFdword            mappedObject6;  /**< Same */
    PFdword            mappedObject7;  /**< Same */
    PFdword            mappedObject8;  /**< Same */
}PFCoTpdoMapPar;

/**
 * RPDO object.
 */
typedef struct{
    PFCoEm            *em;             /**< From pfCoRpdoInit() */
    PFCoSdo           *SDO;            /**< From pfCoRpdoInit() */
    const PFCoRpdoCommPar *RPDOCommPar;/**< From pfCoRpdoInit() */
    const PFCoRpdoMapPar  *RPDOMapPar; /**< From pfCoRpdoInit() */
    PFbyte            *operatingState; /**< From pfCoRpdoInit() */
    PFbyte             nodeId;         /**< From pfCoRpdoInit() */
    PFword            defaultCOB_ID;  /**< From pfCoRpdoInit() */
    PFbyte             restrictionFlags;/**< From pfCoRpdoInit() */
    /** True, if PDO is enabled and valid */
    PFEnBoolean           valid;
    /** Data length of the received PDO message. Calculated from mapping */
    PFbyte             dataLength;
    /** Pointers to 8 data objects, where PDO will be copied */
    PFbyte            *mapPointer[8];
    /** Variable indicates, if new PDO message received from CAN bus.
    Must be 2-byte variable because of correct alignment of CANrxData. */
    PFword            CANrxNew;
    /** 8 data bytes of the received message. Take care for correct (word) alignment!*/
    PFbyte             CANrxData[8];
    PFCoCanModule     *CANdevRx;       /**< From pfCoRpdoInit() */
    PFword            CANdevRxIdx;    /**< From pfCoRpdoInit() */
}PFCoRpdo;


/**
 * TPDO object.
 */
typedef struct{
    PFCoEm            *em;             /**< From pfCoTpdoInit() */
    PFCoSdo           *SDO;            /**< From pfCoTpdoInit() */
    const PFCoTpdoCommPar *TPDOCommPar;/**< From pfCoTpdoInit() */
    const PFCoTpdoMapPar  *TPDOMapPar; /**< From pfCoTpdoInit() */
    PFbyte            *operatingState; /**< From pfCoTpdoInit() */
    PFbyte             nodeId;         /**< From pfCoTpdoInit() */
    PFword            defaultCOB_ID;  /**< From pfCoTpdoInit() */
    PFbyte             restrictionFlags;/**< From pfCoTpdoInit() */
    PFEnBoolean           valid;          /**< True, if PDO is enabled and valid */
    /** Data length of the transmitting PDO message. Calculated from mapping */
    PFbyte             dataLength;
    /** If application set this flag, PDO will be later sent by
    function pfCoTpdoProcess(). Depends on transmission type. */
    PFbyte             sendRequest;
    /** Pointers to 8 data objects, where PDO will be copied */
    PFbyte            *mapPointer[8];
    /** Each flag bit is connected with one mapPointer. If flag bit
    is true, pfCoTpdoProcess() function will send PDO if
    Change of State is detected on value pointed by that mapPointer */
    PFbyte             sendIfCOSFlags;
#if CO_NO_SYNC > 0
    /** SYNC counter used for PDO sending */
    PFbyte             syncCounter;
    /** Previous timer from PFCoSync */
    PFdword            SYNCtimerPrevious;
#endif
    /** Inhibit timer used for inhibit PDO sending */
    PFword            inhibitTimer;
    /** Event timer used for PDO sending */
    PFword            eventTimer;
    PFCoCanModule     *CANdevTx;       /**< From pfCoTpdoInit() */
    PFCoCanTx         *CANtxBuff;      /**< CAN transmit buffer inside CANdev */
    PFword            CANdevTxIdx;    /**< From pfCoTpdoInit() */
}PFCoTpdo;


/**
 * Initialize RPDO object.
 *
 * Function must be called in the communication reset section.
 *
 * @param RPDO This object will be initialized.
 * @param em Emergency object.
 * @param SDO SDO server object.
 * @param operatingState Pointer to variable indicating CANopen device NMT internal state.
 * @param nodeId CANopen Node ID of this device. If default COB_ID is used, value will be added.
 * @param defaultCOB_ID Default COB ID for this PDO (without NodeId).
 * See #PFEnCoDefaultCanId
 * @param restrictionFlags Flag bits indicates, how PDO communication
 * and mapping parameters are handled:
 *  - Bit1: If true, communication parameters are writeable only in pre-operational NMT state.
 *  - Bit2: If true, mapping parameters are writeable only in pre-operational NMT state.
 *  - Bit3: If true, communication parameters are read-only.
 *  - Bit4: If true, mapping parameters are read-only.
 * @param RPDOCommPar Pointer to _RPDO communication parameter_ record from Object
 * dictionary (index 0x1400+).
 * @param RPDOMapPar Pointer to _RPDO mapping parameter_ record from Object
 * dictionary (index 0x1600+).
 * @param idx_RPDOCommPar Index in Object Dictionary.
 * @param idx_RPDOMapPar Index in Object Dictionary.
 * @param CANdevRx CAN device for PDO reception.
 * @param CANdevRxIdx Index of receive buffer in the above CAN device.
 *
 * @return #PFEnCoReturnError: enCO_ERROR_NO or enCO_ERROR_ILLEGAL_ARGUMENT.
 */
PFint16 pfCoRpdoInit(
        PFCoRpdo              *RPDO,
        PFCoEm                *em,
        PFCoSdo               *SDO,
        PFbyte                *operatingState,
        PFbyte                 nodeId,
        PFword                defaultCOB_ID,
        PFbyte                 restrictionFlags,
        const PFCoRpdoCommPar *RPDOCommPar,
        const PFCoRpdoMapPar  *RPDOMapPar,
        PFword                idx_RPDOCommPar,
        PFword                idx_RPDOMapPar,
        PFCoCanModule         *CANdevRx,
        PFword                CANdevRxIdx);


/**
 * Initialize TPDO object.
 *
 * Function must be called in the communication reset section.
 *
 * @param TPDO This object will be initialized.
 * @param em Emergency object.
 * @param SDO SDO object.
 * @param operatingState Pointer to variable indicating CANopen device NMT internal state.
 * @param nodeId CANopen Node ID of this device. If default COB_ID is used, value will be added.
 * @param defaultCOB_ID Default COB ID for this PDO (without NodeId).
 * See #PFEnCoDefaultCanId
 * @param restrictionFlags Flag bits indicates, how PDO communication
 * and mapping parameters are handled:
 *  - Bit1: If true, communication parameters are writeable only in pre-operational NMT state.
 *  - Bit2: If true, mapping parameters are writeable only in pre-operational NMT state.
 *  - Bit3: If true, communication parameters are read-only.
 *  - Bit4: If true, mapping parameters are read-only.
 * @param TPDOCommPar Pointer to _TPDO communication parameter_ record from Object
 * dictionary (index 0x1400+).
 * @param TPDOMapPar Pointer to _TPDO mapping parameter_ record from Object
 * dictionary (index 0x1600+).
 * @param idx_TPDOCommPar Index in Object Dictionary.
 * @param idx_TPDOMapPar Index in Object Dictionary.
 * @param CANdevTx CAN device used for PDO transmission.
 * @param CANdevTxIdx Index of transmit buffer in the above CAN device.
 *
 * @return #PFEnCoReturnError: enCO_ERROR_NO or enCO_ERROR_ILLEGAL_ARGUMENT.
 */
PFint16 pfCoTpdoInit(
        PFCoTpdo              *TPDO,
        PFCoEm                *em,
        PFCoSdo               *SDO,
        PFbyte                *operatingState,
        PFbyte                 nodeId,
        PFword                defaultCOB_ID,
        PFbyte                 restrictionFlags,
        const PFCoTpdoCommPar *TPDOCommPar,
        const PFCoTpdoMapPar  *TPDOMapPar,
        PFword                idx_TPDOCommPar,
        PFword                idx_TPDOMapPar,
        PFCoCanModule         *CANdevTx,
        PFword                CANdevTxIdx);


/**
 * Verify Change of State of the PDO.
 *
 * Function verifies if variable mapped to TPDO has changed its value. Verified
 * are only variables, which has set attribute _CO_ODA_TPDO_DETECT_COS_ in
 * #PFEnCoSdoODattributes.
 *
 * Function may be called by application just before pfCoTpdoProcess() function,
 * for example: `TPDOx->sendRequest = pfCoTpdoIsCOS(TPDOx); pfCoTpdoProcess(TPDOx, ....`
 *
 * @param TPDO TPDO object.
 *
 * @return True if COS was detected.
 */
PFbyte pfCoTpdoIsCOS(PFCoTpdo *TPDO);


/**
 * Send TPDO message.
 *
 * Function prepares TPDO data from Object Dictionary variables. It should not
 * be called by application, it is called from pfCoTpdoProcess().
 *
 *
 * @param TPDO TPDO object.
 *
 * @return Same as pfCoCanSend().
 */
 
PFint16 pfCoTpdoSend(PFCoTpdo *TPDO);


/**
 * Process received PDO messages.
 *
 * Function must be called cyclically in any NMT state. It copies data from RPDO
 * to Object Dictionary variables if: new PDO receives and PDO is valid and NMT
 * operating state is operational. It does not verify _transmission type_.
 *
 * @param RPDO This object.
 */
void pfCoRpdoProcess(PFCoRpdo *RPDO);


/**
 * Process transmitting PDO messages.
 *
 * Function must be called cyclically in any NMT state. It prepares and sends
 * TPDO if necessary. If Change of State needs to be detected, function
 * pfCoTpdoIsCOS() must be called before.
 *
 * @param TPDO This object.
 * @param SYNC SYNC object. Ignored if NULL.
 * @param timeDifference_100us Time difference from previous function call in [100 * microseconds].
 * @param timeDifference_ms Time difference from previous function call in [milliseconds].
 */
void pfCoTpdoProcess(
        PFCoTpdo              *TPDO,
#if CO_NO_SYNC > 0
        PFCoSync              *SYNC,
#endif
        PFword                timeDifference_100us,
        PFword                timeDifference_ms);


/** @} */
#endif
