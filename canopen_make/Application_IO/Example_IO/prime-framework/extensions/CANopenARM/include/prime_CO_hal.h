/*
 * prime_coHal.h
 *
 */

#ifndef PF_CO_HAL
#define PF_CO_HAL

#include "prime_cpu.h"

/*
 * Support for drivers which uses GPIO and/or SPI, e.g. MCP2515.
 * Stack has own versions of those functions and implies that all HW is initialized before call to pfCoInit().
 * Implement this functions if you need to customize them, e.g. using third party HAL library.
 */
 
CO_WEAK_DECL void pfCO_hal_GpioDevicePinInit(PFbyte devId, PFbyte pinNum, PFbyte dir, PFbyte val);
CO_WEAK_DECL PFbyte pfCO_hal_GpioDevicePinStateGet(PFbyte devId, PFbyte pinNum);
CO_WEAK_DECL PFint8 pfCO_hal_GpioDevicePinStateSet(PFbyte devId, PFbyte pinNum, PFbyte state);
CO_WEAK_DECL PFint8 pfCO_hal_SpiDeviceWrite(PFbyte devId, PFbyte *buf, PFdword len, PFdword tmo);
CO_WEAK_DECL PFint32 pfCO_hal_SpiDeviceRead(PFbyte devId, PFbyte *buf, PFdword len, PFdword tmo);

CO_WEAK_DECL PFCoCpuInterruptsFlags pfCO_hal_InterruptsSaveDisable(void);
CO_WEAK_DECL void pfCO_hal_InterruptsRestore(PFCoCpuInterruptsFlags flags);

#endif /* PF_CO_HAL */
