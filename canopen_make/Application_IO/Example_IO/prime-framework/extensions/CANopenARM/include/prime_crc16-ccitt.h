/**
 * Calculation of CRC 16 CCITT polynomial.
 */

#ifndef PF_CRC16_CCITT
#define PF_CRC16_CCITT

/**
 * @defgroup PF_CO_crc16_ccitt CRC 16 CCITT
 * @ingroup PF_CO_CANopen
 * @{
 *
 * Calculation of CRC 16 CCITT polynomial.
 *
 * Equation:
 *
 * `x^16 + x^12 + x^5 + 1`
 */

/**
 * Calculate CRC sum on block of data.
 *
 * @param block Pointer to block of data.
 * @param blockLength Length of data in bytes;
 * @param crc Initial value (zero for xmodem). If block is split into
 * multiple segments, previous CRC is used as initial.
 *
 * @return Calculated CRC.
 */
 
PFword crc16_ccitt(
        const PFbyte     block[],
        PFuint           blockLength,
        PFword          crc);


/** @} */
#endif
