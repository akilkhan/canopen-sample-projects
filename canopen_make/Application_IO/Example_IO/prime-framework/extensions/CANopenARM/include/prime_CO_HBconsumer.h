/**
 * CANopen Heartbeat consumer protocol.
 */


#ifndef PF_CO_HB_CONS
#define PF_CO_HB_CONS

/**
 * @defgroup PF_CO_HBconsumer Heartbeat consumer
 * @ingroup PF_CO_CANopen
 * @{
 *
 * CANopen Heartbeat consumer protocol.
 *
 * Heartbeat consumer monitors Heartbeat messages from remote nodes. If any
 * monitored node don't send his Heartbeat in specified time, Heartbeat consumer
 * sends emergency message. If all monitored nodes are operational, then
 * variable _allMonitoredOperational_ inside PFCoHbConsumer is set to true.
 * Monitoring starts after the reception of the first HeartBeat (not bootup).
 *
 * @see  @ref PF_CO_NMT_Heartbeat
 */

/**
 * One monitored node inside PFCoHbConsumer.
 */
typedef struct{
    PFbyte            NMTstate;       /**< Of the remote node */
    PFbyte            monStarted;     /**< True after reception of the first Heartbeat mesage */
    PFword            timeoutTimer;   /**< Time since last heartbeat received */
    PFword            time;           /**< Consumer heartbeat time from OD */
    PFEnBoolean       CANrxNew;       /**< True if new Heartbeat message received from the CAN bus */
}PFCoHbConsNode;

/**
 * Heartbeat consumer object.
 *
 * Object is initilaized by pfCoHbConsumerInit(). It contains an array of
 * PFCoHbConsNode objects.
 */
 
typedef struct{
    PFCoEm            *em;             /**< From pfCoHbConsumerInit() */
    const PFdword     *HBconsTime;     /**< From pfCoHbConsumerInit() */
    PFCoHbConsNode    *monitoredNodes; /**< From pfCoHbConsumerInit() */
    PFbyte             numberOfMonitoredNodes; /**< From pfCoHbConsumerInit() */
    /** True, if all monitored nodes are NMT operational or no node is
        monitored. Can be read by the application */
    PFbyte             allMonitoredOperational;
    PFCoCanModule     *CANdevRx;       /**< From pfCoHbConsumerInit() */
    PFword            CANdevRxIdxStart; /**< From pfCoHbConsumerInit() */
}PFCoHbConsumer;


/**
 * Initialize Heartbeat consumer object.
 *
 * Function must be called in the communication reset section.
 *
 * @param HBcons This object will be initialized.
 * @param em Emergency object.
 * @param SDO SDO server object.
 * @param HBconsTime Pointer to _Consumer Heartbeat Time_ array
 * from Object Dictionary (index 0x1016). Size of array is equal to numberOfMonitoredNodes.
 * @param monitoredNodes Pointer to the externaly defined array of the same size
 * as numberOfMonitoredNodes.
 * @param numberOfMonitoredNodes Total size of the above arrays.
 * @param CANdevRx CAN device for Heartbeat reception.
 * @param CANdevRxIdxStart Starting index of receive buffer in the above CAN device.
 * Number of used indexes is equal to numberOfMonitoredNodes.
 *
 * @return #PFEnCoReturnError enCO_ERROR_NO or enCO_ERROR_ILLEGAL_ARGUMENT.
 */
PFint16 pfCoHbConsumerInit(
        PFCoHbConsumer        *HBcons,
        PFCoEm                *em,
        PFCoSdo               *SDO,
        const PFdword          HBconsTime[],
        PFCoHbConsNode         monitoredNodes[],
        PFbyte                 numberOfMonitoredNodes,
        PFCoCanModule         *CANdevRx,
        PFword                CANdevRxIdxStart);

/**
 * Process Heartbeat consumer object.
 *
 * Function must be called cyclically.
 *
 * @param HBcons This object.
 * @param NMTisPreOrOperational True if this node is enNMT_PRE_OPERATIONAL or enNMT_OPERATIONAL.
 * @param timeDifference_ms Time difference from previous function call in [milliseconds].
 */
void pfCoHbConsumerProcess(
        PFCoHbConsumer        *HBcons,
        PFEnBoolean               NMTisPreOrOperational,
        PFword                timeDifference_ms);


/** @} */
#endif
