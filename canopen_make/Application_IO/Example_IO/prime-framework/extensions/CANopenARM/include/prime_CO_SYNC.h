//done
/**
 * CANopen SYNC object protocol.
 *
 */

#ifndef PF_CO_SYNC
#define PF_CO_SYNC

/**
 * @defgroup PF_CO_SYNC SYNC
 * @ingroup PF_CO_CANopen
 * @{
 *
 * CANopen SYNC object protocol.
 *
 * For CAN identifier see #PFCoDefaultCanId
 *
 * SYNC message is used for synchronization of the nodes on network. There is
 * one SYNC producer and zero or more SYNC consumers.
 *
 * ####Contents of SYNC message
 * By default SYNC message has no data. If _Synchronous counter overflow value_
 * from Object dictionary (index 0x1019) is different than 0, SYNC message has
 * one data byte: counter incremented by 1 with every SYNC transmission.
 */

/**
 * SYNC producer and consumer object.
 */
 
typedef struct{
    PFCoEm            *em;             /**< From PFCoSync_init() */
    PFbyte            *operatingState; /**< From PFCoSync_init() */
    /** True, if device is SYNC producer. Calculated from _COB ID SYNC Message_
    variable from Object dictionary (index 0x1005). */
    PFEnBoolean        isProducer;
    /** COB_ID of SYNC message. Calculated from _COB ID SYNC Message_
    variable from Object dictionary (index 0x1005). */
    PFword            COB_ID;
    /** Sync period time in [microseconds]. Calculated from _Communication cycle period_
    variable from Object dictionary (index 0x1006). */
    PFdword            periodTime;
    /** Sync period timeout time in [microseconds].
    (periodTimeoutTime = periodTime * 1,5) */
    PFdword            periodTimeoutTime;
    /** Value from _Synchronous counter overflow value_ variable from Object
    dictionary (index 0x1019) */
    PFbyte             counterOverflowValue;
    /** True, if current time is inside synchronous window.
    In this case synchronous PDO may be sent. */
    PFEnBoolean           curentSyncTimeIsInsideWindow;
    /** True in operational, after first SYNC was received or transmitted */
    PFEnBoolean           running;
    /** Counter of the SYNC message if counterOverflowValue is different than zero */
    PFbyte             counter;
    /** Timer for the SYNC message in [microseconds].
    Set to zero after received or transmitted SYNC message */
    PFdword            timer;
    /** Set to nonzero value, if SYNC with wrong data length is received from CAN */
    PFword            receiveError;
    PFCoCanModule     *CANdevRx;       /**< From PFCoSync_init() */
    PFword            CANdevRxIdx;    /**< From PFCoSync_init() */
    PFCoCanModule     *CANdevTx;       /**< From PFCoSync_init() */
    PFCoCanTx         *CANtxBuff;      /**< CAN transmit buffer inside CANdevTx */
    PFword            CANdevTxIdx;    /**< From PFCoSync_init() */
}PFCoSync;

/**
 * Initialize SYNC object.
 *
 * Function must be called in the communication reset section.
 *
 * @param SYNC This object will be initialized.
 * @param em Emergency object.
 * @param SDO SDO server object.
 * @param operatingState Pointer to variable indicating CANopen device NMT internal state.
 * @param COB_ID_SYNCMessage From Object dictionary (index 0x1005).
 * @param communicationCyclePeriod From Object dictionary (index 0x1006).
 * @param synchronousCounterOverflowValue From Object dictionary (index 0x1019).
 * @param CANdevRx CAN device for SYNC reception.
 * @param CANdevRxIdx Index of receive buffer in the above CAN device.
 * @param CANdevTx CAN device for SYNC transmission.
 * @param CANdevTxIdx Index of transmit buffer in the above CAN device.
 *
 * @return #PFEnReturnError: CO_ERROR_NO or CO_ERROR_ILLEGAL_ARGUMENT.
 */
 
PFint16 pfCoSyncInit(
        PFCoSync             *SYNC,
        PFCoEm               *em,
        PFCoSdo              *SDO,
        PFbyte                *operatingState,
        PFdword               COB_ID_SYNCMessage,
        PFdword               communicationCyclePeriod,
        PFbyte                synchronousCounterOverflowValue,
        PFCoCanModule        *CANdevRx,
        PFword                CANdevRxIdx,
        PFCoCanModule        *CANdevTx,
        PFword                CANdevTxIdx);

/**
 * Process SYNC communication.
 *
 * Function must be called cyclically.
 *
 * @param SYNC This object.
 * @param timeDifference_us Time difference from previous function call in [microseconds].
 * @param ObjDict_synchronousWindowLength _Synchronous window length_ variable from
 * Object dictionary (index 0x1007).
 *
 * @return 0: No special meaning.
 * @return 1: New SYNC message recently received or was just transmitted.
 * @return 2: SYNC time was just passed out of window.
 */
PFbyte pfCoSyncProcess(
        PFCoSync              *SYNC,
        PFdword                timeDifference_us,
        PFdword                ObjDict_synchronousWindowLength);

/** @} */
#endif
