#ifndef PF_CO_DRIVER
#define PF_CO_DRIVER

#define __LPC17XX__

/* Include processor header file */
#ifdef __LPC17XX__
	#include "prime_framework.h"
	#include "prime_sysClk.h"
	#include "prime_canCommon.h"
	#include "prime_can2.h"
#elif defined(__LPC177X_8X__)
	#include "lpc177x_8x_can.h"
#endif

#include "prime_CO_hal.h"
#include "prime_CO_OD_cfg.h"

/**
 * @defgroup PF_CO_driver Driver
 * @ingroup PF_CO_CANopen
 * @{
 *
 * Microcontroller specific code for CANopenNode.
 *
 * This file contains type definitions, functions and macros for:
 *  - Basic data types.
 *  - Receive and transmit buffers for CANopen messages.
 *  - Interaction with CAN module on the microcontroller.
 *  - CAN receive and transmit interrupts.
 *
 * This file is not only a CAN driver. There are no classic CAN queues for CAN
 * messages. This file provides direct connection with other CANopen
 * objects. It tries to provide fast responses and tries to avoid unnecessary
 * calculations and memory consumptions.
 *
 * PFCoCanModule contains an array of _Received message objects_ (of type
 * PFCoCanRx) and an array of _Transmit message objects_ (of type PFCoCanTx).
 * Each CANopen communication object owns one member in one of the arrays.
 * For example Heartbeat producer generates one CANopen transmitting object,
 * so it has reserved one member in PFCoCanTx array.
 * SYNC module may produce sync or consume sync, so it has reserved one member
 * in PFCoCanTx and one member in PFCoCanRx array.
 *
 * ###Reception of CAN messages.
 * Before CAN messages can be received, each member in PFCoCanRx must be
 * initialized. pfCoCanRxBufferInit() is called by CANopen module, which
 * uses specific member. For example @ref PF_CO_HBconsumer uses multiple members
 * in PFCoCanRx array. (It monitors multiple heartbeat messages from remote
 * nodes.) It must call pfCoCanRxBufferInit() multiple times.
 *
 * Main arguments to the pfCoCanRxBufferInit() function are CAN identifier
 * and a pointer to callback function. Those two arguments (and some others)
 * are copied to the member of the PFCoCanRx array.
 *
 * Callback function is a function, specified by specific CANopen module
 * (for example by @ref PF_CO_HBconsumer). Each CANopen module defines own
 * callback function. Callback function will process the received CAN message.
 * It will copy the necessary data from CAN message to proper place. It may
 * also trigger additional task, which will further process the received message.
 * Callback function must be fast and must only make the necessary calculations
 * and copying.
 *
 * Received CAN messages are processed by CAN receive interrupt function.
 * After CAN message is received, function first tries to find matching CAN
 * identifier from PFCoCanRx array. If found, then a corresponding callback
 * function is called.
 *
 * Callback function accepts two parameters:
 *  - object is pointer to object registered by pfCoCanRxBufferInit().
 *  - msg  is pointer to CAN message of type PFCoCanRxMsg.
 *
 * Callback function must return #PFEnCoReturnError: enCO_ERROR_NO,
 * enCO_ERROR_RX_OVERFLOW, enCO_ERROR_RX_PDO_OVERFLOW, enCO_ERROR_RX_MSG_LENGTH or
 * enCO_ERROR_RX_PDO_LENGTH.
 *
 *
 * ###Transmission of CAN messages.
 * Before CAN messages can be transmitted, each member in PFCoCanTx must be
 * initialized. pfCoCanTxBufferInit() is called by CANopen module, which
 * uses specific member. For example Heartbeat producer must initialize it's
 * member in PFCoCanTx array.
 *
 * pfCoCanTxBufferInit() returns a pointer of type PFCoCanTx, which contains buffer
 * where CAN message data can be written. CAN message is send with calling
 * pfCoCanSend() function. If at that moment CAN transmit buffer inside
 * microcontroller's CAN module is free, message is copied directly to CAN module.
 * Otherwise pfCoCanSend() function sets _bufferFull_ flag to true. Message will be
 * then sent by CAN TX interrupt as soon as CAN module is freed. Until message is
 * not copied to CAN module, its contents must not change. There may be multiple
 * _bufferFull_ flags in PFCoCanTx array set to true. In that case messages with
 * lower index inside array will be sent first.
 */


/**
 * @name CAN module base address
 * @{
 */
#ifdef __LPC17XX__
    typedef CAN_TypeDef *   PFCoCanModuleBaseAddr;
#elif defined(__LPC177X_8X__)
    typedef PFbyte          PFCoCanModuleBaseAddr;
#endif
    typedef struct
    {
        PFCoCanModuleBaseAddr uBase;
    }PFCoCanModuleHwConfig;

/** @} */

/**
 * Return values of some CANopen functions. If function was executed
 * successfully it returns 0 otherwise it returns <0.
 */
typedef enum{
    enCO_ERROR_NO                 = 0,    /**< Operation completed successfully */
    enCO_ERROR_ILLEGAL_ARGUMENT   = -1,   /**< Error in function arguments */
    enCO_ERROR_OUT_OF_MEMORY      = -2,   /**< Memory allocation failed */
    enCO_ERROR_TIMEOUT            = -3,   /**< Function timeout */
    enCO_ERROR_ILLEGAL_BAUDRATE   = -4,   /**< Illegal baudrate passed to function pfCoCanModuleInit() */
    enCO_ERROR_RX_OVERFLOW        = -5,   /**< Previous message was not processed yet */
    enCO_ERROR_RX_PDO_OVERFLOW    = -6,   /**< previous PDO was not processed yet */
    enCO_ERROR_RX_MSG_LENGTH      = -7,   /**< Wrong receive message length */
    enCO_ERROR_RX_PDO_LENGTH      = -8,   /**< Wrong receive PDO length */
    enCO_ERROR_TX_OVERFLOW        = -9,   /**< Previous message is still waiting, buffer full */
    enCO_ERROR_TX_PDO_WINDOW      = -10,  /**< Synchronous TPDO is outside window */
    enCO_ERROR_TX_UNCONFIGURED    = -11,  /**< Transmit buffer was not confugured properly */
    enCO_ERROR_PARAMETERS         = -12,  /**< Error in function function parameters */
    enCO_ERROR_DATA_CORRUPT       = -13,  /**< Stored data are corrupt */
    enCO_ERROR_CRC                = -14,  /**< CRC does not match */
    enCO_ERROR_INTERNAL           = -15   /**< internal */
}PFEnCoReturnError;

/**
 * CAN receive message structure as aligned in CAN module. It is different in
 * different microcontrollers. It usually contains other variables.
 */
typedef struct{
    /** CAN identifier. It must be read through PFCoCanRxMsg_readIdent() function. */
    /*PFdword            ident;*/
    PFbyte             DLC;           /**< Length of CAN message */
    PFbyte             data[8];        /**< 8 data bytes */
}PFCoCanRxMsg;


/**
 * Received message object
 */
typedef struct{
    PFword            ident;          /**< Standard CAN Identifier (bits 0..10) + RTR (bit 11) */
    PFword            mask;           /**< Standard Identifier mask with same alignment as ident */
    void               *object;         /**< From pfCoCanRxBufferInit() */
    void              (*pFunct)(void *object, const PFCoCanRxMsg *message);  /**< From pfCoCanRxBufferInit() */
}PFCoCanRx;


/**
 * Transmit message object.
 */
typedef struct{
    PFword            ident;          /**< CAN identifier as aligned in CAN module */
    PFbyte             len;
    PFbyte             type;
    PFbyte             data[8];        /**< 8 data bytes */
    volatile PFEnBoolean  bufferFull;     /**< True if previous message is still in buffer */
    /** Synchronous PDO messages has this flag set. It prevents them to be sent outside the synchronous window */
    volatile PFEnBoolean  syncFlag;
}PFCoCanTx;


typedef struct{
    PFCoCanModuleBaseAddr   CANbaseAddress; /**< From pfCoCanModuleInit() */
#if CO_NO_SYNC > 0
    volatile PFbyte     flags;
#endif
}PFCoCanModuleHwData;

/**
 * CAN module object. It may be different in different microcontrollers.
 */
typedef struct{
    PFCoCanModuleHwData hw; /**< From pfCoCanModuleInit() */
    PFCoCanRx         *rxArray;        /**< From pfCoCanModuleInit() */
    PFword            rxSize;         /**< From pfCoCanModuleInit() */
    PFCoCanTx         *txArray;        /**< From pfCoCanModuleInit() */
    PFword            txSize;         /**< From pfCoCanModuleInit() */
    /** Value different than zero indicates, that CAN module hardware filters
      * are used for CAN reception. If there is not enough hardware filters,
      * they won't be used. In this case will be *all* received CAN messages
      * processed by software. */
    volatile PFEnBoolean  useCANrxFilters;
    /** If flag is true, then message in transmit buffer is synchronous PDO
      * message, which will be aborted, if pfCO_clearPendingSyncPDOs() function
      * will be called by application. This may be necessary if Synchronous
      * window time was expired. */
    volatile PFEnBoolean  bufferInhibitFlag;
    /** Equal to 1, when the first transmitted message (bootup message) is in CAN TX buffers */
    volatile PFEnBoolean  firstCANtxMessage;
    /** Number of messages in transmit buffer, which are waiting to be copied to the CAN module */
    volatile PFword   CANtxCount;
    PFdword            errOld;         /**< Previous state of CAN errors */
    void               *em;             /**< Emergency object */
}PFCoCanModule;


/**
 * Endianness.
 *
 * Depending on processor or compiler architecture, one of the two macros must
 * be defined: CO_LITTLE_ENDIAN or CO_BIG_ENDIAN. CANopen itself is little endian.
 */
#define CO_LITTLE_ENDIAN


/**
 * Request CAN configuration (stopped) mode and *wait* until it is set.
 *
 * @param CANbaseAddress CAN module base address.
 */
//void CO_CANsetConfigurationMode(CO_CANmoduleBaseAddr_t CANbaseAddress);


/**
 * Request CAN normal (opearational) mode and *wait* until it is set.
 *
 * @param CANbaseAddress CAN module base address.
 */
//void CO_CANsetNormalMode(CO_CANmoduleBaseAddr_t CANbaseAddress);


/**
 * Initialize CAN module object.
 *
 * Function must be called in the communication reset section. CAN module must
 * be in Configuration Mode before.
 *
 * @param CANmodule This object will be initialized.
 * @param CANbaseAddress CAN module base address.
 * @param rxArray Array for handling received CAN messages
 * @param rxSize Size of the above array. Must be equal to number of receiving CAN objects.
 * @param txArray Array for handling transmitting CAN messages
 * @param txSize Size of the above array. Must be equal to number of transmitting CAN objects.
 * @param CANbitRate Valid values are (in kbps): 10, 20, 50, 125, 250, 500, 800, 1000.
 * If value is illegal, bitrate defaults to 125.
 *
 * Return #PFEnCoReturnError: enCO_ERROR_NO or enCO_ERROR_ILLEGAL_ARGUMENT.
 */
PFEnCoReturnError pfCoCanModuleInit(
        PFCoCanModule         *CANmodule,
        PFCoCanModuleHwConfig *CANhwCfg,
        PFCoCanRx              rxArray[],
        PFword                rxSize,
        PFCoCanTx              txArray[],
        PFword                txSize,
        PFword                CANbitRate);


/**
 * Switch off CANmodule. Call at program exit.
 *
 * @param CANmodule CAN module object.
 */
void pfCoCanModuleDisable(PFCoCanModule *CANmodule);


/**
 * Read CAN identifier from received message
 *
 * @param rxMsg Pointer to received message
 * @return 11-bit CAN standard identifier.
 */
/*PFword CO_CANrxMsg_readIdent(const PFCoCanRxMsg *rxMsg);*/


/**
 * Configure CAN message receive buffer.
 *
 * Function configures specific CAN receive buffer. It sets CAN identifier
 * and connects buffer with specific object. Function must be called for each
 * member in _rxArray_ from PFCoCanModule.
 *
 * @param CANmodule This object.
 * @param index Index of the specific buffer in _rxArray_.
 * @param ident 11-bit standard CAN Identifier.
 * @param mask 11-bit mask for identifier. Most usually set to 0x7FF.
 * Received message (rcvMsg) will be accepted if the following
 * condition is true: (((rcvMsgId ^ ident) & mask) == 0).
 * @param rtr If true, 'Remote Transmit Request' messages will be accepted.
 * @param object CANopen object, to which buffer is connected. It will be used as
 * an argument to pFunct. Its type is (void), pFunct will change its
 * type back to the correct object type.
 * @param pFunct Pointer to function, which will be called, if received CAN
 * message matches the identifier. It must be fast function.
 *
 * Return #PFEnCoReturnError: enCO_ERROR_NO, enCO_ERROR_ILLEGAL_ARGUMENT or
 * enCO_ERROR_OUT_OF_MEMORY (not enough masks for configuration).
 */
 
PFEnCoReturnError pfCoCanRxBufferInit(
        PFCoCanModule         *CANmodule,
        PFword                index,
        PFword                ident,
        PFword                mask,
        PFEnBoolean               rtr,
        void                   *object,
        void                  (*pFunct)(void *object, const PFCoCanRxMsg *message));


/**
 * Configure CAN message transmit buffer.
 *
 * Function configures specific CAN transmit buffer. Function must be called for
 * each member in _txArray_ from PFCoCanModule.
 *
 * @param CANmodule This object.
 * @param index Index of the specific buffer in _txArray_.
 * @param ident 11-bit standard CAN Identifier.
 * @param rtr If true, 'Remote Transmit Request' messages will be transmitted.
 * @param noOfBytes Length of CAN message in bytes (0 to 8 bytes).
 * @param syncFlag This flag bit is used for synchronous TPDO messages. If it is set,
 * message will not be sent, if current time is outside synchronous window.
 *
 * @return Pointer to CAN transmit message buffer. 8 bytes data array inside
 * buffer should be written, before pfCoCanSend() function is called.
 * Zero is returned in case of wrong arguments.
 */
 
PFCoCanTx *pfCoCanTxBufferInit(
        PFCoCanModule         *CANmodule,
        PFword                index,
        PFword                ident,
        PFEnBoolean           rtr,
        PFbyte                noOfBytes,
        PFEnBoolean           syncFlag);


/**
 * Send CAN message.
 *
 * @param CANmodule This object.
 * @param buffer Pointer to transmit buffer, returned by pfCoCanTxBufferInit().
 * Data bytes must be written in buffer before function call.
 *
 * @return #PFEnCoReturnError: enCO_ERROR_NO, enCO_ERROR_TX_OVERFLOW or
 * enCO_ERROR_TX_PDO_WINDOW (Synchronous TPDO is outside window).
 */
PFEnCoReturnError pfCoCanSend(PFCoCanModule *CANmodule, PFCoCanTx *buffer);


/**
 * Clear all synchronous TPDOs from CAN module transmit buffers.
 *
 * CANopen allows synchronous PDO communication only inside time between SYNC
 * message and SYNC Window. If time is outside this window, new synchronous PDOs
 * must not be sent and all pending sync TPDOs, which may be on CAN TX buffers,
 * must be cleared.
 *
 * This function checks (and aborts transmission if necessary) CAN TX buffers
 * when it is called. Function should be called by the stack in the moment,
 * when SYNC time was just passed out of synchronous window.
 *
 * @param CANmodule This object.
 */
void pfCoCanClearPendingSyncPDOs(PFCoCanModule *CANmodule);


/**
 * Verify all errors of CAN module.
 *
 * Function is called directly from pfCoEmProcess() function.
 *
 * @param CANmodule This object.
 */
void pfCoCanVerifyErrors(PFCoCanModule *CANmodule);


/**
 * Receives and transmits CAN messages.
 *
 * Function must be called directly from high priority CAN interrupt.
 *
 * @param CANmodule This object.
 */
void pfCoCanInterrupt(PFCoCanModule *CANmodule);

//void canHandler();

/** @} */
#endif
