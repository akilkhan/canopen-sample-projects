/**
 * CANopen Network management and Heartbeat producer protocol.
 */

#ifndef PF_CO_NMT_HEARTBEAT
#define PF_CO_NMT_HEARTBEAT

/**
 * @defgroup PF_CO_NMT_Heartbeat NMT and Heartbeat
 * @ingroup PF_CO_CANopen
 * @{
 *
 * CANopen Network management and Heartbeat producer protocol.
 *
 * CANopen device can be in one of the #PFEnCoNmtInternalState
 *  - Initializing. It is active before CANopen is initialized.
 *  - Pre-operational. All CANopen objects are active, except PDOs.
 *  - Operational. Process data objects (PDOs) are active too.
 *  - Stopped. Only Heartbeat producer and NMT consumer are active.
 *
 * NMT master can change the internal state of the devices by sending
 * #PFEnCoNmtCommand.
 *
 * ###NMT message contents:
 *
 *   Byte | Description
 *   -----|-----------------------------------------------------------
 *     0  | #PFEnCoNmtCommand
 *     1  | Node ID. If zero, command addresses all nodes.
 *
 * ###Heartbeat message contents:
 *
 *   Byte | Description
 *   -----|-----------------------------------------------------------
 *     0  | #PFEnCoNmtInternalState
 *
 * @see #PFCoDefaultCanId
 *
 * ###Status LED diodes
 * Macros for @ref PF_CO_NMT_statusLEDdiodes are also implemented in this object.
 */


/**
 * @defgroup PF_CO_NMT_statusLEDdiodes Status LED diodes
 * @{
 *
 * Macros for status LED diodes.
 *
 * Helper macros for implementing status LED diodes are used by stack and can
 * also be used by the application. If macro returns 1 LED should be ON,
 * otherwise OFF. Function pfCoNmtBlinkingProcess50ms() must be called cyclically
 * to update the variables.
 */
    #define LED_FLICKERING(NMT)     (((NMT)->LEDflickering>=0)     ? 1 : 0) /**< 10HZ (100MS INTERVAL) */
    #define LED_BLINKING(NMT)       (((NMT)->LEDblinking>=0)       ? 1 : 0) /**< 2.5HZ (400MS INTERVAL) */
    #define LED_SINGLE_FLASH(NMT)   (((NMT)->LEDsingleFlash>=0)    ? 1 : 0) /**< 200MS ON, 1000MS OFF */
    #define LED_DOUBLE_FLASH(NMT)   (((NMT)->LEDdoubleFlash>=0)    ? 1 : 0) /**< 200MS ON, 200MS OFF, 200MS ON, 1000MS OFF */
    #define LED_TRIPLE_FLASH(NMT)   (((NMT)->LEDtripleFlash>=0)    ? 1 : 0) /**< 200MS ON, 200MS OFF, 200MS ON, 200MS OFF, 200MS ON, 1000MS OFF */
    #define LED_QUADRUPLE_FLASH(NMT)(((NMT)->LEDquadrupleFlash>=0) ? 1 : 0) /**< 200MS ON, 200MS OFF, 200MS ON, 200MS OFF, 200MS ON, 200MS OFF, 200MS ON, 1000MS OFF */
    #define LED_GREEN_RUN(NMT)      (((NMT)->LEDgreenRun>=0)       ? 1 : 0) /**< CANOPEN RUN LED ACCORDING TO CIA DR 303-3 */
    #define LED_RED_ERROR(NMT)      (((NMT)->LEDredError>=0)       ? 1 : 0) /**< CANopen error LED according to CiA DR 303-3 */
/** @} */

/**
 * Internal network state of the CANopen node
 */
typedef enum{
    enCO_NMT_INITIALIZING             = 0,    /**< Device is initializing */
    enCO_NMT_PRE_OPERATIONAL          = 127,  /**< Device is in pre-operational state */
    enCO_NMT_OPERATIONAL              = 5,    /**< Device is in operational state */
    enCO_NMT_STOPPED                  = 4     /**< Device is stopped */
}PFEnCoNmtInternalState;


/**
 * Commands from NMT master.
 */
typedef enum{
    enCO_NMT_ENTER_OPERATIONAL        = 1,    /**< Start device */
    enCO_NMT_ENTER_STOPPED            = 2,    /**< Stop device */
    enCO_NMT_ENTER_PRE_OPERATIONAL    = 128,  /**< Put device into pre-operational */
    enCO_NMT_RESET_NODE               = 129,  /**< Reset device */
    enCO_NMT_RESET_COMMUNICATION      = 130   /**< Reset CANopen communication on device */
}PFEnCoNmtCommand;

/**
 * Return code for pfCoNmtProcess() that tells application code what to
 * reset.
 */
 
typedef enum{
    enCO_RESET_NOT  = 0,/**< Normal return, no action */
    enCO_RESET_COMM = 1,/**< Application must provide communication reset. */
    enCO_RESET_APP  = 2,/**< Application must provide complete device reset */
    enCO_RESET_QUIT = 3 /**< Application must quit, no reset of microcontroller (command is not requested by the stack.) */
}PFEnCoNmtResetCmd;


/**
 * NMT consumer and Heartbeat producer object. It includes also variables for
 * @ref PF_CO_NMT_statusLEDdiodes. Object is initialized by pfCoNmtInit().
 */
typedef struct{
    PFbyte             operatingState; /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDflickering;  /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDblinking;    /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDsingleFlash; /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDdoubleFlash; /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDtripleFlash; /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDquadrupleFlash; /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDgreenRun;    /**< See @ref PF_CO_NMT_statusLEDdiodes */
    PFint8              LEDredError;    /**< See @ref PF_CO_NMT_statusLEDdiodes */

    PFbyte             resetCommand;   /**< If different than zero, device will reset */
    PFbyte             nodeId;         /**< CANopen Node ID of this device */
    PFword            HBproducerTimer;/**< Internal timer for HB producer */
    PFword            firstHBTime;    /**< From pfCoNmtInit() */
    PFCoEmpr          *emPr;           /**< From pfCoNmtInit() */
    PFCoCanModule     *HB_CANdev;      /**< From pfCoNmtInit() */
    PFCoCanTx         *HB_TXbuff;      /**< CAN transmit buffer */
}PFCoNmt;


/**
 * Initialize NMT and Heartbeat producer object.
 *
 * Function must be called in the communication reset section.
 *
 * @param NMT This object will be initialized.
 * @param emPr Emergency main object.
 * @param nodeId CANopen Node ID of this device.
 * @param firstHBTime Time between bootup and first heartbeat message in milliseconds.
 * If firstHBTime is greater than _Producer Heartbeat time_
 * (object dictionary, index 0x1017), latter is used instead.
 * @param NMT_CANdev CAN device for NMT reception.
 * @param NMT_rxIdx Index of receive buffer in above CAN device.
 * @param CANidRxNMT CAN identifier for NMT message.
 * @param HB_CANdev CAN device for HB transmission.
 * @param HB_txIdx Index of transmit buffer in the above CAN device.
 * @param CANidTxHB CAN identifier for HB message.
 *
 * @return #PFEnCoReturnError enCO_ERROR_NO or enCO_ERROR_ILLEGAL_ARGUMENT.
 */
PFint16 pfCoNmtInit(
        PFCoNmt               *NMT,
        PFCoEmpr              *emPr,
        PFbyte                 nodeId,
        PFword                firstHBTime,
        PFCoCanModule         *NMT_CANdev,
        PFword                NMT_rxIdx,
        PFword                CANidRxNMT,
        PFCoCanModule         *HB_CANdev,
        PFword                HB_txIdx,
        PFword                CANidTxHB);


/**
 * Calculate blinking bytes.
 *
 * Function must be called cyclically every 50 milliseconds. See @ref PF_CO_NMT_statusLEDdiodes.
 *
 * @param NMT NMT object.
 */
void pfCoNmtBlinkingProcess50ms(PFCoNmt *NMT);

/**
 * Process received NMT and produce Heartbeat messages.
 *
 * Function must be called cyclically.
 *
 * @param NMT This object.
 * @param timeDifference_ms Time difference from previous function call in [milliseconds].
 * @param HBtime _Producer Heartbeat time_ (object dictionary, index 0x1017).
 * @param NMTstartup _NMT startup behavior_ (object dictionary, index 0x1F80).
 * @param errorRegister _Error register_ (object dictionary, index 0x1001).
 * @param errorBehavior pointer to _Error behavior_ array (object dictionary, index 0x1029).
 * Object controls, if device should leave NMT operational state.
 * Length of array must be 6. If pointer is NULL, no calculation is made.
 *
 * @return #PFEnCoNmtResetCmd
 */
 
PFEnCoNmtResetCmd pfCoNmtProcess(
        PFCoNmt              *NMT,
        PFword                timeDifference_ms,
        PFword                HBtime,
        PFdword               NMTstartup,
        PFbyte                errorRegister,
        const PFbyte          errorBehavior[]);


/** @} */
#endif
