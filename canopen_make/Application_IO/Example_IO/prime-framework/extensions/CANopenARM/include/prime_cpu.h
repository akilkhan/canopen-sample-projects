#ifndef PF_CO_CPU
#define PF_CO_CPU

#include "prime_framework.h"
#include <stddef.h>         /* for 'NULL' */

#ifdef __GNUC__
	#define 	CO_WEAK_DECL 	__attribute__ ((weak))
#endif

#define 	CO_ROM_DECL(x)       		 x
#define 	CO_ROM_READ8(x)     		(x)
#define 	CO_ROM_READ16(x)    		(x)
#define 	CO_ROM_READ32(x)    		(x)
#define 	CO_ROM_READ8_FROMADDR(a)    (*((PFbyte *)a))
#define 	CO_ROM_READ16_FROMADDR(a)   (*((PFword *)a))
#define 	CO_ROM_READ32_FROMADDR(a)   (*((PFdword *)a))
/**
 * @defgroup CO_dataTypes Data types
 * @{
 */
    typedef unsigned char CO_bool_t;
    typedef enum{
        CO_false = 0,
        CO_true = 1
    }CO_boolval_t;
    /* int8_t to uint64_t are defined in stdint.h */
    typedef float                   float32_t;  /**< float32_t */
    typedef long double             float64_t;  /**< float64_t */
    typedef char                    char_t;     /**< char_t */
    typedef unsigned char           oChar_t;    /**< oChar_t */
    typedef unsigned char           domain_t;   /**< domain_t */
/** @} */

typedef PFword PFCoCpuInterruptsFlags;		/**< Software Interrupt Flags */

#endif
