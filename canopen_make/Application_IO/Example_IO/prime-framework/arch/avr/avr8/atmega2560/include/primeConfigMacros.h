#pragma once

/*
#define _PCLK_DIV(x)				PCLK_DIV_##x
#define _PWR(x)						PWR_##x
#define _INT_HANDLER(x)				x##_IRQHandler
#define _IRQ_NUM(x)					x##_IRQn
#define _PERIPH(x)					PERIPH_##x
#define _EINT(x)					EINT_##x
*/

#define INT_HANDLER(x)				PF_CONCAT(__vector_, x)	//_INT_HANDLER(x)
#define PERIPH(x)					PF_CONCAT(PERIPH_, x)		//_PERIPH(x)
#define EINT_CH(x)					PF_CONCAT(EINT_, x)			//_EINT_CH(x)


#define __PWM_OVF_VECT(x)		_PWM_OVF_VECT(x)
#define _PWM_OVF_VECT(x)		x##_OVF_vect

#define __PWM_COMPA_VECT(x)		_PWM_COMPA_VECT(x)
#define _PWM_COMPA_VECT(x)		x##_COMPA_vect

#define __PWM_COMPB_VECT(x)		_PWM_COMPB_VECT(x)
#define _PWM_COMPB_VECT(x)		x##_COMPB_vect

#define TIMSK(x)			_TIMSK(x)
#define _TIMSK(x)			TIMSK_INT->##x
 
