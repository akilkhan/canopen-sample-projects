/* Include Headers */

#include "prime_framework.h"
#include "CANopen.h"
#include "user_board.h"
#include "prime_uart0.h"
#include "application.h"

/* Macros */

#define EXAMSTR_CAN_COMM_TEST   0
#define EXAMSTR_CMD_MAX_LEN  	64
#define EXAMSTR_DBGMSG_MAX_LEN  128

#define EXAMSTR_DEBUG(...) \
{ \
    snprintf(gs_dbgMsg, sizeof(gs_dbgMsg) - 1, __VA_ARGS__); \
    DEBUGS(gs_dbgMsg); \
}
#define EXAMSTR_ERROR(...)   EXAMSTR_DEBUG("ERROR: " __VA_ARGS__)
#define EXAMSTR_PANIC(...)   {EXAMSTR_DEBUG("PANIC: " __VA_ARGS__); while(1);}

void app_timerIrqHandler(void);
void app_canIrqHandler(void);

static volatile PFword CO_timer1ms;
static char gs_dbgMsg[EXAMSTR_DBGMSG_MAX_LEN];

static void cli_handleCmd(PFbyte *cmd);
static void cli_process(PFword timer1msDiff);

void delay()
{
	volatile int i=0,j=0;
	for(i=0;i<5000;i++)
	{
		for(j=0;j<300;j++)
		{
			asm("nop");
		}
	}
}

/** Main function **/

int main (void)
{
    PFEnCoNmtResetCmd reset = enCO_RESET_NOT;
    PFbyte   cmdBuf[EXAMSTR_CMD_MAX_LEN];
    PFbyte   cmdIdx = 0;
    PFCoCanModuleHwConfig canHwCfg;

    if(pfBoardinit(&canHwCfg) != 0)
        while(1);
		
	EXAMSTR_DEBUG("*** Phi Robotics ***\r");
    EXAMSTR_DEBUG("*** CANopen HelloBot Master ***\r");
    
    /*Verify, if OD structures have proper alignment of initial values */
    if(CO_OD_RAM.FirstWord != CO_OD_RAM.LastWord)
        EXAMSTR_PANIC("Invalid CO_OD_RAM alignment!\r");
    if(CO_OD_EEPROM.FirstWord != CO_OD_EEPROM.LastWord)
        EXAMSTR_PANIC("Invalid CO_OD_EEPROM alignment!\r");
    if(CO_ROM_READ32(CO_OD_ROM.FirstWord) != CO_ROM_READ32(CO_OD_ROM.LastWord))
        EXAMSTR_PANIC("Invalid CO_OD_ROM alignment!\r");
	
    /* Initialize EEPROM - part 1 */
#ifdef BOARD_USE_EEPROM
    PFEnCoReturnError eeStatus = pfCoEeInit_1(&CO_EEO, (PFbyte*) &CO_OD_EEPROM, sizeof(CO_OD_EEPROM),
											 (PFbyte*) &CO_OD_ROM, sizeof(CO_OD_ROM));
    if(eeStatus != CO_ERROR_NO)
	{
        EXAMSTR_ERROR("EEPROM Initialization Failure!\n");
    }
#endif

    programStart();

    /* Increase variable each startup. Variable is stored in EEPROM. */
    OD_powerOnCounter++;

    while(reset != enCO_RESET_APP)
    {
        /* CANopen communication reset - initialize CANopen objects */
        PFEnCoReturnError err;
        PFword timer1msPrevious;

        /* Disable timer and CAN interrupts */
        timer_disableIrq();
        can_disableIrq();
		
        /* Initialize CANopen */
        err = pfCoInit(&canHwCfg);
		if(err != enCO_ERROR_NO)
        {
            EXAMSTR_PANIC("Failed to Init CANopen stack!\n");
        }

        /* Initialize EEPROM - part 2 */

#ifdef BOARD_USE_EEPROM
        pfCoEeInit_2(&CO_EEO, eeStatus, CO->SDO, CO->em);
#endif

       /* Initialize variables */
       timer1msPrevious = CO_timer1ms;
       OD_performance[ODA_performance_mainCycleMaxTime] = 0;
       OD_performance[ODA_performance_timerCycleMaxTime] = 0;
       reset = enCO_RESET_NOT;

       communicationReset();

       /* Enable CAN and Timer Interrupts */
       timer_enableIrq();
       can_enableIrq();
	   pfRitStart();
	   
       while(reset == enCO_RESET_NOT)
       {
			/* Loop for normal program execution */
			PFword timer1msCopy, timer1msDiff;

			/* Calculate cycle time for performance measurement */
			timer1msCopy = CO_timer1ms;
			timer1msDiff = timer1msCopy - timer1msPrevious;
			timer1msPrevious = timer1msCopy;
		  
			/* Application asynchronous program */
			programAsync(timer1msDiff);
			
			/* CANopen process */
			reset = pfCoProcess(CO, timer1msDiff);

			#ifdef BOARD_USE_EEPROM
				pfCoEeProcess(&CO_EEO);
			#endif
			
					     pfCoProcessRPDO(CO);
   		  
   
   OD_readInput8Bit0[0] = 2; 				// Cheetah-CB Channel
   OD_readInput8Bit0[1] = enBooleanTrue;	// Use Encoder
   OD_readInput8Bit0[2] = 64;				// Encoder CPR
   OD_readInput8Bit0[3] = 100;				// Motor Gear Ratio
   OD_readInput8Bit0[4] = enBooleanTrue; 	// Use PID
   OD_readInput8Bit0[5] = 7;				// Kp Num
   OD_readInput8Bit0[6] = 1;				// Kp Denom
   OD_readInput8Bit0[7] = 105;				// Ti Num
   OD_readInput8Bit0[8] = 1;				// Td Num
   OD_readInput8Bit0[9] = 210;				// Td Denom
   OD_readInput8Bit0[10] = 1;				// Max PID Output Denom
   OD_readInput8Bit0[11] = 45;				// Min PID Output Num
   OD_readInput8Bit0[12] = 1;				// Min PID Output Denom
   OD_readInput8Bit0[13] = 1;				// Max PID Input Num
   OD_readInput8Bit0[14] = 0;				// Min PID Input Num
   OD_readInput8Bit0[15] = 1;				// Min PID Input Denom
   
		  
	   pfCoProcessTPDO(CO);
	   
	

			cmdBuf[cmdIdx] = board_get_char();
			if(cmdBuf[cmdIdx] != 0xFF)
			{
				if(cmdBuf[cmdIdx] == '\n')
				{
					if(cmdIdx > 0 && cmdBuf[cmdIdx-1] == '\r')
                      cmdBuf[cmdIdx-1] = 0;
					else
                      cmdBuf[cmdIdx] = 0;
					cli_handleCmd(cmdBuf);
					cmdIdx = 0;
					board_put_char('>');
				}
              else
                  cmdIdx++;
			}
          cli_process(timer1msDiff);

       }

       /* Disable Timer and CAN interrupts */
       timer_disableIrq();
       can_disableIrq();
	   
       pfCoDelete();
    }
	
    /* Program exit */
    programEnd();

    EXAMSTR_DEBUG("\rReset node!");
    board_reset();

   return 0;
}

#define CLI_DATA_MAX_LEN        32
#define CLI_MAX_ARG_NUM         16

#define CLI_NMT_ARG_NODE_POS    0
#define CLI_NMT_ARG_CMD_POS     1
#define CLI_NMT_ARG_NUM         2
#define CLI_SDOR_ARG_NODE_POS   0
#define CLI_SDOR_ARG_IDX_POS    1
#define CLI_SDOR_ARG_SIDX_POS   2
#define CLI_SDOR_ARG_NUM        3
#define CLI_SDOW_ARG_NODE_POS   0
#define CLI_SDOW_ARG_IDX_POS    1
#define CLI_SDOW_ARG_SIDX_POS   2
#define CLI_SDOW_ARG_LEN_POS    3
#define CLI_SDOW_ARG_VAL_POS    4
#define CLI_SDOW_ARG_NUM        5

static PFbyte gs_data[CLI_DATA_MAX_LEN];
static PFbyte gs_nodeId, gs_objSubIdx;
static PFword gs_objIdx;
static PFCoSdoClient *gs_SDO_C = NULL;
static int gs_bSDORead;

static void cli_nmtCmdPrintHelp(void)
{
    EXAMSTR_DEBUG("\r Send NMT command.");
    EXAMSTR_DEBUG("\r Usage:");
    EXAMSTR_DEBUG("\r  nmt <nodeId> (op | preop | stop | rst | comrst)");
    EXAMSTR_DEBUG("\r  nodeId - node ID in hex format.");
    EXAMSTR_DEBUG("\r  op     - Enter operational state.");
    EXAMSTR_DEBUG("\r  preop  - Enter pre-operational state.");
    EXAMSTR_DEBUG("\r  stop   - Enter stopped state.");
    EXAMSTR_DEBUG("\r  rst    - Reset node.");
    EXAMSTR_DEBUG("\r  comrst - Reset communication.");
}

static int cli_nmtCmdExec(char *pArgs[], PFbyte nArgs)
{
    PFbyte nodeId, nmtCmd;

    if(nArgs != CLI_NMT_ARG_NUM)
    {
        EXAMSTR_ERROR("\rInvalid number of args!");
        cli_nmtCmdPrintHelp();
        return -1;
    }

    nodeId = (PFbyte)strtol(pArgs[CLI_NMT_ARG_NODE_POS], NULL, 16);

    if(strcmp(pArgs[CLI_NMT_ARG_CMD_POS], "op") == 0)
        nmtCmd = enCO_NMT_ENTER_OPERATIONAL;
    else if(strcmp(pArgs[CLI_NMT_ARG_CMD_POS], "preop") == 0)
        nmtCmd = enCO_NMT_ENTER_PRE_OPERATIONAL;
    else if(strcmp(pArgs[CLI_NMT_ARG_CMD_POS], "stop") == 0)
        nmtCmd = enCO_NMT_ENTER_STOPPED;
    else if(strcmp(pArgs[CLI_NMT_ARG_CMD_POS], "rst") == 0)
        nmtCmd = enCO_NMT_RESET_NODE;
    else if(strcmp(pArgs[CLI_NMT_ARG_CMD_POS], "comrst") == 0)
        nmtCmd = enCO_NMT_RESET_COMMUNICATION;
    else
    {
        EXAMSTR_ERROR("Invalid command!\n");
        cli_nmtCmdPrintHelp();
        return -1;
    }

    PFbyte ret = pfCoSendNmtCommand(CO, nmtCmd, nodeId);
    if(ret != 0)
    {
        EXAMSTR_ERROR("\pfCoSendNmtCommand failed!");
        cli_nmtCmdPrintHelp();
        return -1;
    }
	else
		EXAMSTR_DEBUG("\pfCoSendNmtCommand success!");
    return 0;
}

static void cli_sdoCmdPrintHelp(void)
{
    EXAMSTR_DEBUG("\rSDO read/write command.");
    EXAMSTR_DEBUG("\rUsage:");
    EXAMSTR_DEBUG("\r  sdo(r|w) <nodeId> <idx> <sidx> [<len> <val>]");
    EXAMSTR_DEBUG("\r  r | w  - 'r'ead or 'w'rite.");
    EXAMSTR_DEBUG("\r  nodeId - node ID in hex format.");
    EXAMSTR_DEBUG("\r  idx    - Object dictionary index in hex format.");
    EXAMSTR_DEBUG("\r  sidx   - Object dictionary sub-index in hex format.");
    EXAMSTR_DEBUG("\r  len    - length of variable (0001 to 0397) in hex format. If reading, this value is ignored.");
    EXAMSTR_DEBUG("\r  val    - Value (bytes sequence) to be written in hex and little-endian format. If reading, this value is ignored.");
    EXAMSTR_DEBUG("    val    - Value (bytes sequence) to be written in hex and little-endian format. If reading, this value is ignored.");
}

static int cli_sdorCmdExec(char *pArgs[], PFbyte nArgs)
{
    int8_t ret;
	char buf[100];
	static PFbyte cnt;
    if(nArgs != CLI_SDOR_ARG_NUM)
    {
        EXAMSTR_ERROR("Invalid number of args!\n");
        cli_sdoCmdPrintHelp();
        return -1;
    }

    gs_nodeId = (PFbyte)strtol(pArgs[CLI_SDOR_ARG_NODE_POS], NULL, 16);
    gs_objIdx = (PFword)strtol(pArgs[CLI_SDOR_ARG_IDX_POS], NULL, 16);
    gs_objSubIdx = (PFbyte)strtol(pArgs[CLI_SDOR_ARG_SIDX_POS], NULL, 16);
	
    if(gs_SDO_C != NULL)
    {
        EXAMSTR_ERROR("SDO command in progress!\n");
        return -1;
    }
    gs_SDO_C = CO->SDOclient;
    gs_bSDORead = 1;

    ret = pfCoSdoClientSetup(gs_SDO_C, 0, 0, gs_nodeId);
    if(ret != 0)
    {
        EXAMSTR_ERROR("Failed to setup SDO client!\n");
        gs_SDO_C = NULL;
        return -1;
    }

    ret = pfCoSdoClientUploadInitiate(gs_SDO_C, gs_objIdx, gs_objSubIdx, gs_data, CLI_DATA_MAX_LEN, CO_false);
    if(ret != 0)
    {
        EXAMSTR_ERROR("Failed to init SDO upload!\n");
        pfCoSdoClientSetup(gs_SDO_C, 0, 0, 0);
        gs_SDO_C = NULL;
        return -1;
    }
    return 0;
}

static int cli_sdowCmdExec(char *pArgs[], PFbyte nArgs)
{
    int8_t ret;
    PFword dataLen, valLen, i;

    if(nArgs != CLI_SDOW_ARG_NUM)
    {
        EXAMSTR_ERROR("Invalid number of args!\n");
        cli_sdoCmdPrintHelp();
        return -1;
    }

    gs_nodeId = (PFbyte)strtol(pArgs[CLI_SDOW_ARG_NODE_POS], NULL, 16);
    gs_objIdx = (PFword)strtol(pArgs[CLI_SDOW_ARG_IDX_POS], NULL, 16);
    gs_objSubIdx = (PFbyte)strtol(pArgs[CLI_SDOW_ARG_SIDX_POS], NULL, 16);
    dataLen = (PFword)strtol(pArgs[CLI_SDOW_ARG_LEN_POS], NULL, 16);
    if(dataLen > CLI_DATA_MAX_LEN)
    {
        EXAMSTR_ERROR("Too big data length!\n");
        return -1;
    }
    valLen = strlen(pArgs[CLI_SDOW_ARG_VAL_POS]);
    if(valLen % 2)
    {
        EXAMSTR_ERROR("Value should be byte sequence with even number of digits!\n");
        cli_sdoCmdPrintHelp();
        return -1;
    }
    valLen /= 2;
    if(valLen > dataLen)
    {
        EXAMSTR_ERROR("Too big value length!\n");
        return -1;
    }
    for(i = 0; i < dataLen; i++)
    {
        if(i < valLen)
            gs_data[i] = 16*(pArgs[CLI_SDOW_ARG_VAL_POS][2*i]-'0') + (pArgs[CLI_SDOW_ARG_VAL_POS][2*i+1]-'0');
        else
            gs_data[i] = 0;
    }

    if(gs_SDO_C != NULL)
    {
        EXAMSTR_ERROR("SDO command in progress!\n");
        return -1;
    }
    gs_SDO_C = CO->SDOclient;
    gs_bSDORead = 0;

    ret = pfCoSdoClientSetup(gs_SDO_C, 0, 0, gs_nodeId);
    if(ret != 0)
    {
        EXAMSTR_ERROR("Failed to setup SDO client!\n");
        gs_SDO_C = NULL;
        return -1;
    }

    ret = pfCoSdoClientDownloadInitiate(gs_SDO_C, gs_objIdx, gs_objSubIdx, gs_data, dataLen, CO_false);
    if(ret != 0)
    {
        EXAMSTR_ERROR("Failed to init SDO download!\n");
        pfCoSdoClientSetup(gs_SDO_C, 0, 0, 0);
        gs_SDO_C = NULL;
        return -1;
    }

    return 0;
}

static void cli_pdoCmdPrintHelp(void)
{
    /*EXAMSTR_DEBUG("PDO read/write command.\n");
    EXAMSTR_DEBUG("Usage:\n");
    EXAMSTR_DEBUG("  pdo(r|w) <nodeId> <idx> <sidx> [<len> <val>]\n");
    EXAMSTR_DEBUG("  r | w  - 'r'ead or 'w'rite.\n");
    EXAMSTR_DEBUG("  nodeId - node ID in hex format.\n");
    EXAMSTR_DEBUG("  idx    - Object dictionary index in hex format.\n");
    EXAMSTR_DEBUG("  sidx   - Object dictionary sub-index in hex format.\n");
    EXAMSTR_DEBUG("  len    - length of variable (0001 to 0397) in hex format. If reading, this value is ignored.\n");
    EXAMSTR_DEBUG("  val    - Value to be written in hex format. If reading, this value is ignored.\n");*/
}

static int cli_pdorCmdExec(char *pArgs[], PFbyte nArgs)
{
    (void)pArgs;
    (void)nArgs;
    return 0;
}

static int cli_pdowCmdExec(char *pArgs[], PFbyte nArgs)
{
    (void)pArgs;
    (void)nArgs;
    return 0;
}

static void cli_handleCmd(PFbyte *cmd)
{
	char *pToks[CLI_MAX_ARG_NUM+1];
    PFbyte nToks = 0;

    /* retrieve command */
    pToks[nToks] = strtok((char *)cmd, " \t");
    if(pToks[nToks] == NULL)
    {
        return;
    }
    nToks++;

    /* retrieve all arguments */
    while(nToks < (CLI_MAX_ARG_NUM + 1))
    {
        pToks[nToks] = strtok(NULL, " \t");
        if(pToks[nToks] == NULL)
            break;
        nToks++;
    }

    if(strlen(pToks[0]) == 0)
	{
        return;
	}

    if(strcmp(pToks[0], "nmt") == 0)
    {
        cli_nmtCmdExec(&pToks[1], nToks-1);
    }
    else if(strcmp(pToks[0], "sdor") == 0)
    {
        cli_sdorCmdExec(&pToks[1], nToks-1);
    }
    else if(strcmp(pToks[0], "sdow") == 0)
    {
        cli_sdowCmdExec(&pToks[1], nToks-1);
    }
    else if(strcmp(pToks[0], "pdor") == 0)
    {
        cli_pdorCmdExec(&pToks[1], nToks-1);
    }
    else if(strcmp(pToks[0], "pdow") == 0)
    {
        cli_pdowCmdExec(&pToks[1], nToks-1);
    }
    else if(strcmp(pToks[0], "help") == 0)
    {
        cli_nmtCmdPrintHelp();
        cli_sdoCmdPrintHelp();
        cli_pdoCmdPrintHelp();
    }
    else
        EXAMSTR_ERROR("Unknown command!\n");
}

static void cli_process(PFword timer1msDiff)
{
	if(gs_SDO_C != NULL)
    {			
        uint32_t SDOabortCode;
		uint32_t dataLen;
        if(gs_bSDORead)
        {          
            int8_t ret = pfCoSdoClientUpload(gs_SDO_C, timer1msDiff, 1500, &dataLen, &SDOabortCode);
			if(ret == 1)
			{
				delay();
				cli_process(timer1msDiff);
				
			}
			if(ret <= 0)
            {
                if(SDOabortCode)
                {
                    EXAMSTR_ERROR("Failed to read SDO %02X%04X%02X%04X AB: %08X!\n",
                            gs_nodeId, gs_objIdx, gs_objSubIdx, dataLen, (unsigned int)SDOabortCode);
                }
                else
                {
                    uint32_t i;
                    EXAMSTR_DEBUG("Read SDO %02X%04X%02X%04lX OK:",
                            gs_nodeId, gs_objIdx, gs_objSubIdx, dataLen);
                    for(i = 0; i < dataLen; i++)
                        EXAMSTR_DEBUG(" %02X", gs_data[i]);
                    EXAMSTR_DEBUG("\r");
                }
                /* Disable SDO client */
                pfCoSdoClientSetup(gs_SDO_C, 0, 0, 0);
							
                gs_SDO_C = NULL;
            }
        }
        else
        {
            int8_t ret = pfCoSdoClientDownload(gs_SDO_C, timer1msDiff, 1500, &SDOabortCode);
            if(ret <= 0)
            {
                if(SDOabortCode)
                {
                    EXAMSTR_ERROR("Failed to write SDO %02X%04X%02X AB: %08X \r",
                            gs_nodeId, gs_objIdx, gs_objSubIdx, (unsigned int)SDOabortCode);
					EXAMSTR_ERROR("%d\r",ret);
                }
                else
                {
                    uint32_t i;
                    EXAMSTR_DEBUG("Wrote SDO %02X%04X%02X OK:\n",
                            gs_nodeId, gs_objIdx, gs_objSubIdx);
                    for(i = 0; i < dataLen; i++)
                        EXAMSTR_DEBUG(" %02X", gs_data[i]);
                    EXAMSTR_DEBUG("\r");
                }

                /* Disable SDO client */
                pfCoSdoClientSetup(gs_SDO_C, 0, 0, 0);
                gs_SDO_C = NULL;
            }
        }
    }
}

/* Timer Interrupt function executes every Millisecond */

void app_timerIrqHandler()
{
   CO_timer1ms++;

   pfCoProcessRPDO(CO);
   
   program1ms();

   pfCoProcessTPDO(CO);
}

/* CAN Interrupt function */

void app_canIrqHandler()
{
#if 1
   pfCoCanInterrupt(CO->CANmodule[0]);
#else
    CAN_MSG_Type msg;
    uint32_t stat = CAN_IntGetStatus(LPC_CAN2);

    //receive interrupt (New CAN messagge is available in RX buffer)
    if(stat & CAN_ICR_RI)
    {
        if(CAN_ReceiveMsg(LPC_CAN2, &msg) != SUCCESS)
        {
            EXAMSTR_ERROR("Failed to receive CAN message!\n");
        }
        EXAMSTR_DEBUG("Received CAN message %x\n", msg.id);
    }
    else if(stat & (CAN_ICR_TI1 | CAN_ICR_TI2 | CAN_ICR_TI3))
    {
        EXAMSTR_DEBUG("message xmited\n");
    }
    else if(stat & CAN_ICR_EI)
    {
        EXAMSTR_ERROR("ERR_WARN!\n");
    }
    else if(stat & CAN_ICR_EI)
    {
        EXAMSTR_ERROR("EI!\n");
    }
    else if(stat & CAN_ICR_DOI)
    {
        EXAMSTR_ERROR("DOI!\n");
    }
    else if(stat & CAN_ICR_EPI)
    {
        EXAMSTR_ERROR("EPI!\n");
    }
    else if(stat & CAN_ICR_ALI)
    {
        EXAMSTR_ERROR("ALI!\n");
    }
    else if(stat & CAN_ICR_BEI)
    {
        EXAMSTR_ERROR("BEI (%x %d %x)!\n", CAN_ICR_ERRBIT(stat), stat & CAN_ICR_ERRDIR, CAN_ICR_ERRC(stat));
    }
#endif
}

#ifdef OD_testVar
/*
 * Function - ODF_testDomain
 *
 * Function for accessing _test var_ (index 0x2120) from SDO server.
 *
 * For more information see topic <Object dictionary function>.
 */
#define ODF_testDomain_index     0x2120
PFEnCoSdoAbortCode ODF_testDomain(PFCoODFarg *ODF_arg);
#endif

/******************************************************************************/
void programStart(void)	
{
}


/******************************************************************************/
void communicationReset(void)
{
    //CAN_RUN_LED = 0; CAN_ERROR_LED = 0;
    //OD_writeOutput8Bit[0] = leds_read();
    //OD_writeOutput8Bit[1] = 0;

#ifdef OD_testVar
   /* Configure Object dictionary entry at index 0x2120 */
   pfCoODConfigure(CO->SDO, ODF_testDomain_index, ODF_testDomain, 0, 0, 0);
#endif
}


/******************************************************************************/
void programEnd(void)
{
   //CAN_RUN_LED = 0; CAN_ERROR_LED = 0;
}
uint32_t i = 0;

/******************************************************************************/
void programAsync(PFword timer1msDiff)
{
   (void)timer1msDiff;   


	  

	//Is any application critical error set?
	//If error register is set, device will leave operational state.
    if(CO->em->errorStatusBits[8] || CO->em->errorStatusBits[9])
        *CO->emPr->errorRegister |= 0x20;
}


/******************************************************************************/
void program1ms(void)
{
   //Read RPDO and show it on LEDS.
   //leds_write(OD_writeOutput8Bit[0]);

   //Prepare TPDO from buttons.
   //According to PDO mapping and communication parameters, first TPDO is sent
   //automatically on change of state of OD_readInput8Bit[0] variable.
   
   /* 8-bit Data transmission */

   /*OD_readInput8Bit0[0] = 2; 				// Cheetah-CB Channel
   OD_readInput8Bit0[1] = enBooleanTrue;	// Use Encoder
   OD_readInput8Bit0[2] = 64;				// Encoder CPR
   OD_readInput8Bit0[3] = 100;				// Motor Gear Ratio
   OD_readInput8Bit0[4] = enBooleanTrue; 	// Use PID
   OD_readInput8Bit0[5] = 7;				// Kp Num
   OD_readInput8Bit0[6] = 1;				// Kp Denom
   OD_readInput8Bit0[7] = 105;				// Ti Num
   OD_readInput8Bit0[8] = 1;				// Td Num
   OD_readInput8Bit0[9] = 210;				// Td Denom
   OD_readInput8Bit0[10] = 1;				// Max PID Output Denom
   OD_readInput8Bit0[11] = 45;				// Min PID Output Num
   OD_readInput8Bit0[12] = 1;				// Min PID Output Denom
   OD_readInput8Bit0[13] = 1;				// Max PID Input Num
   OD_readInput8Bit0[14] = 0;				// Min PID Input Num
   OD_readInput8Bit0[15] = 1;				// Min PID Input Denom
   */
   
   i++;
   /* 32-bit Data transmission */
  // if(i < 15)
  // {
		OD_readInput32Bit0[0] = 13000;			// MotorMaxSpeed(Max change in Encoder Count)
		//OD_readInput32Bit0[1] = 1000;			// TiDenom

//	}
//	else if(i > 15 && i <30)
//	{	
		OD_readInput32Bit1[0] = 500;			// Max PID Output Num
		OD_readInput32Bit1[1] = 500;			// OutputSpan
//	}
//	else
//	{	
		//OD_readInput32Bit0[0] = 13000;			// Max PID Output Num
		//OD_readInput32Bit0[1] = 13000;			// OutputSpan
//	}
   
   
   
   //OD_readInput32Bit0[0] = 0x36AC2504;
   //OD_readInput32Bit0[1] = 0x22667153;
   
   //OD_readInput32Bit1[0] = 0x34345592;
   //OD_readInput32Bit1[1] = 0xABABBABA;
   
   //OD_readInput32Bit2[0] = ++num;
   //OD_readInput32Bit2[0] = 1000;
   //OD_readInput32Bit2[1] = 0x34391124;
   //OD_readInput32Bit2[2] = 0xCCCCCCCC;
   
}

#ifdef OD_testVar
/******************************************************************************/
/* Function passes some data to SDO server on testDomain variable access */
PFEnCoSdoAbortCode ODF_testDomain(PFCoODFarg *ODF_arg)
{

    /* domain data type is on subIndex 5, nothing to do on other subObjects */
    if(ODF_arg->subIndex != 5) return enCO_SDO_AB_NONE;

    /* reading object dictionary */
    if(ODF_arg->reading)
	{
        /* SDO buffer is filled with sequence 0x01, 0x02, ... */
        /* If domainFileSize is greater than SDObufferSize, this function will */
        /* be called multiple times during one SDO communication cycle */

        const uint32_t domainFileSize = 0x500;
        static uint32_t offset = 0;

        PFword i;
        PFword SDObufferSize = ODF_arg->dataLength;

        /* new SDO read cycle */
        if(ODF_arg->firstSegment)
		{
            ODF_arg->dataLengthTotal = domainFileSize;
            offset = 0;
        }

        /* fill SDO buffer */
        for(i = 0; offset < domainFileSize; i++, offset++)
		{
            if(i >= SDObufferSize)
			{
                /* SDO buffer is full */
                ODF_arg->lastSegment = CO_false;
                break;
            }
            ODF_arg->data[i] = (PFbyte)(offset+1);
        }

        /* all data was copied */
        if(offset == domainFileSize)
		{
            ODF_arg->lastSegment = CO_true;
            ODF_arg->dataLength = i;
        }

        /* return OK */
        return enCO_SDO_AB_NONE;
    }

    /* writing object dictionary */
    else
	{
        PFword i;
        PFword err = 0;
        PFword dataSize = ODF_arg->dataLength;
        static uint32_t offset = 0;

        /* new SDO read cycle */
        if(ODF_arg->firstSegment)
		{
            /* if(ODF_arg->dataLengthTotal) printf("\nWill receive %d bytes of data.\n", ODF_arg->dataLengthTotal); */
            offset = 0;
        }

        /* do something with data, here just verify if they are the same as above */
        for(i=0; i<dataSize; i++, offset++)
		{
            PFbyte b = ODF_arg->data[i];
            if(b != (PFbyte)(offset+1)) err++;
            /* printf("%02X ", b); */
        }

        if(err) return enCO_SDO_AB_INVALID_VALUE;

        /* end of transfer */
        /* if(ODF_arg->lastSegment) */
            /* printf("\nReceived %d bytes of data.\n", offset); */

        /* return OK */
        return enCO_SDO_AB_NONE;
    }
}
#endif
