/*
 *  HYDRA Board.c
 */
 
#include "prime_framework.h"
#include "user_board.h"
#include "prime_CO_hal.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_uart0.h"
#include "prime_rit.h"
#include "prime_can1.h"

#define 	BV(_n_) 		(1 << (_n_))

#define 	BOARD_LED0_PORT	 	4
#define 	BOARD_LED0_PIN  	29
#define 	BOARD_LED1_PORT 	0
#define 	BOARD_LED1_PIN  	26
#define 	BOARD_LED2_PORT 	0
#define 	BOARD_LED2_PIN  	25
#define 	BOARD_BTN0_PORT 	0
#define 	BOARD_BTN0_PIN  	24
#define 	BOARD_BTN1_PORT 	0
#define 	BOARD_BTN1_PIN  	23
#define 	BOARD_BTN2_PORT 	1
#define 	BOARD_BTN2_PIN  	31

extern void app_timerIrqHandler(void);
extern void app_canIrqHandler(void);

#if BOARD_USE_SYSTICK == 1
	void SysTick_Handler(void);
#else
	void RIT_IRQHandler(void);
#endif
void CAN_IRQHandler(void);

#define GPIO_PIN_COUNT		8

PFCfgGpio gpioPins[] = 
{
	// LED 2, LED 3
	{GPIO_PORT_0, GPIO_PIN_25 | GPIO_PIN_26, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// LED 1
	{GPIO_PORT_4, GPIO_PIN_29, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, enGpioFunctionGpio},
	// UART 0 Tx
	{GPIO_PORT_0, GPIO_PIN_2, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_2_TXD0},
	// UART 0 Rx
	{GPIO_PORT_0, GPIO_PIN_3, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_3_RXD0},
	// CAN 1 RD
	{GPIO_PORT_0, GPIO_PIN_21, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_21_RD1},
	// CAN 2 RD
	{GPIO_PORT_0, GPIO_PIN_4, enGpioDirInput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_4_RD2},
	// CAN 1 TD
	{GPIO_PORT_0, GPIO_PIN_22, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_22_TD1},
	// CAN 2 TD
	{GPIO_PORT_0, GPIO_PIN_5, enGpioDirOutput, enGpioPinModePullUp, enGpioOpenDrainDisable, GPIO_0_5_TD2}
};

PFCfgClk clockcfg = 
{	
	100000000,			//cpuFreqHz
	12000000,			//oscFreq
	enPllClkSrcMainOSC	//pllClkSrc
};

PFCfgUart0 uartConfig = 
{
	enPclkDiv_4, 			
	enUart0Baudrate_9600, 	
	enUart0Databits_8, 		
	enUart0ParityNone, 		
	enUart0StopBits_1, 
	enUart0IntNone
};

PFCfgRit ritConfig = 
{
	25000,				// configure RIT for 1ms
	0x00,				// compare mask
	app_timerIrqHandler,	// callback
	enPclkDiv_4,	// pclk divider
	enBooleanTrue,	// halt on break
	enBooleanTrue,	// reset on match
};

void canHandler()
{
	pfUart0WriteString("CAN Error\r");
}

PFCfgCan1 can1config= 
{	
	app_canIrqHandler,
	app_canIrqHandler,
	100000,				// Baudrate
	canHandler,			// CAN Error callback
	enPclkDiv_4,		// ClkDiv
	enCan1IntTxRx		// interrupts
};

int pfBoardinit(PFCoCanModuleHwConfig *canHwCfg)
{
	PFEnStatus status;

	/* System Clock Init */
	pfSysSetCpuClock(&clockcfg);
	
	status = pfGpioInit(gpioPins, GPIO_PIN_COUNT);
	if(status != enStatusSuccess)
	{
		while(1);
	}
		
	/* Initialize UART Configuration parameter structure to default state:
    * Baudrate = 9600bps
    * 8 data bit
    * 1 Stop bit
    * None parity
    */
	
	status = pfUart0Open(&uartConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	
	status = pfCan1Open(&can1config);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	/* Configure CAN Peripheral memory address */
    canHwCfg->uBase = PERIPH_CAN1;

#if BOARD_USE_SYSTICK == 1
    SYSTICK_InternalInit(1);
    SYSTICK_IntCmd(DISABLE);
    SYSTICK_Cmd(ENABLE);
#else
    status = pfRitOpen(&ritConfig);
	if(status != enStatusSuccess)
	{
		while(1);
	}
	pfDelaySetTimerPeriod(10);
#endif

	pfGpioPinsClear(BOARD_LED0_PORT, BOARD_LED0_PIN);
	pfGpioPinsClear(BOARD_LED1_PORT, BOARD_LED1_PIN);
	pfGpioPinsClear(BOARD_LED2_PORT, BOARD_LED2_PIN);
    /* Initialize buttons for TPDO */
    pfGpioPinsSet(BOARD_BTN0_PORT, BOARD_BTN0_PIN);
    pfGpioPinsSet(BOARD_BTN1_PORT, BOARD_BTN1_PIN);
    pfGpioPinsSet(BOARD_BTN2_PORT, BOARD_BTN2_PIN);

#if BOARD_USE_SYSTICK == 0
    /* CAN should have higher prio then 1ms timer */
    NVIC_SetPriority(RIT_IRQn, 10);
    NVIC_SetPriority(CAN_IRQn, 5);
	NVIC_SetPriority(UART0_IRQn,2);
#endif
    return 0;
}

void leds_write(PFbyte ledsBits)
{
    if(ledsBits & 0x1)
        pfGpioPinsSet(BOARD_LED0_PORT, BV(BOARD_LED0_PIN));
    else
        pfGpioPinsClear(BOARD_LED0_PORT, BV(BOARD_LED0_PIN));
    if(ledsBits & 0x2)
        pfGpioPinsSet(BOARD_LED1_PORT, BV(BOARD_LED1_PIN));
    else
        pfGpioPinsClear(BOARD_LED1_PORT, BV(BOARD_LED1_PIN));
    if(ledsBits & 0x4)
        pfGpioPinsSet(BOARD_LED2_PORT, BV(BOARD_LED2_PIN));
    else
        pfGpioPinsClear(BOARD_LED2_PORT, BV(BOARD_LED2_PIN));
}

PFbyte leds_read(void)
{
    PFbyte bits = 0;

    PFdword val = pfGpioPortRead(BOARD_LED0_PORT);
    if(val & BV(BOARD_LED0_PIN))
        bits |= 0x1;
    val = pfGpioPortRead(BOARD_LED1_PORT);
    if(val & BV(BOARD_LED1_PIN))
        bits |= 0x2;
    val = pfGpioPortRead(BOARD_LED2_PORT);
    if(val & BV(BOARD_LED2_PIN))
        bits |= 0x4;

    return bits;
}

PFbyte buttons_read(void)
{
    PFbyte bits = 0;

    PFdword val = pfGpioPortRead(BOARD_BTN0_PORT);
    if(val & BV(BOARD_BTN0_PIN))
        bits |= 0x1;
    val = pfGpioPortRead(BOARD_BTN1_PORT);
    if(val & BV(BOARD_BTN1_PIN))
        bits |= 0x2;
    val = pfGpioPortRead(BOARD_BTN2_PORT);
    if(val & BV(BOARD_BTN2_PIN))
        bits |= 0x4;

    return bits;
}

PFbyte board_get_char(void)
{
    PFbyte tmp = 0;
    if(pfUart0ReadByte(&tmp) == 0)
		return tmp;
	else
		return 0xFF;	
}

void board_put_char(PFbyte c)
{
    pfUart0WriteByte(c); 
}
