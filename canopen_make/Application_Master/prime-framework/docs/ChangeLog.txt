Author:		Swapnil
Date:		04-09-2014
Changes:
1. 	Added ChangeLog.txt in rootdir/docs
============================================================================================================
Author:		Swapnil
Date:		10-09-2014
Changes:
1. 	Added 'extensions' folder to framework. 
	This folder will contain the extended functionalities like filesystem, communication protocol and protocol stacks like USB, TCP/IP, CanOpen, etc.
	Currently LPCUSBLib is added.
============================================================================================================
Author:		Sandeep
Date:		23-09-2014
Changes:
1.	Added polling mode support for I2C for all platforms.
2.	TWI_VECT changed to I2C_VECT in AVR platforms regmap.
============================================================================================================
Author:		Vipul
Date:		23-09-2014
Changes:
1.	Left adjust condition reversed in ADC for ATmega2560
============================================================================================================
Author:		Jaimin
Date:		23-09-2014
Changes:
1. 	Added support for multiple message write in pfCanWrite() for LPC1768.
2.	Improved CAN Tx buffer selection method.
============================================================================================================
Author:		Swapnil
Date:		24-09-2014
Changes:
1.	Added enum for system endianness in prime_types.h
2.	Added prime_utils.c in utils folder for general utility functions.
3. 	Added pfGetSystemEndian() function in prime_utils.c
4.	Added endianness macro for all platforms (PF_LITTLE_ENDIAN).
5.	Added boards folder. This folder will contain board config files.
6.	Added IAP module for LPC1768. (Not tested yet)
============================================================================================================
Author:		Swapnil
Date:		25-09-2014
Changes:
1.	Added following functions in timer module for LPC1768:
		pfTimerIntEnable
		pfTimerIntDisable
		pfTimerGetTickFreq
		pfTimerSetTickFreq
============================================================================================================
Author:		Swapnil
Date:		26-09-2014
Changes:
1.	Install scripts added.
	When install scriptis run, the task is furst rebuild and all include files are copied to root/install/include and root/install/include/$(MCU_CHIP)
	And library file is copied to root/install/include/$(MCU_CHIP)/$(BUILD_MODE)
	Install folder can be directly copied to/referenced in user project.
2.	Macros for endianness added in arch.h
3.	Enum for endianness added in prime_types.h
4.	prime_utils.c added in utils directory.
5.	Function for checking system endianness added in prime_utils.c
============================================================================================================
Author:		Tushar
Date:		29-09-2014
Changes:
1.	Modified CAN1 write function for LPC1768:
		local variables initialised to zero.
============================================================================================================
Author:		Swapnil
Date:		07-10-2014
Changes:
1.	File system module added in extensions.
	File create, read write function tested.
2.	GetErrorString function added in prime_utils.c. The will return a string corresponding to the error codes in PFEnStatus.
3.	Added macros for ASCII values for backspace, tab, cr, lf, and escape in prime_utils.h	
============================================================================================================
Author:		Tushar
Date:		09-10-2014
Changes:
1.	Modified Lynx driver config structure in prime_a3977.h.
2.      Modified SmartLynx driver config structure in prime_l6470.h:
	  - Two macros added to check for maximum driver speed limit and maximum limit of minimum speed setting.
	  - L6470_DAISY_CHAIN_DEVICES_COUNT macro who defines numer of smartlynx in daisy chain replaced by putting deviceCount parameter in open function.
	  - L6470_MAX_DAISY_CHAIN_DEVICES_SUPPORTED macro added to limit number of smartlynx supported.

3.      Modified SmartLynx driver prime_l6470.c:
	  - Driver is made Id based.
          - In open function maximum speed limit initialization is added.
4.	Modified prime_pwm.c, prescalar setting is changed.
5.	Modified prime_timer.c, prescalar setting is changed.	
============================================================================================================