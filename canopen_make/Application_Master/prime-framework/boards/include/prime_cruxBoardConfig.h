#pragma once
#ifdef MCU_CHIP_at90can128
/**	LED GPIO Configuration Macros	*/

#define		CRUX_LED_UL1_PORT			GPIO_PORTG
#define		CRUX_LED_UL1_PIN			BIT_3

#define		CRUX_LED_UL2_PORT			GPIO_PORTG
#define		CRUX_LED_UL2_PIN			BIT_4

#define		CRUX_LED_UL3_PORT			GPIO_PORTD
#define		CRUX_LED_UL3_PIN			BIT_2


/**	Push Button GPIO Configuration Macros  */

#define		CRUX_PUSHBUTTON_PB1_PORT		GPIO_PORTD
#define		CRUX_PUSHBUTTON_PB1_PIN		BIT_3

#define		CRUX_PUSHBUTTON_PB2_PORT		GPIO_PORTD
#define		CRUX_PUSHBUTTON_PB2_PIN		BIT_4

#define		CRUX_PUSHBUTTON_PB3_PORT		GPIO_PORTD
#define		CRUX_PUSHBUTTON_PB3_PIN		BIT_7


/**	USART GPIO Configuration Macros		*/

/**	USART0	*/

#define		CRUX_J3_15_USART0_RXD0_PORT		GPIO_PORTE
#define		CRUX_J3_15_USART0_RXD0_PIN		BIT_0

#define		CRUX_J3_16_USART0_TXD0_PORT		GPIO_PORTE
#define		CRUX_J3_16_USART0_TXD0_PIN		BIT_1	

#define		CRUX_J4_9_USART0_XCK0_PORT		GPIO_PORTE
#define		CRUX_J4_9_USART0_XCK0_PIN		BIT_2	

/**	USART1	
 *\note	If USART1 is used then JP2SW3 and JP3SW1 switches should be open.
 */
#define		CRUX_J4_5_USART1_RXD1_PORT		GPIO_PORTD	
#define		CRUX_J4_5_USART1_RXD1_PIN		BIT_2

#define		CRUX_J4_4_USART1_TXD1_PORT		GPIO_PORTD
#define		CRUX_J4_4_USART1_TXD1_PIN		BIT_3


/**  External Interrupt Source GPIO Configuration Macros */

/**	INT0	*/

#define		CRUX_J3_11_INT0_PORT			GPIO_PORTD
#define		CRUX_J3_11_INT0_PIN			BIT_0

/**INT1		*/

#define		CRUX_J3_12_INT1_PORT			GPIO_PORTD
#define		CRUX_J3_12_INT1_PIN			BIT_1

/**	INT2	*/

#define		CRUX_J4_5_INT2_PORT			GPIO_PORTD				         
#define		CRUX_J4_5_INT2_PIN			BIT_2

/**	INT3	*/

#define		CRUX_J4_4_INT3_PORT			GPIO_PORTD						
#define		CRUX_J4_4_INT3_PIN			BIT_3

/**	INT4	*/

#define		CRUX_J3_17_INT4_PORT			GPIO_PORTE						
#define		CRUX_J3_17_INT4_PIN			BIT_4

/**	INT5	*/

#define 	CRUX_J3_18_INT5_PORT			GPIO_PORTE						
#define 	CRUX_J3_18_INT5_PIN			BIT_5


/**	Timer GPIO Configuration Macros	*/

/**	Timer0   */

#define		CRUX_J3_7_TIMER0_OC0A_PORT		GPIO_PORTB
#define 	CRUX_J3_7_TIMER0_OC0A_PIN		BIT_7

#define CRUX_J4_2_TIMER0_T0_PORT			GPIO_PORTD	
#define CRUX_J4_2_TIMER0_T0_PIN			BIT_7

/**	Timer1	*/

#define 	CRUX_J3_4_TIMER1_OC1A_PORT		GPIO_PORTB
#define 	CRUX_J3_4_TIMER1_OC1A_PIN		BIT_5

#define 	CRUX_J3_5_TIMER1_OC1B_PORT		GPIO_PORTB
#define 	CRUX_J3_5_TIMER1_OC1B_PIN		BIT_6

#define  	CRUX_J3_7_TIMER_OC1C_PORT		GPIO_PORTB
#define  	CRUX_J3_7_TIMER_OC1C_PIN		BIT_7

#define 	CRUX_J4_3_TIMER1_ICP1_PORT		GPIO_PORTD	
#define 	CRUX_J4_3_TIMER1_ICP1_PIN		BIT_4

/**	Timer2	*/

#define 	CRUX_J3_3_TIMER2_OC2A_PORT		GPIO_PORTB
#define 	CRUX_J3_3_TIMER2_OC2A_PIN		BIT_4

#define 	CRUX_J4_6_TIMER2_TOSC1_PORT		GPIO_PORTG
#define 	CRUX_J4_6_TIMER2_TOSC1_PIN		BIT_4

#define 	CRUX_J4_7_TIMER2_TOSC2_PORT		GPIO_PORTG
#define 	CRUX_J4_7_TIMER2_TOSC2_PIN		BIT_3

/**	Timer3	*/

#define 	CRUX_J4_8_TIMER3_OC3A_PORT		GPIO_PORTE
#define 	CRUX_J4_8_TIMER3_OC3A_PIN		BIT_3

#define 	CRUX_J3_17_TIMER3_OC3B_PORT		GPIO_PORTE
#define 	CRUX_J3_17_TIMER3_OC3B_PIN		BIT_4

#define 	CRUX_J3_18_TIMER3_OC3C_PORT		GPIO_PORTE
#define 	CRUX_J3_18_TIMER3_OC3C_PIN		BIT_5



/**	ADC GPIO Configuration Macros	*/

/**	ADC0	*/
#define		CRUX_J4_19_ADC0_PORT			GPIO_PORTF
#define		CRUX_J4_19_ADC0_PIN			BIT_0

/**	ADC1	*/  		
#define		CRUX_J4_18_ADC1_PORT			GPIO_PORTF
#define		CRUX_J4_18_ADC1_PIN			BIT_1

/**	ADC2	*/ 		
#define		CRUX_J4_17_ADC2_PORT			GPIO_PORTF
#define		CRUX_J4_17_ADC2_PIN			BIT_2

/**	ADC3	*/
#define		CRUX_J4_16_ADC3_PORT			GPIO_PORTF
#define		CRUX_J4_16_ADC3_PIN			BIT_3

/**	ADC4    */
#define		CRUX_J4_15_ADC4_PORT			GPIO_PORTF
#define		CRUX_J4_15_ADC4_PIN			BIT_4

/**	ADC5	*/   
#define		CRUX_J4_14_ADC5_PORT			GPIO_PORTF
#define		CRUX_J4_14_ADC5_PIN			BIT_5

/**	ADC6	*/
#define		CRUX_J4_13_ADC6_PORT			GPIO_PORTF
#define		CRUX_J4_13_ADC6_PIN			BIT_6

/**	ADC7   */
#define		CRUX_J4_12_ADC7_PORT			GPIO_PORTF
#define		CRUX_J4_12_ADC7_PIN			BIT_7
																		


/**	SPI GPIO Configuration Macros	*/

#define 	CRUX_J3_9_SPI_MOSI_PORT		GPIO_PORTB
#define 	CRUX_J3_9_SPI_MOSI_PIN		BIT_2

#define 	CRUX_J3_8_SPI_MISO_PORT		GPIO_PORTB
#define 	CRUX_J3_8_SPI_MISO_PIN		BIT_3

#define 	CRUX_J3_6_SPI_SCK_PORT		GPIO_PORTB
#define 	CRUX_J3_6_SPI_SCK_PIN		BIT_1

#define		CRUX_SPI_SS_PORT			GPIO_PORTB
#define		CRUX_SPI_SS_PIN			BIT_0



/** I2C GPIO Configuration Macros*/

#define 	CRUX_J3_12_I2C_SDA_PORT		GPIO_PORTD
#define 	CRUX_J3_12_I2C_SDA_PIN		BIT_1

#define 	CRUX_J3_11_I2C_SCL_PORT		GPIO_PORTD
#define 	CRUX_J3_11_I2C_SCL_PIN		BIT_0


 
/**	CAN GPIO Configuration Macros	*/
 

#define 	CRUX_J3_14_CAN_TXCAN_PORT		GPIO_PORTD
#define 	CRUX_J3_14_CAN_TXCAN_PIN		BIT_5

#define 	CRUX_J3_13_CAN_RXCAN_PORT		GPIO_PORTD
#define 	CRUX_J3_13_CAN_RXCAN_PIN		BIT_6


/**	Analog Comparator GPIO Configuration Macros	 */
 
#define 	CRUX_J4_9_ACOMP_AIN0_PORT		GPIO_PORTE
#define 	CRUX_J4_9_ACOMP_AIN0_PIN		BIT_2

#define 	CRUX_J4_8_ACOMP_AIN1_PORT		GPIO_PORTE
#define 	CRUX_J4_8_ACOMP_AIN1_PIN		BIT_3

 
/**	Divided System Clock GPIO Configuration Macros	
 *\note note When the CKOUT Fuse is programmed, the system Clock will be output on CLKO.
 */ 

#define 	CRUX_J3_10_CLKO_PORT			GPIO_PORTC
#define 	CRUX_J3_10_CLKO_PIN			BIT_7

/** LED gpio pin configuration */
extern PFCfgGpio pfCruxLEDGpioCfg[3];

/** Pushbutton gpio pin configuration */
extern PFCfgGpio pfCruxPushButtonGpioCfg[3];

extern PFCfgGpio pfCruxUsart0MasterGpioCfg[3];		 /** For Synchronous master Mode of USART0 */
extern PFCfgGpio pfCruxUsart0SlaveGpioCfg[3];		 /** For Synchronous Slave Mode of USART0 */

extern PFCfgGpio pfCruxUart0MGpioCfg[2];             /** For Asynchronous Master of USART0 */
extern PFCfgGpio pfCruxUart1MGpioCfg[2];             /** For Asynchronous Mode of USART1 */

extern PFCfgGpio pfCruxInt0GpioCfg;                  /** EINT0 gpio pins configuration */
extern PFCfgGpio pfCruxInt1GpioCfg;                  /** EINT1 gpio pins configuration */
extern PFCfgGpio pfCruxInt2GpioCfg;                  /** EINT2 gpio pins configuration */
extern PFCfgGpio pfCruxInt3GpioCfg;                  /** EINT3 gpio pins configuration */
extern PFCfgGpio pfCruxInt4GpioCfg;                  /** EINT4 gpio pins configuration */
extern PFCfgGpio pfCruxInt5GpioCfg;                  /** EINT5 gpio pins configuration */

extern PFCfgGpio pfCruxADC0GpioCfg;                  /** ADC0 gpio pins configuration */
extern PFCfgGpio pfCruxADC1GpioCfg;                  /** ADC1 gpio pins configuration */
extern PFCfgGpio pfCruxADC2GpioCfg;                  /** ADC2 gpio pins configuration */
extern PFCfgGpio pfCruxADC3GpioCfg;                  /** ADC3 gpio pins configuration */
extern PFCfgGpio pfCruxADC4GpioCfg;                  /** ADC4 gpio pins configuration */
extern PFCfgGpio pfCruxADC5GpioCfg;                  /** ADC5 gpio pins configuration */
extern PFCfgGpio pfCruxADC6GpioCfg;                  /** ADC6 gpio pins configuration */
extern PFCfgGpio pfCruxADC7GpioCfg;                  /** ADC7 gpio pins configuration */

extern PFCfgGpio pfCruxSPIMasterGpioCfg[4];          /** SSP in master mode gpio pin configuration */
extern PFCfgGpio pfCruxSPISlaveGpioCfg[4];           /** SSP in slave mode gpio pin configuration */

extern PFCfgGpio pfCruxI2CGpioCfg[2];                /** I2C gpio pin configuration */
extern PFCfgGpio pfCruxCANCGpioCfg[2];               /** CAN gpio pin configuration */
extern PFCfgGpio pfCruxAnlgCompGpioCfg[2];           /** Analog comparator gpio pin configuration */

#endif	// #ifdef MCU_CHIP_at90can128
