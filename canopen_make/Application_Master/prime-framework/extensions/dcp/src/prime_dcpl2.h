#ifndef PRIMEDCP_L2_H_
#define PRIMEDCP_L2_H_

#include "prime_dcpcore.h"

typedef struct
{
    PFpgeneric  rxHandlerArg;
    PFpgeneric  wrCompArg;
    PFpgeneric  pCfg;
} PFCfgDcpL2;
typedef const PFCfgDcpL2 PFfxCfgDcpL2;
typedef PFCfgDcpL2 * PFpCfgDcpL2;
typedef const PFCfgDcpL2 * PFpfxCfgDcpL2;

typedef PFEnStatus (*PFdcpL2Init)(PFdcpl2IfaceNum uIfNum, PFpfxCfgDcpL2 pCfg);
typedef PFEnStatus (*PFdcpL2Finilize)(PFdcpl2IfaceNum uIfNum);
typedef PFEnStatus (*PFdcpL2StartWrite)(PFdcpl2IfaceNum uIfNum, PFpdcpHwAddr pHwAddr, PFdword uPortId, PFpdcpfxmemptr pBuf, 
                                           PFdcpfxmemsize uBufSz, PFpgeneric pPackDesc);
typedef void (*pfDcpL2RxCheck)(void);

typedef struct
{
    PFdcpMtuSize        uMtuSize;
    PFdcpHwAddrSize     uHwAddrSize;
    PFdcpL2Init           init;
	PFdcpL2Finilize       finalize;
	PFdcpL2StartWrite     startWrite;
	pfDcpL2RxCheck		rxCheck;
} PFDcpL2Drv;
typedef PFDcpL2Drv * PFpDcpL2Drv;

typedef struct
{
    PFbyte                      bBusy;
    PFpgeneric                  wrCompArg;
    PFpgeneric                  packDesc;
    PFdcpmemsize            	uBufSz;
}PFDcpL2DriverWriteData;

typedef struct
{
    PFbyte                     	buf[PF_DCP_L2RX_BUFFER_SIZE];
    PFdcpmemsize            	uByteRead;
    PFpgeneric                  rxHandlerArg;
} PFDcpL2DriverReadData;

typedef struct
{
    PFdcpl2IfaceNum             uIfNum;
    PFDcpL2DriverWriteData		wrData;
    PFDcpL2DriverReadData		rdData;
} PFDcpL2DriverData;
typedef PFDcpL2DriverData * PFpDcpL2DriverData;

#endif /*PRIMEDCP_L2_H_*/


