#ifndef PRIMEDCP_DEBUG_H_
#define PRIMEDCP_DEBUG_H_
#include "prime_debug.h"

PF_EXTERN_C_BEGIN

#define PF_DCP_ERR(...)		PF_DCP_DEBUG_PRINT("PF_DCP_ERR: " __VA_ARGS__)

#if(PF_DCP_DEBUG)
#define PF_DCP_TRACE(...)		PF_DCP_DEBUG_PRINT("PF_DCP: " __VA_ARGS__)
#else
#define PF_DCP_TRACE(...)		        do{}while(0)
#endif

PF_EXTERN_C_END

#endif /*PRIMEDCP_DEBUG_H_*/
