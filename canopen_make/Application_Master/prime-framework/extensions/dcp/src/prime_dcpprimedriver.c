#include "prime_dcpl2.h"
#include "prime_dcppacket.h"
#include "prime_dcpint.h"

#include "prime_framework.h"
#include "prime_sysClk.h"
#include "prime_gpio.h"
#include "prime_delay.h"
#include "prime_rit.h"
#include "prime_timer0.h"
#include "prime_fifo.h"
#include "prime_canCommon.h"
#include "prime_can2.h"
#include "prime_uart2.h"

#define CAN_MASK	0x00010000
#define UART_MASK	0x00020000
#define SPI_MASK	0x00030000

#define TYPE_MASK	0xFFFF0000
#define PORT_MASK	0x0000FFFF

static PFEnStatus pfDcpL2primeDriverInit(PFdcpl2IfaceNum uProtoCtxNum, PFpfxCfgDcpL2 pCfg);
static PFEnStatus pfDcpL2primeDriverFinalize(PFdcpl2IfaceNum uProtoCtxNum);
static PFEnStatus pfDcpL2primeDriverStartWrite(PFdcpl2IfaceNum uProtoCtxNum, PFpdcpHwAddr pHwAddr, PFdword uPortId,
    PFpdcpfxmemptr pBuf, PFdcpfxmemsize uBufSz, PFpgeneric pPackDesc);
void pfDcpPrimeRxCheck(void);
	
#define PF_DCP_PRIMEDRIVER_IFNUM             PF_DCP_MAX_INSTANCE_SUPPORTED
#define PF_DCP_PRIMEDRIVER_DATA_GET(_n_)     (&gs_primeDrvData[_n_])

PFDcpL2Drv g_primeL2Driver = 
{
    PF_C_C99INIT(uMtuSize, PF_DCP_L2TX_BUFFER_SIZE),
    PF_C_C99INIT(uHwAddrSize, PF_DCP_MAX_HWADRRESS_SIZE),
    PF_C_C99INIT(init, pfDcpL2primeDriverInit),
    PF_C_C99INIT(finalize, pfDcpL2primeDriverFinalize),
    PF_C_C99INIT(startWrite, pfDcpL2primeDriverStartWrite),
	PF_C_C99INIT(rxCheck, pfDcpPrimeRxCheck)
};
//Gloabal Variables for CAN
PFCanMsgHeader msgHeader;
PFCanMessage rxMsg;
//Gloabal Variables for Uart
static PFbyte rxUartBuf[PF_DCP_L2RX_BUFFER_SIZE];
//Gloabal Variables for SPI
static PFbyte rxSpiBuf[PF_DCP_L2RX_BUFFER_SIZE];

PFDcpL2DevDesc          primeDevDesc;

static PFDcpL2DriverData  gs_primeDrvData[PF_DCP_PRIMEDRIVER_IFNUM];
static PFbyte gs_hwAddr[PF_DCP_MAX_HWADRRESS_SIZE];

static PFEnBoolean l2PrimeProtoCtxInitFlag[PF_DCP_PRIMEDRIVER_IFNUM] ={enBooleanFalse};
static PFbyte l2PrimeProtoCtxCount=0;

static PFEnBoolean l2CanIfaceInitFlag[PF_DCP_L2CAN_MAX_INTERFACE] = {enBooleanFalse};
static PFCfgDcpL2Can l2CanConfig[PF_DCP_L2CAN_MAX_INTERFACE];
static PFbyte l2CanIfaceCount=0;

static PFEnBoolean l2UartIfaceInitFlag[PF_DCP_L2UART_MAX_INTERFACE]={enBooleanFalse};
static PFCfgDcpL2Uart l2UartConfig[PF_DCP_L2UART_MAX_INTERFACE];
static PFbyte l2UartIfaceCount=0;

static PFEnBoolean l2SpiIfaceInitFlag[PF_DCP_L2SPI_MAX_INTERFACE] = {enBooleanFalse};
static PFCfgDcpL2Spi l2SpiConfig[PF_DCP_L2SPI_MAX_INTERFACE];
static PFbyte spiSelId[PF_DCP_L2SPI_MAX_INTERFACE];
static PFbyte l2SpiIfaceCount=0;

//-----------------------------------------------------------------------------------
   
PFEnStatus pfDcpL2CanInterfaceRegister(PFdword *pPortId, PFpCfgDcpL2Can pIfaceCfg)
{
	PFbyte i;
	PF_ASSERT_RET(pPortId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(pIfaceCfg != PF_NULL_PTR, enStatusInvArgs);
 	PF_ASSERT_RET(l2CanIfaceCount < PF_DCP_L2CAN_MAX_INTERFACE, enStatusNotSupported);
	for(i=0;i<PF_DCP_L2CAN_MAX_INTERFACE;i++)
	{
		if(l2CanIfaceInitFlag[i] == enBooleanFalse)
		{
			pfMemCopy(&l2CanConfig[i], pIfaceCfg, sizeof(PFCfgDcpL2Can));
			l2CanIfaceInitFlag[i] = enBooleanTrue;
			*pPortId = CAN_MASK | i;
			l2CanIfaceCount++;
			break;
		}
	}
	if(i == PF_DCP_L2CAN_MAX_INTERFACE)
	{
		return enStatusError;
	}
	return enStatusSuccess;
}
PFEnStatus pfDcpL2UartInterfaceRegister(PFdword *pPortId, PFpCfgDcpL2Uart pIfaceCfg)
{
	PFbyte i;
	PF_ASSERT_RET(pPortId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(pIfaceCfg != PF_NULL_PTR, enStatusInvArgs);
 	PF_ASSERT_RET(l2UartIfaceCount < PF_DCP_L2CAN_MAX_INTERFACE, enStatusNotSupported);
	
	for(i=0;i<PF_DCP_L2CAN_MAX_INTERFACE;i++)
	{
		if(l2UartIfaceInitFlag[i] == enBooleanFalse)
		{
			pfMemCopy(&l2UartConfig[i],pIfaceCfg,sizeof(PFCfgDcpL2Uart));
			l2UartIfaceInitFlag[i] = enBooleanTrue;
			*pPortId = UART_MASK | i;
			l2UartIfaceCount++;
			break;
		}
	}
	if(i == PF_DCP_L2UART_MAX_INTERFACE)
	{
		return enStatusError;
	}
	return enStatusSuccess;
}
PFEnStatus pfDcpL2SpiInterfaceRegister(PFdword *pPortId, PFpCfgDcpL2Spi pIfaceCfg)
{
    PFbyte i;
    PFEnStatus status;
	PF_ASSERT_RET(pPortId != PF_NULL_PTR, enStatusInvArgs);
	PF_ASSERT_RET(pIfaceCfg != PF_NULL_PTR, enStatusInvArgs);
    PF_ASSERT_RET(l2SpiIfaceCount < PF_DCP_L2SPI_MAX_INTERFACE, enStatusNotSupported);
    for(i=0;i<PF_DCP_L2SPI_MAX_INTERFACE;i++)
    {
        if(l2SpiIfaceInitFlag[i] == enBooleanFalse)
        {
            pfMemCopy(&l2SpiConfig[i],pIfaceCfg,sizeof(PFCfgDcpL2Spi));
            status = l2SpiConfig[i].spiRegisterDevice(&spiSelId[i],&l2SpiConfig[i].spiChipSel); 
            if(status != enStatusSuccess){
                return status;
            }
#if(PF_DCP_L2SPI_ENABLE_MASTER != 1)            
            pfGpioPinsClear(l2SpiConfig[i].spiDcpSync.port ,l2SpiConfig[i].spiDcpSync.pin);
#endif
            l2SpiConfig[i].spiChipSelect(&spiSelId[i],1);
            l2SpiIfaceInitFlag[i] = enBooleanTrue;
            *pPortId = SPI_MASK | i;
            l2SpiIfaceCount++;
            break;
        }
    }
    if(i == PF_DCP_L2SPI_MAX_INTERFACE )
    {
        return enStatusError;
    }
    
    return enStatusSuccess;
}
//-----------------------------------------------------------------------------------
static PFEnStatus pfDcpL2primeDriverInit(PFdcpl2IfaceNum uProtoCtxNum, PFpfxCfgDcpL2 pCfg)
{
	PFpDcpL2DriverData pHwData;
	pHwData = PF_DCP_PRIMEDRIVER_DATA_GET(uProtoCtxNum);
	
	//Check Init count 
	
    if(pHwData == PF_NULL_PTR)
    {
        PF_DCP_ERR("ERROR: invalid device number\n");
        return enStatusInvArgs;
    }
	pfMemSet(&pHwData->wrData, 0, sizeof(PFDcpL2DriverWriteData));
    pHwData->wrData.wrCompArg = pCfg->wrCompArg;

    pfMemSet(&pHwData->rdData, 0, sizeof(PFDcpL2DriverReadData));
    pHwData->rdData.rxHandlerArg = pCfg->rxHandlerArg;
		
    pHwData->uIfNum = uProtoCtxNum;
	primeDevDesc.uIfaceNum = uProtoCtxNum;
	primeDevDesc.pDrv = (PFpdcpmemptr)&g_primeL2Driver;
	
	msgHeader.frameFormat = enCanFrameStandard;
	msgHeader.remoteFrame = enBooleanFalse;
		
	l2PrimeProtoCtxInitFlag[uProtoCtxNum] = enBooleanTrue;
	l2PrimeProtoCtxCount++;

  return enStatusSuccess;
}

static PFEnStatus pfDcpL2primeDriverFinalize(PFdcpl2IfaceNum uProtoCtxNum)
{
	l2PrimeProtoCtxInitFlag[uProtoCtxNum] = enBooleanFalse;
	l2PrimeProtoCtxCount--;
    return enStatusSuccess;
}

static PFEnStatus pfDcpL2primeDriverStartWrite(PFdcpl2IfaceNum uProtoCtxNum, PFpdcpHwAddr pHwAddr, PFdword uPortId, PFpdcpfxmemptr pBuf, 
                        PFdcpfxmemsize uBufSz, PFpgeneric pPackDesc)
{
    PFdword txCount,index;
    PFpDcpL2DriverData pHwData = PF_DCP_PRIMEDRIVER_DATA_GET(uProtoCtxNum);
	PFEnStatus status;
    PFbyte spiRxbyte;
	
	if(pHwData == PF_NULL_PTR)
    {
        PF_DCP_ERR("ERROR: invalid device number\n");
        return enStatusInvArgs;
    }
	if(pHwData->wrData.bBusy) 
    {
        PF_DCP_ERR("ERROR: device BUSY!\n");
        return enStatusBusy;
    }
    
    pHwData->wrData.bBusy = 1;
    pHwData->wrData.packDesc = pPackDesc;
    pHwData->wrData.uBufSz = uBufSz;
    
    switch( uPortId & TYPE_MASK)
    {
        case CAN_MASK:
            if(l2CanIfaceInitFlag[uPortId & PORT_MASK] == enBooleanFalse){
                return enStatusInvArgs;
            }
			pfMemCopy(&msgHeader.id,pHwAddr,sizeof(PFdword)); 
            status = l2CanConfig[uPortId & PORT_MASK].canWrite(&msgHeader,(PFbyte*)pBuf,uBufSz);		
            if(status != enStatusSuccess){ 
                return status;
            }    
            do{
                l2CanConfig[uPortId & PORT_MASK].canGetTxBufferCount(&txCount);
            }while(txCount != 0);
            break;
        case UART_MASK:
            if(l2UartIfaceInitFlag[uPortId & PORT_MASK] == enBooleanFalse){
                return enStatusInvArgs;
            }
            status = l2UartConfig[uPortId & PORT_MASK].uartWrite((PFbyte*)pBuf,uBufSz);
            if(status != enStatusSuccess){
                return status;
            }
            do{
                l2UartConfig[uPortId & PORT_MASK].uartGetTxBufferCount(&txCount);
            }while(txCount !=0);
            break;
        case SPI_MASK:
            if(l2SpiIfaceInitFlag[uPortId & PORT_MASK] == enBooleanFalse){
                return enStatusInvArgs;
            }
            
#if(PF_DCP_L2SPI_ENABLE_MASTER == 1)
            for(index=0;index<uBufSz;index++)
            {
                pfDelayMicroSec(PF_DCP_L2SPI_SYNC_DELAY);
                l2SpiConfig[uPortId & PORT_MASK].spiChipSelect(&spiSelId[uPortId & PORT_MASK],0);
                l2SpiConfig[uPortId & PORT_MASK].spiExchangeByte(&spiSelId[uPortId & PORT_MASK], pBuf[index],&spiRxbyte); //Change: Do type cast in buffer
                l2SpiConfig[uPortId & PORT_MASK].spiChipSelect(&spiSelId[uPortId & PORT_MASK],1);
            }
            status = enStatusSuccess;
#else   //Slave Mode
            l2SpiConfig[uPortId & PORT_MASK].spiChipSelect(&spiSelId[uPortId & PORT_MASK],0);
            status = l2SpiConfig[uPortId & PORT_MASK].spiWrite(&spiSelId[uPortId & PORT_MASK],(PFbyte*)pBuf,uBufSz);
            if(status != enStatusSuccess){
                return status;
            }
            pfGpioPinsSet(l2SpiConfig[uPortId & PORT_MASK].spiDcpSync.port ,l2SpiConfig[uPortId & PORT_MASK].spiDcpSync.pin);
            do{
                l2SpiConfig[uPortId & PORT_MASK].spiGetTxBufferCount(&txCount);
            }while(txCount !=0);
            pfGpioPinsClear(l2SpiConfig[uPortId & PORT_MASK].spiDcpSync.port ,l2SpiConfig[uPortId & PORT_MASK].spiDcpSync.pin);
             l2SpiConfig[uPortId & PORT_MASK].spiChipSelect(&spiSelId[uPortId & PORT_MASK],1);
#endif  //(PF_DCP_L2SPI_ENABLE_MASTER == 1)
            break;
        default :
            PF_DCP_ERR("Invalid Port number\n");
            return enStatusInvArgs;
    }
            
             
		
                
	pfDcpWriteCompleted(pHwData->wrData.wrCompArg, pHwData->wrData.packDesc);
	pHwData->wrData.bBusy = 0;

    return status;
}

void pfDcpPrimeRxCheck(void)
{
	PFdword rxCount=0,rxByteRead=0,index=0;
	PFbyte	uId=0,uProCtxNum=0;
	PFEnStatus status;
	PFpDcpL2DriverData pHwData;

//UART RX CHECK	
	for(uId = 0;uId<PF_DCP_L2UART_MAX_INTERFACE;uId++)
	{
		if(l2UartIfaceInitFlag[uId] == enBooleanTrue)
		{
			l2UartConfig[uId].uartGetRxBufferCount(&rxCount);			
			if(rxCount >= 1)
			{
				status = l2UartConfig[uId].uartRead(rxUartBuf,rxCount,&rxByteRead);
				if(status == enStatusSuccess)
				{
                    primeDevDesc.uPortId = UART_MASK | uId ;
                    pfMemCopy(gs_hwAddr,&primeDevDesc.uPortId,sizeof(PFdword));
					for(uProCtxNum=0;uProCtxNum<PF_DCP_PRIMEDRIVER_IFNUM;uProCtxNum++)
					{
						if(l2PrimeProtoCtxInitFlag[uProCtxNum] == enBooleanTrue)
						{
							pHwData = PF_DCP_PRIMEDRIVER_DATA_GET(uProCtxNum);
							pfMemCopy(pHwData->rdData.buf, rxUartBuf,rxByteRead);	//---TODO:make efficient 
							pHwData->rdData.uByteRead = rxByteRead;
							primeDevDesc.uIfaceNum = uProCtxNum;
							pfDcpRxHandler(pHwData->rdData.rxHandlerArg, &primeDevDesc, gs_hwAddr, pHwData->rdData.buf, pHwData->rdData.uByteRead);
						}
					}
				}
			}
		}
	}
//CAN RX CHECK
	for(uId = 0;uId<PF_DCP_L2CAN_MAX_INTERFACE;uId++)
	{
        if(l2CanIfaceInitFlag[uId] == enBooleanTrue)
        {
            l2CanConfig[uId].canGetRxBufferCount(&rxCount);
            if(rxCount >= 1)
            {
                status = l2CanConfig[uId].canRead(&rxMsg);
                if(status == enStatusSuccess)
                {
                    primeDevDesc.uPortId = CAN_MASK | uId;
                    for(uProCtxNum=0;uProCtxNum<PF_DCP_PRIMEDRIVER_IFNUM;uProCtxNum++)
                    {
                        if(l2PrimeProtoCtxInitFlag[uProCtxNum] == enBooleanTrue)
                        {
                            pHwData = PF_DCP_PRIMEDRIVER_DATA_GET(uProCtxNum);
                            pfMemCopy(pHwData->rdData.buf,rxMsg.data,rxMsg.length);	//---TODO:make efficient 
                            pHwData->rdData.uByteRead = rxMsg.length;
                            pfMemCopy(gs_hwAddr,&rxMsg.id,sizeof(PFdword));
                            primeDevDesc.uIfaceNum = uProCtxNum;
                            pfDcpRxHandler(pHwData->rdData.rxHandlerArg, &primeDevDesc, gs_hwAddr, pHwData->rdData.buf,pHwData->rdData.uByteRead);
                        }
                    }
                }
            }
        }
	}
//SPI RX CHECK
    for(uId = 0;uId<PF_DCP_L2SPI_MAX_INTERFACE;uId++)
    {
        if(l2SpiIfaceInitFlag[uId] == enBooleanTrue)
        {
#if(PF_DCP_L2SPI_ENABLE_MASTER == 1)
            if((pfGpioPortRead(l2SpiConfig[uId].spiDcpSync.port) & l2SpiConfig[uId].spiDcpSync.pin) == l2SpiConfig[uId].spiDcpSync.pin )
            {
                index = 0;
                while((pfGpioPortRead(l2SpiConfig[uId].spiDcpSync.port) & l2SpiConfig[uId].spiDcpSync.pin) == l2SpiConfig[uId].spiDcpSync.pin )
                {
                    pfDelayMicroSec(PF_DCP_L2SPI_SYNC_DELAY); // For Synchronizing with slave's ISR OR loop timing
                    l2SpiConfig[uId].spiChipSelect(&spiSelId[uId],0);
                    l2SpiConfig[uId].spiExchangeByte(&spiSelId[uId], '$',&rxSpiBuf[index++]);
                    l2SpiConfig[uId].spiChipSelect(&spiSelId[uId],1);
                    if(index>=PF_DCP_L2RX_BUFFER_SIZE)
                        break;
                }
                rxByteRead = index;
#else   //Slave Mode
            l2SpiConfig[uId].spiGetRxBufferCount(&rxCount);
            if(rxCount >= 1)
            {
                status = l2SpiConfig[uId].spiRead(&spiSelId[uId],rxSpiBuf,rxCount,&rxByteRead);
                if(status != enStatusSuccess){
                    break;
                }
#endif  //(PF_DCP_L2SPI_ENABLE_MASTER == 1)
                primeDevDesc.uPortId = SPI_MASK | uId ;
                pfMemCopy(gs_hwAddr,&primeDevDesc.uPortId,sizeof(PFdword));
                for(uProCtxNum=0;uProCtxNum<PF_DCP_PRIMEDRIVER_IFNUM;uProCtxNum++)
                {
                    if(l2PrimeProtoCtxInitFlag[uProCtxNum] == enBooleanTrue)
                    {
                        pHwData = PF_DCP_PRIMEDRIVER_DATA_GET(uProCtxNum);
                        pfMemCopy(pHwData->rdData.buf, rxSpiBuf,rxByteRead);
                        pHwData->rdData.uByteRead = rxByteRead;
                        primeDevDesc.uIfaceNum = uProCtxNum;
                        pfDcpRxHandler(pHwData->rdData.rxHandlerArg, &primeDevDesc, gs_hwAddr, pHwData->rdData.buf, pHwData->rdData.uByteRead);
                    }
                }
            }
        }
    }  
}


PFEnStatus pfDcpL2InterfaceRemove(PFdword *pPortId)
{
    switch( (*pPortId) & TYPE_MASK)
    {
        case CAN_MASK:
            l2CanIfaceInitFlag[(*pPortId) & PORT_MASK] = enBooleanFalse;
            l2CanIfaceCount--;
            break;
        case UART_MASK:
            l2UartIfaceInitFlag[(*pPortId) & PORT_MASK] = enBooleanFalse;
            l2UartIfaceCount--;
            break;
        case SPI_MASK:
            l2SpiIfaceInitFlag[(*pPortId) & PORT_MASK] = enBooleanFalse;
            l2SpiIfaceCount--;
            break;
        default:
            PF_DCP_ERR("Invalid Port number\n");
            return enStatusInvArgs;
    }
    return enStatusSuccess;
}


