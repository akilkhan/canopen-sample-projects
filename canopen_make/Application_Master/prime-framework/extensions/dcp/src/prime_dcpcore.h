#ifndef PRIMEDCP_CORE_H_
#define PRIMEDCP_CORE_H_

#include "prime_dcp.h"
#include "prime_dcptypes.h"
#include "prime_list.h"
#include "prime_dcpconfig.h"
#include "prime_dcpdebug.h"

/* WARNING: currently only window size 1 is supported */
#define PF_DCP_RX_WIN_SIZE              1

#define PF_DCP_TEST_FEATURES            1

#define PF_DCP_CONFIG_L2DEVDESC_COPY 	0

/** maximum number of L2 Driver supported */
#define	PF_DCP_MAX_DRIVER_SUPPORTED			1   //One Common Driver

// #define PF_DCP_RXBUFHDRS_ARRSIZE(_n_)   ((_n_) * sizeof(PFDcpInEndpoint))

typedef struct
{
    PFdcpl2IfaceNum uIfaceNum;
	PFpdcpmemptr     pDrv;
	PFdword				uPortId;
} PFDcpL2DevDesc;
typedef PFDcpL2DevDesc * PFpDcpL2DevDesc;
typedef const PFDcpL2DevDesc * PFpfxDcpL2DevDesc;


#include "prime_dcppacket.h"

typedef struct
{
    PFdcpDevAddr    netAddr;
	PFpDcpL2DevDesc pDev;
    PFpdcpHwAddr    pHwAddr;
	PFdword				uPortId;
} PFDcpRoute;
typedef PFDcpRoute * PFpDcpRoute;

typedef struct
{
    PFNode           node;
    PFpDcpRoute     pRoute;
    PFdcpDevAddr    uPeerAddr;
    PFList           packetBufs;
    PFdcpSeqNum     uWinSz;
    PFdcpSeqNum     uAckedPackNum;
} PFDcpOutEndpoint;
typedef PFDcpOutEndpoint * PFpDcpOutEndpoint;

typedef struct
{
    PFNode           node;
    PFdcpDevAddr    uPeerAddr;
    PFdcpSeqNum     uFirstNotAckedPackNum;
    PFpdcpmemptr     pData;
    PFdcpmemsize    uSize;
    PFtime              uExpTime;
#if PF_DCP_TEST_FEATURES == 1
    PFdcpSeqNum     uCurrFragsNum;
#endif
} PFDcpInEndpoint;
typedef PFDcpInEndpoint * PFpDcpInEndpoint;

struct PFdcpContextStruct;

typedef struct
{
    struct PFdcpContextStruct * pCtx;
    PFdcpServiceId              uServId;
    PFList                   outEps;
    PFList                   inEps;
    PFDcpTxResultHandler        pTxCb;
    PFpfxgeneric                pTxArg;
    PFDcpRxHandler              pRxCb;
    PFpfxgeneric                pRxArg;
    PFList                   freeInEps;
	PFdcpmemsize            uRxBufSz;
    PFdcpPacketId           uTxPackId;
    PFdcpSeqNum             uSeqNum;
    PFDcpStats              stats;
#if PF_DCP_TEST_FEATURES == 1
    PFdcpSeqNum             uAckMaxRetryNum;
    PFdcpSeqNum             uRetryCount;
    PFdcpSeqNum             uAckSeqNumShift;
    PFdcpServiceId          uForcedAckServId;
    PFdcpSeqNum             uWinResetFragNum;
    PFdcpSeqNum             uTxFragSeqNumShift;
#endif
} PFDcpServiceContext;
typedef PFDcpServiceContext * PFpDcpServiceContext;
typedef PFDcpServiceContext ** PFppDcpServiceContext;

typedef struct PFdcpContextStruct
{
    PFdcpDevAddr            uOwnAddr;
    PFdcpDevAddr            uOwnMask;
    PFpDcpL2DevDesc         pL2Devs;
    PFdcpl2DevsNum          uL2DevsNum;
    PFpDcpRoute             pStaticRoutes;
    PFdcpRoutesNum          uStaticRoutesNum;          
#if PF_DCP_CONFIG_DYNAMIC_ROUTES
    PFpDcpRoute             pDynamicRoutes;
    PFdcpRoutesNum          uDynamicRoutesNum;
    PFdcpRoutesNum          uDynamicRoutesUsedNum;
#endif
    PFEnBoolean                 uDynamicRoutesInitFlag[PF_DCP_MAX_DYNAMICROUTES_SUPPORTED];
    PFppDcpServiceContext   pServices;
    PFdcpServNum            uServMaxNum;
    PFList                   freePacketBufs;
    PFList                   freeIncomPacketBufs;
    PFList                   incomPacketBufs;
    PFList                   freeOutEps;
} PFDcpContext;
typedef PFDcpContext * PFpDcpContext;


#if (PF_DCP_TEST_FEATURES == 1)
/** enumeration for Service  Feature */
typedef enum
{
	enDcpServiceFeatureAckRetryNum       = 0,       
    enDcpServiceFeatureAckNumShift       = 1,        
    enDcpServiceFeatureForceAckServId    = 2,
    enDcpServiceFeatureWinResetFragNum   = 3,
    enDcpServiceFeatureTxFragSeqNumShift = 4,
} PFENDcpServiceFeature;
typedef const PFENDcpServiceFeature PFfxENDcpServiceFeature;

/**
 * This function sets service feature.
 *
 * \param pServId Pointer to specify Service Context Id.
 * \param eOpt Pointer to specify service feature.
 * \param pVal Pointer to specify value of service feture.
 *
 * \return Service option/feature value set status.
 */
PFEnStatus pfDcpServiceOptionsSet(PFbyte* pServId, PFfxENDcpServiceFeature eOpt, PFpfxgeneric pVal);

#endif //(PF_DCP_TEST_FEATURES == 1)

#endif /*PRIMEDCP_CORE_H_*/

