/**
 * CANopen Service Data Object - client protocol.
 */


#ifndef PF_CO_SDO_CLIENT
#define PF_CO_SDO_CLIENT

/**
 * @defgroup PF_CO_SDOmaster SDO client
 * @ingroup PF_CO_CANopen
 * @{
 *
 * CANopen Service Data Object - client protocol.
 *
 * @see @ref PF_CO_SDO
 */

/**
 * Return values of SDO client functions.
 */
typedef enum{
    /** Transmit buffer is full. Waiting */
    enCO_SDOcli_transmittBufferFull       = 4,
    /** Block download is in progress. Sending train of messages */
    enCO_SDOcli_blockDownldInProgress     = 3,
    /** Block upload in progress. Receiving train of messages */
    enCO_SDOcli_blockUploadInProgress     = 2,
    /** Waiting server response */
    enCO_SDOcli_waitingServerResponse     = 1,
    /** Success, end of communication */
    enCO_SDOcli_ok_communicationEnd       = 0,
    /** Error in arguments */
    enCO_SDOcli_wrongArguments            = -2,
    /** Communication ended with client abort */
    enCO_SDOcli_endedWithClientAbort      = -9,
    /** Communication ended with server abort */
    enCO_SDOcli_endedWithServerAbort      = -10,
    /** Communication ended with timeout */
    enCO_SDOcli_endedWithTimeout          = -11
}PFEnCoSdoClientReturn;


/**
 * SDO Client Parameter. The same as record from Object dictionary (index 0x1280+).
 */
typedef struct{
    /** Equal to 3 */
    PFbyte             maxSubIndex;
    /** Communication object identifier for client transmission. Meaning of the specific bits:
        - Bit 0...10: 11-bit CAN identifier.
        - Bit 11..30: reserved, set to 0.
        - Bit 31: if 1, SDO client object is not used. */
    PFdword            COB_IDClientToServer;
    /** Communication object identifier for message received from server. Meaning of the specific bits:
        - Bit 0...10: 11-bit CAN identifier.
        - Bit 11..30: reserved, set to 0.
        - Bit 31: if 1, SDO client object is not used. */
    PFdword            COB_IDServerToClient;
    /** Node-ID of the SDO server */
    PFbyte             nodeIDOfTheSDOServer;
}PFCoSdoClientPar;


/**
 * SDO client object
 */
typedef struct{
    /** From pfCoSdoClientInit() */
    PFCoSdoClientPar  *SDOClientPar;
    /** From pfCoSdoClientInit() */
    PFCoSdo           *SDO;
    /** Internal state of the SDO client */
    PFbyte             state;
    /** Pointer to data buffer supplied by user */
    PFbyte            *buffer;
    /** By download application indicates data size in buffer.
    By upload application indicates buffer size */
    PFdword            bufferSize;
    /** Offset in buffer of next data segment being read/written */
    PFdword            bufferOffset;
    /** Acknowledgement */
    PFdword            bufferOffsetACK;
    /** data length to be uploaded in block transfer */
    PFdword            dataSize;
    /** Data length transferred in block transfer */
    PFdword            dataSizeTransfered;
    /** Timeout timer for SDO communication */
    PFword            timeoutTimer;
    /** Timeout timer for SDO block transfer */
    PFword            timeoutTimerBLOCK;
    /** Index of current object in Object Dictionary */
    PFword            index;
    /** Subindex of current object in Object Dictionary */
    PFbyte             subIndex;
    /** From pfCoSdoClientInit() */
    PFCoCanModule     *CANdevRx;
    /** From pfCoSdoClientInit() */
    PFword            CANdevRxIdx;
    /** Flag indicates, if new SDO message received from CAN bus.
    It is not cleared, until received message is completely processed. */
    PFword            CANrxNew;      /* must be 2-byte variable because of correct alignment of CANrxData */
    /** 8 data bytes of the received message */
    PFbyte             CANrxData[8];  /* take care for correct (word) alignment! */
    /** Pointer to optional external function. If defined, it is called from high
    priority interrupt after new CAN SDO response message is received. Function
    may wake up external task, which processes SDO client functions */
    void              (*pFunctSignal)(PFdword arg);
    /** Optional argument, which is passed to above function */
    PFdword            functArg;
    /** From pfCoSdoClientInit() */
    PFCoCanModule     *CANdevTx;
    /** CAN transmit buffer inside CANdevTx for CAN tx message */
    PFCoCanTx         *CANtxBuff;
    /** From pfCoSdoClientInit() */
    PFword            CANdevTxIdx;
    /** Toggle bit toggled with each subsequent in segmented transfer */
    PFbyte             toggle;
    /** Server threshold for switch back to segmented transfer, if data size is small.
    Set in pfCoSdoClientInit(). Can be changed by application. 0 Disables switching. */
    PFbyte             pst;
    /** Maximum number of segments in one block. Set in pfCoSdoClientInit(). Can
    be changed by application to 2 .. 127. */
    PFbyte             block_size_max;
    /** Last sector number */
    PFbyte             block_seqno;
    /** Block size in current transfer */
    PFbyte             block_blksize;
    /** Number of bytes in last segment that do not contain data */
    PFbyte             block_noData;
    /** Server CRC support in block transfer */
    PFbyte             crcEnabled;
}PFCoSdoClient;

/**
 * Initialize SDO client object.
 *
 * Function must be called in the communication reset section.
 *
 * @param SDO_C This object will be initialized.
 * @param SDO SDO server object. It is used in case, if client is accessing
 * object dictionary from its own device. If NULL, it will be ignored.
 * @param SDOClientPar Pointer to _SDO Client Parameter_ record from Object
 * dictionary (index 0x1280+). Will be written.
 * @param CANdevRx CAN device for SDO client reception.
 * @param CANdevRxIdx Index of receive buffer in the above CAN device.
 * @param CANdevTx CAN device for SDO client transmission.
 * @param CANdevTxIdx Index of transmit buffer in the above CAN device.
 *
 * @return #PFEnCoReturnError: enCO_ERROR_NO or enCO_ERROR_ILLEGAL_ARGUMENT.
 */
PFint16 pfCoSdoClientInit(
        PFCoSdoClient         *SDO_C,
        PFCoSdo               *SDO,
        PFCoSdoClientPar      *SDOClientPar,
        PFCoCanModule         *CANdevRx,
        PFword                CANdevRxIdx,
        PFCoCanModule         *CANdevTx,
        PFword                CANdevTxIdx);


/**
 * Setup SDO client object.
 *
 * Function must be called before new SDO communication. If previous SDO
 * communication was with the same node, function does not need to be called.
 *
 * @param SDO_C This object.
 * @param COB_IDClientToServer See PFCoSdoClientPar. If zero, then
 * nodeIDOfTheSDOServer is used with default COB-ID.
 * @param COB_IDServerToClient See PFCoSdoClientPar. If zero, then
 * nodeIDOfTheSDOServer is used with default COB-ID.
 * @param nodeIDOfTheSDOServer Node-ID of the SDO server. If zero, SDO client
 * object is not used. If it is the same as node-ID of this node, then data will
 * be exchanged with this node (without CAN communication).
 *
 * @return #PFEnCoSdoClientReturn
 */
PFEnCoSdoClientReturn pfCoSdoClientSetup(
        PFCoSdoClient         *SDO_C,
        PFdword                COB_IDClientToServer,
        PFdword                COB_IDServerToClient,
        PFbyte                 nodeIDOfTheSDOServer);


/**
 * Initiate SDO download communication.
 *
 * Function initiates SDO download communication with server specified in
 * pfCoSdoClientInit() function. Data will be written to remote node.
 * Function is non-blocking.
 *
 * @param SDO_C This object.
 * @param index Index of object in object dictionary in remote node.
 * @param subIndex Subindex of object in object dictionary in remote node.
 * @param dataTx Pointer to data to be written. Data must be valid until end
 * of communication. Note that data are aligned in little-endian
 * format, because CANopen itself uses little-endian. Take care,
 * when using processors with big-endian.
 * @param dataSize Size of data in dataTx.
 * @param blockEnable Try to initiate block transfer.
 *
 * @return #PFEnCoSdoClientReturn
 */
PFEnCoSdoClientReturn pfCoSdoClientDownloadInitiate(
        PFCoSdoClient         *SDO_C,
        PFword                index,
        PFbyte                 subIndex,
        PFbyte                *dataTx,
        PFdword                dataSize,
        PFbyte                 blockEnable);

/**
 * Process SDO download communication.
 *
 * Function must be called cyclically until it returns <=0. It Proceeds SDO
 * download communication initiated with pfCoSdoClientDownloadInitiate().
 * Function is non-blocking.
 *
 * @param SDO_C This object.
 * @param timeDifference_ms Time difference from previous function call in [milliseconds].
 * @param SDOtimeoutTime Timeout time for SDO communication in milliseconds.
 * @param pSDOabortCode Pointer to external variable written by this function
 * in case of error in communication.
 *
 * @return #PFEnCoSdoClientReturn
 */
PFEnCoSdoClientReturn pfCoSdoClientDownload(
        PFCoSdoClient         *SDO_C,
        PFword                timeDifference_ms,
        PFword                SDOtimeoutTime,
        PFdword               *pSDOabortCode);

/**
 * Initiate SDO upload communication.
 *
 * Function initiates SDO upload communication with server specified in
 * pfCoSdoClientInit() function. Data will be read from remote node.
 * Function is non-blocking.
 *
 * @param SDO_C This object.
 * @param index Index of object in object dictionary in remote node.
 * @param subIndex Subindex of object in object dictionary in remote node.
 * @param dataRx Pointer to data buffer data will be written. Buffer must be
 * valid until end of communication. Note that data are aligned
 * in little-endian format, because CANopen itself uses
 * little-endian. Take care, when using processors with big-endian.
 * @param dataRxSize Size of dataRx.
 * @param blockEnable Try to initiate block transfer.
 *
 * @return #PFEnCoSdoClientReturn
 */
PFEnCoSdoClientReturn pfCoSdoClientUploadInitiate(
        PFCoSdoClient         *SDO_C,
        PFword                index,
        PFbyte                 subIndex,
        PFbyte                *dataRx,
        PFdword                dataRxSize,
        PFbyte                 blockEnable);


/**
 * Process SDO upload communication.
 *
 * Function must be called cyclically until it returns <=0. It Proceeds SDO
 * upload communication initiated with pfCoSdoClientUploadInitiate().
 * Function is non-blocking.
 *
 * @param SDO_C This object.
 * @param timeDifference_ms Time difference from previous function call in [milliseconds].
 * @param SDOtimeoutTime Timeout time for SDO communication in milliseconds.
 * @param pDataSize pointer to external variable, where size of received
 * data will be written.
 * @param pSDOabortCode Pointer to external variable written by this function
 * in case of error in communication.
 *
 * @return #PFEnCoSdoClientReturn
 */
PFEnCoSdoClientReturn pfCoSdoClientUpload(
        PFCoSdoClient         *SDO_C,
        PFword                timeDifference_ms,
        PFword                SDOtimeoutTime,
        PFdword               *pDataSize,
        PFdword               *pSDOabortCode);

/**
 * Close SDO communication temporary.
 *
 * Function must be called after finish of each SDO client communication cycle.
 * It disables reception of SDO client CAN messages. It is necessary, because
 * pfCoSdoClientReceive function may otherwise write into undefined SDO buffer.
 */
void pfCoSdoClientClose(PFCoSdoClient *SDO_C);


/** @} */
#endif
