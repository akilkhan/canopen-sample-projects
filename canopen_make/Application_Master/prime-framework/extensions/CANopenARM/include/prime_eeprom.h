//done
/**
 * Eeprom object for generic microcontroller.
 */

#ifndef PF_EEPROM
#define PF_EEPROM


/**
 * @defgroup PF_CO_eeprom Nonvolatile storage
 * @ingroup PF_CO_CANopen
 * @{
 *
 * Storage of nonvolatile CANopen variables into the eeprom.
 */

/**
 * Eeprom object.
 */
 
typedef struct{
    PFbyte      *OD_EEPROMAddress;      /**< From PFCoEe_init_1() */
    PFdword      OD_EEPROMSize;         /**< From PFCoEe_init_1() */
    PFbyte      *OD_ROMAddress;         /**< From PFCoEe_init_1() */
    PFdword      OD_ROMSize;            /**< From PFCoEe_init_1() */
    PFdword      OD_EEPROMCurrentIndex; /**< Internal variable controls the OD_EEPROM write */
    PFEnBoolean  OD_EEPROMWriteEnable;  /**< Writing to EEPROM is enabled */
}PFCoEe;

/**
 * First part of eeprom initialization. Called after microcontroller reset.
 *
 * @param ee This object will be initialized.
 * @param OD_EEPROMAddress Address of OD_EEPROM structure from object dictionary.
 * @param OD_EEPROMSize Size of OD_EEPROM structure from object dictionary.
 * @param OD_ROMAddress Address of OD_ROM structure from object dictionary.
 * @param OD_ROMSize Size of OD_ROM structure from object dictionary.
 *
 * @return #PFEnCoReturnError: enCO_ERROR_NO, enCO_ERROR_DATA_CORRUPT (Data in eeprom corrupt) or
 * enCO_ERROR_CRC (CRC from MBR does not match the CRC of OD_ROM block in eeprom).
 */
PFEnCoReturnError pfCoEeInit_1(
        PFCoEe                *ee,
        PFbyte                *OD_EEPROMAddress,
        PFdword                OD_EEPROMSize,
        PFbyte                *OD_ROMAddress,
        PFdword                OD_ROMSize);

/**
 * Second part of eeprom initialization. Called after CANopen communication reset.
 *
 * @param ee          - This object.
 * @param eeStatus    - Return value from PFCoEe_init_1().
 * @param SDO         - SDO object.
 * @param em          - Emergency object.
 */
 
void pfCoEeInit_2(
        PFCoEe                *ee, 
        PFEnCoReturnError eeStatus,
        PFCoSdo               *SDO,
        PFCoEm               *em);


/**
 * Process eeprom object.
 *
 * Function must be called cyclically. It strores variables from OD_EEPROM data
 * block into eeprom byte by byte (only if values are different).
 *
 * @param ee This object.
 */
void pfCoEeProcess(PFCoEe *ee);


/** @} */
#endif
