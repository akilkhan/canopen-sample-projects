/**
 * Main CANopen stack file.
 *
 * It combines Object dictionary (CO_OD) and all other CANopen source files.
 * Configuration information are read from CO_OD.h file. This file may be
 * customized for different CANopen configuration. (One or multiple CANopen
 * device on one or multiple CAN modules.)
 */


#ifndef PF_CANopen
#define PF_CANopen

/* TODO: protect public functions against null pointers */

/**
 * @defgroup PF_CO_CANopen CANopen stack
 * @{
 *
 * CANopenNode is an opensource CANopen Stack.
 *
 * CANopen is the internationally standardized (EN 50325-4) (CiA DS-301)
 * CAN-based higher-layer protocol for embedded control system. For more
 * information on CANopen see http://www.can-cia.org/
 *
 * Stack is written in ANSI C in object-oriented way. License is LGPL.
 * For more information see http://canopennode.sourceforge.net/
 */
 
    #include "prime_CO_driver.h"
    #include "prime_CO_OD_cfg.h"
    #include "prime_CO_OD.h"
    #include "prime_CO_SDO.h"
    #include "prime_CO_Emergency.h"
    #include "prime_CO_NMT_Heartbeat.h"
    #include "prime_CO_SYNC.h"
    #include "prime_CO_PDO.h"
    #include "prime_CO_HBconsumer.h"
	
#if CO_NO_SDO_CLIENT == 1
    #include "prime_CO_SDOmaster.h"
#endif

/**
 * Default CANopen identifiers.
 *
 * Default CANopen identifiers for CANopen communication objects. Same as
 * 11-bit addresses of CAN messages. These are default identifiers and
 * can be changed in CANopen. Especially PDO identifiers are configured
 * in PDO linking phase of the CANopen network configuration.
 */
 
typedef enum{
     enCO_CAN_ID_NMT_SERVICE       = 0x000,   /**< 0x000, Network management */
     enCO_CAN_ID_SYNC              = 0x080,   /**< 0x080, Synchronous message */
     enCO_CAN_ID_EMERGENCY         = 0x080,   /**< 0x080, Emergency messages (+nodeID) */
     enCO_CAN_ID_TIME_STAMP        = 0x100,   /**< 0x100, Time stamp message */
     enCO_CAN_ID_TPDO_1            = 0x180,   /**< 0x180, Default TPDO1 (+nodeID) */
     enCO_CAN_ID_RPDO_1            = 0x200,   /**< 0x200, Default RPDO1 (+nodeID) */
     enCO_CAN_ID_TPDO_2            = 0x280,   /**< 0x280, Default TPDO2 (+nodeID) */
     enCO_CAN_ID_RPDO_2            = 0x300,   /**< 0x300, Default RPDO2 (+nodeID) */
     enCO_CAN_ID_TPDO_3            = 0x380,   /**< 0x380, Default TPDO3 (+nodeID) */
     enCO_CAN_ID_RPDO_3            = 0x400,   /**< 0x400, Default RPDO3 (+nodeID) */
     enCO_CAN_ID_TPDO_4            = 0x480,   /**< 0x480, Default TPDO4 (+nodeID) */
     enCO_CAN_ID_RPDO_4            = 0x500,   /**< 0x500, Default RPDO5 (+nodeID) */
     enCO_CAN_ID_TSDO              = 0x580,   /**< 0x580, SDO response from server (+nodeID) */
     enCO_CAN_ID_RSDO              = 0x600,   /**< 0x600, SDO request from client (+nodeID) */
     enCO_CAN_ID_HEARTBEAT         = 0x700    /**< 0x700, Heartbeat message */
}PFCoDefaultCanId;

/**
 * Number of CAN modules in use.
 *
 * If constant is set globaly to 2, second CAN module is initialized and fifth
 * and sixth RPDO (if exist) are configured to it.
 */
#ifndef CO_NO_CAN_MODULES
    #define CO_NO_CAN_MODULES 	1
#endif

/**
 * CANopen stack object combines pointers to all CANopen objects.
 */
typedef struct{
    PFCoCanModule     *CANmodule[CO_NO_CAN_MODULES];/**< CAN module objects */
    PFCoSdo           *SDO;            /**< SDO object */
    PFCoEm            *em;             /**< Emergency report object */
    PFCoEmpr          *emPr;           /**< Emergency process object */
    PFCoNmt           *NMT;            /**< NMT object */
#if CO_NO_SYNC == 1
    PFCoSync          *SYNC;           /**< SYNC object */
#endif
    PFCoRpdo          *RPDO[CO_NO_RPDO];/**< RPDO objects */
    PFCoTpdo          *TPDO[CO_NO_TPDO];/**< TPDO objects */
#if CO_NO_HB_CONS > 0
    PFCoHbConsumer    *HBcons;         /**<  Heartbeat consumer object*/
#endif
#if CO_NO_SDO_CLIENT == 1
    PFCoSdoClient     *SDOclient;      /**< SDO client object */
#endif
}PFCo;

/* total number of received CAN messages */
#define CO_RXCAN_NO_MSGS (1+CO_NO_SYNC+CO_NO_RPDO+CO_NO_SDO_SERVER+CO_NO_SDO_CLIENT+CO_NO_HB_CONS)
/* total number of transmitted CAN messages */
#define CO_TXCAN_NO_MSGS (CO_NO_NMT_MASTER+CO_NO_SYNC+CO_NO_EMERGENCY+CO_NO_TPDO+CO_NO_SDO_SERVER+CO_NO_SDO_CLIENT+1)

#if CO_NO_SDO_CLIENT > 0
#define CO_MEM_POOL_SIZE    (CO_NO_CAN_MODULES*sizeof(PFCoCanModule) + \
        sizeof(PFCoSdo) + sizeof(PFCoODExtension) * CO_OD_NoOfElements + \
        sizeof(PFCoEm) + sizeof(PFCoEmpr) + sizeof(PFCoNmt) + \
        CO_NO_SYNC*sizeof(PFCoSync) + CO_NO_RPDO*sizeof(PFCoRpdo) + \
        CO_NO_TPDO*sizeof(PFCoTpdo) + sizeof(PFCoHBconsumer) + \
        sizeof(PFCoHBConsNode)*CO_NO_HB_CONS + CO_NO_SDO_CLIENT*sizeof(PFCoSdoClient) + \
        sizeof(PFCoCanRx)*CO_RXCAN_NO_MSGS + sizeof(PFCoCanTx)*PFCoXCAN_NO_MSGS + \
        CO_NO_CAN_MODULES*(sizeof(PFCoCanRx)*2 + sizeof(PFCoCanTx)*2))
#else
#define CO_MEM_POOL_SIZE    (CO_NO_CAN_MODULES*sizeof(PFCoCanModule) + \
        sizeof(PFCoSdo) + sizeof(PFCoODExtension) * CO_OD_NoOfElements + \
        sizeof(PFCoEm) + sizeof(PFCoEmpr) + sizeof(PFCoNmt) + \
        CO_NO_SYNC*sizeof(PFCoSync) + CO_NO_RPDO*sizeof(PFCoRpdo) + \
        CO_NO_TPDO*sizeof(PFCoTpdo) + sizeof(PFCoHBconsumer) + \
        sizeof(PFCoHBConsNode)*CO_NO_HB_CONS + \
        sizeof(PFCoCanRx)*CO_RXCAN_NO_MSGS + sizeof(PFCoCanTx)*PFCoXCAN_NO_MSGS + \
        CO_NO_CAN_MODULES*(sizeof(PFCoCanRx)*2 + sizeof(PFCoCanTx)*2))
#endif

/** CANopen object */
    extern PFCo *CO;


/**
 * Function pfCoSendNmtCommand() is simple function, which sends CANopen message.
 * This part of code is an example of custom definition of simple CANopen
 * object. Follow the code in CANopen.c file. If macro CO_NO_NMT_MASTER is 1,
 * function pfCoSendNmtCommand can be used to send NMT master message.
 *
 * @param CO CANopen object.
 * @param command NMT command.
 * @param nodeID Node ID.
 *
 * @return 0: Operation completed successfully.
 * @return other: same as pfCoCanSend().
 */
#if CO_NO_NMT_MASTER == 1
    PFbyte pfCoSendNmtCommand(PFCo *CO, PFbyte command, PFbyte nodeID);
#endif


/**
 * Initialize CANopen stack.
 *
 * Function must be called in the communication reset section.
 *
 * @return #PFEnCoReturnError: enCO_ERROR_NO, enCO_ERROR_ILLEGAL_ARGUMENT,
 * enCO_ERROR_OUT_OF_MEMORY, enCO_ERROR_ILLEGAL_BAUDRATE
 */
PFEnCoReturnError pfCoInit(
        /*PFbyte                 *memPool,*/
        PFCoCanModuleHwConfig  *CANhwCfg0
#if CO_NO_CAN_MODULES >= 2
       ,PFCoCanModuleHwConfig  *CANhwCfg1
#endif
);

/**
 * Delete CANopen object and free memory. Must be called at program exit.
 */
void pfCoDelete(void);


/**
 * Process CANopen objects.
 *
 * Function must be called cyclically. It processes all "asynchronous" CANopen
 * objects. Function returns value from pfCoNmtProcess().
 *
 * @param CO This object
 * @param timeDifference_ms Time difference from previous function call in [milliseconds].
 *
 * @return #PFEnCoNmtResetCmd
 */
PFEnCoNmtResetCmd pfCoProcess(
        PFCo                   *CO,
        PFword                timeDifference_ms);


/**
 * Process CANopen SYNC and RPDO objects.
 *
 * Function must be called cyclically from synchronous 1ms task. It processes
 * SYNC and receive PDO CANopen objects.
 *
 * @param CO This object
 */
void pfCoProcessRPDO(PFCo *CO);


/**
 * Process CANopen TPDO objects.
 *
 * Function must be called cyclically from synchronous 1ms task. It processes
 * transmit PDO CANopen objects.
 *
 * @param CO This object
 */
void pfCoProcessTPDO(PFCo *CO);


/** @} */
#endif
