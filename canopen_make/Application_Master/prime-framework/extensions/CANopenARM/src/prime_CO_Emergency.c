/*
 * CANopen Emergency object.
 */

#include "prime_CO_driver.h"
#include "prime_CO_SDO.h"
#include "prime_CO_Emergency.h"
#include "prime_CO_OD.h"

/*
 * Function for accessing _Pre-Defined Error Field_ (index 0x1003) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
 
static PFEnCoSdoAbortCode pfCoODF_1003(PFCoODFarg *ODF_arg);
static PFEnCoSdoAbortCode pfCoODF_1003(PFCoODFarg *ODF_arg)
{
    PFCoEmpr *emPr;
    PFbyte value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    emPr = (PFCoEmpr*) ODF_arg->object;
    value = ODF_arg->data[0];

    if(ODF_arg->reading)
	{
        PFbyte noOfErrors;
        noOfErrors = emPr->preDefErrNoOfErrors;

        if(ODF_arg->subIndex == 0U)
		{
            ODF_arg->data[0] = noOfErrors;
        }
        else if(ODF_arg->subIndex > noOfErrors)
		{
            ret = enCO_SDO_AB_NO_DATA;
        }
        else
		{
            ret = enCO_SDO_AB_NONE;
        }
    }
    else
	{
        /* only '0' may be written to subIndex 0 */
        if(ODF_arg->subIndex == 0U)
		{
            if(value == 0U)
			{
                emPr->preDefErrNoOfErrors = 0U;
            }
            else
			{
                ret = enCO_SDO_AB_INVALID_VALUE;
            }
        }
        else
		{
            ret = enCO_SDO_AB_READONLY;
        }
    }

    return ret;
}


/*
 * Function for accessing _COB ID EMCY_ (index 0x1014) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODF_1004(PFCoODFarg *ODF_arg);
static PFEnCoSdoAbortCode pfCoODF_1004(PFCoODFarg *ODF_arg)
{
    PFbyte *nodeId;
    PFdword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    nodeId = (PFbyte*) ODF_arg->object;
    value = pfCoGetUint32(ODF_arg->data);

    /* add nodeId to the value */
    if(ODF_arg->reading)
	{
        pfCoSetUint32(ODF_arg->data, value + *nodeId);
    }

    return ret;
}

/******************************************************************************/
PFEnCoReturnError pfCoEmInit(
        PFCoEm                *em,
        PFCoEmpr              *emPr,
        PFCoSdo               *SDO,
        PFbyte                *errorStatusBits,
        PFbyte                 errorStatusBitsSize,
        PFbyte                *errorRegister,
        PFdword               *preDefErr,
        PFbyte                 preDefErrSize,
        PFCoCanModule         *CANdev,
        PFword                CANdevTxIdx,
        PFword                CANidTxEM)
{
    PFbyte i;
    PFEnCoReturnError ret = enCO_ERROR_NO;

    /* verify parameters */
    if(errorStatusBitsSize < 6U)
	{
        ret = enCO_ERROR_ILLEGAL_ARGUMENT;
    }

    /* Configure object variables */
    em->errorStatusBits         = errorStatusBits;
    em->errorStatusBitsSize     = errorStatusBitsSize;
    em->bufEnd                  = em->buf + (CO_EM_INTERNAL_BUFFER_SIZE * 8);
    em->bufWritePtr             = em->buf;
    em->bufReadPtr              = em->buf;
    em->bufFull                 = 0U;
    em->wrongErrorReport        = 0U;
    emPr->em                    = em;
    emPr->errorRegister         = errorRegister;
    emPr->preDefErr             = preDefErr;
    emPr->preDefErrSize         = preDefErrSize;
    emPr->preDefErrNoOfErrors   = 0U;
    emPr->inhibitEmTimer        = 0U;

    /* clear error PFEnStatus bits */
    for(i=0U; i<errorStatusBitsSize; i++)
	{
        em->errorStatusBits[i] = 0U;
    }

    /* Configure Object dictionary entry at index 0x1003 and 0x1014 */
    pfCoODConfigure(SDO, OD_H1003_PREDEF_ERR_FIELD, pfCoODF_1003, (void*)emPr, 0, 0U);
    pfCoODConfigure(SDO, OD_H1014_COBID_EMERGENCY,  pfCoODF_1004, (void*)&SDO->nodeId, 0, 0U);

    /* configure emergency message CAN transmission */
    emPr->CANdev = CANdev;
    emPr->CANdev->em = (void*)em; /* update pointer inside CAN device. */
    emPr->CANtxBuff = pfCoCanTxBufferInit(
            CANdev,             /* CAN device */
            CANdevTxIdx,        /* index of specific buffer inside CAN module */
            CANidTxEM,          /* CAN identifier */
            0,                  /* rtr */
            8U,                  /* number of data bytes */
            0);                 /* synchronous message flag bit */

    return ret;
}

/******************************************************************************/
void pfCoEmProcess(
        PFCoEmpr              *emPr,
        PFEnBoolean               NMTisPreOrOperational,
        PFword                timeDifference_100us,
        PFword                emInhTime)
{

    PFCoEm *em = emPr->em;
    PFbyte errorRegister;

    /* verify errors from driver and other */
    pfCoCanVerifyErrors(emPr->CANdev);
    if(em->wrongErrorReport != 0U)
	{
        pfCoErrorReport(em, CO_EM_WRONG_ERROR_REPORT, CO_EMC_SOFTWARE_INTERNAL, (PFdword)em->wrongErrorReport);
        em->wrongErrorReport = 0U;
    }


    /* calculate Error register */
    errorRegister = 0U;
    /* generic error */
    if(em->errorStatusBits[5])
	{
        errorRegister |= enCO_ERR_REG_GENERIC_ERR;
    }
    /* communication error (overrun, error state) */
    if(em->errorStatusBits[2] || em->errorStatusBits[3]){
        errorRegister |= enCO_ERR_REG_COMM_ERR;
    }
    *emPr->errorRegister = (*emPr->errorRegister & 0xEEU) | errorRegister;

    /* inhibit time */
    if(emPr->inhibitEmTimer < emInhTime)
	{
        emPr->inhibitEmTimer += timeDifference_100us;
    }

    /* send Emergency message. */
    if(     NMTisPreOrOperational &&
            !emPr->CANtxBuff->bufferFull &&
            emPr->inhibitEmTimer >= emInhTime &&
            (em->bufReadPtr != em->bufWritePtr || em->bufFull))
    {
        PFdword preDEF;    /* preDefinedErrorField */

        /* add error register */
        em->bufReadPtr[2] = *emPr->errorRegister;

        /* copy data to CAN emergency message */
        pfCoMemcpy(emPr->CANtxBuff->data, em->bufReadPtr, 8U);
        pfCoMemcpy((PFbyte*)&preDEF, em->bufReadPtr, 4U);
        em->bufReadPtr += 8;

        /* Update read buffer pointer and reset inhibit timer */
        if(em->bufReadPtr == em->bufEnd)
		{
            em->bufReadPtr = em->buf;
        }
        emPr->inhibitEmTimer = 0U;

        /* verify message buffer overflow, then clear full flag */
        if(em->bufFull == 2U)
		{
            em->bufFull = 0U;    /* will be updated below */
            pfCoErrorReport(em, CO_EM_EMERGENCY_BUFFER_FULL, CO_EMC_GENERIC, 0U);
        }
        else
		{
            em->bufFull = 0;
        }

        /* write to 'pre-defined error field' (object dictionary, index 0x1003) */
        if(emPr->preDefErr)
		{
            PFbyte i;

            if(emPr->preDefErrNoOfErrors < emPr->preDefErrSize)
                emPr->preDefErrNoOfErrors++;
            for(i=emPr->preDefErrNoOfErrors-1; i>0; i--)
                emPr->preDefErr[i] = emPr->preDefErr[i-1];
            emPr->preDefErr[0] = preDEF;
        }

        /* send CAN message */
        pfCoCanSend(emPr->CANdev, emPr->CANtxBuff);
    }

    return;
}

/******************************************************************************/
void pfCoErrorReport(PFCoEm *em, const PFbyte errorBit, const PFword errorCode, const PFdword infoCode)
{
    PFbyte index = errorBit >> 3;
    PFbyte bitmask = 1 << (errorBit & 0x7);
    PFbyte *errorStatusBits = 0;
    PFEnBoolean sendEmergency = enBooleanTrue;

    if(em == NULL)
	{
        sendEmergency = enBooleanFalse;
    }
    else if(index >= em->errorStatusBitsSize)
	{
        /* if errorBit value not supported, send emergency 'CO_EM_WRONG_ERROR_REPORT' */
        em->wrongErrorReport = errorBit;
        sendEmergency = enBooleanFalse;
    }
    else
	{
        errorStatusBits = &em->errorStatusBits[index];
        /* if error was allready reported, do nothing */
        if((*errorStatusBits & bitmask) != 0)
		{
            sendEmergency = enBooleanFalse;
        }
    }

    if(sendEmergency)
	{
        /* set error bit */
        if(errorBit)
		{
            /* any error except NO_ERROR */
            *errorStatusBits |= bitmask;
        }

        /* verify buffer full, set overflow */
        if(em->bufFull)
		{
            em->bufFull = 2;
        }
        else
		{
            PFbyte bufCopy[8];

            /* prepare data for emergency message */
            pfCoMemcpySwap2(&bufCopy[0], (PFbyte*)&errorCode);
            bufCopy[2] = 0; /* error register will be set later */
            bufCopy[3] = errorBit;
            pfCoMemcpySwap4(&bufCopy[4], (PFbyte*)&infoCode);

            /* copy data to the buffer, increment writePtr and verify buffer full */
            PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
            pfCoMemcpy(em->bufWritePtr, &bufCopy[0], 8);
            em->bufWritePtr += 8;

            if(em->bufWritePtr == em->bufEnd) em->bufWritePtr = em->buf;
            if(em->bufWritePtr == em->bufReadPtr) em->bufFull = 1;
            pfCO_hal_InterruptsRestore(flags);
        }
    }
}

/******************************************************************************/
void pfCoErrorReset(PFCoEm *em, const PFbyte errorBit, const PFdword infoCode)
{
    PFbyte index = errorBit >> 3;
    PFbyte bitmask = 1 << (errorBit & 0x7);
    PFbyte *errorStatusBits = 0;
    PFEnBoolean sendEmergency = enBooleanTrue;

    if(em == NULL)
	{
        sendEmergency = enBooleanFalse;
    }
    else if(index >= em->errorStatusBitsSize)
	{
        /* if errorBit value not supported, send emergency 'CO_EM_WRONG_ERROR_REPORT' */
        em->wrongErrorReport = errorBit;
        sendEmergency = enBooleanFalse;
    }
    else
	{
        errorStatusBits = &em->errorStatusBits[index];
        /* if error was allready cleared, do nothing */
        if((*errorStatusBits & bitmask) == 0)
		{
            sendEmergency = enBooleanFalse;
        }
    }

    if(sendEmergency)
	{
        /* erase error bit */
        *errorStatusBits &= ~bitmask;

        /* verify buffer full */
        if(em->bufFull)
		{
            em->bufFull = 2;
        }
        else
		{
            PFbyte bufCopy[8];

            /* prepare data for emergency message */
            bufCopy[0] = 0;
            bufCopy[1] = 0;
            bufCopy[2] = 0; /* error register will be set later */
            bufCopy[3] = errorBit;
            pfCoMemcpySwap4(&bufCopy[4], (PFbyte*)&infoCode);

            /* copy data to the buffer, increment writePtr and verify buffer full */
            PFCoCpuInterruptsFlags flags = pfCO_hal_InterruptsSaveDisable();
            pfCoMemcpy(em->bufWritePtr, &bufCopy[0], 8);
            em->bufWritePtr += 8;

            if(em->bufWritePtr == em->bufEnd) em->bufWritePtr = em->buf;
            if(em->bufWritePtr == em->bufReadPtr) em->bufFull = 1;
            pfCO_hal_InterruptsRestore(flags);
        }
    }
}

/******************************************************************************/
PFEnBoolean pfCoIsError(PFCoEm *em, const PFbyte errorBit)
{
    PFbyte index = errorBit >> 3;
    PFbyte bitmask = 1 << (errorBit & 0x7);
    PFEnBoolean ret = enBooleanFalse;

    if(index < em->errorStatusBitsSize)
	{
        if((em->errorStatusBits[index] & bitmask) != 0)
		{
            ret = enBooleanTrue;
        }
    }

    return ret;
}
