/*
 * Main CANopen stack file. It combines Object dictionary (CO_OD) and all other
 * CANopen source files. Configuration information are read from CO_OD.h file.
 */

#include "prime_framework.h"
#include "CANopen.h"


/* If defined, global variables will be used, otherwise CANopen objects will
   be generated with malloc(). */
#define PF_CO_USE_GLOBALS

#ifndef PF_CO_USE_GLOBALS
    #define PF_PF_CO_USE_MALLOC
#ifdef PF_PF_CO_USE_MALLOC
    #include <stdlib.h> /*  for malloc, free */
    static PFdword CO_memoryUsed = 0; /* informative */
#endif
#endif


/* Global variables ***********************************************************/
    extern const PFCoODentry CO_OD[CO_OD_NoOfElements];  /* Object Dictionary array */
    static PFCo COO;
    PFCo *CO = NULL;

#if defined(__dsPIC33F__) || defined(__PIC24H__) \
    || defined(__dsPIC33E__) || defined(__PIC24E__)
    /* CAN message buffer for one TX and seven RX messages. */
    #define CO_CANmsgBuffSize   8
#ifdef __HAS_EDS__
    __eds__ PFCoCanRxMsg CO_CANmsg[CO_CANmsgBuffSize] __attribute__((eds,space(dma)));
#else
    PFCoCanRxMsg CO_CANmsg[CO_CANmsgBuffSize] __attribute__((space(dma)));
#endif
#endif

    static PFCoCanRx          *CO_CANmodule_rxArray0;
    static PFCoCanTx          *CO_CANmodule_txArray0;
  #if CO_NO_CAN_MODULES >= 2
    static PFCoCanRx          *CO_CANmodule_rxArray1;
    static PFCoCanTx          *CO_CANmodule_txArray1;
  #endif
    static PFCoODExtension   *CO_SDO_ODExtensions;
  #if CO_NO_HB_CONS > 0
    static PFCoHbConsNode     *CO_HBcons_monitoredNodes;
  #endif

/* Verify features from CO_OD *************************************************/
    /* generate error, if features are not corectly configured for this project */
    #if        CO_NO_NMT_MASTER                           >  1     \
            || CO_NO_SYNC                                 >  1     \
            || CO_NO_EMERGENCY                            != 1     \
            || CO_NO_SDO_SERVER                           != 1     \
            || (CO_NO_SDO_CLIENT != 0 && CO_NO_SDO_CLIENT != 1)    \
            || (CO_NO_RPDO < 1 || CO_NO_RPDO > 0x200)              \
            || (CO_NO_TPDO < 1 || CO_NO_TPDO > 0x200)              \
            /*|| ODL_consumerHeartbeatTime_arrayLength      == 0*/     \
            || ODL_errorStatusBits_stringLength           < 10
        #error Features from CO_OD.h file are not corectly configured for this project!
    #endif


/* Indexes for CANopenNode message objects ************************************/
    #define CO_RXCAN_NMT       0                                      /*  index for NMT message */
    #define CO_RXCAN_SYNC      1                                      /*  index for SYNC message */
    #define CO_RXCAN_RPDO     (CO_RXCAN_SYNC+CO_NO_SYNC)              /*  start index for RPDO messages */
    #define CO_RXCAN_SDO_SRV  (CO_RXCAN_RPDO+CO_NO_RPDO)              /*  start index for SDO server message (request) */
    #define CO_RXCAN_SDO_CLI  (CO_RXCAN_SDO_SRV+CO_NO_SDO_SERVER)     /*  start index for SDO client message (response) */
    #define CO_RXCAN_CONS_HB  (CO_RXCAN_SDO_CLI+CO_NO_SDO_CLIENT)     /*  start index for Heartbeat Consumer messages */
    /* total number of received CAN messages */
//    #define CO_RXCAN_NO_MSGS (1+CO_NO_SYNC+CO_NO_RPDO+CO_NO_SDO_SERVER+CO_NO_SDO_CLIENT+CO_NO_HB_CONS)

    #define CO_TXCAN_NMT       0                                      /*  index for NMT master message */
    #define CO_TXCAN_SYNC      CO_TXCAN_NMT+CO_NO_NMT_MASTER          /*  index for SYNC message */
    #define CO_TXCAN_EMERG    (CO_TXCAN_SYNC+CO_NO_SYNC)              /*  index for Emergency message */
    #define CO_TXCAN_TPDO     (CO_TXCAN_EMERG+CO_NO_EMERGENCY)        /*  start index for TPDO messages */
    #define CO_TXCAN_SDO_SRV  (CO_TXCAN_TPDO+CO_NO_TPDO)              /*  start index for SDO server message (response) */
    #define CO_TXCAN_SDO_CLI  (CO_TXCAN_SDO_SRV+CO_NO_SDO_SERVER)     /*  start index for SDO client message (request) */
    #define CO_TXCAN_HB       (CO_TXCAN_SDO_CLI+CO_NO_SDO_CLIENT)     /*  index for Heartbeat message */
    /* total number of transmitted CAN messages */
//    #define CO_TXCAN_NO_MSGS (CO_NO_NMT_MASTER+CO_NO_SYNC+CO_NO_EMERGENCY+CO_NO_TPDO+CO_NO_SDO_SERVER+CO_NO_SDO_CLIENT+1)

#ifdef PF_CO_USE_GLOBALS
    static PFCoCanModule       COO_CANmodule[CO_NO_CAN_MODULES];
    static PFCoCanRx           COO_CANmodule_rxArray0[CO_RXCAN_NO_MSGS];
    static PFCoCanTx           COO_CANmodule_txArray0[CO_TXCAN_NO_MSGS];
  #if CO_NO_CAN_MODULES >= 2
    static PFCoCanRx           COO_CANmodule_rxArray1[2];
    static PFCoCanTx           COO_CANmodule_txArray1[2];
  #endif
    static PFCoSdo             COO_SDO;
    static PFCoODExtension     COO_SDO_ODExtensions[CO_OD_NoOfElements];
    static PFCoEm              COO_EM;
    static PFCoEmpr            COO_EMpr;
    static PFCoNmt             COO_NMT;
#if CO_NO_SYNC > 0
    static PFCoSync            COO_SYNC;
#endif
    static PFCoRpdo            COO_RPDO[CO_NO_RPDO];
    static PFCoTpdo            COO_TPDO[CO_NO_TPDO];
#if CO_NO_HB_CONS > 0
    static PFCoHbConsumer      COO_HBcons;
    static PFCoHbConsNode      COO_HBcons_monitoredNodes[CO_NO_HB_CONS];
#endif
#if CO_NO_SDO_CLIENT == 1
    static PFCoSdoClient       COO_SDOclient;
#endif
#endif


/******************************************************************************/
#if CO_NO_NMT_MASTER == 1
    PFCoCanTx *NMTM_txBuff = 0;
    /* Helper function for using: */
    PFbyte pfCoSendNmtCommand(PFCo *CO, PFbyte command, PFbyte nodeID)
	{
        if(NMTM_txBuff == 0)
		{
            /* error, pfCoCanTxBufferInit() was not called for this buffer. */
            return enCO_ERROR_TX_UNCONFIGURED; /* -11 */
        }
        NMTM_txBuff->data[0] = command;
        NMTM_txBuff->data[1] = nodeID;
        return pfCoCanSend(CO->CANmodule[0], NMTM_txBuff); /* 0 = success */
    }
#endif

/* CAN node ID - Object dictionary function ***********************************/
static PFEnCoSdoAbortCode PFCoODFnodeId(PFCoODFarg *ODF_arg)
{
    PFbyte value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    value = ODF_arg->data[0];

    if(!ODF_arg->reading)
	{
        if(value < 1U)
		{
            ret = enCO_SDO_AB_VALUE_LOW;
        }
        else if(value > 127U)
		{
            ret = enCO_SDO_AB_VALUE_HIGH;
        }
        else
		{
            ret = enCO_SDO_AB_NONE;
        }
    }
    return ret;
}

/* CAN bit rate - Object dictionary function **********************************/
static PFEnCoSdoAbortCode PFCoODFbitRate(PFCoODFarg *ODF_arg)
{
    PFword value;
    PFEnCoSdoAbortCode ret = enCO_SDO_AB_NONE;

    value = pfCoGetUint16(ODF_arg->data);

    if(!ODF_arg->reading)
	{
        switch(value)
		{
            case 10U:
            case 20U:
            case 50U:
            case 125U:
            case 250U:
            case 500U:
            case 800U:
            case 1000U:
                break;
            default:
                ret = enCO_SDO_AB_INVALID_VALUE;
        }
    }
    return ret;
}

/******************************************************************************/
PFEnCoReturnError pfCoInit(
        /*PFbyte                 *memPool,*/
        PFCoCanModuleHwConfig  *CANhwCfg0
#if CO_NO_CAN_MODULES >= 2
        ,PFCoCanModuleHwConfig  *CANhwCfg1
#endif
        )
{
    PFint16 i;
    PFbyte nodeId;
    PFword CANBitRate;
    PFEnCoReturnError err;
#ifndef PF_CO_USE_GLOBALS
    PFword errCnt;
#endif

    /* Verify parameters from CO_OD */
    if(   sizeof(OD_TPDOCommunicationParameter_t) != sizeof(PFCoTpdoCommPar)
       || sizeof(OD_TPDOMappingParameter_t) != sizeof(PFCoTpdoMapPar)
       || sizeof(OD_RPDOCommunicationParameter_t) != sizeof(PFCoRpdoCommPar)
       || sizeof(OD_RPDOMappingParameter_t) != sizeof(PFCoRpdoMapPar))
    {
        return enCO_ERROR_PARAMETERS;
    }

#if CO_NO_SDO_CLIENT == 1
    if(sizeof(OD_SDOClientParameter_t) != sizeof(PFCoSdoClientPar))
	{
        return enCO_ERROR_PARAMETERS;
    }
#endif

    /* Initialize CANopen object */
#ifdef PF_CO_USE_GLOBALS
    CO = &COO;

    CO->CANmodule[0]                    = &COO_CANmodule[0];
    CO_CANmodule_rxArray0               = &COO_CANmodule_rxArray0[0];
    CO_CANmodule_txArray0               = &COO_CANmodule_txArray0[0];
  #if CO_NO_CAN_MODULES >= 2
    CO->CANmodule[1]                    = &COO_CANmodule[1];
    CO_CANmodule_rxArray1               = &COO_CANmodule_rxArray1[0];
    CO_CANmodule_txArray1               = &COO_CANmodule_txArray1[0];
  #endif
    CO->SDO                             = &COO_SDO;
    CO_SDO_ODExtensions                 = &COO_SDO_ODExtensions[0];
    CO->em                              = &COO_EM;
    CO->emPr                            = &COO_EMpr;
    CO->NMT                             = &COO_NMT;
  #if CO_NO_SYNC > 0
    CO->SYNC                            = &COO_SYNC;
  #endif
    for(i=0; i<CO_NO_RPDO; i++)
        CO->RPDO[i]                     = &COO_RPDO[i];
    for(i=0; i<CO_NO_TPDO; i++)
        CO->TPDO[i]                     = &COO_TPDO[i];
  #if CO_NO_HB_CONS > 0
    CO->HBcons                          = &COO_HBcons;
    CO_HBcons_monitoredNodes            = &COO_HBcons_monitoredNodes[0];
  #endif

  #if CO_NO_SDO_CLIENT == 1
    CO->SDOclient                       = &COO_SDOclient;
  #endif

#else
    if(CO == NULL)
	{    /* Use malloc only once */
        CO = &COO;
  #ifdef PF_CO_USE_MALLOC
        CO->CANmodule[0]                    = (PFCoCanModule *)    malloc(sizeof(PFCoCanModule));
        CO_CANmodule_rxArray0               = (PFCoCanRx *)        malloc(sizeof(PFCoCanRx) * CO_RXCAN_NO_MSGS);
        CO_CANmodule_txArray0               = (PFCoCanTx *)        malloc(sizeof(PFCoCanTx) * CO_TXCAN_NO_MSGS);
  #if CO_NO_CAN_MODULES >= 2
        CO->CANmodule[1]                    = (PFCoCanModule *)    malloc(sizeof(PFCoCanModule));
        CO_CANmodule_rxArray1               = (PFCoCanRx *)        malloc(sizeof(PFCoCanRx) * 2);
        CO_CANmodule_txArray1               = (PFCoCanTx *)        malloc(sizeof(PFCoCanTx) * 2);
  #endif
        CO->SDO                             = (PFCoSdo *)          malloc(sizeof(PFCoSdo));
        CO_SDO_ODExtensions                 = (PFCoODExtension*)  malloc(sizeof(PFCoODExtension) * CO_OD_NoOfElements);
        CO->em                              = (PFCoEm *)           malloc(sizeof(PFCoEm));
        CO->emPr                            = (PFCoEmpr *)         malloc(sizeof(PFCoEmpr));
        CO->NMT                             = (PFCoNmt *)          malloc(sizeof(PFCoNmt));
  #if CO_NO_SYNC > 0
        CO->SYNC                            = (PFCoSync *)         malloc(sizeof(PFCoSync));
  #endif
        for(i=0; i<CO_NO_RPDO; i++){
            CO->RPDO[i]                     = (PFCoRpdo *)         malloc(sizeof(PFCoRpdo));
        }
        for(i=0; i<CO_NO_TPDO; i++){
            CO->TPDO[i]                     = (PFCoTpdo *)         malloc(sizeof(PFCoTpdo));
        }
  #if CO_NO_HB_CONS > 0
        CO->HBcons                          = (PFCoHBConsumer *)   malloc(sizeof(PFCoHBConsumer));
        CO_HBcons_monitoredNodes            = (PFCoHBConsNode *)   malloc(sizeof(PFCoHBConsNode) * CO_NO_HB_CONS);
  #endif
  #if CO_NO_SDO_CLIENT == 1
        CO->SDOclient                       = (PFCoSdoClient *)    malloc(sizeof(PFCoSdoClient));
  #endif
        CO_memoryUsed = sizeof(PFCoCanModule)
                      + sizeof(PFCoCanRx) * CO_RXCAN_NO_MSGS
                      + sizeof(PFCoCanTx) * CO_TXCAN_NO_MSGS
      #if CO_NO_CAN_MODULES >= 2
                      + sizeof(PFCoCanModule)
                      + sizeof(PFCoCanRx) * 2
                      + sizeof(PFCoCanTx) * 2
      #endif
                      + sizeof(PFCoSdo)
                      + sizeof(PFCoODExtension) * CO_OD_NoOfElements
                      + sizeof(PFCoEm)
                      + sizeof(PFCoEmpr)
                      + sizeof(PFCoNmt)
    #if CO_NO_SYNC > 0
                      + sizeof(PFCoSync)
    #endif
                      + sizeof(PFCoRpdo) * CO_NO_RPDO
                      + sizeof(PFCoTpdo) * CO_NO_TPDO
                      + sizeof(PFCoHBConsumer)
                      + sizeof(PFCoHBConsNode) * CO_NO_HB_CONS
    #if CO_NO_SDO_CLIENT == 1
                      + sizeof(PFCoSdoClient)
    #endif
                      + 0;

#else

        CO->CANmodule[0]                    = (PFCoCanModule *)    memPool;
        memPool += sizeof(PFCoCanModule);
        CO_CANmodule_rxArray0               = (PFCoCanRx *)        memPool;
        memPool += sizeof(PFCoCanRx) * CO_RXCAN_NO_MSGS;
        CO_CANmodule_txArray0               = (PFCoCanTx *)        memPool;
        memPool += sizeof(PFCoCanTx) * CO_TXCAN_NO_MSGS;
#if CO_NO_CAN_MODULES >= 2
        CO->CANmodule[1]                    = (PFCoCanModule *)    memPool;
        memPool += sizeof(PFCoCanModule);
        CO_CANmodule_rxArray1               = (PFCoCanRx *)        memPool;
        memPool += sizeof(PFCoCanRx) * 2;
        PFCoCanModulexArray1               = (PFCoCanTx *)        memPool;
        memPool += sizeof(PFCoCanTx) * 2;
#endif
        CO->SDO                             = (PFCoSdo *)          memPool;
        memPool += sizeof(PFCoSdo);
        CO_SDO_ODExtensions                 = (PFCoODExtension*)  memPool;
        memPool += sizeof(PFCoODExtension) * CO_OD_NoOfElements;
        CO->em                              = (PFCoEm *)           memPool;
        memPool += sizeof(PFCoEm);
        CO->emPr                            = (PFCoEmpr *)         memPool;
        memPool += sizeof(PFCoEmpr);
        CO->NMT                             = (PFCoNmt *)          memPool;
        memPool += sizeof(PFCoNmt);
#if CO_NO_SYNC > 0
        CO->SYNC                            = (PFCoSync *)         memPool;
        memPool += sizeof(PFCoSync);
#endif
        for(i=0; i<CO_NO_RPDO; i++)
		{
            CO->RPDO[i]                     = (PFCoRpdo *)         memPool;
            memPool += sizeof(PFCoRpdo);
        }
        for(i=0; i<CO_NO_TPDO; i++)
		{
            CO->TPDO[i]                     = (PFCoTpdo *)         memPool;
            memPool += sizeof(PFCoTpdo);
        }
#if CO_NO_HB_CONS > 0
        CO->HBcons                          = (PFCoHBConsumer *)   memPool;
        memPool += sizeof(PFCoHBConsumer);
        CO_HBcons_monitoredNodes            = (PFCoHBConsNode *)   memPool;
        memPool += sizeof(PFCoHBConsNode) * CO_NO_HB_CONS;
#endif
#if CO_NO_SDO_CLIENT == 1
        CO->SDOclient                       = (PFCoSdoClient *)    memPool;
        memPool += sizeof(PFCoSdoClient);
#endif
#endif
    }

    errCnt = 0;
    if(CO->CANmodule[0]                 == NULL) errCnt++;
    if(CO_CANmodule_rxArray0            == NULL) errCnt++;
    if(CO_CANmodule_txArray0            == NULL) errCnt++;
#if CO_NO_CAN_MODULES >= 2
    if(CO->CANmodule[1]                 == NULL) errCnt++;
    if(CO_CANmodule_rxArray1            == NULL) errCnt++;
    if(CO_CANmodule_txArray1            == NULL) errCnt++;
#endif
    if(CO->SDO                          == NULL) errCnt++;
    if(CO_SDO_ODExtensions              == NULL) errCnt++;
    if(CO->em                           == NULL) errCnt++;
    if(CO->emPr                         == NULL) errCnt++;
    if(CO->NMT                          == NULL) errCnt++;
#if CO_NO_SYNC > 0
    if(CO->SYNC                         == NULL) errCnt++;
#endif
    for(i=0; i<CO_NO_RPDO; i++){
        if(CO->RPDO[i]                  == NULL) errCnt++;
    }
    for(i=0; i<CO_NO_TPDO; i++){
        if(CO->TPDO[i]                  == NULL) errCnt++;
    }
#if CO_NO_HB_CONS > 0
    if(CO->HBcons                       == NULL) errCnt++;
    if(CO_HBcons_monitoredNodes         == NULL) errCnt++;
#endif
#if CO_NO_SDO_CLIENT == 1
    if(CO->SDOclient                    == NULL) errCnt++;
#endif

    if(errCnt != 0) return enCO_ERROR_OUT_OF_MEMORY;
#endif

    //CO_CANsetConfigurationMode(CANhwCfg0);

    /* Read CANopen Node-ID and CAN bit-rate from object dictionary */
    nodeId = OD_CANNodeID; if(nodeId<1 || nodeId>127) nodeId = 0x10;
    CANBitRate = OD_CANBitRate;/* in kbps */


    err = pfCoCanModuleInit(
            CO->CANmodule[0],
            CANhwCfg0,
#if defined(__dsPIC33F__) || defined(__PIC24H__) \
    || defined(__dsPIC33E__) || defined(__PIC24E__)
            ADDR_DMA0,
            ADDR_DMA1,
           &CO_CANmsg[0],
            CO_CANmsgBuffSize,
            __builtin_dmaoffset(&CO_CANmsg[0]),
#if defined(__HAS_EDS__)
            __builtin_dmapage(&CO_CANmsg[0]),
#endif
#endif
            CO_CANmodule_rxArray0,
            CO_RXCAN_NO_MSGS,
            CO_CANmodule_txArray0,
            CO_TXCAN_NO_MSGS,
            CANBitRate);

    if(err)
	{
        pfCoDelete();
        return err;
    }


#if CO_NO_CAN_MODULES >= 2
    //CO_CANsetConfigurationMode(CANhwCfg1);
    err = pfCoCanModuleInit(
            CO->CANmodule[1],
            CANhwCfg1,
            CO_CANmodule_rxArray1,
            2,
            CO_CANmodule_txArray1,
            2,
            250);

    if(err)
	{
        pfCoDelete();
        return err;
    }
#endif


    err = pfCoSdoInit(
            CO->SDO,
            enCO_CAN_ID_RSDO + nodeId,
            enCO_CAN_ID_TSDO + nodeId,
            OD_H1200_SDO_SERVER_PARAM,
            0,
           &CO_OD[0],
            CO_OD_NoOfElements,
            CO_SDO_ODExtensions,
            nodeId,
            CO->CANmodule[0],
            CO_RXCAN_SDO_SRV,
            CO->CANmodule[0],
            CO_TXCAN_SDO_SRV);

    if(err)
	{
        pfCoDelete();
        return err;
    }

    err = pfCoEmInit(
            CO->em,
            CO->emPr,
            CO->SDO,
           &OD_errorStatusBits[0],
            ODL_errorStatusBits_stringLength,
           &OD_errorRegister,
           &OD_preDefinedErrorField[0],
            ODL_preDefinedErrorField_arrayLength,
            CO->CANmodule[0],
            CO_TXCAN_EMERG,
            enCO_CAN_ID_EMERGENCY + nodeId);

    if(err)
	{
        pfCoDelete();
        return err;
    }

    err = pfCoNmtInit(
            CO->NMT,
            CO->emPr,
            nodeId,
            500,
            CO->CANmodule[0],
            CO_RXCAN_NMT,
            enCO_CAN_ID_NMT_SERVICE,
            CO->CANmodule[0],
            CO_TXCAN_HB,
            enCO_CAN_ID_HEARTBEAT + nodeId);

    if(err)
	{
        pfCoDelete();
        return err;
    }


#if CO_NO_NMT_MASTER == 1
    NMTM_txBuff = pfCoCanTxBufferInit(/* return pointer to 8-byte CAN data buffer, which should be populated */
            CO->CANmodule[0], /* pointer to CAN module used for sending this message */
            CO_TXCAN_NMT,     /* index of specific buffer inside CAN module */
            0x0000,           /* CAN identifier */
            0,                /* rtr */
            2,                /* number of data bytes */
            0);               /* synchronous message flag bit */
#endif


#if CO_NO_SYNC > 0
    err = pfCoSyncInit(
            CO->SYNC,
            CO->em,
            CO->SDO,
           &CO->NMT->operatingState,
            OD_COB_ID_SYNCMessage,
            OD_communicationCyclePeriod,
            OD_synchronousCounterOverflowValue,
            CO->CANmodule[0],
            CO_RXCAN_SYNC,
            CO->CANmodule[0],
            CO_TXCAN_SYNC);

    if(err)
	{
        pfCoDelete();
        return err;
    }
#endif

    for(i=0; i<CO_NO_RPDO; i++)
	{
        PFCoCanModule *CANdevRx = CO->CANmodule[0];
        PFword CANdevRxIdx = CO_RXCAN_RPDO + i;

#if CO_NO_CAN_MODULES >= 2
        if(i >= 4)
		{
            CANdevRx = CO->CANmodule[1];
            CANdevRxIdx = i-4;
        }
#endif

        err = pfCoRpdoInit(
                CO->RPDO[i],
                CO->em,
                CO->SDO,
               &CO->NMT->operatingState,
                nodeId,
                ((i<4) ? (enCO_CAN_ID_RPDO_1+i*0x100) : 0),
                0,
                (PFCoRpdoCommPar*) &OD_RPDOCommunicationParameter[i],
                (PFCoRpdoMapPar*) &OD_RPDOMappingParameter[i],
                OD_H1400_RXPDO_1_PARAM+i,
                OD_H1600_RXPDO_1_MAPPING+i,
                CANdevRx,
                CANdevRxIdx);

        if(err)
		{
            pfCoDelete();
            return err;
        }
    }


    for(i=0; i<CO_NO_TPDO; i++){
        err = pfCoTpdoInit(
                CO->TPDO[i],
                CO->em,
                CO->SDO,
               &CO->NMT->operatingState,
                nodeId,
                ((i<4) ? (enCO_CAN_ID_TPDO_1+i*0x100) : 0),
                0,
                (PFCoTpdoCommPar*) &OD_TPDOCommunicationParameter[i],
                (PFCoTpdoMapPar*) &OD_TPDOMappingParameter[i],
                OD_H1800_TXPDO_1_PARAM+i,
                OD_H1A00_TXPDO_1_MAPPING+i,
                CO->CANmodule[0],
                CO_TXCAN_TPDO+i);

        if(err)
		{
            pfCoDelete();
            return err;
        }
    }

#if CO_NO_HB_CONS > 0
    err = pfCoHbConsumerInit(
            CO->HBcons,
            CO->em,
            CO->SDO,
           &OD_consumerHeartbeatTime[0],
            CO_HBcons_monitoredNodes,
            CO_NO_HB_CONS,
            CO->CANmodule[0],
            CO_RXCAN_CONS_HB);

    if(err)
	{
        pfCoDelete();
        return err;
    }
#endif

#if CO_NO_SDO_CLIENT == 1
    err = pfCoSdoClientInit(
            CO->SDOclient,
            CO->SDO,
            (PFCoSdoClientPar*) &OD_SDOClientParameter[0],
            CO->CANmodule[0],
            CO_RXCAN_SDO_CLI,
            CO->CANmodule[0],
            CO_TXCAN_SDO_CLI);

    if(err)
	{
        pfCoDelete();
        return err;
    }
#endif

    /* Configure Object dictionary entry at index 0x2101 and 0x2102 */
    pfCoODConfigure(CO->SDO, 0x2101, PFCoODFnodeId, 0, 0, 0);
    pfCoODConfigure(CO->SDO, 0x2102, PFCoODFbitRate, 0, 0, 0);

    return enCO_ERROR_NO;
}


/******************************************************************************/
void pfCoDelete()
{
	#if !defined(PF_CO_USE_GLOBALS) && defined(PF_PF_CO_USE_MALLOC)
    PFint16 i;
	#endif

    //CO_CANsetConfigurationMode(CANhwCfg0);
    pfCoCanModuleDisable(CO->CANmodule[0]);
#if CO_NO_CAN_MODULES >= 2
    //CO_CANsetConfigurationMode(CANhwCfg1);
    pfCoCanModuleDisable(CO->CANmodule[1]);
#endif

#if !defined(PF_CO_USE_GLOBALS) && defined(PF_CO_USE_MALLOC)

#if CO_NO_SDO_CLIENT == 1
    free(CO->SDOclient);
#endif
#if CO_NO_HB_CONS > 0
    free(CO_HBcons_monitoredNodes);
    free(CO->HBcons);
#endif
    for(i=0; i<CO_NO_RPDO; i++)
	{
        free(CO->RPDO[i]);
    }
    for(i=0; i<CO_NO_TPDO; i++)
	{
        free(CO->TPDO[i]);
    }
#if CO_NO_SYNC > 0
    free(CO->SYNC);
#endif
    free(CO->NMT);
    free(CO->emPr);
    free(CO->em);
    free(CO_SDO_ODExtensions);
    free(CO->SDO);
    free(CO_CANmodule_txArray0);
    free(CO_CANmodule_rxArray0);
    free(CO->CANmodule[0]);
#if CO_NO_CAN_MODULES >= 2
    free(CO_CANmodule_txArray1);
    free(CO_CANmodule_rxArray1);
    free(CO->CANmodule[1]);
#endif
    CO = NULL;
#endif
}


/******************************************************************************/
PFEnCoNmtResetCmd pfCoProcess(
        PFCo                   *CO,
        PFword                timeDifference_ms)
{
    PFEnBoolean NMTisPreOrOperational = enBooleanFalse;
    PFEnCoNmtResetCmd reset = enCO_RESET_NOT;
    static PFbyte ms50 = 0;

    if(CO->NMT->operatingState == enCO_NMT_PRE_OPERATIONAL || CO->NMT->operatingState == enCO_NMT_OPERATIONAL)
        NMTisPreOrOperational = enBooleanTrue;

    ms50 += timeDifference_ms;
    if(ms50 >= 50)
	{
        ms50 = 0;
        pfCoNmtBlinkingProcess50ms(CO->NMT);
    }

    pfCoSdoProcess(
            CO->SDO,
            NMTisPreOrOperational,
            timeDifference_ms,
            1000);


    pfCoEmProcess(
            CO->emPr,
            NMTisPreOrOperational,
            timeDifference_ms * 10,
            OD_inhibitTimeEMCY);

    reset = pfCoNmtProcess(
            CO->NMT,
            timeDifference_ms,
            OD_producerHeartbeatTime,
            OD_NMTStartup,
            OD_errorRegister,
            OD_errorBehavior);


#if CO_NO_HB_CONS > 0
    pfCoHbConsumerProcess(
            CO->HBcons,
            NMTisPreOrOperational,
            timeDifference_ms);
#endif

    return reset;
}


/******************************************************************************/
void pfCoProcessRPDO(PFCo *CO)
{
    PFint16 i;

#if CO_NO_SYNC > 0
    PFbyte SYNCret = pfCoSyncProcess(CO->SYNC, 1000L, OD_synchronousWindowLength);
    if(SYNCret == 2) pfCoCanClearPendingSyncPDOs(CO->CANmodule[0]);
#endif

    for(i=0; i<CO_NO_RPDO; i++)
	{
        pfCoRpdoProcess(CO->RPDO[i]);
    }
}

/******************************************************************************/
void pfCoProcessTPDO(PFCo *CO)
{
    PFint16 i;

    /* Verify PDO Change Of State and process PDOs */
    for(i=0; i<CO_NO_TPDO; i++)
	{
        if(!CO->TPDO[i]->sendRequest) CO->TPDO[i]->sendRequest = pfCoTpdoIsCOS(CO->TPDO[i]);
        pfCoTpdoProcess(CO->TPDO[i],
	#if CO_NO_SYNC > 0
        CO->SYNC,
	#endif
        10, 1);
    }
}
