/*
 * prime_cpu.c
 *
 */
#include "prime_framework.h"
#include "prime_CO_hal.h"

#if 0
CO_WEAK_DECL void pfCO_hal_GpioDevicePinInit(PFbyte devId, PFbyte pinNum, PFbyte dir, PFbyte val)
{
    /* not used */
}

CO_WEAK_DECL PFbyte pfCO_hal_GpioDevicePinStateGet(PFbyte devId, PFbyte pinNum)
{
    /* not used */
    return 0;
}

CO_WEAK_DECL PFint8 pfCO_hal_GpioDevicePinStateSet(PFbyte devId, PFbyte pinNum, PFbyte state)
{
    /* not used */
    return -1;
}

CO_WEAK_DECL PFint8 pfCO_hal_SpiDeviceWrite(PFbyte devId, PFbyte *buf, PFdword len, PFdword tmo)
{
    /* not used */
    return -1;
}

CO_WEAK_DECL PFint32 pfCO_hal_SpiDeviceRead(PFbyte devId, PFbyte *buf, PFdword len, PFdword tmo)
{
    /* not used */
    return -1;
}
#endif

CO_WEAK_DECL PFCoCpuInterruptsFlags pfCO_hal_InterruptsSaveDisable(void)
{
    PFCoCpuInterruptsFlags flags;
    register unsigned long reg = 0x40;

    asm volatile (
        "mrs %0, basepri\n"
        "msr basepri, %1"
         : "=r"(flags) : "r"(reg) : "memory", "cc");

    return flags;
}

CO_WEAK_DECL void pfCO_hal_InterruptsRestore(PFCoCpuInterruptsFlags flags)
{
    asm volatile (
        "msr basepri, %0"
        : : "r"(flags) : "memory", "cc");
}
