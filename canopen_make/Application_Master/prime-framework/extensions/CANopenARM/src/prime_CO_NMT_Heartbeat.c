/*
 * CANopen NMT and Heartbeat producer object.
 */

#include "prime_CO_driver.h"
#include "prime_CO_SDO.h"
#include "prime_CO_Emergency.h"
#include "prime_CO_NMT_Heartbeat.h"
#include "prime_CO_OD.h"

/*
 * Read received message from CAN module.
 *
 * Function will be called (by CAN receive interrupt) every time, when CAN
 * message with correct identifier will be received. For more information and
 * description of parameters see file CO_driver.h.
 */
static void pfCoNmtReceive(void *object, const PFCoCanRxMsg *msg)
{
    PFCoNmt *NMT;
    PFbyte nodeId;

    NMT = (PFCoNmt*)object;   /* this is the correct pointer type of the first argument */

    nodeId = msg->data[1];

    if((msg->DLC == 2) && ((nodeId == 0) || (nodeId == NMT->nodeId)))
	{
        PFbyte command = msg->data[0];

        switch(command)
		{
            case enCO_NMT_ENTER_OPERATIONAL:
                if((*NMT->emPr->errorRegister) == 0U)
				{
                    NMT->operatingState = enCO_NMT_OPERATIONAL;
                }
                break;
				
            case enCO_NMT_ENTER_STOPPED:
                NMT->operatingState = enCO_NMT_STOPPED;
                break;
				
            case enCO_NMT_ENTER_PRE_OPERATIONAL:
                NMT->operatingState = enCO_NMT_PRE_OPERATIONAL;
                break;
				
            case enCO_NMT_RESET_NODE:
                NMT->resetCommand = enCO_RESET_APP;
                break;
				
            case enCO_NMT_RESET_COMMUNICATION:
                NMT->resetCommand = enCO_RESET_COMM;
                break;
        }
    }
}


/******************************************************************************/
PFint16 pfCoNmtInit(
        PFCoNmt               *NMT,
        PFCoEmpr              *emPr,
        PFbyte                 nodeId,
        PFword                firstHBTime,
        PFCoCanModule         *NMT_CANdev,
        PFword                NMT_rxIdx,
        PFword                CANidRxNMT,
        PFCoCanModule         *HB_CANdev,
        PFword                HB_txIdx,
        PFword                CANidTxHB)
{

    /* blinking bytes */
    NMT->LEDflickering          = 0;
    NMT->LEDblinking            = 0;
    NMT->LEDsingleFlash         = 0;
    NMT->LEDdoubleFlash         = 0;
    NMT->LEDtripleFlash         = 0;
    NMT->LEDquadrupleFlash      = 0;

    /* Configure object variables */
    NMT->operatingState         = enCO_NMT_INITIALIZING;
    NMT->LEDgreenRun            = -1;
    NMT->LEDredError            = 1;
    NMT->nodeId                 = nodeId;
    NMT->firstHBTime            = firstHBTime;
    NMT->resetCommand           = 0;
    NMT->HBproducerTimer        = 0xFFFF;
    NMT->emPr                   = emPr;
    /* configure NMT CAN reception */
    pfCoCanRxBufferInit(
            NMT_CANdev,         /* CAN device */
            NMT_rxIdx,          /* rx buffer index */
            CANidRxNMT,         /* CAN identifier */
            0x7FF,              /* mask */
            0,                  /* rtr */
            (void*)NMT,         /* object passed to receive function */
            pfCoNmtReceive);    /* this function will process received message */

    /* configure HB CAN transmission */
    NMT->HB_CANdev = HB_CANdev;
    NMT->HB_TXbuff = pfCoCanTxBufferInit(
            HB_CANdev,          /* CAN device */
            HB_txIdx,           /* index of specific buffer inside CAN module */
            CANidTxHB,          /* CAN identifier */
            0,                  /* rtr */
            1,                  /* number of data bytes */
            0);                 /* synchronous message flag bit */

    return enCO_ERROR_NO;
}


/******************************************************************************/
void pfCoNmtBlinkingProcess50ms(PFCoNmt *NMT)
{

    if(++NMT->LEDflickering >= 1) NMT->LEDflickering = -1;

    if(++NMT->LEDblinking >= 4) NMT->LEDblinking = -4;

    if(++NMT->LEDsingleFlash >= 4) NMT->LEDsingleFlash = -20;

    switch(++NMT->LEDdoubleFlash)
	{
        case    4:  NMT->LEDdoubleFlash = -104; break;
        case -100:  NMT->LEDdoubleFlash =  100; break;
        case  104:  NMT->LEDdoubleFlash =  -20; break;
    }

    switch(++NMT->LEDtripleFlash)
	{
        case    4:  NMT->LEDtripleFlash = -104; break;
        case -100:  NMT->LEDtripleFlash =  100; break;
        case  104:  NMT->LEDtripleFlash = -114; break;
        case -110:  NMT->LEDtripleFlash =  110; break;
        case  114:  NMT->LEDtripleFlash =  -20; break;
    }

    switch(++NMT->LEDquadrupleFlash)
	{
        case    4:  NMT->LEDquadrupleFlash = -104; break;
        case -100:  NMT->LEDquadrupleFlash =  100; break;
        case  104:  NMT->LEDquadrupleFlash = -114; break;
        case -110:  NMT->LEDquadrupleFlash =  110; break;
        case  114:  NMT->LEDquadrupleFlash = -124; break;
        case -120:  NMT->LEDquadrupleFlash =  120; break;
        case  124:  NMT->LEDquadrupleFlash =  -20; break;
    }
}


/******************************************************************************/
PFEnCoNmtResetCmd pfCoNmtProcess(
        PFCoNmt              *NMT,
        PFword                timeDifference_ms,
        PFword                HBtime,
        PFdword               NMTstartup,
        PFbyte                errorRegister,
        const PFbyte          errorBehavior[])
{
    PFbyte CANpassive;

    NMT->HBproducerTimer += timeDifference_ms;

    /* Heartbeat producer message & Bootup message */
    if((HBtime && NMT->HBproducerTimer >= HBtime) || NMT->operatingState == enCO_NMT_INITIALIZING)
	{

        NMT->HBproducerTimer = NMT->HBproducerTimer - HBtime;

        NMT->HB_TXbuff->data[0] = NMT->operatingState;
        pfCoCanSend(NMT->HB_CANdev, NMT->HB_TXbuff);

        if(NMT->operatingState == enCO_NMT_INITIALIZING)
		{
            if(HBtime > NMT->firstHBTime) NMT->HBproducerTimer = HBtime - NMT->firstHBTime;

            if((NMTstartup & 0x04) == 0) 
				NMT->operatingState = enCO_NMT_OPERATIONAL;
            else                         
				NMT->operatingState = enCO_NMT_PRE_OPERATIONAL;
        }
    }

    /* CAN passive flag */
    CANpassive = 0;
    if(pfCoIsError(NMT->emPr->em, CO_EM_CAN_TX_BUS_PASSIVE) || pfCoIsError(NMT->emPr->em, CO_EM_CAN_RX_BUS_PASSIVE))
        CANpassive = 1;

    /* CANopen green RUN LED (DR 303-3) */
    switch(NMT->operatingState)
	{
        case enCO_NMT_STOPPED:          NMT->LEDgreenRun = NMT->LEDsingleFlash;   break;
        case enCO_NMT_PRE_OPERATIONAL:  NMT->LEDgreenRun = NMT->LEDblinking;      break;
        case enCO_NMT_OPERATIONAL:      NMT->LEDgreenRun = 1;                     break;
    }

    /* CANopen red ERROR LED (DR 303-3) */
    if(pfCoIsError(NMT->emPr->em, CO_EM_CAN_TX_BUS_OFF))
        NMT->LEDredError = 1;

    else if(pfCoIsError(NMT->emPr->em, CO_EM_SYNC_TIME_OUT))
        NMT->LEDredError = NMT->LEDtripleFlash;

    else if(pfCoIsError(NMT->emPr->em, CO_EM_HEARTBEAT_CONSUMER) || pfCoIsError(NMT->emPr->em, CO_EM_HB_CONSUMER_REMOTE_RESET))
        NMT->LEDredError = NMT->LEDdoubleFlash;

    else if(CANpassive || pfCoIsError(NMT->emPr->em, CO_EM_CAN_BUS_WARNING))
        NMT->LEDredError = NMT->LEDsingleFlash;

    else if(errorRegister)
        NMT->LEDredError = (NMT->LEDblinking>=0)?-1:1;

    else
        NMT->LEDredError = -1;


    /* in case of error enter pre-operational state */
    if(errorBehavior && (NMT->operatingState == enCO_NMT_OPERATIONAL))
	{
        PFbyte errBehav = CO_OD_getErrorBehavior(errorBehavior, 2);
        if(CANpassive && (errBehav == 0 || errBehav == 2)) errorRegister |= 0x10;

        if(errorRegister)
		{
            /* Communication error */
            if(errorRegister & enCO_ERR_REG_COMM_ERR)
			{
                errBehav = CO_OD_getErrorBehavior(errorBehavior, 1);
                if(errBehav == 0)
				{
                    NMT->operatingState = enCO_NMT_PRE_OPERATIONAL;
                }
                else if(errBehav == 2)
				{
                    NMT->operatingState = enCO_NMT_STOPPED;
                }
                else if(pfCoIsError(NMT->emPr->em, CO_EM_CAN_TX_BUS_OFF)
                     || pfCoIsError(NMT->emPr->em, CO_EM_HEARTBEAT_CONSUMER)
                     || pfCoIsError(NMT->emPr->em, CO_EM_HB_CONSUMER_REMOTE_RESET))
                {
                    errBehav = CO_OD_getErrorBehavior(errorBehavior, 0);
                    if(errBehav == 0)
					{
                        NMT->operatingState = enCO_NMT_PRE_OPERATIONAL;
                    }
                    else if(errBehav == 2)
					{
                        NMT->operatingState = enCO_NMT_STOPPED;
                    }
                }
            }

            /* Generic error */
            if(errorRegister & enCO_ERR_REG_GENERIC_ERR)
			{
                errBehav = CO_OD_getErrorBehavior(errorBehavior, 3);
                if      (errBehav == 0) NMT->operatingState = enCO_NMT_PRE_OPERATIONAL;
                else if (errBehav == 2) NMT->operatingState = enCO_NMT_STOPPED;
            }

            /* Device profile error */
            if(errorRegister & enCO_ERR_REG_DEV_PROFILE)
			{
                errBehav = CO_OD_getErrorBehavior(errorBehavior, 4);
                if      (errBehav == 0) NMT->operatingState = enCO_NMT_PRE_OPERATIONAL;
                else if (errBehav == 2) NMT->operatingState = enCO_NMT_STOPPED;
            }

            /* Manufacturer specific error */
            if(errorRegister & enCO_ERR_REG_MANUFACTURER)
			{
                errBehav = CO_OD_getErrorBehavior(errorBehavior, 5);
                if      (errBehav == 0) NMT->operatingState = enCO_NMT_PRE_OPERATIONAL;
                else if (errBehav == 2) NMT->operatingState = enCO_NMT_STOPPED;
            }

            /* if operational state is lost, send HB immediatelly. */
            if(NMT->operatingState != enCO_NMT_OPERATIONAL)
                NMT->HBproducerTimer = HBtime;
        }
    }
    return NMT->resetCommand;
}
