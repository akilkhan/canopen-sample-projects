/*
 * CANopen Process Data Object.
 */

#include "prime_framework.h"
#include "prime_CO_driver.h"
#include "prime_CO_SDO.h"
#include "prime_CO_Emergency.h"
#include "prime_CO_NMT_Heartbeat.h"
#include "prime_CO_SYNC.h"
#include "prime_CO_PDO.h"
#include "prime_CO_OD_cfg.h"
#include "prime_CO_OD.h"
#include <string.h>

/*
 * Read received message from CAN module.
 *
 * Function will be called (by CAN receive interrupt) every time, when CAN
 * message with correct identifier will be received. For more information and
 * description of parameters see file CO_driver.h.
 */
static void pfCoPdoReceive(void *object, const PFCoCanRxMsg *msg)
{
    PFCoRpdo *RPDO;

    RPDO = (PFCoRpdo*)object;   /* this is the correct pointer type of the first argument */

    if( (RPDO->valid) &&
        (*RPDO->operatingState == enCO_NMT_OPERATIONAL) &&
        (msg->DLC >= RPDO->dataLength) &&
        (!RPDO->CANrxNew))
    {
        /* copy data and set 'new message' flag */
        RPDO->CANrxData[0] = msg->data[0];
        RPDO->CANrxData[1] = msg->data[1];
        RPDO->CANrxData[2] = msg->data[2];
        RPDO->CANrxData[3] = msg->data[3];
        RPDO->CANrxData[4] = msg->data[4];
        RPDO->CANrxData[5] = msg->data[5];
        RPDO->CANrxData[6] = msg->data[6];
        RPDO->CANrxData[7] = msg->data[7];

        RPDO->CANrxNew = 1;
    }
}


/*
 * Configure RPDO Communication parameter.
 *
 * Function is called from commuincation reset or when parameter changes.
 *
 * Function configures following variable from PFCoRpdo: _valid_. It also
 * configures CAN rx buffer. If configuration fails, emergency message is send
 * and device is not able to enter NMT operational.
 *
 * @param RPDO RPDO object.
 * @param COB_IDUsedByRPDO _RPDO communication parameter_, _COB-ID for PDO_ variable
 * from Object dictionary (index 0x1400+, subindex 1).
 */
static void pfCoRpdoConfigCom(PFCoRpdo* RPDO, PFdword COB_IDUsedByRPDO)
{
    PFword ID;
    PFEnCoReturnError r;

    ID = (PFword)COB_IDUsedByRPDO;

    /* is RPDO used? */
    if((COB_IDUsedByRPDO & 0xBFFFF800L) == 0 && RPDO->dataLength && ID)
	{
        /* is used default COB-ID? */
        if(ID == RPDO->defaultCOB_ID) ID += RPDO->nodeId;
        RPDO->valid = enBooleanTrue;
    }
    else
	{
        ID = 0;
        RPDO->valid = enBooleanFalse;
        RPDO->CANrxNew = 0;
    }
    r = pfCoCanRxBufferInit(
            RPDO->CANdevRx,         /* CAN device */
            RPDO->CANdevRxIdx,      /* rx buffer index */
            ID,                     /* CAN identifier */
            0x7FF,                  /* mask */
            0,                      /* rtr */
            (void*)RPDO,            /* object passed to receive function */
            pfCoPdoReceive);        /* this function will process received message */
			
    if(r != enCO_ERROR_NO)
	{
        RPDO->valid = enBooleanFalse;
        RPDO->CANrxNew = 0;
    }
}

/*
 * Configure TPDO Communication parameter.
 *
 * Function is called from commuincation reset or when parameter changes.
 *
 * Function configures following variable from PFCoTpdo: _valid_. It also
 * configures CAN tx buffer. If configuration fails, emergency message is send
 * and device is not able to enter NMT operational.
 *
 * @param TPDO TPDO object.
 * @param COB_IDUsedByTPDO _TPDO communication parameter_, _COB-ID for PDO_ variable
 * from Object dictionary (index 0x1400+, subindex 1).
 * @param syncFlag Indicate, if TPDO is synchronous.
 */
static void pfCoTpdoConfigCom(PFCoTpdo* TPDO, PFdword COB_IDUsedByTPDO, PFbyte syncFlag)
{
	/*@AGv: have to make var volatile otherwise AVR GCC with -Os optimization generates erroneous code
	to compare COB_IDUsedByTPDO with it */
    volatile PFword ID;

    ID = (PFword)COB_IDUsedByTPDO;

    /* is TPDO used? */
    if((COB_IDUsedByTPDO & 0xBFFFF800L) == 0 && TPDO->dataLength && ID)
	{
        /* is used default COB-ID? */
        if(ID == TPDO->defaultCOB_ID)
			ID += TPDO->nodeId;
        TPDO->valid = enBooleanTrue;
    }
    else
	{
        ID = 0;
        TPDO->valid = enBooleanFalse;
    }

    TPDO->CANtxBuff = pfCoCanTxBufferInit(
            TPDO->CANdevTx,            /* CAN device */
            TPDO->CANdevTxIdx,         /* index of specific buffer inside CAN module */
            ID,                        /* CAN identifier */
            0,                         /* rtr */
            TPDO->dataLength,          /* number of data bytes */
            syncFlag);                 /* synchronous message flag bit */

    if(TPDO->CANtxBuff == 0)
	{
        TPDO->valid = enBooleanFalse;
    }
}


/*
 * Find mapped variable in Object Dictionary.
 *
 * Function is called from CO_R(T)PDOconfigMap or when mapping parameter changes.
 *
 * @param SDO SDO object.
 * @param map PDO mapping parameter.
 * @param R_T 0 for RPDO map, 1 for TPDO map.
 * @param ppData Pointer to returning parameter: pointer to data of mapped variable.
 * @param pLength Pointer to returning parameter: *add* length of mapped variable.
 * @param pSendIfCOSFlags Pointer to returning parameter: sendIfCOSFlags variable.
 * @param pIsMultibyteVar Pointer to returning parameter: true for multibyte variable.
 *
 * @return 0 on success, otherwise SDO abort code.
 */
static PFdword pfCoPdoFindMap(
        PFCoSdo               *SDO,
        PFdword                map,
        PFbyte                 R_T,
        PFbyte               **ppData,
        PFbyte                *pLength,
        PFbyte                *pSendIfCOSFlags,
        PFbyte                *pIsMultibyteVar)
{
    PFword entryNo;
    PFword index;
    PFbyte subIndex;
    PFbyte dataLen;
    PFbyte objectLen;
    PFbyte attr;

    index = (PFword)(map>>16);
    subIndex = (PFbyte)(map>>8);
    dataLen = (PFbyte) map;   /* data length in bits */

    /* data length must be byte aligned */
    if(dataLen&0x07) return enCO_SDO_AB_NO_MAP;   /* Object cannot be mapped to the PDO. */

    dataLen >>= 3;    /* new data length is in bytes */
    *pLength += dataLen;

    /* total PDO length can not be more than 8 bytes */
    if(*pLength > 8) return enCO_SDO_AB_MAP_LEN;  /* The number and length of the objects to be mapped would exceed PDO length. */

    /* is there a reference to dummy entries */
    if(index <=7 && subIndex == 0)
	{
        static PFdword dummyTX = 0;
        static PFdword dummyRX;
        PFbyte dummySize = 4;

        if(index<2) dummySize = 0;
        else if(index==2 || index==5) dummySize = 1;
        else if(index==3 || index==6) dummySize = 2;

        /* is size of variable big enough for map */
        if(dummySize < dataLen) return enCO_SDO_AB_NO_MAP;   /* Object cannot be mapped to the PDO. */

        /* Data and ODE pointer */
        if(R_T == 0) *ppData = (PFbyte*) &dummyRX;
        else         *ppData = (PFbyte*) &dummyTX;

        return 0;
    }

    /* find object in Object Dictionary */
    entryNo = pfCoODfind(SDO, index);

    /* Does object exist in OD? */
    if(entryNo == 0xFFFF || subIndex > CO_OD_getEntryMaxSubIndex(&SDO->OD[entryNo]))
        return enCO_SDO_AB_NOT_EXIST;   /* Object does not exist in the object dictionary. */

    attr = pfCoODGetAttribute(SDO, entryNo, subIndex);
    /* Is object Mappable for RPDO? */
    if(R_T==0 && !(attr&CO_ODA_RPDO_MAPABLE && attr&CO_ODA_WRITEABLE)) return enCO_SDO_AB_NO_MAP;   /* Object cannot be mapped to the PDO. */
    /* Is object Mappable for TPDO? */
    if(R_T!=0 && !(attr&CO_ODA_TPDO_MAPABLE && attr&CO_ODA_READABLE)) return enCO_SDO_AB_NO_MAP;   /* Object cannot be mapped to the PDO. */

    /* is size of variable big enough for map */
    objectLen = pfCoODGetLength(SDO, entryNo, subIndex);
    if(objectLen < dataLen) return enCO_SDO_AB_NO_MAP;   /* Object cannot be mapped to the PDO. */

    /* mark multibyte variable */
    *pIsMultibyteVar = (attr & CO_ODA_MB_VALUE) ? 1 : 0;

    /* pointer to data */
    *ppData = (PFbyte*) pfCoODGetDataPointer(SDO, entryNo, subIndex);
#ifdef CO_BIG_ENDIAN
    /* skip unused MSB bytes */
    if(*pIsMultibyteVar)
	{
        *ppData += objectLen - dataLen;
    }
#endif

    /* setup change of state flags */
    if(attr&CO_ODA_TPDO_DETECT_COS)
	{
        PFint16 i;
        for(i=*pLength-dataLen; i<*pLength; i++)
		{
            *pSendIfCOSFlags |= 1<<i;
        }
    }

    return 0;
}


/*
 * Configure RPDO Mapping parameter.
 *
 * Function is called from communication reset or when parameter changes.
 *
 * Function configures following variables from PFCoRpdo: _dataLength_ and
 * _mapPointer_.
 *
 * @param RPDO RPDO object.
 * @param noOfMappedObjects Number of mapped object (from OD).
 *
 * @return 0 on success, otherwise SDO abort code.
 */
static PFdword pfCoRpdoConfigMap(PFCoRpdo* RPDO, PFbyte noOfMappedObjects)
{
    PFint16 i;
    PFbyte length = 0;
    PFdword ret = 0;
    const PFdword* pMap = &RPDO->RPDOMapPar->mappedObject1;

    for(i=noOfMappedObjects; i>0; i--)
	{
        PFint16 j;
        PFbyte* pData;
        PFbyte dummy = 0;
        PFbyte prevLength = length;
        PFbyte MBvar;
        PFdword map = CO_OD_getMappedObjectFromPtr(pMap);
        pMap++;

        /* function do much checking of errors in map */
        ret = pfCoPdoFindMap(
                RPDO->SDO,
                map,
                0,
                &pData,
                &length,
                &dummy,
                &MBvar);
        
		if(ret)
		{
            length = 0;
            pfCoErrorReport(RPDO->em, CO_EM_PDO_WRONG_MAPPING, CO_EMC_PROTOCOL_ERROR, map);
            break;
        }

        /* write PDO data pointers */
#ifdef CO_BIG_ENDIAN
        if(MBvar)
		{
            for(j=length-1; j>=prevLength; j--)
                RPDO->mapPointer[j] = pData++;
        }
        else
		{
            for(j=prevLength; j<length; j++)
                RPDO->mapPointer[j] = pData++;
        }
#else
        for(j=prevLength; j<length; j++)
		{
            RPDO->mapPointer[j] = pData++;
        }
#endif

    }

    RPDO->dataLength = length;

    return ret;
}

/*
 * Configure TPDO Mapping parameter.
 *
 * Function is called from communication reset or when parameter changes.
 *
 * Function configures following variables from PFCoTpdo: _dataLength_,
 * _mapPointer_ and _sendIfCOSFlags_.
 *
 * @param TPDO TPDO object.
 * @param noOfMappedObjects Number of mapped object (from OD).
 *
 * @return 0 on success, otherwise SDO abort code.
 */
static PFdword pfCoTpdoConfigMap(PFCoTpdo* TPDO, PFbyte noOfMappedObjects)
{
    PFint16 i;
    PFbyte length = 0;
    PFdword ret = 0;
    const PFdword* pMap = &TPDO->TPDOMapPar->mappedObject1;

    TPDO->sendIfCOSFlags = 0;

    for(i=noOfMappedObjects; i>0; i--)
	{
        PFint16 j;
        PFbyte* pData;
        PFbyte prevLength = length;
        PFbyte MBvar;
        PFdword map = CO_OD_getMappedObjectFromPtr(pMap);
        pMap++;

        /* function do much checking of errors in map */
        ret = pfCoPdoFindMap
		(
                TPDO->SDO,
                map,
                1,
                &pData,
                &length,
                &TPDO->sendIfCOSFlags,
                &MBvar);
        
		if(ret)
		{
            length = 0;
            pfCoErrorReport(TPDO->em, CO_EM_PDO_WRONG_MAPPING, CO_EMC_PROTOCOL_ERROR, map);
            break;
        }

        /* write PDO data pointers */
#ifdef CO_BIG_ENDIAN
        if(MBvar)
		{
            for(j=length-1; j>=prevLength; j--)
                TPDO->mapPointer[j] = pData++;
        }
        else
		{
            for(j=prevLength; j<length; j++)
                TPDO->mapPointer[j] = pData++;
        }
#else
        for(j=prevLength; j<length; j++)
		{
            TPDO->mapPointer[j] = pData++;
        }
#endif

    }

    TPDO->dataLength = length;

    return ret;
}


/*
 * Function for accessing _RPDO communication parameter_ (index 0x1400+) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODFRpdoCom(PFCoODFarg *ODF_arg)
{
    PFCoRpdo *RPDO;

    RPDO = (PFCoRpdo*) ODF_arg->object;

    /* Reading Object Dictionary variable */
    if(ODF_arg->reading)
	{
        if(ODF_arg->subIndex == 1)
		{
            PFdword *value = (PFdword*) ODF_arg->data;

            /* if default COB ID is used, write default value here */
            if(((*value)&0xFFFF) == RPDO->defaultCOB_ID && RPDO->defaultCOB_ID)
                *value += RPDO->nodeId;

            /* If PDO is not valid, set bit 31 */
            if(!RPDO->valid) *value |= 0x80000000L;
        }
        return enCO_SDO_AB_NONE;
    }

    /* Writing Object Dictionary variable */
    if(RPDO->restrictionFlags & 0x04)
        return enCO_SDO_AB_READONLY;  /* Attempt to write a read only object. */
    if(*RPDO->operatingState == enCO_NMT_OPERATIONAL && (RPDO->restrictionFlags & 0x01))
        return enCO_SDO_AB_DATA_DEV_STATE;   /* Data cannot be transferred or stored to the application because of the present device state. */

    if(ODF_arg->subIndex == 1)
	{   /* COB_ID */
        PFdword *value = (PFdword*) ODF_arg->data;

        /* bits 11...29 must be zero */
        if(*value & 0x3FFF8000L)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

        /* if default COB-ID is being written, write defaultCOB_ID without nodeId */
        if(((*value)&0xFFFF) == (RPDO->defaultCOB_ID + RPDO->nodeId))
		{
            *value &= 0xC0000000L;
            *value += RPDO->defaultCOB_ID;
        }

        /* if PDO is valid, bits 0..29 can not be changed */
        if(RPDO->valid && ((*value ^ CO_OD_getCOB_IDUsedByRPDO(RPDO->RPDOCommPar)) & 0x3FFFFFFFL))
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

        /* configure RPDO */
        pfCoRpdoConfigCom(RPDO, *value);
    }
    else if(ODF_arg->subIndex == 2)
	{   /* Transmission_type */
        PFbyte *value = (PFbyte*) ODF_arg->data;

        /* values from 241...253 are not valid */
        if(*value >= 241 && *value <= 253)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */
    }

    return enCO_SDO_AB_NONE;
}

/*
 * Function for accessing _TPDO communication parameter_ (index 0x1800+) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODFTpdoCom(PFCoODFarg *ODF_arg)
{
    PFCoTpdo *TPDO;

    TPDO = (PFCoTpdo*) ODF_arg->object;

    if(ODF_arg->subIndex == 4) return enCO_SDO_AB_SUB_UNKNOWN;  /* Sub-index does not exist. */

    /* Reading Object Dictionary variable */
    if(ODF_arg->reading)
	{
        if(ODF_arg->subIndex == 1)
		{   /* COB_ID */
            PFdword *value = (PFdword*) ODF_arg->data;

            /* if default COB ID is used, write default value here */
            if(((*value)&0xFFFF) == TPDO->defaultCOB_ID && TPDO->defaultCOB_ID)
                *value += TPDO->nodeId;

            /* If PDO is not valid, set bit 31 */
            if(!TPDO->valid) *value |= 0x80000000L;
        }
        return enCO_SDO_AB_NONE;
    }

    /* Writing Object Dictionary variable */
    if(TPDO->restrictionFlags & 0x04)
        return enCO_SDO_AB_READONLY;  /* Attempt to write a read only object. */
    if(*TPDO->operatingState == enCO_NMT_OPERATIONAL && (TPDO->restrictionFlags & 0x01))
        return enCO_SDO_AB_DATA_DEV_STATE;   /* Data cannot be transferred or stored to the application because of the present device state. */

    if(ODF_arg->subIndex == 1)
	{   /* COB_ID */
        PFdword *value = (PFdword*) ODF_arg->data;

        /* bits 11...29 must be zero */
        if(*value & 0x3FFF8000L)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

        /* if default COB-ID is being written, write defaultCOB_ID without nodeId */
        if(((*value)&0xFFFF) == (TPDO->defaultCOB_ID + TPDO->nodeId))
		{
            *value &= 0xC0000000L;
            *value += TPDO->defaultCOB_ID;
        }

        /* if PDO is valid, bits 0..29 can not be changed */
        if(TPDO->valid && ((*value ^ CO_OD_getCOB_IDUsedByTPDO(TPDO->TPDOCommPar)) & 0x3FFFFFFFL))
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

#if CO_NO_SYNC > 0
        /* configure TPDO */
        pfCoTpdoConfigCom(TPDO, *value, TPDO->CANtxBuff->syncFlag);
        TPDO->syncCounter = 255;
#else
        /* configure TPDO */
        pfCoTpdoConfigCom(TPDO, *value, enBooleanFalse);
#endif
    }
    else if(ODF_arg->subIndex == 2)
	{   /* Transmission_type */
        PFbyte *value = (PFbyte*) ODF_arg->data;

        /* values from 241...253 are not valid */
        if(*value >= 241 && *value <= 253)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */
#if CO_NO_SYNC > 0
        TPDO->CANtxBuff->syncFlag = (*value <= 240) ? 1 : 0;
        TPDO->syncCounter = 255;
#endif
    }
    else if(ODF_arg->subIndex == 3)
	{   /* Inhibit_Time */
        /* if PDO is valid, value can not be changed */
        if(TPDO->valid)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

        TPDO->inhibitTimer = 0;
    }
    else if(ODF_arg->subIndex == 5)
	{   /* Event_Timer */
        PFword *value = (PFword*) ODF_arg->data;

        TPDO->eventTimer = *value;
    }
    else if(ODF_arg->subIndex == 6)
	{   /* SYNC start value */
        PFbyte *value = (PFbyte*) ODF_arg->data;

        /* if PDO is valid, value can not be changed */
        if(TPDO->valid)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

        /* values from 240...255 are not valid */
        if(*value > 240)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */
    }

    return enCO_SDO_AB_NONE;
}

/*
 * Function for accessing _RPDO mapping parameter_ (index 0x1600+) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODFRpdoMap(PFCoODFarg *ODF_arg)
{
    PFCoRpdo *RPDO;

    RPDO = (PFCoRpdo*) ODF_arg->object;

    /* Reading Object Dictionary variable */
    if(ODF_arg->reading)
	{
        PFbyte *value = (PFbyte*) ODF_arg->data;

        if(ODF_arg->subIndex == 0)
		{
            /* If there is error in mapping, dataLength is 0, so numberOfMappedObjects is 0. */
            if(!RPDO->dataLength) *value = 0;
        }
        return enCO_SDO_AB_NONE;
    }

    /* Writing Object Dictionary variable */
    if(RPDO->restrictionFlags & 0x08)
        return enCO_SDO_AB_READONLY;  /* Attempt to write a read only object. */
    if(*RPDO->operatingState == enCO_NMT_OPERATIONAL && (RPDO->restrictionFlags & 0x02))
        return enCO_SDO_AB_DATA_DEV_STATE;   /* Data cannot be transferred or stored to the application because of the present device state. */
    if(RPDO->valid)
        return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

    /* numberOfMappedObjects */
    if(ODF_arg->subIndex == 0)
	{
        PFbyte *value = (PFbyte*) ODF_arg->data;

        if(*value > 8)
            return enCO_SDO_AB_VALUE_HIGH;  /* Value of parameter written too high. */

        /* configure mapping */
        return pfCoRpdoConfigMap(RPDO, *value);
    }

    /* mappedObject */
    else
	{
        PFdword *value = (PFdword*) ODF_arg->data;
        PFbyte* pData;
        PFbyte length = 0;
        PFbyte dummy = 0;
        PFbyte MBvar;

        if(RPDO->dataLength)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

        /* verify if mapping is correct */
        return pfCoPdoFindMap(
                RPDO->SDO,
               *value,
                0,
               &pData,
               &length,
               &dummy,
               &MBvar);
    }

    return enCO_SDO_AB_NONE;
}


/*
 * Function for accessing _TPDO mapping parameter_ (index 0x1A00+) from SDO server.
 *
 * For more information see file prime_CO_SDO.h.
 */
static PFEnCoSdoAbortCode pfCoODFTpdoMap(PFCoODFarg *ODF_arg)
{
    PFCoTpdo *TPDO;

    TPDO = (PFCoTpdo*) ODF_arg->object;

    /* Reading Object Dictionary variable */
    if(ODF_arg->reading)
	{
        PFbyte *value = (PFbyte*) ODF_arg->data;

        if(ODF_arg->subIndex == 0)
		{
            /* If there is error in mapping, dataLength is 0, so numberOfMappedObjects is 0. */
            if(!TPDO->dataLength) *value = 0;
        }
        return enCO_SDO_AB_NONE;
    }

    /* Writing Object Dictionary variable */
    if(TPDO->restrictionFlags & 0x08)
        return enCO_SDO_AB_READONLY;  /* Attempt to write a read only object. */
    if(*TPDO->operatingState == enCO_NMT_OPERATIONAL && (TPDO->restrictionFlags & 0x02))
        return enCO_SDO_AB_DATA_DEV_STATE;   /* Data cannot be transferred or stored to the application because of the present device state. */
    if(TPDO->valid)
        return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

    /* numberOfMappedObjects */
    if(ODF_arg->subIndex == 0)
	{
        PFbyte *value = (PFbyte*) ODF_arg->data;

        if(*value > 8)
            return enCO_SDO_AB_VALUE_HIGH;  /* Value of parameter written too high. */

        /* configure mapping */
        return pfCoTpdoConfigMap(TPDO, *value);
    }

    /* mappedObject */
    else
	{
        PFdword *value = (PFdword*) ODF_arg->data;
        PFbyte* pData;
        PFbyte length = 0;
        PFbyte dummy = 0;
        PFbyte MBvar;

        if(TPDO->dataLength)
            return enCO_SDO_AB_INVALID_VALUE;  /* Invalid value for parameter (download only). */

        /* verify if mapping is correct */
        return pfCoPdoFindMap(
                TPDO->SDO,
               *value,
                1,
               &pData,
               &length,
               &dummy,
               &MBvar);
    }

    return enCO_SDO_AB_NONE;
}


/******************************************************************************/
PFint16 pfCoRpdoInit(
        PFCoRpdo              *RPDO,
        PFCoEm                *em,
        PFCoSdo               *SDO,
        PFbyte                *operatingState,
        PFbyte                 nodeId,
        PFword                defaultCOB_ID,
        PFbyte                 restrictionFlags,
        const PFCoRpdoCommPar *RPDOCommPar,
        const PFCoRpdoMapPar  *RPDOMapPar,
        PFword                idx_RPDOCommPar,
        PFword                idx_RPDOMapPar,
        PFCoCanModule         *CANdevRx,
        PFword                CANdevRxIdx)
{

    /* Configure object variables */
    RPDO->em = em;
    RPDO->SDO = SDO;
    RPDO->RPDOCommPar = RPDOCommPar;
    RPDO->RPDOMapPar = RPDOMapPar;
    RPDO->operatingState = operatingState;
    RPDO->nodeId = nodeId;
    RPDO->defaultCOB_ID = defaultCOB_ID;
    RPDO->restrictionFlags = restrictionFlags;

    /* Configure Object dictionary entry at index 0x1400+ and 0x1600+ */
    pfCoODConfigure(SDO, idx_RPDOCommPar, pfCoODFRpdoCom, (void*)RPDO, 0, 0);
    pfCoODConfigure(SDO, idx_RPDOMapPar, pfCoODFRpdoMap, (void*)RPDO, 0, 0);

    /* configure communication and mapping */
    RPDO->CANrxNew = 0;
    RPDO->CANdevRx = CANdevRx;
    RPDO->CANdevRxIdx = CANdevRxIdx;

    pfCoRpdoConfigMap(RPDO, CO_OD_getNumberOfMappedObjects(RPDOMapPar));
    pfCoRpdoConfigCom(RPDO, CO_OD_getCOB_IDUsedByRPDO(RPDOCommPar));

    return enCO_ERROR_NO;
}


/******************************************************************************/
PFint16 pfCoTpdoInit(
        PFCoTpdo              *TPDO,
        PFCoEm                *em,
        PFCoSdo               *SDO,
        PFbyte                *operatingState,
        PFbyte                 nodeId,
        PFword                defaultCOB_ID,
        PFbyte                 restrictionFlags,
        const PFCoTpdoCommPar *TPDOCommPar,
        const PFCoTpdoMapPar  *TPDOMapPar,
        PFword                idx_TPDOCommPar,
        PFword                idx_TPDOMapPar,
        PFCoCanModule         *CANdevTx,
        PFword                CANdevTxIdx)
{
    PFbyte transmissionType;

    /* Configure object variables */
    TPDO->em = em;
    TPDO->SDO = SDO;
    TPDO->TPDOCommPar = TPDOCommPar;
    TPDO->TPDOMapPar = TPDOMapPar;
    TPDO->operatingState = operatingState;
    TPDO->nodeId = nodeId;
    TPDO->defaultCOB_ID = defaultCOB_ID;
    TPDO->restrictionFlags = restrictionFlags;

    /* Configure Object dictionary entry at index 0x1800+ and 0x1A00+ */
    pfCoODConfigure(SDO, idx_TPDOCommPar, pfCoODFTpdoCom, (void*)TPDO, 0, 0);
    pfCoODConfigure(SDO, idx_TPDOMapPar, pfCoODFTpdoMap, (void*)TPDO, 0, 0);

    /* configure communication and mapping */
    TPDO->CANdevTx = CANdevTx;
    TPDO->CANdevTxIdx = CANdevTxIdx;
#if CO_NO_SYNC > 0
    TPDO->syncCounter = 255;
    TPDO->SYNCtimerPrevious = 0;
#endif
    TPDO->inhibitTimer = 0;
    TPDO->eventTimer = CO_OD_getEventTimer(TPDOCommPar);
    transmissionType = CO_OD_getTransmissionType(TPDOCommPar);
    if(transmissionType>=254) TPDO->sendRequest = 1;

    pfCoTpdoConfigMap(TPDO, CO_OD_getNumberOfMappedObjects(TPDOMapPar));
    //volatile PFdword cobid = pgm_read_dword(&(TPDOCommPar->COB_IDUsedByTPDO));
    pfCoTpdoConfigCom(TPDO, CO_OD_getCOB_IDUsedByTPDO(TPDOCommPar)/*cobid*/, ((transmissionType<=240) ? 1 : 0));

    if((transmissionType>240 &&
         transmissionType<254) ||
         CO_OD_getSYNCStartValue(TPDOCommPar)>240)
		 {
            TPDO->valid = enBooleanFalse;
		}

    return enCO_ERROR_NO;
}


/******************************************************************************/
PFbyte pfCoTpdoIsCOS(PFCoTpdo *TPDO)
{

    /* Prepare TPDO data automatically from Object Dictionary variables */
    PFbyte* pPDOdataByte;
    PFbyte** ppODdataByte;

    pPDOdataByte = &TPDO->CANtxBuff->data[TPDO->dataLength];
    ppODdataByte = &TPDO->mapPointer[TPDO->dataLength];

    switch(TPDO->dataLength){
        case 8: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x80)) return 1;
        case 7: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x40)) return 1;
        case 6: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x20)) return 1;
        case 5: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x10)) return 1;
        case 4: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x08)) return 1;
        case 3: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x04)) return 1;
        case 2: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x02)) return 1;
        case 1: if(*(--pPDOdataByte) != **(--ppODdataByte) && (TPDO->sendIfCOSFlags&0x01)) return 1;
    }

    return 0;
}

//#define TPDO_CALLS_EXTENSION
/******************************************************************************/
PFint16 pfCoTpdoSend(PFCoTpdo *TPDO)
{
    PFint16 i;
    PFbyte* pPDOdataByte;
    PFbyte** ppODdataByte;

#ifdef TPDO_CALLS_EXTENSION
    if(TPDO->SDO->ODExtensions)
	{
        /* for each mapped OD, check mapping to see if an OD extension is available, and call it if it is */
        const PFdword* pMap = &TPDO->TPDOMapPar->mappedObject1;
        PFCoSdo *pSDO = TPDO->SDO;
        PFbyte obsNum = pfCoODGetNumberOfMappedObjects(TPDO->TPDOMapPar);

        for(i=obsNum; i>0; i--)
		{
            PFdword map = pfCoODGetMappedObjectFromPtr(pMap);
            pMap++;
            PFword index = (PFword)(map>>16);
            PFbyte subIndex = (PFbyte)(map>>8);
            PFword entryNo = pfCoODfind(pSDO, index);
            pfCoODExtension *ext = &pSDO->ODExtensions[entryNo];
            if( ext->pODFunc == NULL) continue;
            PFCoODFarg ODF_arg;
            memset((void*)&ODF_arg, 0, sizeof(PFCoODFarg));
            ODF_arg.reading = true;
            ODF_arg.index = index;
            ODF_arg.subIndex = subIndex;
            ODF_arg.object = ext->object;
            ODF_arg.attribute = pfCoODGetAttribute(pSDO, entryNo, subIndex);
            ODF_arg.pFlags = pfCoODGetFlagsPointer(pSDO, entryNo, subIndex);
            ODF_arg.data = pfCoODGetEntryDataPtr(&(pSDO->OD[entryNo]));
            ODF_arg.dataLength = pfCoODGetLength(pSDO, entryNo, subIndex);
            ext->pODFunc(&ODF_arg);
        }
    }
#endif
    pPDOdataByte = &TPDO->CANtxBuff->data[0];
    ppODdataByte = &TPDO->mapPointer[0];

    for(i=TPDO->dataLength; i>0; i--)
        *(pPDOdataByte++) = **(ppODdataByte++);

    TPDO->sendRequest = 0;

    return pfCoCanSend(TPDO->CANdevTx, TPDO->CANtxBuff);
}

//#define RPDO_CALLS_EXTENSION
/******************************************************************************/
void pfCoRpdoProcess(PFCoRpdo *RPDO)
{

    if(RPDO->CANrxNew && RPDO->valid     && *RPDO->operatingState == enCO_NMT_OPERATIONAL)
	{
        PFint16 i;
        PFbyte* pPDOdataByte;
        PFbyte** ppODdataByte;

        pPDOdataByte = &RPDO->CANrxData[0];
        ppODdataByte = &RPDO->mapPointer[0];
        for(i=RPDO->dataLength; i>0; i--)
            **(ppODdataByte++) = *(pPDOdataByte++);

#ifdef RPDO_CALLS_EXTENSION
        if(RPDO->SDO->ODExtensions)
		{
            /* for each mapped OD, check mapping to see if an OD extension is available, and call it if it is */
            const PFdword* pMap = &RPDO->RPDOMapPar->mappedObject1;
            PFCoSdo *pSDO = RPDO->SDO;
            PFbyte objsNum = pfCoODGetNumberOfMappedObjects(RPDO->RPDOMapPar);

            for(i=objsNum; i>0; i--)
			{
                PFdword map = pfCoODGetMappedObjectFromPtr(pMap);
                pMap++;
                PFword index = (PFword)(map>>16);
                PFbyte subIndex = (PFbyte)(map>>8);
                PFword entryNo = pfCoODfind(pSDO, index);
                pfCoODExtension *ext = &pSDO->ODExtensions[entryNo];
                if( ext->pODFunc == NULL) continue;
                PFCoODFarg ODF_arg;
                memset((void*)&ODF_arg, 0, sizeof(PFCoODFarg));
                ODF_arg.reading = false;
                ODF_arg.index = index;
                ODF_arg.subIndex = subIndex;
                ODF_arg.object = ext->object;
                ODF_arg.attribute = pfCoODGetAttribute(pSDO, entryNo, subIndex);
                ODF_arg.pFlags = pfCoODGetFlagsPointer(pSDO, entryNo, subIndex);
                ODF_arg.data = pfCoODGetEntryDataPtr(&(pSDO->OD[entryNo]));
                ODF_arg.dataLength = pfCoODGetLength(pSDO, entryNo, subIndex);
                ext->pODFunc(&ODF_arg);
            }
        }
#endif
    }

    RPDO->CANrxNew = 0;
}


/******************************************************************************/
void pfCoTpdoProcess(
        PFCoTpdo              *TPDO,
#if CO_NO_SYNC > 0
        PFCoSync              *SYNC,
#endif
        PFword                timeDifference_100us,
        PFword                timeDifference_ms)
{
    int32_t i;
    PFbyte transmissionType = CO_OD_getTransmissionType(TPDO->TPDOCommPar);

    if(TPDO->valid && *TPDO->operatingState == enCO_NMT_OPERATIONAL)
	{

        /* Send PDO by application request or by Event timer */
        if(transmissionType >= 253){
            if(TPDO->inhibitTimer == 0 && (TPDO->sendRequest || (CO_OD_getEventTimer(TPDO->TPDOCommPar) && TPDO->eventTimer == 0)))
			{
                if(pfCoTpdoSend(TPDO) == enCO_ERROR_NO){
                    /* successfully sent */
                    TPDO->inhibitTimer = CO_OD_getInhibitTime(TPDO->TPDOCommPar);
                    TPDO->eventTimer = CO_OD_getEventTimer(TPDO->TPDOCommPar);
                }
            }
        }
#if CO_NO_SYNC > 0
        /* Synchronous PDOs */
        else if(SYNC && SYNC->running && SYNC->curentSyncTimeIsInsideWindow)
		{
            /* detect SYNC message */
            if(SYNC->timer < TPDO->SYNCtimerPrevious)
			{
                /* send synchronous acyclic PDO */
                if(transmissionType == 0)
				{
                    if(TPDO->sendRequest) pfCoTpdoSend(TPDO);
                }
                /* send synchronous cyclic PDO */
                else
				{
                    /* is the start of synchronous TPDO transmission */
                    if(TPDO->syncCounter == 255)
					{
                        if(SYNC->counterOverflowValue && CO_OD_getSYNCStartValue(TPDO->TPDOCommPar))
                            TPDO->syncCounter = 254;   /* SYNCStartValue is in use */
                        else
                            TPDO->syncCounter = transmissionType;
                    }
                    /* if the SYNCStartValue is in use, start first TPDO after SYNC with matched SYNCStartValue. */
                    if(TPDO->syncCounter == 254)
					{
                        if(SYNC->counter == CO_OD_getSYNCStartValue(TPDO->TPDOCommPar))
						{
                            TPDO->syncCounter = transmissionType;
                            pfCoTpdoSend(TPDO);
                        }
                    }
                    /* Send PDO after every N-th Sync */
                    else if(--TPDO->syncCounter == 0)
					{
                        TPDO->syncCounter = transmissionType;
                        pfCoTpdoSend(TPDO);
                    }
                }
            }
        }
#endif
    }
    else
	{
        /* Not operational or valid. Force TPDO first send after operational or valid. */
        if(transmissionType>=254) TPDO->sendRequest = 1;
        else                      TPDO->sendRequest = 0;
    }

    /* update timers */
    i = TPDO->inhibitTimer;
    i -= timeDifference_100us;
    TPDO->inhibitTimer = (i<=0) ? 0 : (PFword)i;

    i = TPDO->eventTimer;
    i -= timeDifference_ms;
    TPDO->eventTimer = (i<=0) ? 0 : (PFword)i;

#if CO_NO_SYNC > 0
    TPDO->SYNCtimerPrevious = SYNC->timer;
#endif
}
