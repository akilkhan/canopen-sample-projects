#include "prime_framework.h"
#if (PF_USE_EINT2 == 1)
#include "prime_eint2.h"


static PFcallback eint2CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint2MaxIntCount=0;
static PFbyte eint2InitFlag = 0;


PFEnStatus pfEint2Open(PFpCfgEint2 config)
{
	PFbyte index;
#ifdef PF_EINT2_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		EXTINT->EICRA |=config->mode <<EINT2_CH ;
	}	
	
	eint2MaxIntCount=config->maxCallbacks ;
// 	if(eint2MaxIntCount >0)
// 	eint2CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint2MaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<eint2MaxIntCount;index++)
	{
		if(config->callbackList!=0 )
		eint2CallBackList[index]=config->callbackList[index];
	}
	 eint2InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint2Enable(void)
{
#ifdef PF_EINT2_DEBUG
	if(eint2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT2_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint2Disable(void)
{
#ifdef PF_EINT2_DEBUG
	if(eint2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT2_CH);
	return enStatusSuccess;
}



PFEnStatus pfEint2AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT2_DEBUG
	if(eint2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint2MaxIntCount;index++)
	{
		if (eint2CallBackList[index]==0)
		{
			eint2CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint2RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT2_DEBUG
	if(eint2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint2MaxIntCount;index++)
	{
		if (eint2CallBackList[index]==callback)
		{
			eint2CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint2Close(void)
{
#ifdef PF_EINT2_DEBUG
	if(eint2InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT2_CH);
	//free(eint2CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT2_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint2MaxIntCount;index++)
	{
		if(eint2CallBackList[index] !=0)
		eint2CallBackList[index]();
		
	}
}
#endif		//#if (PF_USE_EINT2 == 1)

