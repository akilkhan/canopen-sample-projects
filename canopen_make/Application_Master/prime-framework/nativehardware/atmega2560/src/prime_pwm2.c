#include "prime_framework.h"
#if(PF_USE_PWM2 == 1)	
	#include "prime_pwm2.h"

static PFcallback pwm2CmpMatchACallback;
static PFcallback pwm2CmpMatchBCallback;
static PFcallback pwm2OverFlowCallback;
static PFbyte pwm2InitFlag=0;
static PFbyte pwm2SettingVal=0;
static PFbyte pwm2Value=0;


PFEnStatus pfPwm2Open(PFpCfgPwm2 config)
{
#ifdef PFPWM2_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
    if( (config->pwm2Mode<0) || ((config->pwm2Mode > enTimerCtcMode)))
	{
		return enStatusInvArgs;
	}
	
	if( (config->exMatchActionA < 0) || (config->exMatchActionA > 3) )
	{
		return enStatusInvArgs;
	}
	if( (config->exMatchActionB < 0) || (config->exMatchActionB > 3) )
	{
		return enStatusInvArgs;
	}
	if(config->cmpMatchACallback ==0)
	{
		return enStatusInvArgs;
	}
	if(config->cmpMatchBCallback ==0)
	{
		return enStatusInvArgs;
	}
	if(config->overFlowCallback ==0)
	{
		return enStatusInvArgs;
	}	
#endif

	if((config->pwm2Mode& 0x01)!=0)
	{
		PWM2_CHANNEL->TCCRA |=config->pwm2Mode;
		PWM2_CHANNEL->TCCRB |=(config->pwm2Mode &0x04)<<1;
	}
	
	PWM2_CHANNEL->OCRA =config->matchValueA;
	PWM2_CHANNEL->OCRB =config->matchValueB;
	
	if(config->exMatchActionA !=enPwm2ExtMatchCtrlNone)
	{
		PWM2_CHANNEL->TCCRA |=config->exMatchActionA <<6;
		GPIO_PORTB->DDR |=(1<<7);
	}		
	if(config->exMatchActionB!=enPwm2ExtMatchCtrlNone)
	{
		PWM2_CHANNEL->TCCRA |=config->exMatchActionB <<4;
		GPIO_PORTG->DDR |=(1<<5);
	}		
	if(config->cmpMatchACallback !=0)
	{
		pwm2CmpMatchACallback =config->cmpMatchACallback;
	}	
	if(config->cmpMatchBCallback !=0)
	{
		pwm2CmpMatchBCallback =config->cmpMatchBCallback;
	}	
	if(config->overFlowCallback !=0)
	{
		pwm2OverFlowCallback =config->overFlowCallback;
	}
	if(config->clockSource >=0 && config->clockSource <=7)
	{
		PWM2_CHANNEL->TCCRB |= config->clockSource;
	}
	if(config->interrupt !=enPwm2IntNone)
	{
		//	PWM2_CHANNEL->TIMSK |=config.interrupt;
		TIMSK_INT->TIMER0 |= config->interrupt;	
	}	
	PWM2_CHANNEL->TCNT = 0;
	pwm2InitFlag=1;
	return enStatusSuccess;
	
}

PFEnStatus pfPwm2Close(void)
{
#ifdef PFPWM2_DEBUG
	if(pwm2InitFlag ==0)
	{
		return enStatusNotConfigured;
	}
#endif
	PWM2_CHANNEL->TCCRB = 0x00;
	PWM2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
		
}

PFEnStatus pfPwm2Stop(void)
{
#ifdef PFPWM2_DEBUG
	if((pwm2InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	pwm2SettingVal=PWM2_CHANNEL->TCCRB;
	pwm2Value=PWM2_CHANNEL->TCNT;
	PWM2_CHANNEL->TCCRB = 0x00;
	return enStatusSuccess;

}

PFEnStatus pfPwm2Start(void)
{
	PFbyte val;
	
#ifdef PFPWM2_DEBUG
	if((pwm2InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif	
	if(pwm2SettingVal!=0)
	{
		PWM2_CHANNEL->TCCRB = pwm2SettingVal;
		PWM2_CHANNEL->TCNT = pwm2Value;
	}
	else
	{
		val =PWM2_CHANNEL->TCCRB;
		PWM2_CHANNEL->TCCRB = 0x00;
		PWM2_CHANNEL->TCCRB =val;
		PWM2_CHANNEL->TCNT = 0;
	}
	return	enStatusSuccess;	
	
}
PFEnStatus pfPwm2Reset(void)
{
#ifdef PFPWM2_DEBUG
	if((pwm2InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	PWM2_CHANNEL->TCNT = 0;
	return enStatusSuccess;
	
}
PFEnStatus pfPwm2ReadCount(PFdword *data)
{
#ifdef PFPWM2_DEBUG
	if((pwm2InitFlag ==0))
	{
		return enStatusNotConfigured;
	}
#endif
	*data =PWM2_CHANNEL->TCNT;
	return enStatusSuccess;
	
}

//ISR(TIMER0_COMPA_vect)
void INT_HANDLER(TIMER0_COMPA)(void)
//INT_HANDLER(TIMER0_COMPA)
{
	if(pwm2CmpMatchACallback !=0)
	pwm2CmpMatchACallback();
}


//ISR(TIMER0_COMPB_vect)
void INT_HANDLER(TIMER0_COMPB)(void)
//INT_HANDLER(TIMER0_COMPB)
{
	if(pwm2CmpMatchBCallback !=0)
	pwm2CmpMatchBCallback();
}


////ISR(TIMER0_OVF_vect)
void INT_HANDLER(TIMER0_OVF)(void)
//INT_HANDLER(TIMER0_OVF)
{
	if(pwm2OverFlowCallback !=0)
	pwm2OverFlowCallback();
}


PFEnStatus pfPwm2UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
	switch(regNum)
	{
		case 0:
		PWM2_CHANNEL->OCRA =regVal;
		break;
		
		case 1:
		PWM2_CHANNEL->OCRB =regVal;
		break;
		
		default:
			return enStatusInvArgs;
	}
	
	return enStatusSuccess;
}
#endif	// #if(PF_USE_PWM2)