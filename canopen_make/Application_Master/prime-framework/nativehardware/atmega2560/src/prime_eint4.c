#include "prime_framework.h"
#if (PF_USE_EINT4 == 1)
#include "prime_eint4.h"


static PFcallback eint4CallBackList[PF_EXT_INT_MAX_CALLBACK]={0};
static PFbyte eint4MaxIntCount=0;
static PFbyte eint4InitFlag = 0;

PFEnStatus pfEint4Open(PFpCfgEint4 config)
{
	PFbyte index;
#ifdef PF_EINT4_DEBUG
	if(config == 0)
	{
		return enStatusInvArgs;
	}
	if((config->mode > 3)|| (config->mode < enIntModeLowLevel))
	{
		return enStatusInvArgs;
	}
	if(config->callbackList == 0)
	{
		return enStatusInvArgs;
	}
	if(config->maxCallbacks < 0)
	{
		return enStatusInvArgs;
	}
#endif

	if(config->mode <=3)
	{
		EXTINT->EICRB |=config->mode <<(EINT4_CH-3);
	}	
	
	eint4MaxIntCount=config->maxCallbacks ;
// 	if(eint4MaxIntCount >0)
// 	eint4CallBackList =(PFcallback*)malloc(sizeof(PFcallback)*eint4MaxIntCount);
// 	else
// 	return enStatusNotSupported;
		
	for(index=0;index<eint4MaxIntCount;index++)
	{
		if(config->callbackList!=0 )
		eint4CallBackList[index]=config->callbackList[index];
	}
	 eint4InitFlag = enBooleanTrue ;
	
	return enStatusSuccess;
	
}


PFEnStatus pfEint4Enable(void)
{
#ifdef PF_EINT4_DEBUG
	if(eint4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK |= (1<<EINT4_CH);
	return enStatusSuccess;
}


PFEnStatus pfEint4Disable(void)
{
#ifdef PF_EINT4_DEBUG
	if(eint4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT4_CH);
	return enStatusSuccess;
}



PFEnStatus pfEint4AddCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT4_DEBUG
	if(eint4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
	if(callback == 0)
	{
		return enStatusInvArgs;
	}
#endif	
	for(index=0;index<=eint4MaxIntCount;index++)
	{
		if (eint4CallBackList[index]==0)
		{
			eint4CallBackList[index]=callback;
			break;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfEint4RemoveCallback(PFcallback callback)
{
	PFbyte index=0;
#ifdef PF_EINT4_DEBUG
	if(eint4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	
	for(index=0;index<=eint4MaxIntCount;index++)
	{
		if (eint4CallBackList[index]==callback)
		{
			eint4CallBackList[index]=0;
			break;
		}
	}
	return enStatusSuccess;
	
}


PFEnStatus pfEint4Close(void)
{
#ifdef PF_EINT4_DEBUG
	if(eint4InitFlag == 0)
	{
		return enStatusNotConfigured;
	}
#endif
	PERIPH_EIMSK->EIMSK &=~(1<<EINT4_CH);
	//free(eint4CallBackList);
	return enStatusSuccess;
}

void INT_HANDLER(EINT4_CHANNEL)(void)
{
	PFbyte index=0;
	
	for(index=0;index<=eint4MaxIntCount;index++)
	{
		if(eint4CallBackList[index] !=0)
		eint4CallBackList[index]();
		
	}
}
#endif		//#if (PF_USE_EINT4 == 1)

