/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework Onchip EEPROM driver for AT90CAN128.
 * 
 *
 * Review status: NO
 *
 */ 

#pragma once

#define EEPROM_CH				EEPROM
#define EEPROM_CHANNEL			PERIPH(EEPROM_CH)


typedef enum
{
	enEepromEraseNwrite,
	enEepromErase,
	enEepromWrite,	
}PFEnEEpromMode;

typedef struct
{
	PFEnEEpromMode mode;			
}PFCfgEeprom;

typedef PFCfgEeprom* PFpCfgEeprom;

/**
 * The function configures EEPROM with given settings.
 *
 * \param config configuration structure which contains the settings for EEPROM to be used.
 */
PFEnStatus pfEEpromOpen(PFpCfgEeprom config);


/**
 * The function reads multiple bytes from EEPROM.
 *
 * \param uiAddress address of EEPROM from where data is to be read
 *
 * \param data pointer to byte, in which function will fill number bytes actually read
 *
 * \param size Total number of bytes to read
 */
PFEnStatus pfEEpromRead(PFword uiAddress ,PFbyte *data,PFbyte size);

/**
 * The function reads multiple bytes from EEPROM.
 *
 * \param uiAddress address of EEPROM where data is to be written
 *
 * \param ucData pointer to byte,to the data to be written.
 */
PFEnStatus pfEEpromWriteByte(PFword uiAddress, PFbyte ucData);


PFEnStatus pfEEpromClose(void);

