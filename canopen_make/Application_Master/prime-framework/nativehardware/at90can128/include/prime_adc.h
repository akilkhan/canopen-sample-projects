/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework On-chip EEPROM driver for AT90CAN128.
 * 
 *
 * Review status: NO
 *
 */ 
#pragma once


#define ADC_CH				ADC
#define ADC_CHANNEL			PERIPH(ADC_CH)


/**
 * \defgroup PF_ADC_API ADC API
 * @{
 */ 

/** 	Enumeration for ADC trigger  source			*/
typedef enum
{
	enAdcNone,						/**< No trigger Source					*/
	enAdcStartNow,					/**< Single conversion	ADC mode		*/
	enAdcFreeRun,					/**< Free Running mode ADC conversion	*/
	enAdcAnalogueComp,				/**< Analog Comparator					*/
	enAdcExtInt,					/**< External Interrupt request			*/
	enAdcTimer0matchA,				/**< Timer/Counter0 match reg A			*/
	enAdcTimer0OverflowA,			/**< Timer/Counter0 overflow			*/
	enAdcTimer1matchB,				/**< Timer/Counter1 match reg B			*/
	enAdcTimer1OverflowB,			/**< Timer/Counter1 overflow			*/
	enAdcTimer1CapturEvent			/**< Timer/Counter1 overflow			*/
}PFEnAdcStart;

/** 	Enumeration for ADC reference voltage			*/

typedef enum
{
	enAdcArefSelect,				/**<  AREF, Internal VREF turned off*/
	enAdcAvccSelect,				/**<  AVCC with external capacitor at AREF pin*/
	enAdcInternal1_1Select,			/**< Internal 1.1V Voltage Reference with external capacitor at AREF pin */
	enAdcInternal2_6Select			/**< Internal 2.56V Voltage Reference with external capacitor at AREF pin */
}PFEnAdcRefSelect;


/** 	Enumeration for ADC Prescalar 			*/
typedef enum
{
	enAdcPrescaleDefault,			/**< To select To select Division Factor = 2*/
	enAdcPrescaleBy2,				/**< To select Division Factor = 2*/
	enAdcPrescaleBy4,				/**< To select Division Factor = 4*/
	enAdcPrescaleBy8,				/**< To select Division Factor = 8*/
	enAdcPrescaleBy16,				/**< To select Division Factor = 16*/
	enAdcPrescaleBy32,				/**< To select Division Factor = 32*/
	enAdcPrescaleBy64,				/**< To select Division Factor = 64*/
	enAdcPrescaleBy128				/**< To select Division Factor = 128*/
}PFEnAdcPreScale;

/** 	Enumeration for ADC channel  source			*/

typedef enum
{
	enAdcChannel_0 = 0,				/**< Selecting Adc Channel = 0 */
	enAdcChannel_1,					/**< Selecting Adc Channel = 1 */
	enAdcChannel_2,					/**< Selecting Adc Channel = 2 */
	enAdcChannel_3,					/**< Selecting Adc Channel = 3 */
	enAdcChannel_4,					/**< Selecting Adc Channel = 4 */
	enAdcChannel_5,					/**< Selecting Adc Channel = 5 */
	enAdcChannel_6,					/**< Selecting Adc Channel = 6 */
	enAdcChannel_7					/**< Selecting Adc Channel = 7 */
}PFEnAdcChannel;


/**		ADC configure Structure	*/
typedef struct
{
	PFEnAdcChannel			channel;				/**< Select channels to scan in bitshift format				*/
	PFEnAdcRefSelect		refVoltage;				/**< Select the refernce voltage							*/		
	PFEnAdcPreScale			prescaler;				/**< Prescaler for ADC clock								*/
	PFEnBoolean				resultLeftAdjust;		/**< Select the left or right adjustment for result			*/
	PFEnAdcStart			conversionMode;			/**< Set conversion mode for ADC							*/
	PFEnBoolean				interrupt;				/**< Select channel interrupts to enable in bitshift format	*/
	PFcallback				callback;				/**< Set callback for ADC interrupt							*/
}PFCfgAdc;

/**
 * \brief pointer to PFCfgAdc structure
 */
typedef PFCfgAdc* PFpCfgAdc;

/**
 * The function configures ADC with given settings.
 
 * \param config configuration structure which contains the settings for the ADC to be used.

 * \return ADC initialization status.
 */
PFEnStatus pfAdcOpen(PFpCfgAdc config);


/**
 * This function selects the given channel
 *
 *\param channel enumeration channel which contains all the channel info
 *
 *\return ADC channel selection status
 */
PFEnStatus pfAdcSelectChannel(PFEnAdcChannel channel);



/**
 * The function sets ADC channel interrupts.
 *
 * \return Interrupt setting status.
 */
PFEnStatus pfAdcEnableInterrupt(void);


/**
 * The function disable ADC channel interrupts.
 *
 * \return Interrupt setting status.
 */
PFEnStatus pfAdcDisableInterrupt(void);


/**
 * The function starts conversion in specified mode.
 * 
 * \param startMode channel interrupt to enable in bitshift format.
 *
 * \return Conversion mode setting status.
 */
PFEnStatus pfAdcStartBurstConversion(void);	

/**
 * The function starts single conversion immediately and returns conversion value.
 * 
 * \param value pointer to PFword data to which the ADC converted value will be written.
 *
 * \return conversion status.
 */
PFEnStatus pfAdcSingleConversion(PFEnAdcChannel channel,PFword* value);

/**
 * The function starts single conversion immediately and returns conversion value in milliVolts.
 * 
 * \param channel Channel number to start conversion for
 * \param value Pointer to PFdword data to which the ADC converted value will be written.
 *
 * \return conversion status.
 */
PFEnStatus pfAdcGetVoltageSingleConversion(PFbyte channel, PFdword* milliVolt);

/**
 * The function returns last conversion value.
 * 
 * \param value pointer to get conversion value. 
 *
 * \return Last value status.
 */
PFEnStatus pfAdcGetLastValue(PFword *value);


/**
 * The function disables ADC module.
 *
 * \return ADC disable status.
 */
PFEnStatus pfAdcClose(void);


/** @} */
