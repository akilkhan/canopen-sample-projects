/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework External Interrupt driver for AT90CAN128.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

/**
 * \defgroup PF_EXT_INT_API EXT INT API
 * @{
 */ 

/**		Enumeration for External Interrupt mode	*/

#define EINT4_CH			4
#define EINT4_CHANNEL		PF_CONCAT(EINT, EINT4_CH)



typedef enum{
	enIntModeLowLevel = 0,		/**< The low level of EINT4 generates an interrupt request						*/	
	enIntModToggle,				/**< Any edge of EINT4 generates asynchronously an interrupt request			*/	
	enIntModeFallingEdge,		/**< The falling edge of EINT4 generates asynchronously an interrupt request	*/	
	enIntModeRisingEdge			/**< The rising edge of EINT3 generates asynchronously an interrupt request		*/	
}PFEnEint4Mode;

/**		External interrupt	configure Structure	*/
typedef struct
{
	PFEnEint4Mode	mode;								/**< External interrupt mode									*/
	PFdword			maxCallbacks;						/**< Maximum number of callbacks allowed for tailchaining		*/
	PFcallback* callbackList;			/**< Pointer to array of callbacks to attach to interrupt		*/
}PFCfgEint4;

typedef PFCfgEint4* PFpCfgEint4;

/**
 * The function configures and enables External Interrupt with given settings.
 
 * \param config configuration structure which contains the settings for the external interrupt to be used.

 * \return External Interrupt status.
 */
PFEnStatus pfEint4Open(PFpCfgEint4 config);

/**
 * The function enables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint4Enable(void);

/**
 * The function disables External Interrupt 
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint4Disable(void);

/**
 * The function adds a callback to callback list if tailchaining is enabled.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return add callback status
 */
PFEnStatus pfEint4AddCallback(PFcallback callback);

/**
 * The function removes the specified callback from callback list.
 *
 * \param callback callback function to add in the callback list.
 *
 * \return remove callback status
 */
PFEnStatus pfEint4RemoveCallback(PFcallback callback);

/**
 * The function disables External Interrupt .
 *
 * \return External Interrupt status.
 */
PFEnStatus pfEint4Close(void);


/** @} */
