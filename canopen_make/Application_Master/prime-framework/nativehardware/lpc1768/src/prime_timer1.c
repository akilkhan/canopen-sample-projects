#include "prime_framework.h"

#if (PF_USE_TIMER1 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_timer1.h"


static PFcallback timer1Callback = 0;
static PFEnBoolean timer1Init = enBooleanFalse;
static PFCfgTimer1 timer1Cfg;

PFEnStatus pfTimer1Open(PFpCfgTimer1 config)
{
	PFbyte loop;
#if (PF_TIMER1_DEBUG ==1)
	CHECK_NULL_PTR(config);
	if(config->clkDiv > 3)
	{
		return enStatusInvArgs;
	}
	if( (config->timer1Mode == 3) || (config->timer1Mode > 5) )
	{
		return enStatusInvArgs;
	}
	if( (config->interrupt == enBooleanTrue) && (config->callback == 0) )
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_TIMER1_DEBUG ==1)	
	// power on timer1 module
	POWER_ON(TIMER1_CH);
	
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(TIMER1_CH), config->clkDiv);
	
	// set prescaler
	TIMER1_CHANNEL->PR = config->prescaler-1;
	
	// set timer1 counter mode, with counter pin selection
	TIMER1_CHANNEL->CTCR = config->timer1Mode;
	
	// set match register count
	TIMER1_CHANNEL->MR0 = config->matchValue[0];
	TIMER1_CHANNEL->MR1 = config->matchValue[1];
	TIMER1_CHANNEL->MR2 = config->matchValue[2];
	TIMER1_CHANNEL->MR3 = config->matchValue[3];
	
	for(loop = 0; loop < 4; loop++)
	{
		// set match action	
		TIMER1_CHANNEL->MCR |= ( (config->matchAction[loop] & 0x07) << (loop * 3));
		
		// set pin action for count match
		TIMER1_CHANNEL->EMR |= ( (config->exMatchAction[loop] & 0x03) << ((loop * 2) + 4));
	}
	
	timer1Callback = config->callback;
	
	if(config->interrupt != enBooleanFalse)
	{
		NVIC_EnableIRQ(IRQ_NUM(TIMER1_CH));
	}
	
	pfMemCopy(&timer1Cfg, config, sizeof(PFCfgTimer1));
	timer1Init = enBooleanTrue;
	return enStatusSuccess;
}

void pfTimer1Close(void)
{
	timer1Init = enBooleanFalse;
	// power off timer1 module
	POWER_OFF(TIMER1_CH);
}

PFEnStatus pfTimer1Start(void)
{
#if (PF_TIMER1_DEBUG ==1)
        CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG ==1)	
	TIMER1_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer1Stop(void)
{
#if (PF_TIMER1_DEBUG ==1)
	CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG ==1)	
	TIMER1_CHANNEL->TCR = 2;
	return enStatusSuccess;
}

PFEnStatus pfTimer1Reset(void)
{
#if (PF_TIMER1_DEBUG == 1)
	CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG ==1)	
	TIMER1_CHANNEL->TCR = 2;
	TIMER1_CHANNEL->TCR = 1;
	return enStatusSuccess;
}

PFEnStatus pfTimer1UpdateMatchRegister(PFbyte regNum, PFdword regVal)
{
#if (PF_TIMER1_DEBUG == 1)
	CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG == 1)	
	switch(regNum)
	{
		case 0:
			TIMER1_CHANNEL->MR0 = regVal;
			break;

		case 1:
			TIMER1_CHANNEL->MR1 = regVal;
			break;

		case 2:
			TIMER1_CHANNEL->MR2 = regVal;
			break;

		case 3:
			TIMER1_CHANNEL->MR3 = regVal;
			break;
		
		default:
			return enStatusInvArgs;
	}
	return enStatusSuccess;
}

PFEnStatus pfTimer1ReadCount(PFdword* count)
{
#if (PF_TIMER1_DEBUG == 1)
	CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG == 1)	
        *count = TIMER1_CHANNEL->TC;
	return enStatusSuccess;
}

PFEnStatus pfTimer1GetIntStatus(PFdword* status)
{
#if (PF_TIMER1_DEBUG == 1)
        CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG == 1)	
	*status = TIMER1_CHANNEL->IR;
	return enStatusSuccess;
}

PFEnStatus pfTimer1ClearIntStatus(PFdword intStatus)
{
#if (PF_TIMER1_DEBUG == 1)
	CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG == 1)	
	TIMER1_CHANNEL->IR = intStatus;
	return enStatusSuccess;
}

void TIMER1_INT_HANDLER(void)
{
	if(timer1Callback != 0)
	{
		timer1Callback();
	}
	TIMER1_CHANNEL->IR = TIMER1_CHANNEL->IR;
}

PFEnStatus pfTimer1IntDisable(void)
{
#if (PF_TIMER1_DEBUG ==1)
        CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG ==1)
	NVIC_EnableIRQ(IRQ_NUM(TIMER1_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer1IntEnable(void)
{
#if (PF_TIMER1_DEBUG ==1)
        CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG ==1)
	NVIC_DisableIRQ(IRQ_NUM(TIMER1_CH));
	return enStatusSuccess;
}

PFEnStatus pfTimer1GetTickFreq(PFdword* tickFreq)
{
	PFdword timerPclk;
#if (PF_TIMER1_DEBUG == 1)	
	CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG == 1)
	timerPclk = pfSysGetPclk(PCLK_DIV(TIMER1_CH));
	*tickFreq = timerPclk / (TIMER1_CHANNEL->PR + 1);
	return enStatusSuccess;
}

PFEnStatus pfTimer1SetTickFreq(PFdword tickFreq)
{
	PFdword f_pclk, prescale;
	PFdword pclk[4] = {25000000, 100000000, 50000000, 12500000}	;
	PFbyte pclkInd, done = 0;
#if (PF_TIMER1_DEBUG == 1)	
	CHECK_DEV_INIT(timer1Init);	
#endif	// #if (PF_TIMER1_DEBUG == 1)	
	for(pclkInd = 0; pclkInd < 4; pclkInd++)
	{
		prescale = pclk[pclkInd] / tickFreq;
		if ((tickFreq * prescale) == pclk[pclkInd])
		{
			done = 1;
			pfSysSetPclkDiv(PCLK_DIV(TIMER1_CH), (PFEnPclkDivider)pclkInd);
			TIMER1_CHANNEL->PR = prescale-1;
			break;
		}
	}
	
	if(done == 1)
	{
		return enStatusSuccess;
	}
	else
	{
		return enStatusNotSupported;
	}
}

PFEnStatus pfTimer1UpdateMatchControlRegister(PFbyte regNum,PFEnTimer1MatchAction matchAction)
{
#if (PF_TIMER1_DEBUG == 1)	
	if((regNum >3) || (matchAction > 7)) 
        return enStatusInvArgs;
#endif	// #if (PF_TIMER1_DEBUG == 1)
    
    TIMER1_CHANNEL->MCR |= ( (matchAction & 0x07) << (regNum * 3));
	return enStatusSuccess;
}

#endif	// #if (PF_USE_TIMER1 == 1)
