#include "prime_framework.h"

#if (PF_USE_RIT == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_rit.h"

static PFEnBoolean ritInit = enBooleanFalse;
static PFcallback ritCallback = 0;
static PFCfgRit ritCfg;

PFEnStatus pfRitOpen(PFpCfgRit config)
{
#if (PF_RIT_DEBUG == 1)
        CHECK_NULL_PTR(config);
	if( (config->clkDiv > 3) || (config->callback == 0) )
        {
		return enStatusInvArgs;
        }
#endif	// #if (PF_RIT_DEBUG == 1)	
	
	// power on RIT module
	POWER_ON(RIT_CH);
	// set PCLK clock divider
	pfSysSetPclkDiv(PCLK_DIV(RIT_CH), config->clkDiv);
	// set compare value
	RIT_CHANNEL->RICOMPVAL = config->compareValue;
	// set compare mask
	RIT_CHANNEL->RIMASK = config->compareMask;
	
	RIT_CHANNEL->RICTRL = 0;
	
	// halt timer when processer is halted 
	if(config->haltOnBreak)
	{
		RIT_CHANNEL->RICTRL |= BIT_MASK_2;
	}
	else
	{
		RIT_CHANNEL->RICTRL &= ~(BIT_MASK_2);
	}
	// reset timer on compare match
	if(config->resetOnMatch)
	{
		RIT_CHANNEL->RICTRL |= BIT_MASK_1;
	}
	else
	{
		RIT_CHANNEL->RICTRL &= ~(BIT_MASK_1);
	}
	// set callback
	ritCallback = config->callback;
	// enable interrupt
	NVIC_EnableIRQ(IRQ_NUM(RIT_CH));
	
	pfMemCopy(&ritCfg, config, sizeof(PFCfgRit));
	ritInit = enBooleanTrue;
	return enStatusSuccess;
}

void pfRitClose(void)
{
	RIT_CHANNEL->RICTRL	&= ~(BIT_MASK_3);			// disable RIT
	POWER_OFF(RIT_CH);
	ritInit = enBooleanFalse;
}

PFEnStatus pfRitStart(void)
{
#if (PF_RIT_DEBUG == 1)
        CHECK_DEV_INIT(ritInit);
#endif	// #if (PF_RIT_DEBUG == 1)		
	RIT_CHANNEL->RICTRL	|= BIT_MASK_3;
	return enStatusSuccess;
}

PFEnStatus pfRitStop(void)
{
#if (PF_RIT_DEBUG == 1)
        CHECK_DEV_INIT(ritInit);
#endif	// #if (PF_RIT_DEBUG == 1)		    
	RIT_CHANNEL->RICTRL	&= ~(BIT_MASK_3);
	return enStatusSuccess;
}

void RIT_INT_HANDLER(void)
{
	if(ritCallback != 0)
	{
		ritCallback();
	}
	
	// clear RIT interrupt flag
	RIT_CHANNEL->RICTRL |= BIT_MASK_1;
}

#endif	// #if (PF_USE_RIT == 1)
