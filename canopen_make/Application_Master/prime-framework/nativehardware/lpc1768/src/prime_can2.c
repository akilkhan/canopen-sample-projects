#include "prime_framework.h"
#if (PF_USE_CAN2 == 1)
#include "prime_utils.h"
#include "prime_sysClk.h"
#include "prime_canCommon.h"
#include "prime_can2.h"

#if(CAN2_USE_FIFO != 0)
#include "prime_fifo.h"
	#warning CAN2 FIFO is enabled for interrupt based communication
	#if( (CAN2_BUFFER_SIZE == 0) || ((CAN2_BUFFER_SIZE & (CAN2_BUFFER_SIZE - 1)) != 0) )
		#error CAN2_BUFFER_SIZE can2not be zero. CAN2_BUFFER_SIZE should be power of 2
	#endif
#endif	// #if(CAN2_USE_FIFO != 0)
	
#define CAN2_MESSAGE_SIZE			(sizeof(PFCanMessage))
#define pfCan2ResetMode()			pfCan2SetCommand(CAN2_MOD_RM, enBooleanTrue)	
#define pfCan2OperatingMode()		pfCan2SetCommand(CAN2_MOD_RM, enBooleanFalse)

static PFEnStatus pfCan2SetBaudrate(PFdword baudrate);
static PFEnStatus pfCan2WriteMessage(PFpCanMessage message);
static PFEnStatus pfCan2ReadMessage(PFpCanMessage message);
//static PFEnStatus pfCan2SetCommand(PFdword cmd, PFEnBoolean enable);

#if(CAN2_USE_FIFO != 0)
static PFEnStatus pfCan2PushRxMessage(PFpCanMessage msg);
static PFEnStatus pfCan2PopRxMessage(PFpCanMessage msg);
static PFEnStatus pfCan2PushTxMessage(PFpCanMessage msg);
static PFEnStatus pfCan2PopTxMessage(PFpCanMessage msg);
	
static void pfCan2Buffer1TxDefaultCallback(void);
static void pfCan2Buffer2TxDefaultCallback(void);
static void pfCan2Buffer3TxDefaultCallback(void);
static void pfCan2RxDefaultCallback(void);
#endif	// #if(CAN2_USE_FIFO != 0)	
	


/* Values of bit time register for different baudrates
   NT = Nominal bit time = TSEG1 + TSEG2 + 3
   SP = Sample point     = ((TSEG2 +1) / (TSEG1 + TSEG2 + 3)) * 100%
                                            SAM,  SJW, TSEG1, TSEG2, NT,  SP */
static const PFdword CAN2_BIT_TIME[] = {           0, /*             not used             */
                                           0, /*             not used             */
                                           0, /*             not used             */
                                           0, /*             not used             */
                                  0x0001C000, /* 0+1,  3+1,   1+1,   0+1,  4, 75% */
                                           0, /*             not used             */
                                  0x0012C000, /* 0+1,  3+1,   2+1,   1+1,  6, 67% */
                                           0, /*             not used             */
                                  0x0023C000, /* 0+1,  3+1,   3+1,   2+1,  8, 63% */
                                           0, /*             not used             */
                                  0x0025C000, /* 0+1,  3+1,   5+1,   2+1, 10, 70% */
                                           0, /*             not used             */
                                  0x0036C000, /* 0+1,  3+1,   6+1,   3+1, 12, 67% */
                                           0, /*             not used             */
                                           0, /*             not used             */
                                  0x0048C000, /* 0+1,  3+1,   8+1,   4+1, 15, 67% */
                                  0x0049C000, /* 0+1,  3+1,   9+1,   4+1, 16, 69% */
                                };

//static PFEnBoolean can2TxBuffFree[3] = {enBooleanFalse, enBooleanFalse, enBooleanFalse};
static PFEnBoolean can2ChInit = enBooleanFalse;
static PFEnCan2Interrupt can2Int;
#if(CAN2_USE_FIFO != 0)
static PFbyte can2RxBuffer[CAN2_BUFFER_SIZE];		// CAN2 transmit buffer
static PFbyte can2TxBuffer[CAN2_BUFFER_SIZE];		// CAN2 receive buffer
static PFFifo can2TxFifo;							// CAN2 transmit fifo structure
static PFFifo can2RxFifo;							// CAN2 receive fifo structure
#endif	// #if(CAN2_USE_FIFO  != 0)
static PFCfgCan2 can2Cfg;

PFEnStatus pfCan2Open(PFpCfgCan2 config)
{
#if (PF_CAN2_DEBUG == 1)
	// Validate config pointer
	CHECK_NULL_PTR(config);
	
	// Validate peripheral clock divider, baudrate value and error callback pointer
	if( (config->clkDiv > 3) || (config->baudrate > CAN2_MAX_BAUDRATE) || ((config->interrupt != enCan2IntNone) && IS_PTR_NULL(config->errCallback)) )
	{
		return enStatusInvArgs;
	}
#if(CAN2_USE_FIFO == 0)	
	// Validate TX and RX callback pointers
	if(((config->interrupt & enCan2IntRx) != 0) && IS_PTR_NULL(config->rxCallback))
	{
		return enStatusInvArgs;
	}
	if( ((config->interrupt & enCan2IntTx) != 0) && IS_PTR_NULL(config->txCallback))
	{
		return enStatusInvArgs;
	}
#endif // #if(CAN2_USE_FIFO == 0)		
#endif	// #if (PF_CAN2_DEBUG == 1)
	
	// power on CAN2 module
	POWER_ON(CAN2_CH);
	
	// set PCLK divider
	pfSysSetPclkDiv(PCLK_DIV(CAN2_CH), config->clkDiv);
	pfSysSetPclkDiv(PCLK_DIV_ACF, config->clkDiv);
	
	// Put CAN2 controller in reset mode
	CAN2_CHANNEL->MOD = CAN_MOD_RM;
	
	// Bypass acceptance filter
	PERIPH_CANAF->AFMR = 2;
	
	// Disable all interrupts 
	CAN2_CHANNEL->IER = 0;
	// Clear status register
	CAN2_CHANNEL->GSR = 0;

	// Set baudrate
	pfCan2SetBaudrate(config->baudrate);
	
#if(CAN2_USE_FIFO != 0)	
	// initialize fifo 
	pfFifoInit(&can2TxFifo, can2TxBuffer, CAN2_BUFFER_SIZE);
	pfFifoInit(&can2RxFifo, can2RxBuffer, CAN2_BUFFER_SIZE);
    pfCanSetCallback(CAN2_CONTROLLER, enCanBuff1TxCallback, pfCan2Buffer1TxDefaultCallback);
    pfCanSetCallback(CAN2_CONTROLLER, enCanBuff2TxCallback, pfCan2Buffer2TxDefaultCallback);
    pfCanSetCallback(CAN2_CONTROLLER, enCanBuff1TxCallback, pfCan2Buffer3TxDefaultCallback);
    pfCanSetCallback(CAN2_CONTROLLER, enCanRxCallback, pfCan2RxDefaultCallback);
#else
	// intialize callbacks
	pfCanSetCallback(2,enCanBuff1TxCallback,config->txCallback);
	pfCanSetCallback(2,enCanBuff2TxCallback,config->txCallback);
	pfCanSetCallback(2,enCanBuff3TxCallback,config->txCallback);
	pfCanSetCallback(2,enCanRxCallback,config->rxCallback);
#endif // #if(CAN2_USE_FIFO == 0)

	// Enable Tx and Rx interrupt
	if(config->interrupt != enCan2IntNone)
	{
		can2Int = config->interrupt;
		pfCanSetCallback(CAN2_CONTROLLER, enCanErrCallback, config->errCallback);
		if((config->interrupt & enCan2IntRx) != 0)
		{	
			CAN2_CHANNEL->IER |= BIT_MASK_0;
		}
		if((config->interrupt & enCan2IntTx) != 0)
		{	
			CAN2_CHANNEL->IER |= (BIT_MASK_1 | BIT_MASK_9 | BIT_MASK_10);
		}
		NVIC_EnableIRQ(CAN_IRQn);
	}
	
	// Put CAN2 controller in operating mode
	CAN2_CHANNEL->MOD = 0;
	
	// Transmitter ready for transmission
	while((CAN2_CHANNEL->GSR & CAN_GSR_TBS) == 0); 
	
//	can2TxBuffFree[0] = enBooleanTrue;
//	can2TxBuffFree[1] = enBooleanTrue;
//	can2TxBuffFree[2] = enBooleanTrue;
	
	pfMemCopy(&can2Cfg, config, sizeof(PFCfgCan2));
	can2ChInit = enBooleanTrue;
	pfCanSetChannelStatus(1,enBooleanTrue);
	return enStatusSuccess;
}

PFEnStatus pfCan2Close(void)
{
	POWER_OFF(CAN2_CH);
	can2ChInit = enBooleanFalse;
	pfCanSetChannelStatus(1,enBooleanFalse);
	return enStatusSuccess;
}

static PFEnStatus pfCan2SetBaudrate(PFdword baudrate)
{
	PFdword can2Pclk, result, nominal_time;
#if (PF_CAN2_DEBUG == 1)
	if(baudrate > CAN2_MAX_BAUDRATE)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_CAN2_DEBUG == 1)	
	
	can2Pclk = pfSysGetPclk(PCLK_DIV(CAN2_CH));
	nominal_time = 10;
	/* Prepare value appropriate for bit time register */
	result  = (can2Pclk / (nominal_time * baudrate)) - 1;
	result &= 0x000003FF;
	result |= CAN2_BIT_TIME[nominal_time];

	CAN2_CHANNEL->BTR  = result;                           /* Set bit timing */
	
	return enStatusSuccess;
}


PFEnStatus pfCan2Write(PFpCanMsgHeader msgHeader,PFbyte* data, PFdword size)
{
	PFEnStatus status;
	PFdword index=0,remainingBytes=0,cpByte=0;
	PFCanMessage message;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(msgHeader);
	CHECK_NULL_PTR(data);
#endif	// #if (PF_CAN2_DEBUG == 1)	
	message.id = msgHeader->id;
	message.frameFormat = msgHeader->frameFormat;
	message.remoteFrame = msgHeader->remoteFrame;
	remainingBytes = size;
	
	if((can2Int & enCan2IntTx) != 0)
	{
	#if(CAN2_USE_FIFO !=0)
		if(size > ((CAN2_BUFFER_SIZE/CAN2_MESSAGE_SIZE) * 8))
			return enStatusNoMem;
		while(remainingBytes != 0)
		{
			if(remainingBytes >= 8)
				cpByte = 8;
			else
				cpByte =remainingBytes;
			
			pfMemCopy(message.data,&data[index],cpByte);
			message.length = cpByte;
			remainingBytes -= cpByte;
			index +=cpByte;
			pfCan2PushTxMessage(&message);
		}
		pfCan2PopTxMessage(&message);
		status = pfCan2WriteMessage(&message);
	#else		//user Callback
		if(size >= 8)
			cpByte = 8;
		else
			cpByte = size;
		pfMemCopy(message.data,&data[index],cpByte);
		message.length = cpByte;
		status = pfCan2WriteMessage(&message);
	#endif
	}
	//Polling Data Transmission
	else
	{
		while(remainingBytes != 0)
		{
			if(remainingBytes >= 8)
				cpByte = 8;
			else
				cpByte =remainingBytes;
			
			pfMemCopy(message.data,&data[index],cpByte);
			message.length = cpByte;
			remainingBytes -= cpByte;
			index +=cpByte;
			status = pfCan2WriteMessage(&message);
			if(status != enStatusSuccess)
				return status;
			while((CAN2_CHANNEL->SR & CAN_SR_TCS1) == 0);
			while((CAN2_CHANNEL->SR & CAN_SR_TCS2) == 0);
			while((CAN2_CHANNEL->SR & CAN_SR_TCS3) == 0);

		}
	}	
	return status;
}

PFEnStatus pfCan2Read(PFpCanMessage message)
{
	PFEnStatus status;
	PFdword buffCount;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(message);
#endif	// #if (PF_CAN2_DEBUG == 1)	
	
#if(CAN2_USE_FIFO != 0)	
	if((can2Int & enCan2IntRx) != 0)
	{
		status = pfCan2GetRxBufferCount(&buffCount);
	#if (PF_CAN2_DEBUG == 1)
		if(status != enStatusSuccess)
		{
			return status;
		}
	#endif	// #if (PF_CAN2_DEBUG == 1)	
		if(buffCount != 0)
		{
			pfCan2PopRxMessage(message);
			return enStatusSuccess;
		}
		else
		{
			return enStatusError;
		}
	}	
#endif	// #if(CAN2_USE_FIFO !=0)
	status = pfCan2ReadMessage(message);
	return status;
}

static PFEnStatus pfCan2WriteMessage(PFpCanMessage message)
{
	PFdword can2Header = 0;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	if(IS_PTR_NULL(message))
	{
		return enStatusInvArgs;
	}	
#endif	// #if (PF_CAN2_DEBUG == 1)	
	
	can2Header = (message->length << 16);
	if(message->remoteFrame == enBooleanTrue)
	{
		can2Header |= BIT_MASK_30;
	}
	if(message->frameFormat == enCanFrameExtended)
	{
		can2Header |= BIT_MASK_31;
	}

	while(1)
	{
		// check status for transmit buffer 1
		if((CAN2_CHANNEL->SR & CAN_SR_TCS1) == CAN_SR_TCS1)
		{
//			can2TxBuffFree[0] = enBooleanFalse;
			CAN2_CHANNEL->TFI1 = can2Header;
			CAN2_CHANNEL->TID1 = message->id;
			CAN2_CHANNEL->TDA1 = message->data[0] | (message->data[1] << 8) | (message->data[2] << 16) | (message->data[3] << 24);
			CAN2_CHANNEL->TDB1 = message->data[4] | (message->data[5] << 8) | (message->data[6] << 16) | (message->data[7] << 24);
			pfCan2SetCommand(CAN_CMR_TR | CAN_CMR_STB1, enBooleanTrue);
			break;
		}
		else if((CAN2_CHANNEL->SR & CAN_SR_TCS2) == CAN_SR_TCS2)		// check status for transmit buffer 2
		{
//			can2TxBuffFree[1] = enBooleanFalse;
			CAN2_CHANNEL->TFI2 = can2Header;
			CAN2_CHANNEL->TID2 = message->id;
			CAN2_CHANNEL->TDA2 = message->data[0] | (message->data[1] << 8) | (message->data[2] << 16) | (message->data[3] << 24);
			CAN2_CHANNEL->TDB2 = message->data[4] | (message->data[5] << 8) | (message->data[6] << 16) | (message->data[7] << 24);
			pfCan2SetCommand(CAN_CMR_TR | CAN_CMR_STB2, enBooleanTrue);
			break;
		}
		else if((CAN2_CHANNEL->SR & CAN_SR_TCS3) == CAN_SR_TCS3)		// check status for transmit buffer 3
		{
//			can2TxBuffFree[2] = enBooleanFalse;
			CAN2_CHANNEL->TFI3 = can2Header;
			CAN2_CHANNEL->TID3 = message->id;
			CAN2_CHANNEL->TDA3 = message->data[0] | (message->data[1] << 8) | (message->data[2] << 16) | (message->data[3] << 24);
			CAN2_CHANNEL->TDB3 = message->data[4] | (message->data[5] << 8) | (message->data[6] << 16) | (message->data[7] << 24);
			pfCan2SetCommand(CAN_CMR_TR | CAN_CMR_STB3, enBooleanTrue);
			break;
		}
	}	//while(1)	
	return enStatusSuccess;
}

static PFEnStatus pfCan2ReadMessage(PFpCanMessage message)
{
	PFbyte loop, byteCount = 0;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(message);
#endif	// #if (PF_CAN2_DEBUG == 1)	
	// Check if there is any unread message in the receive buffer
	if((CAN2_CHANNEL->SR & CAN_SR_RBS) == 0)
	{ 
		return enStatusError;
	}
	// Check data length of received message
	message->length = (CAN2_CHANNEL->RFS >> 16) & 0x0F;
	if(message->length > 8)
	{
		message->length = 8;
	}
	
	// Check if the received message is remote frame request
	if((CAN2_CHANNEL->RFS & CAN_RFS_RTR) != 0)
	{
		message->remoteFrame = enBooleanTrue;
	}
	else
	{
		message->remoteFrame = enBooleanFalse;
	}
	
	// Check frame format for received message
	if((CAN2_CHANNEL->RFS & CAN_RFS_FF) != 0)
	{
		message->frameFormat = enCanFrameExtended;
	}
	else
	{
		message->frameFormat = enCanFrameStandard;
	}	
	
	message->id = CAN2_CHANNEL->RID;
	
	for(loop = 0; (loop < 4)&&(byteCount < message->length); loop++)
	{
		message->data[loop] = CAN2_CHANNEL->RDA >> (loop*8);
		byteCount++;
	}
	for(loop = 0; (loop < 4)&&(byteCount < message->length); loop++)
	{
		message->data[loop + 4] = CAN2_CHANNEL->RDB >> (loop*8);
		byteCount++;
	}
	while(byteCount < 8)
	{
		message->data[byteCount] = 0;
		byteCount++;
	}
		
	pfCan2SetCommand(CAN_CMR_RRB, enBooleanTrue);
	
	return enStatusSuccess;
}

PFEnStatus pfCan2SetCommand(PFdword cmd, PFEnBoolean enable)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
#endif	// #if (PF_CAN2_DEBUG == 1)	

	if(enable == enBooleanTrue)
	{
		CAN2_CHANNEL->CMR |= cmd;
	}
	else
	{
		CAN2_CHANNEL->CMR &= ~(cmd);
	}
	return enStatusSuccess;
}

PFEnBoolean pfCan2CheckStatus(PFEnCan2Status param)
{
	if((CAN2_CHANNEL->GSR & param) != 0)
	{
		return enBooleanTrue;
	}
	else
	{
		return enBooleanFalse;
	}
}

PFEnStatus pfCan2GetIntStatus(PFdword* status)
{
	#if (PF_CAN2_DEBUG == 1)
	    CHECK_NULL_PTR(status);
	#endif
	*status = CAN2_CHANNEL->ICR;
	return enStatusSuccess;
}

PFEnStatus pfCan2GetCtrlStatus(PFdword* status)
{
	#if (PF_CAN2_DEBUG == 1)
	    CHECK_NULL_PTR(status);
	#endif
	*status = CAN2_CHANNEL->SR;
	return enStatusSuccess;
}

PFEnStatus pfCan2GetTxErrCounter(PFbyte* errCount)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(errCount);
#endif	// #if (PF_CAN2_DEBUG == 1)	

	*errCount = CAN2_CHANNEL->GSR >> 24;
	return enStatusSuccess;
}

PFEnStatus pfCan2GetRxErrCounter(PFbyte* errCount)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(errCount);
#endif	// #if (PF_CAN2_DEBUG == 1)	

	*errCount = CAN2_CHANNEL->GSR >> 16;
	return enStatusSuccess;
}

#if(CAN2_USE_FIFO != 0)
static PFEnStatus pfCan2PushRxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN2_DEBUG == 1)		

	msgPtr = (PFbyte*)msg;
	for(loop = 0; loop < CAN2_MESSAGE_SIZE; loop++)
	{
		pfFifoPush(&can2RxFifo, *(msgPtr + loop));
	}
	return enStatusSuccess;
}

static PFEnStatus pfCan2PopRxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN2_DEBUG == 1)		
	
	msgPtr = (PFbyte*)msg;
	if(pfFifoIsEmpty(&can2RxFifo) == enBooleanTrue)
	{
		return enStatusError;
	}
	for(loop = 0; loop < CAN2_MESSAGE_SIZE; loop++)
	{
		*(msgPtr + loop) = pfFifoPop(&can2RxFifo);
	}
	return enStatusSuccess;
}

static PFEnStatus pfCan2PushTxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN2_DEBUG == 1)	

	msgPtr = (PFbyte*)msg;
	for(loop = 0; loop < CAN2_MESSAGE_SIZE; loop++)
	{
		pfFifoPush(&can2TxFifo, *(msgPtr + loop));
	}
	return enStatusSuccess;
}

static PFEnStatus pfCan2PopTxMessage(PFpCanMessage msg)
{
	PFbyte loop;
	PFbyte* msgPtr;
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(msg);
#endif	// #if (PF_CAN2_DEBUG == 1)	
	
	msgPtr = (PFbyte*)msg;
	if(pfFifoIsEmpty(&can2TxFifo) == enBooleanTrue)
	{
		return enStatusError;
	}
	for(loop = 0; loop < CAN2_MESSAGE_SIZE; loop++)
	{
		*(msgPtr + loop) = pfFifoPop(&can2TxFifo);
	}
	
	return enStatusSuccess;
}

PFEnStatus pfCan2GetRxBufferSize(PFdword* size)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_CAN2_DEBUG == 1)    
    *size = (pfFifoLength(&can2RxFifo) / sizeof(PFCanMessage));
    return enStatusSuccess;
}

PFEnStatus pfCan2GetRxBufferCount(PFdword* count)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
	CHECK_NULL_PTR(count);
#endif	// #if (PF_CAN2_DEBUG == 1)
	*count = (can2RxFifo.count/sizeof(PFCanMessage));
	return enStatusSuccess;
}

PFEnStatus pfCan2RxBufferFlush(void)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
#endif	// #if (PF_CAN2_DEBUG == 1)    
	pfFifoFlush(&can2RxFifo);
	return enStatusSuccess;
}

PFEnStatus pfCan2GetTxBufferSize(PFdword* size)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
    CHECK_NULL_PTR(size);
#endif	// #if (PF_CAN2_DEBUG == 1)    
    *size = (pfFifoLength(&can2TxFifo) / sizeof(PFCanMessage));
    return enStatusSuccess;
}

PFEnStatus pfCan2GetTxBufferCount(PFdword* count)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
    CHECK_NULL_PTR(count);
#endif	// #if (PF_CAN2_DEBUG == 1)    
    *count = (can2TxFifo.count / sizeof(PFCanMessage));
    return enStatusSuccess;
}

PFEnStatus pfCan2TxBufferFlush(void)
{
#if (PF_CAN2_DEBUG == 1)
	CHECK_DEV_INIT(can2ChInit);
#endif	// #if (PF_CAN2_DEBUG == 1)    
	pfFifoFlush(&can2TxFifo);
	return enStatusSuccess;
}



static void pfCan2Buffer1TxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan2SetCommand(CAN_CMR_STB1, enBooleanFalse);
//    can2TxBuffFree[0] = enBooleanTrue;
	
    if(pfFifoIsEmpty(&can2TxFifo) == enBooleanFalse)
    {
		pfCan2PopTxMessage(&msg);
		pfCan2WriteMessage(&msg);
    }
}

static void pfCan2Buffer2TxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan2SetCommand(CAN_CMR_STB2, enBooleanFalse);
//    can2TxBuffFree[1] = enBooleanTrue;
	
    if(pfFifoIsEmpty(&can2TxFifo) == enBooleanFalse)
    {
		pfCan2PopTxMessage(&msg);
		pfCan2WriteMessage(&msg);
    }
}

static void pfCan2Buffer3TxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan2SetCommand(CAN_CMR_STB3, enBooleanFalse);
//    can2TxBuffFree[2] = enBooleanTrue;
	
    if(pfFifoIsEmpty(&can2TxFifo) == enBooleanFalse)
    {
		pfCan2PopTxMessage(&msg);
		pfCan2WriteMessage(&msg);
    }
}

static void pfCan2RxDefaultCallback(void)
{
    PFCanMessage msg;
    pfCan2ReadMessage(&msg);
    pfCan2PushRxMessage(&msg);
}

#endif	// #if(CAN2_USE_FIFO != 0)

#endif	// #if (PF_USE_CAN2 == 1)
