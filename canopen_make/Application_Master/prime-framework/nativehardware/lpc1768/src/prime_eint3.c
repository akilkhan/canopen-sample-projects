#include "prime_framework.h"

#if (PF_USE_EINT3 == 1)

#include "prime_utils.h"
#include "prime_sysClk.h"
#if (EINT3_CH == GPIO_INT_CH)
#include "prime_gpio.h"
#endif  // #if (EINT3_CH == GPIO_INT_CH)
#include "prime_eint3.h"

static PFEnBoolean eint3Init = enBooleanFalse;
static PFcallback eint3CallbackList[EINT3_MAX_CALLBACK] = {0};
static PFEnBoolean eint3Enable = enBooleanFalse;
static PFCfgEint3 eint3Cfg;

#if (EINT3_CH == GPIO_INT_CH)
static PFEnBoolean gpioIntInit = enBooleanFalse;
static PFCfgGpioInt gpioIntList[GPIO_MAX_INTERRUPTS];
static PFEnBoolean gpioIntBusy[GPIO_MAX_INTERRUPTS] = {enBooleanFalse};
static PFEnBoolean gpioIntEnable = enBooleanFalse;
#endif  // #if (EINT3_CH == GPIO_INT_CH)


PFEnStatus pfEint3Open(PFpCfgEint3 config)
{
	PFdword loop;
#if (PF_EINT3_DEBUG == 1)
	CHECK_NULL_PTR(config);
	if(config->mode > 0x04)
	{
		return enStatusInvArgs;
	}
	if(config->callbackCount > EINT3_MAX_CALLBACK)
	{
		return enStatusInvArgs;
	}
#endif	// #if (PF_EINT3_DEBUG == 1)

	// disable interrupt before configuring
	NVIC_DisableIRQ(IRQ_NUM(EINT3_CHANNEL));
	
	// set interrupt mode and polarity
	PERIPH_SC->EXTMODE |= (config->mode >> 1) << EINT3_CH;
	PERIPH_SC->EXTPOLAR |= (config->mode & 0x01) << EINT3_CH;
	
	// set callbacks
	if(config->callbackList != 0)
	{
		for(loop = 0; loop < config->callbackCount; loop++)
		{
			eint3CallbackList[loop] = config->callbackList[loop];
		}
	}
	
	pfMemCopy(&eint3Cfg, config, sizeof(PFCfgEint3));
	eint3Init = enBooleanTrue;
	
	return enStatusSuccess;
}

PFEnStatus pfEint3Enable(void)
{
#if (PF_EINT3_DEBUG == 1)
	CHECK_DEV_INIT(eint3Init);
#endif	// #if (PF_EINT3_DEBUG == 1)

	NVIC_EnableIRQ(IRQ_NUM(EINT3_CHANNEL));
	eint3Enable = enBooleanTrue;
	return enStatusSuccess;	
}

PFEnStatus pfEint3Disable(void)
{
#if (PF_EINT3_DEBUG == 1)
	CHECK_DEV_INIT(eint3Init);
#endif	// #if (PF_EINT3_DEBUG == 1)
	
	NVIC_DisableIRQ(IRQ_NUM(EINT3_CHANNEL));
	eint3Enable = enBooleanFalse;
	return enStatusSuccess;	
}

PFEnStatus pfEint3AddCallback(PFcallback callback)
{
	PFEnStatus status = enStatusError;
	PFbyte index;

#if (PF_EINT3_DEBUG == 1)
	CHECK_DEV_INIT(eint3Init);
	CHECK_NULL_PTR(callback);
#endif	// #if (PF_EINT3_DEBUG == 1)
	
	if(eint3Enable == enBooleanTrue)
	{
		NVIC_DisableIRQ(IRQ_NUM(EINT3_CHANNEL));
	}
	
	for(index = 0; index < EINT3_MAX_CALLBACK; index++)
	{
		if(eint3CallbackList[index] == 0)
		{
			eint3CallbackList[index] = callback;
			status = enStatusSuccess;
			break;
		}
	}
	
	if(eint3Enable == enBooleanTrue)
	{
		NVIC_EnableIRQ(IRQ_NUM(EINT3_CHANNEL));
	}
	return status;
}

PFEnStatus pfEint3RemoveCallback(PFcallback callback)
{
	PFEnStatus status = enStatusError;
	PFbyte index;
#if (PF_EINT3_DEBUG == 1)
	CHECK_DEV_INIT(eint3Init);
	CHECK_NULL_PTR(callback);
#endif	// #if (PF_EINT3_DEBUG == 1)
	
	if(eint3Enable == enBooleanTrue)
    {
		NVIC_DisableIRQ(IRQ_NUM(EINT3_CHANNEL));
	}
	for(index = 0; index < EINT3_MAX_CALLBACK; index++)
	{
		if(eint3CallbackList[index] == callback)
		{
			eint3CallbackList[index] = 0;
			status = enStatusSuccess;
			break;
		}
	}
	
	if(eint3Enable == enBooleanTrue)
    {
		NVIC_EnableIRQ(IRQ_NUM(EINT3_CHANNEL));
	}
	return status;
}

PFEnStatus pfEint3Close(void)
{
	NVIC_DisableIRQ(IRQ_NUM(EINT3_CHANNEL));
	eint3Enable = enBooleanFalse;
	eint3Init = enBooleanFalse;
	return enStatusSuccess;
}

#if (EINT3_CH == GPIO_INT_CH)
PFEnStatus pfGpioIntOpen(PFbyte* id, PFpCfgGpioInt config)
{
	PFbyte index;
#if (PF_EINT3_DEBUG == 1)    
    CHECK_NULL_PTR(config);
    CHECK_NULL_PTR(id);
    if( (config->gpioPortNo != GPIO_PORT_0) && (config->gpioPortNo != GPIO_PORT_2) )
    {
        return enStatusInvArgs;
    }
#endif  // #if (PF_EINT3_DEBUG == 1)    
	for (index = 0; index < GPIO_MAX_INTERRUPTS; index++)
	{
		if(gpioIntBusy[index] == enBooleanFalse)
		{
			*id = index;
			gpioIntBusy[index] = enBooleanTrue;
			break;
		}
	}
	if(index == GPIO_MAX_INTERRUPTS)
	{
		return enStatusError;
	}

	pfMemCopy(&gpioIntList[index], config, sizeof(PFCfgGpioInt));
	
	switch(config->gpioPortNo)
	{
		case GPIO_PORT_0:
			if(config->mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnF |= config->gpioIntPin;
			}
			if(config->mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnR |= config->gpioIntPin;
			}
			break;
                
		case GPIO_PORT_2:
			if(config->mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnF |= config->gpioIntPin;
			}
			if(config->mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnR |= config->gpioIntPin;
			}
			break;    
	}

	gpioIntEnable = enBooleanTrue;
	if(eint3Enable != enBooleanTrue)
	{
		pfEint3Enable();
	}
	
	gpioIntInit = enBooleanTrue;
	return enStatusSuccess;
}

PFEnStatus pfGpioIntEnable(PFbyte id)
{
#if (PF_EINT3_DEBUG == 1)
	CHECK_DEV_INIT(gpioIntInit);
#endif	// #if (PF_EINT3_DEBUG == 1)
	switch(gpioIntList[id].gpioPortNo)
	{
		case GPIO_PORT_0:
			if(gpioIntList[id].mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnF |= gpioIntList[id].gpioIntPin;
			}
			if(gpioIntList[id].mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnR |= gpioIntList[id].gpioIntPin;
			}
			break;
			
		case GPIO_PORT_2:
			if(gpioIntList[id].mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnF |= gpioIntList[id].gpioIntPin;
			}
			if(gpioIntList[id].mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnR |= gpioIntList[id].gpioIntPin;
			}
			break;    
	}
	return enStatusSuccess;	
}

PFEnStatus pfGpioIntDisable(PFbyte id)
{
#if (PF_EINT3_DEBUG == 1)
	CHECK_DEV_INIT(gpioIntInit);
#endif	// #if (PF_EINT3_DEBUG == 1)
	switch(gpioIntList[id].gpioPortNo)
	{
		case GPIO_PORT_0:
			if(gpioIntList[id].mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnF &= ~gpioIntList[id].gpioIntPin;
			}
			if(gpioIntList[id].mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO0IntEnR &= ~gpioIntList[id].gpioIntPin;
			}
			break;
			
		case GPIO_PORT_2:
			if(gpioIntList[id].mode & enGpioIntModeFallingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnF &= ~gpioIntList[id].gpioIntPin;
			}
			if(gpioIntList[id].mode & enGpioIntModeRisingEdge)
			{
				PERIPH_GPIOINT->IO2IntEnR &= ~gpioIntList[id].gpioIntPin;
			}
			break;    
	}
	return enStatusSuccess;	
}

PFEnStatus pfGpioIntClose(PFbyte id)
{
#if (PF_EINT3_DEBUG == 1)
	if(id >= GPIO_MAX_INTERRUPTS)
    {
		return enStatusInvArgs;
    }
	if(gpioIntBusy[id] == enBooleanFalse)
    {
		return enStatusInvArgs;
    }
#endif	// #if (PF_EINT3_DEBUG == 1)
	
    pfGpioIntDisable(id);   
        
	gpioIntBusy[id] = enBooleanFalse;
	pfMemSet(&gpioIntList[id],PF_NULL,sizeof(PFCfgGpioInt));
	
	return enStatusSuccess;
}

#endif  // #if (EINT3_CH == GPIO_INT_CH)

void EINT3_HANDLER(void)
{
	PFbyte index;
#if (EINT3_CH == GPIO_INT_CH)
	if(gpioIntEnable == enBooleanTrue)
	{
		//Port0
		if(PERIPH_GPIOINT->IntStatus & BIT_MASK_0)
		{
			for(index = 0; index < GPIO_MAX_INTERRUPTS; index++)
			{
				if( (gpioIntList[index].gpioPortNo == GPIO_PORT_0) && (gpioIntBusy[index] == enBooleanTrue) )
				{
					if( (PERIPH_GPIOINT->IO0IntStatF & gpioIntList[index].gpioIntPin) || (PERIPH_GPIOINT->IO0IntStatR & gpioIntList[index].gpioIntPin) ) 
					{
						PERIPH_GPIOINT->IO0IntClr |= gpioIntList[index].gpioIntPin;
						if(gpioIntList[index].callback != 0)
						{
							gpioIntList[index].callback();
						}
					}
				}    
			}
		}
		//Port2
		if(PERIPH_GPIOINT->IntStatus & BIT_MASK_2)
		{
			for(index = 0; index < GPIO_MAX_INTERRUPTS; index++)
			{
				if( (gpioIntList[index].gpioPortNo == GPIO_PORT_2) && (gpioIntBusy[index] == enBooleanTrue) )
				{
					if( (PERIPH_GPIOINT->IO2IntStatF & gpioIntList[index].gpioIntPin) || (PERIPH_GPIOINT->IO2IntStatR & gpioIntList[index].gpioIntPin) ) 
					{
						PERIPH_GPIOINT->IO2IntClr |= gpioIntList[index].gpioIntPin;
						if(gpioIntList[index].callback != 0)
						{
							gpioIntList[index].callback();
						}
					}
				}    
			}
		}
	}
#endif //GPIO Int
	// clear interrupt flag
	PERIPH_SC->EXTINT = (1 << EINT3_CH);
    if(eint3Enable == enBooleanTrue)
    {
        for(index = 0; index < EINT3_MAX_CALLBACK; index++)
        {
			if(eint3CallbackList[index] != 0)
			{
				eint3CallbackList[index]();
			}
        }
    }
}

#endif	// #if (PF_USE_EINT3 == 1)
