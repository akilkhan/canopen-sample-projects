/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework VCNL4000 driver
 * 
 *
 * Review status: NO
 *
 */

#pragma once
/** 
 * \defgroup PF_VCNL4000_API PF VCNL4000 API
 * @{
 */

/** maximum number of VCNL4000 devices supported */
#define VCNL_MAX_DEVICE_CONNECTED               2

/** VCNL4000 module address */
#define VCNL_SLAVE_ADDR                         0x26

/** VCNL4000 Register addresses */
#define VCNL_REG_CMD                            0x80
#define VCNL_REG_PROD_ID                        0x81
#define VCNL_REG_NOT_IN_USE                     0x82
#define VCNL_REG_LED_CURRENT                    0x83
#define VCNL_REG_AMB_PARAMETER                  0x84
#define VCNL_REG_AMB_DATA_H                     0x85
#define VCNL_REG_AMB_DATA_L                     0x86
#define VCNL_REG_PROX_DATA_H                    0x87
#define VCNL_REG_PROX_DATA_L                    0x88
#define VCNL_REG_LED_SIG_FREQ                   0x89
#define VCNL_REG_LED_MOD_TIMING                 0x8A

/** VCNL Command register values */
#define VCNL_AMBIENCE_DATA_READY                0x40
#define VCNL_PROXIMITY_DATA_READY               0x20
#define VCNL_START_AMBIENCE_CONV                0x10
#define VCNL_START_PROXIMITY_CONV               0x08

/** enumeration for Proximity IR led Current */
typedef enum{
    enProxIRledCurrent_0mA = 0,		/**< IR LED current for proximity measurement  = 0 mA	*/
    enProxIRledCurrent_10mA, 		/**< IR LED current for proximity measurement = 10 mA	*/
	enProxIRledCurrent_20mA, 		/**< IR LED current for proximity measurement = 20 mA	*/
	enProxIRledCurrent_30mA,		/**< IR LED current for proximity measurement = 30 mA	*/
    enProxIRledCurrent_40mA, 		/**< IR LED current for proximity measurement = 40 mA	*/
	enProxIRledCurrent_50mA, 		/**< IR LED current for proximity measurement = 50 mA	*/
	enProxIRledCurrent_60mA,		/**< IR LED current for proximity measurement = 60 mA	*/
    enProxIRledCurrent_70mA, 		/**< IR LED current for proximity measurement = 70 mA	*/
	enProxIRledCurrent_80mA, 		/**< IR LED current for proximity measurement = 80 mA	*/
	enProxIRledCurrent_90mA,		/**< IR LED current for proximity measurement = 90 mA	*/
    enProxIRledCurrent_100mA,		/**< IR LED current for proximity measurement = 100 mA	*/
	enProxIRledCurrent_110mA, 		/**< IR LED current for proximity measurement = 110 mA	*/
	enProxIRledCurrent_120mA,		/**< IR LED current for proximity measurement = 120 mA	*/
    enProxIRledCurrent_130mA, 		/**< IR LED current for proximity measurement = 130 mA	*/
	enProxIRledCurrent_140mA, 		/**< IR LED current for proximity measurement = 140 mA	*/
	enProxIRledCurrent_150mA,		/**< IR LED current for proximity measurement = 150 mA	*/
    enProxIRledCurrent_160mA, 		/**< IR LED current for proximity measurement = 160 mA	*/
	enProxIRledCurrent_170mA, 		/**< IR LED current for proximity measurement = 170 mA	*/
	enProxIRledCurrent_180mA,		/**< IR LED current for proximity measurement = 180 mA	*/
    enProxIRledCurrent_190mA, 		/**< IR LED current for proximity measurement = 190 mA	*/
	enProxIRledCurrent_200mA		/**< IR LED current for proximity measurement = 200 mA	*/
}PFEnProxIRledCurrent;

/** enumeration for Averaging samples per measurement of Ambient light */
typedef enum{
    enAmbAvgSample_0 = 0,		/**< No.of averaging samples per ambient light measurement = 0	*/
    enAmbAvgSample_2,			/**< No.of averaging samples per ambient light measurement = 2	*/
    enAmbAvgSample_4,			/**< No.of averaging samples per ambient light measurement = 4	*/
    enAmbAvgSample_8,			/**< No.of averaging samples per ambient light measurement = 8	*/
    enAmbAvgSample_16,			/**< No.of averaging samples per ambient light measurement = 16	*/
    enAmbAvgSample_32,			/**< No.of averaging samples per ambient light measurement = 32	*/
    enAmbAvgSample_64,			/**< No.of averaging samples per ambient light measurement = 64	*/
    enAmbAvgSample_128			/**< No.of averaging samples per ambient light measurement = 128*/
}PFEnAmbAvgSamples;

/** enumeration for Proximity IR led Modulation frequency selection */
typedef enum{
    enProxIRledFreq_390_625KHz = 0,		/**< IR LED modulation frequency = 390.625 KHz		*/
    enProxIRledFreq_781_25KHz,          /**< IR LED modulation frequency = 781.25 KHz		*/
    enProxIRledFreq_1_5625MHz,			/**< IR LED modulation frequency = 1.5625 MHz		*/
    enProxIRledFreq_3_125MHz 			/**< IR LED modulation frequency = 3.215 MHz		*/
}PFEnProxIRledFreq;

/** enumeration for selection of Modulation Delay Time for Proximity IR led signal */
typedef enum{
    enModDelayTime_0 = 0,				/**< Modulation delay time = 0		*/
    enModDelayTime_1,					/**< Modulation delay time = 1		*/
    enModDelayTime_2,					/**< Modulation delay time = 2		*/
    enModDelayTime_3,					/**< Modulation delay time = 3		*/
    enModDelayTime_4,                   /**< Modulation delay time = 4		*/
    enModDelayTime_5,					/**< Modulation delay time = 5		*/
    enModDelayTime_6,					/**< Modulation delay time = 6		*/
    enModDelayTime_7					/**< Modulation delay time = 7		*/
}PFEnModDelayTime;

/** enumeration for selection of Modulation Dead Time for Proximity IR led signal */
typedef enum{
    enModDeadTime_0 = 0,				/**< Modulation dead time = 0		*/
    enModDeadTime_1,					/**< Modulation dead time = 1		*/
    enModDeadTime_2,					/**< Modulation dead time = 2		*/
    enModDeadTime_3,					/**< Modulation dead time = 3		*/
    enModDeadTime_4,					/**< Modulation dead time = 4		*/
    enModDeadTime_5,					/**< Modulation dead time = 5		*/
    enModDeadTime_6,					/**< Modulation dead time = 6		*/
    enModDeadTime_7						/**< Modulation dead time = 7		*/
}PFEnModDeadTime;

/** VCNL4000 Configuration structure */
typedef struct{
    PFEnProxIRledCurrent proxIrLedCurrent;                  /**< Set IR LED current */
    PFEnBoolean ambContinousSensing;                        /**< Select single shot or continuous ambient light measurement mode */
    PFEnBoolean ambOffsetCompensation;                      /**< Turn on/off offset compensation */
    PFEnAmbAvgSamples ambAvgSamples;                        /**< Select number of samples for averaging per measurement */
    PFEnProxIRledFreq proxIrLedFreq;                        /**< Select modulation frequency of IR led signal */
    PFEnModDelayTime modDelayTime;                          /**< Modulation delay time between signal transmit and start sensing */
    PFEnModDeadTime modDeadTime;                            /**< Modulation dead time 		*/
	PFEnStatus (*i2cWrite)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size);	/**< Pointer to I2C write function	*/
	PFEnStatus (*i2cRead)(PFbyte* data, PFEnBoolean master, PFbyte slaveAddr, PFword size, PFword* readBytes);	/**< Pointer to I2C read function	*/
}PFCfgVcnl;

/** Pointer to structure PFCfgVcnl */
typedef PFCfgVcnl* PFpCfgVcnl;

/**
 * To initialize VCNL4000 sensor module
 *
 * \param devId, pointer to copy id/id's assigned by driver to VCNL/VCNL's
 * \param sensorConfig, pointer to config structure to configure VCNL4000
 * \param deviceCount, number of VCNL4000 d to be initialized
 *
 * \return status of initialize
 * 
 * \note Function needs I2C to be initialized before calling this function
 */
PFEnStatus pfVcnlOpen(PFbyte* devId, PFpCfgVcnl sensorConfig, PFbyte deviceCount);

/**
 * To close VCNL4000 with id devId
 *
 * \param devId Pointer to specify id allocated to driver
 *
 * \return status of VCNL4000 close operation
 */
PFEnStatus pfA3977Close(PFbyte* devId);

/**
 * To get Product Revision ID of VCNL4000 sensor module
 *
 * \param devId pointer to specify id assigned to driver
 * \param prodId pointer to copy Product ID of VCNL4000 sensor module
 *
 * \return status of read product ID 
 */
PFEnStatus pfVcnlGetProductId(PFbyte* devId, PFbyte* prodId);

/**
 * To get Ambient light reading in Lux (lx)  after single conversion,
 * suitable for single measurement mode
 *
 * \param devId pointer to specify id assigned to driver
 * \param ambience, pointer to copy Ambient light measurement result
 *
 * \return status of read Ambient light result
 */
PFEnStatus pfVcnlGetAmbience(PFbyte* devId, PFword* ambience); 

/**
 * To get Proximity measurement reading after single conversion,
 * suitable for single measurement mode
 *
 * \param devId pointer to specify id assigned to driver
 * \param proximity pointer to copy proximity measurement result
 *
 * \return status of read proximity measurement result
 */
PFEnStatus pfVcnlGetProximity(PFbyte* devId, PFword* proximity);  

/**
 * To get last Ambient light reading, in Lux (lx) suitable for continuous measurement mode,
 * in single conversion mode function gives same value for each call
 *
 * \param devId pointer to specify id assigned to driver
 * \param ambience pointer to copy last Ambient light measurement result 
 *
 * \return status of read Ambient light result
 */
PFEnStatus pfVcnlGetLastAmbience(PFbyte* devId, PFword* ambience); 

/**
 * To get last Proximity measurement reading, suitable for continuous measurement mode,
 * in single conversion mode function gives same value for each call
 *
 * \param devId pointer to specify id assigned to driver
 * \param proximity pointer to copy last proximity measurement result
 *
 * \return status of read proximity measurement result
 */
PFEnStatus pfVcnlGetLastProximity(PFbyte* devId, PFword* proximity);

/**
 * To set Proximity IR led current
 *
 * \param devId pointer to specify id assigned to driver
 * \param proxIRledCurrent Proximity IR led Current value to be set
 *
 * \return status of set Proximity IR led current
 */
PFEnStatus pfVcnlSetProxIRledCurrent(PFbyte* devId, PFEnProxIRledCurrent proxIRledCurrent);

/**
 * To set number of sample for averaging per measurement
 *
 * \param devId pointer to specify id assigned to driver
 * \param avgSamples ambient light measurement mode to be set
 *
 * \return status of set averaging
 */
PFEnStatus pfVcnlSetAvgSamples(PFbyte* devId, PFEnAmbAvgSamples avgSamples);

/** @} */

