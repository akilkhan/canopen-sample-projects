#include "prime_framework.h"
#include "prime_config.h"
#include "prime_gpio.h"
#include "prime_a3977.h"
#include "prime_l6470.h"
//#include "prime_servo.h"
#include "prime_xLynxCb.h"
#include "prime_stepper.h"

typedef enum{
	enStepperLynx = 0,
	enStepperSmartlynx,
	enLynx,
	enSmartlynx
}PFEnStepperType;

static PFbyte lynxCount=0;         // number of lynx connected
static PFbyte smartlynxCount=0;    // number of smartlynx connected
static PFbyte stepperIds[MAX_STEPPER_SUPPORTED]={0};
static PFEnBoolean stepperInitFlags[MAX_STEPPER_SUPPORTED]={0};
static PFEnStepperType stepperType[MAX_STEPPER_SUPPORTED]={0};

PFEnStatus pfLynxOpen(PFbyte* devId, PFpCfgStepperA3977 config, PFbyte deviceCount)
{
	volatile PFbyte i=0, j=0;
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;;
	if(config == NULL)
		return enStatusInvArgs;;
	if( (lynxCount+deviceCount+smartlynxCount) > MAX_STEPPER_SUPPORTED )
		return enStatusInvArgs;;
#endif	// PF_STEPPER_DEBUG

	status = pfA3977Open(devId, config, deviceCount);
	if(status == enStatusSuccess)
	{
		for(i=0; i<deviceCount; ++i)
		{
			for(j=0; j<MAX_STEPPER_SUPPORTED; ++j)
			{
				if(stepperInitFlags[j]==enBooleanFalse)
				{
					stepperInitFlags[j]=enBooleanTrue;
					stepperIds[j]=devId[i];
					devId[i]=j;
					stepperType[j]=enLynx;
					++lynxCount;
					break;
				}
			}
		}
	}

	return status;
}

PFEnStatus pfSmartlynxOpen(PFbyte* devId, PFpCfgStepperL6470 config, PFbyte deviceCount)
{
	volatile PFbyte i=0, j=0;
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;;
	if(config == NULL)
		return enStatusInvArgs;;
	if( (lynxCount+deviceCount+smartlynxCount) > MAX_STEPPER_SUPPORTED )
		return enStatusInvArgs;;
#endif	// PF_STEPPER_DEBUG

	status = pfL6470open(devId, config, deviceCount);
	if(status == enStatusSuccess)
	{
		for(i=0; i<deviceCount; ++i)
		{
			for(j=0; j<MAX_STEPPER_SUPPORTED; ++j)
			{
				if(stepperInitFlags[j]==enBooleanFalse)
				{
					stepperInitFlags[j]=enBooleanTrue;
					stepperIds[j]=devId[i];
					devId[i]=j;
					stepperType[j]=enSmartlynx;
					++lynxCount;
					break;
				}
			}
		}
	}

	return status;
}

PFEnStatus pfStepperLynxOpen(PFbyte* devId, PFpCfgXlynxCbStepperLynx config, PFbyte deviceCount)
{
	volatile PFbyte i=0, j=0;
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;;
	if(config == NULL)
		return enStatusInvArgs;;
	if( (lynxCount+deviceCount+smartlynxCount) > MAX_STEPPER_SUPPORTED )
		return enStatusInvArgs;;
#endif	// PF_STEPPER_DEBUG

	status = pfXlynxCbLynxOpen(devId, config, deviceCount);
	if(status == enStatusSuccess)
	{
		for(i=0; i<deviceCount; ++i)
		{
			for(j=0; j<MAX_STEPPER_SUPPORTED; ++j)
			{
				if(stepperInitFlags[j]==enBooleanFalse)
				{
					stepperInitFlags[j]=enBooleanTrue;
					stepperIds[j]=devId[i];
					devId[i]=j;
					stepperType[j]=enStepperLynx;
					++lynxCount;
					break;
				}
			}
		}
	}

	return status;
}

PFEnStatus pfStepperSmartlynxOpen(PFbyte* devId, PFpCfgXlynxCbStepperSmartlynx config, PFbyte deviceCount)
{
	volatile PFbyte i=0, j=0;
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;;
	if(config == NULL)
		return enStatusInvArgs;;
	if( (smartlynxCount+deviceCount+lynxCount) > MAX_STEPPER_SUPPORTED )
		return enStatusInvArgs;;
#endif	// PF_STEPPER_DEBUG

	status = pfXlynxCbSmartlynxOpen(devId, config, deviceCount);
	if(status == enStatusSuccess)
	{
		for(i=0; i<deviceCount; ++i)
		{
			for(j=0; j<MAX_STEPPER_SUPPORTED; ++j)
			{
				if(stepperInitFlags[j]==enBooleanFalse)
				{
					stepperInitFlags[j]=enBooleanTrue;
					stepperIds[j]=devId[i];
					devId[i]=j;
					stepperType[j]=enStepperSmartlynx;
					++smartlynxCount;
					break;
				}
			}
		}
	}

	return status;
}

PFEnStatus pfStepperClose(PFbyte* devId)
{
	volatile PFbyte j=0;
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorClose(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470close(&stepperIds[*devId]);
				 break;
		
		case enLynx	:	
				 status = pfA3977Close(&stepperIds[*devId]);
				 break;
		
		default :	
				 return enStatusInvArgs;;
				 break;
	}
	
	if(status == enStatusSuccess)
	{
		stepperInitFlags[*devId]=enBooleanFalse;
		switch(stepperType[*devId])
		{
			case enStepperSmartlynx :
			case enSmartlynx :
					 --smartlynxCount;
					 break;
			
			case enStepperLynx :
			case enLynx	:	
					 --lynxCount;
					 break;
			
			default :	
					 return enStatusInvArgs;;
					 break;
		}
	}

	return status;
}

PFEnStatus pfStepperResetDevice(PFbyte *devId)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorResetDevice(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470resetDevice(&stepperIds[*devId]);
				 break;
		
		case enLynx	:	
				 status = pfA3977ResetDevice(&stepperIds[*devId]);
				 break;
		
		default :	
				 return enStatusInvArgs;;
				 break;
	}
	
	return status;
}

PFEnStatus pfStepperResetPosition(PFbyte *devId)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorResetPosition(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470resetPosition(&stepperIds[*devId]);
				 break;
		
		case enLynx	:	
				 status = pfA3977ResetPosition(&stepperIds[*devId]);
				 break;
		
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperGoHomePosition(PFbyte *devId)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorGoHomePosition(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470GoHomePosition(&stepperIds[*devId]);
				 break;
		
		case enLynx	:	
				 status = pfA3977GoHomePosition(&stepperIds[*devId]);
				 break;
		
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperSoftStop(PFbyte *devId)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorSoftStop(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470softStop(&stepperIds[*devId]);
				 break;
		
		case enLynx	:	
				 status = pfA3977softStop(&stepperIds[*devId]);
				 break;
		
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperHardStop(PFbyte *devId)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorHardStop(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470hardStop(&stepperIds[*devId]);
				 break;
		
		case enLynx	:	
				 status = pfA3977hardStop(&stepperIds[*devId]);
				 break;
		
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperMove(PFbyte *devId, PFbyte dir, PFdword steps, PFfloat* speed)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
//pfUartWrite("\r\n in stepper move", 18);
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorMove(&stepperIds[*devId], dir, steps, speed);
				 break;
		
		case enSmartlynx :
				 status = pfL6470move(&stepperIds[*devId], dir, steps, *speed);
				 break;
		
		case enLynx	:	
				 status = pfA3977Move(&stepperIds[*devId], dir, steps, *speed);
				 break;
		
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperSetFullStepSpeed(PFbyte* devId, PFfloat* speed)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
				 status = pfXlynxCbMotorSetFullStepSpeed(&stepperIds[*devId], speed);
				 break;
		
		case enSmartlynx :
				 status = pfL6470setFullStepSpeed(&stepperIds[*devId], *speed);
				 break;
		
		case enLynx	:
		case enStepperLynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;
				 break;
	}
	
	return status;
}

PFEnStatus pfStepperRun(PFbyte* devId, PFbyte dir, PFfloat* speed)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 status = pfXlynxCbMotorRun(&stepperIds[*devId], dir, speed);
				 break;
		
		case enSmartlynx :
				 status = pfL6470run(&stepperIds[*devId], dir, *speed);
				 break;
		
		case enLynx	:	
				 status = pfA3977run(&stepperIds[*devId], dir, *speed);
				 break;
		
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperDeviceReady(PFbyte* devId, PFEnBoolean* status)
{
	volatile PFEnStatus stat;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
				 stat = pfXlynxCbMotorDeviceReady(&stepperIds[*devId], status);
				 break;
		
		case enSmartlynx :
				 stat = pfL6470deviceReady(&stepperIds[*devId], status);
				 break;
		
		case enLynx	:	
				 stat = pfA3977DeviceReady(&stepperIds[*devId], status);
				 break;
		
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return stat;
}

PFEnStatus pfStepperSetAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
			 status = pfXlynxCbMotorSetAccelerationProfile(&stepperIds[*devId], acceleration, deceleration);
			 break;
		
		case enSmartlynx :
			 status = pfL6470setAccelerationProfile(&stepperIds[*devId], *acceleration, *deceleration);
			 break;
		
		case enLynx	:	
			 status = pfA3977SetAccelerationProfile(&stepperIds[*devId], *acceleration, *deceleration);
			 break;
		
		default :	
			 return enStatusInvArgs;
			 break;
	}

	return status;
}

PFEnStatus pfStepperGetAccelerationProfile(PFbyte* devId, PFfloat* acceleration, PFfloat* deceleration)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
			 status = pfXlynxCbMotorGetAccelerationProfile(&stepperIds[*devId], acceleration, deceleration);
			 break;
		
		case enSmartlynx :
			 status = pfL6470getAccelerationProfile(&stepperIds[*devId], acceleration, deceleration);
			 break;
		
		case enLynx	:	
			 status = pfA3977getAccelerationProfile(&stepperIds[*devId], acceleration, deceleration);
			 break;
		
		default :	
			 return enStatusInvArgs;
			 break;
	}

	return status;
}

PFEnStatus pfStepperGetPosition(PFbyte* devId, PFsdword* position)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
			 status = pfXlynxCbMotorGetPosition(&stepperIds[*devId], position);
			 break;
		
		case enSmartlynx :
			 status = pfL6470getCurrentPosition(&stepperIds[*devId], position);
			 break;
		
		case enLynx	:	
			 status = pfA3977GetCurrentPosition(&stepperIds[*devId], position);
			 break;
		
		default :	
			 return enStatusInvArgs;
			 break;
	}

	return status;
}

PFEnStatus pfStepperSleep(PFbyte* devId, PFEnBoolean sleepState)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperLynx :
				 status = pfXlynxCbMotorSleep(&stepperIds[*devId], sleepState);
				 break;
		
		case enLynx	:
				 status = pfA3977Sleep(&stepperIds[*devId], sleepState);
				 break;
		
		case enStepperSmartlynx :
		case enSmartlynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperSetStepClockMode(PFbyte* devId, PFbyte dir)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
				 status = pfXlynxCbMotorSetStepClockMode(&stepperIds[*devId], dir);
				 break;
		
		case enSmartlynx :
				 status = pfL6470setStepClockMode(&stepperIds[*devId], dir);
				 break;
		
		case enLynx	:
		case enStepperLynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperTakeSingleStep(PFbyte* devId)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
				 status = pfXlynxCbMotorTakeSingleStep(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470takeSingleStep(&stepperIds[*devId]);
				 break;
		
		case enLynx	:
		case enStepperLynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperGetSpeed(PFbyte* devId, PFfloat* speed)
{
	volatile PFEnStatus status;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
		case enStepperLynx :
			 status = pfXlynxCbMotorGetSpeed(&stepperIds[*devId], speed);
			 break;
		
		case enSmartlynx :
			 status = pfL6470getSpeed(&stepperIds[*devId], speed);
			 break;
		
		case enLynx	:	
			 status = pfA3977getSpeed(&stepperIds[*devId], speed);
			 break;
		
		default :	
			 return enStatusInvArgs;
			 break;
	}

	return status;
}

PFEnStatus pfStepperGoMark(PFbyte* devId)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
				 status = pfXlynxCbMotorGoMark(&stepperIds[*devId]);
				 break;
		
		case enSmartlynx :
				 status = pfL6470goMark(&stepperIds[*devId]);
				 break;
		
		case enLynx	:
		case enStepperLynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperGoUntil(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir, PFfloat* speed)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
				 status = pfXlynxCbMotorGoUntil(&stepperIds[*devId], act, dir, speed);
				 break;
		
		case enSmartlynx :
				 status = pfL6470goUntil(&stepperIds[*devId], act, dir, *speed);
				 break;
		
		case enLynx	:
		case enStepperLynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperReleseSW(PFbyte* devId, PFEnL6470actionForSwitchPress act, PFbyte dir)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
				 status = pfXlynxCbMotorReleseSW(&stepperIds[*devId], act, dir);
				 break;
		
		case enSmartlynx :
				 status = pfL6470releseSW(&stepperIds[*devId], act, dir);
				 break;
		
		case enLynx	:
		case enStepperLynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;
				 break;
	}

	return status;
}

PFEnStatus pfStepperSetMinimumSpeed(PFbyte* devId, PFfloat* speed)
{
	volatile PFEnStatus status;
	volatile PFbyte i=0;

#ifdef PF_STEPPER_DEBUG
	if(devId == NULL)
		return enStatusInvArgs;
	if(stepperInitFlags[*devId]==enBooleanFalse)
		return enStatusNotConfigured;
#endif	// PF_STEPPER_DEBUG
	
	switch(stepperType[*devId])
	{
		case enStepperSmartlynx :
				 status = pfXlynxCbMotorSetMinimumSpeed(&stepperIds[*devId], speed);
				 break;
		
		case enSmartlynx :
				 status = pfL6470setMinimumSpeed(&stepperIds[*devId], *speed);
				 break;
		
		case enLynx	:
		case enStepperLynx :
				 return enStatusNotSupported;
				 break;
				 
		default :	
				 return enStatusInvArgs;;
				 break;
	}

	return status;
}



