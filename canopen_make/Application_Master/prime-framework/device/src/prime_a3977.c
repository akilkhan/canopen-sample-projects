#include "prime_framework.h"
#include "prime_config.h"
#include "prime_utils.h"
#include "prime_delay.h"
#include "prime_gpio.h"
#include "prime_a3977.h"
#include "math.h"

typedef enum{
	enA3977stateStop = 0,
	enA3977stateAcceleration,
	enA3977stateRunning,
	enA3977stateDeceleration
}PFEnA3977currentState;

typedef struct{
	PFbyte a3977DeviceReady;
	PFbyte a3977decelerateFlag;
	PFbyte a3977reAccelerateFlag;
	PFbyte a3977inFullStepModeFlag;
	PFbyte a3977RunFlag;
}PFa3977Flags;

typedef struct{
	PFchar sign;
	PFbyte stepFactor;
	float a3977speed;
	PFbyte a3977Direction;
	PFEnA3977currentState a3977currentState;
	PFdword timerFrequency;
	PFsdword a3977position;
	PFsdword res;
	PFsdword minCount;
	PFsdword newCount;
	PFsdword prevCount;
	PFsdword nextMinCount;
	PFsdword nu_of_steps;
	PFsdword enterFullStepMode;
	PFsdword decelerationStart;
	PFsdword a3977InitialCountForAcc;
	PFsdword a3977InitialCountForDec;
}PFa3977Variables;

static PFCfgStepperA3977 a3977Config[A3977_MAX_NO_OF_LYNX_CONNECTED];
static PFEnBoolean a3977Init[A3977_MAX_NO_OF_LYNX_CONNECTED] = {enBooleanFalse};
static PFbyte a3977DeviceCount=0;

static PFa3977Variables a3977Variables[A3977_MAX_NO_OF_LYNX_CONNECTED];
static PFa3977Flags a3977Flags[A3977_MAX_NO_OF_LYNX_CONNECTED];

PFEnStatus pfA3977Open(PFbyte* id, PFpCfgStepperA3977 config, PFbyte deviceCount)
{
	volatile float temp = 0.0;
	volatile PFbyte i=0, j=0;
	volatile PFEnStatus status;

#if (PF_A3977_DEBUG == 1)
	if(id == NULL)
		return enStatusInvArgs;
	if(config == NULL)
		return enStatusInvArgs;
#endif	// #if (PF_A3977_DEBUG == 1)

	for(i=0; i<deviceCount; ++i)
	{
		for(j=0; j<A3977_MAX_NO_OF_LYNX_CONNECTED; ++j)
		{
			if(a3977Init[j] == enBooleanFalse)
			{
				pfGpioPinsSet(config[i].resetGpio.port, config[i].resetGpio.pin);
				pfGpioPinsSet(config[i].sleepGpio.port, config[i].sleepGpio.pin);
				
				switch(config[i].stepResolution)
				{
					case enA3977StepResolutionFull:
						pfGpioPinsClear(config[i].ms1Gpio.port, config[i].ms1Gpio.pin);
						pfGpioPinsClear(config[i].ms2Gpio.port, config[i].ms2Gpio.pin);
						a3977Variables[j].stepFactor = 8;
						break;
					
					case enA3977StepResolutionHalf:
						pfGpioPinsSet(config[i].ms1Gpio.port, config[i].ms1Gpio.pin);
						pfGpioPinsClear(config[i].ms2Gpio.port, config[i].ms2Gpio.pin);
						a3977Variables[j].stepFactor = 4;
						break;
					
					case enA3977StepResolutionQuarter:
						pfGpioPinsClear(config[i].ms1Gpio.port, config[i].ms1Gpio.pin);
						pfGpioPinsSet(config[i].ms2Gpio.port, config[i].ms2Gpio.pin);
						a3977Variables[j].stepFactor = 2;
						break;
					
					case enA3977StepResolutionEighth:
						pfGpioPinsSet(config[i].ms1Gpio.port, config[i].ms1Gpio.pin);
						pfGpioPinsSet(config[i].ms2Gpio.port, config[i].ms2Gpio.pin);
						a3977Variables[j].stepFactor = 1;
						break;
				
					default:
						return enStatusInvArgs;
				}
				
				pfMemCopy(&a3977Config[j], &config[i], sizeof(PFCfgStepperA3977));
				
				status = config[i].pfTimerGetTickFreq(&a3977Variables[j].timerFrequency);
				if(status != enStatusSuccess)
					return status;
				
				//C0 calculation for acceleration
				temp = 2.0/( (config[i].acceleration)*((float)(1<<config[i].stepResolution)) );
				temp = 0.676 * ((float)a3977Variables[j].timerFrequency) * (sqrt(temp));
				a3977Variables[j].a3977InitialCountForAcc = (PFdword)temp;

				//C0 calculation for deceleration
				temp = 2.0/( (config[i].deceleration)*((float)(1<<config[i].stepResolution)) );
				temp = 0.676 * ((float)a3977Variables[j].timerFrequency) * (sqrt(temp));
				a3977Variables[j].a3977InitialCountForDec = (PFdword)temp;
				
				a3977Init[j] = enBooleanTrue;
				a3977Flags[j].a3977DeviceReady = enBooleanTrue;
				++a3977DeviceCount;
				id[i]=j;
				break;
			}
			
			if(j == A3977_MAX_NO_OF_LYNX_CONNECTED)
				return enStatusError;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfA3977Close(PFbyte* id)
{
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)	
		
	pfGpioPinsClear(a3977Config[*id].resetGpio.port, a3977Config[*id].resetGpio.pin);
	pfGpioPinsClear(a3977Config[*id].sleepGpio.port, a3977Config[*id].sleepGpio.pin);
	
	a3977Init[*id] = enBooleanFalse;
	a3977Variables[*id].a3977position = 0;
	a3977Variables[*id].a3977currentState = enA3977stateStop;
	a3977Config[*id].pfTimerStop();
	--a3977DeviceCount;	

	return enStatusSuccess;
}

PFEnStatus pfA3977CloseAll(void)
{
	volatile PFdword i=0;
#if (PF_A3977_DEBUG == 1)
	if(a3977DeviceCount != 0)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)	
	
	for(i=0; i<A3977_MAX_NO_OF_LYNX_CONNECTED; ++i)
	{
		if(a3977Init[i] == enBooleanTrue)
		{
			pfGpioPinsClear(a3977Config[i].resetGpio.port, a3977Config[i].resetGpio.pin);
			pfGpioPinsClear(a3977Config[i].sleepGpio.port, a3977Config[i].sleepGpio.pin);
			
			a3977Init[i] = enBooleanFalse;
			a3977Variables[i].a3977position = 0;
			a3977Variables[i].a3977currentState = enA3977stateStop;
			a3977Config[i].pfTimerStop();
		}
	}

	a3977DeviceCount=0;
	
	return enStatusSuccess;
}

PFEnStatus pfA3977ResetDevice(PFbyte *id)
{
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)	

	pfGpioPinsClear(a3977Config[*id].resetGpio.port, a3977Config[*id].resetGpio.pin);
	pfDelayMicroSec(100);      //reset pulse of 100us
	pfGpioPinsSet(a3977Config[*id].resetGpio.port, a3977Config[*id].resetGpio.pin);

	a3977Variables[*id].a3977position = 0;
	a3977Variables[*id].res=0;
	a3977Variables[*id].minCount=0;
	a3977Variables[*id].nextMinCount=0;
	a3977Variables[*id].newCount=0;
	a3977Variables[*id].prevCount=0;
	a3977Variables[*id].decelerationStart=0;
	a3977Variables[*id].nu_of_steps=0;
	a3977Variables[*id].a3977speed =0;
	a3977Variables[*id].a3977currentState = enA3977stateStop;

	a3977Flags[*id].a3977DeviceReady = enBooleanTrue;
	a3977Flags[*id].a3977decelerateFlag = enBooleanFalse;
	a3977Flags[*id].a3977reAccelerateFlag = enBooleanFalse;
	a3977Flags[*id].a3977RunFlag=enBooleanFalse;
	a3977Flags[*id].a3977inFullStepModeFlag=enBooleanFalse;
	
	a3977Config[*id].pfTimerStop();
	
	return enStatusSuccess;
}

PFEnStatus pfA3977Sleep(PFbyte *id, PFEnBoolean sleepState)
{
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)	
	
	if(sleepState == enBooleanTrue)
	{
		pfGpioPinsClear(a3977Config[*id].sleepGpio.port, a3977Config[*id].sleepGpio.pin);
		a3977Flags[*id].a3977DeviceReady = enBooleanFalse;
		
		a3977Config[*id].pfTimerStop();
	}
	else
	{
		pfGpioPinsSet(a3977Config[*id].sleepGpio.port, a3977Config[*id].sleepGpio.pin);
		a3977Flags[*id].a3977DeviceReady = enBooleanTrue;
		pfDelayMicroSec(1000);//delay of 1ms is neccesory to get device ready
		a3977Config[*id].pfTimerStart();
	}

	return enStatusSuccess;
}

PFEnStatus pfA3977Move(PFbyte *id, PFbyte dir, PFdword steps, PFfloat speed)
{
	PFEnStatus status;
	float tst = 0;
	volatile PFsdword decelerationCount=0;
	volatile PFdword maxStepsToAttainTargetSpeed=0;
	volatile PFsdword accelerationLimit=0;
	
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
	if( speed > A3977_MAX_SPEED_LIMIT )
		return enStatusInvArgs;
#endif	// #if (PF_A3977_DEBUG == 1)

	if( (a3977Variables[*id].a3977currentState != enA3977stateStop) || (a3977Flags[*id].a3977DeviceReady == enBooleanFalse) )
		return enStatusBusy;
	
	if( (speed > A3977_MAX_SPEED_LIMIT) || (speed <= 0) )
		return enStatusInvArgs;
	
	status = pfA3977getSpeed(id, &tst);
	if(status != enStatusSuccess)
		return status;
	
	if( (speed == tst) || (steps == 0) )
		return enStatusSuccess;

	a3977Flags[*id].a3977DeviceReady = enBooleanFalse;
		
	if(dir == enA3977dirCCW)
	{
		pfGpioPinsClear(a3977Config[*id].dirGpio.port, a3977Config[*id].dirGpio.pin);
		
		a3977Variables[*id].sign = 1;
	}
	else
	{
		pfGpioPinsSet(a3977Config[*id].dirGpio.port, a3977Config[*id].dirGpio.pin);
		
		a3977Variables[*id].sign = -1;
	}
	
	if(steps==1)
	{
//issue step pulse
		pfGpioPinsClear(a3977Config[*id].stepGpio.port, a3977Config[*id].stepGpio.pin);
		pfDelayMicroSec(1);
		pfGpioPinsSet(a3977Config[*id].stepGpio.port, a3977Config[*id].stepGpio.pin);
		
		a3977Variables[*id].a3977position += (PFsdword)(a3977Variables[*id].sign * a3977Variables[*id].stepFactor);
	}
	else
	{
		tst = ( (float)a3977Variables[*id].timerFrequency )/(speed * (float)(1<<a3977Config[*id].stepResolution));
		a3977Variables[*id].minCount = (PFdword)tst;

		if( a3977Variables[*id].a3977InitialCountForAcc < a3977Variables[*id].minCount )
		{
			a3977Config[*id].pfTimerUpdateMatchRegister(0, a3977Variables[*id].minCount);
			a3977Variables[*id].newCount = a3977Variables[*id].minCount;
		}
		else
		{
			a3977Config[*id].pfTimerUpdateMatchRegister(0, a3977Variables[*id].a3977InitialCountForAcc);
			a3977Variables[*id].newCount = a3977Variables[*id].a3977InitialCountForAcc;
		}

		tst = ( speed*speed*(float)(1<<a3977Config[*id].stepResolution) )/( 2.0 * a3977Config[*id].acceleration );
		maxStepsToAttainTargetSpeed = (PFdword)tst;

		if(maxStepsToAttainTargetSpeed==0)
			maxStepsToAttainTargetSpeed=1;

		tst = ( ((float)steps) * a3977Config[*id].deceleration )/(a3977Config[*id].acceleration+a3977Config[*id].deceleration);
		accelerationLimit = (PFdword)tst;
		
		if(accelerationLimit == 0)
			accelerationLimit = 1;

		if(maxStepsToAttainTargetSpeed < accelerationLimit)
			decelerationCount = - ( (maxStepsToAttainTargetSpeed * a3977Config[*id].acceleration) / a3977Config[*id].deceleration );
		else
			decelerationCount = - ( steps - accelerationLimit );

		if(decelerationCount==0)
			decelerationCount = -1;

		a3977Variables[*id].decelerationStart = ((steps + decelerationCount) * (PFsdword)(a3977Variables[*id].sign * a3977Variables[*id].stepFactor)) + a3977Variables[*id].a3977position;

		a3977Variables[*id].nu_of_steps = 0;

		a3977Config[*id].pfTimerReset();

		if(a3977Variables[*id].newCount > a3977Variables[*id].minCount)
			a3977Variables[*id].a3977currentState = enA3977stateAcceleration;
		else
			a3977Variables[*id].a3977currentState = enA3977stateRunning;
		
		a3977Variables[*id].a3977speed = speed;
		
		a3977Flags[*id].a3977RunFlag = 0;
	}

	return enStatusSuccess;
}

PFEnStatus pfA3977ResetPosition(PFbyte *id)
{	
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)	

	a3977Variables[*id].a3977position = 0;
		
	return enStatusSuccess;
}

PFEnStatus pfA3977run(PFbyte *id, PFbyte dir, PFfloat speed)
{
	float tst = 0, curSpeed=0;
	volatile PFEnStatus status;
	volatile PFsdword j=0,n=0;

#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
	if( speed > A3977_MAX_SPEED_LIMIT )
		return enStatusInvArgs;
#endif	// #if (PF_A3977_DEBUG == 1)
		
	if( !a3977Flags[*id].a3977DeviceReady ) 
		return enStatusBusy;
	
	if( (speed > A3977_MAX_SPEED_LIMIT) || (speed <=0) )
		return enStatusInvArgs;

	status = pfA3977getSpeed(id, &curSpeed);
	if(status!=enStatusSuccess)
		return status;
	
	if( (curSpeed == speed) && ( a3977Variables[*id].a3977Direction == dir ) )
		return enStatusSuccess;

	a3977Flags[*id].a3977DeviceReady = enBooleanFalse;
	a3977Flags[*id].a3977RunFlag = enBooleanTrue;

	if( curSpeed != 0 )
	{
		a3977Flags[*id].a3977inFullStepModeFlag = enBooleanFalse;

		switch( a3977Config[*id].stepResolution )
		{
			case enA3977StepResolutionFull:
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				a3977Variables[*id].stepFactor = 8;
				break;
				
			case enA3977StepResolutionHalf:
				pfGpioPinsSet(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsClear(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 4;
				break;
			
			case enA3977StepResolutionQuarter:
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsSet(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 2;
				break;
			
			case enA3977StepResolutionEighth:
				pfGpioPinsSet(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsSet(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 1;
				break;
			
			default:
				return enStatusInvArgs;
		}

		if( curSpeed < speed )
		{
			a3977Variables[*id].a3977speed = speed;
			if( a3977Variables[*id].a3977Direction == dir )//accelerate in same direction
			{				
				tst = ( (float)a3977Variables[*id].timerFrequency )/(speed * (float)(1<<a3977Config[*id].stepResolution));
				a3977Variables[*id].minCount = (PFdword)tst;
				a3977Variables[*id].a3977currentState = enA3977stateAcceleration;
			}
			else
			{//decelerate to zero then reaccelerate to opposite dir
				tst = ( (float)a3977Variables[*id].timerFrequency )/(speed * (float)(1<<a3977Config[*id].stepResolution));
				a3977Variables[*id].nextMinCount = (PFdword)tst;
				 
				a3977Variables[*id].a3977currentState = enA3977stateDeceleration;
				
				a3977Variables[*id].res=0;
				a3977Variables[*id].newCount = a3977Variables[*id].a3977InitialCountForDec;
				do{
					++n;
					a3977Variables[*id].prevCount = a3977Variables[*id].newCount;
					a3977Variables[*id].newCount = a3977Variables[*id].prevCount - ( ( (2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)/((4*n)+1) );
					a3977Variables[*id].res = ((2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)%((4*n)+1);
				}while(a3977Variables[*id].newCount > a3977Variables[*id].minCount);
				
				a3977Variables[*id].nu_of_steps = -n;
				a3977Flags[*id].a3977reAccelerateFlag = enBooleanTrue;
			}
		}
		else
		{
			a3977Variables[*id].a3977speed = speed;
			if( a3977Variables[*id].a3977Direction == dir )//decelerate to lower speed
			{
				tst = ( (float)a3977Variables[*id].timerFrequency )/(speed * (float)(1<<a3977Config[*id].stepResolution));
				a3977Variables[*id].minCount = (PFdword)tst;

				a3977Variables[*id].a3977currentState = enA3977stateDeceleration;
				
				a3977Variables[*id].res=0;
				a3977Variables[*id].newCount = a3977Variables[*id].a3977InitialCountForDec;
				do{
					++n;
					a3977Variables[*id].prevCount = a3977Variables[*id].newCount;
					a3977Variables[*id].newCount = a3977Variables[*id].prevCount - ( ( (2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)/((4*n)+1) );
					a3977Variables[*id].res = ((2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)%((4*n)+1);
				}while(a3977Variables[*id].newCount > a3977Variables[*id].minCount);
				
				a3977Variables[*id].nu_of_steps = -n;
				a3977Flags[*id].a3977decelerateFlag = enBooleanTrue;
			}
			else
			{//decelerate to zero then reaccelerate to opposite dir
				tst = ( (float)a3977Variables[*id].timerFrequency )/(speed * (float)(1<<a3977Config[*id].stepResolution));
				a3977Variables[*id].nextMinCount = (PFdword)tst;

				a3977Variables[*id].a3977currentState = enA3977stateDeceleration;
				
				a3977Variables[*id].res=0;
				a3977Variables[*id].newCount = a3977Variables[*id].a3977InitialCountForDec;
				do{
					++n;
					a3977Variables[*id].prevCount = a3977Variables[*id].newCount;
					a3977Variables[*id].newCount = a3977Variables[*id].prevCount - ( ( (2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)/((4*n)+1) );
					a3977Variables[*id].res = ((2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)%((4*n)+1);
				}while(a3977Variables[*id].newCount > a3977Variables[*id].minCount);
				
				a3977Variables[*id].nu_of_steps = -n;
				a3977Flags[*id].a3977reAccelerateFlag = enBooleanTrue;
			}
		}
	}
	else
	{
		if(dir == enA3977dirCCW)
		{
			pfGpioPinsClear(a3977Config[*id].dirGpio.port, a3977Config[*id].dirGpio.pin);

			a3977Variables[*id].sign = 1;
		}
		else
		{
			pfGpioPinsSet(a3977Config[*id].dirGpio.port, a3977Config[*id].dirGpio.pin);

			a3977Variables[*id].sign = -1;
		}

		tst = ( (float)a3977Variables[*id].timerFrequency )/(speed * (float)(1<<a3977Config[*id].stepResolution));
		a3977Variables[*id].minCount = (PFdword)tst;

		if( a3977Variables[*id].a3977InitialCountForAcc < a3977Variables[*id].minCount )
		{
			a3977Config[*id].pfTimerUpdateMatchRegister(0, a3977Variables[*id].minCount);
			a3977Variables[*id].newCount = a3977Variables[*id].minCount;
		}
		else
		{
			a3977Config[*id].pfTimerUpdateMatchRegister(0,a3977Variables[*id].a3977InitialCountForAcc);
			a3977Variables[*id].newCount = a3977Variables[*id].a3977InitialCountForAcc;
		}

		a3977Variables[*id].nu_of_steps = 0;

		a3977Variables[*id].a3977speed = speed;

		a3977Config[*id].pfTimerReset();

		if(a3977Variables[*id].newCount <= a3977Variables[*id].minCount)
		{
			a3977Variables[*id].a3977currentState = enA3977stateRunning;
			a3977Flags[*id].a3977DeviceReady = enBooleanTrue;
			a3977Variables[*id].a3977Direction = dir;
			
			return enStatusSuccess;
		}
		else
			a3977Variables[*id].a3977currentState = enA3977stateAcceleration;
	}

	a3977Variables[*id].a3977Direction = dir;
	
	return enStatusSuccess;
}

PFEnStatus pfA3977DeviceReady(PFbyte *id, PFEnBoolean* test)
{
// check for initialization
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)

	if( a3977Flags[*id].a3977DeviceReady )
		*test = enBooleanTrue;
	else
		*test = enBooleanFalse;
	
	return enStatusSuccess;
}

PFEnStatus pfA3977hardStop(PFbyte *id)
{	
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)

	a3977Config[*id].pfTimerStop();

	if( a3977Flags[*id].a3977inFullStepModeFlag )
	{
		a3977Flags[*id].a3977inFullStepModeFlag = enBooleanFalse;

		switch(a3977Config[*id].stepResolution)
		{
			case enA3977StepResolutionFull:
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				a3977Variables[*id].stepFactor = 8;
				break;	
				
			case enA3977StepResolutionHalf:
				pfGpioPinsSet(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsClear(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 4;
				break;
			
			case enA3977StepResolutionQuarter:
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsSet(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 2;
				break;
			
			case enA3977StepResolutionEighth:
				pfGpioPinsSet(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsSet(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 1;
				break;
			
			default:
				return enStatusInvArgs;
		}
	}

	a3977Flags[*id].a3977decelerateFlag = enBooleanFalse;
	a3977Flags[*id].a3977reAccelerateFlag = enBooleanFalse;
	a3977Flags[*id].a3977RunFlag=enBooleanFalse;
	a3977Flags[*id].a3977DeviceReady = enBooleanTrue;
	a3977Flags[*id].a3977inFullStepModeFlag = enBooleanTrue;
	
	a3977Variables[*id].res=0;
	a3977Variables[*id].minCount=0;
	a3977Variables[*id].nextMinCount=0;
	a3977Variables[*id].newCount=0;
	a3977Variables[*id].prevCount=0;
	a3977Variables[*id].decelerationStart=0;
	a3977Variables[*id].nu_of_steps=0;
	a3977Variables[*id].a3977speed =0;
	a3977Variables[*id].a3977currentState = enA3977stateStop;
	
	return enStatusSuccess;
}

PFEnStatus pfA3977softStop(PFbyte *id)
{	
	volatile PFsdword n=0,tempRes=0;
	volatile PFsdword tempNewCount=0, tempPrevCount=0;

#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)	

	if(a3977Variables[*id].a3977currentState == enA3977stateStop)
		return enStatusSuccess;
	
	tempRes=0;
	tempNewCount = a3977Variables[*id].a3977InitialCountForDec;//recalculation for acceleration
	do{
		++n;
		tempPrevCount = tempNewCount;
		tempNewCount = tempPrevCount - ( ( (2*tempPrevCount)+tempRes)/((4*n)+1) );
		tempRes = ((2*tempPrevCount)+tempRes)%((4*n)+1);
	}while(tempNewCount > a3977Variables[*id].newCount);

	a3977Config[*id].pfTimerStop();//to avoid timer interrupt in between
	
	a3977Variables[*id].res = tempRes;
	a3977Variables[*id].prevCount = tempPrevCount;
	a3977Variables[*id].newCount = tempNewCount;
	a3977Variables[*id].nu_of_steps = -n;
	
	a3977Flags[*id].a3977DeviceReady = enBooleanFalse;
	a3977Flags[*id].a3977decelerateFlag = enBooleanFalse;
	a3977Flags[*id].a3977reAccelerateFlag = enBooleanFalse;
	a3977Variables[*id].a3977speed =0;
	a3977Variables[*id].a3977currentState = enA3977stateDeceleration;

	if( a3977Flags[*id].a3977inFullStepModeFlag )
	{
		a3977Flags[*id].a3977inFullStepModeFlag = enBooleanFalse;

		switch(a3977Config[*id].stepResolution)
		{	
			case enA3977StepResolutionFull:
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				a3977Variables[*id].stepFactor = 8;
				break;	
				
			case enA3977StepResolutionHalf:
				pfGpioPinsSet(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsClear(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 4;
				break;
			
			case enA3977StepResolutionQuarter:
				pfGpioPinsClear(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsSet(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 2;
				break;
			
			case enA3977StepResolutionEighth:
				pfGpioPinsSet(a3977Config[*id].ms1Gpio.port, a3977Config[*id].ms1Gpio.pin);
				pfGpioPinsSet(a3977Config[*id].ms2Gpio.port, a3977Config[*id].ms2Gpio.pin);
				a3977Variables[*id].stepFactor = 1;
				break;
			
			default:
				return enStatusInvArgs;
		}
	}
	a3977Config[*id].pfTimerStart();

	return enStatusSuccess;
}

PFEnStatus pfA3977GoHomePosition(PFbyte *id)
{
	PFsdword temp=0;
	PFEnStatus status;
	
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)

	if(a3977Variables[*id].a3977currentState != enA3977stateStop)
		return enStatusBusy;

	status = pfA3977GetCurrentPosition(id, &temp);
	if(status!=enStatusSuccess)
		return status;
	
	if(temp==0)
		return enStatusSuccess;
	else
	{
		if(temp<0)
		{
			temp=(PFdword)(-1*temp);
			status = pfA3977Move(id, enA3977dirCCW, temp, 200);
			if(status!=enStatusSuccess)
				return status;
		}
		else
		{
			status = pfA3977Move(id, enA3977dirCW, temp, 200);
			if(status!=enStatusSuccess)
				return status;
		}
	}
	
	return enStatusSuccess;
}

PFEnStatus pfA3977getSpeed(PFbyte *id, PFfloat* speed)
{
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)

	switch(a3977Variables[*id].a3977currentState)
	{
		case enA3977stateAcceleration :
		case enA3977stateDeceleration : 
				*speed = (float)a3977Variables[*id].timerFrequency/(float)(a3977Variables[*id].newCount*(1<<a3977Config[*id].stepResolution));
		        break;
		
		case enA3977stateRunning : 
				*speed = a3977Variables[*id].a3977speed;
		        break;

		case enA3977stateStop : 
				*speed = 0;
		        break;
		
		default : 
				return enStatusInvState;
	}
	
	return enStatusSuccess;
}

PFEnStatus pfA3977GetCurrentPosition(PFbyte *id, PFsdword* position)
{
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)

	*position = a3977Variables[*id].a3977position/(8>>a3977Config[*id].stepResolution);//a3977Variables[*id].stepFactor;
		
	return enStatusSuccess;
}

PFEnStatus pfA3977SetAccelerationProfile(PFbyte *id, PFfloat acceleration, PFfloat deceleration)
{
	float temp=0;
	volatile PFsdword n=0;

#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)
		
	if( !a3977Flags[*id].a3977DeviceReady )
		return enStatusBusy;

	switch( a3977Variables[*id].a3977currentState )
	{
		case enA3977stateAcceleration :
		case enA3977stateDeceleration : 
				return enStatusBusy;

		case enA3977stateRunning : 
		    {
				//C0 calculation
				temp = 2.0/( (a3977Config[*id].acceleration)*((float)(1<<a3977Config[*id].stepResolution)) );
				temp = 0.676 * ((float)a3977Variables[*id].timerFrequency) * (sqrt(temp));
				a3977Variables[*id].a3977InitialCountForAcc = (PFdword)temp;

				//C0 calculation for deceleration
				temp = 2.0/( (a3977Config[*id].deceleration)*((float)(1<<a3977Config[*id].stepResolution)) );
				temp = 0.676 * ((float)a3977Variables[*id].timerFrequency) * (sqrt(temp));
				a3977Variables[*id].a3977InitialCountForDec = (PFdword)temp;

				a3977Variables[*id].res=0;
				a3977Variables[*id].newCount = a3977Variables[*id].a3977InitialCountForAcc;
				do{
					++n;
					a3977Variables[*id].prevCount = a3977Variables[*id].newCount;
					a3977Variables[*id].newCount = a3977Variables[*id].prevCount - ( ( (2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)/((4*n)+1) );
					a3977Variables[*id].res = ((2*a3977Variables[*id].prevCount)+a3977Variables[*id].res)%((4*n)+1);
				}while(a3977Variables[*id].newCount > a3977Variables[*id].minCount);
																			
				a3977Variables[*id].nu_of_steps = n;
				a3977Config[*id].acceleration = acceleration;
				a3977Config[*id].deceleration = deceleration;
																			
				break;
			}

		case enA3977stateStop : 
		    {
				//C0 calculation
				temp = 2.0/( (a3977Config[*id].acceleration)*((float)(1<<a3977Config[*id].stepResolution)) );
				temp = 0.676 * ((float)a3977Variables[*id].timerFrequency) * (sqrt(temp));
				a3977Variables[*id].a3977InitialCountForAcc = (PFdword)temp;

				//C0 calculation for deceleration
				temp = 2.0/( (a3977Config[*id].deceleration)*((float)(1<<a3977Config[*id].stepResolution)) );
				temp = 0.676 * ((float)a3977Variables[*id].timerFrequency) * (sqrt(temp));
				a3977Variables[*id].a3977InitialCountForDec = (PFdword)temp;

				a3977Config[*id].acceleration = acceleration;
				a3977Config[*id].deceleration = deceleration;
				break;
			}
		default :
				return enStatusInvState;
	}
	
	return enStatusSuccess;
}
 
PFEnStatus pfA3977getAccelerationProfile(PFbyte *id, PFfloat* acceleration, PFfloat* deceleration)
{
#if (PF_A3977_DEBUG == 1)
	if(*id > A3977_MAX_NO_OF_LYNX_CONNECTED)
		return enStatusInvArgs;
	if(a3977Init[*id] != enBooleanTrue)
		return enStatusNotConfigured;
#endif	// #if (PF_A3977_DEBUG == 1)
	
	*acceleration = a3977Config[*id].acceleration;
	*deceleration = a3977Config[*id].deceleration;
	
	return enStatusSuccess;
}

void stepperControlFunction(void)
{
	volatile PFdword intStatus=0;
	volatile PFsdword j=0, n=0;
	volatile PFbyte id=0, k=0;

	for(k=0; k<A3977_MAX_NO_OF_LYNX_CONNECTED; ++k)
	{
		if(a3977Init[k] == enBooleanTrue)
		{
			a3977Config[k].pfTimerGetIntStatus(&intStatus);
			if( intStatus && 0x0001 )
			{
				id=k;
				break;
			}
		}
	}

	pfGpioPinsClear(a3977Config[id].stepGpio.port, a3977Config[id].stepGpio.pin);
	pfDelayMicroSec(10);    //delay of 10us
	pfGpioPinsSet(a3977Config[id].stepGpio.port, a3977Config[id].stepGpio.pin);
		
	a3977Variables[id].a3977position += (PFsdword)(a3977Variables[id].sign*a3977Variables[id].stepFactor);

	switch( a3977Variables[id].a3977currentState )
	{
		case enA3977stateAcceleration :
		    {
				if(a3977Variables[id].newCount > a3977Variables[id].minCount)
				{
					++a3977Variables[id].nu_of_steps;
					
					a3977Variables[id].prevCount = a3977Variables[id].newCount;
					a3977Variables[id].newCount = a3977Variables[id].prevCount - ( ( (2*a3977Variables[id].prevCount)+a3977Variables[id].res)/((4*a3977Variables[id].nu_of_steps)+1) );
					a3977Variables[id].res = ((2*a3977Variables[id].prevCount)+a3977Variables[id].res)%((4*a3977Variables[id].nu_of_steps)+1);

					a3977Config[id].pfTimerReset();
					if(a3977Variables[id].newCount > a3977Variables[id].minCount)
					{
						a3977Config[id].pfTimerUpdateMatchRegister(0, a3977Variables[id].newCount);
					}
					else
					{
						a3977Variables[id].a3977currentState = enA3977stateRunning;
						if(a3977Flags[id].a3977RunFlag)
						{//for run cmd
							a3977Flags[id].a3977DeviceReady = enBooleanTrue;
							a3977Flags[id].a3977inFullStepModeFlag = enBooleanTrue;
							j = a3977Variables[id].minCount * (1<<a3977Config[id].stepResolution);//corresponds to full step speed

							pfGpioPinsClear(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);//set into full step mode
							pfGpioPinsClear(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
							a3977Variables[id].stepFactor = 8;

							a3977Config[id].pfTimerUpdateMatchRegister(0,j);
						}
						else
						{//for move cmd
							if( a3977Config[id].stepResolution != enA3977StepResolutionFull)
							{
								a3977Flags[id].a3977inFullStepModeFlag = enBooleanTrue;
								n = (a3977Variables[id].decelerationStart - a3977Variables[id].a3977position) % 8;  //match value is in ustep
								
								if( (n==0) && (a3977Variables[id].a3977position != a3977Variables[id].decelerationStart) )
								{
									j = a3977Variables[id].minCount * (1<<a3977Config[id].stepResolution);//corresponds to full step speed

									pfGpioPinsClear(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);//set into full step mode
									pfGpioPinsClear(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
									a3977Variables[id].stepFactor = 8;

									a3977Config[id].pfTimerUpdateMatchRegister(0,j);
								}
								else
								{
									a3977Variables[id].enterFullStepMode = a3977Variables[id].a3977position + n;//enter run state with usteps till this position afterward run with full step mode
									a3977Config[id].pfTimerUpdateMatchRegister(0, a3977Variables[id].minCount);
								}
							}
						}
					}
				}
				else
				{
					a3977Config[id].pfTimerReset();
					a3977Flags[id].a3977inFullStepModeFlag = enBooleanTrue;

					j = a3977Variables[id].minCount * (1<<a3977Config[id].stepResolution);//corresponds to full step speed

					pfGpioPinsClear(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);//set into full step mode
					pfGpioPinsClear(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
					a3977Variables[id].stepFactor = 8;

					a3977Config[id].pfTimerUpdateMatchRegister(0,j);
					
					a3977Variables[id].a3977currentState = enA3977stateRunning;
					if(a3977Flags[id].a3977RunFlag)
						a3977Flags[id].a3977DeviceReady = enBooleanTrue;
				}

				if( (a3977Variables[id].a3977position == a3977Variables[id].decelerationStart) && (!a3977Flags[id].a3977RunFlag) )
				{
					a3977Variables[id].a3977currentState = enA3977stateDeceleration;
					a3977Flags[id].a3977DeviceReady = enBooleanFalse;
					
					n=0;
					a3977Variables[id].res=0;
					a3977Variables[id].nu_of_steps = (a3977Variables[id].nu_of_steps*(PFdword)a3977Config[id].acceleration)/(PFdword)a3977Config[id].deceleration;
					a3977Variables[id].newCount = a3977Variables[id].a3977InitialCountForDec;//recalculation for deceleration
					do{
						++n;
						a3977Variables[id].prevCount = a3977Variables[id].newCount;
						a3977Variables[id].newCount = a3977Variables[id].prevCount - ( ( (2*a3977Variables[id].prevCount)+a3977Variables[id].res)/((4*n)+1) );
						a3977Variables[id].res = ((2*a3977Variables[id].prevCount)+a3977Variables[id].res)%((4*n)+1);
					}while(n != a3977Variables[id].nu_of_steps);

					a3977Variables[id].nu_of_steps = -a3977Variables[id].nu_of_steps;

					a3977Config[id].pfTimerReset();
					a3977Config[id].pfTimerUpdateMatchRegister(0, a3977Variables[id].newCount);
				}
				break;
			}

		case enA3977stateDeceleration : 
		      {
				++a3977Variables[id].nu_of_steps;

				if(a3977Variables[id].nu_of_steps >= 0)
				{
					if( a3977Flags[id].a3977reAccelerateFlag )//acc in opposite dir
					{
						a3977Variables[id].newCount = a3977Variables[id].a3977InitialCountForAcc;
						a3977Variables[id].res=0;
						a3977Variables[id].minCount = a3977Variables[id].nextMinCount;
						a3977Variables[id].nu_of_steps = 0;
						a3977Variables[id].a3977currentState = enA3977stateAcceleration;
						a3977Flags[id].a3977reAccelerateFlag = enBooleanFalse;
						
						if(a3977Variables[id].a3977Direction == enA3977dirCCW)
						{
							pfGpioPinsClear(a3977Config[id].dirGpio.port, a3977Config[id].dirGpio.pin);
							a3977Variables[id].sign = 1;
						}
						else
						{
							pfGpioPinsSet(a3977Config[id].dirGpio.port, a3977Config[id].dirGpio.pin);
							a3977Variables[id].sign = -1;
						}

						a3977Config[id].pfTimerReset();
						a3977Config[id].pfTimerUpdateMatchRegister(0, a3977Variables[id].newCount);
					}
					else
					{
						if(a3977Flags[id].a3977RunFlag)
						a3977Flags[id].a3977RunFlag = enBooleanFalse;

						a3977Flags[id].a3977DeviceReady = enBooleanTrue;
						a3977Variables[id].a3977currentState = enA3977stateStop;
						a3977Variables[id].a3977speed = 0;
						a3977Config[id].pfTimerStop();
					}

					break;
				}

				a3977Variables[id].prevCount = a3977Variables[id].newCount;
				a3977Variables[id].newCount = a3977Variables[id].prevCount - ( ( (2*a3977Variables[id].prevCount)+a3977Variables[id].res)/((4*a3977Variables[id].nu_of_steps)+1) );
				a3977Variables[id].res = ((2*a3977Variables[id].prevCount)+a3977Variables[id].res)%((4*a3977Variables[id].nu_of_steps)+1);

				if( a3977Flags[id].a3977decelerateFlag && (a3977Variables[id].newCount > a3977Variables[id].minCount) )//same dir with lower speed
				{
					a3977Variables[id].a3977currentState = enA3977stateRunning;
					a3977Flags[id].a3977DeviceReady = enBooleanTrue;
					a3977Flags[id].a3977decelerateFlag = enBooleanFalse;
					
					n=0;
					a3977Variables[id].res=0;
					a3977Variables[id].newCount = a3977Variables[id].a3977InitialCountForAcc;//recalculation for acceleration
					do{
						++n;
						a3977Variables[id].prevCount = a3977Variables[id].newCount;
						a3977Variables[id].newCount = a3977Variables[id].prevCount - ( ( (2*a3977Variables[id].prevCount)+a3977Variables[id].res)/((4*n)+1) );
						a3977Variables[id].res = ((2*a3977Variables[id].prevCount)+a3977Variables[id].res)%((4*n)+1);
					}while(a3977Variables[id].newCount > a3977Variables[id].minCount);
					
					a3977Variables[id].nu_of_steps = n;

					a3977Config[id].pfTimerReset();
					a3977Flags[id].a3977inFullStepModeFlag = enBooleanTrue;
					j = a3977Variables[id].minCount * (1<<a3977Config[id].stepResolution);//corresponds to full step speed

					pfGpioPinsClear(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);//set into full step mode
					pfGpioPinsClear(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
					a3977Variables[id].stepFactor = 8;

					a3977Config[id].pfTimerUpdateMatchRegister(0,j);
					break;
				}
				a3977Config[id].pfTimerReset();
				a3977Config[id].pfTimerUpdateMatchRegister(0, a3977Variables[id].newCount);

				break;
			}

		case enA3977stateRunning  : 
		    {
				if( (a3977Variables[id].a3977position == a3977Variables[id].enterFullStepMode) && (!a3977Flags[id].a3977RunFlag) && (a3977Flags[id].a3977inFullStepModeFlag) )//enter into fullstep mode
				{
					if( !(a3977Variables[id].decelerationStart == a3977Variables[id].enterFullStepMode) )
					{
						a3977Config[id].pfTimerReset();
						j = a3977Variables[id].minCount * (1<<a3977Config[id].stepResolution);//corresponds to full step speed

						pfGpioPinsClear(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);//set into full step mode
						pfGpioPinsClear(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
						a3977Variables[id].stepFactor = 8;

						a3977Config[id].pfTimerUpdateMatchRegister(0,j);
					}
				}

				if( (a3977Variables[id].a3977position == a3977Variables[id].decelerationStart) && (!a3977Flags[id].a3977RunFlag) )//for move cmd
				{
					a3977Variables[id].a3977currentState = enA3977stateDeceleration;
					
					n=0;
					a3977Variables[id].res=0;
					a3977Variables[id].newCount = a3977Variables[id].a3977InitialCountForDec;

					if( a3977Variables[id].decelerationStart == a3977Variables[id].enterFullStepMode )
						a3977Flags[id].a3977inFullStepModeFlag = enBooleanFalse;

					if( a3977Flags[id].a3977inFullStepModeFlag )
					{
						a3977Flags[id].a3977inFullStepModeFlag = enBooleanFalse;

						switch(a3977Config[id].stepResolution)
						{		
							case enA3977StepResolutionHalf:
								pfGpioPinsSet(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);
								pfGpioPinsClear(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
								a3977Variables[id].stepFactor = 4;
								break;
							
							case enA3977StepResolutionQuarter:
								pfGpioPinsClear(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);
								pfGpioPinsSet(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
								a3977Variables[id].stepFactor = 2;
								break;
							
							case enA3977StepResolutionEighth:
								pfGpioPinsSet(a3977Config[id].ms1Gpio.port, a3977Config[id].ms1Gpio.pin);
								pfGpioPinsSet(a3977Config[id].ms2Gpio.port, a3977Config[id].ms2Gpio.pin);
								a3977Variables[id].stepFactor = 1;
								break;
							
							default:
								return;
						}
					}

					do{
						++n;
						a3977Variables[id].prevCount = a3977Variables[id].newCount;
						a3977Variables[id].newCount = a3977Variables[id].prevCount - ( ( (2*a3977Variables[id].prevCount)+a3977Variables[id].res)/((4*n)+1) );
						a3977Variables[id].res = ((2*a3977Variables[id].prevCount)+a3977Variables[id].res)%((4*n)+1);
					}while(a3977Variables[id].newCount > a3977Variables[id].minCount);
					
					a3977Variables[id].nu_of_steps = -n;
				}

				break;
			}

		default	: 
			a3977Flags[id].a3977DeviceReady = enBooleanTrue;
			a3977Variables[id].a3977currentState = enA3977stateStop;
			a3977Config[id].pfTimerStop();
			break;
	}
	
	a3977Config[id].pfTimerClearIntStatus(intStatus);

}

