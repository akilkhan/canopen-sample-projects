#include "prime_framework.h"
#include "prime_utils.h"
#include "prime_gpio.h"
#include "prime_qeiLS7366.h"

#define LS7366Left_STATUS_SIGN			0x01
#define LS7366Left_STATUS_COUNT_DIR		0x02
#define LS7366Left_STATUS_PLS			0x04
#define LS7366Left_STATUS_COUNT_EN		0x08
#define LS7366Left_STATUS_INDEX			0x10
#define LS7366Left_STATUS_COMPARE		0x20
#define LS7366Left_STATUS_BORROW		0x40
#define LS7366Left_STATUS_CARRY			0x80

#define LS7366Left_MDR0_QUAD_MODE_MASK		0x03
#define LS7366Left_MDR0_COUNT_LIMIT_MASK	0x0C
#define LS7366Left_MDR0_INDEX_MODE_MASK		0x30
#define LS7366Left_MDR0_SYNC_INDEX_MASK		0x40
#define LS7366Left_MDR0_FILT_CLK_DIV_MASK	0x80
#define LS7366Left_MDR1_COUNTER_SIZE_MASK	0x03
#define LS7366Left_MDR1_DISABLE_COUNT_MASK		0x04
#define LS7366Left_MDR1_IDX_FLAG_MASK		0x10
#define LS7366Left_MDR1_CMP_FLAG_MASK		0x20
#define LS7366Left_MDR1_BW_FLAG_MASK		0x40
#define LS7366Left_MDR1_CY_FLAG_MASK		0x80

#define LS7366Left_IR_REG_NONE			0x00
#define LS7366Left_IR_REG_MDR0			0x08
#define LS7366Left_IR_REG_MDR1			0x10
#define LS7366Left_IR_REG_DTR			0x18
#define LS7366Left_IR_REG_CNTR			0x20
#define LS7366Left_IR_REG_OTR			0x28
#define LS7366Left_IR_REG_STR			0x30

#define LS7366Left_IR_ACT_CLR			0x00
#define LS7366Left_IR_ACT_RD			0x40
#define LS7366Left_IR_ACT_WR			0x80
#define LS7366Left_IR_ACT_LOAD			0xC0

// #define LS_REG_MDR0					1
// #define LS_REG_MDR1					2
// #define LS_REG_DTR					3
// #define LS_REG_CNTR					4
// #define LS_REG_OTR					5
// #define LS_REG_STR					6

// #define LS_OPN_CLR					0
// #define LS_OPN_READ					1
// #define LS_OPN_WRITE				2
// #define LS_OPN_LOAD					3

static PFCfgQeiLS7366 QeiLS7366Config[PF_MAX_QEI_SUPPORTED];
static PFbyte QeiLS7366SpiId[PF_MAX_QEI_SUPPORTED];
static PFbyte QeiLS7366Id[PF_MAX_QEI_SUPPORTED]={enBooleanFalse};
static PFEnBoolean QeiLS7366Init = enBooleanFalse;
static PFbyte qeiDeviceCount=0;
//static PFEnBoolean QeiLS7366Init = enBooleanTrue;

static void qeiDelay(void)
{
	PFdword del = 10000;
	while(del--);
}

#if 0
PFEnStatus pfQeiLS7366RegOperation(PFbyte id,PFbyte operation, PFbyte reg, PFbyte size, PFdword* wrData, PFdword* rdData)
{
	PFbyte cmd = 0, temp;
	PFdword inData = 0, outData = 0;
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif	
	outData = *wrData;
	cmd = (operation << 6) | (reg << 3);
	switch(operation)
	{
		case LS_OPN_CLR:
			if(QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 0) != enStatusSuccess)
				return enStatusError;
			QeiLS7366Config[id].spiExchangeByte(&QeiLS7366SpiId[id], cmd);
			qeiDelay();
			QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 1);
			break;

		case LS_OPN_READ:
		case LS_OPN_WRITE:
			if(QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 0) != enStatusSuccess)
				return enStatusError;
			QeiLS7366Config[id].spiExchangeByte(&QeiLS7366SpiId[id], cmd);
			//temp = pfSpiExchangeByte(&QeiLS7366SpiId, 0);			//Test code
			switch (size)
			{
				case 4:
					inData = QeiLS7366Config[id].spiExchangeByte(&QeiLS7366SpiId[id], (outData>>24));
				case 3:
					inData = (inData<<8)|QeiLS7366Config[id].spiExchangeByte(&QeiLS7366SpiId[id], (outData>>16));
				case 2:
					inData = (inData<<8)|QeiLS7366Config[id].spiExchangeByte(&QeiLS7366SpiId[id], (outData>>8));
				case 1:
					inData = (inData<<8)|QeiLS7366Config[id].spiExchangeByte(&QeiLS7366SpiId[id], outData);
			}
			QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 1);
			if(operation == LS_OPN_WRITE)
				inData =0;
			else
				*rdData = inData;
			break;
		case LS_OPN_LOAD:
			if(QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 0) != enStatusSuccess)
				return enStatusError;
			QeiLS7366Config[id].spiExchangeByte(&QeiLS7366SpiId[id], cmd);
			qeiDelay();
			QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 1);
			break;
	}
	
	return enStatusSuccess;
}
#endif	// 0

static PFEnStatus pfQeiLS7366RegRead(PFbyte id,PFbyte reg, PFdword* data)
{
	PFEnStatus status;
	PFbyte cmd, readSize, index;
	PFdword	readBytes;
	PFbyte readBuff[4] = {0};
/*#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif*/
	cmd = LS7366Left_IR_ACT_RD | reg;
	if( (reg == LS7366Left_IR_REG_MDR0) || (reg == LS7366Left_IR_REG_MDR1) || (reg == LS7366Left_IR_REG_STR) )
		readSize = 1;
	else
		readSize = QeiLS7366Config[id].counterSize;
	
	status = QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 0);
	if(status != enStatusSuccess)
		return enStatusError;
	qeiDelay();
	QeiLS7366Config[id].spiWrite(&QeiLS7366SpiId[id], &cmd, 1);
	QeiLS7366Config[id].spiRead(&QeiLS7366SpiId[id], readBuff, readSize, &readBytes);
	qeiDelay();
	QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 1);
	
	if( (reg == LS7366Left_IR_REG_MDR0) || (reg == LS7366Left_IR_REG_MDR1) || (reg == LS7366Left_IR_REG_STR) )
	{
		*data = readBuff[0];
	}
	else
	{
		*data = 0;
		for(index = 0; index < readSize; index++)
		{
			*data |= readBuff[index] << ((readSize - (index+1))*8);
		}
	}
	
	return enStatusSuccess;
}

static PFEnStatus pfQeiLS7366RegWrite(PFbyte id,PFbyte reg, PFdword data)
{
	PFEnStatus status;
	PFbyte cmd, writeSize, index;
	PFbyte writeBuff[4] = {0};
/*
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif*/
	cmd = LS7366Left_IR_ACT_WR | reg;
	
	if( (reg == LS7366Left_IR_REG_MDR0) || (reg == LS7366Left_IR_REG_MDR1) || (reg == LS7366Left_IR_REG_STR) )
	{
		writeSize = 1;
		writeBuff[0] = data;
	}
	else
	{
		writeSize = QeiLS7366Config[id].counterSize;
		for(index = 0; index < writeSize; index++)
		{
			writeBuff[index] = data >> ((writeSize - (index+1))*8);
		}
	}
	
	status = QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 0);
	if(status != enStatusSuccess)
		return enStatusError;
	qeiDelay();
	QeiLS7366Config[id].spiWrite(&QeiLS7366SpiId[id], &cmd, 1);
	QeiLS7366Config[id].spiWrite(&QeiLS7366SpiId[id], writeBuff, writeSize);
	qeiDelay();
	QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 1);
	
	return enStatusSuccess;
}

static PFEnStatus pfQeiLS7366RegClear(PFbyte id,PFbyte reg)
{
	PFEnStatus status;
	PFbyte cmd;
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif	
	cmd = LS7366Left_IR_ACT_CLR | reg;
	
	status = QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 0);
	if(status != enStatusSuccess)
		return enStatusError;
	qeiDelay();
	QeiLS7366Config[id].spiWrite(&QeiLS7366SpiId[id], &cmd, 1);
	qeiDelay();
	QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 1);
	
	return enStatusSuccess;
}

static PFEnStatus pfQeiLS7366RegLoad(PFbyte id,PFbyte reg)
{
	PFEnStatus status;
	PFbyte cmd;
	/*
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif	*/
	cmd = LS7366Left_IR_ACT_LOAD | reg;
	
	status = QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 0);
	if(status != enStatusSuccess)
		return enStatusError;
	qeiDelay();
	QeiLS7366Config[id].spiWrite(&QeiLS7366SpiId[id], &cmd, 1);
	qeiDelay();
	QeiLS7366Config[id].spiChipSelect(&QeiLS7366SpiId[id], 1);
	
	return enStatusSuccess;
}

PFEnStatus pfQeiLS7366Open(PFbyte *id, PPFCfgQeiLS7366 config,PFbyte count)
{
	PFdword mdr0, mdr1;
	PFbyte i,loop;
	
#if (PF_LS7366_DEBUG == 1)
	if(id==0)
		return enStatusInvArgs;
	if(config == 0)
		return enStatusInvArgs;
#endif	// #if (PF_LS7366_DEBUG == 1)
	
	for(i = 0;i < count ;i++ )
	{
		for(loop =0;loop < PF_MAX_QEI_SUPPORTED ; loop++)
		{
			
			if(QeiLS7366Id[loop] == enBooleanFalse)
			{
				if(config[i].spiRegisterDevice(&QeiLS7366SpiId[loop], config[i].spiCsGpio) != enStatusSuccess)
				{
					return enStatusError;
				}
			
				QeiLS7366Id[loop] = enBooleanTrue;
				qeiDeviceCount++;
					
				pfGpioPinsSet(config[i].spiCsGpio.port, config[i].spiCsGpio.pin);
				pfGpioPinsClear(config[i].countEnGpio.port, config[i].countEnGpio.pin);
				
				mdr0 = 0;
				mdr0 |= (config[i].quadratureMode &  LS7366Left_MDR0_QUAD_MODE_MASK);
				mdr0 |= (config[i].countLimit & LS7366Left_MDR0_COUNT_LIMIT_MASK);
				mdr0 |= (config[i].indexMode & LS7366Left_MDR0_INDEX_MODE_MASK);
				mdr0 |= (config[i].filterClockDiv & LS7366Left_MDR0_FILT_CLK_DIV_MASK);
				if(config[i].syncIndex == enBooleanTrue)
				{
					mdr0 |= LS7366Left_MDR0_SYNC_INDEX_MASK;
				}
				
				mdr1 = 0;
				mdr1 |= ( (4 - config[i].counterSize) & LS7366Left_MDR1_COUNTER_SIZE_MASK);
				if(config[i].FlagOnIndex == enBooleanTrue)
					mdr1 |= LS7366Left_MDR1_IDX_FLAG_MASK;
				if(config[i].FlagOnCompare == enBooleanTrue)
					mdr1 |= LS7366Left_MDR1_CMP_FLAG_MASK;
				if(config[i].FlagOnBorrow == enBooleanTrue)
					mdr1 |= LS7366Left_MDR1_BW_FLAG_MASK;
				if(config[i].FlagOnCarry == enBooleanTrue)
					mdr1 |= LS7366Left_MDR1_CY_FLAG_MASK;
				pfMemCopy(&QeiLS7366Config[loop], &config[i], sizeof(PFCfgQeiLS7366));
				// Enable count
				pfGpioPinsSet(config[i].countEnGpio.port, config[i].countEnGpio.pin);
				pfQeiLS7366RegWrite(loop,LS7366Left_IR_REG_MDR0, mdr0);
				pfQeiLS7366RegWrite(loop,LS7366Left_IR_REG_MDR1, mdr1);
				pfQeiLS7366RegClear(loop,LS7366Left_IR_REG_CNTR);
				
		// 	pfQeiLS7366RegOperation(LS_OPN_WRITE, LS_REG_MDR0, 1, &mdr0, 0); 
		// 	pfQeiLS7366RegOperation(LS_OPN_WRITE, LS_REG_MDR1, 1, &mdr1, 0);
		// 	pfQeiLS7366RegOperation(LS_OPN_CLR, LS_REG_CNTR, 1, 0, 0);
				
				id[i] =loop;
				break;
			}
			
			if(i== PF_MAX_QEI_SUPPORTED)
			return enStatusError;
			
		}
	}
	QeiLS7366Init = enBooleanTrue;
	return enStatusSuccess;
}

PFEnStatus pfQeiLS7366Close(PFbyte id)
{
	
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif
	pfQeiLS7366CountEnable(id,enBooleanFalse);
	QeiLS7366Config[id].spiUnregisterDevice(&id);
	QeiLS7366Init = enBooleanFalse;
	qeiDeviceCount++;
	return enStatusSuccess;
}

PFEnStatus pfQeiLS7366CountEnable(PFbyte id,PFEnBoolean status)
{
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif
	if(status == enBooleanTrue)
	{
		pfGpioPinsSet(QeiLS7366Config[id].countEnGpio.port, QeiLS7366Config[id].countEnGpio.pin);
		//pfQeiLS7366RegOperation(LS_OPN_CLR, LS_REG_CNTR, 1, 0, 0);
		pfQeiLS7366RegClear(id,LS7366Left_IR_REG_CNTR);
		return enStatusSuccess;
	}
	else
	{
		pfGpioPinsClear(QeiLS7366Config[id].countEnGpio.port, QeiLS7366Config[id].countEnGpio.pin);
		return enStatusSuccess;
	}
	
	
}


PFEnStatus pfQeiLS7366ReadCount(PFbyte id,PFdword* count)
{
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif	
	
// 	pfQeiLS7366RegOperation(LS_OPN_LOAD, LS_REG_OTR, 1, 0, 0);
// 	pfQeiLS7366RegOperation(LS_OPN_READ, LS_REG_OTR, 4, 0, count);

	if(pfQeiLS7366RegLoad(id,LS7366Left_IR_REG_OTR) != enStatusSuccess)
		return enStatusError;
	if(pfQeiLS7366RegRead(id,LS7366Left_IR_REG_OTR, count) != enStatusSuccess)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfQeiLS7366ResetCount(PFbyte id)
{
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif

//	pfQeiLS7366RegOperation(LS_OPN_CLR, LS_REG_CNTR, 1, 0, 0);
	if(pfQeiLS7366RegClear(id,LS7366Left_IR_REG_CNTR) != enStatusSuccess)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfQeiLS7366ReadDataReg(PFbyte id,PFdword* data)
{
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif
	if(pfQeiLS7366RegRead(id,LS7366Left_IR_REG_DTR, data) != enStatusSuccess)
		return enStatusError;
	
	return enStatusSuccess;
}


PFEnStatus pfQeiLS7366WriteDataReg(PFbyte id,PFdword data)
{
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif
	if(pfQeiLS7366RegWrite(id,LS7366Left_IR_REG_DTR, data) != enStatusSuccess)
		return enStatusError;
	
	return enStatusSuccess;
}

PFEnStatus pfQeiLS7366ReadStatus(PFbyte id,PFdword* status)
{
#if (PF_LS7366_DEBUG == 1)
	if(QeiLS7366Init == enBooleanFalse)
	return enStatusNotConfigured;
	if(id > PF_MAX_QEI_SUPPORTED)
	return enStatusInvArgs;
#endif

	if(pfQeiLS7366RegRead(id,LS7366Left_IR_REG_STR, status) != enStatusSuccess)
		return enStatusError;
	
	return enStatusSuccess;
}

