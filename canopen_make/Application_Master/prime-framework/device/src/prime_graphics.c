#include "prime_framework.h"
#include "prime_types.h"
#include "prime_gpio.h"
#include "prime_font8x8.h"
#include "prime_font8x16.h"
#include "prime_font24x16.h"
#include "prime_graphics.h"
#include "prime_lcd.h"

/*
 * To Draw Fast line on the LCD Display
 *
 * param X and Y Coordinate of the one end of the fast line	
 *
 * param X and Y Coordinate of the other end of the fast line
 *
 * return: status of whether fast line is drawn or not
 * 
 */
 
 static PFEnStatus pfGfxDrawFastLine(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2);
/**
 * To Draw quarter circle on the LCD Display
 *
 * param X and Y Coordinate of the center of the circle
 *
 * param Radius of the quarter circle
 *
 * param Quadrant of the quarter circle
 *
 * return status of whether quarter circle is drawn or not
 * 
 */
 PFEnStatus pfGfxDrawQuarterCircle(const PFsdword xc, const PFsdword yc, PFsdword radius, const PFEnQuadrant quadrant);
/**

 * To Draw Elliptical arc in one quadrant on the LCD Display
 *
 * \param  X and Y Coordinate of the ellipse
 *
 * \param Constant of the ellipse (a and b)
 *
 * \param Start angle of the arc to be drawn
 *
 * \param Extent of the arc
 *
 * \param Quadrant of the ellipse 
 *
 * \return status of whether elliptical arc is drawn or not
 * 
 */
PFEnStatus pfGfxDrawEArcInOneQuadrant(const PFsdword xc, const PFsdword yc, const PFsdword a, const PFsdword b, const PFsdword startAngle, PFsdword extent, PFEnQuadrant quadrant);
/**

 * To Draw the elliptical arc on the LCD Display
 *
 * \param X and Y Coordinate of Elliptical arac center
 *
 * \param Constant of the ellipse (a and b)
 *
 * \param Start angle  of the arc 
 *
 * \param  Extent angle of the arc
 *
 * \return status of whether Elliptical arc is drawn or not
 * 
 */
PFEnStatus pfGfxDrawEllipticalArc(const PFdword xc, const PFdword yc, const PFdword a, const PFdword b, PFdword startAngle, PFsdword extent);
 // The constant \a pi.	
#define PI			3.14159265358979323846		// pi 

// The constant \a pi/2.	
#define PI_2		1.57079632679489661923		// pi / 2 

// The constant \a pi*2.	
#define PIx2		6.28318530717958647692		// PI * 2	
#define MakeColour(R, G, B)			( ((R & 0xF8) << 8) | ((G & 0xFC) << 3) | (B >> 3) )
#define GetRed(CLR)					( (CLR >> 8) & 0xF8 )
#define GetGreen(CLR)					( (CLR >> 3) & 0xFC )
#define GetBlue(CLR)					( (CLR << 3) & 0xFF )

#define gMIN(a, b)						( (a < b) ? a : b )
#define gMAX(a, b)						( (a > b) ? a : b )

#define AbsVal(a)						( (a) > 0 ? (a) : -(a) )

#define XMAX_PIXELS						239
#define YMAX_PIXELS 					319


const PFdword sin_lookup[10] = {1736, 3420, 5000, 6427, 7070, 7660, 8660, 9397, 9848, 10000};
const PFdword cos_lookup[10] = {10000, 9848, 9397, 8660, 7660, 7070, 6427, 5000, 3420, 1736};	

static PFdword _gColor = 0;
static volatile PFdword _pen_size = 2;

static PFsword dbgX = 0, dbgY = 0;
static PFbyte  dbgFontType = 1, *pDbgFont = 0;
static PFsdword dbgFontH = 16, dbgFontW = 8, dbgFontClr = WHITE, dbgFontBack = BLACK;
static PFsdword scrollLines = 0, startScroll = 0;
PFEnStatus pfGfxSortPointsInX(PFsdword *x1, PFsdword *y1, PFsdword *x2, PFsdword *y2, PFsdword *x3, PFsdword *y3);
PFEnStatus pfGfxSortPointsInY(PFsdword *x1, PFsdword *y1, PFsdword *x2, PFsdword *y2, PFsdword *x3, PFsdword *y3);
static PFdword pfGfxSin(PFdouble angRadians)	
{
	volatile PFdword angDeg = angRadians * 57.2958;
	volatile PFdword val;
	
	angDeg = angDeg % 360;	
	
	val = sin_lookup[(angDeg%90)/10];
	
	if (angDeg > 180)
	{
		return -(val);
	}
	return (val);
}

static PFdword pfGfxCos(PFdouble angRadians)	
{
	volatile PFdword angDeg = angRadians * 57.2958;
	volatile PFdword val;
	
	angDeg = angDeg % 360;	
	
	val = cos_lookup[(angDeg%90)/10];
	
	if (angDeg > 90 && angDeg < 270)
	{
		return -(val);
	}
	return (val);
}

PFEnStatus pfGfxSetPenSize(const PFdword size)
{
	_pen_size = size;			// Set pen size
	return enStatusSuccess;			// return same pen size for reference
}

PFEnStatus pfGfxGetPenSize(PFword *data)
{
	*data = _pen_size;
	return enStatusSuccess;
}

 PFEnStatus pfGfxSetColor(const PFdword color)
{
	_gColor = color;			// Set pen size
	return enStatusSuccess;			// return same pen size for reference
}

 PFEnStatus pfGfxGetColor( PFword *data)
{
	*data = _gColor; 
	return enStatusSuccess;
}

 PFEnStatus pfGfxPixels(const PFdword xPos, const PFdword yPos, const PFdword size, PFword color)
{
	PFdword loopX, loopY, n;
	n = size-1;
	
	for (loopX = xPos-(n/2); loopX <= xPos+(n-(n/2)); loopX++)			// Put 'size' number of pixels around given x-y coordinate
	{
		for (loopY = yPos-(n/2); loopY <= yPos+(n-(n/2)); loopY++)
			pfILI9320DrawPixel(loopX, loopY, color);
	}
	return enStatusSuccess;
}


PFEnStatus pfGfxDrawPixels(const PFdword xPos, const PFdword yPos)
{
	PFword color = 0 ;
	PFword data;
	pfGfxGetColor(&color);
	pfGfxGetPenSize(&data);
	pfGfxPixels(xPos, yPos, data, color);
	return enStatusSuccess;
}

 PFEnStatus pfGfxDrawRectangle(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2)
{
	 pfGfxDrawLine(x1, y1, x2, y1);				// top line
	 pfGfxDrawLine(x2, y1, x2, y2);				// right vertical
	 pfGfxDrawLine(x1, y2, x2, y2);				// bottom line
	 pfGfxDrawLine(x1, y1, x1, y2);				// left vertical
	return enStatusSuccess;
}

static PFEnStatus pfGfxDrawFastLine(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2)
{
	PFdword start, end;
	PFdword size = _pen_size - 1;
	
	if (x1 == x2)								// Line is parallel to Y axis
	{
		start = gMIN(y1, y2);					// get the upper point as start and lower as end
		end = gMAX(y1, y2);
		pfILI9320FillArea(x1-(size/2), start, x2+(size-(size/2)), end, _gColor);
	}
	else										// Line is parallel to X axis
	{
		start = gMIN(x1, x2);					// get the left point as start and right as end
		end = gMAX(x1, x2);
		pfILI9320FillArea(start, y1-(size/2), end, y2+(size-(size/2)), _gColor);
	}
	return enStatusSuccess;
}

 PFEnStatus pfGfxDrawSolidCircle(const PFsdword xc, const PFsdword yc, PFsdword radius, const PFsdword color)
{
	volatile PFsdword x = -radius;
	volatile PFsdword y = 0;
	volatile PFsdword err = 2 - 2*radius;									// II. Quadrant 
	PFword prevColor = 0;
	pfGfxGetColor(&prevColor);
	pfGfxSetColor(color);
	do 
	{
      pfGfxDrawFastLine(xc - x, yc + y, xc + x, yc + y);	
	  pfGfxDrawFastLine(xc - y, yc + x, xc + y, yc + x);	
	  
	  radius = err;
      if (radius <= y)										// e_xy+e_y < 0
		err += ++y*2+1;						
      
	  if (radius > x || err > y)								// e_xy+e_x > 0 or no 2nd y-step 
		err += ++x*2+1;				
   } while (x < 0);
   
   pfGfxSetColor(prevColor);
   return enStatusSuccess;
}

 PFEnStatus pfGfxDrawSolidRectangle(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2, const PFdword color)
{
	pfILI9320FillArea(x1, y1, x2, y2, color);
	return enStatusSuccess;
	}

PFEnStatus pfGfxDrawSolidQuarterCircle(const PFsdword xc, const PFsdword yc, PFsdword radius,const PFEnQuadrant quadrant, const PFsdword color)
{
	PFsdword xPos = -radius;
	PFsdword yPos = 0;
	volatile PFsdword err = 2 - 2*radius;										// II. Quadrant  
	PFword  prevColor = 0;
 	pfGfxGetColor(&prevColor);
	pfGfxSetColor(color);
	
	do 
	{
		if(quadrant == enQuadrant_III)
			pfGfxDrawFastLine(xc, yc - yPos, xc + xPos, yc - yPos);						// III. Quadrant 
		if(quadrant == enQuadrant_IV)
			pfGfxDrawFastLine(xc, yc + xPos, xc + yPos, yc + xPos);						//  IV. Quadrant 
		if(quadrant == enQuadrant_I)
			pfGfxDrawFastLine(xc, yc + yPos, xc - xPos, yc + yPos);						//   I. Quadrant 
		if(quadrant == enQuadrant_II)
			pfGfxDrawFastLine(xc, yc - xPos, xc - yPos, yc - xPos);						//  II. Quadrant 	
		
		radius = err;
		if (radius <= yPos)
			err += ++yPos*2+1;									// e_xy+e_y < 0 
		if (radius > xPos || err > yPos) 
			err += ++xPos*2+1;									// e_xy+e_x > 0 or no 2nd y-step 
   } while (xPos < 0);
   
   pfGfxSetColor(prevColor);
   return enStatusSuccess;
}

 PFEnStatus pfGfxDrawLine( PFsdword x1, PFsdword y1, const PFsdword x2, const PFsdword y2)
{	
	PFsdword dx =  AbsVal(x2 - x1);
	PFsdword xStart = x1 < x2 ? 1 : -1;
	PFsdword dy = -AbsVal(y2 - y1);
	PFsdword yStart = y1 < y2 ? 1 : -1; 
	volatile PFsdword err = dx+dy, e2;													// error value e_xy 
	if((x1 == x2) || (y1 == y2))
	{
		pfGfxDrawFastLine(x1, y1, x2, y2);
		return enStatusSuccess;
	}
	while(1)
	{  
		pfGfxPixels(x1, y1, _pen_size, _gColor);
		if (x1==x2 && y1==y2) 
			break;
		
		e2 = 2*err;
		if (e2 >= dy)														// e_xy+e_x > 0 			
		{ 
			err += dy; 
			x1 += xStart; 
		}								
		if (e2 <= dx)														// e_xy+e_y < 0 
		{ 
			err += dx; 
			y1 += yStart; 
		}								
	}
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawQuarterCircle(const PFsdword xc, const PFsdword yc, PFsdword radius, const PFEnQuadrant quadrant)
{
	PFsdword xPos = -radius;
	PFsdword yPos = 0;
	PFEnQuadrant quadrantData=0;
	volatile PFdword err = 2 - 2*radius;	// II. Quadrant  
	quadrantData = quadrant;
   
   	do 
	{
		if(quadrantData == enQuadrant_III)
		{
			//lcdPutPixel(xc + xPos, yc - yPos, _gColor);						// III. Quadrant 
			pfGfxPixels(xc + xPos, yc - yPos, _pen_size, _gColor);						// III. Quadrant 
		}
		if(quadrantData == enQuadrant_IV)
		{
			//lcdPutPixel(xc + yPos, yc + xPos, _gColor);						//  IV. Quadrant 
			pfGfxPixels(xc + yPos, yc + xPos, _pen_size, _gColor);						//  IV. Quadrant 
		}
		if(quadrantData == enQuadrant_I)
		{
			//lcdPutPixel(xc - xPos, yc + yPos, _gColor);						//   I. Quadrant 
			pfGfxPixels(xc - xPos, yc + yPos, _pen_size, _gColor);						//   I. Quadrant 
		}
		if(quadrantData == enQuadrant_II)
		{
			//lcdPutPixel(xc - yPos, yc - xPos, _gColor);						//  II. Quadrant 	
			pfGfxPixels(xc - yPos, yc - xPos, _pen_size, _gColor);						//  II. Quadrant 	
		}
		
		radius = err;
		if (radius <= yPos)
			err += ++yPos*2+1;									// e_xy+e_y < 0 
		if (radius > xPos || err > yPos) 
			err += ++xPos*2+1;									// e_xy+e_x > 0 or no 2nd y-step 
   } while (xPos < 0); 
   return enStatusSuccess;
}
#if(PF_USE_FONT == 1)
PFEnStatus pfGfxDrawChar(PFdword x, PFdword y, PFchar character, PFEnGraphicFonts fontType, PFdword fontColor, PFdword backColor)
{
	 PFdword xIndex, yIndex, fData;
	 PFdword fontHeight, fontWidth, chrTemp;
	 PFbyte* fontPointer;
	PFword* fontWordPointer;
	volatile  PFdword tempPtr = 0;
	
	switch((PFEnGraphicFonts)fontType)
	{
		case enFont_8X8:
			fontHeight = 8;
			fontWidth = 8;
			fontPointer = (PFbyte*)Font8x8;
			break;
			
		case enFont_8X16:
			fontHeight = 16;
			fontWidth = 8;
			fontPointer = (PFbyte*)Font8x16;
			break;
		
		case enFont_24X16:
			fontHeight = 24;
			fontWidth = 16;
			fontWordPointer = (PFword*)Font24x16;
			character -= 32;
			break;

		case enFont_16X16: return enStatusNotSupported;
		case enFont_32X32: return enStatusNotSupported;
	}
	
	if((fontType == enFont_8X8) || (fontType == enFont_8X16))
	{
		for(yIndex = 0; yIndex < fontHeight; yIndex++) 
		{
			fData = (*(PFbyte*)(fontPointer + ((character * fontHeight) + yIndex)));
			fData <<= 8;
			
			for(xIndex = 0; xIndex < fontWidth; xIndex++) 
			{
				if( (fData & 0x8000) == 0x8000 ) 
				{
					//lcdWriteData(fontColor);
					pfILI9320DrawPixel(x+xIndex, y+yIndex, fontColor);
				}
				else 
				{
					//lcdWriteData(backColor);
					pfILI9320DrawPixel(x+xIndex, y+yIndex, backColor);
				}
				fData <<= 1;
			}
		}
	}
	else
	{
		chrTemp = character;
		for(yIndex = 0; yIndex < fontHeight; yIndex++) 
		{
			tempPtr = 0;
			tempPtr = ((character * fontHeight) + yIndex);
			//fData = getWord(fontWordPointer + ((chrTemp * fontHeight) + yIndex));
			fData = (*(PFword*)(fontWordPointer + tempPtr));
						
			for(xIndex = 0; xIndex < fontWidth; xIndex++) 
			{
				if( (fData & 0x0001) == 0x0001 ) 
				{
					//lcdWriteData(fontColor);
					pfILI9320DrawPixel(x+xIndex, y+yIndex, fontColor);
				}
				else 
				{
					//lcdWriteData(backColor);
					pfILI9320DrawPixel(x+xIndex, y+yIndex, backColor);
				}
				fData >>= 1;
			}
		}
	}
	return enStatusSuccess;
}

volatile PFword* fontWordPointer = 0;
PFEnStatus pfGfxDrawChar24x16(PFdword x, PFdword y, PFchar character, PFbyte fontType, PFdword fontColor, PFdword backColor)
{
	 PFdword xIndex, yIndex, fData;
	 PFbyte fontHeight = 24, fontWidth = 16;
	PFword * const fPtr = (PFword*)Font24x16;
	volatile PFdword tempPtr = 0;
	
	fontHeight = 24;
	fontWidth = 16;
	fontWordPointer = (PFword*)Font24x16;
	character -= 32;
	
	for(yIndex = 0; yIndex < fontHeight; yIndex++) 
	{
		tempPtr = 0;
		tempPtr = ((character * fontHeight) + yIndex);
		fontWordPointer = fPtr + tempPtr;
		//fData = getWord(fontWordPointer + ((chrTemp * fontHeight) + yIndex));
		fData = (*(PFword*)(fontWordPointer));
					
		for(xIndex = 0; xIndex < fontWidth; xIndex++) 
		{
			if( (fData & 0x0001) == 0x0001 ) 
			{
				//lcdWriteData(fontColor);
				pfILI9320DrawPixel(x+xIndex, y+yIndex, fontColor);
			}
			else 
			{
				//lcdWriteData(backColor);
				pfILI9320DrawPixel(x+xIndex, y+yIndex, backColor);
			}
			fData >>= 1;
		}
	}
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawString(PFdword x, PFdword y, PFchar *string, PFEnGraphicFonts fontType, PFdword fontColor, PFdword backColor) 
{
	volatile PFbyte fontHeight, fontWidth;
	
	switch(fontType)
	{
		case enFont_8X8:
			fontHeight = 8;
			fontWidth = 8;
			break;
		case enFont_8X16:
			fontHeight = 16;
			fontWidth = 8;
			break;
		case enFont_24X16:
			fontHeight = 24;
			fontWidth = 16;
			break;
		case enFont_16X16: return enStatusNotSupported;
		case enFont_32X32: return enStatusNotSupported;
	}
	
	while(*string) 
	{
		if(*string == '\n')
			y += fontHeight;
		else if(*string == '\r')
			x = 0;	
		else	
		{
			if(fontType == enFont_24X16)
				pfGfxDrawChar24x16( x, y, *string, fontType, fontColor, backColor );
			else
				pfGfxDrawChar( x, y, *string, fontType, fontColor, backColor );
			x += fontWidth;	
		}
		string++;
	}
	return enStatusSuccess;
}
#endif// #if(PF_USE_FONT == 1)

 PFEnStatus pfGfxDrawSolidRoundRectangle(const PFdword x1, const PFdword y1, const PFdword x2, const PFdword y2, PFdword radius, const PFdword color)
{
	PFword prevColor = 0;
	
	PFdword xStart = 0;
	PFdword yStart = 0;
	PFdword xEnd = 0;
	PFdword yEnd = 0;
	PFdword a = 0;
	PFdword b = 0;
	PFdword minDim = 0;
	pfGfxGetColor(&prevColor); 
	 xStart = gMIN(x1, x2);
	 yStart = gMIN(y1, y2);
	
	 xEnd = gMAX(x1, x2);
	 yEnd = gMAX(y1, y2);
	
	 a = xEnd - xStart;
	 b = yEnd - yStart;
	
	
	minDim = gMIN(a, b);							// radius should not be greater than half of the minor side
	
	if(2 * radius > minDim)
	{
		radius = minDim / 2;
	}
	
	pfGfxSetColor(prevColor);
	
	// pfGfxDraw corner circles
	pfGfxDrawSolidQuarterCircle(xStart + radius, yStart + radius, radius, enQuadrant_III, color);	// left top
	pfGfxDrawSolidQuarterCircle(xEnd - radius, yStart + radius, radius, enQuadrant_IV, color);		// right top
	pfGfxDrawSolidQuarterCircle(xStart + radius, yEnd - radius, radius, enQuadrant_II, color);		// left bottom
	pfGfxDrawSolidQuarterCircle(xEnd - radius, yEnd - radius, radius, enQuadrant_I, color);		// right bottom
	
	pfGfxDrawSolidRectangle(xStart + radius, yStart, xEnd - radius, yStart + radius, color);		// top
	pfGfxDrawSolidRectangle(xStart, yStart + radius, xEnd, yEnd - radius, color);				// middle
	pfGfxDrawSolidRectangle(xStart + radius, yEnd - radius, xEnd - radius, yEnd, color);			// bottom
	pfGfxSetColor(prevColor);
	return enStatusSuccess;
}

 PFEnStatus pfGfxDrawSolidEllipse(const PFsdword xc, const PFsdword yc, const PFsdword a, const PFsdword b, const PFsdword color)
{
	volatile PFsdword x = -a;
	volatile PFsdword y = 0;													// II. quadrant from bottom left to top right 
	volatile PFsdword e2 = b; 
	PFsdword dx = (1 + 2*x)*e2*e2;									// error increment  
	PFsdword dy = x*x;
	volatile PFsdword err = dx+dy;											// error of 1.step 
	PFword prevColor = 0;
	 pfGfxGetColor(&prevColor);
	
	pfGfxSetColor(color);
 
	do 
	{
		pfGfxDrawFastLine(xc - x, yc + y, xc + x, yc + y);		// I quadrant to II quadrant
		pfGfxDrawFastLine(xc - x, yc - y, xc + x, yc - y);		// III quadrant to IV quadrant
		e2 = 2*err;
		if (e2 >= dx) { x++; err += dx += 2*(PFsdword)b*b; }		// x step 
		if (e2 <= dy) { y++; err += dy += 2*(PFsdword)a*a; }		// y step 
	} while (x <= 0);
 
	while (y++ < b)												// too early stop for flat ellipses with a=1
	{   
		pfILI9320DrawPixel(xc, yc + y, _gColor);									// -> finish tip of ellipse 
		pfILI9320DrawPixel(xc, yc - y, _gColor); 
	}
	
	pfGfxSetColor(prevColor);
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawPolygon(const PFdword *xArray, const PFdword *yArray, const PFdword num)
{
	volatile PFdword indxEnd = 0;
	
	for (indxEnd = 0; indxEnd < (num-1); indxEnd++)			// Connect each point in the array to the next one
	{
		pfGfxDrawLine(*(xArray+indxEnd), *(yArray+indxEnd), *(xArray+indxEnd + 1), *(yArray+indxEnd + 1));
	}
	
	// Connect the last point in the array to the first one to close the polygon
	 pfGfxDrawLine(*(xArray), *(yArray), *(xArray+num - 1), *(yArray+num - 1));
	return enStatusSuccess;
}

 PFEnStatus pfGfxDrawCircularLines(const PFdword xc, const PFdword yc,const PFdword radius, const PFdword num)
{
	volatile PFdword count;
	const PFdouble angle = 2 * PI / num;			// Calculate angle between two lines
	volatile PFdouble x, y;
	
	for (count = 0; count < num; count++)
	{
		x = xc + ( radius * pfGfxCos(count * angle) );
		y = yc + ( radius * pfGfxSin(count * angle) );
		 pfGfxDrawLine(xc, yc, (PFdword)x, (PFdword)y);
	}
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawArcInOneQuadrant(const PFdword xc, const PFdword yc, PFdword radius, const PFdword startAngle, PFdword extent,  PFEnQuadrant quadrant)
{
	volatile PFdouble xStart, yStart, xStop, yStop;
	volatile PFdword angDegStart;
	volatile PFdword angDegEnd;
	volatile PFdouble angRedStart;
	volatile PFdouble angRedEnd;
	
	volatile PFdword x		= -radius;
	volatile PFdword y		= 0;
	volatile PFdword err	= 2 - 2*radius;	
	
	// Check if extent is negative
	if(extent < 0)
	{
		angDegStart = startAngle + extent;
		angDegEnd = startAngle;
		extent = -extent;
	}
	else
	{
		angDegStart = startAngle;
		angDegEnd = startAngle + extent;
	}				
		
	// clamp down start and end angles between 0 to 90
	if(angDegStart < 0)
		angDegStart = 0;
	if(angRedEnd > 90)
		angDegEnd = 90;
		
	angRedStart = angDegStart * 2.0 * PI / 360.0;
	angRedEnd = angDegEnd * 2.0 * PI / 360.0;
	
	
#ifdef USE_MATH_H
	// Calculate xy coordinates for start angle
	xStart = radius * pfGfxCos(angRedStart);
	yStart = radius * pfGfxSin(angRedStart);
	// Calculate xy coordinates for stop angle
	xStop = radius * pfGfxCos(angRedEnd);
	yStop = radius * pfGfxSin(angRedEnd);
#else
	// Calculate xy coordinates for start angle
	xStart = radius * pfGfxCos(angRedStart) / 10000;
	yStart = radius * pfGfxSin(angRedStart) / 10000;
	// Calculate xy coordinates for stop angle
	xStop = radius * pfGfxCos(angRedEnd) / 10000;
	yStop = radius * pfGfxSin(angRedEnd) / 10000;
#endif	
						// II. Quadrant 
	
	// Split the circle in 4 quadrants and draw pixels in given quadrants only
	do 
	{
		if( y >= yStart )
		{
			switch(quadrant)
			{
				case enQuadrant_I:
					pfILI9320DrawPixel(xc - x, yc + y, _gColor);						//   I. Quadrant 
					break;
				case enQuadrant_II:
					pfILI9320DrawPixel(xc - y, yc - x, _gColor);						//  II. Quadrant 
					break;
				case enQuadrant_III:
					pfILI9320DrawPixel(xc + x, yc - y, _gColor);						// III. Quadrant 
					break;
     			case enQuadrant_IV:
					pfILI9320DrawPixel(xc + y, yc + x, _gColor);						//  IV. Quadrant 
					break;
				default:
					break;
			}
			
		}			
		if( - x < xStop )
			break;
			
		radius = err;
		if (radius <= y)										// e_xy + e_y < 0
			err += ++y*2+1;						
      
		if (radius > x || err > y)								// e_xy + e_x > 0 or no 2nd y-step 
			err += ++x*2+1;				
   } while (x < 0);
   return enStatusSuccess;
}


 PFEnQuadrant getQuadrant(PFdword angInDeg)
{
	angInDeg = angInDeg % 360;							// Make angle < 360
	
	if((angInDeg < 90 && angInDeg >= 0) || (angInDeg >= -360 && angInDeg <= -270))
		return enQuadrant_I;
	else if((angInDeg < 180 && angInDeg >= 90) || (angInDeg >= -270 && angInDeg <= -180))
		return enQuadrant_II;
	else if((angInDeg < 270 && angInDeg >= 180) || (angInDeg >= -180 && angInDeg <= -90))
		return enQuadrant_III;
	else
		return enQuadrant_IV;
}

 PFEnQuadrant getNextQuadrant(const PFEnQuadrant quad)
{
	if (quad == enQuadrant_IV)
		return enQuadrant_I;
	else
		return quad + 1;		// As there are only 4 quadrants, reset the number to 1 after 4
}

PFEnStatus pfGfxDrawCircle(const PFsdword xc, const PFsdword yc, PFsdword radius)
{
	volatile PFsdword x = -radius;
	volatile PFsdword y = 0;
	volatile PFsdword err = 2 - 2*radius;									// II. Quadrant 
	
	do 
	{
		//lcdPutPixel(xc - x, yc + y, _gColor);						//   I. Quadrant 
		pfGfxPixels(xc - x, yc + y, _pen_size, _gColor);						//   I. Quadrant 
		//lcdPutPixel(xc - y, yc - x, _gColor);						//  II. Quadrant 
		pfGfxPixels(xc - y, yc - x, _pen_size, _gColor);						//  II. Quadrant 
		//lcdPutPixel(xc + x, yc - y, _gColor);						// III. Quadrant 
		pfGfxPixels(xc + x, yc - y, _pen_size, _gColor);						// III. Quadrant 
		//lcdPutPixel(xc + y, yc + x, _gColor);						//  IV. Quadrant 
		pfGfxPixels(xc + y, yc + x, _pen_size, _gColor);						//  IV. Quadrant 
      
		radius = err;
		if (radius <= y)										// e_xy+e_y < 0
			err += ++y*2+1;						
      
		if (radius > x || err > y)								// e_xy+e_x > 0 or no 2nd y-step 
			err += ++x*2+1;				
	} while (x < 0);
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawQCircle(const PFsdword xc, const PFsdword yc, PFsdword radius, PFEnQuadrant quad)
{
	volatile PFsdword x = -radius;
	volatile PFsdword y = 0;
	volatile PFsdword err = 2 - 2*radius;									// II. Quadrant 
	
	do 
	{
		switch(quad)
		{
			case enQuadrant_I:
				pfGfxPixels(xc - x, yc + y, _pen_size, _gColor);						//   I. Quadrant 
				break;
			
			case enQuadrant_II:
				pfGfxPixels(xc - y, yc - x, _pen_size, _gColor);						//  II. Quadrant 
				break;
		
			case enQuadrant_III:
				pfGfxPixels(xc + x, yc - y, _pen_size, _gColor);						// III. Quadrant 
				break;
			
			case enQuadrant_IV:
				pfGfxPixels(xc + y, yc + x, _pen_size, _gColor);						//  IV. Quadrant 
				break;
		}
      
		radius = err;
		if (radius <= y)										// e_xy+e_y < 0
			err += ++y*2+1;						
      
		if (radius > x || err > y)								// e_xy+e_x > 0 or no 2nd y-step 
			err += ++x*2+1;				
	} while (x < 0);
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawArc(PFsdword xc, PFsdword yc, PFsdword radius, PFsdword startAngle, PFsdword extent)
{
	volatile PFsdword angStart;
	volatile PFsdword angEnd;
	PFEnQuadrant startQuad;
	PFEnQuadrant endQuad; 
	PFEnQuadrant currQuad;
	PFsdword clampedAngStart = angStart % 90;
	PFsdword clampedAngEnd = angEnd % 90;
	if(extent <= -360 || extent >= 360)		// if extent is more than 360, arc will result in circle
	{
		pfGfxDrawCircle(xc, yc, radius);
		return enStatusSuccess;
	}
	
	// forcing angle to lie between 0 and 360 even if it is negative.
	startAngle = startAngle % 360;
	

	// making extent positive and angDegStart to be always less than angDegEnd.
	if(extent < 0)
	{
		angStart = startAngle + extent;
		angEnd = startAngle;
		if(angStart < 0)
		{
			angStart = 360 - (-angStart) % 360;
			angEnd = 360 + startAngle;
		}
		extent = -extent;
	}
	else
	{
		angStart = startAngle;
		angEnd = startAngle + extent;
	}	
	
	 startQuad = getQuadrant(angStart);
	 endQuad = getQuadrant(angEnd);

	if(startQuad != endQuad || clampedAngStart  > clampedAngEnd)			// If arc covering more than one quadrants
	{
		currQuad = startQuad;
		do 
		{
			pfGfxDrawArcInOneQuadrant(xc, yc, radius, clampedAngStart, 90, currQuad);	//pfGfxDraw arc of 90 deg for intermediate quadrants
			clampedAngStart = 0;
			currQuad = getNextQuadrant(currQuad);
			
		} while (endQuad != currQuad);
		
		pfGfxDrawArcInOneQuadrant(xc, yc, radius, 0, clampedAngEnd, endQuad);		// pfGfxDraw remaining part in last quadrant
	}
	else
	{
		pfGfxDrawArcInOneQuadrant(xc, yc, radius, clampedAngStart, extent, startQuad);		// If arc lies in only one quadrant
	}						
	return enStatusSuccess;			
}

 PFEnStatus pfGfxDrawEArcInOneQuadrant(const PFsdword xc, const PFsdword yc, const PFsdword a, const PFsdword b, const PFsdword startAngle, PFsdword extent, PFEnQuadrant quadrant)
{
	volatile PFdouble xStart, yStart, xStop, yStop;
	volatile PFsdword angDegStart;
	volatile PFsdword angDegEnd;
	volatile PFdouble angRedStart;
	volatile PFdouble angRedEnd;
	PFsdword x = -a;
	PFsdword y = 0;													// II. quadrant from bottom left to top right 
	PFsdword e2 = b; 
	PFsdword dx = (1 + 2*x)*e2*e2;									// error increment  
	PFsdword dy = x*x;
	PFsdword err = dx+dy;											// error of 1.step
	if(extent < 0)
	{
		angDegStart = startAngle + extent;
		angDegEnd = startAngle;
		extent = -extent;
	}
	else
	{
		angDegStart = startAngle;
		angDegEnd = startAngle + extent;
	}				
		
	// clamp down start and end angles between 0 to 90
	if(angDegStart < 0)
		angDegStart = 0;
	if(angDegEnd > 90)
		angDegEnd = 90;
		
	angRedStart = (angDegStart * 2.0 * PI) / 360.0;
	angRedEnd = (angDegEnd * 2.0 * PI) / 360.0;
	
	xStart = ((a) * pfGfxCos(angRedStart))/10000;
	yStart = ((b) * pfGfxSin(angRedStart))/10000;
	
	xStop = ((a) * pfGfxCos(angRedEnd))/10000;
	yStop = ((b) * pfGfxSin(angRedEnd))/10000;
	
 
	
	do 
	{
		if((quadrant == enQuadrant_II) || (quadrant == enQuadrant_IV))
		{
			if(-x < xStart+1)
			{
				switch(quadrant)
				{
				case enQuadrant_II:
					pfILI9320DrawPixel(xc + x, yc + y, _gColor);								//  II. Quadrant 
					break;
				case enQuadrant_IV:
					pfILI9320DrawPixel(xc - x, yc - y, _gColor);								//  IV. Quadrant 
					break;
				default:
					break;
				}
			}	
			if(y > yStop+1)
				break;
		}			
		
		if((quadrant == enQuadrant_I) || (quadrant == enQuadrant_III))
		{
			if( y >= yStart )
			{
				switch(quadrant)
				{
					case enQuadrant_I:
						pfILI9320DrawPixel(xc - x, yc + y, _gColor);								//   I. Quadrant 
						break;
					case enQuadrant_III:
						pfILI9320DrawPixel(xc + x, yc - y, _gColor);								// III. Quadrant 
						break;
     				default:
						break;
				}
			
			}	
			if(-x < xStop)
			{
				break;			
			}				
		}				
				
		e2 = 2*err;
		if (e2 >= dx)											// x step 
		{
			x++; 
			err += dx += 2*(PFsdword)b*b;
		}		
		if (e2 <= dy)											// y step 
		{ 
			y++; 
			err += dy += 2*(PFsdword)a*a;
		}		
	} while (x <= 0);
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawEllipse_Bresenham(const PFdword xc, const PFdword yc, const PFdword a, const PFdword b)
{
	volatile PFsdword x = -a;
	volatile PFsdword y = 0;													// II. quadrant from bottom left to top right 
	volatile PFsdword e2 = b; 
	PFsdword dx = (1 + 2*x)*e2*e2;									// error increment  
	PFsdword dy = x*x;
	volatile PFsdword err = dx+dy;											// error of 1.step 
 
	do 
	{
		pfILI9320DrawPixel(xc - x, yc + y, _gColor);								//   I. Quadrant 
		pfILI9320DrawPixel(xc + x, yc + y, _gColor);								//  II. Quadrant 
		pfILI9320DrawPixel(xc + x, yc - y, _gColor);								// III. Quadrant 
		pfILI9320DrawPixel(xc - x, yc - y, _gColor);								//  IV. Quadrant 
		e2 = 2*err;
		if (e2 >= dx)											// x step 
		{
			x++; 
			err += dx += 2*(PFsdword)b*b; 
		}		
		if (e2 <= dy)											// y step 
		{ 
			y++; 
			err += dy += 2*(PFsdword)a*a; 
		}		
	} while (x <= 0);
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawEllipticalArc(const PFdword xc, const PFdword yc, const PFdword a, const PFdword b, PFdword startAngle, PFsdword extent)
{
	PFdword angStart = startAngle;
	PFdword angEnd = startAngle + extent;
	PFEnQuadrant startQuad;
	PFEnQuadrant endQuad; 
	volatile PFEnQuadrant currQuad;
	PFdword clampedAngStart = angStart % 90;
	PFdword clampedAngEnd = angEnd % 90;
	
	if((extent <= -360) || (extent >= 360))				// If extent is more than 360, it will result in full ellipse
	{
		pfGfxDrawEllipse_Bresenham(xc, yc, a, b);
		return enStatusSuccess;
	}
	
	// forcing angle to lie between 0 and 360 even if it is negative.
	startAngle = startAngle % 360;
	
	// making extent positive and angDegStart to be always less than angDegEnd.
	if(extent < 0)
	{
		angStart = startAngle + extent;
		angEnd = startAngle;
		if(angStart < 0)
		{
			angStart = 360 - (-angStart) % 360;
			angEnd = 360 + startAngle;
		}
		extent = -extent;
	}
	else
	{
		angStart = startAngle;
		angEnd = startAngle + extent;
	}	
	
	
	
     startQuad = getQuadrant(angStart);
	 endQuad = getQuadrant(angEnd);
	
	if(startQuad != endQuad || clampedAngStart  > clampedAngEnd)
	{
		currQuad = startQuad;
		do 
		{
			pfGfxDrawEArcInOneQuadrant(xc, yc, a, b, clampedAngStart, 90-clampedAngStart, currQuad);
			clampedAngStart = 0;
			currQuad = getNextQuadrant(currQuad);
			
		} while (endQuad != currQuad);
		
		pfGfxDrawEArcInOneQuadrant(xc, yc, a, b, 0, clampedAngEnd, endQuad);
	}
	else
	{
		pfGfxDrawEArcInOneQuadrant(xc, yc, a, b, clampedAngStart, extent, startQuad);
	}						
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawFineQuadBezier(PFsdword x1, PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword x3, const PFsdword y3)
{
	PFsdword xStart = x1 < x3 ? 1 : -1;
	PFsdword yStart = y1 < y3 ? 1 : -1;													// step direction 
	
	volatile PFsdword f = 1;
	volatile PFsdword fx = x1 - 2*x2 + x3;
	volatile PFsdword fy = y1 - 2*y2 + y3;
	
	volatile PFsqword x = 2*fx*fx;
	volatile PFsqword y = 2*fy*fy;
	volatile PFsqword xy = 2*fx*fy*xStart*yStart;
	
	volatile PFsqword cur = xStart*yStart*(fx*(y3 - y1) - fy*(x3 - x1));						// curvature 
	
	// compute error increments of P0 
	volatile PFsqword dx = AbsVal(y1 - y2)*xy - AbsVal(x1 - x2)*y - cur*AbsVal(y1 - y3);
	volatile PFsqword dy = AbsVal(x1 - x2)*xy - AbsVal(y1 - y2)*x + cur*AbsVal(x1 - x3);	
	
	// compute error increments of P2
	PFsqword xEnd = AbsVal(y3 - y2)*xy - AbsVal(x3 - x2)*y + cur*AbsVal(y1 - y3);
	PFsqword yEnd = AbsVal(x3 - x2)*xy - AbsVal(y3 - y2)*x - cur*AbsVal(x1 - x3);
     
	if (cur == 0)																// straight line 
	{ 
		 pfGfxDrawLine(x1, y1, x3, y3); 
		return enStatusSuccess;
	}							

    // compute required minimum resolution factor
	if (dx == 0 || dy == 0 || xEnd == 0 || yEnd == 0) 
	{
		f = AbsVal(xy/cur)/2 + 1;													// division by zero: use curvature 
	}		
	else 
	{
		fx = 2*y/dx; 
		if (fx > f) 
			f = fx;																// increase resolution 
		
		fx = 2*x/dy; 
		if (fx > f) 
			f = fx; 
		
		fx = 2*y/xEnd; 
		if (fx > f) 
			f = fx; 
		
		fx = 2*x/yEnd; 
		if (fx > f) 
			f = fx; 
	} 
																				// negated curvature? 
	if (cur < 0) 
	{ 
		x = -x; 
		y = -y; 
		
		dx = -dx; 
		dy = -dy; 
		
		xy = -xy; 
	}
		
	dx = f*dx + y/2 - xy; 
	dy = f*dy + x/2 - xy;
	xEnd = dx+dy+xy;																// error 1.step 

	for (fx = f, fy = f; ; ) 
	{																			// plot curve 
		pfILI9320DrawPixel(x1, y1, _gColor);
		if (x1 == x3 && y1 == y3) 
			break;
		do 
		{																		// move f sub-pixel 
			yEnd = 2*xEnd - dy;														// save value for test of y step 
			if (2*xEnd >= dx)														// x step
			{ 
				fx--; 
				dy -= xy; 
				xEnd += dx += y; 
			}		
			 
			if (yEnd <= 0)											// y step
			{ 
				fy--; 
				dx -= xy; 
				xEnd += dy += x; 
			}			 
		} while (fx > 0 && fy > 0);									// pixel complete?
		
		// sufficient sub-steps for a pixel? 
		if (2*fx <= f)											
		{ 
			x1 += xStart; 
			fx += f; 
		}						 
		if (2*fy <= f) 
		{ 
			y1 += yStart; 
			fy += f; 
		}		
  }//
  return enStatusSuccess;
}

PFEnStatus pfGfxDrawCuboid(PFdword x11, PFdword y11, PFdword x12, PFdword y12, PFdword x21, PFdword y21, PFdword x22, PFdword y22)
{
	pfGfxDrawRectangle(x11, y11, x12, y12);
	pfGfxDrawRectangle(x21, y21, x22, y22);
	
	 pfGfxDrawLine(x11, y11, x21, y21);
	 pfGfxDrawLine(x12, y11, x22, y21);
	 pfGfxDrawLine(x11, y12, x21, y22);
	 pfGfxDrawLine(x12, y12, x22, y22);
	return enStatusSuccess;
}
PFEnStatus pfGfxDrawParallelogram( PFdword x1, PFdword y1,  PFdword x2,  PFdword y2, PFdword len)
{
		 pfGfxDrawLine(x1+len, y1, x2, y2);
		 pfGfxDrawLine(x2-len, y2, x1, y1);
		pfGfxDrawFastLine(x1, y1, x1+len, y1);
		pfGfxDrawFastLine(x2-len, y2, x2, y2);		
		return enStatusSuccess;
}

PFEnStatus pfGfxDrawParallelepiped( PFdword  x11, PFdword  y11,
                       PFdword  x12, PFdword  y12,PFbyte l1,
                       PFdword  x21, PFdword  y21,
                       PFdword  x22, PFdword  y22,PFbyte l2)
{
    pfGfxDrawParallelogram(x11,y11,x12,y12,l1);
    pfGfxDrawParallelogram(x21,y21,x22,y22,l2);
	

		 pfGfxDrawLine(x11,y11,x21,y21);
		 pfGfxDrawLine(x12,y12,x22,y22);
		 pfGfxDrawLine((x11+l1-1),y11,(x21+l2-1),y21);
		 pfGfxDrawLine((x12-l1+1),y12,(x22-l2+1),y22);
	return enStatusSuccess;
	

}

PFEnStatus pfGfxDrawStyledLine(PFsdword x1, PFsdword y1, const PFsdword x2, const PFsdword y2, PFEnLineStyle style, PFchar penSize, const PFsdword color)
{	
	PFsdword dx =  AbsVal(x2 - x1);
	PFsdword xStart = x1 < x2 ? 1 : -1;
	PFsdword dy = -AbsVal(y2 - y1);
	PFsdword yStart = y1 < y2 ? 1 : -1; 
	volatile PFsdword err = dx+dy, e2;													// error value e_xy 
	volatile PFsdword indxEnd = 0;
	volatile PFchar draw_flag = 1;
	PFword prev_color;
	
	if (penSize == 0)
		penSize = 1;
	
	  pfGfxGetColor(&prev_color);
	pfGfxSetColor(color);	
																
	while(1)
	{  
		switch (style)
		{
			default:
			case enLineStyleSolid:	break;

			case enLineStyleDotted:
					if ( (indxEnd+1)%(2*penSize) == 0 )
					{
						draw_flag ^= 1;
					}
					break;	
					
			case enLineStyleDashDot:
					if(indxEnd == (9*penSize) || indxEnd == (15*penSize) )
						draw_flag = 0;		
					if (indxEnd == (13*penSize) || indxEnd == (19*penSize) )
						draw_flag = 1;
					if(indxEnd == (19*penSize))
						indxEnd = 0;
		}
		
		if (draw_flag)
		{
			pfGfxPixels(x1, y1, penSize, color);
		}
		indxEnd++;
		
		if (x1==x2 && y1==y2) 
			break;
		
		e2 = 2*err;
		if (e2 >= dy)														// e_xy+e_x > 0 			
		{ 
			err += dy; 
			x1 += xStart; 
		}								
		if (e2 <= dx)														// e_xy+e_y < 0 
		{ 
			err += dx; 
			y1 += yStart; 
		}								
	}
  
	pfGfxSetColor(prev_color);
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawFilledLines(const PFsdword xc, const PFsdword yc, const PFsdword x1, const PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword yRef)
{
	PFsdword xStart1 = xc, yStart1 = yc, xEnd1 = x1, yEnd1 = y1, xStart2 = xc, yStart2 = yc, xEnd2 = x2, yEnd2 = y2;
	PFchar flag1 = 0, flag2 = 0;
	PFsdword dx1, dx2;
	PFsdword xDir1, xDir2;
	PFsdword dy1, dy2;
	PFsdword  yDir1, yDir2;
	volatile PFsdword err1, err2, e1, e2;								// error value e_xy 
	
	dx1 =  AbsVal(xStart1 - xEnd1);
	xDir1 = xStart1 < xEnd1 ? 1 : -1;
	dy1 = -AbsVal(yStart1 - yEnd1);
	yDir1 = yStart1 < yEnd1 ? 1 : -1; 
	err1 = dx1+dy1;													// error value e_xy 
	
	dx2 =  AbsVal(xStart2 - xEnd2);
	xDir2 = xStart2 < xEnd2 ? 1 : -1;
	dy2 = -AbsVal(yStart2 - yEnd2);
	yDir2 = yStart2 < yEnd2 ? 1 : -1; 
	err2 = dx2+dy2;
 
	while(1)
	{  
		if (( yStart1==yRef) && (yStart2==yRef) ) 
			break;
		
		if(!flag1)
		{
			e1 = 2*err1;
			if (e1 >= dy1)														// e_xy+e_x > 0 			
			{ 
				err1 += dy1; 
				xStart1 += xDir1; 
			}								
			if (e1 <= dx1)														// e_xy+e_y < 0 
			{ 
				err1 += dx1; 
				yStart1 += yDir1;
				flag1 = 1;
			}	
		}

		if(!flag2)
		{
			e2 = 2*err2;
			if (e2 >= dy2)														// e_xy+e_x > 0 			
			{ 
				err2 += dy2; 
				xStart2 += xDir2; 
			}								
			if (e2 <= dx2)														// e_xy+e_y < 0 
			{ 
				err2 += dx2; 
				yStart2 += yDir2; 
				flag2 = 1;
			}	
		}
		
		if(flag1 && flag2)
		{
			pfGfxDrawFastLine(xStart1, yStart1, xStart2, yStart2);
			flag1 = 0;
			flag2 = 0;
		}
	}
	return enStatusSuccess;
}


PFEnStatus setPixelAA(PFsdword x, PFsdword y, PFsdword dist)
{
  //  unsigned int lineR, lineG, lineB;
    if(dist > 255)
		dist = 255;
    //r = g = b = dist;
//    print (r,g,b)
    pfILI9320DrawPixel(x, y, MakeColour(dist, dist, dist));
	return enStatusSuccess;
}
PFEnStatus setPixelAABlend(PFsdword x, PFsdword y, PFsdword bColor, PFsdword fColor, PFsdword dist)
{
   PFsdword r, g, b;
    if(dist > 255)
		dist = 255;
    //color = surf.get_at((x,y));
	// getPixel(x,y) -> color (16bit)
	// Red = color & 1111100000000
	// Green = color & 0000001111111111000000
	// color[0] == Red; color[1] = Green; color[2] = Blue
    r = ( (dist * GetRed(bColor)) + ((255 - dist)*GetRed(fColor)) )/255;
    g = ( (dist * GetGreen(bColor)) + ((255 - dist)*GetGreen(fColor)) )/255;
    b = ( (dist * GetBlue(bColor)) + ((255 - dist)*GetBlue(fColor)) )/255;
	
	// lcd setColor(r,g,b)
	// lcd_setPixel(x,y)
//##    print (dist, color, (r,g,b))	
    pfILI9320DrawPixel(x, y, MakeColour(r,g,b));
	return enStatusSuccess;
}	
	


PFEnStatus pfGfxPlotCircle(PFdword xm, PFdword ym, PFdword r, PFdword bColor, PFdword fColor)
{
	PFsdword x = r;
    PFsdword y = 0;
    PFsdword err = 2 - 2*r;
    PFsdword i, e2, x2;
	r = 1 - err;

    while(1)
	{
        i = 255*(AbsVal(err + 2*(x+y) - 2))/r;
        setPixelAABlend(xm + x, ym - y, bColor, fColor, i);
        setPixelAABlend(xm + y, ym + x, bColor, fColor, i);
        setPixelAABlend(xm - x, ym + y, bColor, fColor, i);
        setPixelAABlend(xm - y, ym - x, bColor, fColor, i);
        if (x == 0)
		{	
			break;
		}
        e2 = err;
        x2 = x;

        if (err > y)
		{
            i = 255*(err + 2*x - 1)/r;
            if (i < 255)
			{
                setPixelAABlend(xm + x, ym - y + 1, bColor, fColor, i);
                setPixelAABlend(xm + y - 1, ym + x, bColor, fColor, i);
                setPixelAABlend(xm - x, ym + y - 1, bColor, fColor, i);
                setPixelAABlend(xm - y + 1, ym - x, bColor, fColor, i);
			}
            err -= --x*2 - 1;
		}
		
        if (e2 <= x2)
		{
            x2 -= 1;
            i = 255*(1 - 2*y - e2)/r;
            if (i < 255)
			{
                setPixelAABlend(xm + x2, ym - y, bColor, fColor, i);
                setPixelAABlend(xm + y, ym + x2, bColor, fColor, i);
                setPixelAABlend(xm - x2, ym + y, bColor, fColor, i);
                setPixelAABlend(xm - y, ym - x2, bColor, fColor, i);
			}
            err -= --y*2 - 1;
		}
	}
	return enStatusSuccess;
}
			
PFEnStatus pfGfxPlotCircleAA1(PFdword xm, PFdword ym, PFdword r, PFdword bColor, PFdword fColor)
{
    PFsdword x = -r;
    PFsdword y = 0;
    PFsdword err = 2 - 2 * r;
    PFsdword plot = 1;
	PFsdword i, e2, x2;

	r = 1 - err;
	
    while(plot)
	{
        i = 255 * (AbsVal(err - 2*(x + y) - 2)/r);
        setPixelAABlend(xm - x, ym + y, bColor, fColor, i);
        setPixelAABlend(xm - y, ym - x, bColor, fColor, i);
        setPixelAABlend(xm + x, ym - y, bColor, fColor, i);
        setPixelAABlend(xm + y, ym + x, bColor, fColor, i);
        e2 = err;
        x2 = x;

        if((err + y) > 0)
		{
            i = 255*(err - 2*x - 1)/r;
            if (i < 256)
			{
                setPixelAABlend(xm-x, ym+y+1, bColor, fColor, i);
                setPixelAABlend(xm-y-1, ym-x, bColor, fColor, i);
                setPixelAABlend(xm+x, ym-y-1, bColor, fColor, i);
                setPixelAABlend(xm+y+1, ym+x, bColor, fColor, i);
			}
            x += 1;
            err += x*2 + 1;
		}
		
        if (e2 + x2 <= 0)
		{
            i = 255 * (2*y+3-e2)/r;
            if (i < 256)
			{
                setPixelAABlend(xm-x2-1, ym+y, bColor, fColor, i);
                setPixelAABlend(xm-y, ym-x2-1, bColor, fColor, i);
                setPixelAABlend(xm+x2+1, ym-y, bColor, fColor, i);
                setPixelAABlend(xm+y, ym+x2+1, bColor, fColor, i);
            }
			y += 1;
            err += y*2 + 1;
		}
        plot = (x < 0);
	}	
	return enStatusSuccess;
}




PFEnStatus pfGfxSortPointsInX(PFsdword *x1, PFsdword *y1, PFsdword *x2, PFsdword *y2, PFsdword *x3, PFsdword *y3)
{
	PFsdword temp;

	if(*y1 > *y2)
	{
		temp = *y2;
		*y2 = *y1;
		*y1 = temp;
		
		temp = *x2;
		*x2 = *x1;
		*x1 = temp;
	}
			
	if(*y2 > *y3)
	{
		temp = *y2;
		*y2 = *y3;
		*y3 = temp;
		
		temp = *x3;
		*x3 = *x2;
		*x2 = temp;
	}
			
	if(*y1 > *y2)
	{
		temp = *y2;
		*y2 = *y1;
		*y1 = temp;
		
		temp = *x2;
		*x2 = *x1;
		*x1 = temp;
	}
}

PFEnStatus pfGfxSortPointsInY(PFsdword *x1, PFsdword *y1, PFsdword *x2, PFsdword *y2, PFsdword *x3, PFsdword *y3)
{
	PFsdword temp;

	if(*y1 > *y2)
	{
		temp = *y2;
		*y2 = *y1;
		*y1 = temp;
		
		temp = *x2;
		*x2 = *x1;
		*x1 = temp;
	}
			
	if(*y2 > *y3)
	{
		temp = *y2;
		*y2 = *y3;
		*y3 = temp;
		
		temp = *x3;
		*x3 = *x2;
		*x2 = temp;
	}
			
	if(*y1 > *y2)
	{
		temp = *y2;
		*y2 = *y1;
		*y1 = temp;
		
		temp = *x2;
		*x2 = *x1;
		*x1 = temp;
	}
	return enStatusSuccess;
}


PFEnStatus pfGfxDrawFilledTriangle(const PFsdword x1, const PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword x3, const PFsdword y3)
{
	PFsdword yMin = y1, yMid = y2, yMax = y3;
	PFsdword xMin = x1, xMid = x2, xMax = x3;

	pfGfxSortPointsInY(&xMin, &yMin, &xMid, &yMid, &xMax, &yMax);
	
	pfGfxDrawFilledLines(xMax, yMax, xMid, yMid, xMin, yMin, yMid);
	pfGfxDrawFilledLines(xMin, yMin, xMid, yMid, xMax, yMax, yMid);
	return enStatusSuccess;
}



PFEnStatus pfGfxDrawFilledParallelTriangle(const PFsdword x1, const PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword x3, const PFsdword y3)
{	
	PFsdword xStart1, xStart2, xEnd1, xEnd2, yStart1, yStart2, yEnd1, yEnd2;
	PFchar flag1 = 0, flag2 = 0;
	PFsdword dx1, dx2;
	PFsdword xDir1, xDir2;
	PFsdword  dy1, dy2;
	PFsdword yDir1, yDir2;
	volatile PFsdword err1, err2, e1, e2;													// error value e_xy 
	
	if(y1 == y2)
	{
		xStart1 = x3;
		yStart1 = y3;
		
		xEnd1 = x1;
		yEnd1 = y1;

		xStart2 = x3;
		yStart2 = y3;
		
		xEnd2 = x2;
		yEnd2 = y2;
	}
	else if(y2 == y3)
	{
		xStart1 = x1;
		yStart1 = y1;
		
		xEnd1 = x2;
		yEnd1 = y2;

		xStart2 = x1;
		yStart2 = y1;
		
		xEnd2 = x3;
		yEnd2 = y2;
	}
	else if(y1 == y3)
	{
		xStart1 = x2;
		yStart1 = y2;
	
		xEnd1 = x1;
		yEnd1 = y1;

		xStart2 = x2;
		yStart2 = y2;
		
		xEnd2 = x3;
		yEnd2 = y1;
	}
	
	dx1 =  AbsVal(xStart1 - xEnd1);
	xDir1 = xStart1 < xEnd1 ? 1 : -1;
	dy1 = -AbsVal(yStart1 - yEnd1);
	yDir1 = yStart1 < yEnd1 ? 1 : -1; 
	err1 = dx1+dy1;													// error value e_xy 
	
	dx2 =  AbsVal(xStart2 - xEnd2);
	xDir2 = xStart2 < xEnd2 ? 1 : -1;
	dy2 = -AbsVal(yStart2 - yEnd2);
	yDir2 = yStart2 < yEnd2 ? 1 : -1; 
	err2 = dx2+dy2;
 
	while(1)
	{  
		if( (yStart1 == yEnd1) && (yStart2 == yEnd2) ) 
			break;
		
		if(!flag1)
		{
			e1 = 2*err1;
			if (e1 >= dy1)														// e_xy+e_x > 0 			
			{ 
				err1 += dy1; 
				xStart1 += xDir1; 
			}								
			if (e1 <= dx1)														// e_xy+e_y < 0 
			{ 
				err1 += dx1; 
				yStart1 += yDir1;
				flag1 = 1;
			}	
		}

		if(!flag2)
		{
			e2 = 2*err2;
			if (e2 >= dy2)														// e_xy+e_x > 0 			
			{ 
				err2 += dy2; 
				xStart2 += xDir2; 
			}								
			if (e2 <= dx2)														// e_xy+e_y < 0 
			{ 
				err2 += dx2; 
				yStart2 += yDir2; 
				flag2 = 1;
			}	
		}
		
		if(flag1 && flag2)
		{
			pfGfxDrawFastLine(xStart1, yStart1, xStart2, yStart2);
			flag1 = 0;
			flag2 = 0;
		}
	}
	return enStatusSuccess;
}




PFEnStatus pfGfxDrawFilledTriangle1(const PFsdword x1, const PFsdword y1, const PFsdword x2, const PFsdword y2, const PFsdword x3, const PFsdword y3)
{
	PFsdword yMin = y1, yMid = y2, yMax = y3;
	PFsdword xMin = x1, xMid = x2, xMax = x3;
	
	PFsdword xStart1, yStart1, xEnd1, yEnd1, xStart2, yStart2, xEnd2, yEnd2;
	//int xDraw1, yDraw1, xDraw2, yDraw2;
	PFchar flag1 = 0, flag2 = 0;
	PFsdword dx1, dx2;
	PFsdword xDir1, xDir2;
	PFsdword dy1, dy2;
	PFsdword  yDir1, yDir2;
	volatile PFsdword err1, err2, e1, e2;								// error value e_xy 
	
	pfGfxSortPointsInY(&xMin, &yMin, &xMid, &yMid, &xMax, &yMax);
	
	xStart1 = xMin;
	yStart1 = yMin;
	xEnd1 = xMid;
	yEnd1 = yMid;
	xStart2 = xMin;
	yStart2 = yMin;
	xEnd2 = xMax;
	yEnd2 = yMax;
	
	dx1 =  AbsVal(xStart1 - xEnd1);
	xDir1 = xStart1 < xEnd1 ? 1 : -1;
	dy1 = -AbsVal(yStart1 - yEnd1);
	yDir1 = yStart1 < yEnd1 ? 1 : -1; 
	err1 = dx1+dy1;													// error value e_xy 
	
	dx2 =  AbsVal(xStart2 - xEnd2);
	xDir2 = xStart2 < xEnd2 ? 1 : -1;
	dy2 = -AbsVal(yStart2 - yEnd2);
	yDir2 = yStart2 < yEnd2 ? 1 : -1; 
	err2 = dx2+dy2;
 
	while(1)
	{  
		//lcdPutPixel(xStart1, yStart1, _gColor);
		//lcdPutPixel(xStart2, yStart2, _gColor);
		if (( yStart1==yMid) && (yStart2==yMid) ) 
			break;
		
		if(!flag1)
		{
			e1 = 2*err1;
			if (e1 >= dy1)														// e_xy+e_x > 0 			
			{ 
				err1 += dy1; 
				xStart1 += xDir1; 
			}								
			if (e1 <= dx1)														// e_xy+e_y < 0 
			{ 
				err1 += dx1; 
				yStart1 += yDir1;
				flag1 = 1;
			}	
		}

		if(!flag2)
		{
			e2 = 2*err2;
			if (e2 >= dy2)														// e_xy+e_x > 0 			
			{ 
				err2 += dy2; 
				xStart2 += xDir2; 
			}								
			if (e2 <= dx2)														// e_xy+e_y < 0 
			{ 
				err2 += dx2; 
				yStart2 += yDir2; 
				flag2 = 1;
			}	
		}
		
		if(flag1 && flag2)
		{
			pfGfxDrawFastLine(xStart1, yStart1, xStart2, yStart2);
			flag1 = 0;
			flag2 = 0;
		}
	}
	
	
	xStart1 = xMid;
	yStart1 = yMid;
	xEnd1 = xMax;
	yEnd1 = yMax;

	
	dx1 =  AbsVal(xStart1 - xEnd1);
	xDir1 = xStart1 < xEnd1 ? 1 : -1;
	dy1 = -AbsVal(yStart1 - yEnd1);
	yDir1 = yStart1 < yEnd1 ? 1 : -1; 
	err1 = dx1+dy1;													// error value e_xy 
	

	while(1)
	{  
	;
		if (( yStart1==yMax) && (yStart2==yMax) ) 
			break;
		
		if(!flag1)
		{
			e1 = 2*err1;
			if (e1 >= dy1)														// e_xy+e_x > 0 			
			{ 
				err1 += dy1; 
				xStart1 += xDir1; 
			}								
			if (e1 <= dx1)														// e_xy+e_y < 0 
			{ 
				err1 += dx1; 
				yStart1 += yDir1;
				flag1 = 1;
			}	
		}

		if(!flag2)
		{
			e2 = 2*err2;
			if (e2 >= dy2)														// e_xy+e_x > 0 			
			{ 
				err2 += dy2; 
				xStart2 += xDir2; 
			}								
			if (e2 <= dx2)														// e_xy+e_y < 0 
			{ 
				err2 += dx2; 
				yStart2 += yDir2; 
				flag2 = 1;
			}	
		}
		
		if(flag1 && flag2)
		{
			pfGfxDrawFastLine(xStart1, yStart1, xStart2, yStart2);
			flag1 = 0;
			flag2 = 0;
		}
	}
	return enStatusSuccess;
}


PFEnStatus pfGfxDrawFilledPolygon(const PFsdword *xArray, const PFsdword *yArray, const PFsdword num)
{
	PFdword loop;
	
	for(loop = 1; loop < num-1; loop++)
	{
		pfGfxDrawFilledTriangle1(*xArray, *yArray, *(xArray+loop), *(yArray+loop), *(xArray+loop+1), *(yArray+loop+1));
	}
	return enStatusSuccess;
}
#if(PF_USE_FONT == 1)
PFEnStatus pfSetLcdDebugPrintConfig(PFEnGraphicFonts dFont, PFdword dFontClr, PFdword dFontBackClr)
{
	switch(dFont)
	{
		case enFont_8X8:
			dbgFontH = 8;
			dbgFontW = 8;
			pDbgFont = (PFbyte*)Font8x8;
			break;
			
		case enFont_8X16:
			dbgFontH = 16;
			dbgFontW = 8;
			pDbgFont = (PFbyte*)Font8x16;
			break;
		
		case enFont_16X16: break ;
		case enFont_32X32: break;
	}

	dbgFontType = dFont;
	dbgFontClr = dFontClr;
	dbgFontBack = dFontBackClr;
	pfILI9320FillRGB(dFontBackClr);
	return enStatusSuccess;
}

PFEnStatus pfLcdDebugNewLine(void)
{
	dbgY += dbgFontH;

	if((dbgY+dbgFontH) > 320 )
	{
		dbgY = 0;
		startScroll = 1;
	}

	if(startScroll)
	{
		scrollLines += dbgFontH;
		if(scrollLines > 320)
		{
			scrollLines = dbgFontH;
		}

		pfGfxDrawSolidRectangle(0, (scrollLines-dbgFontH), 240 ,scrollLines, dbgFontBack);
		//lcdScroll(scrollLines);
      pfILI9320Scroll(scrollLines);
	}
	return enStatusSuccess;
}

PFEnStatus pfGfxDrawDebugChar(PFbyte data)
{
	volatile PFsdword xIndex, yIndex, fData;

	if(data == '\r')
	{
		dbgX = 0;
		return enStatusSuccess;
	}
	if(data == '\n')
	{
		pfLcdDebugNewLine();		//dbgY += dbgFontH;
		return enStatusSuccess;
	}
	
	if((dbgX+dbgFontW) > (240))
	{
		dbgX = 0;
		pfLcdDebugNewLine();		//dbgY += dbgFontH;
	}
	

	//lcdSetArea(x, y, fontWidth-1, fontHeight-1);
	for(yIndex = 0; yIndex < dbgFontH; yIndex++) 
	{
		fData = (*(PFbyte*)(pDbgFont + ((data * dbgFontH) + yIndex)));
		if(dbgFontType == enFont_8X8 || dbgFontType == enFont_8X16)
			fData <<= 8;
		
		for(xIndex = 0; xIndex < dbgFontW; xIndex++) 
		{
			if( (fData & 0x8000) == 0x8000 ) 
			{
				//lcdWriteData(fontColor);
				pfILI9320DrawPixel(dbgX + xIndex, dbgY + yIndex, dbgFontClr);
			}
			else 
			{
				//lcdWriteData(backColor);
				pfILI9320DrawPixel(dbgX + xIndex, dbgY + yIndex, dbgFontBack);
			}
			fData <<= 1;
		}
	}

	dbgX += dbgFontW;
	return enStatusSuccess;
}
PFEnStatus pfLcdDebugString(PFbyte* str)
{
	while(*str != 0)
	{
		pfGfxDrawDebugChar(*str);
		str++;
	}
	return enStatusSuccess;
}

PFEnStatus pfLcdDebugScrollUp(PFword lines)
{
	if(scrollLines < dbgFontH)
	{
		scrollLines = 320 - dbgFontH;
	}
	else
		scrollLines -= (dbgFontH * lines);
	return enStatusSuccess;
}

PFEnStatus lcdDebugScrollDown(PFword lines)
{
	if(scrollLines + (dbgFontH * lines) > 320)
		scrollLines = (dbgFontH * lines);
	else
		scrollLines += (dbgFontH * lines);
	return enStatusSuccess;
}
#endif//#if(PF_USE_FONT == 1)
