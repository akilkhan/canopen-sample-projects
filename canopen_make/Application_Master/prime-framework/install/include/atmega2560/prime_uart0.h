/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework UART0 driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

#define  UART0_USE_FIFO 1

#define UART0_CH				UART0
#define UART0_CHANNEL			PERIPH(UART0_CH)

#if(UART0_USE_FIFO != 0)
	#define  UART0_BUFFER_SIZE 64
#endif	//#if(UART0_USE_FIFO != 0)

typedef enum
{
	enUart0Baudrate_2400 = 0,		/**< UART0 baudrate 2400			*/
	enUart0Baudrate_4800,			/**< UART0 baudrate 4800			*/
	enUart0Baudrate_9600,			/**< UART0 baudrate 9600			*/
	enUart0Baudrate_19200,			/**< UART0 baudrate 19200			*/
	enUart0Baudrate_38400,			/**< UART0 baudrate 38400			*/
	enUart0Baudrate_57600,			/**< UART0 baudrate 57600			*/
	enUart0Baudrate_115200			/**< UART0 baudrate 115200			*/
}PFEnUart0Baudrate;


/**		Enumeration for parity setting for the UART0 channel		*/
typedef enum
{
	enUart0ParityNone = 0,			/**< Selecting No parity for communication for UART0	*/
	enUart0ParityEven,				/**< Selecting Even parity for communication  for UART0*/
	enUart0parityOdd				/**< Selecting Odd parity for communication  for UART0	*/
}PFEnUart0Parity;



/**		Enumeration for number of stop bits to use for UART0 channel		*/
typedef enum
{
	enUart0StopBits_1 = 0,			/**< Selecting Number of Stop Bits = 1  for UART0	*/
	enUart0StopBits_2,				/**< Selecting Number of Stop Bits = 2  for UART0	*/
}PFEnUart0StopBits;

typedef enum
{
	enUart0DataBits_5=0x00,			/**< Selecting Number of Date Bits = 5  for UART0	*/
	enUart0DataBits_6=0x01,			/**< Selecting Number of Date Bits = 6  for UART0*/
	enUart0DataBits_7=0x02,			/**< Selecting Number of Date Bits = 7  for UART0	*/
	enUart0DataBits_8=0x03,			/**< Selecting Number of Date Bits = 8 	for UART0*/
	enUart0DataBits_9=0x07			/**< Selecting Number of Date Bits = 9  for UART0	*/
}PFEnUart0DataBits;



/**		Enumeration for interrupts to enable for the UART0 channel		*/
typedef enum
{
	enUart0IntNone,					/**< Interrupts dissabled  for UART0*/
	enUart0IntTx=0x04,				/**< Transmission Interrupts are enabled  for UART0*/
	enUart0IntRx=0x08,				/**< Reception Interrupts are enabled  for UART0*/
	enUart0IntTxRx=0x0c				/**< Transmission as well as reception Interrupts are enabled for UART0*/
}PFEnUart0Interrupt;

typedef enum
{
	enUart0ModeTx=0x01,				/**<  UART0 in transmission mode */
 	enUart0ModeRx,					/**<  UART0 in reception mode */
	enUart0ModeTxRx					/**<  UART0 in transmission as well as reception mode */
}PFEnUart0Mode;

/**		UART0 configuration structure			*/
typedef struct
{
	PFEnUart0Mode		mode;			/**< receiver or transmitter mode*/
	PFEnUart0Baudrate 	baudrate;		/**< Set baudrate for channel*/
	PFEnUart0Parity 	parity;		/**< Parity to be used for communication*/
	PFEnUart0StopBits 	stopBits;		/**< Number of stop bits to be used*/
	PFEnUart0DataBits 	dataBits;		/**< Number of stop bits to be used*/
	PFEnUart0Interrupt interrupts;		/**< Interrupts to enable for the channel*/

#if(UART0_USE_FIFO == 0)
	PFcallback		transmitCallback;
	PFcallback		receiveCallback;
#endif
}PFCfgUart0;

typedef PFCfgUart0* PFpCfgUart0;


/**
Description: The function initializes the port with given settings.

Parameters:
config - configuration structure which contains the settings for the communication channel to be used.

Return:	ENStatus. Returns UART0 initialization status.
*/
PFEnStatus pfUart0Open(PFpCfgUart0 config);


/**
Description: Turn offs the UART0 channel

Return:	ENStatus. Returns UART0 initialization status.
 */
PFEnStatus pfUart0Close(void);


/**
Description: The function writes one byte to UART0 data register

Parameters:
channel - UART0 channel to be used for communication.
data - 8 bit data to write to the UART0 channel.

Return:	ENStatus. Returns UART0 write status.
*/
PFEnStatus pfUart0WriteByte(PFbyte data);

/**
Description: 	The function sends multiple bytes on UART0 channel. 
				If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
				Otherwise it will wait in the function and send each byte by polling the line status.

Parameters:
channel - UART0 channel to be used for communication.
data - Unsigned char pointer to the data to be sent.
size - Total number of bytes to send.

Return:	ENStatus. Returns UART0 write status.
*/
PFEnStatus pfUart0Write(PFbyte* data, PFdword size);

/**
 * The function reads one byte from UART0 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \return read byte from UART0 channel.
 */
PFEnStatus pfUart0ReadByte(PFbyte* data);

/**
 * The function reads one byte from UART0 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param data Unsigned char pointer to the buffer where the read data should be loaded.
 * \param size Total number of bytes to read.
 * \return UART0 read status.
 */
PFEnStatus pfUart0Read(PFbyte* data, PFdword size, PFdword* readBytes);

#if(UART0_USE_FIFO != 0)
/**
 * Returns the number of bytes received in UART0 buffer.
 *
 * \return number of bytes received in UART0 buffer.
 */
PFEnStatus pfUart0GetRxBufferCount(PFdword* count);

/**
 * This function empties the transmit buffer.
 */
PFEnStatus pfUart0TxBufferFlush(void);

/**
 * This function empties the receive buffer.
 */
PFEnStatus pfUart0RxBufferFlush(void);

#endif

