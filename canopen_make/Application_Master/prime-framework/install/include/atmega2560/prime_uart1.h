/**
 *
 *                              Copyright (c) 2014
 *                         PhiRobotics Research Pvt Ltd
 *
 *  For licensing information, see the file 'LICENSE' in the root folder of
 *  this software module.
 *
 * \brief Prime Framework UART1 driver for ATmega2560.
 *
 * 
 * Review status: NO
 *
 */
#pragma once

#define  UART1_USE_FIFO 1

#define UART1_CH				UART1
#define UART1_CHANNEL		PERIPH(UART1_CH)

#if(UART1_USE_FIFO != 0)
	#define  UART1_BUFFER_SIZE 64
#endif	//#if(UART1_USE_FIFO != 0)

typedef enum
{
	enUart1Baudrate_2400 = 0,		/**< UART1 baudrate 2400				*/
	enUart1Baudrate_4800,			/**< UART1 baudrate 4800				*/
	enUart1Baudrate_9600,			/**< UART1 baudrate 9600				*/
	enUart1Baudrate_19200,			/**< UART1 baudrate 19200			*/
	enUart1Baudrate_38400,			/**< UART1 baudrate 38400			*/
	enUart1Baudrate_57600,			/**< UART1 baudrate 57600			*/
	enUart1Baudrate_115200			/**< UART1 baudrate 115200			*/
}PFEnUart1Baudrate;


/**		Enumeration for parity setting for the Uart1 channel		*/
typedef enum
{
	enUart1ParityNone = 0,			/**< Selecting No parity for communication for Uart1	*/
	enUart1ParityEven,				/**< Selecting Even parity for communication  for Uart1*/
	enUart1parityOdd				/**< Selecting Odd parity for communication  for Uart1	*/
}PFEnUart1Parity;

/**		Enumeration for number of stop bits to use for Uart1 channel		*/
typedef enum
{
	enUart1StopBits_1 = 0,			/**< Selecting Number of Stop Bits = 1  for Uart1	*/
	enUart1StopBits_2,				/**< Selecting Number of Stop Bits = 2  for Uart1	*/
}PFEnUart1StopBits;

typedef enum
{
	enUart1DataBits_5=0x00,			/**< Selecting Number of Date Bits = 5  for Uart1	*/
	enUart1DataBits_6=0x01,			/**< Selecting Number of Date Bits = 6  for Uart1*/
	enUart1DataBits_7=0x02,			/**< Selecting Number of Date Bits = 7  for Uart1	*/
	enUart1DataBits_8=0x03,			/**< Selecting Number of Date Bits = 8 	for Uart1*/
	enUart1DataBits_9=0x07			/**< Selecting Number of Date Bits = 9  for Uart1	*/
}PFEnUart1DataBits;



/**		Enumeration for interrupts to enable for the Uart1 channel		*/
typedef enum
{
	enUart1IntNone,					/**< Interrupts dissabled  for Uart1*/
	enUart1IntTx=0x04,				/**< Transmission Interrupts are enabled  for Uart1*/
	enUart1IntRx=0x08,				/**< Reception Interrupts are enabled  for Uart1*/
	enUart1IntTxRx=0x0c				/**< Transmission as well as reception Interrupts are enabled for Uart1*/
}PFEnUart1Interrupt;


typedef enum
{
	enUart1ModeTx=0x01,				/**<  Uart1 in transmission mode */
 	enUart1ModeRx,					/**<  Uart1 in reception mode */
	enUart1ModeTxRx					/**<  Uart1 in transmission as well as reception mode */
}PFEnUart1Mode;

/**		UART1  configuration structure			*/
typedef struct
{
	PFEnUart1Mode		mode;			/**<   receiver or transmitter mode */
	PFEnUart1Baudrate 	baudrate;		/**<   Set baudrate for channel */
	PFEnUart1Parity 		parity;		/**<   Parity to be used for communication */
	PFEnUart1StopBits 	stopBits;		/**<   Number of stop bits to be used */
	PFEnUart1DataBits 	dataBits;		/**<   Number of stop bits to be used */
	PFEnUart1Interrupt interrupts;		/**<   Interrupts to enable for the channel */
#if(UART1_USE_FIFO == 0)
	PFcallback		transmitCallback;
	PFcallback		receiveCallback;
#endif
}PFCfgUart1;
typedef PFCfgUart1* PFpCfgUart1;


/**
Description: The function initializes the port with given settings.

Parameters:
config - configuration structure which contains the settings for the communication channel to be used.

Return:	ENStatus. Returns UART1 initialization status.
*/
PFEnStatus pfUart1Open(PFpCfgUart1 config);


/**
Description: Turn offs the UART1 channel

Return:	ENStatus. Returns UART1 initialization status.
 */
PFEnStatus pfUart1Close(void);


/**
Description: The function writes one byte to UART1 data register

Parameters:
channel - UART1 channel to be used for communication.
data - 8 bit data to write to the UART1 channel.

Return:	ENStatus. Returns UART1 write status.
*/
PFEnStatus pfUart1WriteByte(PFbyte data);

/**
Description: 	The function sends multiple bytes on UART1 channel. 
				If transmit interrupt is enabled, the function will enqueue the data in transmit FIFO.
				Otherwise it will wait in the function and send each byte by polling the line status.

Parameters:
channel - UART1 channel to be used for communication.
data - Unsigned char pointer to the data to be sent.
size - Total number of bytes to send.

Return:	ENStatus. Returns UART1 write status.
*/
PFEnStatus pfUart1Write(PFbyte* data, PFdword size);

/**
 * The function reads one byte from UART1 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \return read byte from UART1 channel.
 */
PFEnStatus pfUart1ReadByte(PFbyte* data);

/**
 * The function reads one byte from UART1 channel.
 * If receive interrupt is enabled, the function will read the byte from receive FIFO.
 * Otherwise it will wait in the function for one byte to receive by polling the line status.
 *
 * \param data Unsigned char pointer to the buffer where the read data should be loaded.
 * \param size Total number of bytes to read.
 * \return UART1 read status.
 */
PFEnStatus pfUart1Read(PFbyte* data, PFdword size, PFdword* readBytes);

#if(UART1_USE_FIFO != 0)
/**
 * Returns the number of bytes received in UART1 buffer.
 *
 * \return number of bytes received in UART1 buffer.
 */
PFEnStatus pfUart1GetRxBufferCount(PFdword* count);

/**
 * This function empties the transmit buffer.
 */
PFEnStatus pfUart1TxBufferFlush(void);

/**
 * This function empties the receive buffer.
 */
PFEnStatus pfUart1RxBufferFlush(void);

#endif

