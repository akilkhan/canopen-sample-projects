#pragma once
/**
 * \defgroup PF_TIMER_API Timer API
 * @{
 */ 
 
 #define TIMER8_CH				TIMER2
 #define TIMER8_CHANNEL			PERIPH(TIMER8_CH)
 
 #define TIMSK(x)			_TIMSK(x)
 #define _TIMSK(x)			TIMSK_INT->##x
 
 
//  #if(( TIMER8 !=TIMER0) ||(TIMER8 !=TIMER2))
//  #error "TIMER 8bit is available only on TIMER0 and TIMER2 for this controller "
//  #endif
 
  //#define TIMER8_OVF_VECT 		__TIMER8_OVF_VECT(PWM_TIMER)
 //#define __TIMER8_OVF_VECT(x)		_TIMER8_OVF_VECT(x)
 //#define _TIMER8_OVF_VECT(x)		x##_OVF_vect
 //
 //#define TIMER8_COMPA_VECT 		__TIMER8_COMPA_VECT(PWM_TIMER)
 //#define __TIMER8_COMPA_VECT(x)		_TIMER8_COMPA_VECT(x)
 //#define _TIMER8_COMPA_VECT(x)		x##_COMPA_vect
 //
 //#define TIMER8_COMPB_VECT 		__TIMER8_COMPB_VECT(PWM_TIMER)
 //#define __TIMER8_COMPB_VECT(x)		_TIMER8_COMPB_VECT(x)
 //#define _TIMER8_COMPA_VECT(x)		x##_COMPB_vect
 
/** Enumeration for timer modes			*/
typedef enum
{
	enNoclock = 0,			/**No clock source is selected and the timer is disabled	*/
	enNoprescaledClk,		/**< clock source is selected and the timer is not pre-scaled	*/
	enClkDivideby8,			/**< Clock source freqency is divided by 8	*/
	enClkDivideby64,		/**< Clock source freqency is divided by 8	*/
	enClkDivideby256,		/**< Clock source freqency is divided by 8	*/
	enClkDivideby1024,		/**< Clock source freqency is divided by 8	*/
	enExtClkFallingEdge,	/**<  timer with external source on falling edge 	*/
	enExtClkRisingEdge		/**<  timer with external source on falling edge 	*/
}PFEnClocksource;


/** Enumeration for external match control		*/
typedef enum
{
	enExtMatchCtrlNone = 0,				/**< Do nothing on count match						*/
	enExtMatchCtrlTogglePin,			/**< Toggle match pin								*/
	enExtMatchCtrlClearPin,				/**< Clear match pin								*/
	enExtMatchCtrlSetPin				/**< Set match pin									*/
}PFEnExtMatchCtrl;

typedef enum
{
	enTimerNormalMode,	/**<  timer in Nornal Mode 	*/
	enTimerCtcMode=0x02	/**<  timer in clear timer on compare  Mode 	*/
	
}PFEnTimerMode;

typedef enum
{
	enTimerIntNone,			/**	No interrupt	*/
	enTimerOverflowIntr=0x01,	/**	interrupt on overflow	*/
	enTimerMatchRegAIntr=0x02,	/**	interrupt on matching with the value in OCR0A 	*/
	enTimerMatchRegBIntr=0x04,	/**	interrupt on matching with the value in OCR0B 	*/
	enTimerAllIntr=0x07,	/**	interrupt on matching with the value in OCR0A ,OCR0B and overflow 	*/
}PFEnTimerIntrrupt;

/**		Timer configure structure		*/
typedef struct
{
	PFEnClocksource		clockSource;		/**< Select clock source				*/
	PFEnTimerMode		timerMode;			/** timer mode*/
	PFbyte				matchValueA;		/**< Match register A compare value							*/
	PFbyte				matchValueB;		/**< Match register B compare value							*/
	PFEnExtMatchCtrl 	exMatchActionA;	/**< match pin control on count match 				*/ 
	PFEnExtMatchCtrl 	exMatchActionB;	/**< match pin control on count match 				*/
	PFEnTimerIntrrupt	interrupt;			/**< To enable or disable timer interrupt			*/
	PFcallback			cmpMatchACallback;	/**< Callback function for timer ISR				*/
	PFcallback			cmpMatchBCallback;
	PFcallback			overFlowCallback;
}PFCfgTimer;

/** Pinter to PFCfgTiemr structure		*/
typedef PFCfgTimer* PPFCfgTimer;

/**
 * Intialized timer with given parameters
 * 
 * \param config timer configuration structure
 * 
 * \return timer intialization status
 */
PFEnStatus pfTimerOpen(PPFCfgTimer config);

/**
 * Stops timer operation and turn offs the timer module
 * 
 * \return timer turn off operation status
 */
PFEnStatus pfTimerClose(void);

/**
 * Starts timer operation
 * 
 * \return timer start status
 */
PFEnStatus pfTimerStart(void);

/**
 * Stops timer operation
 * 
 * \return timer stop status
 */
PFEnStatus pfTimerStop(void);

/**
 * Resets the timer operation. Timer will start counting from zero again.
 * 
 * \return timer reset status
 */
PFEnStatus pfTimerReset(void);

/**
 * Returns the timer count
 * 
 * \return timer count
 */
PFEnStatus pfTimerReadCount(PFbyte* data);

/**
* Writes new value to the match register and enables latch for the match register
*
* \param regNum index of match register to be updated
* \param regValue new value for match register
*
* \return match register update status
*/
PFEnStatus pfTimerUpdateMatchRegister(PFbyte regNum, PFdword regVal); 



/** @} */


