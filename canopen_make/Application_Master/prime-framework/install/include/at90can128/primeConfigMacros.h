#pragma once

/*
#define _PCLK_DIV(x)				PCLK_DIV_##x
#define _PWR(x)						PWR_##x
#define _INT_HANDLER(x)				x##_IRQHandler
#define _IRQ_NUM(x)					x##_IRQn
#define _PERIPH(x)					PERIPH_##x
#define _EINT(x)					EINT_##x
*/

#define INT_HANDLER(x)				PF_CONCAT(__vector_, x)	//_INT_HANDLER(x)
#define PERIPH(x)					PF_CONCAT(PERIPH_, x)		//_PERIPH(x)
#define EINT_CH(x)					PF_CONCAT(EINT_, x)			//_EINT_CH(x)


#define _PF_CONCAT(X, Y)			X##Y
#define PF_CONCAT(X, Y)				_PF_CONCAT(X, Y)









