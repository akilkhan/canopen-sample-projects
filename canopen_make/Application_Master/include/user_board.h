/*
 * HYDRA board.h
 *
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "prime_framework.h"
#include "prime_CO_driver.h"
#include "prime_can2.h"

#define BOARD_USE_SYSTICK       0

#if BOARD_USE_SYSTICK == 1
	#include "prime_systick.h"
#else
	#include "prime_rit.h"
#endif

#define USED_BOARD_DEBUG_UART   0

#if (USED_BOARD_DEBUG_UART==0)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART0
#elif (USED_BOARD_DEBUG_UART==1)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART1
#elif (USED_BOARD_DEBUG_UART==2)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART2
#elif (USED_BOARD_DEBUG_UART==3)
	#define 	BOARD_DEBUG_UART 	PERIPH_UART3
#endif

#define 	DEBUGS(x)   	pfUart0WriteString(x)
#define 	GETCHAR()   	board_get_char()

int pfBoardinit(PFCoCanModuleHwConfig *canHwCfg);

#define board_reset()       do{/*WDT_Start(10000);*/while(1);/*NVIC_SystemReset();*/}while(0)

#if BOARD_USE_SYSTICK == 1
	#define timer_enableIrq()   SYSTICK_IntCmd(ENABLE)
	#define timer_disableIrq()  SYSTICK_IntCmd(DISABLE)
#else
	#define timer_enableIrq()   NVIC_EnableIRQ(RIT_IRQn)
	#define timer_disableIrq()  NVIC_DisableIRQ(RIT_IRQn)
#endif

#define 	can_enableIrq()     NVIC_EnableIRQ(CAN_IRQn)
#define 	can_disableIrq()    NVIC_DisableIRQ(CAN_IRQn)

void leds_write(PFbyte ledsBits);
PFbyte leds_read(void);
PFbyte buttons_read(void);
PFbyte board_get_char(void);
void board_put_char(PFbyte c);

#endif /* BOARD_H_ */
