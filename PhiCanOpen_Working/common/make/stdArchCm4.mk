#
#                            Copyright (c) 2012-2013
#                         PhiRobotics Research Pvt. Ltd.
#               Vedant Commercial Complex, Vartak Nagar, Thane(w),
#                           Maharashtra-400606, India
#
#  For licensing information, see the file 'LICENSE' in the root folder of
#  this software module.
#

ARCHCPPFLAGS =
ARCHASFLAGS = -mthumb -mno-thumb-interwork
ARCHCFLAGS = -mthumb -mno-thumb-interwork -fno-strict-aliasing -fwrapv -fverbose-asm -D__NEWLIB__ -I$(PHIXOS_PATH)/cpu/cortex-m4
ARCHCXXFLAGS = -mthumb -mno-thumb-interwork -fno-strict-aliasing -fwrapv -fverbose-asm -D__NEWLIB__ -I$(PHIXOS_PATH)/cpu/cortex-m4
ARCHLDFLAGS = -mthumb -mno-thumb-interwork -nostartfiles -L $(PHIHAL_PATH)/src/arch/cm4/scripts/ -Wl,--no-warn-mismatch

ifndef LDSCRIPT
LDSCRIPT = stm32f407xx_rom.xld
endif

ifeq ($(DEBUG), 1)
ARCHASFLAGS += -O0
ARCHCFLAGS += -O0 -DDEBUG_STACK_STAMP=0x5A
ARCHCXXFLAGS += -O0
else
ARCHASFLAGS += -Os
ARCHCFLAGS += -Os
ARCHCXXFLAGS += -Os
endif

ARCHCFLAGS += -DPHI_CFG_DEBUG_ASSERT_PRINT=$(DEBUG)
ARCHCXXFLAGS += -DPHI_CFG_DEBUG_ASSERT_PRINT=$(DEBUG)

CPU = cortex-m4
ifeq ($(CPU_STM32F407), 1)
ARCHCPPFLAGS += -D__ARM_STM32F407__
MCU = stm32f407
CPU_STM32F4XX = 1
endif

ifeq ($(IMAGE_TYPE),RAM)
ARCHCPPFLAGS += -DPHI_CFG_EXTRAM_SIZE=$(CFG_EXTRAM_SIZE)
else
ARCHCPPFLAGS += -DPHI_CFG_EXTRAM_SIZE=0
endif

ARCHCPPFLAGS += \
	-DPHI_CFG_OS_STACK_SIZE=$(CFG_OS_STACK_SIZE)

LDSCRIPT_SRC = $(PHIHAL_PATH)/src/arch/cm4/scripts/$(LDSCRIPT)
LDSCRIPT_TRG = $(BUILD_ROOT)/$(PACKAGE_NAME)/scripts/$(LDSCRIPT)_p
