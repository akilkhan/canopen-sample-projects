#
#                            Copyright (c) 2012-2013
#                         PhiRobotics Research Pvt. Ltd.
#               Vedant Commercial Complex, Vartak Nagar, Thane(w),
#                           Maharashtra-400606, India
#
#  For licensing information, see the file 'LICENSE' in the root folder of
#  this software module.
#

ARCHCFLAGS =
ARCHCXXFLAGS =

ifeq ($(DEBUG), 1)
ARCHCFLAGS += -DDEBUG_STACK_STAMP=0x5A
ARCHCFLAGS += -Os
ARCHCXXFLAGS += -Os
else
ARCHCFLAGS += -Os
CXXFLAGS += -Os
endif

ARCHASFLAGS = -ffunction-sections -fdata-sections
ARCHCFLAGS += -ffunction-sections -fdata-sections
ARCHCXXFLAGS += -ffunction-sections -fdata-sections
ARCHLDFLAGS = -mrelax -Wl,--defsym,__stack_start=__stack-$(CFG_STACK_SIZE)
# to save space prevent assert condition strings to be printed (kept in PROGMEM)
ARCHCFLAGS += -DPHI_CFG_DEBUG_ASSERT_PRINT=0
ARCHCXXFLAGS += -DPHI_CFG_DEBUG_ASSERT_PRINT=0

ifeq ($(CPU_ATMEGA2560), 1)
MCU = atmega2560
endif
ifeq ($(CPU_AT90CAN128), 1)
MCU = at90can128
endif
