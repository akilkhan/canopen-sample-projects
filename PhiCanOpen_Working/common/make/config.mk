#
#                            Copyright (c) 2012-2013
#                         PhiRobotics Research Pvt. Ltd.
#               Vedant Commercial Complex, Vartak Nagar, Thane(w),
#                           Maharashtra-400606, India
#
#  For licensing information, see the file 'LICENSE' in the root folder of
#  this software module.
#

VERBOSE = 0
DEBUG = $(DEBUG_BUILD)

# Global configuration
CFG_KERN_MONITOR = 1
CFG_KERN_PRI_INHERIT = 0
CFG_KERN_SIGNALS = 0

include $(COMMON_PATH)/make/stdBoardBuild.mk
-include $(PROJ_ROOT)/boardBuild.mk

ifeq ($(PHI_NO_OS),1)
CFG_KERN=0
CFG_DEPS_APPL = $(PHIHAL_OBJDIR)/libphiHal.a
CFG_LIBS_APPL = -lphiCommon,-lphiHal
else
CFG_KERN=1
CFG_DEPS_APPL = $(PHIXOS_OBJDIR)/libphiXOS.a $(PHIHAL_OBJDIR)/libphiHal.a
CFG_LIBS_APPL = -lphiCommon,-lphiHal,-lphiXOS
endif


ifeq ($(PHI_ARCH_CM3),1)
PHI_ARCH = 1
include $(COMMON_PATH)/make/stdArchCm3.mk
endif
ifeq ($(PHI_ARCH_AVR),1)
PHI_ARCH = 1
include $(COMMON_PATH)/make/stdArchAvr.mk
endif
ifeq ($(PHI_ARCH_CM4),1)
PHI_ARCH = 1
include $(COMMON_PATH)/make/stdArchCm4.mk
endif

ifneq ($(PHI_ARCH),1)
$(error Architecture is not defined)
endif

ifeq ($(PHI_NO_OS),1)
ifeq ($(DEBUG_BUILD),1)
BUILD_ROOT =$(PROJ_ROOT)/build_root/$(TARGET_BOARD)/debug_noOS
else
BUILD_ROOT =$(PROJ_ROOT)/build_root/$(TARGET_BOARD)/release_noOS
endif 
else
ifeq ($(DEBUG_BUILD),1)
BUILD_ROOT =$(PROJ_ROOT)/build_root/$(TARGET_BOARD)/debug
else
BUILD_ROOT =$(PROJ_ROOT)/build_root/$(TARGET_BOARD)/release
endif 
endif

ifeq ($(PHIHAL_PATH),)
PHIHAL_PATH=$(PROJ_ROOT)/phiHal
endif

ifeq ($(PHI_INTERNAL),1)
PHICOMMON_OBJDIR = $(BUILD_ROOT)/phiCommon
PHIHAL_OBJDIR = $(BUILD_ROOT)/phiHal
PHIXOS_OBJDIR = $(BUILD_ROOT)/phiXOS
else
ifeq ($(DEBUG_BUILD),1)
	LIBDIR = $(COMMON_PATH)/lib/$(MCU)/debug
else
	LIBDIR = $(COMMON_PATH)/lib/$(MCU)/release
endif
PHICOMMON_OBJDIR = $(LIBDIR)
PHIHAL_OBJDIR = $(LIBDIR)
PHIXOS_OBJDIR = $(LIBDIR)
endif

OPTCFLAGS = -ffunction-sections -fdata-sections
#OPTCFLAGS = -funsafe-loop-optimizations

# For AVRStudio
#DEBUGCFLAGS = -gdwarf-2 -g2

# For GDB
DEBUGCFLAGS = -ggdb -g3

#
# define some variables based on the AVR base path in $(AVR)
#
CC      = "$(CROSS_COMPILE)gcc"
CXX     = "$(CROSS_COMPILE)g++"
CPP     = "$(CROSS_COMPILE)cpp" -P
AR      = "$(CROSS_COMPILE)ar"
AS      = $(CC) -x assembler-with-cpp
LD      = $(CC)
LDXX	= $(CXX)
OBJCOPY = "$(CROSS_COMPILE)objcopy"
OBJSIZE = "$(CROSS_COMPILE)size"
STRIP   = "$(CROSS_COMPILE)strip"
INSTALL = cp -a
RM      = rm -f
RM_R    = rm -rf
RN      = mv
MKDIR_P = mkdir -p
SHELL   = /bin/sh
#CHECKER = sparse
#DOXYGEN = doxygen
#AVRDUDE = avrdude
#FLEXCAT = $(top_srcdir)/tools/flexcat/flexcat

# For conversion from ELF to COFF for use in debugging / simulating in AVR Studio or VMLAB.
COFFCONVERT=$(OBJCOPY) \
	--debugging \
	--change-section-address .data-0x800000 \
	--change-section-address .bss-0x800000 \
	--change-section-address .noinit-0x800000 \
	--change-section-address .eeprom-0x810000

INCDIR  = -I. -I$(COMMON_PATH)/include
# -IphiXOS/net/lwip/src/include -IphiXOS/net/lwip/src/include/ipv4
#LIBDIR  = lib

# output format can be srec, ihex (avrobj is always created)
FORMAT = srec
#FORMAT = ihex

# Compiler flags for generating dependencies
DEP_FLAGS = -MMD -MP

# Compiler flags for generating source listings
LIST_FLAGS = -Wa,-anhlmsd=$(@:.o=.lst) -dp

# Linker flags for generating map files
# Only in embedded related projects generate map files
MAP_FLAGS = -Wl,-Map=$(@:%.elf=%.map)

# Compiler warning flags for both C and C++
WARNFLAGS = \
	-W -Wformat -Wall -Wundef -Wpointer-arith -Wcast-qual \
	-Wcast-align -Wwrite-strings -Wsign-compare \
	-Wmissing-noreturn \
	-Wextra -Wstrict-aliasing=2\
#	-Wunsafe-loop-optimizations

# Compiler warning flags for C only
C_WARNFLAGS = \
	-Wmissing-prototypes -Wstrict-prototypes

C_COMPILER_STD = -std=gnu99

# Default C preprocessor flags (for C, C++ and cpp+as)
CPPFLAGS = $(ARCHCPPFLAGS) $(INCDIR) $(ADD_CPP_FLAGS)

# Default C compiler flags
CFLAGS = $(ARCHCFLAGS) $(OPTCFLAGS) $(DEBUGCFLAGS) $(WARNFLAGS) $(C_WARNFLAGS) \
	$(DEP_FLAGS) $(LIST_FLAGS) $(C_COMPILER_STD)

# Default C++ compiler flags
CXXFLAGS = $(ARCHCXXFLAGS) $(OPTCFLAGS) $(DEBUGCFLAGS) $(WARNFLAGS) \
	$(DEP_FLAGS) $(LIST_FLAGS) -fno-exceptions

# Default compiler assembly flags
CPPAFLAGS = $(ARCHCPPAFLAGS) $(DEBUGCFLAGS) -MMD

# Default assembler flags
ASFLAGS	= $(ARCHASFLAGS) $(DEBUGCFLAGS)

# Default linker flags
LDFLAGS = $(ARCHLDFLAGS) $(MAP_FLAGS)
LDFLAGS += -Wl,--gc-sections

# Flags for avrdude
AVRDUDEFLAGS = $(DPROG)

# additional libs
LIBS = -lm

# Archiver flags
ARFLAGS = rcs

ifndef MCU
$(error MCU is not defined)
endif

CPPFLAGS += \
	-DPHI_CFG_STACK_SIZE=$(CFG_STACK_SIZE) \
	-DPHI_CFG_KERN_MONITOR=$(CFG_KERN_MONITOR) \
	-DPHI_CFG_KERN_PRI_INHERIT=$(CFG_KERN_PRI_INHERIT) \
	-DPHI_CFG_KERN_SIGNALS=$(CFG_KERN_SIGNALS) \
	-DPHI_CFG_DEBUG_TRACE_ENABLE=$(DEBUG)
	
ifeq ($(DEBUG), 1)
CPPFLAGS += -DPHI_DEBUG_ENABLE=1
endif
