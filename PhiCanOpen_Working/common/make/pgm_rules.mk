ifeq ($(CPU_AVR),1)
PMCSRC = $(MPMCSRC)
else
CSRC = $(MPMCSRC)
endif

PMCOBJ0 = $(foreach file,$(PMCSRC),$(OBJDIR)/$(file:%.c=%_P0.o))
PMCOBJ1 = $(foreach file,$(PMCSRC),$(OBJDIR)/$(file:%.c=%_P1.o))
PMCOBJ2 = $(foreach file,$(PMCSRC),$(OBJDIR)/$(file:%.c=%_P2.o))
PMCOBJ3 = $(foreach file,$(PMCSRC),$(OBJDIR)/$(file:%.c=%_P3.o))

ADDITIONAL_OBJ = $(PMCOBJ0) $(PMCOBJ1) $(PMCOBJ2) $(PMCOBJ3)
ADDITIONAL_SRC = $(PMCSRC) 

$(PMCOBJ0) : $(OBJDIR)/%_P0.o : %.c
	$L "$(PACKAGE_NAME): Compiling $< (PROGMEM 0)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -c -D_PROGMEM_ARG=0 $(CFLAGS) $(CPPFLAGS) $< -o $@

$(PMCOBJ1) : $(OBJDIR)/%_P1.o : %.c
	$L "$(PACKAGE_NAME): Compiling $< (PROGMEM 1)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -c -D_PROGMEM_ARG=1 $(CFLAGS) $(CPPFLAGS) $< -o $@

$(PMCOBJ2) : $(OBJDIR)/%_P2.o : %.c
	$L "$(PACKAGE_NAME): Compiling $< (PROGMEM 2)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -c -D_PROGMEM_ARG=2 $(CFLAGS) $(CPPFLAGS) $< -o $@

$(PMCOBJ3) : $(OBJDIR)/%_P3.o : %.c
	$L "$(PACKAGE_NAME): Compiling $< (PROGMEM 3)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -c -D_PROGMEM_ARG=3 $(CFLAGS) $(CPPFLAGS) $< -o $@

