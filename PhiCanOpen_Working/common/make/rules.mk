#
#                            Copyright (c) 2012-2013
#                         PhiRobotics Research Pvt. Ltd.
#               Vedant Commercial Complex, Vartak Nagar, Thane(w),
#                           Maharashtra-400606, India
#
#  For licensing information, see the file 'LICENSE' in the root folder of
#  this software module.
#

# Verbosity
ifeq ($(VERBOSE),1)
# Verbose build
Q :=
L := @echo
else
# Quiet build
Q := @
L := @echo
endif

ifneq ($(CXXSRC),)
LD      = $(LDXX)
endif

ifneq ($(LIB),)
	TRG = lib$(LIB)
else
	TRG = $(PACKAGE_NAME).elf
endif

# In embedded we have to build s19, hex and bin
ifneq ($(LIB),)
	TRG_TRG := $(OUTDIR)/$(TRG)
else
	TRG_TRG := $(OUTDIR)/$(TRG) $(OUTDIR)/$(PACKAGE_NAME).s19 $(OUTDIR)/$(PACKAGE_NAME).hex
ifeq ($(BUILD_TGT_BIN),1)
	TRG_TRG += $(OUTDIR)/$(PACKAGE_NAME).bin
endif
endif

ifeq ($(BUILD_PRODUCT),1)
	TRG_PDC += $(OUTDIR)/$(PACKAGE_NAME).product
endif

TRG_ALL := $(TRG_TRG) $(TRG_PDC)

# The default target
.PHONY: all
all: $(TRG_ALL)

# In embedded systems the target CPU is needed,
# but there are different options on how to pass
# it to the compiler.
ifneq ($(MCU),)
        MCPU = -mmcu=$(MCU)
endif
ifneq ($(CPU),)
        MCPU = -mcpu=$(CPU)
endif

# If a CPU is specified add to
# project specific flags.
ifneq ($(MCPU),)
        CFLAGS    += $(MCPU)
        CXXFLAGS  += $(MCPU)
        ASFLAGS   += $(MCPU)
        CPPAFLAGS += $(MCPU)
        LDFLAGS   += $(MCPU)
endif

ifneq ($(LDSCRIPT),)
        LDFLAGS += -Wl,-dT$(LDSCRIPT)_p
endif

# Debug stuff
ifneq ($(DEBUG),1)
        CFLAGS += -fomit-frame-pointer
        CXXFLAGS += -fomit-frame-pointer
endif

# Product build stuff
ifeq ($(BUILD_PRODUCT),1)
        PRODUCT_CMD = $Q $(STRIP) -S -g -d -x $@
		PRODUCT_ROOT = $(PROJ_ROOT)/build/.product/$(PRODUCT_NAME)/$(PRODUCT_VERSION)
		PRODUCT_INCLUDE_ROOT = $(PRODUCT_ROOT)/include
ifeq ($(DEBUG_BUILD),1)
		PRODUCT_LIB_ROOT = $(PRODUCT_ROOT)/lib/$(MCU)/debug
else
		PRODUCT_LIB_ROOT = $(PRODUCT_ROOT)/lib/$(MCU)/release
endif 
		PRODUCT_STRUCTURE_CREATE = $(MKDIR_P) $(PRODUCT_INCLUDE_ROOT) ; $(MKDIR_P) $(PRODUCT_LIB_ROOT) ;
		PRODUCT_INSTALL = $(INSTALL) $(PROJ_ROOT)/$(PROJECT_DIR)/include/* $(PRODUCT_INCLUDE_ROOT) ;
		PRODUCT_INSTALL += $(INSTALL) $(OUTDIR)/$(TRG) $(PRODUCT_LIB_ROOT) ;
# special case for common package
ifeq ($(PACKAGE_NAME),phiCommon)
		PRODUCT_STRUCTURE_CREATE += $(MKDIR_P) $(PRODUCT_ROOT)/build ; $(MKDIR_P) $(PRODUCT_ROOT)/make
		PRODUCT_INSTALL += $(INSTALL) $(PROJ_ROOT)/$(PROJECT_DIR)/build/* $(PRODUCT_ROOT)/build ;
		PRODUCT_INSTALL += $(INSTALL) $(PROJ_ROOT)/$(PROJECT_DIR)/make/* $(PRODUCT_ROOT)/make ;
endif 
else
		PRODUCT_CMD =
endif

COBJ    = $(foreach file,$(CSRC:%.c=%.o),$(OBJDIR)/$(file))
CXXOBJ  = $(foreach file,$(CXXSRC:%.cpp=%.o),$(OBJDIR)/$(file))
PCOBJ   = $(foreach file,$(PCSRC:%.c=%_P.o),$(OBJDIR)/$(file))
AOBJ    = $(foreach file,$(ASRC:%.s=%.o),$(OBJDIR)/$(file))
CPPAOBJ = $(foreach file,$(CPPASRC:%.S=%.o),$(OBJDIR)/$(file))
OBJ     = $(COBJ) $(CXXOBJ) $(PCOBJ) $(AOBJ) $(CPPAOBJ)
SRC    := $(CSRC) $(CXXSRC) $(PCSRC) $(ASRC) $(CPPASRC)

$(LDSCRIPT_TRG) : $(LDSCRIPT_SRC)
	$L "$(PACKAGE_NAME): Prepare linker script $< (CPP)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CPP) -DCFG_STACK_SIZE=$(CFG_STACK_SIZE) -DCFG_OS_STACK_SIZE=$(CFG_OS_STACK_SIZE) $< -o $@

# Compile: instructions to create assembler and/or object files from C source
$(COBJ) : $(OBJDIR)/%.o : %.c
	$L "$(PACKAGE_NAME): Compiling $< (C)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@
	$(PRODUCT_CMD)

# Compile: instructions to create assembler and/or object files from C++ source
$(CXXOBJ) : $(OBJDIR)/%.o : %.cpp
	$L "$(PACKAGE_NAME): Compiling $< (C++)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CXX) -c $(CXXFLAGS) $(CPPFLAGS) $< -o $@
	$(PRODUCT_CMD)

# Generate assembly sources from C files (debug)
$(OBJDIR)/%.s : %.c
	$L "$(PACKAGE_NAME): Generating asm source $<"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -S $(CFLAGS) $< -o $@

# Generate special progmem variant of a source file
$(PCOBJ) : $(OBJDIR)/%_P.o : %.c
	$L "$(PACKAGE_NAME): Compiling $< (PROGMEM)"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -c -D_PROGMEM $(CFLAGS) $(CPPFLAGS) $< -o $@
	$(PRODUCT_CMD)

# Assemble: instructions to create object file from assembler files
$(AOBJ): $(OBJDIR)/%.o : %.s
	$L "$(PACKAGE_NAME): Assembling $<"
	@$(MKDIR_P) $(dir $@)
	$Q $(AS) -c $(ASFLAGS) $< -o $@
	$(PRODUCT_CMD)

$(CPPAOBJ): $(OBJDIR)/%.o : %.S
	$L "$(PACKAGE_NAME): Assembling with CPP $<"
	@$(MKDIR_P) $(dir $@)
	$Q $(CC) -c $(CPPAFLAGS) $(CPPFLAGS) $< -o $@
	$(PRODUCT_CMD)

# Link: instructions to create elf output file from object files
$(OUTDIR)/$(PACKAGE_NAME).elf $(OUTDIR)/$(PACKAGE_NAME)_nostrip: $(OBJ) $(LDSCRIPT_TRG) $(DEPS)
	$L "$(PACKAGE_NAME): Linking $@"
	@$(MKDIR_P) $(dir $@)
	$Q $(LD) $(OBJ) $(LDFLAGS) -L $(BUILD_ROOT)/$(PACKAGE_NAME)/scripts -o $@ $(LIBS)
	$(OBJSIZE) $@

# Instructions to create a static library from object files
$(OUTDIR)/lib$(LIB): $(OBJ)
	$L "$(PACKAGE_NAME): Creating static library $@"
	@$(MKDIR_P) $(dir $@)
	$Q $(AR) $(ARFLAGS) $@ $(OBJ)

# Strip debug info
$(OUTDIR)/$(PACKAGE_NAME): $(OUTDIR)/$(PACKAGE_NAME)_nostrip
	$L "$(PACKAGE_NAME): Generating stripped executable $@"
	$Q $(STRIP) -o $@ $^

$(OUTDIR)/$(PACKAGE_NAME).product: $(TRG_TRG)
	$L "$(PACKAGE_NAME): Creating product $(PACKAGE_NAME)"
	$(MKDIR_P) $(PRODUCT_ROOT)
	$(PRODUCT_STRUCTURE_CREATE)
	$(PRODUCT_INSTALL)
	touch $@

$(OUTDIR)/$(PACKAGE_NAME).hex: $(OUTDIR)/$(PACKAGE_NAME).elf
	$(OBJCOPY) -R .eeprom -O ihex $< $@

$(OUTDIR)/$(PACKAGE_NAME).s19: $(OUTDIR)/$(PACKAGE_NAME).elf
	$(OBJCOPY) -R .eeprom -O srec $< $@

$(OUTDIR)/$(PACKAGE_NAME).bin: $(OUTDIR)/$(PACKAGE_NAME).elf
	$(OBJCOPY) -R .eeprom -O binary $< $@

$(OUTDIR)/$(PACKAGE_NAME).obj: $(OUTDIR)/$(PACKAGE_NAME).elf
	$(OBJCOPY) -O avrobj $< $@

$(OUTDIR)/$(PACKAGE_NAME).rom: $(OUTDIR)/$(PACKAGE_NAME).elf
	$(OBJCOPY) -O $(FORMAT) $< $@
#	$(OBJCOPY) -j .eeprom --set-section-flags=.eeprom="alloc,load" -O $(FORMAT) $< $(@:.rom=.eep)

%.cof: %.elf
	$(COFFCONVERT) -O coff-ext-avr $< $@
#	$(COFFCONVERT) -O coff-avr $< $@   # For use with AVRstudio 3

#make instruction to delete created files
cleanall: clean
clean:
	-$(RM_R) $(OBJDIR)
	-$(RM_R) $(OUTDIR)
	-$(RM_R) $(BUILD_ROOT)/$(PACKAGE_NAME)/scripts 
	-$(RM_R) $(BUILD_ROOT)/configs
	
cleanprod:
	-$(RM_R) $(PRODUCT_ROOT)

# Include dependencies
ifneq ($(strip $(OBJ)),)
-include $(OBJ:%.o=%.d)
endif
