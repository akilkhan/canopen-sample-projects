#!/bin/sh

#
#                             Copyright (c) 2012-2013
#                          PhiRobotics Research Pvt. Ltd.
#               Vedant Commercial Complex, Vartak Nagar, Thane(w),
#                           Maharashtra-400606, India
#
#  For licensing information, see the file 'LICENSE' in the root folder of
#  this software module.
#

what=
tgt=

export DEBUG_BUILD=1
export IMAGE_TYPE="ROM"

for arg in $* ; do
	if [[ $arg == "rel" ]]; then
		export DEBUG_BUILD=0;
		echo "Enable release build option"
	else
		if [[ $arg == "ram" ]]; then
			export IMAGE_TYPE="RAM";
			echo "Enable RAM image build option"
		fi
	fi
done

if [[ $DEBUG_BUILD == 0 ]]; then
	if [[ $IMAGE_TYPE == "RAM" ]]; then
		what=$3;
		tgt=$4;
	else
		what=$2;
		tgt=$3;
	fi
else
	if [[ $IMAGE_TYPE == "RAM" ]]; then
		what=$2;
		tgt=$3;
	else
		what=$1;
		tgt=$2;
	fi
fi

echo "Checking for personal build configuration..."

if [ -e "./config_$TARGET_BOARD.mk" ]
then
	echo "Using local build configuration"
	source ./config_$TARGET_BOARD.mk
else
	if [ -e "d:/hwdev/configs/make/config_$TARGET_BOARD.mk" ]
	then
		echo "Using global build configuration"
		source d:/hwdev/configs/make/config_$TARGET_BOARD.mk
	else
		echo "Implying build configuration is defined in environemnt"
	fi
fi

function build_task {
    if [ $1 == "clean" ]; then
	    echo "=========== CLEAN $2 ==========="
	elif [ $1 == "cleanprod" ]; then
        echo "=========== CLEAN PRODUCT ==========="
	else
	    echo "=========== BUILD $2 ==========="
	fi

    CWD=$(pwd)
    export PROJECT_DIR=$2
    cd $2
    make $1
    if [ $? -ne 0 ]; then
		echo "$2 build failed!!!"
		exit -1
	fi
    cd $CWD
}

function build_all {
	for task in $BUILD_TASKS;
	do
		build_task all $task 
	done
}

function clean_all {
    for task in $BUILD_TASKS;
    do
        build_task clean $task 
    done
}

function clean_prod {
    for task in $BUILD_TASKS;
    do
        build_task cleanprod $task
        break 
    done
}

case $what in
    all)
    	build_all
        ;;
    clean)
    	clean_all
        ;;
    cleanprod)
        clean_prod
        ;;
    help)
        echo "$0 [rel] (<task_name> [clean] | all | clean)"
        echo "    <task_name> 		- builds particular task"
        echo "    <task_name> clean - cleans particular build dir"
        echo "    rel       		- builds release version (smaller size)"
        echo "    all       		- builds everything"
        echo "    clean     		- cleans everything"
        ;;
    *)
    	if [ -z $what ]; then
    		echo "Please specify task name or all (see help)"
    		exit 1
    	fi
    	if [ -z $tgt ]; then
    		tgt="all" 
    	fi
    	build_task $tgt $what
        ;;
esac
