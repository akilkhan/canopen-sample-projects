################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CDL/LPC11UxxLib/src/lpc11uxx_gpio.c \
../CDL/LPC11UxxLib/src/lpc11uxx_i2c.c \
../CDL/LPC11UxxLib/src/lpc11uxx_nmi.c \
../CDL/LPC11UxxLib/src/lpc11uxx_pinsel.c \
../CDL/LPC11UxxLib/src/lpc11uxx_timer16.c \
../CDL/LPC11UxxLib/src/lpc11uxx_timer32.c 

OBJS += \
./CDL/LPC11UxxLib/src/lpc11uxx_gpio.o \
./CDL/LPC11UxxLib/src/lpc11uxx_i2c.o \
./CDL/LPC11UxxLib/src/lpc11uxx_nmi.o \
./CDL/LPC11UxxLib/src/lpc11uxx_pinsel.o \
./CDL/LPC11UxxLib/src/lpc11uxx_timer16.o \
./CDL/LPC11UxxLib/src/lpc11uxx_timer32.o 

C_DEPS += \
./CDL/LPC11UxxLib/src/lpc11uxx_gpio.d \
./CDL/LPC11UxxLib/src/lpc11uxx_i2c.d \
./CDL/LPC11UxxLib/src/lpc11uxx_nmi.d \
./CDL/LPC11UxxLib/src/lpc11uxx_pinsel.d \
./CDL/LPC11UxxLib/src/lpc11uxx_timer16.d \
./CDL/LPC11UxxLib/src/lpc11uxx_timer32.d 


# Each subdirectory must supply rules for building sources it contributes
CDL/LPC11UxxLib/src/%.o: ../CDL/LPC11UxxLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


