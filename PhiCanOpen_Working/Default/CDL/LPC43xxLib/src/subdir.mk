################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CDL/LPC43xxLib/src/Font5x7.c \
../CDL/LPC43xxLib/src/LCDTerm.c \
../CDL/LPC43xxLib/src/debug_frmwrk.c \
../CDL/LPC43xxLib/src/ipc_buffer.c \
../CDL/LPC43xxLib/src/ipc_cmd_buffer.c \
../CDL/LPC43xxLib/src/ipc_int.c \
../CDL/LPC43xxLib/src/ipc_mbx.c \
../CDL/LPC43xxLib/src/ipc_msg_buffer.c \
../CDL/LPC43xxLib/src/ipc_queue.c \
../CDL/LPC43xxLib/src/lpc43xx_adc.c \
../CDL/LPC43xxLib/src/lpc43xx_atimer.c \
../CDL/LPC43xxLib/src/lpc43xx_can.c \
../CDL/LPC43xxLib/src/lpc43xx_cgu.c \
../CDL/LPC43xxLib/src/lpc43xx_dac.c \
../CDL/LPC43xxLib/src/lpc43xx_emc.c \
../CDL/LPC43xxLib/src/lpc43xx_evrt.c \
../CDL/LPC43xxLib/src/lpc43xx_gpdma.c \
../CDL/LPC43xxLib/src/lpc43xx_gpio.c \
../CDL/LPC43xxLib/src/lpc43xx_i2c.c \
../CDL/LPC43xxLib/src/lpc43xx_i2s.c \
../CDL/LPC43xxLib/src/lpc43xx_lcd.c \
../CDL/LPC43xxLib/src/lpc43xx_mcpwm.c \
../CDL/LPC43xxLib/src/lpc43xx_nvic.c \
../CDL/LPC43xxLib/src/lpc43xx_pwr.c \
../CDL/LPC43xxLib/src/lpc43xx_qei.c \
../CDL/LPC43xxLib/src/lpc43xx_rgu.c \
../CDL/LPC43xxLib/src/lpc43xx_rit.c \
../CDL/LPC43xxLib/src/lpc43xx_rtc.c \
../CDL/LPC43xxLib/src/lpc43xx_sct.c \
../CDL/LPC43xxLib/src/lpc43xx_scu.c \
../CDL/LPC43xxLib/src/lpc43xx_ssp.c \
../CDL/LPC43xxLib/src/lpc43xx_timer.c \
../CDL/LPC43xxLib/src/lpc43xx_uart.c \
../CDL/LPC43xxLib/src/lpc43xx_wwdt.c \
../CDL/LPC43xxLib/src/sdio.c 

OBJS += \
./CDL/LPC43xxLib/src/Font5x7.o \
./CDL/LPC43xxLib/src/LCDTerm.o \
./CDL/LPC43xxLib/src/debug_frmwrk.o \
./CDL/LPC43xxLib/src/ipc_buffer.o \
./CDL/LPC43xxLib/src/ipc_cmd_buffer.o \
./CDL/LPC43xxLib/src/ipc_int.o \
./CDL/LPC43xxLib/src/ipc_mbx.o \
./CDL/LPC43xxLib/src/ipc_msg_buffer.o \
./CDL/LPC43xxLib/src/ipc_queue.o \
./CDL/LPC43xxLib/src/lpc43xx_adc.o \
./CDL/LPC43xxLib/src/lpc43xx_atimer.o \
./CDL/LPC43xxLib/src/lpc43xx_can.o \
./CDL/LPC43xxLib/src/lpc43xx_cgu.o \
./CDL/LPC43xxLib/src/lpc43xx_dac.o \
./CDL/LPC43xxLib/src/lpc43xx_emc.o \
./CDL/LPC43xxLib/src/lpc43xx_evrt.o \
./CDL/LPC43xxLib/src/lpc43xx_gpdma.o \
./CDL/LPC43xxLib/src/lpc43xx_gpio.o \
./CDL/LPC43xxLib/src/lpc43xx_i2c.o \
./CDL/LPC43xxLib/src/lpc43xx_i2s.o \
./CDL/LPC43xxLib/src/lpc43xx_lcd.o \
./CDL/LPC43xxLib/src/lpc43xx_mcpwm.o \
./CDL/LPC43xxLib/src/lpc43xx_nvic.o \
./CDL/LPC43xxLib/src/lpc43xx_pwr.o \
./CDL/LPC43xxLib/src/lpc43xx_qei.o \
./CDL/LPC43xxLib/src/lpc43xx_rgu.o \
./CDL/LPC43xxLib/src/lpc43xx_rit.o \
./CDL/LPC43xxLib/src/lpc43xx_rtc.o \
./CDL/LPC43xxLib/src/lpc43xx_sct.o \
./CDL/LPC43xxLib/src/lpc43xx_scu.o \
./CDL/LPC43xxLib/src/lpc43xx_ssp.o \
./CDL/LPC43xxLib/src/lpc43xx_timer.o \
./CDL/LPC43xxLib/src/lpc43xx_uart.o \
./CDL/LPC43xxLib/src/lpc43xx_wwdt.o \
./CDL/LPC43xxLib/src/sdio.o 

C_DEPS += \
./CDL/LPC43xxLib/src/Font5x7.d \
./CDL/LPC43xxLib/src/LCDTerm.d \
./CDL/LPC43xxLib/src/debug_frmwrk.d \
./CDL/LPC43xxLib/src/ipc_buffer.d \
./CDL/LPC43xxLib/src/ipc_cmd_buffer.d \
./CDL/LPC43xxLib/src/ipc_int.d \
./CDL/LPC43xxLib/src/ipc_mbx.d \
./CDL/LPC43xxLib/src/ipc_msg_buffer.d \
./CDL/LPC43xxLib/src/ipc_queue.d \
./CDL/LPC43xxLib/src/lpc43xx_adc.d \
./CDL/LPC43xxLib/src/lpc43xx_atimer.d \
./CDL/LPC43xxLib/src/lpc43xx_can.d \
./CDL/LPC43xxLib/src/lpc43xx_cgu.d \
./CDL/LPC43xxLib/src/lpc43xx_dac.d \
./CDL/LPC43xxLib/src/lpc43xx_emc.d \
./CDL/LPC43xxLib/src/lpc43xx_evrt.d \
./CDL/LPC43xxLib/src/lpc43xx_gpdma.d \
./CDL/LPC43xxLib/src/lpc43xx_gpio.d \
./CDL/LPC43xxLib/src/lpc43xx_i2c.d \
./CDL/LPC43xxLib/src/lpc43xx_i2s.d \
./CDL/LPC43xxLib/src/lpc43xx_lcd.d \
./CDL/LPC43xxLib/src/lpc43xx_mcpwm.d \
./CDL/LPC43xxLib/src/lpc43xx_nvic.d \
./CDL/LPC43xxLib/src/lpc43xx_pwr.d \
./CDL/LPC43xxLib/src/lpc43xx_qei.d \
./CDL/LPC43xxLib/src/lpc43xx_rgu.d \
./CDL/LPC43xxLib/src/lpc43xx_rit.d \
./CDL/LPC43xxLib/src/lpc43xx_rtc.d \
./CDL/LPC43xxLib/src/lpc43xx_sct.d \
./CDL/LPC43xxLib/src/lpc43xx_scu.d \
./CDL/LPC43xxLib/src/lpc43xx_ssp.d \
./CDL/LPC43xxLib/src/lpc43xx_timer.d \
./CDL/LPC43xxLib/src/lpc43xx_uart.d \
./CDL/LPC43xxLib/src/lpc43xx_wwdt.d \
./CDL/LPC43xxLib/src/sdio.d 


# Each subdirectory must supply rules for building sources it contributes
CDL/LPC43xxLib/src/%.o: ../CDL/LPC43xxLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


