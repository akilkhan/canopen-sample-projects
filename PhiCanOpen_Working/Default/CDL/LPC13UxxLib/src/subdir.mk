################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CDL/LPC13UxxLib/src/lpc13uxx_Serial.c \
../CDL/LPC13UxxLib/src/lpc13uxx_clkconfig.c \
../CDL/LPC13UxxLib/src/lpc13uxx_gpio.c \
../CDL/LPC13UxxLib/src/lpc13uxx_nmi.c \
../CDL/LPC13UxxLib/src/lpc13uxx_timer16.c \
../CDL/LPC13UxxLib/src/lpc13uxx_timer32.c \
../CDL/LPC13UxxLib/src/lpc13uxx_uart.c 

OBJS += \
./CDL/LPC13UxxLib/src/lpc13uxx_Serial.o \
./CDL/LPC13UxxLib/src/lpc13uxx_clkconfig.o \
./CDL/LPC13UxxLib/src/lpc13uxx_gpio.o \
./CDL/LPC13UxxLib/src/lpc13uxx_nmi.o \
./CDL/LPC13UxxLib/src/lpc13uxx_timer16.o \
./CDL/LPC13UxxLib/src/lpc13uxx_timer32.o \
./CDL/LPC13UxxLib/src/lpc13uxx_uart.o 

C_DEPS += \
./CDL/LPC13UxxLib/src/lpc13uxx_Serial.d \
./CDL/LPC13UxxLib/src/lpc13uxx_clkconfig.d \
./CDL/LPC13UxxLib/src/lpc13uxx_gpio.d \
./CDL/LPC13UxxLib/src/lpc13uxx_nmi.d \
./CDL/LPC13UxxLib/src/lpc13uxx_timer16.d \
./CDL/LPC13UxxLib/src/lpc13uxx_timer32.d \
./CDL/LPC13UxxLib/src/lpc13uxx_uart.d 


# Each subdirectory must supply rules for building sources it contributes
CDL/LPC13UxxLib/src/%.o: ../CDL/LPC13UxxLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


