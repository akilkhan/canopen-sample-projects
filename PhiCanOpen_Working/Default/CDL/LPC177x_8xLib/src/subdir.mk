################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CDL/LPC177x_8xLib/src/debug_frmwrk.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_adc.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_bod.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_can.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_clkpwr.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_crc.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_dac.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_eeprom.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_emac.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_emc.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_exti.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_gpdma.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_gpio.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_i2c.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_i2s.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_iap.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_lcd.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_mci.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_mcpwm.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_nvic.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_pinsel.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_pwm.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_qei.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_rtc.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_ssp.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_systick.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_timer.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_uart.c \
../CDL/LPC177x_8xLib/src/lpc177x_8x_wwdt.c 

OBJS += \
./CDL/LPC177x_8xLib/src/debug_frmwrk.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_adc.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_bod.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_can.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_clkpwr.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_crc.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_dac.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_eeprom.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_emac.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_emc.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_exti.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_gpdma.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_gpio.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_i2c.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_i2s.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_iap.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_lcd.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_mci.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_mcpwm.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_nvic.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_pinsel.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_pwm.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_qei.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_rtc.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_ssp.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_systick.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_timer.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_uart.o \
./CDL/LPC177x_8xLib/src/lpc177x_8x_wwdt.o 

C_DEPS += \
./CDL/LPC177x_8xLib/src/debug_frmwrk.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_adc.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_bod.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_can.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_clkpwr.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_crc.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_dac.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_eeprom.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_emac.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_emc.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_exti.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_gpdma.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_gpio.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_i2c.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_i2s.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_iap.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_lcd.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_mci.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_mcpwm.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_nvic.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_pinsel.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_pwm.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_qei.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_rtc.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_ssp.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_systick.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_timer.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_uart.d \
./CDL/LPC177x_8xLib/src/lpc177x_8x_wwdt.d 


# Each subdirectory must supply rules for building sources it contributes
CDL/LPC177x_8xLib/src/%.o: ../CDL/LPC177x_8xLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


