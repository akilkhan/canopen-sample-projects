################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CDL/CMSISv2p00_LPC43xx/src/core_cm0.c \
../CDL/CMSISv2p00_LPC43xx/src/core_cm3.c \
../CDL/CMSISv2p00_LPC43xx/src/core_cm4.c \
../CDL/CMSISv2p00_LPC43xx/src/cr_startup_lpc43xx.c \
../CDL/CMSISv2p00_LPC43xx/src/fpu_enable.c \
../CDL/CMSISv2p00_LPC43xx/src/fpu_init.c \
../CDL/CMSISv2p00_LPC43xx/src/init.c \
../CDL/CMSISv2p00_LPC43xx/src/system_LPC43xx.c 

OBJS += \
./CDL/CMSISv2p00_LPC43xx/src/core_cm0.o \
./CDL/CMSISv2p00_LPC43xx/src/core_cm3.o \
./CDL/CMSISv2p00_LPC43xx/src/core_cm4.o \
./CDL/CMSISv2p00_LPC43xx/src/cr_startup_lpc43xx.o \
./CDL/CMSISv2p00_LPC43xx/src/fpu_enable.o \
./CDL/CMSISv2p00_LPC43xx/src/fpu_init.o \
./CDL/CMSISv2p00_LPC43xx/src/init.o \
./CDL/CMSISv2p00_LPC43xx/src/system_LPC43xx.o 

C_DEPS += \
./CDL/CMSISv2p00_LPC43xx/src/core_cm0.d \
./CDL/CMSISv2p00_LPC43xx/src/core_cm3.d \
./CDL/CMSISv2p00_LPC43xx/src/core_cm4.d \
./CDL/CMSISv2p00_LPC43xx/src/cr_startup_lpc43xx.d \
./CDL/CMSISv2p00_LPC43xx/src/fpu_enable.d \
./CDL/CMSISv2p00_LPC43xx/src/fpu_init.d \
./CDL/CMSISv2p00_LPC43xx/src/init.d \
./CDL/CMSISv2p00_LPC43xx/src/system_LPC43xx.d 


# Each subdirectory must supply rules for building sources it contributes
CDL/CMSISv2p00_LPC43xx/src/%.o: ../CDL/CMSISv2p00_LPC43xx/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


