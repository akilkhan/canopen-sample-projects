################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CDL/LPC18xxLib/src/LPC18xx_debug_frmwrk.c \
../CDL/LPC18xxLib/src/debug_frmwrk.c \
../CDL/LPC18xxLib/src/lpc18xx_adc.c \
../CDL/LPC18xxLib/src/lpc18xx_atimer.c \
../CDL/LPC18xxLib/src/lpc18xx_can.c \
../CDL/LPC18xxLib/src/lpc18xx_cgu.c \
../CDL/LPC18xxLib/src/lpc18xx_dac.c \
../CDL/LPC18xxLib/src/lpc18xx_emc.c \
../CDL/LPC18xxLib/src/lpc18xx_evrt.c \
../CDL/LPC18xxLib/src/lpc18xx_gpdma.c \
../CDL/LPC18xxLib/src/lpc18xx_gpio.c \
../CDL/LPC18xxLib/src/lpc18xx_i2c.c \
../CDL/LPC18xxLib/src/lpc18xx_i2s.c \
../CDL/LPC18xxLib/src/lpc18xx_lcd.c \
../CDL/LPC18xxLib/src/lpc18xx_mcpwm.c \
../CDL/LPC18xxLib/src/lpc18xx_nvic.c \
../CDL/LPC18xxLib/src/lpc18xx_pwr.c \
../CDL/LPC18xxLib/src/lpc18xx_qei.c \
../CDL/LPC18xxLib/src/lpc18xx_rgu.c \
../CDL/LPC18xxLib/src/lpc18xx_rit.c \
../CDL/LPC18xxLib/src/lpc18xx_rtc.c \
../CDL/LPC18xxLib/src/lpc18xx_sct.c \
../CDL/LPC18xxLib/src/lpc18xx_scu.c \
../CDL/LPC18xxLib/src/lpc18xx_sdio.c \
../CDL/LPC18xxLib/src/lpc18xx_ssp.c \
../CDL/LPC18xxLib/src/lpc18xx_timer.c \
../CDL/LPC18xxLib/src/lpc18xx_uart.c \
../CDL/LPC18xxLib/src/lpc18xx_wwdt.c 

OBJS += \
./CDL/LPC18xxLib/src/LPC18xx_debug_frmwrk.o \
./CDL/LPC18xxLib/src/debug_frmwrk.o \
./CDL/LPC18xxLib/src/lpc18xx_adc.o \
./CDL/LPC18xxLib/src/lpc18xx_atimer.o \
./CDL/LPC18xxLib/src/lpc18xx_can.o \
./CDL/LPC18xxLib/src/lpc18xx_cgu.o \
./CDL/LPC18xxLib/src/lpc18xx_dac.o \
./CDL/LPC18xxLib/src/lpc18xx_emc.o \
./CDL/LPC18xxLib/src/lpc18xx_evrt.o \
./CDL/LPC18xxLib/src/lpc18xx_gpdma.o \
./CDL/LPC18xxLib/src/lpc18xx_gpio.o \
./CDL/LPC18xxLib/src/lpc18xx_i2c.o \
./CDL/LPC18xxLib/src/lpc18xx_i2s.o \
./CDL/LPC18xxLib/src/lpc18xx_lcd.o \
./CDL/LPC18xxLib/src/lpc18xx_mcpwm.o \
./CDL/LPC18xxLib/src/lpc18xx_nvic.o \
./CDL/LPC18xxLib/src/lpc18xx_pwr.o \
./CDL/LPC18xxLib/src/lpc18xx_qei.o \
./CDL/LPC18xxLib/src/lpc18xx_rgu.o \
./CDL/LPC18xxLib/src/lpc18xx_rit.o \
./CDL/LPC18xxLib/src/lpc18xx_rtc.o \
./CDL/LPC18xxLib/src/lpc18xx_sct.o \
./CDL/LPC18xxLib/src/lpc18xx_scu.o \
./CDL/LPC18xxLib/src/lpc18xx_sdio.o \
./CDL/LPC18xxLib/src/lpc18xx_ssp.o \
./CDL/LPC18xxLib/src/lpc18xx_timer.o \
./CDL/LPC18xxLib/src/lpc18xx_uart.o \
./CDL/LPC18xxLib/src/lpc18xx_wwdt.o 

C_DEPS += \
./CDL/LPC18xxLib/src/LPC18xx_debug_frmwrk.d \
./CDL/LPC18xxLib/src/debug_frmwrk.d \
./CDL/LPC18xxLib/src/lpc18xx_adc.d \
./CDL/LPC18xxLib/src/lpc18xx_atimer.d \
./CDL/LPC18xxLib/src/lpc18xx_can.d \
./CDL/LPC18xxLib/src/lpc18xx_cgu.d \
./CDL/LPC18xxLib/src/lpc18xx_dac.d \
./CDL/LPC18xxLib/src/lpc18xx_emc.d \
./CDL/LPC18xxLib/src/lpc18xx_evrt.d \
./CDL/LPC18xxLib/src/lpc18xx_gpdma.d \
./CDL/LPC18xxLib/src/lpc18xx_gpio.d \
./CDL/LPC18xxLib/src/lpc18xx_i2c.d \
./CDL/LPC18xxLib/src/lpc18xx_i2s.d \
./CDL/LPC18xxLib/src/lpc18xx_lcd.d \
./CDL/LPC18xxLib/src/lpc18xx_mcpwm.d \
./CDL/LPC18xxLib/src/lpc18xx_nvic.d \
./CDL/LPC18xxLib/src/lpc18xx_pwr.d \
./CDL/LPC18xxLib/src/lpc18xx_qei.d \
./CDL/LPC18xxLib/src/lpc18xx_rgu.d \
./CDL/LPC18xxLib/src/lpc18xx_rit.d \
./CDL/LPC18xxLib/src/lpc18xx_rtc.d \
./CDL/LPC18xxLib/src/lpc18xx_sct.d \
./CDL/LPC18xxLib/src/lpc18xx_scu.d \
./CDL/LPC18xxLib/src/lpc18xx_sdio.d \
./CDL/LPC18xxLib/src/lpc18xx_ssp.d \
./CDL/LPC18xxLib/src/lpc18xx_timer.d \
./CDL/LPC18xxLib/src/lpc18xx_uart.d \
./CDL/LPC18xxLib/src/lpc18xx_wwdt.d 


# Each subdirectory must supply rules for building sources it contributes
CDL/LPC18xxLib/src/%.o: ../CDL/LPC18xxLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


