################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CDL/LPC17xxLib/src/lpc17xx_adc.c \
../CDL/LPC17xxLib/src/lpc17xx_can.c \
../CDL/LPC17xxLib/src/lpc17xx_clkpwr.c \
../CDL/LPC17xxLib/src/lpc17xx_dac.c \
../CDL/LPC17xxLib/src/lpc17xx_debug_frmwrk.c \
../CDL/LPC17xxLib/src/lpc17xx_emac.c \
../CDL/LPC17xxLib/src/lpc17xx_exti.c \
../CDL/LPC17xxLib/src/lpc17xx_gpdma.c \
../CDL/LPC17xxLib/src/lpc17xx_gpio.c \
../CDL/LPC17xxLib/src/lpc17xx_i2c.c \
../CDL/LPC17xxLib/src/lpc17xx_i2s.c \
../CDL/LPC17xxLib/src/lpc17xx_libcfg_default.c \
../CDL/LPC17xxLib/src/lpc17xx_mcpwm.c \
../CDL/LPC17xxLib/src/lpc17xx_nvic.c \
../CDL/LPC17xxLib/src/lpc17xx_pinsel.c \
../CDL/LPC17xxLib/src/lpc17xx_pwm.c \
../CDL/LPC17xxLib/src/lpc17xx_qei.c \
../CDL/LPC17xxLib/src/lpc17xx_rit.c \
../CDL/LPC17xxLib/src/lpc17xx_rtc.c \
../CDL/LPC17xxLib/src/lpc17xx_spi.c \
../CDL/LPC17xxLib/src/lpc17xx_ssp.c \
../CDL/LPC17xxLib/src/lpc17xx_systick.c \
../CDL/LPC17xxLib/src/lpc17xx_timer.c \
../CDL/LPC17xxLib/src/lpc17xx_uart.c \
../CDL/LPC17xxLib/src/lpc17xx_wdt.c 

OBJS += \
./CDL/LPC17xxLib/src/lpc17xx_adc.o \
./CDL/LPC17xxLib/src/lpc17xx_can.o \
./CDL/LPC17xxLib/src/lpc17xx_clkpwr.o \
./CDL/LPC17xxLib/src/lpc17xx_dac.o \
./CDL/LPC17xxLib/src/lpc17xx_debug_frmwrk.o \
./CDL/LPC17xxLib/src/lpc17xx_emac.o \
./CDL/LPC17xxLib/src/lpc17xx_exti.o \
./CDL/LPC17xxLib/src/lpc17xx_gpdma.o \
./CDL/LPC17xxLib/src/lpc17xx_gpio.o \
./CDL/LPC17xxLib/src/lpc17xx_i2c.o \
./CDL/LPC17xxLib/src/lpc17xx_i2s.o \
./CDL/LPC17xxLib/src/lpc17xx_libcfg_default.o \
./CDL/LPC17xxLib/src/lpc17xx_mcpwm.o \
./CDL/LPC17xxLib/src/lpc17xx_nvic.o \
./CDL/LPC17xxLib/src/lpc17xx_pinsel.o \
./CDL/LPC17xxLib/src/lpc17xx_pwm.o \
./CDL/LPC17xxLib/src/lpc17xx_qei.o \
./CDL/LPC17xxLib/src/lpc17xx_rit.o \
./CDL/LPC17xxLib/src/lpc17xx_rtc.o \
./CDL/LPC17xxLib/src/lpc17xx_spi.o \
./CDL/LPC17xxLib/src/lpc17xx_ssp.o \
./CDL/LPC17xxLib/src/lpc17xx_systick.o \
./CDL/LPC17xxLib/src/lpc17xx_timer.o \
./CDL/LPC17xxLib/src/lpc17xx_uart.o \
./CDL/LPC17xxLib/src/lpc17xx_wdt.o 

C_DEPS += \
./CDL/LPC17xxLib/src/lpc17xx_adc.d \
./CDL/LPC17xxLib/src/lpc17xx_can.d \
./CDL/LPC17xxLib/src/lpc17xx_clkpwr.d \
./CDL/LPC17xxLib/src/lpc17xx_dac.d \
./CDL/LPC17xxLib/src/lpc17xx_debug_frmwrk.d \
./CDL/LPC17xxLib/src/lpc17xx_emac.d \
./CDL/LPC17xxLib/src/lpc17xx_exti.d \
./CDL/LPC17xxLib/src/lpc17xx_gpdma.d \
./CDL/LPC17xxLib/src/lpc17xx_gpio.d \
./CDL/LPC17xxLib/src/lpc17xx_i2c.d \
./CDL/LPC17xxLib/src/lpc17xx_i2s.d \
./CDL/LPC17xxLib/src/lpc17xx_libcfg_default.d \
./CDL/LPC17xxLib/src/lpc17xx_mcpwm.d \
./CDL/LPC17xxLib/src/lpc17xx_nvic.d \
./CDL/LPC17xxLib/src/lpc17xx_pinsel.d \
./CDL/LPC17xxLib/src/lpc17xx_pwm.d \
./CDL/LPC17xxLib/src/lpc17xx_qei.d \
./CDL/LPC17xxLib/src/lpc17xx_rit.d \
./CDL/LPC17xxLib/src/lpc17xx_rtc.d \
./CDL/LPC17xxLib/src/lpc17xx_spi.d \
./CDL/LPC17xxLib/src/lpc17xx_ssp.d \
./CDL/LPC17xxLib/src/lpc17xx_systick.d \
./CDL/LPC17xxLib/src/lpc17xx_timer.d \
./CDL/LPC17xxLib/src/lpc17xx_uart.d \
./CDL/LPC17xxLib/src/lpc17xx_wdt.d 


# Each subdirectory must supply rules for building sources it contributes
CDL/LPC17xxLib/src/%.o: ../CDL/LPC17xxLib/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


