################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CANopenNode/CANopen.c \
../CANopenNode/CO_Emergency.c \
../CANopenNode/CO_HBconsumer.c \
../CANopenNode/CO_NMT_Heartbeat.c \
../CANopenNode/CO_PDO.c \
../CANopenNode/CO_SDO.c \
../CANopenNode/CO_SDOmaster.c \
../CANopenNode/CO_SYNC.c \
../CANopenNode/application.c \
../CANopenNode/crc16-ccitt.c 

OBJS += \
./CANopenNode/CANopen.o \
./CANopenNode/CO_Emergency.o \
./CANopenNode/CO_HBconsumer.o \
./CANopenNode/CO_NMT_Heartbeat.o \
./CANopenNode/CO_PDO.o \
./CANopenNode/CO_SDO.o \
./CANopenNode/CO_SDOmaster.o \
./CANopenNode/CO_SYNC.o \
./CANopenNode/application.o \
./CANopenNode/crc16-ccitt.o 

C_DEPS += \
./CANopenNode/CANopen.d \
./CANopenNode/CO_Emergency.d \
./CANopenNode/CO_HBconsumer.d \
./CANopenNode/CO_NMT_Heartbeat.d \
./CANopenNode/CO_PDO.d \
./CANopenNode/CO_SDO.d \
./CANopenNode/CO_SDOmaster.d \
./CANopenNode/CO_SYNC.d \
./CANopenNode/application.d \
./CANopenNode/crc16-ccitt.d 


# Each subdirectory must supply rules for building sources it contributes
CANopenNode/%.o: ../CANopenNode/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O2 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


