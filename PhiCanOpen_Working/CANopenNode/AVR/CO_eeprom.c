/**
 * Microcontroller specific code for CANopenNode nonvolatile variables.
 *
 * This file is a template for other microcontrollers.
 *
 * @file        eeprom.h
 * @ingroup     CO_eeprom
 * @version     SVN: \$Id: eeprom.c 46 2013-08-24 09:18:16Z jani22 $
 * @author      Janez Paternoster
 * @copyright   2004 - 2013 Janez Paternoster
 *
 * This file is part of CANopenNode, an opensource CANopen Stack.
 * Project home page is <http://canopennode.sourceforge.net>.
 * For more information on CANopen see <http://www.can-cia.org/>.
 *
 * CANopenNode is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "CO_driver.h"
#include "CO_SDO.h"
#include "CO_OD.h"
#include "CO_Emergency.h"
#include "CO_eeprom.h"
#include "crc16-ccitt.h"
#include <avr/eeprom.h>

#if defined(__AVR_ATmega2560__)
    #define EE_SIZE (0x1000)
#else
    #error Unknown AVR core!
#endif

extern struct sCO_OD_EEPROM CO_OD_EEPROM_init EEMEM;

/* Master boot record is stored at the beginning of eeprom */
typedef struct{
    uint32_t            OD_EEPROMSize;  /* Size of OD_EEPROM block */
}EE_MBR_t;

EE_MBR_t eeMBR EEMEM = {sizeof(CO_OD_EEPROM_init)};

#define EE_readBlock(data, addr, len)   eeprom_read_block(data, addr, len)
#define EE_updateByte(addr, b)          eeprom_update_byte(addr, b)
//#define EE_writeByteNoWait(addr, b)     eeprom_write_byte(addr, b);
#define EE_isWriteInProcess()           (eeprom_is_ready() ? CO_false : CO_true)

/**
 * OD function for accessing _Store parameters_ (index 0x1010) from SDO server.
 *
 * For more information see file CO_SDO.h.
 */
static CO_SDO_abortCode_t CO_ODF_1010(CO_ODF_arg_t *ODF_arg);
static CO_SDO_abortCode_t CO_ODF_1010(CO_ODF_arg_t *ODF_arg){
    CO_EE_t *ee;
    uint32_t value;
    CO_SDO_abortCode_t ret = CO_SDO_AB_NONE;

    /* @AGv: for AVR CO_OD_ROM is not stored in EEPROM, because it is kept in MCU flash and can not be restored,
     * if you need non-volatile OD variable put it to CO_OD_EEPROM */
#if 1
    if(!ODF_arg->reading)
        ret = CO_SDO_AB_UNSUPPORTED_ACCESS;
#else
    ee = (CO_EE_t*) ODF_arg->object;
    value = CO_getUint32(ODF_arg->data);

    if(!ODF_arg->reading){
        /* don't change the old value */
        CO_memcpy(ODF_arg->data, (const uint8_t*)ODF_arg->ODdataStorage, 4U);

        if(ODF_arg->subIndex == 1U){
            if(value == 0x65766173UL){
                /* write ee->OD_ROMAddress, ee->OD_ROMSize to eeprom (blocking function) */

                /* verify data */
                if(0/*error*/){
                    ret = CO_SDO_AB_HW;
                }
            }
            else{
                ret = CO_SDO_AB_DATA_TRANSF;
            }
        }
    }
#endif
    return ret;
}


/**
 * OD function for accessing _Restore default parameters_ (index 0x1011) from SDO server.
 *
 * For more information see file CO_SDO.h.
 */
static CO_SDO_abortCode_t CO_ODF_1011(CO_ODF_arg_t *ODF_arg);
static CO_SDO_abortCode_t CO_ODF_1011(CO_ODF_arg_t *ODF_arg){
    CO_EE_t *ee;
    uint32_t value;
    CO_SDO_abortCode_t ret = CO_SDO_AB_NONE;

    /* @AGv: for AVR CO_OD_ROM is not stored in EEPROM, because it is kept in MCU flash and can not be restored,
     * if you need non-volatile OD variable put it to CO_OD_EEPROM */
#if 1
    if(!ODF_arg->reading)
        ret = CO_SDO_AB_UNSUPPORTED_ACCESS;
#else
    ee = (CO_EE_t*) ODF_arg->object;
    value = CO_getUint32(ODF_arg->data);

    if(!ODF_arg->reading){
        /* don't change the old value */
        CO_memcpy(ODF_arg->data, (const uint8_t*)ODF_arg->ODdataStorage, 4U);

        if(ODF_arg->subIndex >= 1U){
            if(value == 0x64616F6CUL){
                /* Clear the eeprom */

            }
            else{
                ret = CO_SDO_AB_DATA_TRANSF;
            }
        }
    }
#endif
    return ret;
}

/******************************************************************************/
CO_ReturnError_t CO_EE_init_1(
        CO_EE_t                *ee,
        uint8_t                *OD_EEPROMAddress,
        uint32_t                OD_EEPROMSize,
        uint8_t                *OD_ROMAddress,
        uint32_t                OD_ROMSize)
{
    /* configure object variables */
    ee->OD_EEPROMAddress = OD_EEPROMAddress;
    ee->OD_EEPROMSize = OD_EEPROMSize;
    ee->OD_EEPROMCurrentIndex = 0U;
    ee->OD_EEPROMWriteEnable = CO_false;

    if(OD_EEPROMSize > (EE_SIZE - sizeof(EE_MBR_t)))
        return CO_ERROR_OUT_OF_MEMORY;

    /* read the CO_OD_EEPROM from EEPROM, first verify, if data are OK */
    EE_MBR_t MBR;
    EE_readBlock((uint8_t*)&MBR, &eeMBR, sizeof(EE_MBR_t));

    /* read the CO_OD_EEPROM from EEPROM, first verify, if data are OK */
    if(MBR.OD_EEPROMSize >= (2*sizeof(uint32_t)) && MBR.OD_EEPROMSize == OD_EEPROMSize){
        uint32_t firstWordRAM = *((uint32_t*)OD_EEPROMAddress);
        uint32_t firstWordEE, lastWordEE;
        EE_readBlock((uint8_t*)&firstWordEE, (uint8_t *)&CO_OD_EEPROM_init, sizeof(uint32_t));
        EE_readBlock((uint8_t*)&lastWordEE, (uint8_t *)&CO_OD_EEPROM_init+OD_EEPROMSize - sizeof(uint32_t), sizeof(uint32_t));
        if(firstWordRAM == firstWordEE && firstWordRAM == lastWordEE){
            EE_readBlock(OD_EEPROMAddress, (uint8_t *)&CO_OD_EEPROM_init, OD_EEPROMSize);
            ee->OD_EEPROMWriteEnable = CO_true;
        }
        else{
            return CO_ERROR_DATA_CORRUPT;
        }
    }
    else{
        return CO_ERROR_DATA_CORRUPT;
    }

    return CO_ERROR_NO;
}


/******************************************************************************/
void CO_EE_init_2(
        CO_EE_t                *ee,
        CO_ReturnError_t        eeStatus,
        CO_SDO_t               *SDO,
        CO_EM_t                *em)
{
    CO_OD_configure(SDO, OD_H1010_STORE_PARAM_FUNC, CO_ODF_1010, (void*)ee, 0, 0U);
    CO_OD_configure(SDO, OD_H1011_REST_PARAM_FUNC, CO_ODF_1011, (void*)ee, 0, 0U);
    if(eeStatus != CO_ERROR_NO){
        CO_errorReport(em, CO_EM_NON_VOLATILE_MEMORY, CO_EMC_HARDWARE, (uint32_t)eeStatus);
    }
}


/******************************************************************************/
void CO_EE_process(CO_EE_t *ee){
    if((ee != 0) && (ee->OD_EEPROMWriteEnable) && !EE_isWriteInProcess()){
        uint8_t RAMdata;//, eeData;

        /* read eeprom */
        RAMdata = ee->OD_EEPROMAddress[ee->OD_EEPROMCurrentIndex];
        EE_updateByte((uint8_t *)&CO_OD_EEPROM_init + ee->OD_EEPROMCurrentIndex, RAMdata);
#if 0
        eeData = EE_readByte(&CO_OD_EEPROM_init[ee->OD_EEPROMCurrentIndex]);
        /* if bytes in EEPROM and in RAM are different, then write to EEPROM */
        if(eeData != RAMdata){
            EE_writeByteNoWait(&CO_OD_EEPROM_init[ee->OD_EEPROMCurrentIndex], RAMdata);
        }
#endif
        /* verify next addr */
        if(++ee->OD_EEPROMCurrentIndex == ee->OD_EEPROMSize){
            ee->OD_EEPROMCurrentIndex = 0U;
        }
    }
}
