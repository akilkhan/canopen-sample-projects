/*
 * CO_cpu.c
 *
 */

#include "CO_hal.h"
#include <avr/interrupt.h>
#define F_CPU   CPU_F
#include <util/delay.h>

#ifndef BV
#define BV(_b_)     (1 << (_b_))
#endif

#ifndef __OPTIMIZE__
#define CPU_DELAY_TICK(_tmo_)   do{(void)(_tmo_);}while(0)
#else
#define CPU_DELAY_TICK(_tmo_)    \
{ \
    if((_tmo_)-- == 0) \
        return -1; \
    _delay_ms(1); \
}
#endif

#define GPIOPIN_SET(_port_, _pin_, _v_) \
{ \
    if(_v_) \
        PORT ## _port_ |= BV(_pin_); \
    else \
        PORT ## _port_ &= ~BV(_pin_); \
}

#define GPIOPIN_GET(_port_, _pin_) (PIN ## _port_ & BV(_pin_))

#define GPIOPIN_INIT(_port_, _pin_, _d_, _v_) \
{ \
    if(_d_) \
        DDR ## _port_ |= BV(_pin_); \
    else \
        DDR ## _port_ &= ~BV(_pin_); \
    GPIOPIN_SET(_port_, _pin_, _v_); \
}

CO_WEAK_DECL void CO_hal_GpioDevicePinInit(uint8_t devId, uint8_t pinNum, uint8_t dir, uint8_t val)
{
    switch(devId)
    {
    case 0: GPIOPIN_INIT(A, pinNum, dir, val); break;
    case 1: GPIOPIN_INIT(B, pinNum, dir, val); break;
    case 2: GPIOPIN_INIT(C, pinNum, dir, val); break;
    case 3: GPIOPIN_INIT(D, pinNum, dir, val); break;
    case 4: GPIOPIN_INIT(E, pinNum, dir, val); break;
    case 5: GPIOPIN_INIT(F, pinNum, dir, val); break;
    case 6: GPIOPIN_INIT(G, pinNum, dir, val); break;
    case 7: GPIOPIN_INIT(H, pinNum, dir, val); break;
    case 8: GPIOPIN_INIT(J, pinNum, dir, val); break;
    case 9: GPIOPIN_INIT(K, pinNum, dir, val); break;
    case 10: GPIOPIN_INIT(L, pinNum, dir, val); break;
    }
}

CO_WEAK_DECL uint8_t CO_hal_GpioDevicePinStateGet(uint8_t devId, uint8_t pinNum)
{
    switch(devId)
    {
    case 0: return GPIOPIN_GET(A, pinNum);
    case 1: return GPIOPIN_GET(B, pinNum);
    case 2: return GPIOPIN_GET(C, pinNum);
    case 3: return GPIOPIN_GET(D, pinNum);
    case 4: return GPIOPIN_GET(E, pinNum);
    case 5: return GPIOPIN_GET(F, pinNum);
    case 6: return GPIOPIN_GET(G, pinNum);
    case 7: return GPIOPIN_GET(H, pinNum);
    case 8: return GPIOPIN_GET(J, pinNum);
    case 9: return GPIOPIN_GET(K, pinNum);
    case 10: return GPIOPIN_GET(L, pinNum);
    }

    return 0;
}

CO_WEAK_DECL int8_t CO_hal_GpioDevicePinStateSet(uint8_t devId, uint8_t pinNum, uint8_t state)
{
    switch(devId)
    {
    case 0: GPIOPIN_SET(A, pinNum, state); break;
    case 1: GPIOPIN_SET(B, pinNum, state); break;
    case 2: GPIOPIN_SET(C, pinNum, state); break;
    case 3: GPIOPIN_SET(D, pinNum, state); break;
    case 4: GPIOPIN_SET(E, pinNum, state); break;
    case 5: GPIOPIN_SET(F, pinNum, state); break;
    case 6: GPIOPIN_SET(G, pinNum, state); break;
    case 7: GPIOPIN_SET(H, pinNum, state); break;
    case 8: GPIOPIN_SET(J, pinNum, state); break;
    case 9: GPIOPIN_SET(K, pinNum, state); break;
    case 10: GPIOPIN_SET(L, pinNum, state); break;
    default:
        return -1;
    }

    return 0;
}

CO_WEAK_DECL int8_t CO_hal_SpiDeviceWrite(uint8_t devId, uint8_t *buf, uint32_t len, uint32_t tmo)
{
    uint32_t i;
    //uint8_t b;

    if(devId != 0)
        return -1;

    for(i = 0; i < len; i++)
    {
        /* Start transmission */
        SPDR = buf[i];
        /* Wait for transmission complete */
        while(!(SPSR & (1<<SPIF)))
        {
            CPU_DELAY_TICK(tmo);
        }
        //b = SPDR;
    }

    return 0;
}

CO_WEAK_DECL int32_t CO_hal_SpiDeviceRead(uint8_t devId, uint8_t *buf, uint32_t len, uint32_t tmo)
{
    uint32_t i;

    if(devId != 0)
        return -1;

    for(i = 0; i < len; i++)
    {
        /* Start transmission */
        SPDR = 0xFF;
        /* Wait for transmission complete */
        while(!(SPSR & (1<<SPIF)))
        {
            CPU_DELAY_TICK(tmo);
        }
        /* read data */
        buf[i] = SPDR;
    }

    return (int32_t)i;
}

CO_WEAK_DECL CO_CpuInterruptsFlags_t CO_hal_InterruptsSaveDisable(void)
{
    CO_CpuInterruptsFlags_t flags = SREG & BV(7);
    cli();

    return flags;
}

CO_WEAK_DECL void CO_hal_InterruptsRestore(CO_CpuInterruptsFlags_t flags)
{
    //sei();
    SREG |= flags;
}
