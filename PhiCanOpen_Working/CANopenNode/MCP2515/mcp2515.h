/*
 * mcp2515.h
 *
 */

#ifndef MCP2515_H_
#define MCP2515_H_

#ifndef BV
#define BV(_n_) (1 << (_n_))
#endif

#define MCP2515_MAX_BRP_VAL         63
#define MCP2515_MIN_BRP_VAL         0

#define MCP2515_SLEEP_TRY_COUNT     200 /*2 sec*/

#define MCP2515_CMD_TMO             200 /*ms*/
#define MCP2515_READ_TMO            200 /*ms*/
#define MCP2515_WRITE_TMO           200 /*ms*/

#define MCP2515_TXBUFS_NUMBER       3
#define MCP2515_RXBUFS_NUMBER       2
#define MCP2515_RX0FILT_NUMBER      2
#define MCP2515_RX1FILT_NUMBER      4
#define MCP2515_MAX_FILTS_PER_BUF   MAX(MCP2515_RX0FILT_NUMBER, MCP2515_RX1FILT_NUMBER)
#define MCP2515_MAX_FILTS           (MCP2515_RX0FILT_NUMBER + MCP2515_RX1FILT_NUMBER)

/* MCP2515 commands */
#define MCP2515_CMD_RESET           0xC0
#define MCP2515_CMD_READ            0x03
#define MCP2515_CMD_READ_RXB0SIDH   0x90
#define MCP2515_CMD_READ_RXB0D0     0x92
#define MCP2515_CMD_READ_RXB1SIDH   0x94
#define MCP2515_CMD_READ_RXB1D0     0x96
#define MCP2515_CMD_WRITE           0x02
#define MCP2515_CMD_LOAD_TXBSIDH(_n_) (0x40 + 2*(_n_))
#define MCP2515_CMD_LOAD_TXBD0(_n_)   (0x41 + 2*(_n_))
#define MCP2515_CMD_RTS(_m_)        (0x80 | ((_m_) & 0x7))
#define MCP2515_CMD_READ_STATUS     0xA0
#define MCP2515_CMD_RX_STATUS       0xB0
#define MCP2515_CMD_BIT_MOD         0x05

/* resgister addresses */
#define MCP2515_REG_CANSTAT         0x0E
#define MCP2515_REG_CANCTRL         0x0F
#define MCP2515_REG_TEC             0x1C
#define MCP2515_REG_REC             0x1D
#define MCP2515_REG_EFLG            0x2D
#define MCP2515_REG_CANINTF         0x2C
#define MCP2515_REG_CANINTE         0x2B
#define MCP2515_REG_CNF1            0x2A
#define MCP2515_REG_CNF2            0x29
#define MCP2515_REG_CNF3            0x28
#define MCP2515_REG_TXBCTRL(_n_)    (0x30 + (_n_)*0x10)
#define MCP2515_REG_TXBSIDH(_n_)    (0x31 + (_n_)*0x10)
#define MCP2515_REG_TXBSIDL(_n_)    (0x32 + (_n_)*0x10)
#define MCP2515_REG_TXBEID8(_n_)    (0x33 + (_n_)*0x10)
#define MCP2515_REG_TXBEID0(_n_)    (0x34 + (_n_)*0x10)
#define MCP2515_REG_TXBDLC(_n_)     (0x35 + (_n_)*0x10)
#define MCP2515_REG_TXBD0(_n_)      (0x36 + (_n_)*0x10)
#define MCP2515_REG_TXBD1(_n_)      (0x37 + (_n_)*0x10)
#define MCP2515_REG_TXBD2(_n_)      (0x38 + (_n_)*0x10)
#define MCP2515_REG_TXBD3(_n_)      (0x39 + (_n_)*0x10)
#define MCP2515_REG_TXBD4(_n_)      (0x3A + (_n_)*0x10)
#define MCP2515_REG_TXBD5(_n_)      (0x3B + (_n_)*0x10)
#define MCP2515_REG_TXBD6(_n_)      (0x3C + (_n_)*0x10)
#define MCP2515_REG_TXBD7(_n_)      (0x3D + (_n_)*0x10)
#define MCP2515_REG_RXB0CTRL        0x60
#define MCP2515_REG_RXB1CTRL        0x70
#define MCP2515_REG_RXBSIDH(_n_)    (0x61 + (_n_)*0x10)
#define MCP2515_REG_RXBD0(_n_)      (0x66 + (_n_)*0x10)
#define MCP2515_REG_RXFSIDH(_n_)    (0x00 + (_n_)*0x04 + ((_n_) < 3 ? 0x00 : 0x04))
#define MCP2515_REG_RXFSIDL(_n_)    (0x01 + (_n_)*0x04 + ((_n_) < 3 ? 0x00 : 0x04))
#define MCP2515_REG_RXFEID8(_n_)    (0x02 + (_n_)*0x04 + ((_n_) < 3 ? 0x00 : 0x04))
#define MCP2515_REG_RXFEID0(_n_)    (0x03 + (_n_)*0x04 + ((_n_) < 3 ? 0x00 : 0x04))
#define MCP2515_REG_RXMSIDH(_n_)    (0x20 + (_n_)*0x04)
#define MCP2515_REG_RXMSIDL(_n_)    (0x21 + (_n_)*0x04)
#define MCP2515_REG_RXMEID8(_n_)    (0x22 + (_n_)*0x04)
#define MCP2515_REG_RXMEID0(_n_)    (0x23 + (_n_)*0x04)

#define MCP2515_CANSTAT_MODE_M      0xE0
#define MCP2515_CANSTAT_NORM        0x00
#define MCP2515_CANSTAT_SLEEP       0x20
#define MCP2515_CANSTAT_LOOP        0x40
#define MCP2515_CANSTAT_LISTEN      0x60
#define MCP2515_CANSTAT_CONFIG      0x80
#define MCP2515_CANSTAT_ICODE_M     0x0E
#define MCP2515_CANSTAT_NOINT       0x00
#define MCP2515_CANSTAT_ERRINT      0x02
#define MCP2515_CANSTAT_WAKEINT     0x04
#define MCP2515_CANSTAT_TXBINT(_n_) (0x6 + 2*(_n_))
#define MCP2515_CANSTAT_RXBINT(_n_) (0xC + 2*(_n_))

#define MCP2515_CANCTRL_NORM        0x00
#define MCP2515_CANCTRL_SLEEP       0x20
#define MCP2515_CANCTRL_LOOP        0x40
#define MCP2515_CANCTRL_LISTEN      0x60
#define MCP2515_CANCTRL_CONFIG      0x80
#define MCP2515_CANCTRL_ABAT        0x10
#define MCP2515_CANCTRL_OSM         0x08
#define MCP2515_CANCTRL_CLKEN       0x04
#define MCP2515_CANCTRL_CLKPRE(_p_) ((_p_) & 0x3)

#define MCP2515_CANINTE_MERRE       BV(7)
#define MCP2515_CANINTE_WAKIE       BV(6)
#define MCP2515_CANINTE_ERRIE       BV(5)
#define MCP2515_CANINTE_TXIE(_n_)   BV(2 + (_n_))
#define MCP2515_CANINTE_TXIE_ALL    (BV(2) | BV(3) | BV(4))
#define MCP2515_CANINTE_RXIE(_n_)   BV(0 + (_n_))
#define MCP2515_CANINTE_RXIE_ALL    (BV(0) | BV(1))

#define MCP2515_CANINTF_MERRF       BV(7)
#define MCP2515_CANINTF_WAKIF       BV(6)
#define MCP2515_CANINTF_ERRIF       BV(5)
#define MCP2515_CANINTF_TXIF(_n_)   BV(2 + (_n_))
#define MCP2515_CANINTF_RXIF(_n_)   BV(0 + (_n_))

#define MCP2515_EFLG_RX1OVR         BV(7)
#define MCP2515_EFLG_RX0OVR         BV(6)
#define MCP2515_EFLG_TXBO           BV(5)
#define MCP2515_EFLG_TXEP           BV(4)
#define MCP2515_EFLG_RXEP           BV(3)
#define MCP2515_EFLG_RXWAR          BV(2)
#define MCP2515_EFLG_TXWAR          BV(1)
#define MCP2515_EFLG_EWARN          BV(0)

#define MCP2515_CNF1_SJW(_sjw_)     ((((_sjw_)-1) & 0x3) << 6)
#define MCP2515_CNF1_BRP(_brp_)     ((_brp_) & 0x3F)
#define MCP2515_CNF2_BTL            BV(7)
#define MCP2515_CNF2_SAM            BV(6)
#define MCP2515_CNF2_PHSEG1(_ps_)   ((((_ps_)-1) & 0x7) << 3)
#define MCP2515_CNF2_PRSEG(_prs_)   (((_prs_)-1) & 0x7)
#define MCP2515_CNF3_PHSEG2(_ps_)   (((_ps_)-1) & 0x7)

#define MCP2515_TXBCTRL_ABTF        BV(6)
#define MCP2515_TXBCTRL_MLOA        BV(5)
#define MCP2515_TXBCTRL_TXERR       BV(4)
#define MCP2515_TXBCTRL_TXREQ       BV(3)
#define MCP2515_TXBCTRL_TXP(_p_)    ((_p_) & 0x3)

#define MCP2515_TXBSIDH_SID(_id_)   (((uint16_t)(_id_)) >> 3)
#define MCP2515_TXBSIDL_SID(_id_)   ((((uint16_t)(_id_)) & 0x7) << 5)
#define MCP2515_TXBSIDL_EXIDE       BV(3)
#define MCP2515_TXBSIDL_EID(_id_)   ((((uint32_t)(_id_)) >> 16) & 0x3)
#define MCP2515_TXBEID8_EID(_id_)   ((((uint32_t)(_id_)) >> 8) & 0xFF)
#define MCP2515_TXBEID0_EID(_id_)   (((uint32_t)(_id_)) & 0xFF)
#define MCP2515_TXBDLC_RTR          BV(6)
#define MCP2515_TXBDLC_DLC(_l_)    ((_l_) & 0xF)

#define MCP2515_RXBCTRL_RXM(_v_)    (((_v_) & 0x3) << 5)
#define MCP2515_RXBCTRL_RXRTR       BV(3)
#define MCP2515_RXB0CTRL_BUKT       BV(2)
#define MCP2515_RXB0CTRL_FILHIT     BV(0)
#define MCP2515_RXB1CTRL_FILHIT(_v_) ((_v_) & 0x7)

#define MCP2515_STATUS_TXIF(_n_)    (0x1 << (2*(_n_)+3))
#define MCP2515_STATUS_TXREQ(_n_)   (0x1 << (2*((_n_)+1)))
#define MCP2515_STATUS_RXIF(_n_)    (0x1 << (_n_))

#define MCP2515_RXSTATUS_MSG_M      (0xC0)
#define MCP2515_RXSTATUS_NOMSG      0x00
#define MCP2515_RXSTATUS_BUF0       0x40
#define MCP2515_RXSTATUS_BUF1       0x80
#define MCP2515_RXSTATUS_BUFALL     0xC0
#define MCP2515_RXSTATUS_MSGTYPE_M  (0x18)
#define MCP2515_RXSTATUS_STDDATA    (0x00)
#define MCP2515_RXSTATUS_STDRTR     (0x08)
#define MCP2515_RXSTATUS_EXTDATA    (0x10)
#define MCP2515_RXSTATUS_EXTRTR     (0x18)
#define MCP2515_RXSTATUS_FILT(_v_)  ((_v_) & 0x7)

#define MCP2515_RXBSIDH_SID(_r_)   (((uint16_t)(_r_)) << 3)
#define MCP2515_RXBSIDL_SID(_r_)   ((_r_) >> 5)
#define MCP2515_RXBSIDL_SRR        BV(4)
#define MCP2515_RXBSIDL_IDE        BV(3)
#define MCP2515_RXBSIDL_SID(_r_)   ((_r_) >> 5)
#define MCP2515_RXBSIDL_EID(_r_)   ((((uint32_t)(_r_)) & 0x3) << 16)
#define MCP2515_RXBEID8_EID(_r_)   (((uint32_t)(_r_)) << 8)
#define MCP2515_RXBEID0_EID(_r_)   ((uint32_t)(_r_))
#define MCP2515_RXBDLC_RTR         BV(6)
#define MCP2515_RXBDLC_DLC(_r_)    ((_r_) & 0xF)

#define MCP2515_RXFSIDH_SID(_id_)   (((uint16_t)(_id_)) >> 3)
#define MCP2515_RXFSIDL_SID(_id_)   ((((uint16_t)(_id_)) & 0x7) << 5)
#define MCP2515_RXFSIDL_IDE         BV(3)
#define MCP2515_RXFSIDL_EID(_id_)   ((((uint32_t)(_id_)) >> 16) & 0x3)
#define MCP2515_RXFEID8_EID(_id_)   ((((uint32_t)(_id_)) >> 8) & 0xFF)
#define MCP2515_RXFEID0_EID(_id_)   (((uint32_t)(_id_)) & 0xFF)

#define MCP2515_RXMSIDH_SID(_id_)   (((uint16_t)(_id_)) >> 3)
#define MCP2515_RXMSIDL_SID(_id_)   ((((uint16_t)(_id_)) & 0x7) << 5)
#define MCP2515_RXMSIDL_EID(_id_)   ((((uint32_t)(_id_)) >> 16) & 0x3)
#define MCP2515_RXMEID8_EID(_id_)   ((((uint32_t)(_id_)) >> 8) & 0xFF)
#define MCP2515_RXMEID0_EID(_id_)   (((uint32_t)(_id_)) & 0xFF)

#define MCP2515_CMD_BUF(_nm_, _l_)      uint8_t _nm_[(_l_)+2];
#define MCP2515_CMD_DATA(_cb_)          (((uint8_t *)(_cb_))+2)
#define MCP2515_LDTXB_BUF(_nm_, _l_)      uint8_t _nm_[(_l_)+1];
#define MCP2515_LDTXB_DATA(_cb_)          (((uint8_t *)(_cb_))+1)

#endif /* MCP2515_H_ */
