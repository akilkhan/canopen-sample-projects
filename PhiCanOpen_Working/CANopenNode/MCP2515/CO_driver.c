/*
 * CAN module object for generic microcontroller.
 *
 * This file is a template for other microcontrollers.
 *
 * @file        CO_driver.c
 * @ingroup     CO_driver
 * @version     SVN: \$Id: CO_driver.c 46 2013-08-24 09:18:16Z jani22 $
 * @author      Janez Paternoster
 * @copyright   2004 - 2013 Janez Paternoster
 *
 * This file is part of CANopenNode, an opensource CANopen Stack.
 * Project home page is <http://canopennode.sourceforge.net>.
 * For more information on CANopen see <http://www.can-cia.org/>.
 *
 * CANopenNode is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "CO_driver.h"
#include "CO_Emergency.h"
#include "mcp2515.h"
#include <string.h>
#ifdef PHI_ARCH_AVR
#   include <avr/pgmspace.h>
#endif

#define MCP2515_DEBUG

#define MCP2515_FREAD_NSS    0x1 /* not select slave before reading */
#define MCP2515_FREAD_NUS    0x2 /* not unselect slave after reading */

#define MCP2515_SYNC_TXB0    0x1 /* sync messsage in tx buf 0 */
#define MCP2515_SYNC_TXB1    0x2 /* sync messsage in tx buf 1 */
#define MCP2515_SYNC_TXB2    0x4 /* sync messsage in tx buf 2 */
#define MCP2515_FILT_BUSY(_n_)  (0x1 << (_n_)) /* filter in use flag */

#define FLOAT_ROUND(val)    ((val - (unsigned long)val) > 0.5 ? (unsigned long)val+1 : (unsigned long)val)

#define CAN_MAX_TQ_PER_BIT    25
#define CAN_MIN_TQ_PER_BIT    8

typedef struct
{
    uint8_t uTprs, uTph1, uTph2, uTsjw;
} CO_CANbitTiming_t;

#ifdef PHI_ARCH_AVR
#define CO_CANBITTIME_GET(_f_)  pgm_read_byte(&(_f_))
static const CO_CANbitTiming_t gs_bitTimCfg[CAN_MAX_TQ_PER_BIT-CAN_MIN_TQ_PER_BIT+1] PROGMEM = {
#else
#define CO_CANBITTIME_GET(_f_) (_f_)
static const CO_CANbitTiming_t gs_bitTimCfg[CAN_MAX_TQ_PER_BIT-CAN_MIN_TQ_PER_BIT+1] = {
#endif
    /* 8*/ { 3, 2, 2, 1 },
    /* 9*/ { 0, 0, 0, 0 },
    /*10*/ { 4, 3, 2, 1 },
    /*11*/ { 0, 0, 0, 0 },
    /*12*/ { 5, 3, 3, 1 },
    /*13*/ { 0, 0, 0, 0 },
    /*14*/ { 0, 0, 0, 0 },
    /*15*/ { 7, 4, 3, 1 },
    /*16*/ { 7, 4, 4, 1 },
    /*17*/ { 0, 0, 0, 0 },
    /*18*/ { 0, 0, 0, 0 },
    /*19*/ { 0, 0, 0, 0 },
    /*20*/ { 8, 6, 5, 1 },
    /*21*/ { 0, 0, 0, 0 },
    /*22*/ { 0, 0, 0, 0 },
    /*23*/ { 0, 0, 0, 0 },
    /*24*/ { 0, 0, 0, 0 },
    /*25*/ { 0, 0, 0, 0 },
};

static int8_t mcp2515_addFilter(CO_CANmoduleHwData_t * pHw, uint8_t nFilt, CO_bool_t ide, uint32_t val);
static int8_t mcp2515_addMask(CO_CANmoduleHwData_t * pHw, uint8_t nMask, CO_bool_t ide, uint32_t val);
static const CO_CANbitTiming_t *can_getBitTiming(uint8_t uTBit)
{
    if((unsigned char)(uTBit - CAN_MIN_TQ_PER_BIT) > sizeof(gs_bitTimCfg)/sizeof(CO_CANbitTiming_t))
        return 0;

    return &gs_bitTimCfg[uTBit - CAN_MIN_TQ_PER_BIT];
};

static const CO_CANbitTiming_t *can_calcTiming(
        uint32_t uBr,
        float fCpuFreq,
        int32_t nMinBrp,
        int32_t uMaxBrp,
        int32_t *pBrp)
{
    /* temporary simple implementation */
    float fTq, fBeMin = 1.0;
    const CO_CANbitTiming_t *pBitCfg = 0;
    uint8_t i = CAN_MIN_TQ_PER_BIT;
    while((pBitCfg = can_getBitTiming(i)))
    {
        if(CO_CANBITTIME_GET(pBitCfg->uTsjw) == 0)
        {
            i++;
            continue;
        }
        fTq = 1.0 / ((float)uBr * (float)i);
        /* check that TQ fit into CPU freq */
        if(fTq >= (1.0 / fCpuFreq))
        {
            /* check that TQ fit into AVR CAN freq prescaler */
            float fbrp = fCpuFreq * fTq - 1;
            *pBrp = FLOAT_ROUND(fbrp);
            if(*pBrp >= nMinBrp && *pBrp <= uMaxBrp)
            {
                float err = fbrp > *pBrp ? fbrp - (float)*pBrp : (float)*pBrp - fbrp;
                if(err < fBeMin)
                {
                    fBeMin = err;
                    if(err == 0)
                        break;
                }
            }
        }
        i++;
    }
    if(fBeMin == 1.0)
        return 0;
    /*if(beMin > 0)
    {
        Tq = (float)(brp + 1) / cpuFreq;
        CAN_TRACE("CAN_DRV: WARNING actual baudrate is %f, requested %lu\r\n", 1.0/(Tq*i), br);
    }*/

    return pBitCfg;
}

static int32_t mcp2515_doRead(CO_CANmoduleHwData_t *pHw, uint8_t *pCmd, uint8_t uCmdLen,
        uint8_t *pBuf, uint8_t uLen, uint8_t uFlags, uint32_t uTmo)
{
    int8_t eRes;

    if((uFlags & MCP2515_FREAD_NSS) == 0)
    {
        /* slave select */
        eRes = CO_hal_GpioDevicePinStateSet(pHw->ssGpioDevId, pHw->ssGpioPin, 0);
        if(eRes != 0)
            return eRes;
    }

    eRes = CO_hal_SpiDeviceWrite(pHw->spiDevId, pCmd, uCmdLen, uTmo);
    if(eRes != 0)
    {
        if((uFlags & MCP2515_FREAD_NUS) == 0)
            CO_hal_GpioDevicePinStateSet(pHw->ssGpioDevId, pHw->ssGpioPin, 1);
        return eRes;
    }

    int32_t read = CO_hal_SpiDeviceRead(pHw->spiDevId, pBuf, uLen, uTmo);

    if((uFlags & MCP2515_FREAD_NUS) == 0)
    {
        /* slave unselect */
        CO_hal_GpioDevicePinStateSet(pHw->ssGpioDevId, pHw->ssGpioPin, 1);
    }

    return read;
}

static int8_t mcp2515_doWrite(CO_CANmoduleHwData_t *pHw, uint8_t *pBuf, uint8_t uLen, uint32_t uTmo)
{
    /* slave select */
    int8_t eRes = CO_hal_GpioDevicePinStateSet(pHw->ssGpioDevId, pHw->ssGpioPin, 0);
    if(eRes != 0)
        return eRes;

    eRes = CO_hal_SpiDeviceWrite(pHw->spiDevId, pBuf, uLen, uTmo);
    if(eRes != 0)
    {
        CO_hal_GpioDevicePinStateSet(pHw->ssGpioDevId, pHw->ssGpioPin, 1);
        return eRes;
    }

    /* slave unselect */
    eRes = CO_hal_GpioDevicePinStateSet(pHw->ssGpioDevId, pHw->ssGpioPin, 1);
    return eRes;
}

static int32_t mcp2515_readFromAddr(CO_CANmoduleHwData_t *pHw, uint8_t uAddr,
        uint8_t *pBuf, uint8_t uLen, uint32_t uTmo)
{
    uint8_t cmd[2] = {MCP2515_CMD_READ, uAddr};
    return mcp2515_doRead(pHw, cmd, sizeof(cmd), pBuf, uLen, 0, uTmo);
}

static int8_t mcp2515_read_status(CO_CANmoduleHwData_t *pHw, uint8_t *pBuf, uint32_t uTmo)
{
    uint8_t uCmd = MCP2515_CMD_READ_STATUS;
    int32_t read = mcp2515_doRead(pHw, &uCmd, sizeof(uCmd), pBuf, 1, 0, uTmo);
    return read < 0 || read != sizeof(uCmd) ? -1 : 0;
}

static int8_t mcp2515_command(CO_CANmoduleHwData_t *pHw, uint8_t uCmd)
{
    return mcp2515_doWrite(pHw, &uCmd, sizeof(uCmd), MCP2515_CMD_TMO);
}

static int8_t mcp2515_write(CO_CANmoduleHwData_t *pHw, uint8_t uAddr,
        uint8_t *pBuf, uint8_t uLen, uint32_t uTmo)
{
    pBuf[0] = MCP2515_CMD_WRITE;
    pBuf[1] = uAddr;

    return mcp2515_doWrite(pHw, pBuf, uLen + 2, uTmo);
}

static int8_t mcp2515_modifyBits(CO_CANmoduleHwData_t *pHw, uint8_t uAddr,
        uint8_t uMask, uint8_t uVal, uint32_t uTmo)
{
    uint8_t buf[] = {MCP2515_CMD_BIT_MOD, uAddr, uMask, uVal};

    return mcp2515_doWrite(pHw, buf, sizeof(buf), uTmo);
}

static int8_t mcp2515_requestTx(CO_CANmoduleHwData_t *pHw, uint8_t uBufsMask, uint32_t uTmo)
{
    uint8_t cmd = MCP2515_CMD_RTS(uBufsMask);

    return mcp2515_doWrite(pHw, &cmd, sizeof(cmd), uTmo);
}

static int8_t mcp2515_loadTxBuf(CO_CANmoduleHwData_t *pHw, uint8_t nBuf,
        CO_bool_t data, uint8_t *pBuf, uint8_t uLen, uint32_t uTmo)
{
    if(data == CO_false)
        pBuf[0] = MCP2515_CMD_LOAD_TXBSIDH(nBuf);
    else
        pBuf[0] = MCP2515_CMD_LOAD_TXBD0(nBuf);

    return mcp2515_doWrite(pHw, pBuf, uLen + 1, uTmo);
}

static int8_t mcp2515_getFreeTxBuf(CO_CANmoduleHwData_t *pHw)
{
    uint8_t uStat, i;

    int8_t eRes = mcp2515_read_status(pHw, &uStat, MCP2515_READ_TMO);
    if(eRes != 0)
        return -1;

    for(i = 0; i < MCP2515_TXBUFS_NUMBER; i++)
    {
        if((uStat & MCP2515_STATUS_TXREQ(i)) == 0)
            return i;
    }

    return -1;
}

static int8_t mcp2515_handleRx(CO_CANmodule_t *CANmodule, uint8_t nBuf)
{
    CO_CANrxMsg_t rcvMsg;      /* received message in CAN module */
    uint16_t index;             /* index of received message */
    uint32_t rcvMsgIdent;       /* identifier of the received message */
    CO_CANrx_t *buffer = NULL;  /* receive message buffer from CO_CANmodule_t object. */
    CO_bool_t msgMatched = CO_false;
    uint8_t buf[5];

    int32_t eRes = mcp2515_readFromAddr(&CANmodule->hw, MCP2515_REG_RXBSIDH(nBuf), buf, sizeof(buf), MCP2515_READ_TMO);
    if(eRes < 0 || eRes != sizeof(buf))
        return -1;

    rcvMsgIdent = MCP2515_RXBSIDH_SID(buf[0]) | MCP2515_RXBSIDL_SID(buf[1]);

    if((buf[1] & MCP2515_RXBSIDL_SRR) == 0)
    {
        rcvMsg.DLC = MCP2515_RXBDLC_DLC(buf[4]);
        eRes = mcp2515_readFromAddr(&CANmodule->hw, MCP2515_REG_RXBD0(nBuf), rcvMsg.data, rcvMsg.DLC, MCP2515_READ_TMO);
        if(eRes < 0 || eRes != rcvMsg.DLC)
            return -1;
    }
    else
    {
        rcvMsg.DLC = 0;
        rcvMsgIdent |= 0x0800;
    }

    if(CANmodule->useCANrxFilters){
        /* CAN module filters are used. Message with known 11-bit identifier has */
        /* been received */
        eRes = mcp2515_readFromAddr(&CANmodule->hw, nBuf == 0 ? MCP2515_REG_RXB0CTRL : MCP2515_REG_RXB1CTRL,
                                    buf, 1, MCP2515_READ_TMO);
        if(eRes < 0 || eRes != 1)
            return -1;
        if(nBuf == 0)
            index = buf[0] & MCP2515_RXB0CTRL_FILHIT ? 1 : 0;
        else
            index = MCP2515_RXB1CTRL_FILHIT(buf[0]);
        if(index < CANmodule->rxSize){
            buffer = &CANmodule->rxArray[index];
            /* verify also RTR */
            if(((rcvMsgIdent ^ buffer->ident) & buffer->mask) == 0U){
                msgMatched = CO_true;
            }
        }
    }
    else{
        /* CAN module filters are not used, message with any standard 11-bit identifier */
        /* has been received. Search rxArray form CANmodule for the same CAN-ID. */
        buffer = &CANmodule->rxArray[0];
        for(index = CANmodule->rxSize; index > 0U; index--){
            if(((rcvMsgIdent ^ buffer->ident) & buffer->mask) == 0U){
                msgMatched = CO_true;
                break;
            }
            buffer++;
        }
    }

    /* Call specific function, which will process the message */
    if(msgMatched && (buffer != NULL) && (buffer->pFunct != NULL)){
        buffer->pFunct(buffer->object, &rcvMsg);
    }

    return 0;
}

static void mcp2515_handleTx(CO_CANmodule_t *CANmodule, uint8_t nBuf)
{
    /* First CAN message (bootup) was sent successfully */
    CANmodule->firstCANtxMessage = CO_false;
    /* clear flag from previous message */
    CANmodule->bufferInhibitFlag = CO_false;
    /* Are there any new messages waiting to be send */
    if(CANmodule->CANtxCount > 0U){
        uint16_t i;             /* index of transmitting message */

        /* first buffer */
        CO_CANtx_t *buffer = &CANmodule->txArray[0];
        /* search through whole array of pointers to transmit message buffers. */
        for(i = CANmodule->txSize; i > 0U; i--){
            /* if message buffer is full, send it. */
            if(buffer->bufferFull){
                buffer->bufferFull = CO_false;
                CANmodule->CANtxCount--;

                /* Copy message to CAN buffer */
#if CO_NO_SYNC > 0
                CANmodule->bufferInhibitFlag = buffer->syncFlag;
#else
                CANmodule->bufferInhibitFlag = CO_false;
#endif
                /* canSend... */
                MCP2515_LDTXB_BUF(buf, 8);
                /*ID*/
                MCP2515_LDTXB_DATA(buf)[0] = MCP2515_TXBSIDH_SID(buffer->ident & 0x07FFU);
                MCP2515_LDTXB_DATA(buf)[1] = MCP2515_TXBSIDL_SID(buffer->ident & 0x07FFU);
                MCP2515_LDTXB_DATA(buf)[2] = 0; /* TXBnEID8 */
                MCP2515_LDTXB_DATA(buf)[3] = 0; /* TXBnEID0 */
                /* DLC */
                uint8_t dlc = (buffer->ident >> 11) & 0xFU;
                MCP2515_LDTXB_DATA(buf)[4] = MCP2515_TXBDLC_DLC(dlc);
                /* RTR */
                if(buffer->ident & 0x8000U)
                    MCP2515_LDTXB_DATA(buf)[4] |= MCP2515_TXBDLC_RTR;

                int8_t eRes = mcp2515_loadTxBuf(&CANmodule->hw, nBuf, CO_false, buf, 5, MCP2515_WRITE_TMO);
                if(eRes != 0)
                    break;                      /* exit for loop */
                /* data */
                for(uint8_t k = 0; k < dlc; k++)
                    MCP2515_LDTXB_DATA(buf)[k] = buffer->data[k];

                eRes = mcp2515_loadTxBuf(&CANmodule->hw, nBuf, CO_true, buf, dlc, MCP2515_WRITE_TMO);
                if(eRes != 0)
                    break;                      /* exit for loop */
            #if 0 /*def MCP2515_DEBUG*/
                mcp2515_dump(&CANmodule->hw, MCP2515_REG_TXBSIDH(nBuf), 15);
            #endif
                /* request transmission */
                eRes = mcp2515_requestTx(&CANmodule->hw, 1 << nBuf, MCP2515_WRITE_TMO);
#if CO_NO_SYNC > 0
                if(eRes == 0 && buffer->syncFlag){
                    if(i == 0)
                        CANmodule->hw.flags |= MCP2515_SYNC_TXB0;
                    else if(i == 1)
                        CANmodule->hw.flags |= MCP2515_SYNC_TXB1;
                    else
                        CANmodule->hw.flags |= MCP2515_SYNC_TXB2;
                }
#endif
                break;                      /* exit for loop */
            }
            buffer++;
        }/* end of for loop */

        /* Clear counter if no more messages */
        if(i == 0U){
            CANmodule->CANtxCount = 0U;
        }
    }
}

static int8_t mcp2515_addFilter(CO_CANmoduleHwData_t * pHw, uint8_t nFilt, CO_bool_t ide, uint32_t val)
{
    //CAN_TRACE("add filter %d\r\n", nFilt);

    /* RXFnSIDH, RXFnSIDL, RXFnEID8, RXFnEID0 */
    MCP2515_CMD_BUF(buf, 4);

    if(ide)
    {
        MCP2515_CMD_DATA(buf)[0] = MCP2515_RXFSIDH_SID(val >> 18);
        MCP2515_CMD_DATA(buf)[1] = MCP2515_RXFSIDL_SID(val >> 18) |
                MCP2515_RXFSIDL_IDE | MCP2515_RXFSIDL_EID(val);
        MCP2515_CMD_DATA(buf)[2] = MCP2515_RXFEID8_EID(val);
        MCP2515_CMD_DATA(buf)[3] = MCP2515_RXFEID0_EID(val);
    }
    else
    {
        MCP2515_CMD_DATA(buf)[0] = MCP2515_RXFSIDH_SID(val);
        MCP2515_CMD_DATA(buf)[1] = MCP2515_RXFSIDL_SID(val);
        MCP2515_CMD_DATA(buf)[2] = 0;
        MCP2515_CMD_DATA(buf)[3] = 0;
    }

    return mcp2515_write(pHw, MCP2515_REG_RXFSIDH(nFilt), buf, 4, MCP2515_WRITE_TMO);
}

static int8_t mcp2515_addMask(CO_CANmoduleHwData_t * pHw, uint8_t nMask, CO_bool_t ide, uint32_t val)
{
    //CAN_TRACE("add mask %d\r\n", nMask);

    /* RXMnSIDH, RXMnSIDL, RXMnEID8, RXMnEID0 */
    MCP2515_CMD_BUF(buf, 4);

    if(ide)
    {
        MCP2515_CMD_DATA(buf)[0] = MCP2515_RXMSIDH_SID(val >> 18);
        MCP2515_CMD_DATA(buf)[1] = MCP2515_RXMSIDL_SID(val >> 18) |
                MCP2515_RXMSIDL_EID(val);
        MCP2515_CMD_DATA(buf)[2] = MCP2515_RXMEID8_EID(val);
        MCP2515_CMD_DATA(buf)[3] = MCP2515_RXMEID0_EID(val);
    }
    else
    {
        MCP2515_CMD_DATA(buf)[0] = MCP2515_RXMSIDH_SID(val);
        MCP2515_CMD_DATA(buf)[1] = MCP2515_RXMSIDL_SID(val);
        MCP2515_CMD_DATA(buf)[2] = 0;
        MCP2515_CMD_DATA(buf)[3] = 0;
    }

    return mcp2515_write(pHw, MCP2515_REG_RXMSIDH(nMask), buf, 4, MCP2515_WRITE_TMO);
}
static int8_t mcp2515_initFilters(CO_CANmoduleHwData_t *pHw)
{
    int8_t eRes;

    //CAN_TRACE("clear filters\r\n");
    MCP2515_CMD_BUF(buf, 11);
    memset(MCP2515_CMD_DATA(buf), 0, 11);
    eRes = mcp2515_write(pHw, MCP2515_REG_RXFSIDH(0), buf, 8, MCP2515_WRITE_TMO);
    if(eRes != 0)
    {
        //CAN_ERR("Failed to write RXF0SIDH!\r\n");
        return eRes;
    }
    eRes = mcp2515_write(pHw, MCP2515_REG_RXFSIDH(3), buf, 8, MCP2515_WRITE_TMO);
    if(eRes != 0)
    {
        //CAN_ERR("Failed to write RXF3SIDH!\r\n");
        return eRes;
    }

    //CAN_TRACE("disable filtering on RX0\r\n");
    eRes = mcp2515_write(pHw, MCP2515_REG_RXMSIDH(0), buf, 4, MCP2515_WRITE_TMO);
    if(eRes != 0)
    {
        //CAN_ERR("Failed to write RXM0SIDH!\r\n");
        return eRes;
    }
    /* @AGv: special hack, to receive extended frames with disabled
     * filtering (zero masks) we need to set EXIDE bits at least in one RXFnSIDL
    MCP2515_CMD_DATA(buf)[0] = MCP2515_RXFSIDL_IDE;
    eRes = mcp2515_write(pHw, MCP2515_REG_RXFSIDL(1), buf, 1, MCP2515_WRITE_TMO);
    if(eRes != 0)
    {
        //CAN_ERR("Failed to write RXF1SIDL!\r\n");
        return eRes;
    }*/

    //CAN_TRACE("disable filtering on RX1\r\n");
    eRes = mcp2515_write(pHw, MCP2515_REG_RXMSIDH(1), buf, 4, MCP2515_WRITE_TMO);
    if(eRes != 0)
    {
        //CAN_ERR("Failed to write RXM1SIDH!\r\n");
        return eRes;
    }
    /* @AGv: special hack, to receive extended frames with disabled
     * filtering (zero masks) we need to set EXIDE bits at least in one RXFnSIDL
    MCP2515_CMD_DATA(buf)[0] = MCP2515_RXFSIDL_IDE;
    eRes = mcp2515_write(pHw, MCP2515_REG_RXFSIDL(3), buf, 1, MCP2515_WRITE_TMO);
    if(eRes != 0)
    {
        //CAN_ERR("Failed to write RXF1SIDL!\r\n");
        return eRes;
    }*/
    return 0;
}

#ifdef MCP2515_DEBUG
static void mcp2515_dump(CO_CANmoduleHwData_t *pHw, uint8_t uAddr, uint8_t uLen)
{
    volatile uint8_t buf[20];
    volatile uint8_t tmp = 0;

    if(uLen > sizeof(buf))
        uLen = sizeof(buf);

    int32_t eRes = mcp2515_readFromAddr(pHw, uAddr, buf, uLen, MCP2515_READ_TMO);
    if(eRes < 0)
    {
        //CAN_ERR("Failed to read %d bytes!\r\n", uLen);
        return;
    }

    for(uint8_t i = 0; i < uLen; i++)
    {
        tmp++;
        //        CAN_TRACE("%X: %X\r\n", uAddr+i, buf[i]);
    }
}
#endif

/******************************************************************************/
CO_ReturnError_t CO_CANmodule_init(
        CO_CANmodule_t         *CANmodule,
        CO_CANmoduleHwConfig_t *CANhwCfg,
        CO_CANrx_t              rxArray[],
        uint16_t                rxSize,
        CO_CANtx_t              txArray[],
        uint16_t                txSize,
        uint16_t                CANbitRate)
{
    uint16_t i;

    /* Configure object variables */
    CANmodule->hw.ssGpioDevId = CANhwCfg->ssGpioDevId;
    CANmodule->hw.ssGpioPin = CANhwCfg->ssGpioPin;
    CANmodule->hw.spiDevId = CANhwCfg->spiDevId;
#if CO_NO_SYNC > 0
    CANmodule->hw.flags = 0;
#endif
    CANmodule->hw.filters = 0;
    CANmodule->hw.err = 0;
    CANmodule->hw.errOld = 0;
    CANmodule->rxArray = rxArray;
    CANmodule->rxSize = rxSize;
    CANmodule->txArray = txArray;
    CANmodule->txSize = txSize;
    CANmodule->useCANrxFilters = (rxSize <= MCP2515_MAX_FILTS) ? CO_true : CO_false;/* microcontroller dependent */
    CANmodule->bufferInhibitFlag = CO_false;
    CANmodule->firstCANtxMessage = CO_true;
    CANmodule->CANtxCount = 0U;
    CANmodule->em = NULL;

    for(i=0U; i<rxSize; i++){
        rxArray[i].ident = 0U;
        rxArray[i].pFunct = NULL;
    }
    for(i=0U; i<txSize; i++){
        txArray[i].bufferFull = CO_false;
    }

    /* Configure CAN module registers */
    //CAN_TRACE("reset chip\r\n");
    if(mcp2515_command(&CANmodule->hw, MCP2515_CMD_RESET) != 0)
        return CO_ERROR_HW;

    /* make buffer large enough to keep all messages to be send */
    MCP2515_CMD_BUF(buf, 3);

    //CAN_TRACE("set CONFIG mode\r\n");
    MCP2515_CMD_DATA(buf)[0] = MCP2515_CANCTRL_CONFIG;
    if(mcp2515_write(&CANmodule->hw, MCP2515_REG_CANCTRL, buf, 1, MCP2515_WRITE_TMO) != 0)
        return CO_ERROR_HW;

    /* configure bit timings */
    int32_t iBrp;
    const CO_CANbitTiming_t *pBitTimCfg =
            can_calcTiming((uint32_t)CANbitRate*1000, CANhwCfg->fosc, MCP2515_MIN_BRP_VAL, MCP2515_MAX_BRP_VAL, &iBrp);
    if(pBitTimCfg == 0)
        return CO_ERROR_ILLEGAL_BAUDRATE;

    /* Configure CAN timing */
    //CAN_TRACE("set bit timings\r\n");
    iBrp = (iBrp - 1)/2;
    uint8_t tmp = CO_CANBITTIME_GET(pBitTimCfg->uTph2);
    MCP2515_CMD_DATA(buf)[0] = MCP2515_CNF3_PHSEG2(tmp);
    tmp = CO_CANBITTIME_GET(pBitTimCfg->uTph1);
    uint8_t tmp2 = CO_CANBITTIME_GET(pBitTimCfg->uTprs);
    MCP2515_CMD_DATA(buf)[1] = MCP2515_CNF2_BTL | MCP2515_CNF2_SAM |
            MCP2515_CNF2_PHSEG1(tmp) | MCP2515_CNF2_PRSEG(tmp2);
    tmp = CO_CANBITTIME_GET(pBitTimCfg->uTsjw);
    MCP2515_CMD_DATA(buf)[2] = MCP2515_CNF1_SJW(tmp) | MCP2515_CNF1_BRP(iBrp);
    if(mcp2515_write(&CANmodule->hw, MCP2515_REG_CNF3, buf, 3, MCP2515_WRITE_TMO) != 0)
         return CO_ERROR_HW;

    /* interrupts */
    //CAN_TRACE("configure interrupts\r\n");
    if(mcp2515_modifyBits(&CANmodule->hw, MCP2515_REG_CANINTE, 0xFF,
            /*MCP2515_CANINTE_MERRE |*/ MCP2515_CANINTE_ERRIE |/* MCP2515_CANINTE_WAKIE |*/
            MCP2515_CANINTE_TXIE_ALL | MCP2515_CANINTE_RXIE_ALL,
            MCP2515_WRITE_TMO) != 0)
         return CO_ERROR_HW;

    if(mcp2515_modifyBits(&CANmodule->hw, MCP2515_REG_RXB0CTRL, MCP2515_RXB0CTRL_BUKT,
                          MCP2515_RXB0CTRL_BUKT, MCP2515_WRITE_TMO) != 0)
         return CO_ERROR_HW;

    /*volatile uint8_t regs[2];
    int32_t eRes = mcp2515_readFromAddr(&CANmodule->hw, MCP2515_REG_RXB0CTRL, regs, 1, MCP2515_READ_TMO);
    if(eRes < 0 || eRes != sizeof(regs))
        return CO_ERROR_HW;*/

    /* Configure CAN module hardware filters */
    if(mcp2515_initFilters(&CANmodule->hw) != 0)
        return CO_ERROR_HW;
    if(CANmodule->useCANrxFilters){
        /* CAN module filters are used, they will be configured with */
        /* CO_CANrxBufferInit() functions, called by separate CANopen */
        /* init functions. */
        /* Configure all masks so, that received message must match filter */
    }
    else{
        /* CAN module filters are not used, all messages with standard 11-bit */
        /* identifier will be received */
        /* Configure mask 0 so, that all messages with standard identifier are accepted */
    }

    /* goto NORM mode: One Shot Mode - off, CLKOUT Pin - disable */
    //CAN_TRACE("set NORM mode\r\n");
    MCP2515_CMD_DATA(buf)[0] = MCP2515_CANCTRL_NORM;//MCP2515_CANCTRL_LOOP;
    if(mcp2515_write(&CANmodule->hw, MCP2515_REG_CANCTRL, buf, 1, MCP2515_WRITE_TMO) != 0)
         return CO_ERROR_HW;

    //MCP2515_INT_ENABLE(pHw);

    return CO_ERROR_NO;
}


/******************************************************************************/
void CO_CANmodule_disable(CO_CANmodule_t *CANmodule){
    /* turn off the module */
    if(mcp2515_command(&CANmodule->hw, MCP2515_CMD_RESET) != 0)
        return;
}


/******************************************************************************/
CO_ReturnError_t CO_CANrxBufferInit(
        CO_CANmodule_t         *CANmodule,
        uint16_t                index,
        uint16_t                ident,
        uint16_t                mask,
        CO_bool_t               rtr,
        void                   *object,
        void                  (*pFunct)(void *object, const CO_CANrxMsg_t *message))
{
    CO_ReturnError_t ret = CO_ERROR_NO;

    if((CANmodule!=NULL) && (object!=NULL) && (pFunct!=NULL) && (index < CANmodule->rxSize)){
        /* buffer, which will be configured */
        CO_CANrx_t *buffer = &CANmodule->rxArray[index];

        /* Configure object variables */
        buffer->object = object;
        buffer->pFunct = pFunct;

        /* CAN identifier and CAN mask, bit aligned with CAN module. Different on different microcontrollers. */
        buffer->ident = ident & 0x07FFU;
        if(rtr){
            buffer->ident |= 0x0800U;
        }
        buffer->mask = (mask & 0x07FFU) | 0x0800U;

        CO_CpuInterruptsFlags_t flags = CO_hal_InterruptsSaveDisable();
        /* Set CAN hardware module filter and mask. */
        if(CANmodule->useCANrxFilters){
            uint8_t regs[2];
            int32_t eRes;

            MCP2515_CMD_BUF(buf, 3);
            //CAN_TRACE("set CONFIG mode\r\n");
            MCP2515_CMD_DATA(buf)[0] = MCP2515_CANCTRL_CONFIG;
            if(mcp2515_write(&CANmodule->hw, MCP2515_REG_CANCTRL, buf, 1, MCP2515_WRITE_TMO) != 0){
                CO_hal_InterruptsRestore(flags);
                return CO_ERROR_HW;
            }

            if(index < MCP2515_RX0FILT_NUMBER){
                /* if all filters of buf0 are busy */
                if(CANmodule->hw.filters & MCP2515_FILT_BUSY(0) && CANmodule->hw.filters & MCP2515_FILT_BUSY(1)){
                    /* fall back to SW filtering */
                    CANmodule->useCANrxFilters = CO_false;
                    if(mcp2515_initFilters(&CANmodule->hw) != 0)
                        ret = CO_ERROR_HW;
                }
                else {
                    /* read mask0 */
                    eRes = mcp2515_readFromAddr(&CANmodule->hw, MCP2515_REG_RXMSIDH(0), regs, sizeof(regs), MCP2515_READ_TMO);
                    if(eRes < 0 || eRes != sizeof(regs)){
                        ret = CO_ERROR_HW;
                    }
                    /* if mask0 is not set yet */
                    else if(regs[0] == 0 && regs[1] == 0){
                        //if(mcp2515_addMask(&CANmodule->hw, 1, CO_false, mask & 0x07FFU) != 0){
                        int8_t rc = mcp2515_addMask(&CANmodule->hw, 0, CO_false, mask & 0x07FFU);
                        if(rc != 0){
                            ret = CO_ERROR_HW;
                        }
                        else if(mcp2515_addFilter(&CANmodule->hw, index, CO_false, ident & 0x07FFU) != 0){
                            ret = CO_ERROR_HW;
                        }
                    }
                    /* if mask matches to configured mask0 */
                    else if(regs[0] == ((mask >> 3) & 0xFF) && (regs[1] >> 5) == (mask & 0x7)){
                        if(mcp2515_addFilter(&CANmodule->hw, index, CO_false, ident & 0x07FFU) != 0){
                            ret = CO_ERROR_HW;
                        }
                    }
                    else {
                        /* fall back to SW filtering */
                        CANmodule->useCANrxFilters = CO_false;
                        if(mcp2515_initFilters(&CANmodule->hw) != 0)
                            ret = CO_ERROR_HW;
                    }
                }
            }
            /* if all filters of buf1 are busy */
            else if(CANmodule->hw.filters & MCP2515_FILT_BUSY(2) && CANmodule->hw.filters & MCP2515_FILT_BUSY(3) &&
                   CANmodule->hw.filters & MCP2515_FILT_BUSY(4) && CANmodule->hw.filters & MCP2515_FILT_BUSY(5)){
                /* fall back to SW filtering */
                CANmodule->useCANrxFilters = CO_false;
                if(mcp2515_initFilters(&CANmodule->hw) != 0)
                    ret = CO_ERROR_HW;
            }
            else {
                /* read mask1 */
                eRes = mcp2515_readFromAddr(&CANmodule->hw, MCP2515_REG_RXMSIDH(1), regs, sizeof(regs), MCP2515_READ_TMO);
                if(eRes < 0 || eRes != sizeof(regs)){
                    ret = CO_ERROR_HW;
                }
                /* if mask1 is not set yet */
                else if(regs[0] == 0 && regs[1] == 0){
                    //if(mcp2515_addMask(&CANmodule->hw, 1, CO_false, mask & 0x07FFU) != 0){
                    int8_t rc = mcp2515_addMask(&CANmodule->hw, 1, CO_false, mask & 0x07FFU);
                    if(rc != 0){
                        ret = CO_ERROR_HW;
                    }
                    else if(mcp2515_addFilter(&CANmodule->hw, index, CO_false, ident & 0x07FFU) != 0){
                        ret = CO_ERROR_HW;
                    }
                }
                /* if mask matches to configured mask1 */
                else if(regs[0] == ((mask >> 3) & 0xFF) && (regs[1] >> 5) == (mask & 0x7)){
                    if(mcp2515_addFilter(&CANmodule->hw, index, CO_false, ident & 0x07FFU) != 0){
                        ret = CO_ERROR_HW;
                    }
                }
                else {
                    /* fall back to SW filtering */
                    CANmodule->useCANrxFilters = CO_false;
                    if(mcp2515_initFilters(&CANmodule->hw) != 0)
                        ret = CO_ERROR_HW;
                }
            }
            //CAN_TRACE("set NORM mode\r\n");
            MCP2515_CMD_DATA(buf)[0] = MCP2515_CANCTRL_NORM;
            if(mcp2515_write(&CANmodule->hw, MCP2515_REG_CANCTRL, buf, 1, MCP2515_WRITE_TMO) != 0){
                ret = CO_ERROR_HW;
            }
        }
        CO_hal_InterruptsRestore(flags);
    }
    else
        return CO_ERROR_ILLEGAL_ARGUMENT;

    return ret;
}


/******************************************************************************/
CO_CANtx_t *CO_CANtxBufferInit(
        CO_CANmodule_t         *CANmodule,
        uint16_t                index,
        uint16_t                ident,
        CO_bool_t               rtr,
        uint8_t                 noOfBytes,
        CO_bool_t               syncFlag)
{
    CO_CANtx_t *buffer = NULL;

    if((CANmodule != NULL) && (index < CANmodule->txSize)){
        /* get specific buffer */
        buffer = &CANmodule->txArray[index];

        /* CAN identifier, DLC and rtr, bit aligned with CAN module transmit buffer.
         * Microcontroller specific. */
        buffer->ident = ((uint32_t)ident & 0x07FFU)
                      | ((uint32_t)(((uint32_t)noOfBytes & 0xFU) << 11U))
                      | ((uint32_t)(rtr ? 0x8000U : 0U));

        buffer->bufferFull = CO_false;
#if CO_NO_SYNC > 0
        buffer->syncFlag = syncFlag;
#else
        (void)syncFlag;
#endif
    }

    return buffer;
}

/******************************************************************************/
CO_ReturnError_t CO_CANsend(CO_CANmodule_t *CANmodule, CO_CANtx_t *buffer){
    CO_ReturnError_t err = CO_ERROR_NO;
    int8_t eRes = -1;

    /* Verify overflow */
    if(buffer->bufferFull){
        if(!CANmodule->firstCANtxMessage){
            /* don't set error, if bootup message is still on buffers */
            CO_errorReport((CO_EM_t*)CANmodule->em, CO_EM_CAN_TX_OVERFLOW, CO_EMC_CAN_OVERRUN, buffer->ident);
        }
        err = CO_ERROR_TX_OVERFLOW;
    }

    CO_CpuInterruptsFlags_t flags = CO_hal_InterruptsSaveDisable();
    /* if CAN TX buffer is free, copy message to it */
    int8_t i = mcp2515_getFreeTxBuf(&CANmodule->hw);
    if(i != -1){
#if CO_NO_SYNC > 0
        CANmodule->bufferInhibitFlag = buffer->syncFlag;
#else
        CANmodule->bufferInhibitFlag = CO_false;
#endif
        /* copy message and txRequest */
        MCP2515_LDTXB_BUF(buf, 8);
        /*ID*/
        MCP2515_LDTXB_DATA(buf)[0] = MCP2515_TXBSIDH_SID(buffer->ident & 0x07FFU);
        MCP2515_LDTXB_DATA(buf)[1] = MCP2515_TXBSIDL_SID(buffer->ident & 0x07FFU);
        MCP2515_LDTXB_DATA(buf)[2] = 0; /* TXBnEID8 */
        MCP2515_LDTXB_DATA(buf)[3] = 0; /* TXBnEID0 */
        /* DLC */
        uint8_t dlc = (buffer->ident >> 11) & 0xFU;
        MCP2515_LDTXB_DATA(buf)[4] = MCP2515_TXBDLC_DLC(dlc);
        /* RTR */
        if(buffer->ident & 0x8000U)
            MCP2515_LDTXB_DATA(buf)[4] |= MCP2515_TXBDLC_RTR;

        eRes = mcp2515_loadTxBuf(&CANmodule->hw, i, CO_false, buf, 5, MCP2515_WRITE_TMO);
        if(eRes == 0)
        {
            /* data */
            for(uint8_t k = 0; k < dlc; k++)
                MCP2515_LDTXB_DATA(buf)[k] = buffer->data[k];

            eRes = mcp2515_loadTxBuf(&CANmodule->hw, i, CO_true, buf, dlc, MCP2515_WRITE_TMO);
            if(eRes == 0)
            {
            #if 0 /*def MCP2515_DEBUG*/
                mcp2515_dump(&CANmodule->hw, MCP2515_REG_TXBSIDH(i), 15);
            #endif
                /* request transmission */
                eRes = mcp2515_requestTx(&CANmodule->hw, 1 << i, MCP2515_WRITE_TMO);
#if CO_NO_SYNC > 0
                if(eRes == 0 && buffer->syncFlag){
                    if(i == 0)
                        CANmodule->hw.flags |= MCP2515_SYNC_TXB0;
                    else if(i == 1)
                        CANmodule->hw.flags |= MCP2515_SYNC_TXB1;
                    else
                        CANmodule->hw.flags |= MCP2515_SYNC_TXB2;
                }
#endif
            }
        }
    }
    /* if no buffer is free or error, message will be sent by interrupt */
    if(eRes != 0){
        buffer->bufferFull = CO_true;
        CANmodule->CANtxCount++;
    }
    CO_hal_InterruptsRestore(flags);

    return err;
}

#if CO_NO_SYNC > 0
/******************************************************************************/
void CO_CANclearPendingSyncPDOs(CO_CANmodule_t *CANmodule){
    uint32_t tpdoDeleted = 0U;

    CO_CpuInterruptsFlags_t flags = CO_hal_InterruptsSaveDisable();
    /* Abort message from CAN module, if there is synchronous TPDO.
     * Take special care with this functionality. */
    if(CANmodule->hw.flags & (MCP2515_SYNC_TXB0 | MCP2515_SYNC_TXB1 | MCP2515_SYNC_TXB2) &&
        CANmodule->bufferInhibitFlag){
        /* clear TXREQ */
        if(CANmodule->hw.flags & MCP2515_SYNC_TXB0){
            mcp2515_modifyBits(&CANmodule->hw, MCP2515_REG_TXBCTRL(0), MCP2515_TXBCTRL_TXREQ,
                    0, MCP2515_WRITE_TMO);
        }
        if(CANmodule->hw.flags & MCP2515_SYNC_TXB1){
            mcp2515_modifyBits(&CANmodule->hw, MCP2515_REG_TXBCTRL(1), MCP2515_TXBCTRL_TXREQ,
                    0, MCP2515_WRITE_TMO);
        }
        if(CANmodule->hw.flags & MCP2515_SYNC_TXB2){
            mcp2515_modifyBits(&CANmodule->hw, MCP2515_REG_TXBCTRL(2), MCP2515_TXBCTRL_TXREQ,
                    0, MCP2515_WRITE_TMO);
        }
        CANmodule->hw.flags = 0;
        CANmodule->bufferInhibitFlag = CO_false;
        tpdoDeleted = 1U;
    }
    /* delete also pending synchronous TPDOs in TX buffers */
    if(CANmodule->CANtxCount != 0U){
        uint16_t i;
        CO_CANtx_t *buffer = &CANmodule->txArray[0];
        for(i = CANmodule->txSize; i > 0U; i--){
            if(buffer->bufferFull){
                if(buffer->syncFlag){
                    buffer->bufferFull = CO_false;
                    CANmodule->CANtxCount--;
                    tpdoDeleted = 2U;
                }
            }
            buffer++;
        }
    }
    CO_hal_InterruptsRestore(flags);


    if(tpdoDeleted != 0U){
        CO_errorReport((CO_EM_t*)CANmodule->em, CO_EM_TPDO_OUTSIDE_WINDOW, CO_EMC_COMMUNICATION, tpdoDeleted);
    }
}
#endif

#ifdef MCP2515_DEBUG
static volatile CO_bool_t gs_dump = CO_false;
#endif

/******************************************************************************/
void CO_CANverifyErrors(CO_CANmodule_t *CANmodule){
    CO_EM_t* em = (CO_EM_t*)CANmodule->em;
    uint8_t err = CANmodule->hw.err;

#ifdef MCP2515_DEBUG
    if(gs_dump){
        mcp2515_dump(&CANmodule->hw, MCP2515_REG_CANINTE, 5);
        mcp2515_dump(&CANmodule->hw, MCP2515_REG_RXB0CTRL, 1);
        mcp2515_dump(&CANmodule->hw, MCP2515_REG_RXB1CTRL, 1);
    }
#endif

    if(CANmodule->hw.errOld != err){
        CANmodule->hw.errOld = err;

        if(err & MCP2515_EFLG_TXBO){                               /* bus off */
            CO_errorReport(em, CO_EM_CAN_TX_BUS_OFF, CO_EMC_BUS_OFF_RECOVERED, err);
        }
        else{                                               /* not bus off */
            CO_errorReset(em, CO_EM_CAN_TX_BUS_OFF, err);

            if(err & (MCP2515_EFLG_RXWAR | MCP2515_EFLG_TXWAR)){     /* bus warning */
                CO_errorReport(em, CO_EM_CAN_BUS_WARNING, CO_EMC_NO_ERROR, err);
            }
            else{
                CO_errorReset(em, CO_EM_CAN_BUS_WARNING, err);
            }
            if(err & MCP2515_EFLG_RXEP){                           /* RX bus passive */
                CO_errorReport(em, CO_EM_CAN_RX_BUS_PASSIVE, CO_EMC_CAN_PASSIVE, err);
            }
            else{
                CO_errorReset(em, CO_EM_CAN_RX_BUS_PASSIVE, err);
            }

            if(err & MCP2515_EFLG_TXEP){                           /* TX bus passive */
                if(!CANmodule->firstCANtxMessage){
                    CO_errorReport(em, CO_EM_CAN_TX_BUS_PASSIVE, CO_EMC_CAN_PASSIVE, err);
                }
            }
            else{
                CO_bool_t isError = CO_isError(em, CO_EM_CAN_TX_BUS_PASSIVE);
                if(isError){
                    CO_errorReset(em, CO_EM_CAN_TX_BUS_PASSIVE, err);
                    CO_errorReset(em, CO_EM_CAN_TX_OVERFLOW, err);
                }
            }
        }

        if(err & (/*MCP2515_EFLG_RX0OVR |*/ MCP2515_EFLG_RX1OVR)){                                 /* CAN RX bus overflow */
            CO_errorReport(em, CO_EM_CAN_RXB_OVERFLOW, CO_EMC_CAN_OVERRUN, err);
        }
    }
}

//volatile static CO_bool_t gs_intReset = CO_false;

/******************************************************************************/
void CO_CANinterrupt(CO_CANmodule_t *CANmodule){
    uint8_t regs[2];

    int32_t eRes = mcp2515_readFromAddr(&CANmodule->hw, MCP2515_REG_CANINTF, regs, sizeof(regs), MCP2515_READ_TMO);
    if(eRes < 0 || eRes != sizeof(regs))
        return;

    if(regs[0] & MCP2515_CANINTF_RXIF(0))
    {
        if(mcp2515_handleRx(CANmodule, 0) != 0)
            regs[0] &= ~MCP2515_CANINTF_RXIF(0);
    }
    if(regs[0] & MCP2515_CANINTF_RXIF(1))
    {
        if(mcp2515_handleRx(CANmodule, 1) != 0)
            regs[0] &= ~MCP2515_CANINTF_RXIF(1);
    }
    if(regs[0] & MCP2515_CANINTF_TXIF(2))
    {
#if CO_NO_SYNC > 0
        CANmodule->hw.flags &= ~MCP2515_SYNC_TXB2;
#endif
        mcp2515_handleTx(CANmodule, 2);
    }
    if(regs[0] & MCP2515_CANINTF_TXIF(1))
    {
#if CO_NO_SYNC > 0
        CANmodule->hw.flags &= ~MCP2515_SYNC_TXB1;
#endif
        mcp2515_handleTx(CANmodule, 1);
    }
    if(regs[0] & MCP2515_CANINTF_TXIF(0))
    {
#if CO_NO_SYNC > 0
        CANmodule->hw.flags &= ~MCP2515_SYNC_TXB0;
#endif
        mcp2515_handleTx(CANmodule, 0);
    }
    /*if(regs[0] & MCP2515_CANINTF_WAKIF)
    {
        mcp2515_handleWakeup(&CANmodule->hw);
    }*/
    if(regs[0] & MCP2515_CANINTF_ERRIF)
    {
        CANmodule->hw.err = regs[1];
        //mcp2515_handleError(CANmodule, regs[1]);
    }
    /*if(regs[0] & MCP2515_CANINTF_MERRF)
    {
    }*/

    if(regs[0] != 0)
    {
        /* ACK handled interrupts */
        mcp2515_modifyBits(&CANmodule->hw, MCP2515_REG_CANINTF, regs[0], 0x0, MCP2515_WRITE_TMO);
    }
#if 0
    if(gs_intReset)
    {
        mcp2515_dump(&CANmodule->hw, MCP2515_REG_CANINTF, 1);
        /* ACK handled interrupts */
        //mcp2515_modifyBits(&CANmodule->hw, MCP2515_REG_CANINTF, regs[0], 0x0, MCP2515_WRITE_TMO);
		MCP2515_CMD_BUF(buf, 1);
		MCP2515_CMD_DATA(buf)[0] = 0;
		eRes = mcp2515_write(&CANmodule->hw, MCP2515_REG_CANINTF, buf, 1, MCP2515_WRITE_TMO);
        mcp2515_dump(&CANmodule->hw, MCP2515_REG_CANINTF, 1);
    }
#endif
}
