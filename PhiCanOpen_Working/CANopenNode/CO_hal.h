/*
 * CO_hal.h
 *
 */

#ifndef CO_HAL_H_
#define CO_HAL_H_

#include "CO_cpu.h"

/*
 * Support for drivers which uses GPIO and/or SPI, e.g. MCP2515.
 * Stack has own versions of those functions and implies that all HW is initialized before call to CO_init().
 * Implement this functions if you need to customize them, e.g. using third party HAL library.
 */
CO_WEAK_DECL void CO_hal_GpioDevicePinInit(uint8_t devId, uint8_t pinNum, uint8_t dir, uint8_t val);
CO_WEAK_DECL uint8_t CO_hal_GpioDevicePinStateGet(uint8_t devId, uint8_t pinNum);
CO_WEAK_DECL int8_t CO_hal_GpioDevicePinStateSet(uint8_t devId, uint8_t pinNum, uint8_t state);
CO_WEAK_DECL int8_t CO_hal_SpiDeviceWrite(uint8_t devId, uint8_t *buf, uint32_t len, uint32_t tmo);
CO_WEAK_DECL int32_t CO_hal_SpiDeviceRead(uint8_t devId, uint8_t *buf, uint32_t len, uint32_t tmo);

CO_WEAK_DECL CO_CpuInterruptsFlags_t CO_hal_InterruptsSaveDisable(void);
CO_WEAK_DECL void CO_hal_InterruptsRestore(CO_CpuInterruptsFlags_t flags);


#endif /* CO_HAL_H_ */
