#ifndef CO_CPU_H
#define CO_CPU_H

#include <stddef.h>         /* for 'NULL' */
#include <stdint.h>         /* for 'int8_t' to 'uint64_t' */

#ifdef __GNUC__
#define CO_WEAK_DECL __attribute__ ((weak))
#endif

/*#define CO_EEPROM_DECL(x)    x*/
#define CO_ROM_DECL(x)       x
#define CO_ROM_READ8(x)     (x)
#define CO_ROM_READ16(x)    (x)
#define CO_ROM_READ32(x)    (x)
#define CO_ROM_READ8_FROMADDR(a)    (*((uint8_t *)a))
#define CO_ROM_READ16_FROMADDR(a)   (*((uint16_t *)a))
#define CO_ROM_READ32_FROMADDR(a)   (*((uint32_t *)a))

/**
 * @defgroup CO_dataTypes Data types
 * @{
 */
    typedef unsigned char CO_bool_t;
    typedef enum{
        CO_false = 0,
        CO_true = 1
    }CO_boolval_t;
    /* int8_t to uint64_t are defined in stdint.h */
    typedef float                   float32_t;  /**< float32_t */
    typedef long double             float64_t;  /**< float64_t */
    typedef char                    char_t;     /**< char_t */
    typedef unsigned char           oChar_t;    /**< oChar_t */
    typedef unsigned char           domain_t;   /**< domain_t */
/** @} */

typedef uint32_t CO_CpuInterruptsFlags_t;

#endif
