/*
 * CO_cpu.c
 *
 */

#include "CO_hal.h"

#if 0
CO_WEAK_DECL void CO_hal_GpioDevicePinInit(uint8_t devId, uint8_t pinNum, uint8_t dir, uint8_t val)
{
    /* not used */
}

CO_WEAK_DECL uint8_t CO_hal_GpioDevicePinStateGet(uint8_t devId, uint8_t pinNum)
{
    /* not used */
    return 0;
}

CO_WEAK_DECL int8_t CO_hal_GpioDevicePinStateSet(uint8_t devId, uint8_t pinNum, uint8_t state)
{
    /* not used */
    return -1;
}

CO_WEAK_DECL int8_t CO_hal_SpiDeviceWrite(uint8_t devId, uint8_t *buf, uint32_t len, uint32_t tmo)
{
    /* not used */
    return -1;
}

CO_WEAK_DECL int32_t CO_hal_SpiDeviceRead(uint8_t devId, uint8_t *buf, uint32_t len, uint32_t tmo)
{
    /* not used */
    return -1;
}
#endif

CO_WEAK_DECL CO_CpuInterruptsFlags_t CO_hal_InterruptsSaveDisable(void)
{
    CO_CpuInterruptsFlags_t flags;
    register unsigned long reg = 0x40;

    asm volatile (
        "mrs %0, basepri\n"
        "msr basepri, %1"
         : "=r"(flags) : "r"(reg) : "memory", "cc");

    return flags;
}

CO_WEAK_DECL void CO_hal_InterruptsRestore(CO_CpuInterruptsFlags_t flags)
{
    asm volatile (
        "msr basepri, %0"
        : : "r"(flags) : "memory", "cc");
}
