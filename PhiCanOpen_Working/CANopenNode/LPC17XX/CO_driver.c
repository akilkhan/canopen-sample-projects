/*
 * CAN module object for generic microcontroller.
 *
 * This file is a template for other microcontrollers.
 *
 * @file        CO_driver.c
 * @ingroup     CO_driver
 * @version     SVN: \$Id: CO_driver.c 46 2013-08-24 09:18:16Z jani22 $
 * @author      Janez Paternoster
 * @copyright   2004 - 2013 Janez Paternoster
 *
 * This file is part of CANopenNode, an opensource CANopen Stack.
 * Project home page is <http://canopennode.sourceforge.net>.
 * For more information on CANopen see <http://www.can-cia.org/>.
 *
 * CANopenNode is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "CO_driver.h"
#include "CO_Emergency.h"

#define LPC17XX_SYNC_TXB1    0x1 /* sync messsage in tx buf 1 */
#define LPC17XX_SYNC_TXB2    0x2 /* sync messsage in tx buf 2 */
#define LPC17XX_SYNC_TXB3    0x4 /* sync messsage in tx buf 3 */

uint32_t CO_interruptStatus = 0;

/******************************************************************************/
/*void CO_CANsetConfigurationMode(CO_CANmoduleBaseAddr_t CANbaseAddress)
{
    CAN_ModeConfig(CANbaseAddress, CAN_RESET_MODE, ENABLE);
}*/


/******************************************************************************/
/*void CO_CANsetNormalMode(CO_CANmoduleBaseAddr_t CANbaseAddress)
{
    CAN_ModeConfig(CANbaseAddress, CAN_OPERATING_MODE, ENABLE);
}*/


/******************************************************************************/
CO_ReturnError_t CO_CANmodule_init(
        CO_CANmodule_t         *CANmodule,
        CO_CANmoduleHwConfig_t *CANhwCfg,
        CO_CANrx_t              rxArray[],
        uint16_t                rxSize,
        CO_CANtx_t              txArray[],
        uint16_t                txSize,
        uint16_t                CANbitRate)
{
    uint16_t i;

    /* Configure object variables */
    CANmodule->hw.CANbaseAddress = CANhwCfg->uBase;
#if CO_NO_SYNC > 0
    CANmodule->hw.flags = 0;
#endif
    CANmodule->rxArray = rxArray;
    CANmodule->rxSize = rxSize;
    CANmodule->txArray = txArray;
    CANmodule->txSize = txSize;
    CANmodule->useCANrxFilters = CO_false;/* microcontroller dependent */
    CANmodule->bufferInhibitFlag = CO_false;
    CANmodule->firstCANtxMessage = CO_true;
    CANmodule->CANtxCount = 0U;
    CANmodule->errOld = 0U;
    CANmodule->em = NULL;

    for(i=0U; i<rxSize; i++){
        rxArray[i].ident = 0U;
        rxArray[i].pFunct = NULL;
    }
    for(i=0U; i<txSize; i++){
        txArray[i].bufferFull = CO_false;
    }


    /* Configure CAN module registers */
    /* Configure CAN timing */
    CAN_Init(CANmodule->hw.CANbaseAddress, (uint32_t)CANbitRate*1000);

    /* Configure CAN module hardware filters */
    if(CANmodule->useCANrxFilters){
        /* CAN module filters are used, they will be configured with */
        /* CO_CANrxBufferInit() functions, called by separate CANopen */
        /* init functions. */
        /* Configure all masks so, that received message must match filter */
#ifdef __LPC17XX__
        CAN_SetAFMode(LPC_CANAF, CAN_Normal);
#elif defined(__LPC177X_8X__)
        CAN_SetAFMode(CAN_NORMAL);
#endif
    }
    else{
        /* CAN module filters are not used, all messages with standard 11-bit */
        /* identifier will be received */
        /* Configure mask 0 so, that all messages with standard identifier are accepted */
#ifdef __LPC17XX__
        CAN_SetAFMode(LPC_CANAF, CAN_AccBP);
#elif defined(__LPC177X_8X__)
        CAN_SetAFMode(CAN_ACC_BP);
#endif
    }

    /* configure CAN interrupt registers */
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_RIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_TIE1, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_TIE2, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_TIE3, ENABLE);
    /*CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_EIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_DOIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_EPIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_ALIE, ENABLE);
    CAN_IRQCmd(CANmodule->hw.CANbaseAddress, CANINT_BEIE, ENABLE);*/

    return CO_ERROR_NO;
}


/******************************************************************************/
void CO_CANmodule_disable(CO_CANmodule_t *CANmodule){
    /* turn off the module */
    CAN_ModeConfig(CANmodule->hw.CANbaseAddress, CAN_RESET_MODE, ENABLE);
    CAN_DeInit(CANmodule->hw.CANbaseAddress);
}


/******************************************************************************/
/*uint16_t CO_CANrxMsg_readIdent(const CO_CANrxMsg_t *rxMsg){
    return (uint16_t) rxMsg->ident;
}*/


/******************************************************************************/
CO_ReturnError_t CO_CANrxBufferInit(
        CO_CANmodule_t         *CANmodule,
        uint16_t                index,
        uint16_t                ident,
        uint16_t                mask,
        CO_bool_t               rtr,
        void                   *object,
        void                  (*pFunct)(void *object, const CO_CANrxMsg_t *message))
{
    CO_ReturnError_t ret = CO_ERROR_NO;

    if((CANmodule!=NULL) && (object!=NULL) && (pFunct!=NULL) && (index < CANmodule->rxSize)){
        /* buffer, which will be configured */
        CO_CANrx_t *buffer = &CANmodule->rxArray[index];

        /* Configure object variables */
        buffer->object = object;
        buffer->pFunct = pFunct;

        /* CAN identifier and CAN mask */
        buffer->ident = ident & 0x07FFU;
        if(rtr){
            buffer->ident |= 0x0800U;
        }
        buffer->mask = (mask & 0x07FFU) | 0x0800U;

/*#define LPC_CANAF_SFF_CONTR(v)  ((v) & 0x7000 >> 13)
#define LPC_CANAF_SFF_NU        (0x800)
#define LPC_CANAF_SFF_ID(v)     ((v) & 0x7FF)

        uint32_t i;
        CO_bool_t bFiltFound = CO_false;
        for(i = 0; i < LPC_CANAF->SFF_GRP_sa / sizeof(uint32_t); i++)
        {
            uint16_t id = LPC_CANAF_RAM->mask[i] & 0xFFFF;
            if(LPC_CANAF_SFF_CONTR(id) == 1 && (id & LPC_CANAF_SFF_NU) == 0 && LPC_CANAF_SFF_ID(id) == (ident & 0x07FFU))
            {
                bFiltFound = CO_true;
                break;
            }
        }

        if(!bFiltFound)*/{
            /* Set CAN hardware module filter and mask. */
            CO_CpuInterruptsFlags_t flags = CO_hal_InterruptsSaveDisable();
            if(CANmodule->useCANrxFilters){
                if(CAN_LoadExplicitEntry(CANmodule->hw.CANbaseAddress, ident & 0x07FFU, STD_ID_FORMAT) != CAN_OK){
                    CANmodule->useCANrxFilters = CO_false;
            #ifdef __LPC17XX__
                    CAN_SetAFMode(LPC_CANAF, CAN_AccBP);
            #elif defined(__LPC177X_8X__)
                    CAN_SetAFMode(CAN_ACC_BP);
            #endif
                }
            }
            CO_hal_InterruptsRestore(flags);
        }
    }
    else{
        ret = CO_ERROR_ILLEGAL_ARGUMENT;
    }

    return ret;
}


/******************************************************************************/
CO_CANtx_t *CO_CANtxBufferInit(
        CO_CANmodule_t         *CANmodule,
        uint16_t                index,
        uint16_t                ident,
        CO_bool_t               rtr,
        uint8_t                 noOfBytes,
        CO_bool_t               syncFlag)
{
    CO_CANtx_t *buffer = NULL;

    if((CANmodule != NULL) && (index < CANmodule->txSize)){
        /* get specific buffer */
        buffer = &CANmodule->txArray[index];

        /* CAN identifier, DLC and rtr, bit aligned with CAN module transmit buffer.
         * Microcontroller specific. */
        buffer->ident = ident & 0x07FFU;
        buffer->len = noOfBytes > 8 ? 8 : noOfBytes;
        buffer->type = rtr ? REMOTE_FRAME : DATA_FRAME;
        buffer->bufferFull = CO_false;
        buffer->syncFlag = syncFlag;
    }

    return buffer;
}

#include "board.h"

/******************************************************************************/
CO_ReturnError_t CO_CANsend(CO_CANmodule_t *CANmodule, CO_CANtx_t *buffer){
    CO_ReturnError_t err = CO_ERROR_NO;
    CAN_MSG_Type msg;
    uint32_t stat;
    uint8_t i;
    Status res = ERROR;

    /* Verify overflow */
    if(buffer->bufferFull){
        if(!CANmodule->firstCANtxMessage){
            /* don't set error, if bootup message is still on buffers */
            CO_errorReport((CO_EM_t*)CANmodule->em, CO_EM_CAN_TX_OVERFLOW, CO_EMC_CAN_OVERRUN, buffer->ident);
        }
        err = CO_ERROR_TX_OVERFLOW;
    }

    CO_CpuInterruptsFlags_t flags = CO_hal_InterruptsSaveDisable();
    /* if CAN TX buffer is free, copy message to it */
    stat = CAN_GetCTRLStatus(CANmodule->hw.CANbaseAddress, CANCTRL_STS);
    if(stat & (CAN_SR_TBS1 | CAN_SR_TBS2 | CAN_SR_TBS3)){
        CANmodule->bufferInhibitFlag = buffer->syncFlag;
        /* copy message and txRequest */
        msg.id = buffer->ident;
        msg.len = buffer->len;
        msg.type = buffer->type;
        msg.format = STD_ID_FORMAT;
        for(i = 0; i < msg.len && i < 4; i++)
            msg.dataA[i] = buffer->data[i];
        for(; i < msg.len; i++)
            msg.dataB[i-4] = buffer->data[i];
        res = CAN_SendMsg(CANmodule->hw.CANbaseAddress, &msg);
#if CO_NO_SYNC > 0
        if(res == SUCCESS && buffer->syncFlag){
            /* @AGV: some kind of hack: CAN_SendMsg will use the first free TX buffer,
             * we need to know it to set appropriate sync flag */
            if(stat & CAN_SR_TBS1)
                CANmodule->hw.flags |= LPC17XX_SYNC_TXB1;
            else if(stat & CAN_SR_TBS2)
                CANmodule->hw.flags |= LPC17XX_SYNC_TXB2;
            else if(stat & CAN_SR_TBS3)
                CANmodule->hw.flags |= LPC17XX_SYNC_TXB3;
        }
#endif
    }
    /* if no buffer is free or error, message will be sent by interrupt */
    if(res != SUCCESS){
        buffer->bufferFull = CO_true;
        CANmodule->CANtxCount++;
    }
    CO_hal_InterruptsRestore(flags);

    return err;
}


#if CO_NO_SYNC > 0
/******************************************************************************/
void CO_CANclearPendingSyncPDOs(CO_CANmodule_t *CANmodule){
    uint32_t tpdoDeleted = 0U;

    CO_CpuInterruptsFlags_t flags = CO_hal_InterruptsSaveDisable();
    /* Abort message from CAN module, if there is synchronous TPDO. */
    if(CANmodule->hw.flags & (LPC17XX_SYNC_TXB1 | LPC17XX_SYNC_TXB2 | LPC17XX_SYNC_TXB3) &&
        CANmodule->bufferInhibitFlag){
        /* clear TXREQ */
        if(CANmodule->hw.flags & LPC17XX_SYNC_TXB1){
            CAN_SetCommand(CANmodule->hw.CANbaseAddress, CAN_CMR_AT | CAN_CMR_STB1);
        }
        if(CANmodule->hw.flags & LPC17XX_SYNC_TXB2){
            CAN_SetCommand(CANmodule->hw.CANbaseAddress, CAN_CMR_AT | CAN_CMR_STB2);
        }
        if(CANmodule->hw.flags & LPC17XX_SYNC_TXB3){
            CAN_SetCommand(CANmodule->hw.CANbaseAddress, CAN_CMR_AT | CAN_CMR_STB3);
        }
        CANmodule->hw.flags = 0;
        CANmodule->bufferInhibitFlag = CO_false;
        tpdoDeleted = 1U;
    }
    /* delete also pending synchronous TPDOs in TX buffers */
    if(CANmodule->CANtxCount != 0U){
        uint16_t i;
        CO_CANtx_t *buffer = &CANmodule->txArray[0];
        for(i = CANmodule->txSize; i > 0U; i--){
            if(buffer->bufferFull){
                if(buffer->syncFlag){
                    buffer->bufferFull = CO_false;
                    CANmodule->CANtxCount--;
                    tpdoDeleted = 2U;
                }
            }
            buffer++;
        }
    }
    CO_hal_InterruptsRestore(flags);


    if(tpdoDeleted != 0U){
        CO_errorReport((CO_EM_t*)CANmodule->em, CO_EM_TPDO_OUTSIDE_WINDOW, CO_EMC_COMMUNICATION, tpdoDeleted);
    }
}
#endif

/******************************************************************************/
void CO_CANverifyErrors(CO_CANmodule_t *CANmodule){
    uint16_t rxErrors, txErrors, overflow;
    CO_EM_t* em = (CO_EM_t*)CANmodule->em;
    uint32_t err;
    uint32_t stat;

    stat = CAN_GetCTRLStatus(CANmodule->hw.CANbaseAddress, CANCTRL_GLOBAL_STS);
    rxErrors = (uint8_t)((stat&0xFF) >> 16);
    txErrors = (uint8_t)((stat&0xFF) >> 24);
    if(stat & CAN_GSR_BS) txErrors = 256; /*bus off*/
    overflow = stat & CAN_GSR_DOS ? 1 : 0;

    err = ((uint32_t)txErrors << 16) | ((uint32_t)rxErrors << 8) | overflow;

    if(CANmodule->errOld != err){
        CANmodule->errOld = err;

        if(txErrors >= 256U){                               /* bus off */
            CO_errorReport(em, CO_EM_CAN_TX_BUS_OFF, CO_EMC_BUS_OFF_RECOVERED, err);
        }
        else{                                               /* not bus off */
            CO_errorReset(em, CO_EM_CAN_TX_BUS_OFF, err);

            if((rxErrors >= 96U) || (txErrors >= 96U)){     /* bus warning */
                CO_errorReport(em, CO_EM_CAN_BUS_WARNING, CO_EMC_NO_ERROR, err);
            }

            if(rxErrors >= 128U){                           /* RX bus passive */
                CO_errorReport(em, CO_EM_CAN_RX_BUS_PASSIVE, CO_EMC_CAN_PASSIVE, err);
            }
            else{
                CO_errorReset(em, CO_EM_CAN_RX_BUS_PASSIVE, err);
            }

            if(txErrors >= 128U){                           /* TX bus passive */
                if(!CANmodule->firstCANtxMessage){
                    CO_errorReport(em, CO_EM_CAN_TX_BUS_PASSIVE, CO_EMC_CAN_PASSIVE, err);
                }
            }
            else{
                CO_bool_t isError = CO_isError(em, CO_EM_CAN_TX_BUS_PASSIVE);
                if(isError){
                    CO_errorReset(em, CO_EM_CAN_TX_BUS_PASSIVE, err);
                    CO_errorReset(em, CO_EM_CAN_TX_OVERFLOW, err);
                }
            }

            if((rxErrors < 96U) && (txErrors < 96U)){       /* no error */
                CO_errorReset(em, CO_EM_CAN_BUS_WARNING, err);
            }
        }

        if(overflow != 0U){                                 /* CAN RX bus overflow */
            CO_errorReport(em, CO_EM_CAN_RXB_OVERFLOW, CO_EMC_CAN_OVERRUN, err);
        }
    }
}


/******************************************************************************/
void CO_CANinterrupt(CO_CANmodule_t *CANmodule){

    uint32_t stat = CAN_IntGetStatus(CANmodule->hw.CANbaseAddress);

    /* receive interrupt */
    if(stat & CAN_ICR_RI){
        CAN_MSG_Type msg;
        CO_CANrxMsg_t rcvMsg;      /* pointer to received message in CAN module */
        uint16_t index;             /* index of received message */
        uint32_t rcvMsgIdent;       /* identifier of the received message */
        CO_CANrx_t *buffer = NULL;  /* receive message buffer from CO_CANmodule_t object. */
        CO_bool_t msgMatched = CO_false;
        uint8_t i;

        if(CANmodule->useCANrxFilters){
            /* read filter index before recv buffer will be released */
/*#ifdef __LPC17XX__
            index = CAN_RFS_ID_INDEX(CANmodule->hw.CANbaseAddress->RFS);
#elif defined(__LPC177X_8X__)
            LPC_CAN_TypeDef* pCan = CANmodule->hw.CANbaseAddress == CAN_ID_1 ? LPC_CAN1 : LPC_CAN2;
            index = CAN_RFS_ID_INDEX(pCan->RFS);
#endif*/
        }

        if(CAN_ReceiveMsg(CANmodule->hw.CANbaseAddress, &msg) == SUCCESS)
        {
            rcvMsgIdent = msg.id;
            if(msg.type == REMOTE_FRAME)
                rcvMsgIdent |= 0x0800;
#if 0
            if(CANmodule->useCANrxFilters){
                /* CAN module filters are used. Message with known 11-bit identifier has */
                /* been received */
                //index = 0;  /* get index of the received message here. Or something similar */
                if(index < CANmodule->rxSize){
                    buffer = &CANmodule->rxArray[index];
                    /* verify also RTR */
                    if(((rcvMsgIdent ^ buffer->ident) & buffer->mask) == 0U){
                        msgMatched = CO_true;
                    }
                }
            }
            else
#endif
            {
                /* CAN module filters are not used, message with any standard 11-bit identifier */
                /* has been received. Search rxArray form CANmodule for the same CAN-ID. */
                buffer = &CANmodule->rxArray[0];
                for(index = CANmodule->rxSize; index > 0U; index--){
                    if(((rcvMsgIdent ^ buffer->ident) & buffer->mask) == 0U){
                        msgMatched = CO_true;
                        break;
                    }
                    buffer++;
                }
            }

            /* Call specific function, which will process the message */
            if(msgMatched && (buffer != NULL) && (buffer->pFunct != NULL)){
                rcvMsg.DLC = msg.len;
                for(i = 0; i < msg.len && i < 4; i++)
                    rcvMsg.data[i] = msg.dataA[i];
                for(; i < msg.len; i++)
                    rcvMsg.data[i] = msg.dataB[i-4];
                buffer->pFunct(buffer->object, &rcvMsg);
            }
            /* Clear interrupt flag (no need) */
        }
    }
    /* transmit interrupt */
    else if(stat & (CAN_ICR_TI1 | CAN_ICR_TI2 | CAN_ICR_TI3)){
        /* Clear interrupt flag (no need) */
#if CO_NO_SYNC > 0
        if(stat & CAN_ICR_TI1)
            CANmodule->hw.flags &= ~LPC17XX_SYNC_TXB1;
        else if(stat & CAN_ICR_TI2)
            CANmodule->hw.flags &= ~LPC17XX_SYNC_TXB2;
        else if(stat & CAN_ICR_TI3)
            CANmodule->hw.flags &= ~LPC17XX_SYNC_TXB3;
#endif
        /* First CAN message (bootup) was sent successfully */
        CANmodule->firstCANtxMessage = CO_false;
        /* clear flag from previous message */
        CANmodule->bufferInhibitFlag = CO_false;
        /* Are there any new messages waiting to be send */
        if(CANmodule->CANtxCount > 0U){
            uint16_t i, k;             /* index of transmitting message */
            CAN_MSG_Type msg;

            /* first buffer */
            CO_CANtx_t *buffer = &CANmodule->txArray[0];
            /* search through whole array of pointers to transmit message buffers. */
            for(i = CANmodule->txSize; i > 0U; i--){
                /* if message buffer is full, send it. */
                if(buffer->bufferFull){
                    buffer->bufferFull = CO_false;
                    CANmodule->CANtxCount--;

                    /* Copy message to CAN buffer */
                    CANmodule->bufferInhibitFlag = buffer->syncFlag;

                    msg.id = buffer->ident;
                    msg.len = buffer->len;
                    msg.type = buffer->type;
                    msg.format = STD_ID_FORMAT;
                    for(k = 0; k < msg.len && k < 4; k++)
                        msg.dataA[k] = buffer->data[k];
                    for(; k < msg.len; k++)
                        msg.dataB[k-4] = buffer->data[k];
#if CO_NO_SYNC > 0
                    uint32_t txStat = 0;
                    if(buffer->syncFlag)
                        txStat = CAN_GetCTRLStatus(CANmodule->hw.CANbaseAddress, CANCTRL_STS);
#endif
                    if(CAN_SendMsg(CANmodule->hw.CANbaseAddress, &msg) != SUCCESS)
                    {
                        CO_errorReport((CO_EM_t*)CANmodule->em, CO_EM_CAN_TX_OVERFLOW, CO_EMC_CAN_OVERRUN, buffer->ident);
                    }
#if CO_NO_SYNC > 0
                    else if(buffer->syncFlag){
                        /* @AGV: some kind of hack: CAN_SendMsg will use the first free TX buffer,
                         * we need to know it to set appropriate sync flag */
                        if(txStat & CAN_SR_TBS1)
                            CANmodule->hw.flags |= LPC17XX_SYNC_TXB1;
                        else if(txStat & CAN_SR_TBS2)
                            CANmodule->hw.flags |= LPC17XX_SYNC_TXB2;
                        else if(txStat & CAN_SR_TBS3)
                            CANmodule->hw.flags |= LPC17XX_SYNC_TXB3;
                    }
#endif
                    break;                      /* exit for loop */
                }
                buffer++;
            }/* end of for loop */

            /* Clear counter if no more messages */
            if(i == 0U){
                CANmodule->CANtxCount = 0U;
            }
        }
    }
    else if(stat & CAN_ICR_EPI){

    }
    else if(stat != 0){
        /* some other interrupt reason */
    }
}
