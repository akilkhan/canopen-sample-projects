#
#                               Copyright (c) 2014
#                       PhiRobotics Technologies Pvt Ltd
#               Vedant Commercial Complex, Vartak Nagar, Thane(w),
#                           Maharashtra-400606, India
#
#  For licensing information, see the file 'LICENSE' in the root folder of
#  this software module.
#

CSRC += \
	$(CANOPEN_STACK_PATH)/CANopen.c \
	$(CANOPEN_STACK_PATH)/CO_Emergency.c \
	$(CANOPEN_STACK_PATH)/CO_HBconsumer.c \
	$(CANOPEN_STACK_PATH)/CO_NMT_Heartbeat.c \
	$(CANOPEN_STACK_PATH)/CO_PDO.c \
	$(CANOPEN_STACK_PATH)/CO_SDO.c \
	$(CANOPEN_STACK_PATH)/CO_SDOmaster.c \
	$(CANOPEN_STACK_PATH)/CO_SYNC.c \
	$(CANOPEN_STACK_PATH)/crc16-ccitt.c \
	#
	
CANOPEN_CHIP_EXTRA_INCLUDES =
 	
# CAN chip definitions
ifeq ($(CO_CAN_LPC17XX),1)
CSRC += \
	$(CANOPEN_STACK_PATH)/LPC17XX/CO_driver.c \
	#
CANOPEN_CHIP_EXTRA_INCLUDES += -I$(CANOPEN_STACK_PATH)/LPC17XX
else
ifeq ($(CO_CAN_AVR),1)
CSRC += \
	$(CANOPEN_STACK_PATH)/AVR/CO_driver.c \
	#
CANOPEN_CHIP_EXTRA_INCLUDES += -I$(CANOPEN_STACK_PATH)/AVR
else
ifeq ($(CO_CAN_MCP2515),1)
CSRC += \
	$(CANOPEN_STACK_PATH)/MCP2515/CO_driver.c \
	#
CANOPEN_CHIP_EXTRA_INCLUDES += -I$(CANOPEN_STACK_PATH)/MCP2515
endif
endif
endif

# EEPROM chip definitions
ifeq ($(CO_EEPROM_LPC17XX),1)
CSRC += \
	$(CANOPEN_STACK_PATH)/LPC17XX/CO_eeprom.c \
	#
CANOPEN_CHIP_EXTRA_INCLUDES += -I$(CANOPEN_STACK_PATH)/LPC17XX
else
ifeq ($(CO_EEPROM_AVR),1)
CSRC += \
	$(CANOPEN_STACK_PATH)/AVR/CO_eeprom.c \
	#
CANOPEN_CHIP_EXTRA_INCLUDES += -I$(CANOPEN_STACK_PATH)/AVR
endif
endif

# CPU definitions
ifeq ($(CO_CPU_AVR),1)
CSRC += \
	$(CANOPEN_STACK_PATH)/AVR/CO_cpu.c \
	#
CANOPEN_CHIP_EXTRA_INCLUDES += -I$(CANOPEN_STACK_PATH)/AVR
else
ifeq ($(CO_CPU_LPC17XX),1)
CSRC += \
	$(CANOPEN_STACK_PATH)/LPC17XX/CO_cpu.c \
	#
CANOPEN_CHIP_EXTRA_INCLUDES += -I$(CANOPEN_STACK_PATH)/LPC17XX
endif
endif

ASRC += \
	#

CPPASRC += \
	#

INCLUDES += -I$(CANOPEN_STACK_PATH) $(CANOPEN_CHIP_EXTRA_INCLUDES)

CPPFLAGS += $(INCLUDES)
