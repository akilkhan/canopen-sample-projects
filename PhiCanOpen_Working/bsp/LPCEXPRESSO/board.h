/*
 * board.h
 *
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "CO_driver.h"
#include "debug_frmwrk.h"
#include "lpc17xx_can.h"

#define BOARD_USE_SYSTICK       0

#if BOARD_USE_SYSTICK == 1
#include "lpc17xx_systick.h"
#else
#include "lpc17xx_timer.h"
#endif

#define USED_BOARD_DEBUG_UART   3

#if (USED_BOARD_DEBUG_UART==0)
#define BOARD_DEBUG_UART LPC_UART0
#elif (USED_BOARD_DEBUG_UART==1)
#define BOARD_DEBUG_UART LPC_UART1
#elif (USED_BOARD_DEBUG_UART==2)
#define BOARD_DEBUG_UART LPC_UART2
#elif (USED_BOARD_DEBUG_UART==3)
#define BOARD_DEBUG_UART LPC_UART3
#endif

#define DEBUGS(x)   UARTPuts(BOARD_DEBUG_UART, x)
#define DEBUGC(x)   UARTPutChar(BOARD_DEBUG_UART, x)
#define DEBUGD(x)   UARTPutDec(BOARD_DEBUG_UART, x)
#define DEBUGD16(x) UARTPutDec16(BOARD_DEBUG_UART, x)
#define DEBUGD32(x) UARTPutDec32(BOARD_DEBUG_UART, x)
#define DEBUGH(x)   UARTPutHex(BOARD_DEBUG_UART, x)
#define DEBUGH16(x) UARTPutHex16(BOARD_DEBUG_UART, x)
#define DEBUGH32(x) UARTPutHex32(BOARD_DEBUG_UART, x)
#define GETCHAR()   board_get_char()

int board_init(CO_CANmoduleHwConfig_t *canHwCfg);
#define board_reset()       NVIC_SystemReset()
#if BOARD_USE_SYSTICK == 1
#define timer_enableIrq()   SYSTICK_IntCmd(ENABLE)
#define timer_disableIrq()  SYSTICK_IntCmd(DISABLE)
#else
#define timer_enableIrq()   NVIC_EnableIRQ(TIMER0_IRQn)
#define timer_disableIrq()  NVIC_DisableIRQ(TIMER0_IRQn)
#endif
#define can_enableIrq()     NVIC_EnableIRQ(CAN_IRQn)
#define can_disableIrq()    NVIC_DisableIRQ(CAN_IRQn)
void leds_write(uint8_t ledsBits);
uint8_t buttons_read(void);
uint8_t board_get_char(void);
void board_put_char(uint8_t c);

#endif /* BOARD_H_ */
