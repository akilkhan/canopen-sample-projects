/*
 * board.c
 */
#include <stdlib.h>
#include "board.h"
#include "CO_hal.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_clkpwr.h"
#include "lpc17xx_gpio.h"

#define BV(_n_) (1 << (_n_))

#define BOARD_LED0_PORT 4
#define BOARD_LED0_PIN  29
#define BOARD_LED1_PORT 0
#define BOARD_LED1_PIN  26
#define BOARD_LED2_PORT 0
#define BOARD_LED2_PIN  25
#define BOARD_BTN0_PORT 0
#define BOARD_BTN0_PIN  24
#define BOARD_BTN1_PORT 0
#define BOARD_BTN1_PIN  23
#define BOARD_BTN2_PORT 1
#define BOARD_BTN2_PIN  31

extern void app_timerIrqHandler(void);
extern void app_canIrqHandler(void);

void _exit(int status);
int _kill(int pid, int sig);
int _getpid(void);
void * _sbrk(int incr);
#if BOARD_USE_SYSTICK == 1
void SysTick_Handler(void);
#else
void TIMER0_IRQHandler(void);
#endif
void CAN_IRQHandler(void);

static void gpiopin_init(uint8_t portNum, uint8_t pinNum, uint8_t dir, uint8_t val)
{
    PINSEL_CFG_Type PinCfg;

    PinCfg.Funcnum = 0;
    PinCfg.OpenDrain = 0;
    PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    PinCfg.Pinnum = pinNum;
    PinCfg.Portnum = portNum;
    PINSEL_ConfigPin(&PinCfg);

    FIO_SetDir(portNum, BV(pinNum), dir);
    if(val)
        FIO_SetValue(portNum, BV(pinNum));
    else
        FIO_ClearValue(portNum, BV(pinNum));
}

int board_init(CO_CANmoduleHwConfig_t *canHwCfg)
{
    UART_CFG_Type UARTConfigStruct;
    TIM_TIMERCFG_Type TIMConfigStruct;
    TIM_MATCHCFG_Type TIM_MatchConfigStruct;
    PINSEL_CFG_Type PinCfg;

    WDT_Init(WDT_CLKSRC_PCLK, WDT_MODE_RESET);

    canHwCfg->uBase = LPC_CAN2;

#if (USED_BOARD_DEBUG_UART == 0)
    /*
     * Initialize UART0 pin connect
     */
    PinCfg.Funcnum = 1;
    PinCfg.OpenDrain = 0;
    PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    PinCfg.Pinnum = 2;
    PinCfg.Portnum = 0;
    PINSEL_ConfigPin(&PinCfg);
    PinCfg.Pinnum = 3;
    PINSEL_ConfigPin(&PinCfg);
#else
#error USED_BOARD_DEBUG_UART not implemented!
#endif

    /* Initialize UART Configuration parameter structure to default state:
     * Baudrate = 9600bps
     * 8 data bit
     * 1 Stop bit
     * None parity
     */
    UART_ConfigStructInit(&UARTConfigStruct);
    // Re-configure baudrate to 115200bps
    UARTConfigStruct.Baud_rate = 115200;
    // Initialize DEBUG_UART_PORT peripheral with given to corresponding parameter
    UART_Init(BOARD_DEBUG_UART, &UARTConfigStruct);
    // Enable UART Transmit
    UART_TxCmd(BOARD_DEBUG_UART, ENABLE);

    /* init 1 ms timer */
#if BOARD_USE_SYSTICK == 1
    SYSTICK_InternalInit(1);
    SYSTICK_IntCmd(DISABLE);
    SYSTICK_Cmd(ENABLE);
#else
    TIM_ConfigStructInit(TIM_TIMER_MODE, &TIMConfigStruct);
    TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &TIMConfigStruct);
    TIM_MatchConfigStruct.MatchChannel = 0;
    TIM_MatchConfigStruct.IntOnMatch = ENABLE;
    TIM_MatchConfigStruct.StopOnMatch = DISABLE;
    TIM_MatchConfigStruct.ResetOnMatch = ENABLE;
    TIM_MatchConfigStruct.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
    TIM_MatchConfigStruct.MatchValue = CLKPWR_GetPCLK(CLKPWR_PCLKSEL_TIMER0)/1000;	/* 1 ms delay */
    TIM_ConfigMatch(LPC_TIM0, &TIM_MatchConfigStruct);
    TIM_Cmd(LPC_TIM0, ENABLE);
#endif

    CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCAN1, ENABLE);
    CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCAN2, ENABLE);
    /*
     * Initialize CAN2 pin connect
     */
    PinCfg.Funcnum = 2;
    PinCfg.OpenDrain = 0;
    PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    PinCfg.Pinnum = 4;
    PinCfg.Portnum = 0;
    PINSEL_ConfigPin(&PinCfg);
    PinCfg.Pinnum = 5;
    PINSEL_ConfigPin(&PinCfg);

    CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCGPIO, ENABLE);

    //Initialize other LED diodes for RPDO
    gpiopin_init(BOARD_LED0_PORT, BOARD_LED0_PIN, 1, 0);
    gpiopin_init(BOARD_LED1_PORT, BOARD_LED1_PIN, 1, 0);
    gpiopin_init(BOARD_LED2_PORT, BOARD_LED2_PIN, 1, 0);

    //Initialize buttons for TPDO
    gpiopin_init(BOARD_BTN0_PORT, BOARD_BTN0_PIN, 0, 1);
    gpiopin_init(BOARD_BTN1_PORT, BOARD_BTN1_PIN, 0, 1);
    gpiopin_init(BOARD_BTN2_PORT, BOARD_BTN2_PIN, 0, 1);

#if BOARD_USE_SYSTICK == 0
    /* CAN should have higher prio then 1ms timer */
    NVIC_SetPriority(TIMER0_IRQn, 10);
    NVIC_SetPriority(CAN_IRQn, 5);
    //NVIC_EnableIRQ(TIMER0_IRQn);
#endif
    NVIC_EnableIRQ(CAN_IRQn);

    return 0;
}

void leds_write(uint8_t ledsBits)
{
    if(ledsBits & 0x1)
        FIO_SetValue(BOARD_LED0_PORT, BV(BOARD_LED0_PIN));
    else
        FIO_ClearValue(BOARD_LED0_PORT, BV(BOARD_LED0_PIN));
    if(ledsBits & 0x2)
        FIO_SetValue(BOARD_LED1_PORT, BV(BOARD_LED1_PIN));
    else
        FIO_ClearValue(BOARD_LED1_PORT, BV(BOARD_LED1_PIN));
    if(ledsBits & 0x4)
        FIO_SetValue(BOARD_LED2_PORT, BV(BOARD_LED2_PIN));
    else
        FIO_ClearValue(BOARD_LED2_PORT, BV(BOARD_LED2_PIN));
}

uint8_t leds_read(void)
{
    uint8_t bits = 0;

    uint32_t val = FIO_ReadValue(BOARD_LED0_PORT);
    if(val & BV(BOARD_LED0_PIN))
        bits |= 0x1;
    val = FIO_ReadValue(BOARD_LED1_PORT);
    if(val & BV(BOARD_LED1_PIN))
        bits |= 0x2;
    val = FIO_ReadValue(BOARD_LED2_PORT);
    if(val & BV(BOARD_LED2_PIN))
        bits |= 0x4;

    return bits;
}

uint8_t buttons_read(void)
{
    uint8_t bits = 0;

    uint32_t val = FIO_ReadValue(BOARD_BTN0_PORT);
    if(val & BV(BOARD_BTN0_PIN))
        bits |= 0x1;
    val = FIO_ReadValue(BOARD_BTN1_PORT);
    if(val & BV(BOARD_BTN1_PIN))
        bits |= 0x2;
    val = FIO_ReadValue(BOARD_BTN2_PORT);
    if(val & BV(BOARD_BTN2_PIN))
        bits |= 0x4;

    return bits;
}

uint8_t board_get_char(void)
{
    uint8_t tmp = 0;
    if(UART_Receive(BOARD_DEBUG_UART, &tmp, 1, NONE_BLOCKING) == 0)
        return 0xFF;
    return(tmp);
}

void board_put_char(uint8_t c)
{
    UART_Send(BOARD_DEBUG_UART, &c, 1, BLOCKING);//NONE_BLOCKING);
}

#if BOARD_USE_SYSTICK == 1
void SysTick_Handler(void)
{
    //clear interrupt flag bit
    SYSTICK_ClearCounterFlag();
    app_timerIrqHandler();
}
#else
void TIMER0_IRQHandler(void)
{
    //clear interrupt flag bit
    TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);
    app_timerIrqHandler();
}
#endif

void CAN_IRQHandler(void)
{
    app_canIrqHandler();
}

void _exit(int status)
{
    (void)status;
    while(1);
}

int _kill(int pid, int sig)
{
    (void)pid;
    (void)sig;
    return 0;
}

int _getpid(void)
{
    return 0;
}

extern unsigned int  __heap_start;

void * _sbrk(int incr)
{
  static unsigned char *heap = NULL;
  unsigned char *prev_heap;

  if (heap == NULL) {
    heap = (unsigned char *)&__heap_start;
  }
  prev_heap = heap;
  /* check removed to show basic approach */

  heap += incr;

  return (void *)prev_heap;
}
