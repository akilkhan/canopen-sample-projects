/*
 * board.h
 *
 */

#ifndef BOARD_H_
#define BOARD_H_

//#include "stdint.h"
#include "CO_driver.h"
#include <avr/io.h>
#include <avr/wdt.h>

#define BOARD_USE_EEPROM    1

#define USED_BOARD_DEBUG_UART   0

#if (USED_BOARD_DEBUG_UART==0)
#define BOARD_DEBUG_UART 0//LPC_UART0
#elif (USED_BOARD_DEBUG_UART==1)
#define BOARD_DEBUG_UART 1//LPC_UART1
#elif (USED_BOARD_DEBUG_UART==2)
#define BOARD_DEBUG_UART 2//LPC_UART2
#elif (USED_BOARD_DEBUG_UART==3)
#define BOARD_DEBUG_UART 3//LPC_UART3
#endif

#define DEBUGS(x)   board_puts(x)
/*
#define DEBUGC(x)   do{}while(0) //UARTPutChar(BOARD_DEBUG_UART, x)
#define DEBUGD(x)   do{}while(0) //UARTPutDec(BOARD_DEBUG_UART, x)
#define DEBUGD16(x) do{}while(0) //UARTPutDec16(BOARD_DEBUG_UART, x)
#define DEBUGD32(x) do{}while(0) //UARTPutDec32(BOARD_DEBUG_UART, x)
#define DEBUGH(x)   do{}while(0) //UARTPutHex(BOARD_DEBUG_UART, x)
#define DEBUGH16(x) do{}while(0) //UARTPutHex16(BOARD_DEBUG_UART, x)
#define DEBUGH32(x) do{}while(0) //UARTPutHex32(BOARD_DEBUG_UART, x)
*/
#define GETCHAR()   board_get_char()

#ifndef BV
#define BV(_n_) (1 << (_n_))
#endif

int board_init(CO_CANmoduleHwConfig_t *canHwCfg);
#define board_reset()       do{ wdt_enable(WDTO_15MS); while(1); }while(0)
#define timer_enableIrq()   do{ TIMSK0 |= BV(OCIE0A); }while(0)
#define timer_disableIrq()  do{ TIMSK0 &= ~BV(OCIE0A); }while(0)
#define can_enableIrq()     do{ PCMSK0 |= BV(PCINT5); }while(0)
#define can_disableIrq()    do{ PCMSK0 &= ~BV(PCINT5); }while(0)
void leds_write(uint8_t ledsBits);
uint8_t leds_read(void);
uint8_t buttons_read(void);
uint8_t board_get_char(void);
void board_put_char(uint8_t c);
void board_puts(const char* s);

#endif /* BOARD_H_ */
