/*
 * board.c
 */
#include <string.h>
#include <avr/interrupt.h>
#include "CO_hal.h"
#include "board.h"

#define BOARD_LED0_PORT ('D'-'A')
#define BOARD_LED0_PIN  4
#define BOARD_LED1_PORT ('D'-'A')
#define BOARD_LED1_PIN  5
#define BOARD_LED2_PORT ('D'-'A')
#define BOARD_LED2_PIN  6
#define BOARD_LED3_PORT ('D'-'A')
#define BOARD_LED3_PIN  7
#define BOARD_BTN0_PORT ('K'-'A' - 1)
#define BOARD_BTN0_PIN  4
#define BOARD_BTN1_PORT ('K'-'A' - 1)
#define BOARD_BTN1_PIN  5
#define BOARD_BTN2_PORT ('K'-'A' - 1)
#define BOARD_BTN2_PIN  6
#define BOARD_BTN3_PORT ('K'-'A' - 1)
#define BOARD_BTN3_PIN  7
#define BOARD_MCP2515_GPIO_PORT ('J'-'A' - 1)
#define BOARD_MCP2515_GPIO_PIN  7

#define BOARD_MCP2515_FREQ      8000000UL
#define BOARD_MCP2515_SPI_FREQ  3686400UL//1843200UL //921600UL //115200UL

#define BOARD_TICK_FREQ         1000UL

extern void app_timerIrqHandler(void);
extern void app_canIrqHandler(void);

int board_init(CO_CANmoduleHwConfig_t *canHwCfg)
{
    uint16_t ubrr = CPU_F / 16 / 115200 - 1;

    cli();

    wdt_reset();
    MCUSR &= ~(1<<WDRF);
    wdt_disable();

    canHwCfg->fosc = BOARD_MCP2515_FREQ;
    canHwCfg->ssGpioDevId = BOARD_MCP2515_GPIO_PORT;
    canHwCfg->ssGpioPin = BOARD_MCP2515_GPIO_PIN;
    canHwCfg->spiDevId = 0;

#if (USED_BOARD_DEBUG_UART == 0)
    /*
     * Initialize USART0 pin connect
     */
    //DDRE &= ~BV(DDE0);
    CO_hal_GpioDevicePinInit(4, 0, 0, 1);
    //DDRE |= BV(DDE1);
    CO_hal_GpioDevicePinInit(4, 1, 1, 1);
    /* Set baud rate */
    UBRR0H = (uint8_t)(ubrr>>8);
    UBRR0L = (uint8_t)ubrr;
    /* Set frame format: 8data, 1stop bit */
    UCSR0C = BV(UCSZ01) | BV(UCSZ00);
    /* Enable receiver and transmitter */
    UCSR0B = BV(RXEN0) | BV(TXEN0);
#else
#error USED_BOARD_DEBUG_UART not implemented!
#endif

    /* init 1 ms timer */
    TCCR0A = TCCR0B = TIMSK0 = 0;
    TIFR0 = BV(OCF0B) | BV(OCF0A) | BV(TOV0);
    TCCR0A = BV(WGM01);
    if((CPU_F/1)/BOARD_TICK_FREQ <= (1UL << 8))
    {
        TCCR0B = BV(CS00);
        OCR0A = (uint8_t)((CPU_F/1)/BOARD_TICK_FREQ);
    }
    else if((CPU_F/8)/BOARD_TICK_FREQ <= (1UL << 8))
    {
        TCCR0B = BV(CS01);
        OCR0A = (uint8_t)((CPU_F/8)/BOARD_TICK_FREQ);
    }
    else if((CPU_F/64)/BOARD_TICK_FREQ <= (1UL << 8))
    {
        TCCR0B = BV(CS01) | BV(CS00);
        OCR0A = (uint8_t)((CPU_F/64)/BOARD_TICK_FREQ);
    }
    else if((CPU_F/256)/BOARD_TICK_FREQ <= (1UL << 8))
    {
        TCCR0B = BV(CS02);
        OCR0A = (uint8_t)((CPU_F/256)/BOARD_TICK_FREQ);
    }
    else if((CPU_F/1024)/BOARD_TICK_FREQ <= (1UL << 8))
    {
        TCCR0B = BV(CS02) | BV(CS00);
        OCR0A = (uint8_t)((CPU_F/1024)/BOARD_TICK_FREQ);
    }
    else
        return -1;

    TCNT0 = 0;
    //TIMSK0 = BV(OCIE0A);

    /* initialize MCP2515 interrupt pin */
    //DDRB &= ~BV(DDB5);
    CO_hal_GpioDevicePinInit(1, 5, 0, 1);
    PCMSK0 |= BV(PCINT5);
    PCICR = BV(PCIE0);

    /* init MCP2515 SPI */
    //DDRB |= BV(DDB2);  /*MOSI*/
    CO_hal_GpioDevicePinInit(1, 2, 1, 1);
    //DDRB |= BV(DDB1);  /*SCK*/
    CO_hal_GpioDevicePinInit(1, 1, 1, 1);
    //DDRB &= ~BV(DDB3); /*MISO*/
    CO_hal_GpioDevicePinInit(1, 3, 0, 1);
    CO_hal_GpioDevicePinInit(1, 0, 1, 1); /*SS*/
    CO_hal_GpioDevicePinInit(BOARD_MCP2515_GPIO_PORT, BOARD_MCP2515_GPIO_PIN, 1, 1); /*MCP2515 CS*/

    SPCR = 0; SPSR = 0;
    if(BOARD_MCP2515_SPI_FREQ >= CPU_F/2)
    {
        SPSR |= BV(SPI2X);
    }
    else if(BOARD_MCP2515_SPI_FREQ >= CPU_F/4)
    {}
    else if(BOARD_MCP2515_SPI_FREQ >= CPU_F/8)
    {
        SPCR |= BV(SPR0);
        SPSR |= BV(SPI2X);
    }
    else if(BOARD_MCP2515_SPI_FREQ >= CPU_F/16)
    {
        SPCR |= BV(SPR0);
    }
    else if(BOARD_MCP2515_SPI_FREQ >= CPU_F/32)
    {
        SPCR |= BV(SPR1);
        SPSR |= BV(SPI2X);
    }
    else if(BOARD_MCP2515_SPI_FREQ >= CPU_F/64)
    {
        SPCR |= BV(SPR1) | BV(SPR0);
        SPSR |= BV(SPI2X);
    }
    else if(BOARD_MCP2515_SPI_FREQ >= CPU_F/128)
    {
        SPCR |= BV(SPR1) | BV(SPR0);
    }
    else
        return -1;

    SPCR |= BV(MSTR);
    SPCR |= BV(SPE);

    //Initialize other LED diodes for RPDO
    CO_hal_GpioDevicePinInit(BOARD_LED0_PORT, BOARD_LED0_PIN, 1, 1);
    CO_hal_GpioDevicePinInit(BOARD_LED1_PORT, BOARD_LED1_PIN, 1, 1);
    CO_hal_GpioDevicePinInit(BOARD_LED2_PORT, BOARD_LED2_PIN, 1, 1);
    CO_hal_GpioDevicePinInit(BOARD_LED3_PORT, BOARD_LED3_PIN, 1, 1);

    //Initialize buttons for TPDO
    CO_hal_GpioDevicePinInit(BOARD_BTN0_PORT, BOARD_BTN0_PIN, 0, 1);
    CO_hal_GpioDevicePinInit(BOARD_BTN1_PORT, BOARD_BTN1_PIN, 0, 1);
    CO_hal_GpioDevicePinInit(BOARD_BTN2_PORT, BOARD_BTN2_PIN, 0, 1);
    CO_hal_GpioDevicePinInit(BOARD_BTN3_PORT, BOARD_BTN3_PIN, 0, 1);

    sei();

    return 0;
}

void leds_write(uint8_t ledsBits)
{
    if(ledsBits & 0x1)
        CO_hal_GpioDevicePinStateSet(BOARD_LED0_PORT, BOARD_LED0_PIN, 1);
    else
        CO_hal_GpioDevicePinStateSet(BOARD_LED0_PORT, BOARD_LED0_PIN, 0);
    if(ledsBits & 0x2)
        CO_hal_GpioDevicePinStateSet(BOARD_LED1_PORT, BOARD_LED1_PIN, 1);
    else
        CO_hal_GpioDevicePinStateSet(BOARD_LED1_PORT, BOARD_LED1_PIN, 0);
    if(ledsBits & 0x4)
        CO_hal_GpioDevicePinStateSet(BOARD_LED2_PORT, BOARD_LED2_PIN, 1);
    else
        CO_hal_GpioDevicePinStateSet(BOARD_LED2_PORT, BOARD_LED2_PIN, 0);
    if(ledsBits & 0x8)
        CO_hal_GpioDevicePinStateSet(BOARD_LED3_PORT, BOARD_LED3_PIN, 1);
    else
        CO_hal_GpioDevicePinStateSet(BOARD_LED3_PORT, BOARD_LED3_PIN, 0);
}

uint8_t leds_read(void)
{
    uint8_t bits = 0;

    if(CO_hal_GpioDevicePinStateGet(BOARD_LED0_PORT, BOARD_LED0_PIN))
        bits |= 0x1;
    if(CO_hal_GpioDevicePinStateGet(BOARD_LED1_PORT, BOARD_LED1_PIN))
        bits |= 0x2;
    if(CO_hal_GpioDevicePinStateGet(BOARD_LED2_PORT, BOARD_LED2_PIN))
        bits |= 0x4;
    if(CO_hal_GpioDevicePinStateGet(BOARD_LED3_PORT, BOARD_LED3_PIN))
        bits |= 0x8;

    return bits;
}

uint8_t buttons_read(void)
{
    uint8_t bits = 0;

    if(CO_hal_GpioDevicePinStateGet(BOARD_BTN0_PORT, BOARD_BTN0_PIN))
        bits |= 0x1;
    if(CO_hal_GpioDevicePinStateGet(BOARD_BTN1_PORT, BOARD_BTN1_PIN))
        bits |= 0x2;
    if(CO_hal_GpioDevicePinStateGet(BOARD_BTN2_PORT, BOARD_BTN2_PIN))
        bits |= 0x4;
    if(CO_hal_GpioDevicePinStateGet(BOARD_BTN3_PORT, BOARD_BTN3_PIN))
        bits |= 0x8;

    return bits;
}

uint8_t board_get_char(void)
{
#if (USED_BOARD_DEBUG_UART == 0)
    if( !(UCSR0A & BV(RXC0)) )
        return 0xFF;
    return(UDR0);
#elif (USED_BOARD_DEBUG_UART == 1)
    if( !(UCSR1A & BV(RXC1)) )
        return 0xFF;
    return(UDR1);
#endif
}

void board_put_char(uint8_t c)
{
#if (USED_BOARD_DEBUG_UART == 0)
    while( !( UCSR0A & (1<<UDRE0)) );
    /* Put data into buffer, sends the data */
    UDR0 = c;
#elif (USED_BOARD_DEBUG_UART == 1)
    while( !( UCSR1A & (1<<UDRE1)) );
    /* Put data into buffer, sends the data */
    UDR1 = c;
#endif
}

void board_puts(const char* s)
{
    uint32_t len = strlen(s), i;
    for(i = 0; i < len; i++)
        board_put_char(s[i]);
}

ISR(TIMER0_COMPA_vect)
{
    if(CO_hal_GpioDevicePinStateGet(1, 5) == 0)
    {
        CO_CpuInterruptsFlags_t flags = CO_hal_InterruptsSaveDisable();
        app_canIrqHandler();
        CO_hal_InterruptsRestore(flags);
    }
    app_timerIrqHandler();
}

ISR(PCINT0_vect)
{
    app_canIrqHandler();
}
