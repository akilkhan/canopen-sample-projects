#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CANopen.h"
#include "board.h"
#include "application.h"

#ifdef BOARD_USE_EEPROM
    #include "prime_eeprom.h"
#endif

#define EXAMIO_DBGMSG_MAX_LEN  64

#define EXAMIO_DEBUG(...) \
{ \
    sprintf(gs_dbgMsg, __VA_ARGS__); \
    DEBUGS(gs_dbgMsg); \
}
#define EXAMIO_ERROR(...)   EXAMIO_DEBUG("ERROR: " __VA_ARGS__)
#define EXAMIO_PANIC(...)   {EXAMIO_DEBUG("PANIC: " __VA_ARGS__); while(1);}
//#define EXAMIO_DEBUG        printf
//#define EXAMIO_ERROR        printf
//#define EXAMIO_PANIC(...)   {printf("PANIC: " __VA_ARGS__); while(1);}

void app_timerIrqHandler(void);
void app_canIrqHandler(void);

//static uint8_t CO_memPool[CO_MEM_POOL_SIZE];
static volatile UNSIGNED16 CO_timer1ms;
static char gs_dbgMsg[EXAMIO_DBGMSG_MAX_LEN];
#ifdef BOARD_USE_EEPROM
static CO_EE_t CO_EEO;         /* Eeprom object */
#endif

/* main ***********************************************************************/
int main (void)
{
    CO_NMT_reset_cmd_t reset = CO_RESET_NOT;
    CO_CANmoduleHwConfig_t canHwCfg;

    if(board_init(&canHwCfg) != 0)
        while(1);

    EXAMIO_DEBUG("Start CANopen I/O example...\n");

#if 0
    CAN_Init(BOARD_ADDR_CAN, 250000);
#if 1//def __LPC17XX__
    CAN_SetAFMode(LPC_CANAF, CAN_AccBP);
#else
    CAN_SetAFMode(CAN_ACC_BP);
#endif
    //CAN_ModeConfig(BOARD_ADDR_CAN, CAN_SELFTEST_MODE, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_RIE, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_TIE1, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_TIE2, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_TIE3, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_EIE, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_DOIE, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_EPIE, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_ALIE, ENABLE);
    CAN_IRQCmd(BOARD_ADDR_CAN, CANINT_BEIE, ENABLE);
    NVIC_EnableIRQ(CAN_IRQn);

    while(1)
    {
#if __LPC17XX__
        if(board_get_char() == 's')
        {
            CAN_MSG_Type msg;
            msg.id = 0xc;
            msg.len = 8;
            msg.type = DATA_FRAME;//REMOTE_FRAME;
            msg.format = STD_ID_FORMAT;
            EXAMIO_DEBUG("Send message\n");
            if(CAN_SendMsg(BOARD_ADDR_CAN, &msg) != SUCCESS)
            {
                EXAMIO_ERROR("Failed to send message!\n");
            }
        }
#endif
    }
#endif

    //Verify, if OD structures have proper alignment of initial values
    if(CO_OD_RAM.FirstWord != CO_OD_RAM.LastWord)
        EXAMIO_PANIC("Invalid CO_OD_RAM alignment!\n");
    if(CO_OD_EEPROM.FirstWord != CO_OD_EEPROM.LastWord)
        EXAMIO_PANIC("Invalid CO_OD_EEPROM alignment!\n");
    if(CO_ROM_READ32(CO_OD_ROM.FirstWord) != CO_ROM_READ32(CO_OD_ROM.LastWord))
        EXAMIO_PANIC("Invalid CO_OD_ROM alignment!\n");

    /* initialize EEPROM - part 1 */
#ifdef BOARD_USE_EEPROM
    CO_ReturnError_t eeStatus = CO_EE_init_1(&CO_EEO, (uint8_t*) &CO_OD_EEPROM, sizeof(CO_OD_EEPROM),
                            (uint8_t*) &CO_OD_ROM, sizeof(CO_OD_ROM));
    if(eeStatus != CO_ERROR_NO){
        EXAMIO_ERROR("EEPROM initialization failure!\n");
    }
#endif

    programStart();

    //increase variable each startup. Variable is stored in eeprom.
    OD_powerOnCounter++;
	
    while(reset != CO_RESET_APP)
    {
        /* CANopen communication reset - initialize CANopen objects */
        CO_ReturnError_t err;
        UNSIGNED16 timer1msPrevious;
        //UNSIGNED16 TMR_TMR_PREV = 0;

        //disable timer and CAN interrupts
        timer_disableIrq();
        can_disableIrq();

        //initialize CANopen
        err = CO_init(/*CO_memPool,*/ &canHwCfg);
        if(err != CO_ERROR_NO)
        {
            EXAMIO_PANIC("Failed to init CANopen stack!\n");
            //CO_errorReport(CO->EM, ERROR_MEMORY_ALLOCATION_ERROR, err);
        }
		
        /* initialize eeprom - part 2 */
#ifdef BOARD_USE_EEPROM
        CO_EE_init_2(&CO_EEO, eeStatus, CO->SDO, CO->em);
#endif

       //initialize variables
       timer1msPrevious = CO_timer1ms;
       OD_performance[ODA_performance_mainCycleMaxTime] = 0;
       OD_performance[ODA_performance_timerCycleMaxTime] = 0;
       reset = CO_RESET_NOT;

       communicationReset();
	
       //start CAN and enable interrupts
       //CO_CANsetNormalMode(BOARD_ADDR_CAN);
       timer_enableIrq();
       can_enableIrq();

       while(reset == CO_RESET_NOT)
       {
          /* loop for normal program execution */
          UNSIGNED16 timer1msCopy, timer1msDiff;

          //calculate cycle time for performance measurement
          timer1msCopy = CO_timer1ms;
          timer1msDiff = timer1msCopy - timer1msPrevious;
          timer1msPrevious = timer1msCopy;
          /*UNSIGNED16 t0 = CO_TMR_TMR;
          UNSIGNED16 t = t0;
          if(t >= TMR_TMR_PREV){
             t = t - TMR_TMR_PREV;
             //t = (timer1msDiff * 100) + (t / (CO_PBCLK / 100));
          }
          else if(timer1msDiff){
             t = TMR_TMR_PREV - t;
             //t = (timer1msDiff * 100) - (t / (CO_PBCLK / 100));
          }
          else t = 0;
          OD_performance[ODA_performance_mainCycleTime] = t;
          if(t > OD_performance[ODA_performance_mainCycleMaxTime])
             OD_performance[ODA_performance_mainCycleMaxTime] = t;
          TMR_TMR_PREV = t0;*/

          //Application asynchronous program
          programAsync(timer1msDiff);
	
          //CANopen process
          reset = CO_process(CO, timer1msDiff);
	
		  

#ifdef BOARD_USE_EEPROM
          CO_EE_process(&CO_EEO);
#endif
       }
       //disable timer and CAN interrupts
       timer_disableIrq();
       can_disableIrq();

       CO_delete();
    }

    /* program exit */
    programEnd();

    EXAMIO_DEBUG("Reset node!\n");
    board_reset();

   return 0;
}

/* timer interrupt function executes every millisecond ************************/
void app_timerIrqHandler()
{
	CO_timer1ms++;

   CO_process_RPDO(CO);

   program1ms();

   CO_process_TPDO(CO);

   //verify timer overflow
   /*if(CO_TMR_ISR_FLAG == 1){
      CO_errorReport(CO->EM, ERROR_ISR_TIMER_OVERFLOW, 0);
      CO_TMR_ISR_FLAG = 0;
   }*/

   //calculate cycle time for performance measurement
   /*UNSIGNED16 t = CO_TMR_TMR / (CO_FCY / 100);
   OD_performance[ODA_performance_timerCycleTime] = t;
   if(t > OD_performance[ODA_performance_timerCycleMaxTime])
      OD_performance[ODA_performance_timerCycleMaxTime] = t;*/
}


/* CAN interrupt function *****************************************************/
void app_canIrqHandler()
{
	EXAMIO_DEBUG("CAN IRQ!\n");
#if 1
   CO_CANinterrupt(CO->CANmodule[0]);
#else
    CAN_MSG_Type msg;
    UNSIGNED32 stat = CAN_IntGetStatus(BOARD_ADDR_CAN);

    //receive interrupt (New CAN messagge is available in RX buffer)
    if(stat & CAN_ICR_RI)
    {
        EXAMIO_DEBUG("Receive CAN message\n");
        if(CAN_ReceiveMsg(BOARD_ADDR_CAN, &msg) != SUCCESS)
        {
            EXAMIO_ERROR("Failed to receive CAN message!\n");
        }
    }
    else if(stat & (CAN_ICR_TI1 | CAN_ICR_TI2 | CAN_ICR_TI3))
    {
        EXAMIO_DEBUG("message xmited\n");
    }
    else if(stat & CAN_ICR_EI)
    {
        EXAMIO_ERROR("ERR_WARN!\n");
    }
    else if(stat & CAN_ICR_EI)
    {
        EXAMIO_ERROR("EI!\n");
    }
    else if(stat & CAN_ICR_DOI)
    {
        EXAMIO_ERROR("DOI!\n");
    }
    else if(stat & CAN_ICR_EPI)
    {
        EXAMIO_ERROR("EPI!\n");
    }
    else if(stat & CAN_ICR_ALI)
    {
        EXAMIO_ERROR("ALI!\n");
    }
    else if(stat & CAN_ICR_BEI)
    {
        EXAMIO_ERROR("BEI (%x %d %x)!\n", CAN_ICR_ERRBIT(stat), stat & CAN_ICR_ERRDIR, CAN_ICR_ERRC(stat));
    }
#endif
}

#ifdef OD_testVar
/*
 * Function - ODF_testDomain
 *
 * Function for accessing _test var_ (index 0x2120) from SDO server.
 *
 * For more information see topic <Object dictionary function>.
 */
#define ODF_testDomain_index     0x2120
CO_SDO_abortCode_t ODF_testDomain(CO_ODF_arg_t *ODF_arg);
#endif

/******************************************************************************/
void programStart(void)
{
}


/******************************************************************************/
void communicationReset(void)
{
   //CAN_RUN_LED = 0; CAN_ERROR_LED = 0;
   OD_writeOutput8Bit[0] = leds_read();
   OD_writeOutput8Bit[1] = 0;

#ifdef OD_testVar
   /* Configure Object dictionary entry at index 0x2120 */
    CO_OD_configure(CO->SDO, ODF_testDomain_index, ODF_testDomain, 0, 0, 0);
#endif
}


/******************************************************************************/
void programEnd(void)
{
   //CAN_RUN_LED = 0; CAN_ERROR_LED = 0;
}


/******************************************************************************/
void programAsync(uint16_t timer1msDiff)
{
    (void)timer1msDiff;
   //CAN_RUN_LED = LED_GREEN_RUN(CO->NMT);
   //CAN_ERROR_LED = LED_RED_ERROR(CO->NMT);

   //Is any application critical error set?
   //If error register is set, device will leave operational state.
    if(CO->em->errorStatusBits[8] || CO->em->errorStatusBits[9])
        *CO->emPr->errorRegister |= 0x20;
}

/******************************************************************************/
void program1ms(void)
{
   //Read RPDO and show it on LEDS.
   leds_write(OD_writeOutput8Bit[0]);

   //Example error is simulated from buttons on Explorer16
   //if(!PORTDbits.RD6) CO_errorReport(CO->EM, ERROR_TEST1_INFORMATIVE, 0x12345678L);
   //if(!PORTDbits.RD7) CO_errorReset(CO->EM, ERROR_TEST1_INFORMATIVE, 0xAAAAAABBL);

   //Prepare TPDO from buttons.
   //According to PDO mapping and communication parameters, first TPDO is sent
   //automatically on change of state of OD_readInput8Bit[0] variable.
   OD_readInput8Bit[0] = buttons_read();
}

#ifdef OD_testVar
/******************************************************************************/
/* Function passes some data to SDO server on testDomain variable access */
CO_SDO_abortCode_t ODF_testDomain(CO_ODF_arg_t *ODF_arg){

    /* domain data type is on subIndex 5, nothing to do on other subObjects */
    if(ODF_arg->subIndex != 5) return CO_SDO_AB_NONE;

    /* reading object dictionary */
    if(ODF_arg->reading){
        /* SDO buffer is filled with sequence 0x01, 0x02, ... */
        /* If domainFileSize is greater than SDObufferSize, this function will */
        /* be called multiple times during one SDO communication cycle */

        const uint32_t domainFileSize = 0x500;
        static uint32_t offset = 0;

        uint16_t i;
        uint16_t SDObufferSize = ODF_arg->dataLength;

        /* new SDO read cycle */
        if(ODF_arg->firstSegment){
            ODF_arg->dataLengthTotal = domainFileSize;
            offset = 0;
        }

        /* fill SDO buffer */
        for(i = 0; offset < domainFileSize; i++, offset++){
            if(i >= SDObufferSize){
                /* SDO buffer is full */
                ODF_arg->lastSegment = CO_false;
                break;
            }
            ODF_arg->data[i] = (uint8_t)(offset+1);
        }

        /* all data was copied */
        if(offset == domainFileSize){
            ODF_arg->lastSegment = CO_true;
            ODF_arg->dataLength = i;
        }

        /* return OK */
        return CO_SDO_AB_NONE;
    }

    /* writing object dictionary */
    else{
        uint16_t i;
        uint16_t err = 0;
        uint16_t dataSize = ODF_arg->dataLength;
        static uint32_t offset = 0;

        /* new SDO read cycle */
        if(ODF_arg->firstSegment){
            /* if(ODF_arg->dataLengthTotal) printf("\nWill receive %d bytes of data.\n", ODF_arg->dataLengthTotal); */
            offset = 0;
        }

        /* do something with data, here just verify if they are the same as above */
        for(i=0; i<dataSize; i++, offset++){
            uint8_t b = ODF_arg->data[i];
            if(b != (uint8_t)(offset+1)) err++;
            /* printf("%02X ", b); */
        }

        if(err) return CO_SDO_AB_INVALID_VALUE;

        /* end of transfer */
        /* if(ODF_arg->lastSegment) */
            /* printf("\nReceived %d bytes of data.\n", offset); */

        /* return OK */
        return CO_SDO_AB_NONE;
    }
}
#endif
