/*
 * CO_OD_cfg.h
 *
 */

#ifndef CO_OD_CFG_H_
#define CO_OD_CFG_H_

/*******************************************************************************
   FEATURES
*******************************************************************************/
   #define CO_NO_SYNC                     1   //Associated objects: 1005, 1006, 1007, 2103, 2104
   #define CO_NO_EMERGENCY                1   //Associated objects: 1014, 1015
   #define CO_NO_SDO_SERVER               1   //Associated objects: 1200
   #define CO_NO_SDO_CLIENT               1   //Associated objects: 1280
   #define CO_NO_RPDO                     16  //Associated objects from index 1400 to 160F, count = 32
   #define CO_NO_TPDO                     16  //Associated objects from index 1800 to 1A0F, count = 32
   #define CO_NO_NMT_MASTER               1
   #define CO_NO_HB_CONS                  4


#endif /* CO_OD_CFG_H_ */
