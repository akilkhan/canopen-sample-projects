/*
 * CO_OD_cfg.h
 *
 */

#ifndef CO_OD_CFG_H_
#define CO_OD_CFG_H_

/*******************************************************************************
   FEATURES
*******************************************************************************/
   #define CO_NO_SYNC                     0   //Associated objects: 1005, 1006, 1007, 2103, 2104
   #define CO_NO_EMERGENCY                1   //Associated objects: 1014, 1015
   #define CO_NO_SDO_SERVER               1   //Associated objects: 1200
   #define CO_NO_SDO_CLIENT               0
   #define CO_NO_RPDO                     4   //Associated objects: 1400, 1401, 1402, 1403, 1600, 1601, 1602, 1603
   #define CO_NO_TPDO                     4   //Associated objects: 1800, 1801, 1802, 1803, 1A00, 1A01, 1A02, 1A03
   #define CO_NO_NMT_MASTER               0
   #define CO_NO_HB_CONS                  0


#endif /* CO_OD_CFG_H_ */
