#!/bin/sh

#
#                               Copyright (c) 2014
#                       PhiRobotics Technologies Pvt Ltd
#               Vedant Commercial Complex, Vartak Nagar, Thane(w),
#                           Maharashtra-400606, India
#
#  For licensing information, see the file 'LICENSE' in the root folder of
#  this software module.
#

export TARGET_BOARD="HYDRA"
export PHI_NO_OS=1
export PHI_INTERNAL=1

CWD=$(pwd)

export PROJ_ROOT=$CWD
BUILD_TASKS=""
BUILD_TASKS="$BUILD_TASKS CDL"

# use it when everything will be Ok */
#currently cl, file, timer2, semaphore couldn't be built
#BUILD_TASKS="$BUILD_TASKS examples/*"

#use it to separately build only needed applications
BUILD_TASKS="$BUILD_TASKS examples/Example_IO"
BUILD_TASKS="$BUILD_TASKS examples/Example_Master"
BUILD_TASKS="$BUILD_TASKS examples/Example_IO_mini"

export BUILD_TASKS

export BUILD_TGT_BIN=1
# since this build system is based on PhiHAL's one we have to properly define some build variables
# in this build system PHIHAL_PATH is actually used to point to linker script templates which are moved to common package
export PHIHAL_PATH=$PROJ_ROOT/common
export COMMON_PATH=$PROJ_ROOT/common

export CDL_PATH=$PROJ_ROOT/CDL
export CANOPEN_STACK_PATH=$PROJ_ROOT/CANopenNode
export BSP_PATH=$PROJ_ROOT/bsp

export ADD_CPP_FLAGS=""

$COMMON_PATH/build/do_build.sh $* 2>&1
